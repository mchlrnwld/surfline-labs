import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

DATA_FILENAME = "./data/cleansed_data.csv"


def get_train_test(columns, test_percentage=0.2, numrows=None):

    if numrows:
        dat = pd.read_csv(DATA_FILENAME, nrows=numrows)
    else:
        dat = pd.read_csv(DATA_FILENAME)

    y = dat['human_report_surf_condition_int'].values
    #one_hot = np.zeros((y.size, y.max() + 1))
    #one_hot[np.arange(y.size), y] = 1
    #y = one_hot

    dat = dat[columns]
    dat = dat.dropna()

    max_data = len(dat.index)
    test_cut = int(max_data - max_data * test_percentage)

    y_train = y[0:test_cut]
    y_test = y[test_cut:max_data]
    x_train = dat.values[0:test_cut]
    x_test = dat.values[test_cut:max_data]

    return x_train, y_train, x_test, y_test


def plot_confusion_matrix(y_true, y_pred, classes,
                          normalize=False,
                          title=None,
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if not title:
        if normalize:
            title = 'Normalized confusion matrix'
        else:
            title = 'Confusion matrix, without normalization'

    # Compute confusion matrix
    cm = confusion_matrix(y_true, y_pred)
    # Only use the labels that appear in the data
    #lab = unique_labels(y_true, y_pred).astype(int)
    #print(type(lab))
    #print(lab)
    #classes = classes[unique_labels(y_true, y_pred)]
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    fig, ax = plt.subplots(figsize=(10,10))
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    #ax.figure.colorbar(im, ax=ax)
    # We want to show all ticks...
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           # ... and label them with the respective list entries
           xticklabels=classes, yticklabels=classes,
           title=title,
           ylabel='True label',
           xlabel='Predicted label')

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt),
                    ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")
    fig.tight_layout()
    return ax