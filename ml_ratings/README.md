# ML Ratings

This project uses the ML training set prepared using LOTUS hindcast data for
Dec 1st 2017 to Feb 28th 2018 munged with SL human reports and GFS winds. The
data is normalized for spot direction (zero is offshore from every spot).

The aim is to use XGBoost to predict the human surf heights and ratings.

# Running the project

`make build`

`make notebook`

# Workflow

The project is divided into four operational stages:

`clean_data.ipynb`

This notebook reads the raw ML data file containing LOTUS swell data, wind and 
human report data, with normalized offshore directions and does some basic 
housekeeping, then removes any hour for which their isn't a corresponding human 
report and adds a wind vector component perpendicular to the beach. This 
notebook only needs to be run once to create the cleansed data file.

`create_experiment_data.ipynb`

This notebook takes the cleansed data and creates a model training set of 
directionally binned period and heights, with user specified additional 
columns from the original data set. These data sets are saved with an 
experiment specific prefix in both a test and train split.

`experiment-coloredbox-1.ipynb`
`experiment-maxheight-1.ipynb`

These experiments use xgboost to create predictive models for max surf heights
and human ratings respectively.

`process_live.ipynb`

A proof of concept notebook that loads a pickled model created in 
`experiment-maxheight-1.ipynb` and connects to the LOTUS API running on a docker
container on port 5000. Instructions for building and running this, and creating
the network connection are in the [LOTUS API Folder](../lotus_ml_api/README.md) 