# Example Project

The surfline-labs repo is a home for a variety of projects from full, 
working model prototypes to R&D and proof of concept work. There are some rules
defining how all projects should be structured:

- Projects should use docker to containerize their operation and manage their
own dependencies.
- All code required to build and manager the container should be included in the
project folder.
- All code required to run the project inside the container should be in `/src`
- Project documentation should describe the setup and installtion process from first principles

## Cloning this project

This project can be copied as a boilerplate as a start for new projects. It 
creates a simple python app that graphs some wind data.

- Copy this folder at the same level in this repo changing the folder name to 
your project name
- Edit `environment.yml` and change the name of your environment at the start of
the file to match your projet name. Add project dependencies and requirements to
this boilerplate
- Edit `Makefile` and amend `PROJECTNAME=` to your project name.

## Building this project.

To build the project:
 
 ```
 cp .env.sample .env
 make build
 ``` 

## Running this project

To run the project:

```
make run
source activate *PROJECTNAME*
python wind_graph.py
```