/**
 *  Index component which is displayed on the home page. Features include
 *  pagination and clickable blog description/images which will direct you
 *  to a post.
 */

import React from 'react'
import { graphql } from 'gatsby'

import IndexPage from '../components/index-page'

const BlogIndex = ({ data, location }) => {
  return (
    <IndexPage data={data} location={location} />
  )
}

export default BlogIndex

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    allMdx(limit: 4, sort: { fields: [frontmatter___date], order: DESC }) {
      totalCount
      edges {
        node {
          excerpt(pruneLength: 110)
          fields {
            slug
          }
          frontmatter {
            date(formatString: "MMMM DD, YYYY")
            title
            description
            author
            duration
            thumbnail {
              childImageSharp {
                fluid(maxWidth: 417, maxHeight: 236) {
                   ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
    }
  }
`
