import React from 'react'

import 'typeface-ibm-plex-sans'
import 'typeface-source-sans-pro'

const RequestAccess = () => {

  let submitForms = function() {
    const beachName = document.getElementById('beachNameForm').value;
    const email = document.getElementById('emailForm').value;

    window.analytics.track("Submitted Email Signup", {
      email: email,
      beach_name: beachName,
      source: 'LABS'
    })
  }

  return (
    <div className='container-fluid requestAccessContainer'>
      <div className='row'>
        <div className='offset-lg-2 col-lg-3 grid-col'>
          <div className='wantThis'>
            Want this where you surf?
          </div>
          <div className='formWrapper'>
            <form className='formRmvMrgn'>
              <input type='text' id='beachNameForm' className='requestAccessForm beachForm' placeholder='Beach Name & Location' size={38} />
            </form>
          </div>
        </div>
        <div className='col-lg-3 grid-col'>
          {/* Need this placeholder to vertical align with parent */}
          <div className='wantThis invisible'>
            invisible
          </div>
          <div className='formWrapper'>
            <form className='formRmvMrgn'>
              <input type='email' id='emailForm' className='requestAccessForm emailForm' placeholder='Email Address*' size={38} required />
              <div className='sendRequestContainer'>
                <div className='sendRequest'>
                  Send Request
                </div>
                <button type='submit' className='sendRequestButton' onClick={submitForms} />
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  )
}

export default RequestAccess