---
title: Machine Learning Surf Cameras
date: "2015-05-06T23:46:37.121Z"
thumbnail: '../../../images/preview_images/image_2.png'
author: 'Ben Freeston'
duration: '4 min read'
---

The surf zone on a popular beach is a particularly dynamic environment with waves, wind and currents interacting with surfers and bathers

Wow! I love blogging so much already.

Did you know that "despite its name, salted duck eggs can also be made from
chicken eggs, though the taste and texture will be somewhat different, and the
egg yolk will be less rich."?
([Wikipedia Link](https://en.wikipedia.org/wiki/Salted_duck_egg))

Yeah, I didn't either.
