---
title: Nearshore Bathymetry Modeling
date: "2015-05-01T22:12:03.284Z"
description: "Hello World"
thumbnail: '../../../images/preview_images/image_1.png'
author: 'Ben Freeston'
duration: '4 min read'
---

Modeling the bathymetry near to surf spots gives us a great insight and indication of how they will react to swell events.

This is my first post on my new fake blog! How exciting!

I'm sure I'll write a lot more interesting things in the future.

Oh, and here's a great quote from this Wikipedia on
[salted duck eggs](https://en.wikipedia.org/wiki/Salted_duck_egg).

> A salted duck egg is a Chinese preserved food product made by soaking duck
> eggs in brine, or packing each egg in damp, salted charcoal. In Asian
> supermarkets, these eggs are sometimes sold covered in a thick layer of salted
> charcoal paste. The eggs may also be sold with the salted paste removed,
> wrapped in plastic, and vacuum packed. From the salt curing process, the
> salted duck eggs have a briny aroma, a gelatin-like egg white and a
> firm-textured, round yolk that is bright orange-red in color.

![Chinese Salty Egg](salty_egg.jpg)
