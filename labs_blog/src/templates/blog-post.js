/**
 *  Blog post template for creating blog posts. Features include clickable
 *  modals and post body text loaded in from mdx files.
 */

import React, { useState } from 'react'
import { Link, graphql } from 'gatsby'
import { MDXRenderer } from 'gatsby-plugin-mdx'
import { Modal, ModalBody} from 'reactstrap'

import Layout from '../components/layout'
import logoModal from '../images/logos/logo_modal.png'
import 'typeface-ibm-plex-sans'
import 'typeface-source-sans-pro'

const BlogPostTemplate = ({ data }) => {
  // Load in post text
  const post = data.mdx

  // Create a toggle for our modal which will tell it to display/hide
  const [modal, setModal] = useState(false);
  const toggle = () => setModal(!modal);

  // Use these to hide/display success alerts
  const [className, setClassname] = useState('');
  const [message, setMessage] = useState('');

  let submitForms = function(event) {
    // Prevent default behavior of POSTing on a form submit
    event.preventDefault();

    const beachName = document.getElementById('modalFormLocationSubmit').value;
    const email = document.getElementById('modalFormEmailSubmit').value;

    window.analytics.track('Submitted Email Signup', {
      email: email,
      beach_name: beachName,
      source: 'LABS'
    })

    // Display success alert
    setClassname('alert alert-success modalNotification');
    setMessage('Success! Your information has been submitted.');

    // Remove success alert after 5 seconds
    setTimeout(() => {
      setClassname('');
      setMessage('');
    }, 5000);

    // Reset form input
    document.getElementById('modalFormLocationSubmit').value = '';
    document.getElementById('modalFormEmailSubmit').value = '';
  }

  return (
    <Layout>
      <div className='container-fluid postContainer'>
        <div className='row'>
          <div className='col-2 d-none d-lg-block grid-col'>
            <div className='getAccess'>
              <div className='getAccessTitle'>
                Surfline Labs
              </div>
              <div className='getAccessDescription'>
                Enter the lab. Access new Tech. <b>Surf More.</b>
              </div>
              <div className='getAccessButton'>
                <button onClick={toggle} className='getAccessBtn' aria-label='Load modal popup' />
                <Modal isOpen={modal} toggle={toggle} className='myModal'>
                  <ModalBody className='modalBody'>
                    <div className='backgroundModalLogo'>
                      <img src={logoModal} alt='Background logo on modal' />
                    </div>
                    <div className='modalTitleText'>
                      Together, we'll create the future of surfing applications.
                    </div>
                    <div className='modalBodyText'>
                      Become a beta tester for Surfline Labs, work with us to develop the future of surfing apps, and get exclusive access to new technology as it develops.
                    </div>
                    <form>
                      <div className='modalFormEmail'>
                        <input type='email' id='modalFormEmailSubmit' className='getAccessInput' placeholder='Your Email Address*' size={44} required />
                      </div>
                      <div className='modalFormLocation'>
                        <input type='text' id='modalFormLocationSubmit' className='getAccessInput' placeholder='Local Beach / Location' size={44} />
                      </div>
                      <div className='modalRequestAccess'>
                        Request Access
                      </div>
                      <button type='submit' className='modalSubmitFormButton' onClick={submitForms} aria-label='Submit email form' />
                    </form>
                    <div className='modalFooter'>
                      By signing up to Labs, you are agreeing to consent to Surfline’s
                      <Link to={'https://www.surfline.com/terms-of-use'} className='modalTerms modalLinkStyle'>
                        &nbsp;Terms of Use&nbsp;
                      </Link>
                      &
                      <Link to={'https://www.surfline.com/privacy-policy'} className='modalTerms modalLinkStyle'>
                        &nbsp;Privacy Policy
                      </Link>
                    </div>
                  </ModalBody>
                  <div id='alert' className={className} role='alert'>
                    {message}
                  </div>
                </Modal>
              </div>
            </div>
          </div>
          <div className='col-10 grid-col'>
            <div className='date'>
              {post.frontmatter.date}
            </div>
            <div className='postTitle'>
              {post.frontmatter.title}
            </div>
            <div className='postBody'>
              <MDXRenderer>{post.body}</MDXRenderer>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default BlogPostTemplate

export const query = graphql`
  query BlogPostBySlug($slug: String!) {
    site {
      siteMetadata {
        title
      }
    }
    mdx(fields: { slug: { eq: $slug } }) {
      id
      body
      frontmatter {
        title
        date(formatString: "MMMM DD, YYYY")
        description
      }
    }
    avatar: file(absolutePath: { regex: "/profile_pics/ben_pic.png/" }) {
        childImageSharp {
          fixed(width: 48, height: 48) {
            ...GatsbyImageSharpFixed
          }
        }
    }
  }
`
