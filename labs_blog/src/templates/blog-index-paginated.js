/**
 *  Index component which is displayed on the home page. Features include
 *  pagination and clickable blog description/images which will direct you
 *  to a post. This is the template that `gatsby-awesome-pagination`'s
 *  paginate function uses. They require it to be a template, hence why we
 *  have created an index component which this template accesses.
 */

import React from 'react'
import { graphql } from 'gatsby'
import IndexPage from '../components/index-page'

const BlogIndexPaginated = ({ data, location, pageContext }) => {
  const { previousPagePath, nextPagePath } = pageContext;

  return (
    <IndexPage data={data} location={location} previousPagePath={previousPagePath} nextPagePath={nextPagePath} />
  )
}

export default BlogIndexPaginated

export const query = graphql`
  query($skip: Int!, $limit: Int!) {
    site {
      siteMetadata {
        title
      }
    }
    allMdx(
      sort: { fields: [frontmatter___date], order: DESC }
      skip: $skip
      limit: $limit
    ) {
      totalCount
      edges {
        node {
          excerpt(pruneLength: 110)
          fields {
            slug
          }
          frontmatter {
            date(formatString: "MMMM DD, YYYY")
            title
            description
            author
            duration
            thumbnail {
              childImageSharp {
                fluid(maxWidth: 417, maxHeight: 236) {
                   ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
    }
  }
`