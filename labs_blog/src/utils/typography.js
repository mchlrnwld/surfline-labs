import Typography from "typography"

const typography = new Typography({
  baseFontSize: "18px",
  baseLineHeight: 1.666,
  headerFontFamily: [
      "Chathura",
    "Avenir Next",
    "Helvetica Neue",
    "Segoe UI",
    "Helvetica",
    "Arial",
    "sans-serif",
  ],
  bodyFontFamily: ["Source Sans Pro", "Georgia", "serif"],
})

export default typography
export const rhythm = typography.rhythm
export const scale = typography.scale
