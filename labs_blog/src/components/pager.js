/**
 *  Pager component which allows user to scroll through multiple pages
 *  of blog posts. Backend logic in gatsby-node.js `paginate` function.
 */

import React from 'react'
import { Link } from 'gatsby'

import arrowLeft from '../images/logos/arrow_left.svg'
import arrowRight from '../images/logos/arrow_right.svg'
import 'typeface-ibm-plex-mono'

const Pager = ( { numPosts, previousPagePath, nextPagePath } ) => {
  // Styling for when the link is active
  const linkActiveStyle = {
    textDecoration: 'none',
    color: 'black'
  }

  // If there are at least 2 pages, set nextPagePath to /2 when on /
  if (numPosts > 4 && previousPagePath === undefined && nextPagePath === undefined) {
    nextPagePath = '/2'
  }

  // Store all of the pagination links
  let paginatorPages = []

  // Default is 1 page
  paginatorPages.push(
    <li>
      <Link to={'/'} className='paginateLink' activeStyle={linkActiveStyle}>
        1
      </Link>
    </li>
  )

  /* Add more pagination links depending on the amount of posts.
   * Dividing by 4 because we are displaying 4 posts per page.
   */
  for (let i = 1; i < numPosts / 4; i++) {
    paginatorPages.push(
      <li>
        <Link to={'/' + (i + 1)} className='paginateLink' activeStyle={linkActiveStyle}>
          {i + 1}
        </Link>
      </li>
    )
  }

  return (
    <ul className='paginate-horizontal'>
      <li>
        <Link to={previousPagePath} className='paginateLink' activeStyle={linkActiveStyle}>
          <img src={arrowLeft} alt={'Arrow left'} />
        </Link>
      </li>
      {paginatorPages.map(page => page)}
      <li>
        <Link to={nextPagePath} className='paginateLinkRight' activeStyle={linkActiveStyle}>
          <img src={arrowRight} alt={'Arrow right'} />
        </Link>
      </li>
    </ul>
  )
}

export default Pager