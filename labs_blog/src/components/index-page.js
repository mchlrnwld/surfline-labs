/**
 *  Index component which is displayed on the home page. Features include
 *  pagination and clickable blog description/images which will direct you
 *  to a post.
 */

import React from 'react'
import { Link } from 'gatsby'
import Img from 'gatsby-image'

import Layout from '../components/layout'
import Pager from '../components/pager'
import 'typeface-ibm-plex-sans'
import 'typeface-source-sans-pro'

// Remove text decoration from the links
const noTextDecStyle = {
  textDecoration: 'none',
  color: 'black'
}

const IndexPage = ({ data, location, previousPagePath, nextPagePath }) => {
  // Get data from query
  const siteTitle = data.site.siteMetadata.title;
  const posts = data.allMdx.edges;
  const numPosts = data.allMdx.totalCount;

  return (
    <Layout location={location} title={siteTitle}>
      {posts.map(({ node }) => {
        // Get data from query
        const title = node.frontmatter.title || node.fields.slug
        const description = node.excerpt
        const key = node.fields.slug
        const image = node.frontmatter.thumbnail.childImageSharp.fluid
        const author = node.frontmatter.author
        const readDuration = node.frontmatter.duration

        return (
          <div>
            <div className='container-fluid sectionContainer'>
              <div className='row'>
                <div className='col-md-1 grid-col' />
                <div className='col-md-6 grid-col'>
                  <div className='previewImage'>
                    <Link to={key}>
                      <Img fluid={image} />
                    </Link>
                  </div>
                </div>
                <div className='col-md-5 grid-col'>
                  <div className='title'>
                    <Link to={key} style={noTextDecStyle}>
                      {title}
                    </Link>
                  </div>
                  <div className='description'>
                    <Link to={key} style={noTextDecStyle}>
                      {description}
                    </Link>
                  </div>
                  <div className='readInfo'>
                    {author}
                    <div className='ellipse' />
                    {readDuration}
                  </div>
                </div>
              </div>
            </div>
            <hr className='indexBreak' size={1}/>
          </div>
        )
      })}
      <Pager numPosts={numPosts} previousPagePath={previousPagePath} nextPagePath={nextPagePath} />
    </Layout>
  )
}

export default IndexPage
