/**
 *  Image component which allows you to display a dynamic image
 *  within a blog post. Caption optional.
 */

import React from 'react'
import { StaticQuery, graphql } from 'gatsby'
import Img from 'gatsby-image'

const Image = ({ imgName, className, caption }) => (
  <StaticQuery
    query={graphql`
      query {
        allImageSharp {
          edges {
            node {
              desktop: fluid(maxWidth: 1500) {
                ...GatsbyImageSharpFluid
                originalName
              }
              mobile: fluid(maxWidth: 640) {
                ...GatsbyImageSharpFluid
                originalName
              }
            }
          }
        }
      }
    `}

    render={data => {
      // Get gatsby image payload
      const image = data.allImageSharp.edges.find(
        edge => edge.node.desktop.originalName === imgName
      )

      // Use art-direction to load in different images for different screen sizes
      const sources = [
        image.node.mobile,
        {
          ...image.node.desktop,
          media: `(min-device-width: 640px)`
        },
      ]

      if (caption === '') {
          return <Img fluid={sources} className={"image " + className} />
      } else {
          return (
            <div>
              <Img fluid={sources} className={"image " + className} />
              <div className='container-fluid caption'>
                {caption}
              </div>
            </div>
          )
      }
    }}
  />
)

export default Image