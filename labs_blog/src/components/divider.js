/**
 *  Horizontal divider displayed between the end of a blog post
 *  and the start of the Bio component.
 */

import React from 'react'

const Divider = () => {
  return (
    <hr className='postBreak' size={1} />
  )
}

export default Divider
