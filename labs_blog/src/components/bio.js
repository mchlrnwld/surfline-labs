/**
 *  Bio component displayed at the end of blog posts. Includes an avatar,
 *  name, description, and twitter handle.
 */

import React from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import Image from 'gatsby-image'

import { rhythm } from '../utils/typography'
import followButton from '../images/logos/follow_button.svg'
import 'typeface-source-sans-pro'

const Bio = () => {
  const data = useStaticQuery(graphql`
    query BioQuery {
      avatar: file(absolutePath: { regex: "/ben_pic.png/" }) {
        childImageSharp {
          fixed(width: 50, height: 50) {
            ...GatsbyImageSharpFixed
          }
        }
      }
      site {
        siteMetadata {
          author {
            name
            summary
          }
          social {
            twitter
          }
        }
      }
    }
  `)

  const { author, social } = data.site.siteMetadata;
  const twitterHandle = `https://twitter.com/${social.twitter}`;

  return (
    <div style={{display: `flex`}}>
      <Image
        fixed={data.avatar.childImageSharp.fixed}
        alt={author.name}
        style={{
          marginRight: rhythm(1 / 2),
          marginBottom: 0,
          minWidth: 50,
          borderRadius: `100%`,
        }}
        imgStyle={{
          borderRadius: `50%`,
        }}
      />
      <p className='aboutAuthorTitle'>
        {author.name}, {author.summary}
      </p>
      <a className='twitterHandle' href={twitterHandle}>
        @{social.twitter}
      </a>
      <a href={twitterHandle}>
        <img src={followButton} className='twitterButton' alt='Follow twitter'/>
      </a>
    </div>
  )
}

export default Bio
