/**
 *  Request Access component for use in blog posts. Features include a form
 *  to sign up to the labs email list.
 */

import React, { useState } from 'react'

import 'typeface-ibm-plex-sans'
import 'typeface-source-sans-pro'

const RequestAccess = () => {
  // Use these to hide/display success alerts
  const [className, setClassname] = useState('');
  const [message, setMessage] = useState('');

  let submitForms = function(event) {
    // Prevent default behavior of POSTing on a form submit
    event.preventDefault();

    // Send data to segment
    const beachName = document.getElementById('beachNameForm').value;
    const email = document.getElementById('emailForm').value;
    window.analytics.track('Submitted Email Signup', {
      email: email,
      beach_name: beachName,
      source: 'LABS'
    })

    // Display success alert
    setClassname('alert alert-success requestAccessNotification');
    setMessage('Success! Your information has been submitted.');

    // Remove success alert after 5 seconds
    setTimeout(() => {
      setClassname('');
      setMessage('');
    }, 5000);

    // Reset form input
    document.getElementById('beachNameForm').value = '';
    document.getElementById('emailForm').value = '';
  }

  return (
    <div className='container-fluid requestAccessContainer'>
      <div className={className} role='alert'>
        {message}
      </div>
      <div className='row'>
        <div className='offset-lg-2 col-lg-3 grid-col'>
          <div className='wantThis'>
            Want this where you surf?
          </div>
          <div className='formWrapper'>
            <form className='formRmvMrgn'>
              <input type='text' id='beachNameForm' className='requestAccessForm beachForm' placeholder='Beach Name & Location' size={38} />
            </form>
          </div>
        </div>
        <div className='col-lg-3 grid-col'>
          {/* Need this placeholder to vertical align with parent */}
          <div className='wantThis invisible'>
            invisible
          </div>
          <div className='formWrapper'>
            <form className='formRmvMrgn' onSubmit={submitForms}>
              <input type='email' id='emailForm' className='requestAccessForm emailForm' placeholder='Email Address*' size={38} required />
              <div className='sendRequestContainer'>
                <div className='sendRequest'>
                  Send Request
                </div>
                <button type='submit' className='sendRequestButton' aria-label='Submit email form' />
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  )
}

export default RequestAccess