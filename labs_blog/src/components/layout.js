/**
 *  Layout component which controls how the site looks globally.
 */

import React from 'react'
import { Link } from 'gatsby'

import Nav from './nav'
import Footer from './footer'
import logoHeader from '../images/logos/logo_header.svg'
import logoHeaderMobile from '../images/logos/logo_mobile.svg'
import accessLab from '../images/logos/access_lab.svg'

const Layout = ({ children }) => {
  return (
    <div>
      <div>
        <div className='homeHeaderMobile'>
          <Link to='/'>
            <img className='logoMobile' src={logoHeaderMobile} alt='Surfline logo mobile'/>
          </Link>
          <Link to='/'>
            <img className='accessLabs' src={accessLab} alt='Download app'/>
          </Link>
        </div>
        <hr className='headerBreakMobile' size={1} />
      </div>
      <div>
        <div className='homeHeader'>
          <Link to='/'>
            <img className='logo' src={logoHeader} alt='Surfline logo'/>
          </Link>
        </div>
        <hr className='headerBreak' size={1} />
      </div>
      <Nav/>
      <main>{children}</main>
      <Footer />
    </div>
  )
}

export default Layout
