/**
 *  Footer component displayed at the very bottom of the site. Includes
 *  privacy policy, email sign up, and helpful links.
 */

import React, { useState } from 'react'
import { Link } from 'gatsby'

import slLogo from '../images/logos/SL_logo.svg'
import 'typeface-ibm-plex-mono'
import 'typeface-ibm-plex-sans'

const Footer = () => {
  // Use these to hide/display success alerts
  const [className, setClassname] = useState('');
  const [message, setMessage] = useState('');

  let submitForms = function(event) {
    // Prevent default behavior of POSTing on a form submit
    event.preventDefault();

    // Send data to segment
    let email = document.getElementById('emailSignUp').value;
    window.analytics.track('Submitted Email Signup', {
      email: email,
      source: 'LABS'
    })

    // Display success alert
    setClassname('alert alert-success footerNotification');
    setMessage('Success! Your email has been submitted.');

    // Remove success alert after 5 seconds
    setTimeout(() => {
      setClassname('');
      setMessage('');
      }, 5000);

    // Reset form input
    document.getElementById('emailSignUp').value = '';
  }

  return (
    <div className='container-fluid footerContainer'>
      <div id='alert' className={className} role='alert'>
        {message}
      </div>
      <div className='row'>
        <div className='col-md-4 grid-col'>
          <div className='surflineLogoFooter'>
            <a href='https://www.surfline.com/'>
              <img src={slLogo} alt='Surfline logo'/>
            </a>
          </div>
          <div className='termsAndPrivacyFooter'>
            ©2020 Surfline/Wavetrak, Inc.<br />
            <a href={'https://www.surfline.com/terms-of-use'} className='terms linkStyle'>
              &nbsp;Terms of Use&nbsp;
            </a>
            and
            <a href={'https://www.surfline.com/privacy-policy'} className='terms linkStyle'>
              &nbsp;Privacy Policy
            </a>
          </div>
        </div>
        <div className='col-md-3 grid-col'>
          <div className='goHomeFooter'>
            <Link to={'/'} className='surflineLabs linkStyle'>
              surfline labs
            </Link>
          </div>
          <ul className='footerList'>
            <li>
              <Link to={'/'} className='linkStyle'>
                About Labs
              </Link>
            </li>
            <li>
              <Link to={'/'} className='linkStyle'>
                Labs app
              </Link>
            </li>
            <li>
              <a href={'https://careers.surfline.com/'} className='linkStyle'>
                Careers
              </a>
            </li>
            <li>
              <Link to={'/'} className='linkStyle'>
                Contact
              </Link>
            </li>
          </ul>
        </div>
        <div className='col-md-5 grid-col'>
          <ul className='signUpFooter'>
            <li className='signUpTitleFooter'>
              sign up
            </li>
            <li className='firstToKnowFooter'>
              Be the first to know about new tech, updates and articles from the lab.
            </li>
            <li>
              <form className='submitFooterForm' onSubmit={submitForms}>
                <input type='email' id='emailSignUp' className='enterEmailFooter' placeholder='Your Email Address' required />
                <button type='submit' className='submitEmailButton' aria-label='Submit email' />
              </form>
              <hr className='emailBreak'/>
            </li>
          </ul>
        </div>
      </div>
    </div>
  )
}

export default Footer