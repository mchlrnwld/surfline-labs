/**
 *  Navigation component displayed at the top of the site. Navigates to
 *  the latest posts, about section, and a download app link.
 */

import React from 'react'
import { Link } from 'gatsby'

import 'typeface-ibm-plex-sans'

// Styling for when the nav link is not active
const navLinkStyle = {
  textDecoration: 'none',
  color: 'black'
}

// Styling for when the nav link is active
const navLinkActiveStyle = {
  paddingBottom: '12px',
  textDecoration: 'none',
  borderBottom: '2px solid #314EE6'
}

// Styling for when the nav link is active for mobile devices
const navLinkActiveStyleMobile = {
  paddingBottom: '18px',
  textDecoration: 'none',
  borderBottom: '2px solid #314EE6'
}

const Nav = () => {
  return (
    <div>
      <div className='navContainerMobile'>
        <ul className='navMobile'>
          <li className='latestMobile'>
            <Link to='/' style={navLinkStyle} activeStyle={navLinkActiveStyleMobile}>
              Latest
            </Link>
          </li>
          <li className='aboutMobile'>
            <Link to='' style={navLinkStyle} activeStyle={navLinkActiveStyleMobile}>
              About
            </Link>
          </li>
        </ul>
        <hr className='navBreak' size={1}/>
      </div>
      <div className='navContainer'>
        <ul className='nav'>
          <li className='latest'>
            <Link to='/' style={navLinkStyle} activeStyle={navLinkActiveStyle}>
              Latest
            </Link>
          </li>
          <li className='about'>
            <Link to='' style={navLinkStyle} activeStyle={navLinkActiveStyle}>
              About
            </Link>
          </li>
          <li className='download'>
            <Link to='' style={navLinkStyle} activeStyle={navLinkActiveStyle}>
              Download App
            </Link>
          </li>
        </ul>
        <hr className='navBreak' size={1}/>
      </div>
    </div>
  )
}

export default Nav