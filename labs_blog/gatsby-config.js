module.exports = {
    siteMetadata: {
        title: `Surfline Labs`,
        author: {
            name: `Ben Freeston`,
            summary: `VP of Data Science`,
        },
        description: `Imagination and Innovation.`,
        siteUrl: `http://localhost:3000`,
        social: {
            twitter: `ben_freeston`,
        },
    },
    plugins: [
        {
            resolve: `gatsby-source-filesystem`,
            options: {
                path: `${__dirname}/src/pages/blog/`,
                name: `blog`,
            },
        },
        {
            resolve: `gatsby-source-filesystem`,
            options: {
                path: `${__dirname}/content/assets`,
                name: `assets`,
            },
        },
        {
            resolve: `gatsby-source-filesystem`,
            options: {
                path: `${__dirname}/src/images`,
                name: `images`,
            },
        },
        {
            resolve: `gatsby-source-filesystem`,
            options: {
                path: `${__dirname}/src/components`,
                name: `components`,
            },
        },
        {
            resolve: `gatsby-plugin-mdx`,
            options: {
                extensions: [".mdx", ".md"],
                gatsbyRemarkPlugins: [
                    {
                        resolve: `gatsby-remark-images`,
                        options: {
                            maxWidth: 590,
                        },
                    },
                    {
                        resolve: `gatsby-remark-responsive-iframe`,
                        options: {
                            wrapperStyle: `margin-bottom: 1.0725rem`,
                        },
                    },
                    `gatsby-remark-prismjs`,
                    `gatsby-remark-copy-linked-files`,
                    `gatsby-remark-smartypants`,
                ],
            },
        },
        {
            resolve: `gatsby-plugin-google-analytics`,
            options: {
                //trackingId: `ADD YOUR TRACKING ID HERE`,
            },
        },
        {
            resolve: `gatsby-plugin-manifest`,
            options: {
                name: `Gatsby Starter Blog`,
                short_name: `GatsbyJS`,
                start_url: `/`,
                background_color: `#ffffff`,
                theme_color: `#663399`,
                display: `minimal-ui`,
                icon: `content/assets/gatsby-icon.png`,
            },
        },
        {
            resolve: `gatsby-plugin-typography`,
            options: {
                pathToConfigModule: `src/utils/typography`,
            },
        },
        {
            resolve: `gatsby-plugin-segment-js`,
            options: {
                // your segment write key for your production environment
                // when process.env.NODE_ENV === 'production'
                // required; non-empty string
                prodKey: `Se6IGuzcvUhLx55hLHL0RiXkS6oUTz8L`,

                // if you have a development env for your segment account, paste that key here
                // when process.env.NODE_ENV === 'development'
                // optional; non-empty string
                devKey: `VQDMbHw65jkXg4go8KmBnDiXzeAz7GiO`,

                // boolean (defaults to false) on whether you want
                // to include analytics.page() automatically
                // if false, see below on how to track pageviews manually
                trackPage: false,

                // number (defaults to 50); time to wait after a route update before it should
                // track the page change, to implement this, make sure your `trackPage` property is set to `true`
                trackPageDelay: 50,

                // If you need to proxy events through a custom endpoint,
                // add a `host` property (defaults to https://cdn.segment.io)
                // Segment docs:
                //   - https://segment.com/docs/connections/sources/custom-domains
                //   - https://segment.com/docs/connections/sources/catalog/libraries/website/javascript/#proxy
                host: `https://cdn.segment.io`,

                // boolean (defaults to false); whether to delay load Segment
                // ADVANCED FEATURE: only use if you leverage client-side routing (ie, Gatsby <Link>)
                // This feature will force Segment to load _after_ either a page routing change
                // or user scroll, whichever comes first. This delay time is controlled by
                // `delayLoadTime` setting. This feature is used to help improve your website's
                // TTI (for SEO, UX, etc).  See links below for more info.
                // NOTE: But if you are using server-side routing and enable this feature,
                // Segment will never load (because although client-side routing does not do
                // a full page refresh, server-side routing does, thereby preventing Segment
                // from ever loading).
                // See here for more context:
                // GIF: https://github.com/benjaminhoffman/gatsby-plugin-segment-js/pull/19#issuecomment-559569483
                // TTI: https://github.com/GoogleChrome/lighthouse/blob/master/docs/scoring.md#performance
                // Problem/solution: https://marketingexamples.com/seo/performance
                delayLoad: true,

                // number (default to 1000); time to wait after scroll or route change
                // To be used when `delayLoad` is set to `true`
                delayLoadTime: 1000,

                // Whether to completely skip calling `analytics.load()`.
                // ADVANCED FEATURE: only use if you are calling `analytics.load()` manually
                // elsewhere in your code or are using a library
                // like: https://github.com/segmentio/consent-manager that will call it for you.
                // Useful for only loading the tracking script once a user has opted in to being tracked, for example.
                manualLoad: false
            }
        },
        {
            resolve: `gatsby-transformer-remark`,
            options: {
                // CommonMark mode (default: true)
                commonmark: true,
                // Footnotes mode (default: true)
                footnotes: true,
                // Pedantic mode (default: true)
                pedantic: true,
                // GitHub Flavored Markdown mode (default: true)
                gfm: true,
                // Plugins configs
                plugins: [],
            },
        },
        // this (optional) plugin enables Progressive Web App + Offline functionality
        // To learn more, visit: https://gatsby.dev/offline
        // `gatsby-plugin-offline`,
        `gatsby-transformer-sharp`,
        `gatsby-plugin-sharp`,
        `gatsby-awesome-pagination`,
        `gatsby-plugin-react-helmet`,
        `gatsby-plugin-modal-routing`,
        `gatsby-plugin-feed-mdx`
    ],
}
