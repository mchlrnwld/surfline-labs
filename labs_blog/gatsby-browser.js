// Custom typefaces
import 'typeface-montserrat'
import 'typeface-merriweather'

import 'prismjs/themes/prism.css'
import './src/global.css'
import 'bootstrap/dist/css/bootstrap.css';
require('typeface-source-sans-pro');
require('typeface-chathura');

// Track route changes with segment
export function onRouteUpdate() {
  window.analytics && window.analytics.page('labs');
}