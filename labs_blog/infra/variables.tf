variable "region" {
  description = "The region these resources are being created in"
  default     = "us-west-1"
}

variable "company" {
  description = "The company (sl, bw, ft, wt, msw)"
  default     = "sl"
}

variable "environment" {
  description = "The environment (sandbox, staging, prod)"
  default     = "staging"
}

variable "subdomain" {
  description = "The host subdomain."
  default     = "staging"
}

variable "lambda_function_associations" {
  description = "List of lambda functions to execute at the edge."
  default     = []
}

variable "cdn_acm_domains" {
  description = "CDN SSL cert domain."
}

variable "cdn_fqdn" {
  description = "Mapping of CDN SSL cert domain to full DNS name."
}
