provider "aws" {
  region = var.region
}

module "labs-surfline-com-cdn" {
  source = "../../../wavetrak-infrastructure/terraform/modules/aws/cloudfront-with-s3"
  company = var.company
  environment = var.environment
  application = "${var.subdomain}.surfline.com"
  service = "cdn"
  cdn_acm_count = "1"
  cdn_acm_domains = var.cdn_acm_domains
  cdn_fqdn = var.cdn_fqdn

  comment = "${var.environment} ${var.subdomain}.surfline.com CDN wth S3"

  versioning_enabled = false
  allowed_origins = "*"

  lambda_function_associations = var.lambda_function_associations
}