terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "surfline-labs/labs-blog/terraform.tfstate"
    region = "us-west-1"
  }
}

module "s3-cdn" {
  source = "../../"

  environment   = "prod"
  subdomain     = "labs"

  cdn_acm_domains = [
    "surfline.com"
  ]

  cdn_fqdn = {
    # Should be app.surfzone.ai must match cdn_acm_domain above.
    "surfline.com" = [
      # Should be full subdomain
      "labs.surfline.com"
    ]
  }
}
