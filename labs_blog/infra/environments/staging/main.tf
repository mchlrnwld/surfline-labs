terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "surfline-labs/labs-blog/terraform.tfstate"
    region = "us-west-1"
  }
}

# Lambda@Edge must be Virginia region.
provider "aws" {
  region = "us-east-1"
}

# For staging we want to add basic auth so a) it doesn't get crawled and b) isn't "public".
locals {
  full_function_name = "sl-labs-auth-lambda-staging"
  company = "sl"
}

# Create the Lambda function
resource "aws_lambda_function" "lambda_function" {
  function_name                  = local.full_function_name
  runtime                        = "nodejs12.x"
  s3_bucket                      = "sl-artifacts-dev-us-east-1"
  s3_key                         = "lambda-functions/${local.full_function_name}.zip"
  handler                        = "handlers/index.handler"
  memory_size                    = "128"
  timeout                        = "5"
  role                           = aws_iam_role.lambda_role.arn
  publish                        = true

  tags = {
    Company     = "sl"
    Service     = "lambda"
    Application = "labs-auth-staging"
    Environment = "staging"
    Terraform   = "true"
  }
}

resource "aws_iam_role" "lambda_role" {
  name        = "${local.company}-labs-auth-staging-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [{
    "Action": "sts:AssumeRole",
    "Principal": {
      "Service": ["lambda.amazonaws.com", "edgelambda.amazonaws.com"]
    },
    "Effect": "Allow"
  }]
}
EOF
}

module "s3-cdn" {
  source = "../../"

  environment   = "staging"
  subdomain     = "staging.labs"

  cdn_acm_domains = [
    "labs.surfline.com"
  ]

  cdn_fqdn = {
    # Should be app.surfzone.ai must match cdn_acm_domain above.
    "labs.surfline.com" = [
      # Should be full subdomain
      "staging.labs.surfline.com"
    ]
  }

  lambda_function_associations = [{
    event_type   = "viewer-request"
    lambda_arn   = aws_lambda_function.lambda_function.qualified_arn
    include_body = false
  }]
}
