# labs.surfline.com Blog Infrastructure

Infra for [labs.surfline.com](https://labs.surfline.com)  website.

This repo sets up:
* Cloudfront
* S3 Bucket

DNS Routing and certs are setup manually.

Two envs are supported `staging` and `prod`.

## Basic Auth (Lambda@Edge)

For staging Lambda@Edge is used to provide basic auth in front of staging domain. The Lambda function source code is within this infra repo since it's real purpose is part of the infra.

## Naming Conventions

See https://wavetrak.atlassian.net/wiki/display/MGSVCS/AWS+Naming+Conventions.

## Terraform

_Uses terraform version `0.12.28+`._

Terraform projects are broken up into modules and environments. `modules/` defines infrastructure to be deployed into each environment. Each individual environment will then implement it's respective module. See https://www.terraform.io/docs/modules/usage.html for more information.

### Using Terraform

The following commands are used to develop and deploy infrastructure changes.

```bash
ENV=staging make plan
ENV=staging make apply
```

Valid environments are `staging`, and `prod`.

#### State Management

State is uploaded to S3 after `make apply` is run.
