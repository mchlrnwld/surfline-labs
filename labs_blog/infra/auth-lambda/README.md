# auth-lambda

# Development

SAM local is used to test the Lambda function locally it's not used for deployment.

```bash
# We use sam to test the Lambda function: install sam https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install-mac.html
brew tap aws/tap
brew install aws-sam-cli
```

To execute the Lambda function locally:

```bash
make invoke
```

# Deployment

```bash
make deploy
```