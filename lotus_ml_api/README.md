# LOTUS ML API

A light weight containerized API sitting on top of the LOTUS part files:

https://msw-lotus-validation-ui.s3.amazonaws.com/partition_data/index.html

The API transforms the data into a directionally binned representation that 
matches the input format used in the `ml_ratings` folder of this repo.

At present the API only works for HB Pier assuming an offshore direction of 205
degrees.

# Running the API

`make build`

Run a stand alone server on port 5000

`make server`

Run a shell and create a server instance there:

```
make shell
python app.py	
```

# Allowing network access from other containers

This docker container will sit on port 5000 and be available locally, to connect
to this container from another docker container you'll need to connect them
on a shared network.

```
docker network create <ml_net>
docker network connect <ml_net> <other_container>
docker network connect lotus-api
```

Once the networks are connected the API is accessible via:

`http://lotus-api:5000`

# TODO

- Create call method for all spots
- Create formatted version for UI display as well as ML