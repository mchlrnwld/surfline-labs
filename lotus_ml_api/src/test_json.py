import pandas as pd
import numpy as np

dat = pd.DataFrame({"test_col": np.random.rand(30),
                    "test_col2": np.random.rand(30)})

print(pd.read_json(dat.to_json()))