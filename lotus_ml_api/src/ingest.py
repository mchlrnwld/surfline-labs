import datetime
import time
import calendar
import pandas as pd
import numpy as np
import requests
import math


def run_ingest():
    out = ""
    shore_dir = 205
    """
    Read a standard wavewatch partitions file
    """

    r = requests.get("https://msw-lotus-validation-ui.s3.amazonaws.com/partition_data/0148_CAL_3m.txt")
    lines = r.text.split("\n")
    lines.pop(-1)

    header_line = lines.pop(0)

    j = 0
    surface = []
    idx = []

    dir_bins = np.arange(0, 370, 10)
    rows = []

    while True:
        line = lines[j].strip()
        split = line.split()

        # UTC timestamp as "YYYY-MM-DD HH:MM:SS"
        timestamp = datetime.datetime.strptime(line[0:13],
                                               '%Y%m%d %H:%M')

        timestamp = calendar.timegm(timestamp.utctimetuple())

        part_count = int(split[8])

        # Convert wind speed to MPH from KNOTS
        wind_str = float(split[6]) * 1.15078
        wind_dir = float(split[7])

        period = np.zeros([36])
        sigh = np.zeros([36])

        # Process partitions
        parts = []
        # Read partitions backwards so the largest and most energetic
        # overwrite those less so
        for k in np.arange(part_count, 0, -1):

            dat = lines[j + k].split()

            sig_height = float(dat[1])
            peak_period = float(dat[2])
            mean_direction = float(dat[3])
            dir_bin_num = np.digitize(get_relative_angle(mean_direction,
                                                         shore_dir),
                                      dir_bins) - 1
            # print(sig_height, "@", peak_period, mean_direction, 'deg')
            period[dir_bin_num] = peak_period
            sigh[dir_bin_num] = sig_height

        idx.append(pd.to_datetime(timestamp, unit='s'))

        wind_dir_norm = get_relative_angle(wind_dir, shore_dir)

        wind_on_comp = np.cos(np.radians(wind_dir_norm)) * wind_str

        wind = np.array([wind_dir_norm, wind_str, wind_on_comp])

        row = np.append(np.append(np.array(period),
                                  np.array(sigh)), wind)

        rows.append(row)

        j = j + part_count + 1

        if j >= len(lines):
            break

    data = np.stack(rows, axis=0)
    per_idx = ["per_" + str(i) for i in np.array(range(0, 36))]
    sig_idx = ["sig_" + str(i) for i in np.array(range(0, 36))]
    feature_columns = np.concatenate([per_idx, sig_idx, ['GFS_wind_dir_norm',
                                                         'GFS_wind_speed',
                                                         'GFS_onshore_comp']])

    dat = pd.DataFrame(data, columns=feature_columns, index=idx)

    return dat.to_json()


def get_relative_angle(angle, shore_dir):
    a = 360 - shore_dir
    b = (angle + a) % 360
    return b


if __name__ == '__main__':
    run_ingest()
