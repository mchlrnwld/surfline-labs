from flask import Flask, Response
from ingest import run_ingest

app = Flask(__name__)


class Forecast(object):

    def __init__(self, offshore_dir, lotus_path):
        self.offshore_dir = offshore_dir
        pass


@app.route('/')
def main():
    return Response(run_ingest(), content_type="text/json")


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
