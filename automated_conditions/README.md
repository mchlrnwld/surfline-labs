# ML Ratings/SurfHeights

Training and validation scripts for deep-learning based estimation of ratings and surf heights. 

# Running the project

`make build`

`make notebook`

# Workflow

All hyper-parameters for model training are stored in a config file such as 

`config.yaml`

# To-Do

- [X] Add lightweight CPU docker file
- [ ] Document config file
- [ ] Document docker files
- [ ] Full validation notebook example
