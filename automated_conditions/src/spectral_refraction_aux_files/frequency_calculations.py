import numpy as np  # type: ignore


def create_frequency_distribution(
    minimum_frequency: float, maximum_frequency: float, logfactor: float = 1.1
) -> np.array:
    """
    Create frequency distribution based on min/max frequency.

    Args:
        minimum_frequency: Lower frequency bound to create distribution.
        maximum_frequency: Upper frequency bound to create distribution.
    """
    number_of_frequencies = int(
        round(
            (np.log(maximum_frequency / minimum_frequency) / np.log(logfactor) + 1)
        )
    )
    frequency_growth_rate = (maximum_frequency / minimum_frequency) ** (
        1 / (number_of_frequencies - 1)
    )
    frequencies = np.array(
        [
            minimum_frequency * (frequency_growth_rate ** i)
            for i in range(number_of_frequencies)
        ]
    )
    return frequencies


def create_discrete_frequency_steps(frequencies: np.array) -> np.array:
    """
    Compute frequency step discretisation.

    Args:
        frequencies: Numpy array of frequencies to create
                     frequency step discretisation for.
    """
    discrete_frequency_steps = np.zeros(len(frequencies))
    for i in range(1, len(frequencies) - 1):
        discrete_frequency_steps[i] = 0.5 * (
            frequencies[i + 1] - frequencies[i - 1]
        )
    discrete_frequency_steps[0] = (
        discrete_frequency_steps[1]
        * discrete_frequency_steps[1]
        / discrete_frequency_steps[2]
    )
    discrete_frequency_steps[-1] = (
        discrete_frequency_steps[-2]
        * discrete_frequency_steps[-2]
        / discrete_frequency_steps[-3]
    )
    return discrete_frequency_steps
