import numpy as np
from scipy.special import gamma
import pandas as pd
import os
import datetime
from scipy.interpolate import RectBivariateSpline


# Specific to my machine's configuration
import warnings
with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    import xarray as xr


def write_ww3_2dspec(output_file_name, nb_spots, model_grid, step_time,
                     spec_data, lon_spot, lat_spot, wind_speed, wind_dir,
                     spot_number=1, depth=99999, fmin=0.035, fmax=0.5,
                     delta_dir=10):
    """
    Write spectral data to txt file, following WW3 formating

    Parameters
    ----------
    output_file_name : string with the full name of the resulting file
    nb_spots : number of spots in the file


    model_grid : name of the model grid
    step_time : datetime value for the data
    spec_data : spectral data to write to the file
    lon_spot : longitude coordinates for the target position
    lat_spot : latitude coordinates for the target position
    wind_speed : wind speed in m/s
    wind_dir : wind direction following the meteorological convention
    spot_number: number of the spot (default: 1)
    depth: depth in meters (default: 99999)
    fmin : Minimum frequency  (default: 0.035)
    fmin : Maximum frequency  (default: 0.5)
    delta_dir : Directional bin size in degrees (default: 10)
    """

    ndir = int(360/delta_dir)           # number of directions

    # create frequency distribution, logarithmic spacing with a factor of 1.1
    nfr = int(round((np.log(fmax/fmin)/np.log(1.1)+1)))
    xfr = (fmax/fmin) ** (1/(nfr-1))
    freq = []
    for k in range(0, nfr):
        freq.append(fmin*xfr ** k)
    freq = np.array(freq)
    dirs = np.arange(0, 360, delta_dir)

    # Convert from the north axis reference to the radian axis reference
    dirs = (270-dirs+180) % 360

    # Find the turning point to re-arrange the matrix
    diff_dirs = np.diff(dirs)
    alpha_dir_pos = np.nonzero(diff_dirs > 10)
    alpha_dir_pos = alpha_dir_pos[0][0] - 1

    dirs_rad = dirs*np.pi/180  # Convert to radians

    nb_spots = str(nb_spots)
    nb_spots = nb_spots.ljust(3, ' ')

    if not os.path.isfile(output_file_name):

        output_file = open(output_file_name, 'w')
        output_file.write("'WAVEWATCH III SPECTRA'     ")
        output_file.write('%.0f    ' % nfr)
        output_file.write('%.0f   ' % ndir)
        output_file.write('%s ' % nb_spots)
        output_file.write('%s' % "'" + model_grid + "'")
        output_file.write('\n')

        for k in range(len(freq)):
            if (k+1) % 8 == 0 or k == len(freq)-1:
                output_file.write('%.3E\n' % freq[k])
            else:
                output_file.write('%.3E ' % freq[k])

        for k in range(len(dirs_rad)):
            if (k+1) % 7 == 0 or k == len(dirs_rad)-1:
                output_file.write('%.3E\n' % dirs_rad[k])
            else:
                output_file.write('%.3E  ' % dirs_rad[k])

        output_file.close()

    # Readjust spec to match the ww3 orientation
    # Needs to be adjusted if Number of Frequencies and Directions changes
    spec_data_final = np.zeros([nfr, ndir])
    spec_data_final[:, 0:alpha_dir_pos] = spec_data[:, ndir-alpha_dir_pos:ndir]
    spec_data_final[:, alpha_dir_pos:ndir] = spec_data[:, 0:ndir-alpha_dir_pos]
    spec_data_final = np.fliplr(spec_data_final)

    output_file = open(output_file_name, 'a')
    output_file.write(step_time.strftime('%Y%m%d %H%M%S') + '\n')
    if spot_number < 10:
        output_file.write("'" + str(spot_number) + "         '  ")
    if spot_number >= 10 and spot_number < 100:
        output_file.write("'" + str(spot_number) + "        '  ")
    if spot_number >= 100:
        output_file.write("'" + str(spot_number) + "       '  ")
    output_file.write('%.2f ' % lat_spot)
    output_file.write('%.2f    ' % lon_spot)
    output_file.write('%.1f   ' % depth)
    output_file.write('%.2f ' % wind_speed)
    output_file.write('%.1f   ' % wind_dir)
    output_file.write('%.2f ' % 0)
    output_file.write('%.1f\n' % 270)
    line_counter = 1
    for dd in range(len(dirs)):
        energy_2d_slice = spec_data_final[:, dd]
        for k in range(len(energy_2d_slice)):
            if line_counter % 7 == 0:
                output_file.write('%.3E\n' % energy_2d_slice[k])
            else:
                output_file.write('%.3E  ' % energy_2d_slice[k])
            line_counter += 1
    output_file.write('\n')
    output_file.close()


def get_partitions_from_grid(input_file, lon_spot, lat_spot, npar=6):
    """
    Pull partition data from WW3 netcdf output file, for one single location
    Parameters
    ----------
    input_file : string with the full name of the file
    lon_spot : longitude coordinates for the target position
    lat_spot : latitude coordinates for the target position
    npar : number of partitions in the file including the wind sea
    partition (default: 6)
    """

    xarray_temp = xr.open_dataset(input_file)
    model_df = xarray_temp.to_dataframe()
    model_df = model_df.reset_index()

    lon_grid_vec_original = model_df["longitude"].unique()
    # ww3 netcdf grids are not evenly spaced so we use this to fix that
    lon_grid_vec = np.round(lon_grid_vec_original*1000)/1000

    lat_grid_vec_original = model_df["latitude"].unique()
    # ww3 netcdf grids are not evenly spaced so we use this to fix that
    lat_grid_vec = np.round(lat_grid_vec_original*1000)/1000

    # Pull date from file and convert to datetime
    step_time = model_df["time"].unique()
    step_time = step_time[0]
    step_time = datetime.datetime.utcfromtimestamp(step_time.tolist()/1e9)

    # transform longitude into 0-360 format
    if lon_spot < 0:
        lon_spot = 360+lon_spot

    # Check if the point coordinates are within the model grid
    error_flag = 0
    try:
        px = np.where(lon_grid_vec == lon_spot)[0][0]
        py = np.where(lat_grid_vec == lat_spot)[0][0]
    except:
        error_flag = 1

    hs_part = np.zeros(npar)
    tp_part = np.zeros(npar)
    pdir_part = np.zeros(npar)
    dspread_part = np.zeros(npar)

    wind_speed = 0
    wind_dir = 0
    overall_sigH = 0

    if error_flag == 0:

        cond_reg_1 = model_df["latitude"] == lat_grid_vec_original[py]
        cond_reg_2 = model_df["longitude"] == lon_grid_vec_original[px]
        base_subset_model = model_df[cond_reg_1 & cond_reg_2]

        wind_x = base_subset_model['uwnd'].values
        wind_y = base_subset_model['vwnd'].values

        # To convert to knots  multiply by (3600/1852)
        wind_speed = np.sqrt(wind_x**2 + wind_y**2)
        wind_dir = (270-(np.arctan2(wind_y, wind_x)*(180/np.pi))) % 360

        overall_sigH = base_subset_model['hs'].values

        for part in range(0, npar):

            hs_var_name = 'phs' + str(part)
            tp_var_name = 'ptp' + str(part)
            pdir_var_name = 'pdir' + str(part)
            dspread_var_name = 'pspr' + str(part)

            if not np.isnan(base_subset_model[hs_var_name].values):
                hs_part[part] = base_subset_model[hs_var_name]
                tp_part[part] = base_subset_model[tp_var_name]
                pdir_part[part] = base_subset_model[pdir_var_name]
                dspread_part[part] = base_subset_model[dspread_var_name]

    return overall_sigH, wind_speed, wind_dir, hs_part, tp_part, pdir_part, \
        dspread_part, step_time


def get_partitions_from_grid_multiple_spots(input_file, lon_spot_list, lat_spot_list, npar=6):
    """
    Pull partition data from WW3 netcdf output file, for one single location

    Parameters
    ----------
    input_file : string with the full name of the file
    lon_spot_list : longitude coordinates for the target positions
    lat_spot_list : latitude coordinates for the target positions

    npar : number of partitions in the file including the wind sea
    partition (default: 6)
    """
    nb_spots = len(lon_spot_list)

    xarray_temp = xr.open_dataset(input_file)
    model_df = xarray_temp.to_dataframe()
    model_df = model_df.reset_index()

    lon_grid_vec_original = model_df["longitude"].unique()
    # ww3 netcdf grids are not evenly spaced so we use this to fix that
    lon_grid_vec = np.round(lon_grid_vec_original*1000)/1000

    lat_grid_vec_original = model_df["latitude"].unique()
    # ww3 netcdf grids are not evenly spaced so we use this to fix that
    lat_grid_vec = np.round(lat_grid_vec_original*1000)/1000

    # Pull date from file and convert to datetime
    step_time = model_df["time"].unique()
    step_time = step_time[0]
    step_time = datetime.datetime.utcfromtimestamp(step_time.tolist()/1e9)

    hs_part = np.zeros([npar, nb_spots])
    tp_part = np.zeros([npar, nb_spots])
    pdir_part = np.zeros([npar, nb_spots])
    dspread_part = np.zeros([npar, nb_spots])

    wind_speed = np.zeros(nb_spots)
    wind_dir = np.zeros(nb_spots)
    overall_sigH = np.zeros(nb_spots)

    for k in range(nb_spots):
        lon_spot = lon_spot_list[k]
        lat_spot = lat_spot_list[k]

        # transform longitude into 0-360 format
        if lon_spot < 0:
            lon_spot = 360+lon_spot

        # Check if the point coordinates are within the model grid
        error_flag = 0
        try:
            px = np.where(lon_grid_vec == lon_spot)[0][0]
            py = np.where(lat_grid_vec == lat_spot)[0][0]
        except:
            error_flag = 1

        if error_flag == 0:

            cond_reg_1 = model_df["latitude"] == lat_grid_vec_original[py]
            cond_reg_2 = model_df["longitude"] == lon_grid_vec_original[px]
            base_subset_model = model_df[cond_reg_1 & cond_reg_2]

            wind_x = base_subset_model['uwnd'].values
            wind_y = base_subset_model['vwnd'].values

            # To convert to knots  multiply by (3600/1852)
            wind_speed[k] = np.sqrt(wind_x**2 + wind_y**2)
            wind_dir[k] = (270-(np.arctan2(wind_y, wind_x)*(180/np.pi))) % 360

            overall_sigH[k] = base_subset_model['hs'].values

            for part in range(0, npar):

                hs_var_name = 'phs' + str(part)
                tp_var_name = 'ptp' + str(part)
                pdir_var_name = 'pdir' + str(part)
                dspread_var_name = 'pspr' + str(part)

                if not np.isnan(base_subset_model[hs_var_name].values):
                    hs_part[part][k] = base_subset_model[hs_var_name]
                    tp_part[part][k] = base_subset_model[tp_var_name]
                    pdir_part[part][k] = base_subset_model[pdir_var_name]
                    dspread_part[part][k] = base_subset_model[dspread_var_name]

    xarray_temp.close()
    return overall_sigH, wind_speed, wind_dir, hs_part, tp_part, pdir_part, \
        dspread_part, step_time


def jonswap(f, Hs, Tp, gamma_constant=3.3, sigma_low=.07, sigma_high=.09,
            g=9.81, method='yamaguchi', normalize=True):
    """
    Generate JONSWAP spectrum

    Parameters
    ----------
    f : Array of frequencies
    Hs : Required zeroth order moment wave height
    Tp : Required peak wave period

    gamma_constant : JONSWAP peak-enhancement factor (default: 3.3)
    sigma_low : Sigma value for frequencies <= ``1/Tp`` (default: 0.07)
    sigma_high : Sigma value for frequencies > ``1/Tp`` (default: 0.09)
    g : Gravitational constant (default: 9.81)
    method : Method to compute alpha (default: yamaguchi)
    normalize : Normalize resulting spectrum to match ``Hs``
    """

    # Pierson-Moskowitz
    if method.lower() == 'yamaguchi':
        alpha = 1. / (.06533 * gamma_constant ** .8015 + .13467) / 16.
    elif method.lower() == 'goda':
        alpha = 1. / (.23 + .03 * gamma_constant - .185 /
                      (1.9 + gamma_constant)) / 16.
    else:
        raise ValueError('Unknown method: %s' % method)

    E_pm = alpha * Hs**2 * Tp**-4 * f**-5 * np.exp(-1.25 * (Tp * f)**-4)

    # JONSWAP
    sigma = np.ones(f.shape) * sigma_low
    sigma[f > 1./Tp] = sigma_high

    jonswap_spec = E_pm * \
        gamma_constant**np.exp(-0.5 * (Tp * f - 1)**2. / sigma**2.)

    if normalize:
        jonswap_spec *= Hs**2. / (16. * np.trapz(jonswap_spec, f, axis=0))

    return jonswap_spec


def create_2d_spectra(hs, tp, pdir, dspread, overall_sigH,
                      fmin=0.035, fmax=0.5, delta_dir=10):
    """
    Create a multi-modal spectrum based on the wave parameters Hs, Tp,
    PDir and DSpread

    Parameters
    ----------
    hs : Array of significant wave height for each partition [m]
    tp : Array of peak period for each partition [s]
    pdir : Array of peak direction for each partition [degrees]
    dspread : Array of directional spread for each partition [degrees]
    overall_sigH : Significant wave hieght for the entire spectra calculated
    by the model

    fmin : Minimum frequency  (default: 0.035)
    fmin : Maximum frequency  (default: 0.5)
    delta_dir : Directional bin size in degrees (default: 10)
    """

    ndir = 360/delta_dir           # number of directions

    # create frequency distribution, logarithmic spacing with a factor of 1.1
    nfr = int(round((np.log(fmax/fmin)/np.log(1.1)+1)))
    xfr = (fmax/fmin) ** (1/(nfr-1))
    freq = []
    for k in range(0, nfr):
        freq.append(fmin*xfr ** k)
    freq = np.array(freq)
    dirs = np.arange(0, 360, delta_dir)

    # Convert from the north axis reference to the radian axis reference
    dirs_rad = (270-dirs+180) % 360
    dirs_rad = dirs_rad*np.pi/180

    # Compute frequency step discretisation
    dfr = np.zeros(nfr)
    for k in range(1, nfr-1):
        dfr[k] = 0.5 * (freq[k+1] - freq[k-1])
    dfr[0] = dfr[1]*dfr[1]/dfr[2]
    dfr[-1] = dfr[-2]*dfr[-2]/dfr[-3]

    # Remove noisy partitions
    empty_parts = np.where(hs < 0.05)
    hs = np.delete(hs, empty_parts)
    tp = np.delete(tp, empty_parts)
    pdir = np.delete(pdir, empty_parts)
    dspread = np.delete(dspread, empty_parts)

    empty_parts = np.where(tp < 0.5)
    hs = np.delete(hs, empty_parts)
    tp = np.delete(tp, empty_parts)
    pdir = np.delete(pdir, empty_parts)
    dspread = np.delete(dspread, empty_parts)

    npar = len(hs)
    spec_data = np.zeros((int(nfr), int(ndir)))

    for part in range(0, npar):
        # Create JONSWAP spectrum
        jonswap_spec = jonswap(freq, hs[part], tp[part])

        # limit the minimum spread value to 8º to maintain stability
        # spread_value = max(8, dspread[part])
        # sigma = max(0, spread_value*np.pi/180)
        # todo: Creates division by zero in some circumstances
        sigma = max(8*np.pi/180, dspread[part]*np.pi/180)
        sigma_stage2 = 2/sigma**2

        if sigma_stage2 > 3 or dspread[part] < 10:
            ms = int(np.ceil(sigma_stage2-3))
            js = list(range(0, ms))
            ratios = np.zeros(ms)
            for jsx in js:
                ratios[jsx] = (sigma_stage2-jsx)/(sigma_stage2-jsx-0.5)
            R = np.prod(ratios)*gamma(sigma_stage2-ms+1) / \
                gamma(sigma_stage2-ms+0.5)
        else:
            R = gamma(sigma_stage2+1)/gamma(sigma_stage2+0.5)

        gamma_ratio = R/(2*np.sqrt(np.pi))

        alpha_dir = 180-abs(180-abs(dirs-pdir[part]))
        for k in range(0, nfr):
            ee = gamma_ratio * \
                np.cos(np.radians(alpha_dir)/2)**(2*sigma_stage2)
            spec_data[k, :] = spec_data[k, :] + jonswap_spec[k] * ee

    spec_data_2d_sigH = 0
    for i in range(0, int(nfr)):
        for j in range(0, int(ndir)):
            spec_data_2d_sigH = spec_data_2d_sigH + \
                (spec_data[i, j]*dfr[i] * delta_dir*np.pi/180)

    synthetic_sigH = 4 * np.sqrt(spec_data_2d_sigH)

    if abs(synthetic_sigH-overall_sigH) > 0.5:

        print('##############################################################')
        print('Problem with Spectra')
        print('##############################################################')
        print(synthetic_sigH, overall_sigH)

    if synthetic_sigH == 0:
        print('##############################################################')
        print('Spectra is empty')
        print('##############################################################')

    return spec_data, synthetic_sigH


def create2Dspectra_LOLA_format(hs, tp, pdir, dspread, overall_sigH,
                                output_file_name, fmin=0.0418, fmax=0.411,
                                delta_dir=15):
    """
    Create a multi-modal spectrum based on the wave parameters Hs, Tp, PDir
    and DSpread but with LOLA direction and frequency list

    Parameters
    ----------
    hs : Array of significant wave height for each partition [m]
    tp : Array of peak period for each partition [s]
    pdir : Array of peak direction for each partition [degrees]
    dspread : Array of directional spread for each partition [degrees]
    overall_sigH : Significant wave hieght for the entire spectra calculated
    by the model output_file_name: Name of the output file

    fmin : Minimum frequency  (default: 0.0418)
    fmin : Maximum frequency  (default: 0.411)
    delta_dir : Directional bin size in degrees (default: 15)
    """

    ndir = 360/delta_dir           # number of directions

    # create frequency distribution, logarithmic spacing with a factor of 1.1
    nfr = int(round((np.log(fmax/fmin)/np.log(1.1)+1)))
    xfr = (fmax/fmin) ** (1/(nfr-1))
    freq = []
    for k in range(0, nfr):
        freq.append(fmin*xfr ** k)
    freq = np.array(freq)
    dirs = np.arange(delta_dir, 360+delta_dir, delta_dir)

    # Convert from the north axis reference to the radian axis reference
    dirs_rad = (270-dirs+180) % 360
    dirs_rad = dirs_rad*np.pi/180

    # Compute frequency step discretisation
    dfr = np.zeros(nfr)
    for k in range(1, nfr-1):
        dfr[k] = 0.5 * (freq[k+1] - freq[k-1])
    dfr[0] = dfr[1]*dfr[1]/dfr[2]
    dfr[-1] = dfr[-2]*dfr[-2]/dfr[-3]

    # Remove noisy partitions
    empty_parts = np.where(hs < 0.05)
    hs = np.delete(hs, empty_parts)
    tp = np.delete(tp, empty_parts)
    pdir = np.delete(pdir, empty_parts)
    dspread = np.delete(dspread, empty_parts)

    empty_parts = np.where(tp < 0.5)
    hs = np.delete(hs, empty_parts)
    tp = np.delete(tp, empty_parts)
    pdir = np.delete(pdir, empty_parts)
    dspread = np.delete(dspread, empty_parts)

    npar = len(hs)
    spec_data = np.zeros((int(nfr), int(ndir)))

    for part in range(0, npar):
        # Create JONSWAP spectrum
        jonswap_spec = jonswap(freq, hs[part], tp[part])

        # limit the minimum spread value to 8º to maintain stability
        # spread_value = max(8, dspread[part])
        # sigma = max(0, spread_value*np.pi/180)
        # todo: Creates division by zero in some circumstances
        sigma = max(8*np.pi/180, dspread[part]*np.pi/180)
        sigma_stage2 = 2/sigma**2

        if sigma_stage2 > 3 or dspread[part] < 10:
            ms = int(np.ceil(sigma_stage2-3))
            js = list(range(0, ms))
            ratios = np.zeros(ms)
            for jsx in js:
                ratios[jsx] = (sigma_stage2-jsx)/(sigma_stage2-jsx-0.5)
            R = np.prod(ratios)*gamma(sigma_stage2-ms+1) / \
                gamma(sigma_stage2-ms+0.5)
        else:
            R = gamma(sigma_stage2+1)/gamma(sigma_stage2+0.5)

        gamma_ratio = R/(2*np.sqrt(np.pi))

        alpha_dir = 180-abs(180-abs(dirs-pdir[part]))
        for k in range(0, nfr):
            ee = gamma_ratio * \
                np.cos(np.radians(alpha_dir)/2)**(2*sigma_stage2)
            spec_data[k, :] = spec_data[k, :] + jonswap_spec[k] * ee

    spec_data_2d_sigH = 0
    for i in range(0, int(nfr)):
        for j in range(0, int(ndir)):
            spec_data_2d_sigH = spec_data_2d_sigH + \
                (spec_data[i, j]*dfr[i] * delta_dir*np.pi/180)

    synthetic_sigH = 4 * np.sqrt(spec_data_2d_sigH)

    if abs(synthetic_sigH-overall_sigH) > 0.5:

        print('##############################################################')
        print('Problem with Spectra')
        print('##############################################################')
        print(synthetic_sigH, overall_sigH)

    if synthetic_sigH == 0:
        print('##############################################################')
        print('Spectra is empty')
        print('##############################################################')
    else:

        output_file = open(output_file_name, 'w')
        for i in range(0, int(nfr)):
            for j in range(0, int(ndir)):
                output_file.write('%s' % str(i+1).rjust(12))
                output_file.write('%s' % str(j+1).rjust(12))
                output_file.write('  %.9f' % freq[i])
                output_file.write('       %.8E' % dfr[i])
                if dirs[j] < 100:
                    output_file.write('   %.7f' % dirs[j])
                else:
                    output_file.write('   %.6f' % dirs[j])
                output_file.write('      %.8f' % (delta_dir*np.pi/180))
                output_file.write('        %.8E\n' % spec_data[i, j])

        output_file.close()

    return spec_data, synthetic_sigH


def transform_2Dspectra_to_LOLA_format(input_file_name, output_file_name,
                                       target_date, fmin=0.0418, fmax=0.411,
                                       delta_dir=15):
    """
    Read a netcdf file with Lotus spectral data and transform it to LOLA
    direction and frequency list

    Parameters
    ----------
    input_file_name : Name of the netcdf input file
    output_file_name: Name of the output file

    fmin : Minimum frequency  (default: 0.0418)
    fmin : Maximum frequency  (default: 0.411)
    delta_dir : Directional bin size in degrees (default: 15)
    """

    ndir = 360/delta_dir           # number of directions

    # create frequency distribution, logarithmic spacing with a factor of 1.1
    nfr = int(round((np.log(fmax/fmin)/np.log(1.1)+1)))
    xfr = (fmax/fmin) ** (1/(nfr-1))
    freq = []
    for k in range(0, nfr):
        freq.append(fmin*xfr ** k)
    freq = np.array(freq)
    dirs = np.arange(delta_dir, 360+delta_dir, delta_dir)

    # Compute frequency step discretisation
    dfr = np.zeros(nfr)
    for k in range(1, nfr-1):
        dfr[k] = 0.5 * (freq[k+1] - freq[k-1])
    dfr[0] = dfr[1]*dfr[1]/dfr[2]
    dfr[-1] = dfr[-2]*dfr[-2]/dfr[-3]

    model_df = xr.open_dataset(input_file_name)

    base_subset_model = model_df.sel(
        time=target_date.strftime("%Y-%m-%dT%H:00:00"))
    lotus_freq_list = base_subset_model['frequency'].values
    lotus_dir_list = base_subset_model['directions'].values
    lotus_spectra = base_subset_model['spectral_wave_density_2D'].values

    spec_1 = lotus_spectra[:, 0]
    spec_2 = lotus_spectra[:, 1:]

    # Readjust the direction and spectra arrays to better fit the LOLA
    # configuration
    lotus_dir_list = np.concatenate([lotus_dir_list[1:], np.array([360])])
    lotus_spectra = np.concatenate((spec_2, spec_1[:, None]), axis=1)

    interp_spline = RectBivariateSpline(
        lotus_freq_list, lotus_dir_list, lotus_spectra)

    lola_spectra = interp_spline(freq, dirs)

    # Remove any negative values artifacts created by the interpolation
    lola_spectra[lola_spectra < 0] = 0

    output_file = open(output_file_name, 'w')
    for i in range(0, int(nfr)):
        for j in range(0, int(ndir)):
            output_file.write('%s' % str(i+1).rjust(12))
            output_file.write('%s' % str(j+1).rjust(12))
            output_file.write('  %.9f' % freq[i])
            output_file.write('       %.8E' % dfr[i])
            if dirs[j] < 100:
                output_file.write('   %.7f' % dirs[j])
            else:
                output_file.write('   %.6f' % dirs[j])
            output_file.write('      %.8f' % (delta_dir*np.pi/180))
            output_file.write('        %.8E\n' % lola_spectra[i, j])

    output_file.close()


def main():

    # Examples
    #

    ##########################################################################
    # Option 1 - needs input file
    # Pull data from a Grid file and create the spectra from the partition
    #########################################################################

    input_file = 'ww3_lotus_SCAL_1m.2020020500.nc'
    lon_spot = 240
    lat_spot = 33.5

    ###########################################################################
    # Option 2
    # insert partition data directly and get spectra
    ###########################################################################

    hs_part = np.asarray([1.77, 0.76, 0.27])
    tp_part = np.asarray([10.95, 8.32, 10.62])
    pdir_part = np.asarray([182, 106, 166])
    dspread_part = np.asarray([14, 12, 6])
    overall_sigH = 1.95

    [spec_data, synthetic_sigH] = create_2d_spectra(
        hs_part, tp_part, pdir_part, dspread_part, overall_sigH)

    ##########################################################################
    # Write Spectra to text file with WW3 format
    ##########################################################################

    output_file = 'teste.spec2d'
    nb_spots = 1
    model_grid = 'GLOB_30m'
    wind_speed = 0
    wind_dir = 90
    step_time = datetime.datetime.now()

    write_ww3_2dspec(output_file, nb_spots, model_grid, step_time,
                     spec_data, lon_spot, lat_spot, wind_speed, wind_dir)

    ##########################################################################
    # Create a file for the Spectral Refraction with LOLA format based on
    # partition data
    ##########################################################################

    output_file = 'LOLA_spec.dat'
    [spec_data_LOLA, synthetic_sigH] = create2Dspectra_LOLA_format(
        hs_part, tp_part, pdir_part, dspread_part, overall_sigH, output_file)

    ##########################################################################
    # Create a file for the Spectral Refraction with LOLA format based on a
    # Lotus Spectral netcdf file
    ##########################################################################

    target_date = datetime.datetime.strptime('2020022000', '%Y%m%d%H')
    input_file = 'ww3_lotus_GLOB_30m.spot_000.000_050.500.nc'
    output_file = 'lola_output.txt'
    transform_2Dspectra_to_LOLA_format(input_file, output_file, target_date)


if __name__ == '__main__':
    main()
