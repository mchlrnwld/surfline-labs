import json
import math
import multiprocessing
import os
import time as tm
from datetime import datetime
from functools import partial
from glob import glob
from typing import Union

import numpy as np
import pandas as pd
from flatten_json import flatten

multiprocessing.freeze_support()


def add_24hforecast(datadf: pd.DataFrame, forecastdf: pd.DataFrame) -> pd.DataFrame:
    df = datadf.copy()

    forecast_24h = []
    forecastupdatetime = []
    forecast_surf_min = []
    forecast_surf_max = []
    forecast_surf_occ = []
    for time, subregion in zip(df["Datetime"], df["subregion"]):

        time_datetime = datetime.fromtimestamp(time)
        am = time_datetime.hour < 12
        dt = datetime.strptime(str(time_datetime.year) + "-" + str(
            time_datetime.month) + "-" + str(time_datetime.day) + "-12", '%Y-%m-%d-%H')
        dt_unix = tm.mktime(dt.timetuple())

        forecastdf_filt = forecastdf.copy(
        ).loc[forecastdf["subregion_id"] == subregion]
        forecastdf_filt = forecastdf_filt.reset_index(drop=True)

        if len(forecastdf_filt) > 0:

            matching_index = [
                forecastdf_filt["valid_time"].sub(dt_unix).abs().idxmin()]

            forecastupdatetime.append(
                forecastdf_filt.iloc[matching_index]["update_time"].item())

            if abs(dt_unix - forecastdf_filt.iloc[matching_index]["valid_time"].item()) < 43200:

                if am:
                    forecast_24h.append(
                        forecastdf_filt.iloc[matching_index]["am_surf_condition"].item())
                    forecast_surf_min.append(
                        forecastdf_filt.iloc[matching_index]["am_surf_minimum_ft"].item() / 3.2808)
                    forecast_surf_max.append(
                        forecastdf_filt.iloc[matching_index]["am_surf_maximum_ft"].item() / 3.2808)
                    forecast_surf_occ.append(
                        forecastdf_filt.iloc[matching_index]["am_surf_occasional_ft"].item() / 3.2808)
                else:
                    forecast_24h.append(
                        forecastdf_filt.iloc[matching_index]["pm_surf_condition"].item())
                    forecast_surf_min.append(
                        forecastdf_filt.iloc[matching_index]["pm_surf_minimum_ft"].item() / 3.2808)
                    forecast_surf_max.append(
                        forecastdf_filt.iloc[matching_index]["pm_surf_maximum_ft"].item() / 3.2808)
                    forecast_surf_occ.append(
                        forecastdf_filt.iloc[matching_index]["pm_surf_occasional_ft"].item() / 3.2808)
            else:
                forecast_24h.append(None)
                forecast_surf_min.append(None)
                forecast_surf_max.append(None)
                forecast_surf_occ.append(None)

        else:
            forecastupdatetime.append(None)
            forecast_24h.append(None)
            forecast_surf_min.append(None)
            forecast_surf_max.append(None)
            forecast_surf_occ.append(None)

    df["forecast_24h"] = forecast_24h
    df["forecast_24h_updatetime"] = forecastupdatetime
    df["forecast_24h_surf_min_mt"] = forecast_surf_min
    df["forecast_24h_surf_max_mt"] = forecast_surf_max
    df["forecast_24h_surf_occ_mt"] = forecast_surf_occ

    return df


def add_region_from_subregion(datadf: pd.DataFrame, regiondf: pd.DataFrame) -> pd.DataFrame:
    """
    Merges a reports dataframe with a forecast dataframe by finding the report closest to each forecast
    :param datadf: pandas dataframe containing subregions
    :param regiondf: pandas dataframe containing subregions and corresponding regions
    :returns: new dataframe with all subregions and regions
    """
    df = datadf.copy()

    regions = []
    for sub in df["subregion"]:
        match = regiondf.loc[regiondf["_id"] == sub]["region"].to_numpy()
        if match:
            regions.append(match[0])
        else:
            regions.append(None)

    df["region"] = regions

    return df


def merge_reports_into_forecasts(reportsdf: pd.DataFrame, forecastdf: pd.DataFrame) -> pd.DataFrame:
    """
    Merges a reports dataframe with a forecast dataframe by finding the report closest to each forecast
    :param reportsdf: pandas dataframe containing reports
    :param forecastdf: pandas dataframe containing the forecast values
    :returns: new dataframe with all forecast and corresponding reports data
    """
    forecast_w_reports = forecastdf.copy()

    matching_report_index = []
    for forecasttime in forecast_w_reports["Datetime"]:
        matching_report_index.append(
            ((reportsdf["valid_time"].sub(forecasttime)).abs()).idxmin())

    surf_minimum_ft = [reportsdf.iloc[x]["surf_minimum_ft"]
                       for x in matching_report_index]
    surf_maximum_ft = [reportsdf.iloc[x]["surf_maximum_ft"]
                       for x in matching_report_index]
    surf_occasional_ft = [reportsdf.iloc[x]["surf_occasional_ft"]
                          for x in matching_report_index]
    surf_condition = [reportsdf.iloc[x]["surf_condition"]
                      for x in matching_report_index]
    surf_reporter = [reportsdf.iloc[x]["surf_reporter"]
                     for x in matching_report_index]

    reporttimes = [reportsdf.iloc[x]["valid_time"]
                   for x in matching_report_index]
    timediffs = reporttimes - forecast_w_reports["Datetime"].values

    forecast_w_reports["surf_minimum_ft"] = surf_minimum_ft
    forecast_w_reports["surf_maximum_ft"] = surf_maximum_ft
    forecast_w_reports["surf_occasional_ft"] = surf_occasional_ft
    forecast_w_reports["surf_condition"] = surf_condition
    forecast_w_reports["surf_reporter"] = surf_reporter
    forecast_w_reports["report_time"] = reporttimes
    forecast_w_reports["report_time_diff"] = timediffs

    forecast_w_reports.loc[forecast_w_reports["report_time_diff"].abs() > (0.51 * 3600),
                           ["surf_minimum_ft", "surf_maximum_ft", "surf_occasional_ft", "surf_condition",
                            "surf_reporter"]] = None

    return forecast_w_reports


def merge_winddata_into_forecasts(winddf: pd.DataFrame, forecastdf: pd.DataFrame) -> pd.DataFrame:
    """
    Merges a reports dataframe with a forecast dataframe by finding the report closest to each forecast
    :param winddf: pandas dataframe containing nam wind data
    :param forecastdf: pandas dataframe containing the forecast values
    :returns: new dataframe with all forecast and corresponding reports data
    """
    forecast_w_wind = forecastdf.copy()

    features = ["WindSpeedLand_ms",
                "WindDirLand_deg",
                "WindGustLand_ms",
                "WindSpeedSea_ms",
                "WindDirSea_deg",
                "WindGustSea_ms",
                "TempSurfaceLand_c",
                "Temp2mtLand_c",
                "MSLPressureLand_hPa",
                "TempSurfaceSea_c",
                "Temp2mtSea_c",
                "MSLPressureSea_hPa"]

    matching_wind_index = []
    for forecasttime in forecast_w_wind["Datetime"]:
        matching_wind_index.append(
            ((winddf["Datetime"].sub(forecasttime)).abs()).idxmin())

    for f in features:
        vals = [winddf.iloc[x][f] for x in matching_wind_index]
        forecast_w_wind[f] = vals

    reporttimes = [winddf.iloc[x]["Datetime"]
                   for x in matching_wind_index]
    timediffs = reporttimes - forecast_w_wind["Datetime"].values
    forecast_w_wind["WindDateTime"] = reporttimes
    forecast_w_wind["WindTimeDiff"] = timediffs

    forecast_w_wind.loc[forecast_w_wind["WindTimeDiff"].abs() > (0.51 * 3600),
                        features] = None

    return forecast_w_wind


def get_rel_angles(angles):
    angles = angles % 360
    angles = np.where(angles > 180, angles - 360, angles)
    return angles


def merge_spotinfo_into_data(data: pd.DataFrame, spotinfo: dict) -> pd.DataFrame:
    datadf = data.copy()

    spotinfo_flattened = flatten(spotinfo, ".")

    datadf["SpotName"] = spotinfo_flattened.get("name", np.nan)
    datadf["subregion"] = spotinfo_flattened.get("subregion.$oid", np.nan)
    # datadf["spot_offshoredirection_deg"] = spotinfo.get("offshoreDirection", np.nan)

    # bestwinddirectionmin = spotinfo_flattened.get("best.windDirection.0.min", np.nan)
    # bestwinddirectionmax = spotinfo_flattened.get("best.windDirection.0.max", np.nan)
    # bestswellwindow0min = spotinfo_flattened.get("best.swellWindow.0.min", np.nan)
    # bestswellwindow0max = spotinfo_flattened.get("best.swellWindow.0.max", np.nan)
    # bestswellwindow1min = spotinfo_flattened.get("best.swellWindow.1.min", np.nan)
    # bestswellwindow1max = spotinfo_flattened.get("best.swellWindow.1.max", np.nan)
    # bestswellwindow2min = spotinfo_flattened.get("best.swellWindow.2.min", np.nan)
    # bestswellwindow2max = spotinfo_flattened.get("best.swellWindow.2.max", np.nan)
    # bestsizemin = spotinfo_flattened.get("best.sizeRange.0.min", np.nan)
    # bestsizemax = spotinfo_flattened.get("best.sizeRange.0.max", np.nan)
    # bestswellperiodmin = spotinfo_flattened.get("best.swellPeriod.0.min", np.nan)
    # bestswellperiodmax = spotinfo_flattened.get("best.swellPeriod.0.max", np.nan)

    # datadf["has_bestwinddirection"] = bestwinddirectionmin != np.nan
    # datadf["has_bestswellwindow0"] = bestswellwindow0min != np.nan
    # datadf["has_bestswellwindow1"] = bestswellwindow1min != np.nan
    # datadf["has_bestswellwindow2"] = bestswellwindow2min != np.nan
    # datadf["has_bestsize"] = bestsizemin != np.nan
    # datadf["has_bestswellperiod"] = bestswellperiodmin != np.nan

    # datadf["spot_bestwinddirection_max_deg"] = bestwinddirectionmin
    # datadf["spot_bestwinddirection_min_deg"] = bestwinddirectionmax
    # datadf["spot_bestswellwindow0_min_deg"] = bestswellwindow0min
    # datadf["spot_bestswellwindow0_max_deg"] = bestswellwindow0max
    # datadf["spot_bestswellwindow1_min_deg"] = bestswellwindow1min
    # datadf["spot_bestswellwindow1_max_deg"] = bestswellwindow1max
    # datadf["spot_bestswellwindow2_min_deg"] = bestswellwindow2min
    # datadf["spot_bestswellwindow2_max_deg"] = bestswellwindow2max
    # datadf["spot_bestsize_min"] = bestsizemin / 3.2808
    # datadf["spot_bestsize_max"] = bestsizemax / 3.2808
    # datadf["spot_bestswellperiod_min"] = bestswellperiodmin
    # datadf["spot_bestswellperiod_max"] = bestswellperiodmax

    key = "spot_offshoredirection_deg"
    datadf[key + "_sin"] = np.sin(2 * math.pi * datadf[key].astype(float) / 360)
    datadf[key + "_cos"] = np.cos(2 * math.pi * datadf[key].astype(float) / 360)

    key = "LotusWindDir_deg"
    datadf[key + "_sin"] = np.sin(2 * math.pi *
                                  (datadf[key].astype(float) + 180) / 360)
    datadf[key + "_cos"] = np.cos(2 * math.pi *
                                  (datadf[key].astype(float) + 180) / 360)
    datadf[key + "_rel"] = get_rel_angles(datadf[key] -
                                          datadf["spot_offshoredirection_deg"])
    datadf[key + "_rel_sin"] = np.sin(2 * math.pi *
                                      (datadf[key + "_rel"].astype(float) + 180) / 360)
    datadf[key + "_rel_cos"] = np.cos(2 * math.pi *
                                      (datadf[key + "_rel"].astype(float) + 180) / 360)

    for i in range(6):
        datadf.loc[(datadf["LotusPDirPart" + str(i) + "_deg"] == 0) &
                   (datadf["LotusSigHPart" + str(i) + "_mt"] == 0) &
                   (datadf["LotusTPPart" + str(i) + "_sec"] == 0) &
                   (datadf["LotusSpreadPart" + str(i) + "_deg"] == 0),
                   [
                       "LotusPDirPart" + str(i) + "_deg",
                       "LotusTPPart" + str(i) + "_sec",
                       "LotusSpreadPart" + str(i) + "_deg"
                   ]] = np.nan

    # keys = ["spot_bestwinddirection_max_deg", "spot_bestwinddirection_min_deg",
    #         "spot_bestswellwindow0_min_deg", "spot_bestswellwindow0_max_deg",
    #         "spot_bestswellwindow1_min_deg", "spot_bestswellwindow1_max_deg",
    #         "spot_bestswellwindow2_min_deg", "spot_bestswellwindow2_max_deg",
    keys = ["LotusPDirPart0_deg",
            "LotusPDirPart1_deg",
            "LotusPDirPart2_deg",
            "LotusPDirPart3_deg",
            "LotusPDirPart4_deg",
            "LotusPDirPart5_deg"
            ]

    for key in keys:
        rel = get_rel_angles(
            datadf[key] - datadf["spot_offshoredirection_deg"])
        datadf[key + "_rel"] = rel
        datadf[key + "_rel_sin"] = np.sin(2 *
                                          math.pi * (rel.astype(float) + 180) / 360)
        datadf[key + "_rel_cos"] = np.cos(2 *
                                          math.pi * (rel.astype(float) + 180) / 360)

    # datadf["spot_bestswellwindow1_min_deg_rel_cos_rep"] = np.array(datadf["spot_bestswellwindow1_min_deg_rel_cos"])
    # datadf["spot_bestswellwindow1_min_deg_rel_sin_rep"] = np.array(datadf["spot_bestswellwindow1_min_deg_rel_sin"])
    # datadf["spot_bestswellwindow2_min_deg_rel_cos_rep"] = np.array(datadf["spot_bestswellwindow2_min_deg_rel_cos"])
    # datadf["spot_bestswellwindow2_min_deg_rel_sin_rep"] = np.array(datadf["spot_bestswellwindow2_min_deg_rel_sin"])

    # datadf["spot_bestswellwindow1_max_deg_rel_cos_rep"] = np.array(datadf["spot_bestswellwindow1_max_deg_rel_cos"])
    # datadf["spot_bestswellwindow1_max_deg_rel_sin_rep"] = np.array(datadf["spot_bestswellwindow1_max_deg_rel_sin"])
    # datadf["spot_bestswellwindow2_max_deg_rel_cos_rep"] = np.array(datadf["spot_bestswellwindow2_max_deg_rel_cos"])
    # datadf["spot_bestswellwindow2_max_deg_rel_sin_rep"] = np.array(datadf["spot_bestswellwindow2_max_deg_rel_sin"])

    # datadf["spot_bestswellwindow1_min_deg_rel_cos_rep"].fillna(datadf["spot_bestswellwindow0_min_deg_rel_cos"], inplace=True)
    # datadf["spot_bestswellwindow1_min_deg_rel_sin_rep"].fillna(datadf["spot_bestswellwindow0_min_deg_rel_sin"], inplace=True)
    # datadf["spot_bestswellwindow2_min_deg_rel_cos_rep"].fillna(datadf["spot_bestswellwindow0_min_deg_rel_cos"], inplace=True)
    # datadf["spot_bestswellwindow2_min_deg_rel_sin_rep"].fillna(datadf["spot_bestswellwindow0_min_deg_rel_sin"], inplace=True)

    # datadf["spot_bestswellwindow1_max_deg_rel_cos_rep"].fillna(datadf["spot_bestswellwindow0_max_deg_rel_cos"], inplace=True)
    # datadf["spot_bestswellwindow1_max_deg_rel_sin_rep"].fillna(datadf["spot_bestswellwindow0_max_deg_rel_sin"], inplace=True)
    # datadf["spot_bestswellwindow2_max_deg_rel_cos_rep"].fillna(datadf["spot_bestswellwindow0_max_deg_rel_cos"], inplace=True)
    # datadf["spot_bestswellwindow2_max_deg_rel_sin_rep"].fillna(datadf["spot_bestswellwindow0_max_deg_rel_sin"], inplace=True)

    return datadf


def get_historical_tide(datadf: pd.DataFrame, tidesdict: dict) -> pd.DataFrame:
    resp = tidesdict

    if resp.get("port") is None:

        with open('missing_tides.csv', 'a') as file:
            line = datadf["SpotName"].unique()[0] + "," + datadf["SpotId"].unique()[0] + "," + str(
                datadf["SpotLat"].unique()[0]) + "," + str(datadf["SpotLon"].unique()[0]) + "," + str(len(datadf))
            file.write(line)
            file.write('\n')

        datadf["port_distance"] = None
        datadf["port_distanceunit"] = None
        datadf["port_minheight"] = None
        datadf["port_maxheight"] = None
        datadf["tide_level"] = None
        datadf["tide_time"] = None
    else:
        port_distance = resp["port"]["distance"]
        port_distanceunit = resp["port"]["distance_unit"]
        port_maxheight = resp["port"]["maxHeight"]
        port_minheight = resp["port"]["minHeight"]

        datadf["port_distance"] = port_distance
        datadf["port_distanceunit"] = port_distanceunit
        datadf["port_minheight"] = port_minheight
        datadf["port_maxheight"] = port_maxheight

        if not resp["levels"]:
            datadf["tide_level"] = None
            datadf["tide_time"] = None
        else:

            levels = [level["shift"] for level in resp["levels"]]
            leveltimes = [level["time"] for level in resp["levels"]]

            tide_levels = []
            tide_times = []
            for reporttime in datadf["Datetime"]:
                timediffs = abs(reporttime - np.array(leveltimes))
                closesttidetime = np.argmin(timediffs)
                if timediffs[closesttidetime] < 3600:
                    tide_levels.append(levels[closesttidetime])
                    tide_times.append(leveltimes[closesttidetime])
                else:
                    tide_levels.append(None)
                    tide_times.append(None)

            datadf["tide_level"] = tide_levels
            datadf["tide_time"] = tide_times

    return datadf


def merge_csvs(csvs: Union[list, tuple]) -> pd.DataFrame:
    """
    Takes a list of csvs, loads them into pandas datframes and merges them into one big dataframe
    :param csvs: list or tuple of filepaths to csv files
    :returns: pandas datafarme containing all merged data
    """
    df = pd.DataFrame()

    for csvi in csvs:
        dfi = pd.read_csv(csvi)
        df = pd.concat([df, dfi], ignore_index=True)
    return df


def generate_data(spot, input):
    report = home + "/ratings/human_reports/" + spot + ".csv"
    lotus = home + "/ratings/BWH_files/lotus_bwh_" + spot + ".csv"
    spotmeta = home + "spot_database/individual_spots/" + spot + ".json"
    wind = home + "/ratings/NAM_hindcast/NAM_hindcast_" + spot + ".csv"
    tidesfolder = home + "/ratings/tides/"
    sds_data_filt = sds_data.loc[sds_data["SpotId"] == spot]

    outfile = home + "/ratings/merged_data/" + spot + ".csv"

    with open(spotmeta) as f:
        spotinfo = json.load(f)

    spotinfo_flattened = flatten(spotinfo, ".")
    subregion = spotinfo_flattened.get("subregion.$oid", np.nan)

    merged = pd.read_csv(lotus)

    if os.path.isfile(report):
        reportsdf = pd.read_csv(report)
        merged = merge_reports_into_forecasts(reportsdf, merged)
        # merged = merged.loc[merged["report_time_diff"].abs() < (0.51*3600)]
        # merged.dropna(subset=["surf_condition"])
        merged["surf_condition_num"] = [lookup_ratings[cat]
                                        for cat in merged["surf_condition"]]
        merged['surf_condition_num_5pt'] = [reduced_class_map[cat]
                                            for cat in merged["surf_condition"]]
    else:
        merged["surf_minimum_ft"] = None
        merged["surf_maximum_ft"] = None
        merged["surf_occasional_ft"] = None
        merged["surf_condition"] = None
        merged["surf_condition_num"] = None
        merged["surf_condition_num_5pt"] = None
        merged["surf_reporter"] = None
        merged["report_time"] = None
        merged["report_time_diff"] = None

    # merged.dropna(subset=["surf_condition_num"], inplace=True)

    if len(merged) > 1:

        merged["spot_offshoredirection_deg"] = np.array(
            sds_data_filt["offshoreDirection"])[0]

        merged = merge_spotinfo_into_data(merged, spotinfo)
        merged = add_region_from_subregion(merged, subregiondf)

        forecast24h = home + "/ratings/24hforecasts/" + subregion + ".csv"
        if os.path.isfile(forecast24h):
            forecast24hdf = pd.read_csv(forecast24h)
            merged = add_24hforecast(merged, forecast24hdf)
        else:
            merged["forecast_24h"] = None
            merged["forecast_24h_updatetime"] = None
            merged["forecast_24h_surf_min_mt"] = None
            merged["forecast_24h_surf_max_mt"] = None
            merged["forecast_24h_surf_occ_mt"] = None

        merged["forecast_24h_num"] = [lookup_ratings[cat]
                                      for cat in merged["forecast_24h"]]
        merged["forecast_24h_num_5pt"] = [reduced_class_map[cat]
                                          for cat in merged["forecast_24h"]]

        if os.path.isfile(wind):
            winddf = pd.read_csv(wind)
            merged = merge_winddata_into_forecasts(winddf, merged)
        else:
            features = ["WindSpeedLand_ms",
                        "WindDirLand_deg",
                        "WindGustLand_ms",
                        "WindSpeedSea_ms",
                        "WindDirSea_deg",
                        "WindGustSea_ms",
                        "TempSurfaceLand_c",
                        "Temp2mtLand_c",
                        "MSLPressureLand_hPa",
                        "TempSurfaceSea_c",
                        "Temp2mtSea_c",
                        "MSLPressureSea_hPa",
                        "WindDateTime",
                        "WindTimeDiff"]

            for f in features:
                merged[f] = None

        # with open("/Users/michaelreinwald/ratings/wind_type.json") as f:
        #     windtype = json.load(f)
        # merged["wind_type"] = [windtype.get(
        #     spot) for spot in merged["SpotId"]]

        # merged["ratings_from_lotus_bulk"] = np.array(bulk_algo_rating(np.array(merged["LotusWindDir_deg"]),
        #                                                                 np.array(merged["spot_offshoredirection_deg"]),
        #                                                                 np.array(
        #                                                                     merged["LotusWindSpeed_ms"]),
        #                                                                 np.array(
        #                                                                     merged["LotusMinBWH_mt"]),
        #                                                                 np.array(
        #                                                                     merged["LotusMaxBWH_mt"]),
        #                                                                 np.array(
        #                                                                     merged["wind_type"])
        #                                                                 )) - 1

        with open(tidesfolder + spot + ".json") as f:
            tidesdict = json.load(f)

        merged = get_historical_tide(merged, tidesdict)

        merged.to_csv(outfile, index=False)


def run(spots, no_workers=4):
    pool = multiprocessing.Pool(processes=no_workers)  # spawn 4 processes
    calculate_partial = partial(generate_data,
                                input=input)  # partial function creation to allow input to be sent to the calculate
    # function
    result = pool.map(calculate_partial, spots)
    pool.close()


if __name__ == '__main__':

    home = "/Users/michaelreinwald/"

    lookup_ratings = {
        'NONE': None,
        None: None,
        "FLAT": 0,
        "VERY_POOR": 1,
        "POOR": 2,
        "POOR_TO_FAIR": 3,
        "FAIR": 4,
        "FAIR_TO_GOOD": 5,
        "GOOD": 6,
        "VERY_GOOD": 7,
        "GOOD_TO_EPIC": 8,
        "EPIC": 9}

    reduced_class_map = {
        'NONE': None,
        None: None,
        'FLAT': 0,
        'VERY_POOR': 0,
        'POOR': 1,
        'POOR_TO_FAIR': 2,
        'FAIR': 3,
        'FAIR_TO_GOOD': 4,
        'GOOD': 4,
        'VERY_GOOD': 4,
        'GOOD_TO_EPIC': 4,
        'EPIC': 4}

    # spots = [os.path.split(filename)[1] for filename in glob(
    #     home + "/ratings/human_reports/*.csv")]
    # print(spots)
    sds_data = pd.read_csv(home + "/spot_database/spots_sds_full_3.csv")
    subregiondf = pd.read_csv("/Users/michaelreinwald/spot_database/Subregions.csv")

    tidesfolder = home + "/ratings/tides/"

    spotlist = []
    for spot in sds_data["SpotId"]:
        spot = str(spot)
        report = home + "/ratings/human_reports/" + spot + ".csv"
        lotus = home + "/ratings/BWH_files/lotus_bwh_" + spot + ".csv"
        wind = home + "/ratings/NAM_hindcast/NAM_hindcast_" + spot + ".csv"
        spotmeta = home + "spot_database/individual_spots/" + spot + ".json"
        sds_data_filt = sds_data.loc[sds_data["SpotId"] == spot]

        outfile = home + "/ratings/merged_data/" + spot + ".csv"
        if not os.path.isfile(outfile):
            if os.path.isfile(lotus):
                if os.path.isfile(spotmeta):
                    # if os.path.isfile(wind):
                    if os.path.isfile(tidesfolder + spot + ".json"):
                        spotlist.append(spot)
    print(len(spotlist))
    # print(spotlist)

    run(spotlist, no_workers=4)
