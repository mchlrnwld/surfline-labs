import os
import numpy as np
import requests
import pandas as pd
from bson.json_util import dumps
from tqdm import tqdm
from flatten_json import flatten
import json

def pull_sds_data(poi_id, graphurl):
    """
    Used to pull forecast data from the science data service using the graphql
    query.
    Args:
        poi_id: point of interest id for the spot to pull forecast data for
    Returns:
        the pulled sds data in dict form
    """
    query = f"""
    query {{
        pointOfInterest(pointOfInterestId: "{poi_id}") {{
            pointOfInterestId
            name
            lat
            lon
            surfSpotConfiguration {{
                offshoreDirection
                optimalSwellDirection
                spectralRefractionMatrix
            }}
            spots {{
                __typename

                ...on SLSpot {{
                    _id
                    name
                    timezone
                }}
            }}
        }}
    }}
    """
    resp = requests.post(graphurl, json={"query": query})

    if resp.status_code != 200:
        raise Exception(f'Error pulling SDS data ({resp.status_code})')

    if resp:
        print('yes')
        return resp.json()
        
    else:
        return None


def download_json(poi, graphurl, outfile):

    resp = pull_sds_data(str(poi), graphurl)
    if resp:
        with open(outfile, 'w') as file:
            file.write(dumps(resp))



if __name__ == '__main__':

    outfolder = '/Users/michaelreinwald/spot_database/'

    masterfile = outfolder+'/spots.csv'
    masterdf = pd.read_csv(masterfile)

    graphurl = "https://services.surfline.com/graphql?api_key=temporary_and_insecure_wavetrak_api_key"

    poi = "bb1c0dd3-c828-405a-8429-deed884e2f9c"
    spotId = "5c5382c9dc2e2000015732f4"
    download_json(poi, graphurl, outfolder+'/sds_spotinfo/'+str(spotId)+'.json')

    # spotnames = []
    # spotids = []
    # pois = []
    # lats = []
    # lons = []
    # offshores = []

    # for poi in tqdm(masterdf['pointOfInterestId'].unique(), total=len(masterdf['pointOfInterestId'].unique())):

    #     resp = pull_sds_data(str(poi), graphurl)

    #     spot_json_info_flattened = flatten(resp, ".")
    #     spotId = spot_json_info_flattened.get('data.pointOfInterest.spots.0._id', np.nan)
    #     poidata = resp["data"].get("pointOfInterest", np.nan)
    #     if poidata:
    #         spotdata = poidata.get("spots", np.nan)
    #         if spotdata:
    #             for spottype in resp["data"]["pointOfInterest"]["spots"]:
    #                 if spottype["__typename"] == "SLSpot":
    #                     spotId = spottype["_id"]
    #     spotName = spot_json_info_flattened.get('data.pointOfInterest.name', np.nan)
    #     lat = spot_json_info_flattened.get('data.pointOfInterest.lat', np.nan)
    #     lon = spot_json_info_flattened.get('data.pointOfInterest.lon', np.nan)
    #     offshore = spot_json_info_flattened.get('data.pointOfInterest.surfSpotConfiguration.offshoreDirection', np.nan)

    #     if spotId:
    #         if resp:
    #             with open(outfolder+'/sds_spotinfo/'+str(spotId)+'.json', 'w') as file:
    #                 file.write(dumps(resp))

    #             spotnames.append(spotName)
    #             spotids.append(spotId)
    #             pois.append(str(poi))
    #             lats.append(lat)
    #             lons.append(lon)
    #             offshores.append(offshore)

    # summarydict = {'SpotName': spotnames, 
    #                 'SpotId': spotids, 
    #                 'SpotPOI': pois,
    #                 'lat': lats, 
    #                 'lon': lons, 
    #                 'offshoreDirection': offshores
    #                 }
    # summarydf = pd.DataFrame(summarydict)               
    # summarydf.to_csv(outfolder+'/spots_sds_full_3.csv', index=False)




    # outfolder = '/Users/michaelreinwald/spot_database/'

    # spotnames = []
    # spotids = []
    # pois = []
    # lats = []
    # lons = []
    # offshores = []

    # for file in os.listdir(outfolder+'/sds_spotinfo/'):

    #     if file.endswith('json'):

    #         with open(outfolder+'/sds_spotinfo/'+file, 'r') as f:
    #             resp = json.load(f)

    #         spot_json_info_flattened = flatten(resp, ".")
    #         spotId = spot_json_info_flattened.get('data.pointOfInterest.spots.0._id', np.nan)
    #         poi = spot_json_info_flattened.get('data.pointOfInterest.pointOfInterestId', np.nan)
    #         spotName = spot_json_info_flattened.get('data.pointOfInterest.name', np.nan)
    #         lat = spot_json_info_flattened.get('data.pointOfInterest.lat', np.nan)
    #         lon = spot_json_info_flattened.get('data.pointOfInterest.lon', np.nan)
    #         offshore = spot_json_info_flattened.get('data.pointOfInterest.surfSpotConfiguration.offshoreDirection', np.nan)

    #         if os.path.isfile(outfolder+'/sds_spotinfo/'+str(spotId)+'.json'):

    #             spotnames.append(spotName)
    #             spotids.append(spotId)
    #             pois.append(str(poi))
    #             lats.append(lat)
    #             lons.append(lon)
    #             offshores.append(offshore)

    # summarydict = {'SpotName': spotnames, 
    #                 'SpotId': spotids, 
    #                 'SpotPOI': pois,
    #                 'lat': lats, 
    #                 'lon': lons, 
    #                 'offshoreDirection': offshores
    #                 }
    # summarydf = pd.DataFrame(summarydict)               
    # summarydf.to_csv(outfolder+'/spots_sds_full_3.csv', index=False)
