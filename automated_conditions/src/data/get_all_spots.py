#
# This is a script to query the spot data using pymongo
# For examples on how to query and filter, see:
# https://docs.mongodb.com/manual/reference/sql-comparison/
# https://pymongo.readthedocs.io/en/stable/tutorial.html
# Requirements:
# prod mongo dB access (username + password)
# Python 3.6+

import sys
import json
from typing import Optional
from bson.json_util import dumps
from pymongo import MongoClient, errors
import pandas as pd
from flatten_json import flatten
from datetime import datetime


def initialize_collection(username: str, pwd: str, collection: str):
    """
    Get collection object with user credentials
    :param username: prod mongodb username
    :param pwd: prod mongodb password
    :returns: client and collection object
    """
    # !!! using the 'srv' uri also requires dnspython to be installed
    uri = "mongodb+srv://" + username + ":" + pwd + "@prod-kbyg-mongodb-atlas.aj1dk.mongodb.net/?authSource=admin"
    client = MongoClient(uri, connect=False)
    database = client["KBYG"]
    collection = database[collection]
    # print('Number of entries in collection: ', collection.count_documents({}))
    return client, collection

def query_collection(collection, query: dict, projection: Optional[dict] = None, iterative: Optional[bool] = True) -> list:
    """
    Get filtered iterable cursor object
    :param collection: initialized collection
    :param query: dictionary with all queries/filters
    :param projection: dictionary with all projections/selected fields
    :param iterative: if false, query all sessions at once, which could lead to high disk I/O
    :returns: list of all sessions as dicts
    """
    cursor = collection.find(query, projection=projection)
    if iterative:
        docs = []
        for c in cursor:
            docs.append(c)
    else:
        docs = list(cursor)
    return docs


def savejson(docs: list, outfile: str) -> None:
    """
    Save list of spot dictionaries as json file
    :param docs: list of spot dictionaries
    :param outfile: full file path (including .json) where to save the file
    :returns:
    """
    with open(outfile, 'w') as file:
        file.write(dumps(docs))


def loadjson(file: str) -> list:
    """
    Load data from json
    :param file: full file path (including .json) of downloaded json file
    :returns: list with each entry being a dictionary
    """
    with open(file) as f:
        data = json.load(f)
    return data


if __name__ == "__main__":

    #use your mongodb credentials to connect to the KBYG database
    client, collection = initialize_collection("michael-reinwald", "YdEJR1IxsaQLxJjf", collection="Spots")

    #get a list of ALL spots
    try:
        spotlist = query_collection(collection, query={}, iterative=False)
        print(len(spotlist))
    except errors.ServerSelectionTimeoutError:
        print(datetime.now(), "FAIL")
        client.close()
        sys.exit(1)

    #save the entire list in one json file
    savejson(spotlist, "/Users/michaelreinwald/spot_database/spots.json")

    #create separate json files for each spot
    for spot in spotlist:
        spotid = str(spot["_id"])
        savejson(spot, "/Users/michaelreinwald/spot_database/individual_spots/"+spotid+".json")
    
    #flatten each spot dictionary and save everything as one csv file
    spotdf = pd.DataFrame([flatten(spot, ".") for spot in spotlist])
    spotdf.to_csv("/Users/michaelreinwald/spot_database/spots.csv", index=False)

    #just some output for cron to keep track of the jobß
    print(datetime.now(), "SUCCESS")
    client.close()





    client, collection = initialize_collection("michael-reinwald", "YdEJR1IxsaQLxJjf", collection="Subregions")

    #get a list of ALL spots
    try:
        spotlist = query_collection(collection, query={}, iterative=False)
        print(len(spotlist))
    except errors.ServerSelectionTimeoutError:
        print(datetime.now(), "FAIL")
        client.close()
        sys.exit(1)

    #save the entire list in one json file
    savejson(spotlist, "/Users/michaelreinwald/spot_database/subregions.json")

    #flatten each spot dictionary and save everything as one csv file
    spotdf = pd.DataFrame([flatten(spot, ".") for spot in spotlist])
    spotdf.to_csv("/Users/michaelreinwald/spot_database/subregions.csv", index=False)

    #just some output for cron to keep track of the jobß
    print(datetime.now(), "SUCCESS")
    client.close()




    client, collection = initialize_collection("michael-reinwald", "YdEJR1IxsaQLxJjf", collection="Regions")

    #get a list of ALL spots
    try:
        spotlist = query_collection(collection, query={}, iterative=False)
        print(len(spotlist))
    except errors.ServerSelectionTimeoutError:
        print(datetime.now(), "FAIL")
        client.close()
        sys.exit(1)

    #save the entire list in one json file
    savejson(spotlist, "/Users/michaelreinwald/spot_database/regions.json")

    #flatten each spot dictionary and save everything as one csv file
    spotdf = pd.DataFrame([flatten(spot, ".") for spot in spotlist])
    spotdf.to_csv("/Users/michaelreinwald/spot_database/regions.csv", index=False)

    #just some output for cron to keep track of the jobß
    print(datetime.now(), "SUCCESS")
    client.close()