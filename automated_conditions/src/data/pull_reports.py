import pandas as pd
import boto3
import botocore
from glob import glob
from tqdm import tqdm
import os
from typing import Union


def download_specific_surfreport(spotid: str, botoprofile: str, outfile: str) -> None:
    """
    Get all reports for a spot
    :param sessionid: 24 char string spotID
    :param botoprofile: profile string in your aws config
    :param outfile: full file path (including .parquet) where to download the file
    :returns: None
    """
    session = boto3.Session(profile_name=botoprofile)
    s3 = session.resource('s3')
    bucket = s3.Bucket("wt-data-lake-prod")
    pull_specific_surfreport(bucket, spotid, outfile)


def download_surfreports(botoprofile: str, folder: str) -> None:
    """
    Get all reports for a spot
    :param sessionid: 24 char string spotID
    :param botoprofile: profile string in your aws config
    :param outfile: full file path (including .parquet) where to download the file
    :returns: None
    """
    session = boto3.Session(profile_name=botoprofile)
    s3 = session.resource('s3')
    bucket = s3.Bucket("wt-data-lake-prod")
    pull_surfreports(bucket, folder)
    

def pull_specific_surfreport(bucket, spotid: str, outfile: str) -> None:
    """
    Try downloading events, raise error if file doesn't exist
    :param bucket: s3 bucket object
    :param spotid: 24 char string spotID
    :param outfile: full file path (including .parquet) where to download the file
    :returns: None
    """
    try:
        bucket.download_file("science/historical_human_surf_reports/"+spotid+"/data.parquet", outfile)
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == "404":
            # print('The object does not exist')
            pass
        else:
            print('Something else has gone wrong')
            raise

def pull_surfreports(bucket, folder: str) -> None:
    os.makedirs(folder, exist_ok=True)
    for obj in tqdm(bucket.objects.filter(Prefix="science/historical_human_surf_reports/")):
        try:
            spotid = obj.key.split("/")[2]
            bucket.download_file(obj.key, folder+spotid+".parquet")
            convert_to_csv(folder+spotid+".parquet", folder+spotid+".csv")
        except botocore.exceptions.ClientError as e:
            if e.response['Error']['Code'] == "404":
                # print('The object does not exist')
                pass
            else:
                print('Something else has gone wrong')
                raise


def download_24hforecasts(botoprofile: str, folder: str) -> None:
    session = boto3.Session(profile_name=botoprofile)
    s3 = session.resource('s3')
    bucket = s3.Bucket("wt-data-lake-prod")
    pull_24hforecasts(bucket, folder)
    

def pull_24hforecasts(bucket, folder: str) -> None:
    os.makedirs(folder, exist_ok=True)
    for obj in tqdm(bucket.objects.filter(Prefix="science/historical_human_subregion_surf_forecasts/")):
        if "/24/" in obj.key:
            try:
                subregion = obj.key.split("/")[2]
                bucket.download_file(obj.key, folder+subregion+".parquet")
                convert_to_csv(folder+subregion+".parquet", folder+subregion+".csv")
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "404":
                    # print('The object does not exist')
                    pass
                else:
                    print('Something else has gone wrong')
                    raise


def convert_to_csv(infile: str, outfile: str) -> None:
    """
    Loads parquet and saves csv
    :param infile: parquet file to load
    :param outfile: csv file to save
    :returns: None
    """
    assert infile.endswith(".parquet"), "Expecting parquet file as input"
    assert outfile.endswith(".csv"), "Expecting csv file as output"

    df = pd.read_parquet(infile)
    df.to_csv(outfile, index=False)

def merge_csvs(csvs: Union[list,tuple]) -> pd.DataFrame:
    """
    Takes a list of csvs, loads them into pandas datframes and merges them into one big dataframe
    :param csvs: list or tuple of filepaths to csv files
    :returns: pandas datafarme containing all merged data
    """
    df = pd.DataFrame()

    for csvi in csvs:
        dfi = pd.read_csv(csvi)
        df = pd.concat([df,dfi], ignore_index=True)
    return df


if __name__ == '__main__':

    home = "/Users/michaelreinwald/ratings/"
    folder_24h = home+"/24hforecasts/"
    folder_reports = home+"/human_reports/"

    download_24hforecasts(botoprofile="surfline-prod",  folder=folder_24h)
    allforecasts = glob(folder_24h+"/*.csv")
    allforecastsdf = merge_csvs(allforecasts)
    allforecastsdf.to_csv(folder_24h+"/allregions.csv")

    download_surfreports(botoprofile="surfline-prod",  folder=folder_reports)
