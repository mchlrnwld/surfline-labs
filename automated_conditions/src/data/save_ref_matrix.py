
import pandas as pd
import json
import numpy as np
import pickle
from tqdm import tqdm
import os 
outfolder = '/Users/michaelreinwald/spot_database/'

refmatrices = {}

for spotid in os.listdir(outfolder+'/sds_spotinfo/'):
    if spotid.endswith("json"):
        spotid = spotid[:-5] 
        with open(outfolder+'/sds_spotinfo/'+str(spotid)+'.json') as f:
            spot_json_info = json.load(f)

        def_ref_matrix = spot_json_info['data']['pointOfInterest']['surfSpotConfiguration']['spectralRefractionMatrix']

        if def_ref_matrix != "null":

            def_ref_matrix = np.array(def_ref_matrix)
            
            if np.shape(def_ref_matrix) == (24,18):

                with open(outfolder+'/ref_matrix/'+str(spotid)+'.pkl', 'wb') as fp:
                    pickle.dump(def_ref_matrix, fp)

                refmatrices[spotid] = def_ref_matrix

with open(outfolder+'/ref_matrices_all.pkl', 'wb') as fp:
        pickle.dump(refmatrices, fp)
#

# summarydf = pd.read_csv(masterfile)
# for spotid in summarydf['SpotId']:
#     with open(outfolder+'/sds_spotinfo/'+spotid+'.json') as f:
#         spot_json_info = json.load(f)

#     spot_lon = spot_json_info['data']['pointOfInterest']['lon']
#     spot_lat = spot_json_info['data']['pointOfInterest']['lat']
#     spot_timezone = spot_json_info['data']['pointOfInterest']['spots'][0]['timezone']

#     nb_info_grids = len(spot_json_info['data']['pointOfInterest']['gridPoints'])

#     spot_lotus_lon = []
#     spot_lotus_lat = []
#     lotus_grid = []
#     def_ref_matrix = np.zeros((24, 18))

#     for k in range(0, nb_info_grids):
#         model_name = spot_json_info['data']['pointOfInterest']['gridPoints'][k]['grid']['model']
#         grid_name = spot_json_info['data']['pointOfInterest']['gridPoints'][k]['grid']['grid']
#         if model_name == 'Lotus-WW3':
#             if not lotus_grid:
#                 lotus_grid = grid_name
#                 spot_lotus_lon = spot_json_info['data']['pointOfInterest']['gridPoints'][k]['lon']
#                 spot_lotus_lat = spot_json_info['data']['pointOfInterest']['gridPoints'][k]['lat']
#                 spot_breakingWaveHeightIntercept = spot_json_info['data'][
#                     'pointOfInterest']['gridPoints'][k]['breakingWaveHeightIntercept']
#                 spot_breakingWaveHeightCoefficient = spot_json_info['data'][
#                     'pointOfInterest']['gridPoints'][k]['breakingWaveHeightCoefficient']
#                 for i in range(24):
#                     for j in range(18):
#                         def_ref_matrix[i, j] = spot_json_info['data']['pointOfInterest']['gridPoints'][k]['spectralRefractionMatrix'][i][j]
#             else:
#                 #if grid_name[-3:] == '15m':  # force global
#                 if grid_name[-2:] == '3m':  # force nearshore
#                     lotus_grid = grid_name
#                     spot_lotus_lon = spot_json_info['data']['pointOfInterest']['gridPoints'][k]['lon']
#                     spot_lotus_lat = spot_json_info['data']['pointOfInterest']['gridPoints'][k]['lat']
#                     spot_breakingWaveHeightIntercept = spot_json_info['data'][
#                         'pointOfInterest']['gridPoints'][k]['breakingWaveHeightIntercept']
#                     spot_breakingWaveHeightCoefficient = spot_json_info['data'][
#                         'pointOfInterest']['gridPoints'][k]['breakingWaveHeightCoefficient']
#                     for i in range(24):
#                         for j in range(18):
#                             def_ref_matrix[i, j] = spot_json_info['data']['pointOfInterest']['gridPoints'][k]['spectralRefractionMatrix'][i][j]

#     with open(outfolder+'/ref_matrix/'+spotid+'.pkl', 'wb') as fp:
#         pickle.dump(def_ref_matrix, fp)

#     coefficients = {"breakingWaveHeightCoefficient": spot_breakingWaveHeightCoefficient, 
#                     "breakingWaveHeightIntercept": spot_breakingWaveHeightIntercept}

#     with open(outfolder+'/coefficients/'+spotid+'.pkl', 'wb') as fp:
#         pickle.dump(coefficients, fp)

#     # def_ref_matrix_expanded = np.ones((24,25))
#     # def_ref_matrix_expanded[:24,:18] = def_ref_matrix
#     # for i in np.arange(18,25):
#     #     def_ref_matrix_expanded[:,i] = def_ref_matrix[:,17]
#     # def_ref_matrix_expanded = def_ref_matrix_expanded.T
    
#     refmatrices[spotid] = def_ref_matrix

# with open(outfolder+'/all_ref_matrices.pkl', 'wb') as fp:
#         pickle.dump(refmatrices, fp)
