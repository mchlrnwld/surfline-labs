import pandas as pd
import requests
import time
import numpy as np
from bson.json_util import dumps
import os
from tqdm import tqdm

def save_tides(lat, lon, outfile: str) -> None:

    timestart = str(1569884399)
    timeend = str(time.time())

    resp = requests.get("http://services.surfline.com/tide/data?lat="+lat+"&lon="+lon+"&start="+timestart+"&end="+timeend)

    if resp:
        resp = resp.json()

        with open(outfile, 'w') as file:
            file.write(dumps(resp))

if __name__ == '__main__':

    spotlist = pd.read_csv("/Users/michaelreinwald/spot_database/spots_sds_full_3.csv")
    spotlist = spotlist.dropna(subset=['SpotId'])

    spots_filt = []
    for spot in spotlist["SpotId"].unique():
        spot = str(spot)
        if not os.path.isfile("/Users/michaelreinwald/ratings/tides/"+spot+".json"):
            spots_filt.append(spot)

    for spot in tqdm(spots_filt):

        spotlist_filt = spotlist.loc[spotlist["SpotId"] == spot]

        lat = str(spotlist_filt["lat"].unique().item())
        lon = str(spotlist_filt["lon"].unique().item())

        save_tides(lat, lon, "/Users/michaelreinwald/ratings/tides/"+spot+".json")
