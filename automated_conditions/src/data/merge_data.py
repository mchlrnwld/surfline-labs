import pandas as pd
from glob import glob
import os
from tqdm import tqdm

from typing import Union
import json
import numpy as np
import math
from datetime import datetime
import time as tm
import requests
from bson.json_util import dumps
from flatten_json import flatten
from transforms import bulk_algo_rating
import sklearn
from sort_all_swells import sort_swells, split_train_test, split_train_test_regions, split_train_test_subregions
import time
import pickle
import multiprocessing
from functools import partial
import shutil
multiprocessing.freeze_support()


if __name__ == '__main__':

    home = "/Users/michaelreinwald/"

    df = pd.read_csv(home+"/ratings/alldata_sds.csv")
    print(len(df))
    df = pd.read_csv(home+"/ratings/alldata_sds_filt.csv")
    print(len(df))

    subregions = pd.read_csv(home+"spot_database/Subregions.csv")
    regions = pd.read_csv(home+"spot_database/regions.csv")

    allspots = glob(home+"/ratings/merged_data/*")
    
    df = pd.DataFrame()
    for csvi in tqdm(allspots):
        dfi = pd.read_csv(csvi)

        if not dfi["SpotName"].astype(str).str.contains("Overview").any():
            dfi = dfi.loc[dfi["report_time_diff"].abs() < (0.51*3600)]
            dfi = dfi.dropna(subset=["surf_condition", "spot_offshoredirection_deg", "LotusWindDir_deg", "tide_level"])

            if len(dfi)>0:
                shutil.copy2(csvi,home+"ratings/merged_data_wreports/"+os.path.basename(csvi))
                dfi = dfi.dropna(subset=["surf_condition_num_5pt"])
                df = pd.concat([df, dfi], ignore_index=True)

    df.reset_index(drop=True)
    
    names = []
    for sub in tqdm(df["subregion"]):
        subregions_filt = subregions.loc[subregions["_id"] == sub]
        names.append(subregions_filt["name"].unique().item())
    df["subregionName"] = names
    dfsub = df["subregionName"].value_counts()
    dfsub.to_csv(home+"/ratings/subregionstats.csv")

    names = []
    for sub in tqdm(df["region"]):
        regions_filt = regions.loc[regions["_id"] == sub]
        names.append(regions_filt["name"].unique().item())
    df["regionName"] = names
    dfsub = df["regionName"].value_counts()
    dfsub.to_csv(home+"/ratings/regionstats.csv")

    df.to_csv(home+"/ratings/alldata_sds.csv", index=False)
    print(len(df))


    #filtering

    df_filt = df[df['region'].map(df['region'].value_counts()) > 100]

    le_spot = sklearn.preprocessing.LabelEncoder()
    le_spot.fit(df_filt["SpotId"])
    df_filt["SpotId_encoded"] = le_spot.transform(df_filt["SpotId"])
    print(len(le_spot.classes_))
    with open(home+"/ratings/spot_encoder.pkl", 'wb') as f:
        pickle.dump(le_spot, f)

    le_subregion = sklearn.preprocessing.LabelEncoder()
    le_subregion.fit(df_filt["subregion"])
    df_filt["subregion_encoded"] = le_subregion.transform(
        df_filt["subregion"])
    print(len(le_subregion.classes_))
    with open(home+"/ratings/subregion_encoder.pkl", 'wb') as f:
        pickle.dump(le_subregion, f)

    le_region = sklearn.preprocessing.LabelEncoder()
    le_region.fit(df_filt["region"])
    df_filt["region_encoded"] = le_region.transform(
        df_filt["region"])
    print(len(le_region.classes_))
    with open(home+"/ratings/region_encoder.pkl", 'wb') as f:
        pickle.dump(le_region, f)

    df_filt.to_csv(home+"/ratings/alldata_sds_filt.csv", index=False)
    print(len(df_filt))

    split_train_test_regions(mastercsv=home+"/ratings/alldata_sds_filt.csv", 
                    testsize=0.15, 
                    randomstate=1337,
                    stratify="surf_condition_num_5pt")

    split_train_test_subregions(mastercsv=home+"/ratings/alldata_sds_filt.csv", 
                    testsize=0.15, 
                    randomstate=1337,
                    stratify="surf_condition_num_5pt")

    # df = pd.read_csv(home+"/ratings/alldata_sds.csv")

    # df_old_train = pd.read_csv(home+"/ratings/old/sds_train.csv")
    # df_old_test = pd.read_csv(home+"/ratings/old/sds_test.csv")
    # df_old = pd.concat([df_old_train, df_old_test])

    # names = []
    # increase = []
    # for region in tqdm(df["region"].unique()):
    #     regions_filt = regions.loc[regions["_id"] == region]
    #     names.append(regions_filt["name"].unique().item())
    #     prev_size = len(df_old.loc[df_old["region"] == region])
    #     new_size = len(df.loc[df["region"] == region])
    #     if prev_size == 0:
    #         increase.append(None)
    #     else:
    #         increase.append(new_size / prev_size)

    # increasedf = pd.DataFrame({'region': names, 'increase': increase})
    # increasedf = increasedf.sort_values(by=['increase'])

    # increasedf.to_csv(home+"increasebyregions.csv", index=False)


    allspots = pd.read_csv("/Users/michaelreinwald/spot_database/spots_sds_full_3.csv")
    print(len(allspots))

    spotswhindcastfiles = os.listdir("/Users/michaelreinwald/ratings/merged_data/")
    spotswhindcast = [s[:-4] for s in spotswhindcastfiles]
    allspots = allspots.loc[allspots["SpotId"].astype(str).isin(spotswhindcast)]
    print(len(allspots))

    spotdata = pd.read_csv("/Users/michaelreinwald/spot_database/spots.csv")
    subregions=pd.read_csv("/Users/michaelreinwald/spot_database/Subregions.csv")
    regions=pd.read_csv("/Users/michaelreinwald/spot_database/regions.csv")

    reports = []
    for spot in allspots["SpotId"]:
        if os.path.isfile("/Users/michaelreinwald/ratings/merged_data_wreports/"+spot+".csv"):
            reportsdf = pd.read_csv("/Users/michaelreinwald/ratings/merged_data_wreports/"+spot+".csv")
            reportsdf.dropna(subset=["surf_condition_num_5pt"], inplace=True)
            reports.append(len(reportsdf))
        else:
            reports.append(None)
    allspots["NoOfReports"] = reports

    subregionlist = []
    for spot in allspots["SpotId"]:
            match = spotdata.loc[spotdata["_id"] == spot]["subregion"].to_numpy()
            if match:
                subregionlist.append(match[0])
            else:
                subregionlist.append(None)
    allspots["SubregionId"] = subregionlist

    regionlist = []
    for subregion in allspots["SubregionId"]:
        match = subregions.loc[subregions["_id"] == subregion]["region"].to_numpy()
        if match:
            regionlist.append(match[0])
        else:
            regionlist.append(None)
    allspots["RegionId"] = regionlist

    names = []
    for sub in tqdm(allspots["SubregionId"]):
        subregions_filt = subregions.loc[subregions["_id"] == sub]
        names.append(subregions_filt["name"].unique().item())
    allspots["SubregionName"] = names

    names = []
    for sub in tqdm(allspots["RegionId"]):
        regions_filt = regions.loc[regions["_id"] == sub]
        names.append(regions_filt["name"].unique().item())
    allspots["RegionName"] = names


    allspots.to_csv("/Users/michaelreinwald/ratings/inference_data.csv", index=False)




