import pandas as pd
import numpy as np
from tqdm import tqdm
import os
from sklearn.model_selection import StratifiedShuffleSplit, ShuffleSplit
import sklearn
import pickle

def split_train_test(mastercsv="/data/mike/ratings_data/alldata_updated2.csv", 
                    testsize=0.2, 
                    randomstate=1337,
                    stratify="surf_condition_num",
                    prefix="",
                    outfolder="/home/mreinwald/updated_dataset/clean/data/"):

    fulldataset = pd.read_csv(mastercsv)

    # split the dataframe into parts with similar distribution of surf_condition_num
    sss = StratifiedShuffleSplit(n_splits=1, test_size=testsize, random_state=randomstate)
    split= sss.split(fulldataset[stratify],fulldataset[stratify])
    for trainind, testind in split:
        train = fulldataset.iloc[trainind].copy()
        train = train.reset_index(drop=True)
        test = fulldataset.iloc[testind].copy()
        test.dropna(subset=["forecast_24h_num"], inplace=True)
        test = test.reset_index(drop=True)

    train.to_csv(outfolder+prefix+"train.csv", index=False)
    test.to_csv(outfolder+prefix+"test.csv", index=False)


def split_train_test_regions(mastercsv="/data/mike/ratings_data/alldata_updated2.csv", 
                    testsize=0.15, 
                    randomstate=1337,
                    stratify="surf_condition_num"):

    os.makedirs("/Users/michaelreinwald/ratings/regions/", exist_ok=True)

    fulldataset = pd.read_csv(mastercsv)

    traindf = pd.DataFrame()
    testdf = pd.DataFrame()

    for subregion in fulldataset["region"].unique():

        fulldataset_subregion = fulldataset.loc[fulldataset["region"] == subregion]
        fulldataset_subregion = fulldataset_subregion.reset_index(drop=True)
        print(fulldataset_subregion["regionName"].unique().item(), len(fulldataset_subregion))

        if fulldataset_subregion["regionName"].unique().item() == 'Big Island Hawaii':
            sss = ShuffleSplit(test_size=testsize, random_state=randomstate)
            split= sss.split(fulldataset_subregion,fulldataset_subregion)
        else:
            sss = StratifiedShuffleSplit(n_splits=1, test_size=testsize, random_state=randomstate)
            split= sss.split(fulldataset_subregion[stratify],fulldataset_subregion[stratify])
        for trainind, testind in split:
            traindf_subregion = fulldataset_subregion.iloc[trainind].copy()
            traindf_subregion = traindf_subregion.reset_index(drop=True)
            testdf_subregion = fulldataset_subregion.iloc[testind].copy()
            testdf_subregion = testdf_subregion.reset_index(drop=True)

        traindf_subregion.to_csv("/Users/michaelreinwald/ratings/regions/"+str(subregion)+"_train.csv", index=False)
        testdf_subregion.to_csv("/Users/michaelreinwald/ratings/regions/"+str(subregion)+"_test.csv", index=False)

        traindf = pd.concat([traindf,traindf_subregion], ignore_index=True)
        testdf = pd.concat([testdf,testdf_subregion], ignore_index=True)
    
    traindf.to_csv("/Users/michaelreinwald/ratings/allregions_train.csv", index=False)
    testdf.to_csv("/Users/michaelreinwald/ratings/allregions_test.csv", index=False)



def split_train_test_subregions(mastercsv="/data/mike/ratings_data/alldata_updated2.csv", 
                    testsize=0.15, 
                    randomstate=1337,
                    stratify="surf_condition_num"):

    os.makedirs("/Users/michaelreinwald/ratings/subregions/", exist_ok=True)

    fulldataset = pd.read_csv(mastercsv)

    traindf = pd.DataFrame()
    testdf = pd.DataFrame()

    for subregion in fulldataset["subregion"].unique():

        fulldataset_subregion = fulldataset.loc[fulldataset["subregion"] == subregion]
        fulldataset_subregion = fulldataset_subregion.reset_index(drop=True)
        print(fulldataset_subregion["subregionName"].unique().item(), len(fulldataset_subregion), np.min(fulldataset_subregion[stratify].value_counts()))

        if np.min(fulldataset_subregion[stratify].value_counts()) <2:
            sss = ShuffleSplit(test_size=testsize, random_state=randomstate)
            split= sss.split(fulldataset_subregion,fulldataset_subregion)
        else:
            sss = StratifiedShuffleSplit(n_splits=1, test_size=testsize, random_state=randomstate)
            split= sss.split(fulldataset_subregion[stratify],fulldataset_subregion[stratify])

        for trainind, testind in split:
            traindf_subregion = fulldataset_subregion.iloc[trainind].copy()
            traindf_subregion = traindf_subregion.reset_index(drop=True)
            testdf_subregion = fulldataset_subregion.iloc[testind].copy()
            testdf_subregion = testdf_subregion.reset_index(drop=True)

        traindf_subregion.to_csv("/Users/michaelreinwald/ratings/subregions/"+str(subregion)+"_train.csv", index=False)
        testdf_subregion.to_csv("/Users/michaelreinwald/ratings/subregions/"+str(subregion)+"_test.csv", index=False)

        traindf = pd.concat([traindf,traindf_subregion], ignore_index=True)
        testdf = pd.concat([testdf,testdf_subregion], ignore_index=True)
    
    traindf.to_csv("/Users/michaelreinwald/ratings/allsubregions_train.csv", index=False)
    testdf.to_csv("/Users/michaelreinwald/ratings/allsubregions_test.csv", index=False)
    

def sort_swells(csv, outcsv):

    data = pd.read_csv(csv)

    for rowi in tqdm(range(len(data)), desc='Sorting swells'):
        data_row = data.iloc[rowi]

        sighlist = [data_row["LotusSigHPart"+str(i)+"_mt"] for i in np.arange(0,6)]
        PDirsinlist = [data_row["LotusPDirPart"+str(i)+"_deg_rel_sin"] for i in np.arange(0,6)]
        PDircoslist = [data_row["LotusPDirPart"+str(i)+"_deg_rel_cos"] for i in np.arange(0,6)]
        tppartlist = [data_row["LotusTPPart"+str(i)+"_sec"] for i in np.arange(0,6)]
        spreadlist = [data_row["LotusSpreadPart"+str(i)+"_deg"] for i in np.arange(0,6)]

        sorted_order = np.argsort(np.nan_to_num(np.array(sighlist)))[::-1]
        
        sorted_sighlist = [sighlist[i] for i in sorted_order]
        sorted_PDirsinlist = [PDirsinlist[i] for i in sorted_order]
        sorted_PDircoslist = [PDircoslist[i] for i in sorted_order]
        sorted_tppartlist = [tppartlist[i] for i in sorted_order]
        sorted_spreadlist = [spreadlist[i] for i in sorted_order]

        data.loc[rowi, ["LotusSigHPart"+str(i)+"_mt_sorted" for i in np.arange(0,6)]] = sorted_sighlist
        data.loc[rowi, ["LotusPDirPart"+str(i)+"_deg_rel_sin_sorted" for i in np.arange(0,6)]] = sorted_PDirsinlist
        data.loc[rowi, ["LotusPDirPart"+str(i)+"_deg_rel_cos_sorted" for i in np.arange(0,6)]] = sorted_PDircoslist

        data.loc[rowi, ["LotusTPPart"+str(i)+"_sec_sorted" for i in np.arange(0,6)]] = sorted_tppartlist
        data.loc[rowi, ["LotusSpreadPart"+str(i)+"_deg_sorted" for i in np.arange(0,6)]] = sorted_spreadlist

    data.to_csv(outcsv, index=False)

