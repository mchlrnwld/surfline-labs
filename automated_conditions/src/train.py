from tools.process_data import process_data, generate_dataset_fromspectra, generate_loader, generate_loader_finetuning
from torch.utils.data import DataLoader
from tools.dataloaders import CustomDataset
from tools.nn_architectures import MLP, MLP_subregionembedding
from sklearn.preprocessing import StandardScaler, MinMaxScaler
import pandas as pd
from yaml import load, Loader
import numpy as np
import pickle
import torch
from torch import nn
from tools.loops import train_model

import argparse
import os

def run_training_adam(modelyaml, trainfile, testfile, outfolder, run_on_cuda, fromdf=False):

    #create model folder
    os.makedirs(outfolder, exist_ok=True)

    #open yaml file
    with open(modelyaml,'r') as f:
        params = load(f, Loader=Loader)

    #define scaler
    scaler = params["scaler"]
    assert scaler in ["standard", "minmax"], "only implemented standard or minmax scaler"
    if scaler == "standard":
        scaler = StandardScaler()
    else:
        scaler = MinMaxScaler()


    ###########################
    ###### TRAINING DATA ######
    ###########################

    train_loader = generate_loader(trainfile, scaler, params, outfolder, trainmode=True, fromdf=fromdf)

    ###########################
    ##### VALIDATION DATA #####
    ###########################

    test_loader = generate_loader(testfile, scaler, params, outfolder, trainmode=False, fromdf=fromdf)

    ###########################

    #cuda yes/no
    if not torch.cuda.is_available():
        use_cuda=False
    else:
        use_cuda = run_on_cuda

    #define model architecture

    inputsize = params["inputsize"]

    if params["use_subregion"]:
        model = MLP_subregionembedding(in_features=inputsize, 
                out_features=params["out_features"], 
                hidden=params["hidden"],
                dropout=params["dropout"], 
                embed_len=params["embed_len"], 
                embed_dim=params["embed_dim"])
    else:
        model = MLP(in_features=inputsize, 
                    out_features=params["out_features"], 
                    hidden=params["hidden"],
                    dropout=params["dropout"], 
                    lastlayer=params["lastlayer"])
    
    #define loss function
    criterion = nn.MSELoss(reduction="mean")

    #define optimizer
    optimizer = torch.optim.Adam(model.parameters(), lr=params["lr"])
    
    #define LR scheduler
    scheduler = torch.optim.lr_scheduler.CosineAnnealingWarmRestarts(optimizer, int(params["lr_schedule_rate"]))

    #run training
    modeldict = train_model(train_loader, 
                            test_loader, 
                            model, 
                            criterion, 
                            optimizer, 
                            params, 
                            outfolder, 
                            scheduler=scheduler, 
                            use_cuda=use_cuda, 
                            use_subregion=params["use_subregion"])

    #add file paths to dictionary
    modeldict["modelyaml"] = modelyaml
    modeldict["trainfile"] = trainfile
    modeldict["testfile"] = testfile

    return modeldict

def run_training(modelyaml, trainfile, testfile, outfolder, run_on_cuda, fromdf=False):

    #create model folder
    os.makedirs(outfolder, exist_ok=True)

    #open yaml file
    with open(modelyaml,'r') as f:
        params = load(f, Loader=Loader)

    #define scaler
    scaler = params["scaler"]
    assert scaler in ["standard", "minmax"], "only implemented standard or minmax scaler"
    if scaler == "standard":
        scaler = StandardScaler()
    else:
        scaler = MinMaxScaler()


    ###########################
    ###### TRAINING DATA ######
    ###########################

    train_loader = generate_loader(trainfile, scaler, params, outfolder, trainmode=True, fromdf=fromdf)

    ###########################
    ##### VALIDATION DATA #####
    ###########################

    test_loader = generate_loader(testfile, scaler, params, outfolder, trainmode=False, fromdf=fromdf)

    ###########################

    #cuda yes/no
    if not torch.cuda.is_available():
        use_cuda=False
    else:
        use_cuda = run_on_cuda

    #define model architecture

    inputsize = params["inputsize"]

    if params["use_subregion"]:
        model = MLP_subregionembedding(in_features=inputsize, 
                out_features=params["out_features"], 
                hidden=params["hidden"],
                dropout=params["dropout"], 
                embed_len=params["embed_len"], 
                embed_dim=params["embed_dim"])
    else:
        model = MLP(in_features=inputsize, 
                    out_features=params["out_features"], 
                    hidden=params["hidden"],
                    dropout=params["dropout"], 
                    lastlayer=params["lastlayer"])
    
    #define loss function
    criterion = nn.MSELoss(reduction="mean")

    #define optimizer
    optimizer = torch.optim.RMSprop(model.parameters(), lr=params["lr"])
    
    #define LR scheduler
    scheduler = torch.optim.lr_scheduler.CosineAnnealingWarmRestarts(optimizer, int(params["lr_schedule_rate"]))

    #run training
    modeldict = train_model(train_loader, 
                            test_loader, 
                            model, 
                            criterion, 
                            optimizer, 
                            params, 
                            outfolder, 
                            scheduler=scheduler, 
                            use_cuda=use_cuda, 
                            use_subregion=params["use_subregion"])

    #add file paths to dictionary
    modeldict["modelyaml"] = modelyaml
    modeldict["trainfile"] = trainfile
    modeldict["testfile"] = testfile

    return modeldict

def run_finetuning(modelyaml, trainfile, testfile, outfolder, run_on_cuda, fromdf=False):

    #open yaml file
    with open(modelyaml,'r') as f:
        params = load(f, Loader=Loader)

    if not params["noscaling"]:
        #load scaler
        with open(outfolder+"/scaler.pkl", 'rb') as fp:
            scaler = pickle.load(fp)
    else:
        scaler = None

    train_loader = generate_loader_finetuning(trainfile, scaler, params, fromdf=fromdf)

    test_loader = generate_loader(testfile, scaler, params, outfolder, trainmode=False, fromdf=fromdf)

    #cuda yes/no
    if not torch.cuda.is_available():
        use_cuda=False
    else:
        use_cuda = run_on_cuda

    inputsize = params["inputsize"]

    if params["use_subregion"]:
        model = MLP_subregionembedding(in_features=inputsize, 
                out_features=params["out_features"], 
                hidden=params["hidden"],
                dropout=params["dropout"], 
                embed_len=params["embed_len"], 
                embed_dim=params["embed_dim"])
    else:
        model = MLP(in_features=inputsize, 
                    out_features=params["out_features"], 
                    hidden=params["hidden"],
                    dropout=params["dropout"], 
                    lastlayer=params["lastlayer"])
    #define loss function
    criterion = nn.MSELoss(reduction="mean")

    #load weights
    modelweight = torch.load(outfolder+"/weights.pth", map_location=torch.device("cpu"))
    model.load_state_dict(modelweight)

    for param in model.parameters():
        param.requires_grad = False

    # print(len(model.layers))

    # model.output.bias.requires_grad = True
    # model.output.weight.requires_grad = True

    # for i, layer in enumerate(model.layers.modules()):
    #     if i < len(model.layers):
    #         for param in layer.parameters():
    #             param.requires_grad = False

    # for param in model.output.parameters():
    #     param.requires_grad = True

    ct = 0
    for child in model.children():
        print(ct, type(child))
        print(child)
        ct += 1
        if ct != 3:
            for param in child.parameters():
                param.requires_grad = True

    #define loss function
    criterion = nn.MSELoss(reduction="mean")

    #define optimizer
    optimizer = torch.optim.Adam(model.parameters(), lr=params["lr"])
    
    #define LR scheduler
    scheduler = torch.optim.lr_scheduler.CosineAnnealingWarmRestarts(optimizer, int(params["lr_schedule_rate"]))

    #run training
    modeldict = train_model(train_loader, 
                            test_loader, 
                            model, 
                            criterion, 
                            optimizer, 
                            params, 
                            outfolder, 
                            finetune=True,
                            scheduler=scheduler, 
                            use_cuda=use_cuda, 
                            use_subregion=params["use_subregion"])

    return modeldict


def parse_args():
    parser = argparse.ArgumentParser(description='Training script for ratings and surf heights models')
    parser.add_argument('--modelyaml', type=str, help='looking up yaml config')
    parser.add_argument('--trainfile', type=str, help='training csv')
    parser.add_argument('--testfile', type=str, help='test csv')
    parser.add_argument('--outfolder', type=str, help='output folder for results')
    parser.add_argument('--cuda', action='store_true', help='run on gpu yes/no')
    arguments = parser.parse_args()
    return arguments


if __name__ == '__main__':

    # get parsed arguments
    args = parse_args()

    #run training
    modeldict = run_training(args.modelyaml, args.trainfile, args.testfile, args.outfolder, args.run_on_cuda)



    