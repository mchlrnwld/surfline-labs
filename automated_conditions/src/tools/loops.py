from tqdm import tqdm
import torch
import os
import json

def train_model(train_loader, test_loader, model, criterion, optimizer, params, outfolder, finetune=False, scheduler=None, use_cuda=False, use_subregion=False):
    """
    Main function to train model
    :param train_loader: pytorch dataloader with training data
    :param test_loader: pytorch dataloader with test data
    :param model: pytorch NN
    :param criterion: loss function for tensors
    :param optimizer: pytorch optimizer
    :param params: dictionary from yaml
    :param outfolder: model folder
    :param scheduler: LR scheduler (optional)
    :param use_cuda: run on cuda yes/no
    :param use_subregion: use subregion embeddings yes/no
    :returns: dictionary with training metrics
    """

    #make model folder
    os.makedirs(outfolder, exist_ok=True)

    #loop through epochs
    train_mean = []
    val_mean = []
    for epoch in range(1, params["n_epochs"]+1):
        
        #id there is a scheduler, print out learning rate every epoch
        if scheduler:
            lr = scheduler.get_last_lr()[0] 
            print('epoch '+str(epoch), "lr: ", lr)

        ###################
        # train the model #
        ###################
        
        train_loss, _ = train_one_epoch(model, 
                                            train_loader, 
                                            criterion, 
                                            optimizer, 
                                            use_cuda=use_cuda,
                                            scheduler=scheduler,
                                            use_subregion=use_subregion)
        
        ######################   
        # validate the model #
        ######################
        
        
        valid_loss, _, _ =  validate_model(model, 
                                            test_loader, 
                                            criterion, 
                                            use_cuda=False, 
                                            m_eval=True,
                                            use_subregion=use_subregion)
        
        #create list of loss values over epochs
        val_mean.append(valid_loss.detach().cpu().numpy().item())
        train_mean.append(train_loss.detach().cpu().numpy().item())
        print(train_loss.detach().cpu().numpy().item(), valid_loss.detach().cpu().numpy().item())

    #extract final train/test losses
    final_loss_train = train_loss.detach().cpu().numpy().item()
    final_loss_test = valid_loss.detach().cpu().numpy().item()
        
    #save model weights
    if finetune:
        weightfile = outfolder+"weights_tuned.pth"
    else:
        weightfile = outfolder+"weights.pth"
    torch.save(model.state_dict(), weightfile)

    #save training metrics as json
    modeldict = {
        "train_loss": train_mean,
        "test_loss": val_mean,
        "final_loss_train": final_loss_train,
        "final_loss_test": final_loss_test,
    }
    if finetune:
        with open(outfolder+"modeldict_finetuned.json", 'w') as f:
            json.dump(modeldict, f, indent=4)
    else:
        with open(outfolder+"modeldict.json", 'w') as f:
            json.dump(modeldict, f, indent=4)

    return modeldict

def train_one_epoch(model, train_loader, criterion, optimizer, use_cuda, scheduler=None, use_subregion=False):
    """
    Function to train one epoch
    :param model: pytorch NN
    :param train_loader: pytorch dataloader with training data
    :param criterion: loss function for tensors
    :param optimizer: pytorch optimizer
    :param scheduler: LR scheduler (optional)
    :param use_cuda: run on cuda yes/no
    :param use_subregion: use subregion embeddings yes/no
    :returns: loss and output variables
    """
    
    #use_cuda yes/no
    if use_cuda:
        model.cuda()
    else:
        model.cpu()

    #loop through dataloader
    model.train()
    train_loss = 0
    for batch_idx, loadeddata in tqdm(enumerate(train_loader), total=len(train_loader)):

        #if we use subregions the loaded variables look different
        if use_subregion:
            #data from loader
            (X, y, subregion) = loadeddata
            #cuda?
            if use_cuda:
                X, y, subregion = X.cuda(), y.cuda(), subregion.cuda()
            #run model
            output = model(X, subregion)
        else:
            #data from loader
            (X, y) = loadeddata
            #cuda?
            if use_cuda:
                X, y = X.cuda(), y.cuda()
            #run model
            output = model(X)

        # calculate loss
        loss = criterion(output.squeeze(), y.squeeze())

        #clear gradients for next train
        optimizer.zero_grad()   

        #backpropagation, compute gradients
        loss.backward()         

        #apply gradients
        optimizer.step()      

        #calculate loss over batch
        train_loss = train_loss + ((1 / (batch_idx + 1)) * (loss.data - train_loss))

    #make scheduler stesp
    if scheduler:
        scheduler.step()
        
    return train_loss, output





def validate_model(model, test_loader, criterion, use_cuda, m_eval=True, use_subregion=False):
    """
    Function to validate the model on test data
    :param model: pytorch NN
    :param test_loader: pytorch dataloader with training data
    :param criterion: loss function for tensors
    :param use_cuda: run on cuda yes/no
    :param m_eval: switch model behaviour to evaluation (remove dropout, backprop, etc)
    :param use_subregion: use subregion embeddings yes/no
    :returns: loss and output variables
    """

    #use_cuda yes/no
    if use_cuda:
        model.cuda()
    else:
        model.cpu()
    
    #put model into evaluation mode
    if m_eval:
        model.eval()

    #loop through dataloader
    outputs = []
    targets = []
    valid_loss = 0
    with torch.no_grad():
        for batch_idx, loadeddata in enumerate(tqdm(test_loader)):

            #if we use subregions the loaded variables look different
            if use_subregion:
                #data from loader
                (X, y, subregion) = loadeddata
                #cuda?
                if use_cuda:
                    X, y, subregion = X.cuda(), y.cuda(), subregion.cuda()
                #run model
                output = model(X, subregion)
            else:
                #data from loader
                (X, y) = loadeddata
                #cuda?
                if use_cuda:
                    X, y = X.cuda(), y.cuda()
                #run model
                output = model(X)

            #calculate loss
            loss = criterion(output.squeeze(), y.squeeze())

            #calculate loss over batch
            valid_loss = valid_loss + ((1 / (batch_idx + 1)) * (loss.data - valid_loss))

            #create list of target values and guesses
            outputs.append(output.detach().cpu().numpy().squeeze())
            targets.append(y.detach().cpu().numpy().squeeze())

    return valid_loss, outputs, targets
