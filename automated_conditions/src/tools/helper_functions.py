import numpy as np
import pandas as pd
import seaborn as sn
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix, f1_score

def plot_cm(pred, true, name='model',
            bin_labels=None, normalize_over='all',
            log_density=False, type='clf', show=True,
            out_file=None):
    """
    Plots confusion matrix and log density map of pred and true.

    Args:
        pred: Array of predicted classification indexes.
        true: Array of ground truth classification indexes.
        name: Specify name of model, will be used to label axes of plots.
        bin_labels: Specify 'names' corresponding to classification indexes
                        to be used as tick labels

    Returns:
        Returns nothing, plots confusion matrix and log density map for inputs
    """
    pred = pred.astype(int)
    true = true.astype(int)

    # Common input problem is 2D array
    if true.ndim > 1:
        true = true.flatten()

    if bin_labels is None:
        bin_labels =[
            "FLAT",
            "VERY_POOR",
            "POOR",
            "POOR_TO_FAIR",
            "FAIR",
            "FAIR_TO_GOOD",
            "GOOD",
            "VERY_GOOD",
            "GOOD_TO_EPIC",
            "EPIC"]
        
        lookup_ratings = {
            "FLAT": 0,
            "VERY_POOR":1,
            "POOR": 2,
            "POOR_TO_FAIR": 3,
            "FAIR": 4,
            "FAIR_TO_GOOD": 5,
            "GOOD": 6,
            "VERY_GOOD": 7,
            "GOOD_TO_EPIC": 8,
            "EPIC": 9}

    cm = confusion_matrix(true, pred, normalize=normalize_over,
                          labels=np.arange(0,len(bin_labels)))
    cm_rounded = cm.round(decimals=2)  # rounded for readable confusion matrix
    cm = pd.DataFrame(cm,
                      index=[i for i in bin_labels],
                      columns=[i for i in bin_labels])
    cm_rounded = pd.DataFrame(cm_rounded,
                              index=[i for i in bin_labels],
                              columns=[i for i in bin_labels])

    # Plot confusion matrix normalized over truth, rounded to 2 decimals
    plt.figure(figsize=(10, 7))
    sn.set(font_scale=1.4)
    sn.heatmap(cm_rounded, annot=True, annot_kws={"size": 10}, cmap="viridis", vmin=0., vmax=1.)
    plt.xlabel(name)
    plt.ylabel('Human')
    if show:
        plt.show()
    if out_file is not None:
        plt.savefig(out_file)
    plt.close()

    if log_density:
        # Plot log density map to see anything strange lost by rounding
        plt.figure(figsize=(10, 7))
        plt.imshow(np.log(cm+1 + 0.00001))
        plt.xlabel(name)
        plt.ylabel('Human')
        if show:
            plt.show()


def get_f1(y_pred, y_test):
    """
    Prints f1 between predicted and true values
    :param y_pred: predicted variables
    :param y_test: true variables
    """
    #calculate score
    score = f1_score(y_test.flatten(), y_pred, average='weighted')

    #print score
    print(f'F1 score: {score}')