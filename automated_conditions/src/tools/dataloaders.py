import numpy as np
import torch
from torch.utils.data import Dataset

class CustomDataset(Dataset):
    def __init__(self, X, y, jitter=None, subregions_cat=None):
        """
        Standard class for loading data
        :param X: input data
        :param y: target variables
        :param jitter: noise factors applied to input data
        :kwargs param subregions_cat: subregion ids vector
        """

        #input
        self.X = X

        #target variables
        self.y = y

        #noise factors
        self.jitter = jitter

        #subregions
        self.subregions_cat = subregions_cat

    def __len__(self):
        return len(self.y)

    def getshape(self):
        return np.shape(self.X)

    def __getitem__(self, index):
        """
        Loads data defined by index
        :param index: index
        """
        #input
        x = self.X[index,:]

        #apply noise
        if self.jitter is not None:
            x = x + np.random.uniform(-1 * np.array(self.jitter).squeeze().astype(float),np.array(self.jitter).squeeze().astype(float))

        #target variable
        y = self.y[index]
        y = np.expand_dims(y, axis=0)

        #subregion id
        if self.subregions_cat:
            subregion = self.subregions_cat[index]
            return torch.tensor(x, dtype=torch.float32), torch.tensor(y, dtype=torch.float32), torch.tensor(subregion, dtype=torch.int)
        else:
            return torch.tensor(x, dtype=torch.float32), torch.tensor(y, dtype=torch.float32)