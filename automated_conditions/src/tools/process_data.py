import pandas as pd
from sklearn.preprocessing import StandardScaler
from skimage.transform import resize
import numpy as np
from torch.utils.data import DataLoader
from torch.utils.data.sampler import WeightedRandomSampler
from tools.dataloaders import CustomDataset
from spectral_refraction_aux_files.approximate_spectra_from_swell_partitions import \
    approximate_spectra_from_swell_partitions
from spectral_refraction_aux_files.swell_partition import SwellPartition
from spectral_refraction_aux_files.lotus_directions import directions
from spectral_refraction_aux_files.lotus_frequencies import frequencies
from spectral_refraction_aux_files.swell_partition import SwellPartition
from spectral_refraction_aux_files.frequency_calculations import (
    create_discrete_frequency_steps,
    create_frequency_distribution,
)
from scipy.interpolate import RectBivariateSpline
from tqdm import tqdm
import pickle
import torch
from collections import Counter
from imblearn.over_sampling import SMOTE
from typing import TypeVar, Generic, Tuple, Union, Optional

npArray = TypeVar("npArray")


def collect_features(df):

    wind = np.array([df["LotusWindSpeed_ms"] / 25, df["LotusWindDir_deg_rel_sin"],
                         df["LotusWindDir_deg_rel_cos"]])

    tidemin = df["port_minheight"].item()
    tidemax = df["port_maxheight"].item()
    tide = (df["tide_level"] - tidemin) / (tidemax - tidemin)
    tide = np.expand_dims(tide, axis=0)

    features = np.concatenate((wind, tide))

    return features

def flip_angle(angle):

    angle = angle+360
    #reduce the angle  
    angle =  angle % 360

    # force it to be the positive remainder, so that 0 <= angle < 360  
    angle = (angle + 360) % 360 

    #force into the minimum absolute value residue class, so that -180 < angle <= 180  
    if (angle > 180):  
        angle = angle - 360
    return angle


def calculate_swellpartitions(df, relative_direction=False):
    hs_part = [df["LotusSigHPart0_mt"],
               df["LotusSigHPart1_mt"],
               df["LotusSigHPart2_mt"],
               df["LotusSigHPart3_mt"],
               df["LotusSigHPart4_mt"],
               df["LotusSigHPart5_mt"]]

    tp_part = [df["LotusTPPart0_sec"],
               df["LotusTPPart1_sec"],
               df["LotusTPPart2_sec"],
               df["LotusTPPart3_sec"],
               df["LotusTPPart4_sec"],
               df["LotusTPPart5_sec"]]

    if relative_direction:
        pdir_part = [flip_angle(df["LotusPDirPart"+swellpart+"_deg_rel"]) for swellpart in ["0", "1", "2", "3", "4", "5"]]
    else:
        pdir_part = [df["LotusPDirPart0_deg"],
                    df["LotusPDirPart1_deg"],
                    df["LotusPDirPart2_deg"],
                    df["LotusPDirPart3_deg"],
                    df["LotusPDirPart4_deg"],
                    df["LotusPDirPart5_deg"]]

    # pdir_part = [get_rel_angles(p - df_sample["spot_offshoredirection_deg"]) + 180 for p in pdir_part]

    dspread_part = [df["LotusSpreadPart0_deg"],
                    df["LotusSpreadPart1_deg"],
                    df["LotusSpreadPart2_deg"],
                    df["LotusSpreadPart3_deg"],
                    df["LotusSpreadPart4_deg"],
                    df["LotusSpreadPart5_deg"]]

    swell_partitions = [
        SwellPartition(height, period, direction, spread)
        for height, period, direction, spread in zip(
            hs_part, tp_part, pdir_part, dspread_part
        )
    ]

    return swell_partitions


def process_data(df: Union[pd.DataFrame, npArray], scaler: StandardScaler, minmax=False, trainmode=False) -> npArray:
    """
    Transforms a dataframe or numpy array into a new numpy array by scaling and filling nan's
    :param df: pandas dataframe or numpy array containing data
    :param scaler: scaler for normalizing/standardizing the data
    :returns: numpy array of transformed data
    """

    # transform dataframe using scaler and convert into numpy array
    if trainmode:
        data = np.array(scaler.fit_transform(df))
    else:
        data = np.array(scaler.transform(df))

    # fill nan's with mean value of scaler
    if minmax:
        data = np.where(np.isnan(data), 0, data)
    else:
        data = np.where(np.isnan(data), scaler.mean_, data)

    return data



def generate_loader_finetuning(data, scaler, params, fromdf=False):
    # load training data and target variables
    if fromdf:
        traindf = data
    else:
        chunks = pd.read_csv(data, chunksize=1000)
        traindf = pd.DataFrame()
        for chunk in chunks:
            traindf = pd.concat([traindf, chunk])

    if params["use_subregion"]:
        train_subregions_cat = traindf["subregions_cat"]
    else:
        train_subregions_cat = None

    # pre-process data, depending on method
    train, train_y = process_inputs(traindf, params, trainmode=True)

    # # oversample target variables yes/no
    # if params["oversampling"]:
    #     sm = SMOTE(random_state=42, k_neighbors=5, n_jobs=1)
    #     train, train_y = sm.fit_resample(train, train_y)

    jitter = None

    if not params["noscaling"]:
        train = scaler.transform(train)

        if params["scaler"] == 'minmax':
            train = np.where(np.isnan(train), 0, train)
        else:
            train = np.where(np.isnan(train), scaler.mean_, train)

        # add noise
        if scaler == "standard":
            jitterfactor = params["jitterfactor"]
            jitter = jitterfactor * np.sqrt(scaler.var_)

    # pytorch dataset/dataloader
    train_dataset = CustomDataset(train, train_y, jitter, subregions_cat=train_subregions_cat)
    batchsize = params["batchsize"]
    train_loader = DataLoader(train_dataset, batch_size=batchsize, shuffle=True, num_workers=0)

    return train_loader


def process_inputs(traindf, params, trainmode):
    with open("./data/ref_matrices_all.pkl", 'rb') as f:
        ref_matrices_dict = pickle.load(f)

    if params["spectramode"] == 'lola_zeroed':
        train, train_y = generate_dataset_fromlolaspectra_zeroed(traindf, params, ref_matrices_dict,
                                                                 trainmode=trainmode)
    elif params["spectramode"] == 'rellola_wtides_noscaling':
        train, train_y = generate_dataset_fromlolaspectrawtides_noscaling(traindf, params, trainmode=trainmode)

    elif params["spectramode"] == 'rellola_wtides':
        train, train_y = generate_dataset_fromlolaspectrawtides(traindf, params, trainmode=trainmode)

    elif params["spectramode"] == 'lolarefmat':
        train, train_y = generate_dataset_fromlolaspectrarefmat(traindf, params, ref_matrices_dict,
                                                                trainmode=trainmode)

    elif params["spectramode"] == 'lolamax':
        train, train_y = generate_dataset_fromlolaspectramax(traindf, params, ref_matrices_dict)

    elif params["spectramode"] == 'lolamean':
        train, train_y = generate_dataset_fromlolaspectramean(traindf, params, ref_matrices_dict)

    elif params["spectramode"] == 'lolasummed':
        train, train_y = generate_dataset_fromlolaspectrasummed(traindf, params, ref_matrices_dict)

    elif params["spectramode"] == 'lola':
        train, train_y = generate_dataset_fromlolaspectraonly(traindf, params, trainmode=trainmode)

    elif params["spectramode"] == 'lolamult':
        train, train_y = generate_dataset_fromlolaspectramult(traindf, params, ref_matrices_dict,
                                                              trainmode=trainmode)

    elif params["spectramode"] == 'lolamultsummed':
        train, train_y = generate_dataset_fromlolaspectramultsummed(traindf, params, ref_matrices_dict)

    elif params["from_spectra"]:
        train, train_y = generate_dataset_fromspectra(traindf, params, ref_matrices_dict,
                                                      use_refmat=params["use_refmat"],
                                                      rel_winddir=params["rel_winddir"],
                                                      add_regionalforecast=params["add_regionalforecast"],
                                                      add_bwh=params["add_bwh"],
                                                      spectramode=params["spectramode"], trainmode=trainmode)

    else:
        train = np.array(traindf.copy()[params["features"]])
        train_y = np.array(traindf.copy()[params["targets"]]).astype(int)

    return train, train_y


def generate_loader_subregions(data, scaler, params, outfolder, trainmode=False, fromdf=False):
    # load training data and target variables
    if fromdf:
        traindf = data
    else:
        chunks = pd.read_csv(data, chunksize=1000)
        traindf = pd.DataFrame()
        for chunk in chunks:
            traindf = pd.concat([traindf, chunk])

    jitter = None

    if trainmode:

        for i, subregion_encoded in enumerate(traindf["region_encoded"].unique()):
            traindf_filt = traindf.loc[traindf["region_encoded"] == subregion_encoded]

            # pre-process data, depending on method
            train, train_y = process_inputs(traindf, params, trainmode)

            # oversample target variables yes/no
            if params["oversampling"]:
                if np.min(traindf_filt["surf_condition_num_5pt"].value_counts()) >= 5:
                    sm = SMOTE(random_state=42, k_neighbors=5, n_jobs=1)
                    train, train_y = sm.fit_resample(train, train_y)

            subregions = np.repeat(subregion_encoded, len(train_y))

            if i == 0:
                X_train = train.astype(float)
                y_train = train_y
                subregions_full = subregions

            else:
                X_train = np.append(X_train, train, axis=0)
                y_train = np.append(y_train, train_y, axis=0)
                subregions_full = np.append(subregions_full, subregions, axis=0)

        train = X_train.astype(float)
        train_y = y_train

    else:

        # pre-process data, depending on method
        train, train_y = process_inputs(traindf, params, trainmode)

    if not params["noscaling"]:
        if trainmode:
            train = scaler.fit_transform(train)
            # save scaler
            with open(outfolder + "/scaler.pkl", 'wb') as fp:
                pickle.dump(scaler, fp)

            # add noise
            if scaler == "standard":
                jitterfactor = params["jitterfactor"]
                jitter = jitterfactor * np.sqrt(scaler.var_)
        else:
            train = scaler.transform(train)

    # if params["scaler"]=='minmax':
    #     train = np.where(np.isnan(train), 0, train)
    # else:
    #     train = np.where(np.isnan(train), scaler.mean_, train)

    if trainmode:

        print(len(subregions_full))
        print(np.unique(subregions_full))

        class_sample_count = np.array(
            [len(np.where(subregions_full == t)[0]) for t in range(27)])
        print(class_sample_count)

        weight = 1. / class_sample_count
        samples_weight = np.array([weight[int(t)] for t in subregions_full])

        samples_weight = torch.from_numpy(samples_weight)
        samples_weight = samples_weight.double()

        sampler = WeightedRandomSampler(
            weights=samples_weight,
            num_samples=len(samples_weight),
            replacement=True)

        # pytorch dataset/dataloader
        train_dataset = CustomDataset(train, train_y, jitter)
        batchsize = params["batchsize"]
        train_loader = DataLoader(train_dataset, batch_size=batchsize, num_workers=0, sampler=sampler)

    else:
        # pytorch dataset/dataloader
        train_dataset = CustomDataset(train, train_y, jitter)
        batchsize = len(train_dataset)
        train_loader = DataLoader(train_dataset, batch_size=batchsize, shuffle=trainmode, num_workers=0)

    return train_loader


def generate_loader_subregions2(data, scaler, params, outfolder, trainmode=False, fromdf=False):
    # load training data and target variables
    if fromdf:
        traindf = data
    else:
        chunks = pd.read_csv(data, chunksize=1000)
        traindf = pd.DataFrame()
        for chunk in chunks:
            traindf = pd.concat([traindf, chunk])

    # pre-process data, depending on method
    train, train_y = process_inputs(traindf, params, trainmode)

    if not params["noscaling"]:
        if trainmode:
            train = scaler.fit_transform(train)
        else:
            train = scaler.transform(train)

        if params["scaler"] == 'minmax':
            train = np.where(np.isnan(train), 0, train)
        else:
            train = np.where(np.isnan(train), scaler.mean_, train)

    regions = traindf["region_encoded"]

    jitter = None
    if trainmode:
        if not params["noscaling"]:
            # save scaler
            with open(outfolder + "/scaler.pkl", 'wb') as fp:
                pickle.dump(scaler, fp)

        # oversample target variables yes/no
        if params["oversampling"]:
            sm = SMOTE(random_state=42, k_neighbors=10, n_jobs=4)
            train_merge, train_y = sm.fit_resample(np.concatenate(train, train_y), regions)
            train = train_merge[:, :-1]
            train_y = train_merge[:, -1].squeeze()

            sm = SMOTE(random_state=42, k_neighbors=10, n_jobs=4)
            train, train_y = sm.fit_resample(train, train_y)

        # add noise
        if not params["noscaling"]:
            if scaler == "standard":
                jitterfactor = params["jitterfactor"]
                jitter = jitterfactor * np.sqrt(scaler.var_)

    # pytorch dataset/dataloader
    train_dataset = CustomDataset(train, train_y, jitter)
    batchsize = len(train_dataset)
    if trainmode:
        batchsize = params["batchsize"]
    train_loader = DataLoader(train_dataset, batch_size=batchsize, shuffle=trainmode, num_workers=0)

    return train_loader


def generate_loader(data, scaler, params, outfolder, trainmode=False, fromdf=False):
    # load training data and target variables
    if fromdf:
        traindf = data
    else:
        chunks = pd.read_csv(data, chunksize=1000)
        traindf = pd.DataFrame()
        for chunk in chunks:
            traindf = pd.concat([traindf, chunk])

    if params["use_subregion"]:
        train_subregions_cat = traindf["subregions_cat"]
    else:
        train_subregions_cat = None

    # pre-process data, depending on method
    train, train_y = process_inputs(traindf, params, trainmode)

    if not params["noscaling"]:
        if trainmode:
            train = scaler.fit_transform(train)
        else:
            train = scaler.transform(train)

        if params["scaler"] == 'minmax':
            train = np.where(np.isnan(train), 0, train)
        else:
            train = np.where(np.isnan(train), scaler.mean_, train)

    jitter = None
    if trainmode:
        # save scaler
        if not params["noscaling"]:
            with open(outfolder + "/scaler.pkl", 'wb') as fp:
                pickle.dump(scaler, fp)

        # oversample target variables yes/no
        if params["oversampling"]:
            sm = SMOTE(random_state=42, k_neighbors=10, n_jobs=4)
            train, train_y = sm.fit_resample(train, train_y)

        # add noise
        if not params["noscaling"]:
            if scaler == "standard":
                jitterfactor = params["jitterfactor"]
                jitter = jitterfactor * np.sqrt(scaler.var_)

    # pytorch dataset/dataloader
    train_dataset = CustomDataset(train, train_y, jitter, subregions_cat=train_subregions_cat)
    batchsize = 1#len(train_dataset)
    if trainmode:
        batchsize = params["batchsize"]
    train_loader = DataLoader(train_dataset, batch_size=batchsize, shuffle=trainmode, num_workers=0)

    return train_loader


def get_rel_angles(angles):
    """
    Normalizes relative angles so that they range between -180 and 180
    :param angles: relative angles
    :returns: corrected relative angles
    """
    # find modulus of 360
    angles = angles % 360

    # if modulus is larger than 180, subtract 360
    angles = np.where(angles > 180, angles - 360, angles)

    return angles


def generate_dataset_fromspectra(df, params, ref_matrices_dict, use_refmat=False, rel_winddir=False,
                                 add_regionalforecast=False, add_bwh=False, spectramode='full', trainmode=False):
    """
    Generates dataset specific to using spectra and refraction matrices
    :param df: dataframe containing all feature variables
    :param ref_matrices_dict: dictionary containing all refraction matrices as values and spot id's as keys
    :param use_refmat: use refraction matrices in feature space true/false
    :param use_refmat: use RELATIVE wind direction in feature space true/false
    :returns: numpy array of transformed data
    """

    assert spectramode in ['full', 'sum', 'meanandvar', 'fullrel', 'full_wflipped'], 'spectramodel is not recognized'

    # specific values for generating spectrum
    minimum_frequency = 0.035
    maximum_frequency = 0.5
    direction_bin_size = 10

    # generate input vectors over all rows in dataframe
    inputvecs = []
    y = []
    for i in tqdm(range(len(df))):
        df_sample = df.iloc[i]

        train_y = df_sample[params["targets"]].item().astype(int)

        # use either relative or absolute wind direction
        if rel_winddir:

            if spectramode == 'meanandvar':
                wind = np.array([df_sample["LotusWindSpeed_ms"], df_sample["LotusWindDir_deg_rel_cos"]])
            else:
                wind = np.array([df_sample["LotusWindSpeed_ms"], df_sample["LotusWindDir_deg_rel_sin"],
                                 df_sample["LotusWindDir_deg_rel_cos"]])
        else:
            wind = np.array(
                [df_sample["LotusWindSpeed_ms"], df_sample["LotusWindDir_deg_sin"], df_sample["LotusWindDir_deg_cos"]])

        # collect swell partition data

        hs_part = [df_sample["LotusSigHPart0_mt"],
                   df_sample["LotusSigHPart1_mt"],
                   df_sample["LotusSigHPart2_mt"],
                   df_sample["LotusSigHPart3_mt"],
                   df_sample["LotusSigHPart4_mt"],
                   df_sample["LotusSigHPart5_mt"]]

        tp_part = [df_sample["LotusTPPart0_sec"],
                   df_sample["LotusTPPart1_sec"],
                   df_sample["LotusTPPart2_sec"],
                   df_sample["LotusTPPart3_sec"],
                   df_sample["LotusTPPart4_sec"],
                   df_sample["LotusTPPart5_sec"]]

        pdir_part = [df_sample["LotusPDirPart0_deg"],
                     df_sample["LotusPDirPart1_deg"],
                     df_sample["LotusPDirPart2_deg"],
                     df_sample["LotusPDirPart3_deg"],
                     df_sample["LotusPDirPart4_deg"],
                     df_sample["LotusPDirPart5_deg"]]

        # if we don't use refraction matrices, we'll use the RELATIVE spectra to offshore direction
        if not use_refmat:
            pdir_part = [get_rel_angles(p - df_sample["spot_offshoredirection_deg"]) + 180 for p in pdir_part]

        if spectramode == 'fullrel':
            pdir_part = [get_rel_angles(p - df_sample["spot_offshoredirection_deg"]) + 180 for p in pdir_part]

        dspread_part = [df_sample["LotusSpreadPart0_deg"],
                        df_sample["LotusSpreadPart1_deg"],
                        df_sample["LotusSpreadPart2_deg"],
                        df_sample["LotusSpreadPart3_deg"],
                        df_sample["LotusSpreadPart4_deg"],
                        df_sample["LotusSpreadPart5_deg"]]

        swell_partitions = [
            SwellPartition(height, period, direction, spread)
            for height, period, direction, spread in zip(
                hs_part, tp_part, pdir_part, dspread_part
            )
        ]

        # calculate spectra from swell partitions
        valid_spectra_wave_energy = approximate_spectra_from_swell_partitions(
            swell_partitions=swell_partitions,
            minimum_frequency=minimum_frequency,
            maximum_frequency=maximum_frequency,
            direction_bin_size=direction_bin_size)

        valid_spectra_wave_energy_sum = np.sum(valid_spectra_wave_energy, axis=1).squeeze()
        valid_spectra_wave_energy_var = np.var(valid_spectra_wave_energy, axis=1).squeeze()
        valid_spectra_wave_energy_mean = np.mean(valid_spectra_wave_energy, axis=1).squeeze()

        spectra = valid_spectra_wave_energy.flatten()
        spectra_flipped = valid_spectra_wave_energy[:, ::-1].flatten()

        if spectramode == 'sum':
            spectra = valid_spectra_wave_energy_sum
        elif spectramode == 'meanandvar':
            spectra = np.concatenate((valid_spectra_wave_energy_mean, valid_spectra_wave_energy_var))

        ref_matrix = ref_matrices_dict[df_sample["SpotId"]]

        offshoredirection = df_sample["spot_offshoredirection_deg"].item()
        offshorebin = int(np.round(offshoredirection / 15))

        if offshorebin == 0:
            ref_matrix_before = ref_matrix[:, offshorebin + 12:]
            ref_matrix_after = ref_matrix[:, :offshorebin + 12:]

        elif offshoredirection < 180:
            ref_matrix_before_a = ref_matrix[:, :offshorebin]
            ref_matrix_before_b = ref_matrix[:, offshorebin + 12:]
            ref_matrix_after = ref_matrix[:, offshorebin:offshorebin + 12]
            ref_matrix_before = np.concatenate((ref_matrix_before_b, ref_matrix_before_a), axis=1)

        else:
            ref_matrix_before = ref_matrix[:, offshorebin - 12:offshorebin]
            ref_matrix_after_a = ref_matrix[:, offshorebin:]
            ref_matrix_after_b = ref_matrix[:, :offshorebin - 12]
            ref_matrix_after = np.concatenate((ref_matrix_after_a, ref_matrix_after_b), axis=1)

        ref_matrix_rel = np.concatenate((ref_matrix_before, ref_matrix_after), axis=1)
        ref_matrix_rel_flipped = ref_matrix_rel[:, ::-1]

        # if we use refraction matrices, expand feature vector
        if use_refmat:

            if spectramode == 'fullrel':

                inputvec = np.concatenate((wind, ref_matrix_rel.flatten(), spectra))
                inputvec_flipped = np.concatenate(
                    (wind * np.array([1, -1, 1]), ref_matrix_rel_flipped.flatten(), spectra_flipped))

            else:

                ref_matrix = ref_matrices_dict[df_sample["SpotId"]].flatten()
                inputvec = np.concatenate((wind, ref_matrix.flatten(), spectra))

        else:
            inputvec = np.concatenate((wind, spectra)).squeeze()
            if spectramode != "meanandvar":
                inputvec_flipped = np.concatenate((wind * np.array([1, -1, 1]), spectra_flipped)).squeeze()

        if add_regionalforecast:
            inputvec = np.concatenate((inputvec, np.array([df_sample["forecast_24h_num_5pt"]])))
            inputvec_flipped = np.concatenate((inputvec_flipped, np.array([df_sample["forecast_24h_num_5pt"]])))

        if add_bwh:
            bwh = np.array([df_sample["LotusMinBWH_mt"], df_sample["LotusMaxBWH_mt"]])
            inputvec = np.concatenate((inputvec, bwh)).squeeze()
            inputvec_flipped = np.concatenate((inputvec_flipped, bwh)).squeeze()

        inputvecs.append(inputvec)
        y.append(train_y)

        if spectramode == 'fullrel':
            if trainmode:
                inputvecs.append(inputvec_flipped)
                y.append(train_y)
        elif spectramode == 'full_wflipped':
            if trainmode:
                inputvecs.append(inputvec_flipped)
                y.append(train_y)

    inputvecs = np.array(inputvecs)
    y = np.array(y)

    return inputvecs, y


def generate_dataset_fromlolaspectrawtides(df, params, trainmode=False):
    # specific values for generating spectrum
    minimum_frequency = 0.035
    maximum_frequency = 0.5
    direction_bin_size = 10

    direction_bin_size2 = 15
    lola_directions = np.arange(direction_bin_size2, 360 + direction_bin_size2, direction_bin_size2, )
    minimum_frequency2 = 0.0418
    maximum_frequency2 = 0.411
    lola_frequencies = create_frequency_distribution(minimum_frequency2, maximum_frequency2)
    adjusted_directions = np.concatenate([directions[1:], np.array([360])])

    discrete_frequency_steps = create_discrete_frequency_steps(lola_frequencies)
    discrete_frequency_steps = np.tile(discrete_frequency_steps, (24, 1)).T
    discrete_frequency_steps = np.array(discrete_frequency_steps)

    # generate input vectors over all rows in dataframe
    inputvecs = []
    y = []
    for i in range(len(df)):
        df_sample = df.iloc[i]

        train_y = df_sample[params["targets"]].item().astype(int)

        wind = np.array([df_sample["LotusWindSpeed_ms"], df_sample["LotusWindDir_deg_rel_sin"],
                         df_sample["LotusWindDir_deg_rel_cos"]])

        tidemin = df_sample["port_minheight"].item()
        tidemax = df_sample["port_maxheight"].item()
        tide = (df_sample["tide_level"] - tidemin) / (tidemax - tidemin)
        tide = np.expand_dims(tide, axis=0)

        # collect swell partition data
        swell_partitions = calculate_swellpartitions(df_sample)

        # calculate spectra from swell partitions
        valid_spectra_wave_energy = approximate_spectra_from_swell_partitions(
            swell_partitions=swell_partitions,
            minimum_frequency=minimum_frequency,
            maximum_frequency=maximum_frequency,
            direction_bin_size=direction_bin_size)

        adjusted_spectra_wave_energy = np.concatenate(
            (valid_spectra_wave_energy[:, 1:], valid_spectra_wave_energy[:, 0][:, None]), axis=1, )
        spline_interpolation = RectBivariateSpline(frequencies, adjusted_directions, adjusted_spectra_wave_energy)
        lola_spectra = spline_interpolation(lola_frequencies, lola_directions)
        lola_spectra[lola_spectra < 0] = 0

        # ref_matrix = ref_matrices_dict[df_sample["SpotId"]]

        # def_ref_matrix_expanded = np.ones((24,25))
        # def_ref_matrix_expanded[:24,:18] = ref_matrix
        # for i in np.arange(18,25):
        #     def_ref_matrix_expanded[:,i] = ref_matrix[:,17]
        # def_ref_matrix_expanded = def_ref_matrix_expanded.T
        # def_ref_matrix_expanded = np.where(def_ref_matrix_expanded>0,1,0)

        transformed_spectra = discrete_frequency_steps * (direction_bin_size2 * np.pi / 180) * lola_spectra

        offshoredirection = df_sample["spot_offshoredirection_deg"].item()
        offshorebin = int(np.round(offshoredirection / 15))

        if offshorebin == 0:
            transformed_spectra_before = transformed_spectra[:, offshorebin + 12:]
            transformed_spectra_after = transformed_spectra[:, :offshorebin + 12]
            # ref_matrix_before = def_ref_matrix_expanded[:,offshorebin+12:]
            # ref_matrix_after = def_ref_matrix_expanded[:,:offshorebin+12]

        elif offshoredirection < 180:
            transformed_spectra_before_a = transformed_spectra[:, :offshorebin]
            transformed_spectra_before_b = transformed_spectra[:, offshorebin + 12:]
            transformed_spectra_after = transformed_spectra[:, offshorebin:offshorebin + 12]
            transformed_spectra_before = np.concatenate((transformed_spectra_before_b, transformed_spectra_before_a),
                                                        axis=1)
            # ref_matrix_before_a = def_ref_matrix_expanded[:,:offshorebin]
            # ref_matrix_before_b = def_ref_matrix_expanded[:,offshorebin+12:]
            # ref_matrix_after = def_ref_matrix_expanded[:,offshorebin:offshorebin+12]
            # ref_matrix_before = np.concatenate((ref_matrix_before_b, ref_matrix_before_a), axis=1)

        else:
            transformed_spectra_before = transformed_spectra[:, offshorebin - 12:offshorebin]
            transformed_spectra_after_a = transformed_spectra[:, offshorebin:]
            transformed_spectra_after_b = transformed_spectra[:, :offshorebin - 12]
            transformed_spectra_after = np.concatenate((transformed_spectra_after_a, transformed_spectra_after_b),
                                                       axis=1)
            # ref_matrix_before = def_ref_matrix_expanded[:,offshorebin-12:offshorebin]
            # ref_matrix_after_a = def_ref_matrix_expanded[:,offshorebin:]
            # ref_matrix_after_b = def_ref_matrix_expanded[:,:offshorebin-12]
            # ref_matrix_after = np.concatenate((ref_matrix_after_a, ref_matrix_after_b), axis=1)

        transformed_spectra_rel = np.concatenate((transformed_spectra_before, transformed_spectra_after), axis=1)
        transformed_spectra_rel_flipped = transformed_spectra_rel[:, ::-1]
        # ref_matrix_rel = np.concatenate((ref_matrix_before, ref_matrix_after), axis=1)
        # ref_matrix_rel_flipped = ref_matrix_rel[:,::-1]

        inputvec = np.concatenate((wind, transformed_spectra_rel.flatten(), tide))
        inputvec_flipped = np.concatenate(
            (wind * np.array([1, -1, 1]), transformed_spectra_rel_flipped.flatten(), tide))

        inputvecs.append(inputvec)
        y.append(train_y)

        if trainmode:
            inputvecs.append(inputvec_flipped)
            y.append(train_y)

    inputvecs = np.array(inputvecs)
    y = np.array(y)

    return inputvecs, y


def generate_dataset_fromlolaspectrawtides_noscaling(df, params, trainmode=False):
    # specific values for generating spectrum
    minimum_frequency = 0.035
    maximum_frequency = 0.5
    direction_bin_size = 10

    direction_bin_size2 = 15
    lola_directions = np.arange(direction_bin_size2, 360 + direction_bin_size2, direction_bin_size2, )
    minimum_frequency2 = 0.0418
    maximum_frequency2 = 0.411
    lola_frequencies = create_frequency_distribution(minimum_frequency2, maximum_frequency2)
    adjusted_directions = np.concatenate([directions[1:], np.array([360])])

    discrete_frequency_steps = create_discrete_frequency_steps(lola_frequencies)
    discrete_frequency_steps = np.tile(discrete_frequency_steps, (24, 1)).T
    discrete_frequency_steps = np.array(discrete_frequency_steps)

    # generate input vectors over all rows in dataframe
    inputvecs = []
    y = []
    for i in range(len(df)):
        df_sample = df.iloc[i]

        train_y = df_sample[params["targets"]].item().astype(int)

        wind = np.array([df_sample["LotusWindSpeed_ms"] / 25, df_sample["LotusWindDir_deg_rel_sin"],
                         df_sample["LotusWindDir_deg_rel_cos"]])
        wind_flipped = wind * [1, -1, 1]

        wind[1] = (wind[1] + 1) / 2
        wind[2] = (wind[2] + 1) / 2
        wind_flipped[1] = (wind_flipped[1] + 1) / 2
        wind_flipped[2] = (wind_flipped[2] + 1) / 2

        tidemin = df_sample["port_minheight"].item()
        tidemax = df_sample["port_maxheight"].item()
        tide = (df_sample["tide_level"] - tidemin) / (tidemax - tidemin)
        tide = np.expand_dims(tide, axis=0)

        # collect swell partition data
        swell_partitions = calculate_swellpartitions(df_sample)

        # calculate spectra from swell partitions
        valid_spectra_wave_energy = approximate_spectra_from_swell_partitions(
            swell_partitions=swell_partitions,
            minimum_frequency=minimum_frequency,
            maximum_frequency=maximum_frequency,
            direction_bin_size=direction_bin_size)

        adjusted_spectra_wave_energy = np.concatenate(
            (valid_spectra_wave_energy[:, 1:], valid_spectra_wave_energy[:, 0][:, None]), axis=1, )
        spline_interpolation = RectBivariateSpline(frequencies, adjusted_directions, adjusted_spectra_wave_energy)
        lola_spectra = spline_interpolation(lola_frequencies, lola_directions)
        lola_spectra[lola_spectra < 0] = 0

        # ref_matrix = ref_matrices_dict[df_sample["SpotId"]]

        # def_ref_matrix_expanded = np.ones((24,25))
        # def_ref_matrix_expanded[:24,:18] = ref_matrix
        # for i in np.arange(18,25):
        #     def_ref_matrix_expanded[:,i] = ref_matrix[:,17]
        # def_ref_matrix_expanded = def_ref_matrix_expanded.T
        # def_ref_matrix_expanded = np.where(def_ref_matrix_expanded>0,1,0)

        # transformed_spectra = discrete_frequency_steps * (direction_bin_size2 * np.pi / 180) * lola_spectra
        transformed_spectra = lola_spectra

        offshoredirection = df_sample["spot_offshoredirection_deg"].item()
        offshorebin = int(np.round(offshoredirection / 15))

        if offshorebin == 0:
            transformed_spectra_before = transformed_spectra[:, offshorebin + 12:]
            transformed_spectra_after = transformed_spectra[:, :offshorebin + 12]
            # ref_matrix_before = def_ref_matrix_expanded[:,offshorebin+12:]
            # ref_matrix_after = def_ref_matrix_expanded[:,:offshorebin+12]

        elif offshoredirection < 180:
            transformed_spectra_before_a = transformed_spectra[:, :offshorebin]
            transformed_spectra_before_b = transformed_spectra[:, offshorebin + 12:]
            transformed_spectra_after = transformed_spectra[:, offshorebin:offshorebin + 12]
            transformed_spectra_before = np.concatenate((transformed_spectra_before_b, transformed_spectra_before_a),
                                                        axis=1)
            # ref_matrix_before_a = def_ref_matrix_expanded[:,:offshorebin]
            # ref_matrix_before_b = def_ref_matrix_expanded[:,offshorebin+12:]
            # ref_matrix_after = def_ref_matrix_expanded[:,offshorebin:offshorebin+12]
            # ref_matrix_before = np.concatenate((ref_matrix_before_b, ref_matrix_before_a), axis=1)

        else:
            transformed_spectra_before = transformed_spectra[:, offshorebin - 12:offshorebin]
            transformed_spectra_after_a = transformed_spectra[:, offshorebin:]
            transformed_spectra_after_b = transformed_spectra[:, :offshorebin - 12]
            transformed_spectra_after = np.concatenate((transformed_spectra_after_a, transformed_spectra_after_b),
                                                       axis=1)
            # ref_matrix_before = def_ref_matrix_expanded[:,offshorebin-12:offshorebin]
            # ref_matrix_after_a = def_ref_matrix_expanded[:,offshorebin:]
            # ref_matrix_after_b = def_ref_matrix_expanded[:,:offshorebin-12]
            # ref_matrix_after = np.concatenate((ref_matrix_after_a, ref_matrix_after_b), axis=1)

        transformed_spectra_rel = np.concatenate((transformed_spectra_before, transformed_spectra_after), axis=1)
        transformed_spectra_rel = transformed_spectra_rel / 20
        transformed_spectra_rel_flipped = transformed_spectra_rel[:, ::-1]
        # ref_matrix_rel = np.concatenate((ref_matrix_before, ref_matrix_after), axis=1)
        # ref_matrix_rel_flipped = ref_matrix_rel[:,::-1]

        inputvec = np.concatenate((wind, transformed_spectra_rel.flatten(), tide))
        inputvec_flipped = np.concatenate(
            (wind_flipped, transformed_spectra_rel_flipped.flatten(), tide))

        inputvecs.append(inputvec)
        y.append(train_y)

        if trainmode:
            inputvecs.append(inputvec_flipped)
            y.append(train_y)

    inputvecs = np.array(inputvecs)
    y = np.array(y)

    return inputvecs, y


def generate_dataset_fromlolaspectraonly(df, params, trainmode=False):
    # specific values for generating spectrum
    minimum_frequency = 0.035
    maximum_frequency = 0.5
    direction_bin_size = 10

    direction_bin_size2 = 15
    lola_directions = np.arange(direction_bin_size2, 360 + direction_bin_size2, direction_bin_size2, )
    minimum_frequency2 = 0.0418
    maximum_frequency2 = 0.411
    lola_frequencies = create_frequency_distribution(minimum_frequency2, maximum_frequency2)
    adjusted_directions = np.concatenate([directions[1:], np.array([360])])

    discrete_frequency_steps = create_discrete_frequency_steps(lola_frequencies)
    discrete_frequency_steps = np.tile(discrete_frequency_steps, (24, 1)).T
    discrete_frequency_steps = np.array(discrete_frequency_steps)

    # generate input vectors over all rows in dataframe
    inputvecs = []
    y = []
    for i in range(len(df)):
        df_sample = df.iloc[i]

        train_y = df_sample[params["targets"]].item().astype(int)

        wind = np.array([df_sample["LotusWindSpeed_ms"], df_sample["LotusWindDir_deg_rel_sin"],
                         df_sample["LotusWindDir_deg_rel_cos"]])

        # collect swell partition data
        swell_partitions = calculate_swellpartitions(df_sample)

        # calculate spectra from swell partitions
        valid_spectra_wave_energy = approximate_spectra_from_swell_partitions(
            swell_partitions=swell_partitions,
            minimum_frequency=minimum_frequency,
            maximum_frequency=maximum_frequency,
            direction_bin_size=direction_bin_size)

        adjusted_spectra_wave_energy = np.concatenate(
            (valid_spectra_wave_energy[:, 1:], valid_spectra_wave_energy[:, 0][:, None]), axis=1, )
        spline_interpolation = RectBivariateSpline(frequencies, adjusted_directions, adjusted_spectra_wave_energy)
        lola_spectra = spline_interpolation(lola_frequencies, lola_directions)
        lola_spectra[lola_spectra < 0] = 0

        # ref_matrix = ref_matrices_dict[df_sample["SpotId"]]

        # def_ref_matrix_expanded = np.ones((24,25))
        # def_ref_matrix_expanded[:24,:18] = ref_matrix
        # for i in np.arange(18,25):
        #     def_ref_matrix_expanded[:,i] = ref_matrix[:,17]
        # def_ref_matrix_expanded = def_ref_matrix_expanded.T
        # def_ref_matrix_expanded = np.where(def_ref_matrix_expanded>0,1,0)

        transformed_spectra = discrete_frequency_steps * (direction_bin_size2 * np.pi / 180) * lola_spectra

        offshoredirection = df_sample["spot_offshoredirection_deg"].item()
        offshorebin = int(np.round(offshoredirection / 15))

        if offshorebin == 0:
            transformed_spectra_before = transformed_spectra[:, offshorebin + 12:]
            transformed_spectra_after = transformed_spectra[:, :offshorebin + 12]
            # ref_matrix_before = def_ref_matrix_expanded[:,offshorebin+12:]
            # ref_matrix_after = def_ref_matrix_expanded[:,:offshorebin+12]

        elif offshoredirection < 180:
            transformed_spectra_before_a = transformed_spectra[:, :offshorebin]
            transformed_spectra_before_b = transformed_spectra[:, offshorebin + 12:]
            transformed_spectra_after = transformed_spectra[:, offshorebin:offshorebin + 12]
            transformed_spectra_before = np.concatenate((transformed_spectra_before_b, transformed_spectra_before_a),
                                                        axis=1)
            # ref_matrix_before_a = def_ref_matrix_expanded[:,:offshorebin]
            # ref_matrix_before_b = def_ref_matrix_expanded[:,offshorebin+12:]
            # ref_matrix_after = def_ref_matrix_expanded[:,offshorebin:offshorebin+12]
            # ref_matrix_before = np.concatenate((ref_matrix_before_b, ref_matrix_before_a), axis=1)

        else:
            transformed_spectra_before = transformed_spectra[:, offshorebin - 12:offshorebin]
            transformed_spectra_after_a = transformed_spectra[:, offshorebin:]
            transformed_spectra_after_b = transformed_spectra[:, :offshorebin - 12]
            transformed_spectra_after = np.concatenate((transformed_spectra_after_a, transformed_spectra_after_b),
                                                       axis=1)
            # ref_matrix_before = def_ref_matrix_expanded[:,offshorebin-12:offshorebin]
            # ref_matrix_after_a = def_ref_matrix_expanded[:,offshorebin:]
            # ref_matrix_after_b = def_ref_matrix_expanded[:,:offshorebin-12]
            # ref_matrix_after = np.concatenate((ref_matrix_after_a, ref_matrix_after_b), axis=1)

        transformed_spectra_rel = np.concatenate((transformed_spectra_before, transformed_spectra_after), axis=1)
        transformed_spectra_rel_flipped = transformed_spectra_rel[:, ::-1]
        # ref_matrix_rel = np.concatenate((ref_matrix_before, ref_matrix_after), axis=1)
        # ref_matrix_rel_flipped = ref_matrix_rel[:,::-1]

        inputvec = np.concatenate((wind, transformed_spectra_rel.flatten()))
        inputvec_flipped = np.concatenate((wind * np.array([1, -1, 1]), transformed_spectra_rel_flipped.flatten()))

        inputvecs.append(inputvec)
        y.append(train_y)

        if trainmode:
            inputvecs.append(inputvec_flipped)
            y.append(train_y)

    inputvecs = np.array(inputvecs)
    y = np.array(y)

    return inputvecs, y


def generate_dataset_fromlolaspectrarefmat(df, params, ref_matrices_dict, trainmode=False):
    # specific values for generating spectrum
    minimum_frequency = 0.035
    maximum_frequency = 0.5
    direction_bin_size = 10

    direction_bin_size2 = 15
    lola_directions = np.arange(direction_bin_size2, 360 + direction_bin_size2, direction_bin_size2, )
    minimum_frequency2 = 0.0418
    maximum_frequency2 = 0.411
    lola_frequencies = create_frequency_distribution(minimum_frequency2, maximum_frequency2)
    adjusted_directions = np.concatenate([directions[1:], np.array([360])])

    discrete_frequency_steps = create_discrete_frequency_steps(lola_frequencies)
    discrete_frequency_steps = np.tile(discrete_frequency_steps, (24, 1)).T
    discrete_frequency_steps = np.array(discrete_frequency_steps)

    # generate input vectors over all rows in dataframe
    inputvecs = []
    y = []
    for i in range(len(df)):
        df_sample = df.iloc[i]

        train_y = df_sample[params["targets"]].item().astype(int)

        wind = np.array([df_sample["LotusWindSpeed_ms"], df_sample["LotusWindDir_deg_rel_sin"],
                         df_sample["LotusWindDir_deg_rel_cos"]])

        # collect swell partition data
        swell_partitions = calculate_swellpartitions(df_sample)

        # calculate spectra from swell partitions
        valid_spectra_wave_energy = approximate_spectra_from_swell_partitions(
            swell_partitions=swell_partitions,
            minimum_frequency=minimum_frequency,
            maximum_frequency=maximum_frequency,
            direction_bin_size=direction_bin_size)

        adjusted_spectra_wave_energy = np.concatenate(
            (valid_spectra_wave_energy[:, 1:], valid_spectra_wave_energy[:, 0][:, None]), axis=1, )
        spline_interpolation = RectBivariateSpline(frequencies, adjusted_directions, adjusted_spectra_wave_energy)
        lola_spectra = spline_interpolation(lola_frequencies, lola_directions)
        lola_spectra[lola_spectra < 0] = 0

        ref_matrix = ref_matrices_dict[df_sample["SpotId"]]

        def_ref_matrix_expanded = np.ones((24, 25))
        def_ref_matrix_expanded[:24, :18] = ref_matrix
        for j in np.arange(18, 25):
            def_ref_matrix_expanded[:, j] = ref_matrix[:, 17]
        def_ref_matrix_expanded = def_ref_matrix_expanded.T
        def_ref_matrix_onesandzeroes = np.where(def_ref_matrix_expanded > 0, 1, 0)

        transformed_spectra = discrete_frequency_steps * (direction_bin_size2 * np.pi / 180) * (
                lola_spectra * def_ref_matrix_onesandzeroes)

        offshoredirection = df_sample["spot_offshoredirection_deg"].item()
        offshorebin = int(np.round(offshoredirection / 15))

        if offshorebin == 0:
            transformed_spectra_before = transformed_spectra[:, offshorebin + 12:]
            transformed_spectra_after = transformed_spectra[:, :offshorebin + 12]
            ref_matrix_before = def_ref_matrix_expanded[:, offshorebin + 12:]
            ref_matrix_after = def_ref_matrix_expanded[:, :offshorebin + 12]

        elif offshoredirection < 180:
            transformed_spectra_before_a = transformed_spectra[:, :offshorebin]
            transformed_spectra_before_b = transformed_spectra[:, offshorebin + 12:]
            transformed_spectra_after = transformed_spectra[:, offshorebin:offshorebin + 12]
            transformed_spectra_before = np.concatenate((transformed_spectra_before_b, transformed_spectra_before_a),
                                                        axis=1)
            ref_matrix_before_a = def_ref_matrix_expanded[:, :offshorebin]
            ref_matrix_before_b = def_ref_matrix_expanded[:, offshorebin + 12:]
            ref_matrix_after = def_ref_matrix_expanded[:, offshorebin:offshorebin + 12]
            ref_matrix_before = np.concatenate((ref_matrix_before_b, ref_matrix_before_a), axis=1)

        else:
            transformed_spectra_before = transformed_spectra[:, offshorebin - 12:offshorebin]
            transformed_spectra_after_a = transformed_spectra[:, offshorebin:]
            transformed_spectra_after_b = transformed_spectra[:, :offshorebin - 12]
            transformed_spectra_after = np.concatenate((transformed_spectra_after_a, transformed_spectra_after_b),
                                                       axis=1)
            ref_matrix_before = def_ref_matrix_expanded[:, offshorebin - 12:offshorebin]
            ref_matrix_after_a = def_ref_matrix_expanded[:, offshorebin:]
            ref_matrix_after_b = def_ref_matrix_expanded[:, :offshorebin - 12]
            ref_matrix_after = np.concatenate((ref_matrix_after_a, ref_matrix_after_b), axis=1)

        transformed_spectra_rel = np.concatenate((transformed_spectra_before, transformed_spectra_after), axis=1)
        transformed_spectra_rel_flipped = transformed_spectra_rel[:, ::-1]
        ref_matrix_rel = np.concatenate((ref_matrix_before, ref_matrix_after), axis=1)
        ref_matrix_rel_flipped = ref_matrix_rel[:, ::-1]

        inputvec = np.concatenate((wind, transformed_spectra_rel.flatten(), ref_matrix_rel.flatten()))
        inputvec_flipped = np.concatenate(
            (wind * np.array([1, -1, 1]), transformed_spectra_rel_flipped.flatten(), ref_matrix_rel_flipped.flatten()))

        inputvecs.append(inputvec)
        y.append(train_y)

        if trainmode:
            inputvecs.append(inputvec_flipped)
            y.append(train_y)

    inputvecs = np.array(inputvecs)
    y = np.array(y)

    return inputvecs, y


def generate_dataset_fromlolaspectra_zeroed(df, params, ref_matrices_dict, trainmode=False):
    # specific values for generating spectrum
    minimum_frequency = 0.035
    maximum_frequency = 0.5
    direction_bin_size = 10

    direction_bin_size2 = 15
    lola_directions = np.arange(direction_bin_size2, 360 + direction_bin_size2, direction_bin_size2, )
    minimum_frequency2 = 0.0418
    maximum_frequency2 = 0.411
    lola_frequencies = create_frequency_distribution(minimum_frequency2, maximum_frequency2)
    adjusted_directions = np.concatenate([directions[1:], np.array([360])])

    discrete_frequency_steps = create_discrete_frequency_steps(lola_frequencies)
    discrete_frequency_steps = np.tile(discrete_frequency_steps, (24, 1)).T
    discrete_frequency_steps = np.array(discrete_frequency_steps)

    # generate input vectors over all rows in dataframe
    inputvecs = []
    y = []
    for i in range(len(df)):
        df_sample = df.iloc[i]

        train_y = df_sample[params["targets"]].item().astype(int)

        wind = np.array([df_sample["LotusWindSpeed_ms"], df_sample["LotusWindDir_deg_rel_sin"],
                         df_sample["LotusWindDir_deg_rel_cos"]])

        # collect swell partition data
        swell_partitions = calculate_swellpartitions(df_sample)

        # calculate spectra from swell partitions
        valid_spectra_wave_energy = approximate_spectra_from_swell_partitions(
            swell_partitions=swell_partitions,
            minimum_frequency=minimum_frequency,
            maximum_frequency=maximum_frequency,
            direction_bin_size=direction_bin_size)

        adjusted_spectra_wave_energy = np.concatenate(
            (valid_spectra_wave_energy[:, 1:], valid_spectra_wave_energy[:, 0][:, None]), axis=1, )
        spline_interpolation = RectBivariateSpline(frequencies, adjusted_directions, adjusted_spectra_wave_energy)
        lola_spectra = spline_interpolation(lola_frequencies, lola_directions)
        lola_spectra[lola_spectra < 0] = 0

        ref_matrix = ref_matrices_dict[df_sample["SpotId"]]

        def_ref_matrix_expanded = np.ones((24, 25))
        def_ref_matrix_expanded[:24, :18] = ref_matrix
        for j in np.arange(18, 25):
            def_ref_matrix_expanded[:, j] = ref_matrix[:, 17]
        def_ref_matrix_expanded = def_ref_matrix_expanded.T
        def_ref_matrix_expanded = np.where(def_ref_matrix_expanded > 0, 1, 0)

        transformed_spectra = discrete_frequency_steps * (direction_bin_size2 * np.pi / 180) * (
                lola_spectra * def_ref_matrix_expanded)

        offshoredirection = df_sample["spot_offshoredirection_deg"].item()

        offshorebin = int(np.round(offshoredirection / 15))

        if offshorebin == 0:
            transformed_spectra_before = transformed_spectra[:, offshorebin + 12:]
            transformed_spectra_after = transformed_spectra[:, :offshorebin + 12]
            # ref_matrix_before = def_ref_matrix_expanded[:,offshorebin+12:]
            # ref_matrix_after = def_ref_matrix_expanded[:,:offshorebin+12]

        elif offshoredirection < 180:
            transformed_spectra_before_a = transformed_spectra[:, :offshorebin]
            transformed_spectra_before_b = transformed_spectra[:, offshorebin + 12:]
            transformed_spectra_after = transformed_spectra[:, offshorebin:offshorebin + 12]
            transformed_spectra_before = np.concatenate((transformed_spectra_before_b, transformed_spectra_before_a),
                                                        axis=1)
            # ref_matrix_before_a = def_ref_matrix_expanded[:,:offshorebin]
            # ref_matrix_before_b = def_ref_matrix_expanded[:,offshorebin+12:]
            # ref_matrix_after = def_ref_matrix_expanded[:,offshorebin:offshorebin+12]
            # ref_matrix_before = np.concatenate((ref_matrix_before_b, ref_matrix_before_a), axis=1)

        else:
            transformed_spectra_before = transformed_spectra[:, offshorebin - 12:offshorebin]
            transformed_spectra_after_a = transformed_spectra[:, offshorebin:]
            transformed_spectra_after_b = transformed_spectra[:, :offshorebin - 12]
            transformed_spectra_after = np.concatenate((transformed_spectra_after_a, transformed_spectra_after_b),
                                                       axis=1)
            # ref_matrix_before = def_ref_matrix_expanded[:,offshorebin-12:offshorebin]
            # ref_matrix_after_a = def_ref_matrix_expanded[:,offshorebin:]
            # ref_matrix_after_b = def_ref_matrix_expanded[:,:offshorebin-12]
            # ref_matrix_after = np.concatenate((ref_matrix_after_a, ref_matrix_after_b), axis=1)

        transformed_spectra_rel = np.concatenate((transformed_spectra_before, transformed_spectra_after), axis=1)
        transformed_spectra_rel_flipped = transformed_spectra_rel[:, ::-1]
        # ref_matrix_rel = np.concatenate((ref_matrix_before, ref_matrix_after), axis=1)
        # ref_matrix_rel_flipped = ref_matrix_rel[:,::-1]

        inputvec = np.concatenate((wind, transformed_spectra_rel.flatten()))
        inputvec_flipped = np.concatenate((wind * np.array([1, -1, 1]), transformed_spectra_rel_flipped.flatten()))

        inputvecs.append(inputvec)
        y.append(train_y)

        if trainmode:
            inputvecs.append(inputvec_flipped)
            y.append(train_y)

    inputvecs = np.array(inputvecs)
    y = np.array(y)

    return inputvecs, y


def generate_dataset_fromlolaspectramult(df, params, ref_matrices_dict, trainmode=False):
    # specific values for generating spectrum
    minimum_frequency = 0.035
    maximum_frequency = 0.5
    direction_bin_size = 10

    direction_bin_size2 = 15
    lola_directions = np.arange(direction_bin_size2, 360 + direction_bin_size2, direction_bin_size2, )
    minimum_frequency2 = 0.0418
    maximum_frequency2 = 0.411
    lola_frequencies = create_frequency_distribution(minimum_frequency2, maximum_frequency2)
    adjusted_directions = np.concatenate([directions[1:], np.array([360])])

    discrete_frequency_steps = create_discrete_frequency_steps(lola_frequencies)
    discrete_frequency_steps = np.tile(discrete_frequency_steps, (24, 1)).T
    discrete_frequency_steps = np.array(discrete_frequency_steps)

    # generate input vectors over all rows in dataframe
    inputvecs = []
    y = []
    for i in range(len(df)):
        df_sample = df.iloc[i]

        train_y = df_sample[params["targets"]].item().astype(int)

        wind = np.array([df_sample["LotusWindSpeed_ms"], df_sample["LotusWindDir_deg_rel_sin"],
                         df_sample["LotusWindDir_deg_rel_cos"]])

        # collect swell partition data
        swell_partitions = calculate_swellpartitions(df_sample)

        # calculate spectra from swell partitions
        valid_spectra_wave_energy = approximate_spectra_from_swell_partitions(
            swell_partitions=swell_partitions,
            minimum_frequency=minimum_frequency,
            maximum_frequency=maximum_frequency,
            direction_bin_size=direction_bin_size)

        adjusted_spectra_wave_energy = np.concatenate(
            (valid_spectra_wave_energy[:, 1:], valid_spectra_wave_energy[:, 0][:, None]), axis=1, )
        spline_interpolation = RectBivariateSpline(frequencies, adjusted_directions, adjusted_spectra_wave_energy)
        lola_spectra = spline_interpolation(lola_frequencies, lola_directions)
        lola_spectra[lola_spectra < 0] = 0

        ref_matrix = ref_matrices_dict[df_sample["SpotId"]]

        def_ref_matrix_expanded = np.ones((24, 25))
        def_ref_matrix_expanded[:24, :18] = ref_matrix
        for j in np.arange(18, 25):
            def_ref_matrix_expanded[:, j] = ref_matrix[:, 17]
        def_ref_matrix_expanded = def_ref_matrix_expanded.T

        transformed_spectra = discrete_frequency_steps * (direction_bin_size2 * np.pi / 180) * (
                lola_spectra * def_ref_matrix_expanded)

        offshoredirection = df_sample["spot_offshoredirection_deg"].item()

        offshorebin = int(np.round(offshoredirection / 15))

        if offshorebin == 0:
            transformed_spectra_before = transformed_spectra[:, offshorebin + 12:]
            transformed_spectra_after = transformed_spectra[:, :offshorebin + 12]
            # ref_matrix_before = def_ref_matrix_expanded[:,offshorebin+12:]
            # ref_matrix_after = def_ref_matrix_expanded[:,:offshorebin+12]

        elif offshoredirection < 180:
            transformed_spectra_before_a = transformed_spectra[:, :offshorebin]
            transformed_spectra_before_b = transformed_spectra[:, offshorebin + 12:]
            transformed_spectra_after = transformed_spectra[:, offshorebin:offshorebin + 12]
            transformed_spectra_before = np.concatenate((transformed_spectra_before_b, transformed_spectra_before_a),
                                                        axis=1)
            # ref_matrix_before_a = def_ref_matrix_expanded[:,:offshorebin]
            # ref_matrix_before_b = def_ref_matrix_expanded[:,offshorebin+12:]
            # ref_matrix_after = def_ref_matrix_expanded[:,offshorebin:offshorebin+12]
            # ref_matrix_before = np.concatenate((ref_matrix_before_b, ref_matrix_before_a), axis=1)

        else:
            transformed_spectra_before = transformed_spectra[:, offshorebin - 12:offshorebin]
            transformed_spectra_after_a = transformed_spectra[:, offshorebin:]
            transformed_spectra_after_b = transformed_spectra[:, :offshorebin - 12]
            transformed_spectra_after = np.concatenate((transformed_spectra_after_a, transformed_spectra_after_b),
                                                       axis=1)
            # ref_matrix_before = def_ref_matrix_expanded[:,offshorebin-12:offshorebin]
            # ref_matrix_after_a = def_ref_matrix_expanded[:,offshorebin:]
            # ref_matrix_after_b = def_ref_matrix_expanded[:,:offshorebin-12]
            # ref_matrix_after = np.concatenate((ref_matrix_after_a, ref_matrix_after_b), axis=1)

        transformed_spectra_rel = np.concatenate((transformed_spectra_before, transformed_spectra_after), axis=1)
        transformed_spectra_rel_flipped = transformed_spectra_rel[:, ::-1]
        # ref_matrix_rel = np.concatenate((ref_matrix_before, ref_matrix_after), axis=1)
        # ref_matrix_rel_flipped = ref_matrix_rel[:,::-1]

        inputvec = np.concatenate((wind, transformed_spectra_rel.flatten()))
        inputvec_flipped = np.concatenate((wind * np.array([1, -1, 1]), transformed_spectra_rel_flipped.flatten()))

        inputvecs.append(inputvec)
        y.append(train_y)

        if trainmode:
            inputvecs.append(inputvec_flipped)
            y.append(train_y)

    inputvecs = np.array(inputvecs)
    y = np.array(y)

    return inputvecs, y


def generate_dataset_fromlolaspectrasummed(df, params, ref_matrices_dict):
    # specific values for generating spectrum
    minimum_frequency = 0.035
    maximum_frequency = 0.5
    direction_bin_size = 10

    direction_bin_size2 = 15
    lola_directions = np.arange(direction_bin_size2, 360 + direction_bin_size2, direction_bin_size2, )
    minimum_frequency2 = 0.0418
    maximum_frequency2 = 0.411
    lola_frequencies = create_frequency_distribution(minimum_frequency2, maximum_frequency2)
    adjusted_directions = np.concatenate([directions[1:], np.array([360])])

    discrete_frequency_steps = create_discrete_frequency_steps(lola_frequencies)
    discrete_frequency_steps = np.tile(discrete_frequency_steps, (24, 1)).T
    discrete_frequency_steps = np.array(discrete_frequency_steps)

    # generate input vectors over all rows in dataframe
    inputvecs = []
    y = []
    for i in range(len(df)):
        df_sample = df.iloc[i]

        train_y = df_sample[params["targets"]].item().astype(int)

        wind = np.array([df_sample["LotusWindSpeed_ms"], df_sample["LotusWindDir_deg_rel_cos"]])

        # collect swell partition data
        swell_partitions = calculate_swellpartitions(df_sample)

        # calculate spectra from swell partitions
        valid_spectra_wave_energy = approximate_spectra_from_swell_partitions(
            swell_partitions=swell_partitions,
            minimum_frequency=minimum_frequency,
            maximum_frequency=maximum_frequency,
            direction_bin_size=direction_bin_size)

        adjusted_spectra_wave_energy = np.concatenate(
            (valid_spectra_wave_energy[:, 1:], valid_spectra_wave_energy[:, 0][:, None]), axis=1, )
        spline_interpolation = RectBivariateSpline(frequencies, adjusted_directions, adjusted_spectra_wave_energy)
        lola_spectra = spline_interpolation(lola_frequencies, lola_directions)
        lola_spectra[lola_spectra < 0] = 0

        ref_matrix = ref_matrices_dict[df_sample["SpotId"]]

        def_ref_matrix_expanded = np.ones((24, 25))
        def_ref_matrix_expanded[:24, :18] = ref_matrix
        for j in np.arange(18, 25):
            def_ref_matrix_expanded[:, j] = ref_matrix[:, 17]
        def_ref_matrix_expanded = def_ref_matrix_expanded.T

        def_ref_matrix_expanded = np.where(def_ref_matrix_expanded > 0, 1, 0)

        transformed_spectra = discrete_frequency_steps * (direction_bin_size2 * np.pi / 180) * (
                lola_spectra * def_ref_matrix_expanded)

        transformed_spectra_var = np.var(transformed_spectra, axis=1).squeeze()
        transformed_spectra_mean = np.mean(transformed_spectra, axis=1).squeeze()

        inputvec = np.concatenate((wind, transformed_spectra_var.flatten(), transformed_spectra_mean.flatten()))

        inputvecs.append(inputvec)
        y.append(train_y)

    inputvecs = np.array(inputvecs)
    y = np.array(y)

    return inputvecs, y


def generate_dataset_fromlolaspectramax(df, params, ref_matrices_dict):
    # specific values for generating spectrum
    minimum_frequency = 0.035
    maximum_frequency = 0.5
    direction_bin_size = 10

    direction_bin_size2 = 15
    lola_directions = np.arange(direction_bin_size2, 360 + direction_bin_size2, direction_bin_size2, )
    minimum_frequency2 = 0.0418
    maximum_frequency2 = 0.411
    lola_frequencies = create_frequency_distribution(minimum_frequency2, maximum_frequency2)
    adjusted_directions = np.concatenate([directions[1:], np.array([360])])

    discrete_frequency_steps = create_discrete_frequency_steps(lola_frequencies)
    discrete_frequency_steps = np.tile(discrete_frequency_steps, (24, 1)).T
    discrete_frequency_steps = np.array(discrete_frequency_steps)

    # generate input vectors over all rows in dataframe
    inputvecs = []
    y = []
    for i in range(len(df)):
        df_sample = df.iloc[i]

        train_y = df_sample[params["targets"]].item().astype(int)

        wind = np.array([df_sample["LotusWindSpeed_ms"], df_sample["LotusWindDir_deg_rel_cos"]])

        # collect swell partition data
        swell_partitions = calculate_swellpartitions(df_sample)

        # calculate spectra from swell partitions
        valid_spectra_wave_energy = approximate_spectra_from_swell_partitions(
            swell_partitions=swell_partitions,
            minimum_frequency=minimum_frequency,
            maximum_frequency=maximum_frequency,
            direction_bin_size=direction_bin_size)

        adjusted_spectra_wave_energy = np.concatenate(
            (valid_spectra_wave_energy[:, 1:], valid_spectra_wave_energy[:, 0][:, None]), axis=1, )
        spline_interpolation = RectBivariateSpline(frequencies, adjusted_directions, adjusted_spectra_wave_energy)
        lola_spectra = spline_interpolation(lola_frequencies, lola_directions)
        lola_spectra[lola_spectra < 0] = 0

        ref_matrix = ref_matrices_dict[df_sample["SpotId"]]

        def_ref_matrix_expanded = np.ones((24, 25))
        def_ref_matrix_expanded[:24, :18] = ref_matrix
        for j in np.arange(18, 25):
            def_ref_matrix_expanded[:, j] = ref_matrix[:, 17]
        def_ref_matrix_expanded = def_ref_matrix_expanded.T

        def_ref_matrix_expanded = np.where(def_ref_matrix_expanded > 0, 1, 0)

        transformed_spectra = discrete_frequency_steps * (direction_bin_size2 * np.pi / 180) * (
                lola_spectra * def_ref_matrix_expanded)

        transformed_spectra_max = np.max(transformed_spectra, axis=1).squeeze()

        inputvec = np.concatenate((wind, transformed_spectra_max.flatten()))

        inputvecs.append(inputvec)
        y.append(train_y)

    inputvecs = np.array(inputvecs)
    y = np.array(y)

    return inputvecs, y


def generate_dataset_fromlolaspectramean(df, params, ref_matrices_dict):
    # specific values for generating spectrum
    minimum_frequency = 0.035
    maximum_frequency = 0.5
    direction_bin_size = 10

    direction_bin_size2 = 15
    lola_directions = np.arange(direction_bin_size2, 360 + direction_bin_size2, direction_bin_size2, )
    minimum_frequency2 = 0.0418
    maximum_frequency2 = 0.411
    lola_frequencies = create_frequency_distribution(minimum_frequency2, maximum_frequency2)
    adjusted_directions = np.concatenate([directions[1:], np.array([360])])

    discrete_frequency_steps = create_discrete_frequency_steps(lola_frequencies)
    discrete_frequency_steps = np.tile(discrete_frequency_steps, (24, 1)).T
    discrete_frequency_steps = np.array(discrete_frequency_steps)

    # generate input vectors over all rows in dataframe
    inputvecs = []
    y = []
    for i in range(len(df)):
        df_sample = df.iloc[i]

        train_y = df_sample[params["targets"]].item().astype(int)

        wind = np.array([df_sample["LotusWindSpeed_ms"], df_sample["LotusWindDir_deg_rel_cos"]])

        # collect swell partition data
        swell_partitions = calculate_swellpartitions(df_sample)

        # calculate spectra from swell partitions
        valid_spectra_wave_energy = approximate_spectra_from_swell_partitions(
            swell_partitions=swell_partitions,
            minimum_frequency=minimum_frequency,
            maximum_frequency=maximum_frequency,
            direction_bin_size=direction_bin_size)

        adjusted_spectra_wave_energy = np.concatenate(
            (valid_spectra_wave_energy[:, 1:], valid_spectra_wave_energy[:, 0][:, None]), axis=1, )
        spline_interpolation = RectBivariateSpline(frequencies, adjusted_directions, adjusted_spectra_wave_energy)
        lola_spectra = spline_interpolation(lola_frequencies, lola_directions)
        lola_spectra[lola_spectra < 0] = 0

        ref_matrix = ref_matrices_dict[df_sample["SpotId"]]

        def_ref_matrix_expanded = np.ones((24, 25))
        def_ref_matrix_expanded[:24, :18] = ref_matrix
        for j in np.arange(18, 25):
            def_ref_matrix_expanded[:, j] = ref_matrix[:, 17]
        def_ref_matrix_expanded = def_ref_matrix_expanded.T

        def_ref_matrix_expanded = np.where(def_ref_matrix_expanded > 0, 1, 0)

        transformed_spectra = discrete_frequency_steps * (direction_bin_size2 * np.pi / 180) * (
                lola_spectra * def_ref_matrix_expanded)

        transformed_spectra_mean = np.mean(transformed_spectra, axis=1).squeeze()

        inputvec = np.concatenate((wind, transformed_spectra_mean.flatten()))

        inputvecs.append(inputvec)
        y.append(train_y)

    inputvecs = np.array(inputvecs)
    y = np.array(y)

    return inputvecs, y


def generate_dataset_fromlolaspectramultsummed(df, params, ref_matrices_dict):
    # specific values for generating spectrum
    minimum_frequency = 0.035
    maximum_frequency = 0.5
    direction_bin_size = 10

    direction_bin_size2 = 15
    lola_directions = np.arange(direction_bin_size2, 360 + direction_bin_size2, direction_bin_size2, )
    minimum_frequency2 = 0.0418
    maximum_frequency2 = 0.411
    lola_frequencies = create_frequency_distribution(minimum_frequency2, maximum_frequency2)
    adjusted_directions = np.concatenate([directions[1:], np.array([360])])

    discrete_frequency_steps = create_discrete_frequency_steps(lola_frequencies)
    discrete_frequency_steps = np.tile(discrete_frequency_steps, (24, 1)).T
    discrete_frequency_steps = np.array(discrete_frequency_steps)

    # generate input vectors over all rows in dataframe
    inputvecs = []
    y = []
    for i in range(len(df)):
        df_sample = df.iloc[i]

        train_y = df_sample[params["targets"]].item().astype(int)

        wind = np.array([df_sample["LotusWindSpeed_ms"], df_sample["LotusWindDir_deg_rel_cos"]])

        # collect swell partition data
        swell_partitions = calculate_swellpartitions(df_sample)

        # calculate spectra from swell partitions
        valid_spectra_wave_energy = approximate_spectra_from_swell_partitions(
            swell_partitions=swell_partitions,
            minimum_frequency=minimum_frequency,
            maximum_frequency=maximum_frequency,
            direction_bin_size=direction_bin_size)

        adjusted_spectra_wave_energy = np.concatenate(
            (valid_spectra_wave_energy[:, 1:], valid_spectra_wave_energy[:, 0][:, None]), axis=1, )
        spline_interpolation = RectBivariateSpline(frequencies, adjusted_directions, adjusted_spectra_wave_energy)
        lola_spectra = spline_interpolation(lola_frequencies, lola_directions)
        lola_spectra[lola_spectra < 0] = 0

        ref_matrix = ref_matrices_dict[df_sample["SpotId"]]

        def_ref_matrix_expanded = np.ones((24, 25))
        def_ref_matrix_expanded[:24, :18] = ref_matrix
        for j in np.arange(18, 25):
            def_ref_matrix_expanded[:, j] = ref_matrix[:, 17]
        def_ref_matrix_expanded = def_ref_matrix_expanded.T

        transformed_spectra = discrete_frequency_steps * (direction_bin_size2 * np.pi / 180) * (
                lola_spectra * def_ref_matrix_expanded)

        transformed_spectra_var = np.var(transformed_spectra, axis=1).squeeze()
        transformed_spectra_mean = np.mean(transformed_spectra, axis=1).squeeze()

        inputvec = np.concatenate((wind, transformed_spectra_var.flatten(), transformed_spectra_mean.flatten()))

        inputvecs.append(inputvec)
        y.append(train_y)

    inputvecs = np.array(inputvecs)
    y = np.array(y)

    return inputvecs, y


def generate_dataset_fromlolaspectrarefmat_2d(df, params, ref_matrices_dict, trainmode=False):
    # specific values for generating spectrum
    minimum_frequency = 0.035
    maximum_frequency = 0.5
    direction_bin_size = 10

    direction_bin_size2 = 15
    lola_directions = np.arange(direction_bin_size2, 360 + direction_bin_size2, direction_bin_size2, )
    minimum_frequency2 = 0.0418
    maximum_frequency2 = 0.411
    lola_frequencies = create_frequency_distribution(minimum_frequency2, maximum_frequency2)
    adjusted_directions = np.concatenate([directions[1:], np.array([360])])

    discrete_frequency_steps = create_discrete_frequency_steps(lola_frequencies)
    discrete_frequency_steps = np.tile(discrete_frequency_steps, (24, 1)).T
    discrete_frequency_steps = np.array(discrete_frequency_steps)

    # generate input vectors over all rows in dataframe
    inputvecs = []
    y = []
    winds = []
    for i in range(len(df)):
        df_sample = df.iloc[i]

        train_y = df_sample[params["targets"]].item().astype(int)

        wind = np.array([df_sample["LotusWindSpeed_ms"], df_sample["LotusWindDir_deg_rel_sin"],
                         df_sample["LotusWindDir_deg_rel_cos"]])

        # collect swell partition data
        swell_partitions = calculate_swellpartitions(df_sample)

        # calculate spectra from swell partitions
        valid_spectra_wave_energy = approximate_spectra_from_swell_partitions(
            swell_partitions=swell_partitions,
            minimum_frequency=minimum_frequency,
            maximum_frequency=maximum_frequency,
            direction_bin_size=direction_bin_size)

        adjusted_spectra_wave_energy = np.concatenate(
            (valid_spectra_wave_energy[:, 1:], valid_spectra_wave_energy[:, 0][:, None]), axis=1, )
        spline_interpolation = RectBivariateSpline(frequencies, adjusted_directions, adjusted_spectra_wave_energy)
        lola_spectra = spline_interpolation(lola_frequencies, lola_directions)
        lola_spectra[lola_spectra < 0] = 0

        ref_matrix = ref_matrices_dict[df_sample["SpotId"]]

        def_ref_matrix_expanded = np.ones((24, 25))
        def_ref_matrix_expanded[:24, :18] = ref_matrix
        for j in np.arange(18, 25):
            def_ref_matrix_expanded[:, j] = ref_matrix[:, 17]
        def_ref_matrix_expanded = def_ref_matrix_expanded.T
        # def_ref_matrix_expanded = np.where(def_ref_matrix_expanded>0,1,0)

        transformed_spectra = discrete_frequency_steps * (direction_bin_size2 * np.pi / 180) * lola_spectra

        offshoredirection = df_sample["spot_offshoredirection_deg"].item()
        offshorebin = int(np.round(offshoredirection / 15))

        if offshorebin == 0:
            transformed_spectra_before = transformed_spectra[:, offshorebin + 12:]
            transformed_spectra_after = transformed_spectra[:, :offshorebin + 12]
            ref_matrix_before = def_ref_matrix_expanded[:, offshorebin + 12:]
            ref_matrix_after = def_ref_matrix_expanded[:, :offshorebin + 12]

        elif offshoredirection < 180:
            transformed_spectra_before_a = transformed_spectra[:, :offshorebin]
            transformed_spectra_before_b = transformed_spectra[:, offshorebin + 12:]
            transformed_spectra_after = transformed_spectra[:, offshorebin:offshorebin + 12]
            transformed_spectra_before = np.concatenate((transformed_spectra_before_b, transformed_spectra_before_a),
                                                        axis=1)
            ref_matrix_before_a = def_ref_matrix_expanded[:, :offshorebin]
            ref_matrix_before_b = def_ref_matrix_expanded[:, offshorebin + 12:]
            ref_matrix_after = def_ref_matrix_expanded[:, offshorebin:offshorebin + 12]
            ref_matrix_before = np.concatenate((ref_matrix_before_b, ref_matrix_before_a), axis=1)

        else:
            transformed_spectra_before = transformed_spectra[:, offshorebin - 12:offshorebin]
            transformed_spectra_after_a = transformed_spectra[:, offshorebin:]
            transformed_spectra_after_b = transformed_spectra[:, :offshorebin - 12]
            transformed_spectra_after = np.concatenate((transformed_spectra_after_a, transformed_spectra_after_b),
                                                       axis=1)
            ref_matrix_before = def_ref_matrix_expanded[:, offshorebin - 12:offshorebin]
            ref_matrix_after_a = def_ref_matrix_expanded[:, offshorebin:]
            ref_matrix_after_b = def_ref_matrix_expanded[:, :offshorebin - 12]
            ref_matrix_after = np.concatenate((ref_matrix_after_a, ref_matrix_after_b), axis=1)

        transformed_spectra_rel = np.concatenate((transformed_spectra_before, transformed_spectra_after), axis=1)

        transformed_spectra_rel = resize(transformed_spectra_rel, (64, 64))
        transformed_spectra_rel_flipped = transformed_spectra_rel[:, ::-1]

        ref_matrix_rel = np.concatenate((ref_matrix_before, ref_matrix_after), axis=1)
        ref_matrix_rel = resize(ref_matrix_rel, (64, 64))
        ref_matrix_rel_flipped = ref_matrix_rel[:, ::-1]

        transformed_spectra_rel = np.expand_dims(transformed_spectra_rel, axis=0)
        transformed_spectra_rel_flipped = np.expand_dims(transformed_spectra_rel_flipped, axis=0)

        ref_matrix_rel = np.expand_dims(ref_matrix_rel, axis=0)
        ref_matrix_rel_flipped = np.expand_dims(ref_matrix_rel_flipped, axis=0)

        inputvec = np.concatenate((transformed_spectra_rel, ref_matrix_rel), axis=0)
        inputvec_flipped = np.concatenate((transformed_spectra_rel_flipped, ref_matrix_rel_flipped), axis=0)

        inputvecs.append(inputvec)
        y.append(train_y)
        winds.append(wind)

        if trainmode:
            inputvecs.append(inputvec_flipped)
            y.append(train_y)
            winds.append(wind * np.array([1, -1, 1]))

    inputvecs = np.array(inputvecs)
    y = np.array(y)
    winds = np.array(winds)

    return inputvecs, winds, y
