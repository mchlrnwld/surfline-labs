import torch
import torch.nn as nn


class MLP_minimal(nn.Module):

    def __init__(self, in_features=5, out_features=1, depth=1, hidden=5):
        """
        Class for a simple MLP, using same number of neurons in each layer
        :param in_features: number of features in input
        :param out_features: number of features to estimate
        :param depth: how many hidden layers 
        """
        super().__init__()

        #number of hidden layers
        self.depth = depth

        #first layer
        self.input = nn.Linear(in_features=in_features, out_features=hidden)

        #hidden layer to hidden layer
        self.fc = nn.Linear(in_features=hidden, out_features=hidden)

        #last layer
        self.output = nn.Linear(in_features=hidden, out_features=out_features)

    def forward(self, X):
        """
        Run model on input
        :param X: feature vector
        :returns: estimated variable
        """

        #first layer
        X = nn.LeakyReLU()(self.input(X))

        #possibly more layers
        for f in range(self.depth-1):
            X = nn.LeakyReLU()(self.fc(X))

        #output layer
        pred = self.output(X)

        return pred

    
class MLP(nn.Module):

    def __init__(self, in_features=51, out_features=1, hidden=1024, lastlayer=8, dropout=None):
        """
        Class for a MLP, halving the number of neurons in every layer, until number is smaller than 32
        :param in_features: number of features in input
        :param out_features: number of features to estimate
        :param hidden: number of neurons in first hidden layer
        :param dropout: optional dropout rate
        """
        super().__init__()

        self.lastlayer = lastlayer

        #dropout layer
        self.dropout = dropout
        if self.dropout:
            self.dropoutlayer = nn.Dropout(dropout)
           
        #number of neurons in first hidden layer
        self.hidden = hidden
        
        #define number of neurons in all hidden layers
        j = []
        hid = hidden
        while hid > self.lastlayer:
            hid = hid/2
            j.append(int(hid)) 
        self.j = j

        #first layer
        self.fc1 = nn.Linear(in_features=in_features, out_features=hidden)
        
        #subsequent layers
        layers = []
        for j in self.j:
            layers.append(nn.Linear(in_features=j*2, out_features=j))
        self.layers = torch.nn.ModuleList(layers)
            
        self.output = nn.Linear(in_features=self.lastlayer, out_features=out_features)

    def forward(self, X):
        """
        Run model on input
        :param X: feature vector
        :returns: estimated variable
        """
        #first layer
        vector = nn.LeakyReLU()(self.fc1(X))
        if self.dropout:
            vector = self.dropoutlayer(vector)

        #subsequent layers 
        for l in self.layers:
            vector = nn.LeakyReLU()(l(vector))
            #optional dropout layer
            if self.dropout:
                vector = self.dropoutlayer(vector)
                               
        #output layer
        pred = self.output(vector)

        return pred
    
    
    
    
class MLP_subregionembedding(nn.Module):

    def __init__(self, in_features=51, out_features=1, hidden=1024, dropout=None, embed_len=59, embed_dim=8):
        """
        Class for a MLP, halving the number of neurons in every layer, until number is smaller than 32
        uses subregions as embedded features
        :param in_features: number of features in input
        :param out_features: number of features to estimate
        :param hidden: number of neurons in first hidden layer
        :param embed_len: number of unique subregions
        :param embed_len: dimension of embedding layer
        """
        super().__init__()

        #dropout layer
        self.dropout = dropout
        if self.dropout:
            self.dropoutlayer = nn.Dropout(dropout)
        
        #subregion embedding
        self.embed = nn.Embedding(embed_len, embed_dim)
           
        #number of neurons in first hidden layer
        self.hidden = hidden
        
        #total number of input features: embedding vector and input vector
        in_features = in_features+embed_dim

        #define number of neurons in all hidden layers
        j = []
        hid = hidden
        while hid > 32:
            hid = hid/2
            j.append(int(hid)) 
        self.j = j

        #first layer
        self.fc1 = nn.Linear(in_features=in_features, out_features=hidden)

        #subsequent layers
        layers = []
        for j in self.j:
            layers.append(nn.Linear(in_features=j*2, out_features=j))
        self.layers = torch.nn.ModuleList(layers)
            
        #last layer
        self.output = nn.Linear(in_features=32, out_features=out_features)

    def forward(self, X, subregion):
        """
        Run model on input
        :param X: feature vector
        :param subregion: subregion number
        :returns: estimated variable
        """
        
        #load subregion-specific embedding vector
        subregion_embedded = self.embed(subregion)
        
        #merger feature vector with embedding vector
        vector = torch.cat([subregion_embedded, X], dim=-1)

        #first layer
        vector = nn.LeakyReLU()(self.fc1(vector))
        if self.dropout:
            vector = self.dropoutlayer(vector)

        #subsequent layers      
        for l in self.layers:
            vector = nn.LeakyReLU()(l(vector))
            if self.dropout:
                vector = self.dropoutlayer(vector)
                               
        # Output layer
        pred = self.output(vector)

        return pred
    