import numpy as np
from tqdm import tqdm
import matplotlib.pyplot as plt
import scipy
import seaborn as sn
from sklearn.metrics import confusion_matrix, f1_score
import pandas as pd
import os
from typing import TypeVar, Generic, Tuple, Union, Optional
npArray = TypeVar("npArray")


def smooth_timeseries(vals:npArray, maxchange:float = 0.32) -> npArray:
    """
    Transforms a numpy array of sequential 1h ratings into a new numpy array by smoothing
    :param vals: numpy array containing floating point ratings
    :param maxchange: floating point value, defines maximum change to ignore change in ratings
    :returns: numpy array of smoothed data
    """

    # calculate change in values
    valschange = np.diff(vals, n=1, prepend=vals[0])
    valschangeabs = abs(valschange)
    valsprev = np.concatenate(([vals[0]],vals[:-1]))

    # calculate change in ratings
    valsint = np.round(vals).astype(int)
    valschangeint = np.diff(valsint, n=1, prepend=valsint[0])

    a = vals.copy()

    counter = 0

    #while loop:
    # - no change in ratings with absolute change smaller than maxchange
    while len(a[((valschangeint != 0) & (valschangeabs < maxchange))]) > 0:

        counter = counter + 1
        if counter == 100:
            return a

        a = np.where((valschangeint != 0) & (valschangeabs < maxchange), valsprev, a)

        # recalculate changes in array
        valschange = np.diff(a, n=1, prepend=a[0])
        valschangeabs = abs(valschange)
        valsprev = np.concatenate(([a[0]],a[:-1]))
        valsint = np.round(a).astype(int)
        valschangeint = np.diff(valsint, n=1, prepend=valsint[0])

    return a







# def smooth_timeseries(vals:npArray, maxchange:float = 0.25) -> npArray:
#     """
#     Transforms a numpy array of sequential 1h ratings into a new numpy array by smoothing
#     :param vals: numpy array containing floating point ratings
#     :param maxchange: floating point value, defines maximum change to ignore change in ratings
#     :returns: numpy array of smoothed data
#     """

#     # calculate change in values
#     valschange = np.diff(vals, n=1, prepend=vals[0])
#     valschangeabs = abs(valschange)
#     valsprev = np.concatenate(([vals[0]],vals[:-1]))
#     valsafter = np.concatenate((vals[1:],[vals[-1]]))
#     valsprevint = np.round(valsprev).astype(int)
#     valsafterint = np.round(valsafter).astype(int)

#     # calculate change in ratings
#     valsint = np.round(vals).astype(int)
#     valschangeint = np.diff(valsint, n=1, prepend=valsint[0])
#     valschangeintabs = abs(valschangeint)

#     a = vals.copy()

#     counter = 0

#     #while loop:
#     # - no change in ratings with absolute change smaller than maxchange
#     # - no jumping forth and back in two sucessive steps
#     # - no jumps larger or equal than 2 ratings
#     while (len(a[((valschangeint != 0) & (valschangeabs < maxchange))]) > 0) or (len(a[(valsprevint == valsafterint) & (valsprevint != valsint)]) > 0) or (len(a[(valschangeintabs >= 2)]) > 0):

#         counter = counter + 1
#         if counter == 50:
#             return a

#         # test for jumps larger or equal than 2 ratings
#         # if present, lower change by 50%
#         a = np.where((valschangeintabs > 1), valsprev + (valschange * 0.5), a)

#         # recalculate changes in array
#         valschange = np.diff(a, n=1, prepend=a[0])
#         valschangeabs = abs(valschange)
#         valsprev = np.concatenate(([a[0]],a[:-1]))
#         valsafter = np.concatenate((a[1:],[a[-1]]))
#         valsint = np.round(a).astype(int)
#         valschangeint = np.diff(valsint, n=1, prepend=valsint[0])
#         valschangeintabs = abs(valschangeint)
#         valsprevint = np.round(valsprev).astype(int)
#         valsafterint = np.round(valsafter).astype(int)

#         a = np.where((valschangeint != 0) & (valschangeabs < maxchange), valsprev, a)

#         # recalculate changes in array
#         valschange = np.diff(a, n=1, prepend=a[0])
#         valschangeabs = abs(valschange)
#         valsprev = np.concatenate(([a[0]],a[:-1]))
#         valsafter = np.concatenate((a[1:],[a[-1]]))
#         valsint = np.round(a).astype(int)
#         valschangeint = np.diff(valsint, n=1, prepend=valsint[0])
#         valschangeintabs = abs(valschangeint)
#         valsprevint = np.round(valsprev).astype(int)
#         valsafterint = np.round(valsafter).astype(int)

#         a = np.where((valsprevint == valsafterint) & (valsprevint != valsint), valsprev, a)

#         # recalculate changes in array
#         valschange = np.diff(a, n=1, prepend=a[0])
#         valschangeabs = abs(valschange)
#         valsprev = np.concatenate(([a[0]],a[:-1]))
#         valsafter = np.concatenate((a[1:],[a[-1]]))
#         valsint = np.round(a).astype(int)
#         valschangeint = np.diff(valsint, n=1, prepend=valsint[0])
#         valschangeintabs = abs(valschangeint)
#         valsprevint = np.round(valsprev).astype(int)
#         valsafterint = np.round(valsafter).astype(int)

#     return a


def calculate_temporal_smoothness(values):

    diff = []
    diff2 = []
    for i in range(len(values)-2):
        diff.append(abs(np.round(values[i+1]) - np.round(values[i])))
        diff2.append(abs(np.round(values[i+2] - np.round(values[i]))))

    c = [a == 1 and b == 0 for a, b in zip(diff, diff2)]
    forthbackbyone = np.count_nonzero(c)

    c = [a >= 2 and b == 0 for a, b in zip(diff, diff2)]
    fortbackbytwoormore = np.count_nonzero(c)

    jumpbytwoormore = (abs(np.diff(np.round(values))) > 1 ).sum()

    averagechange = np.mean(abs(np.diff(np.round(values))))

    return forthbackbyone, fortbackbytwoormore, jumpbytwoormore, averagechange


def temporal_smoothness(values):

    print('Dataset size: ', len(values))

    diff = []
    diff2 = []
    for i in range(len(values)-2):
        diff.append(abs(np.round(values[i+1]) - np.round(values[i])))
        diff2.append(abs(np.round(values[i+2] - np.round(values[i]))))

    c = [a == 1 and b == 0 for a, b in zip(diff, diff2)]
    print("Jumping forth and back by 1:, ", np.count_nonzero(c))

    c = [a >= 2 and b == 0 for a, b in zip(diff, diff2)]
    print("Jumping forth and back by >= 2:, ", np.count_nonzero(c))

    largejumps = (abs(np.diff(np.round(values))) > 1 ).sum()
    print("Jumping by >=2: ", largejumps)

    averagechange = np.mean(abs(np.diff(np.round(values))))
    print("Average change per timestep: ", averagechange)


reduced_class_names = ['FLAT_TO_VPOOR', 'POOR', 'POOR_TO_FAIR', 'FAIR', 'FAIR_TO_GOOD+']

def run_spotreports(testdf, traindf, modelfolder):

    s = []
    ids = []
    f1nn = []
    f124h = []
    l = []
    ltrain = []
    pnn = []
    p24h = []
    mae = []
    mae24h = []
    correct = []
    correct24h = []

    os.makedirs(modelfolder+"/results/", exist_ok=True)
    os.makedirs(modelfolder+"/results/SpotAnalysis_ConfMatrix/", exist_ok=True)
    os.makedirs(modelfolder+"/results/SpotAnalysis_ReportDistributions/", exist_ok=True)

    for spotname in tqdm(testdf["SpotName"].unique()):

        test_filt = testdf.copy()
        test_filt = test_filt.loc[test_filt["SpotName"] == spotname]

        if len(test_filt) > 1:

            testid = test_filt["SpotId"].unique()

            train_filt = traindf.copy()
            train_filt = train_filt.loc[train_filt["SpotName"] == spotname]

            outputsval_nn = np.round(np.array(test_filt["nn"])).squeeze().astype(int)

            outputsval_nn = np.where(outputsval_nn<0,0,outputsval_nn)
            outputsval_nn = np.where(outputsval_nn>=4,4,outputsval_nn)

            targetsval = np.array(test_filt["surf_condition_num_5pt"]).squeeze().astype(int)

            outputsval = test_filt["forecast_24h_num_5pt"]
            
            percentcorrect_nn = (outputsval_nn == targetsval).sum()
            percentcorrect_24 = (outputsval == targetsval).sum()
            correct.append(percentcorrect_nn / len(outputsval_nn) * 100)
            correct24h.append(percentcorrect_24 / len(outputsval) * 100)

            s.append(spotname)
            l.append(len(test_filt))
            ltrain.append(len(train_filt))
            ids.append(testid)
            
            mae.append(np.mean(abs(outputsval_nn-targetsval)))
            mae24h.append(np.mean(abs(outputsval-targetsval)))
            

            f1nn.append(f1_score(targetsval.ravel().astype(int).flatten(), np.round(outputsval_nn), average='weighted'))
            pnn.append(scipy.stats.pearsonr(outputsval_nn, targetsval)[0])
            
            f124h.append(f1_score(targetsval.ravel().astype(int).flatten(), np.round(outputsval), average='weighted'))
            p24h.append(scipy.stats.pearsonr(outputsval, targetsval)[0])

            bin_labels=reduced_class_names
            

            plt.figure(figsize=(21,19))
            ax1 = plt.subplot(2,2,1)
            ax1.set_title('f1: ' + str(np.round(f1_score(targetsval.ravel().astype(int).flatten(), np.round(outputsval_nn), average='weighted'), 2)))
            cm = confusion_matrix(targetsval, np.round(outputsval_nn), normalize='true', labels=np.arange(0,len(bin_labels)))
            cm_rounded = cm.round(decimals=2)
            cm = pd.DataFrame(cm, index=[i for i in bin_labels], columns=[i for i in bin_labels])
            cm_rounded = pd.DataFrame(cm_rounded, index=[i for i in bin_labels], columns=[i for i in bin_labels])
            sn.set(font_scale=1.4)
            sn.heatmap(cm_rounded, annot=True, annot_kws={"size": 10}, cmap="viridis", vmin=0., vmax=1.)
            plt.ylabel('Human')

            ax2 = plt.subplot(2,2,2)
            ax2.set_title('f1: ' + str(np.round(f1_score(targetsval.ravel().astype(int).flatten(), np.round(outputsval), average='weighted'), 2)))
            cm = confusion_matrix(targetsval, np.round(outputsval), normalize='true', labels=np.arange(0,len(bin_labels)))
            cm_rounded = cm.round(decimals=2)
            cm = pd.DataFrame(cm, index=[i for i in bin_labels], columns=[i for i in bin_labels])
            cm_rounded = pd.DataFrame(cm_rounded, index=[i for i in bin_labels], columns=[i for i in bin_labels])
            sn.set(font_scale=1.4)
            sn.heatmap(cm_rounded, annot=True, annot_kws={"size": 10}, cmap="viridis", vmin=0., vmax=1.)

            ax3 = plt.subplot(2,2,3)
            cm = confusion_matrix(targetsval, np.round(outputsval_nn), normalize=None, labels=np.arange(0,len(bin_labels)))
            cm_rounded = cm.round(decimals=3)
            cm = pd.DataFrame(cm, index=[i for i in bin_labels], columns=[i for i in bin_labels])
            cm_rounded = pd.DataFrame(cm_rounded, index=[i for i in bin_labels], columns=[i for i in bin_labels])
            sn.set(font_scale=1.4)
            sn.heatmap(cm_rounded, annot=True, annot_kws={"size": 10}, cmap="viridis", vmin=0., vmax=200., fmt='d')
            plt.xlabel('Neural Network')
            plt.ylabel('Human')

            ax4 = plt.subplot(2,2,4)
            cm = confusion_matrix(targetsval, np.round(outputsval), normalize=None, labels=np.arange(0,len(bin_labels)))
            cm_rounded = cm.round(decimals=3)
            cm = pd.DataFrame(cm, index=[i for i in bin_labels], columns=[i for i in bin_labels])
            cm_rounded = pd.DataFrame(cm_rounded, index=[i for i in bin_labels], columns=[i for i in bin_labels])
            sn.set(font_scale=1.4)
            sn.heatmap(cm_rounded, annot=True, annot_kws={"size": 10}, cmap="viridis", vmin=0., vmax=200., fmt='d')
            plt.xlabel('Human Forecast')

            plt.suptitle(spotname + ", n_train=" + str(len(train_filt)) + ', n_test=' + str(len(test_filt)))

            ax1.set_xticklabels([])
            ax2.set_xticklabels([])
            ax2.set_yticklabels([])
            ax4.set_yticklabels([])

            plt.tight_layout()
            plt.subplots_adjust(top=0.9)
            plt.savefig(modelfolder+"/results/SpotAnalysis_ConfMatrix/"+spotname.replace('/','_')+".png")
            plt.close()
            plt.close('all')

            targetcomplete = np.concatenate((np.array(test_filt["surf_condition_num_5pt"]), np.array(train_filt["surf_condition_num_5pt"])))
            forecastcomplete = np.concatenate((np.array(test_filt["forecast_24h_num_5pt"]), np.array(train_filt["forecast_24h_num_5pt"])))
            
            ratings = ['FLAT_TO_VPOOR','POOR','POOR_TO_FAIR','FAIR','FAIR_TO_GOOD+']
            data = np.concatenate((targetcomplete, forecastcomplete, outputsval_nn))
            labels = ['Human Reports']*len(targetcomplete)
            labels.extend(['Human Forecasts']*len(forecastcomplete))
            labels.extend(['Neural Network']*len(outputsval_nn))
            datadf = pd.DataFrame({"data": data, "method": labels})
            plt.figure()
            ax = sn.displot(data=datadf, x='data', hue='method', 
                            bins=np.arange(-0.5,5.5), 
                            multiple='dodge', 
                            shrink=0.5, 
                            stat='probability', 
                            common_norm=False,
                            aspect=3)
            plt.xticks(rotation=0)
            plt.xlabel('Rating')
            plt.ylabel('Percentage of Reports')
            plt.title(spotname)
            ax.set(xticks=np.arange(0,5))
            ax.set_xticklabels(ratings)
            plt.savefig(modelfolder+"/results/SpotAnalysis_ReportDistributions/"+spotname.replace('/','_')+".png")
            plt.close('all')


    df = pd.DataFrame({"Spot": s, "SpotId": ids, "NoOfReports_test": l, "NoOfReports_train": ltrain, 
                    "MAE-NN": mae, "MAE-24h": mae24h,
                    "correct-NN": correct, "correct-24h": correct24h,
                    "f1-NN": np.round(f1nn, 2), "f1-24h": np.round(f124h, 2), "f1-diff": np.round(np.array(f1nn)-np.array(f124h), 2),
                    "p-NN": np.round(pnn, 2), "p-24h": np.round(p24h, 2), "p-diff": np.round(np.array(pnn)-np.array(p24h), 2)
                    })
    df = df.sort_values(by=['NoOfReports_train'], ascending=False)

    df.to_csv(modelfolder+"/results/spotanalysis.csv", index=False)
    print('done')





def run_spotreports_distributionmatch(testdf, traindf, modelfolder):

    s = []
    ids = []
    f1nn = []
    f124h = []
    l = []
    ltrain = []
    pnn = []
    p24h = []
    mae = []
    mae24h = []
    correct = []
    correct24h = []

    os.makedirs(modelfolder+"/results/", exist_ok=True)
    os.makedirs(modelfolder+"/results/SpotAnalysis_MatchedDist_ConfMatrix/", exist_ok=True)
    os.makedirs(modelfolder+"/results/SpotAnalysis_MatchedDist_ReportDistributions/", exist_ok=True)

    for spotname in tqdm(testdf["SpotName"].unique()):

        test_filt = testdf.copy()
        test_filt = test_filt.loc[test_filt["SpotName"] == spotname]

        if len(test_filt) > 1:

            testid = test_filt["SpotId"].unique()

            train_filt = traindf.copy()
            train_filt = train_filt.loc[train_filt["SpotName"] == spotname]

            if len(train_filt) > 1:

                outputsval_nn = np.array(test_filt["nn"]).squeeze()

                targetsval = np.array(test_filt["surf_condition_num_5pt"]).squeeze().astype(int)

                outputsval = test_filt["forecast_24h_num_5pt"]

                fulldist = np.array(train_filt["surf_condition_num_5pt"]).squeeze().astype(int)

                binwidths = []
                binwidth=0
                for i in range(4):
                    leng = len(train_filt.copy().loc[train_filt["surf_condition_num_5pt"] == i])
                    if leng>0:
                        binwidth = binwidth + (leng / len(train_filt))
                    else:
                        binwidth = binwidth + 1e-12
                    if binwidth>1:
                        binwidth=1

                    binwidths.append(np.quantile(fulldist, binwidth))   
                    
                output_matcheddist = []
                for o in outputsval_nn:
                    if o < binwidths[0]:
                        o = 0.
                    elif o < binwidths[1]:
                        o = 1.
                    elif o < binwidths[2]:
                        o = 2.
                    elif o < binwidths[3]:
                        o = 3.
                    elif o >= binwidths[3]:
                        o = 4.
                    output_matcheddist.append(o)

                outputsval_nn = np.array(output_matcheddist).astype(int)
                
                percentcorrect_nn = (outputsval_nn == targetsval).sum()
                percentcorrect_24 = (outputsval == targetsval).sum()
                correct.append(percentcorrect_nn / len(outputsval_nn) * 100)
                correct24h.append(percentcorrect_24 / len(outputsval) * 100)

                s.append(spotname)
                l.append(len(test_filt))
                ltrain.append(len(train_filt))
                ids.append(testid)
                
                mae.append(np.mean(abs(outputsval_nn-targetsval)))
                mae24h.append(np.mean(abs(outputsval-targetsval)))
                

                f1nn.append(f1_score(targetsval.ravel().astype(int).flatten(), np.round(outputsval_nn), average='weighted'))
                pnn.append(scipy.stats.pearsonr(outputsval_nn, targetsval)[0])
                
                f124h.append(f1_score(targetsval.ravel().astype(int).flatten(), np.round(outputsval), average='weighted'))
                p24h.append(scipy.stats.pearsonr(outputsval, targetsval)[0])

                bin_labels=reduced_class_names
                

                plt.figure(figsize=(21,19))
                ax1 = plt.subplot(2,2,1)
                ax1.set_title('f1: ' + str(np.round(f1_score(targetsval.ravel().astype(int).flatten(), np.round(outputsval_nn), average='weighted'), 2)))
                cm = confusion_matrix(targetsval, np.round(outputsval_nn), normalize='true', labels=np.arange(0,len(bin_labels)))
                cm_rounded = cm.round(decimals=2)
                cm = pd.DataFrame(cm, index=[i for i in bin_labels], columns=[i for i in bin_labels])
                cm_rounded = pd.DataFrame(cm_rounded, index=[i for i in bin_labels], columns=[i for i in bin_labels])
                sn.set(font_scale=1.4)
                sn.heatmap(cm_rounded, annot=True, annot_kws={"size": 10}, cmap="viridis", vmin=0., vmax=1.)
                plt.ylabel('Human')

                ax2 = plt.subplot(2,2,2)
                ax2.set_title('f1: ' + str(np.round(f1_score(targetsval.ravel().astype(int).flatten(), np.round(outputsval), average='weighted'), 2)))
                cm = confusion_matrix(targetsval, np.round(outputsval), normalize='true', labels=np.arange(0,len(bin_labels)))
                cm_rounded = cm.round(decimals=2)
                cm = pd.DataFrame(cm, index=[i for i in bin_labels], columns=[i for i in bin_labels])
                cm_rounded = pd.DataFrame(cm_rounded, index=[i for i in bin_labels], columns=[i for i in bin_labels])
                sn.set(font_scale=1.4)
                sn.heatmap(cm_rounded, annot=True, annot_kws={"size": 10}, cmap="viridis", vmin=0., vmax=1.)

                ax3 = plt.subplot(2,2,3)
                cm = confusion_matrix(targetsval, np.round(outputsval_nn), normalize=None, labels=np.arange(0,len(bin_labels)))
                cm_rounded = cm.round(decimals=3)
                cm = pd.DataFrame(cm, index=[i for i in bin_labels], columns=[i for i in bin_labels])
                cm_rounded = pd.DataFrame(cm_rounded, index=[i for i in bin_labels], columns=[i for i in bin_labels])
                sn.set(font_scale=1.4)
                sn.heatmap(cm_rounded, annot=True, annot_kws={"size": 10}, cmap="viridis", vmin=0., vmax=200., fmt='d')
                plt.xlabel('Neural Network')
                plt.ylabel('Human')

                ax4 = plt.subplot(2,2,4)
                cm = confusion_matrix(targetsval, np.round(outputsval), normalize=None, labels=np.arange(0,len(bin_labels)))
                cm_rounded = cm.round(decimals=3)
                cm = pd.DataFrame(cm, index=[i for i in bin_labels], columns=[i for i in bin_labels])
                cm_rounded = pd.DataFrame(cm_rounded, index=[i for i in bin_labels], columns=[i for i in bin_labels])
                sn.set(font_scale=1.4)
                sn.heatmap(cm_rounded, annot=True, annot_kws={"size": 10}, cmap="viridis", vmin=0., vmax=200., fmt='d')
                plt.xlabel('Human Forecast')

                plt.suptitle(spotname + ", n_train=" + str(len(train_filt)) + ', n_test=' + str(len(test_filt)))

                ax1.set_xticklabels([])
                ax2.set_xticklabels([])
                ax2.set_yticklabels([])
                ax4.set_yticklabels([])

                plt.tight_layout()
                plt.subplots_adjust(top=0.9)
                plt.savefig(modelfolder+"/results/SpotAnalysis_MatchedDist_ConfMatrix/"+spotname.replace('/','_')+".png")
                plt.close()
                plt.close('all')

                targetcomplete = np.concatenate((np.array(test_filt["surf_condition_num_5pt"]), np.array(train_filt["surf_condition_num_5pt"])))
                forecastcomplete = np.concatenate((np.array(test_filt["forecast_24h_num_5pt"]), np.array(train_filt["forecast_24h_num_5pt"])))
                
                ratings = ['FLAT_TO_VPOOR','POOR','POOR_TO_FAIR','FAIR','FAIR_TO_GOOD+']
                data = np.concatenate((targetcomplete, forecastcomplete, outputsval_nn))
                labels = ['Human Reports']*len(targetcomplete)
                labels.extend(['Human Forecasts']*len(forecastcomplete))
                labels.extend(['Neural Network']*len(outputsval_nn))
                datadf = pd.DataFrame({"data": data, "method": labels})
                plt.figure()
                ax = sn.displot(data=datadf, x='data', hue='method', 
                                bins=np.arange(-0.5,5.5), 
                                multiple='dodge', 
                                shrink=0.5, 
                                stat='probability', 
                                common_norm=False,
                                aspect=3)
                plt.xticks(rotation=0)
                plt.xlabel('Rating')
                plt.ylabel('Percentage of Reports')
                plt.title(spotname)
                ax.set(xticks=np.arange(0,5))
                ax.set_xticklabels(ratings)
                plt.savefig(modelfolder+"/results/SpotAnalysis_MatchedDist_ReportDistributions/"+spotname.replace('/','_')+".png")
                plt.close('all')


    df = pd.DataFrame({"Spot": s, "SpotId": ids, "NoOfReports_test": l, "NoOfReports_train": ltrain, 
                    "MAE-NN": mae, "MAE-24h": mae24h,
                    "correct-NN": correct, "correct-24h": correct24h,
                    "f1-NN": np.round(f1nn, 2), "f1-24h": np.round(f124h, 2), "f1-diff": np.round(np.array(f1nn)-np.array(f124h), 2),
                    "p-NN": np.round(pnn, 2), "p-24h": np.round(p24h, 2), "p-diff": np.round(np.array(pnn)-np.array(p24h), 2)
                    })
    df = df.sort_values(by=['NoOfReports_train'], ascending=False)

    df.to_csv(modelfolder+"/results/spotanalysis_matcheddist.csv", index=False)
    print('done')






def run_subregionreports_distributionmatch(testdf, traindf, modelfolder, subregionsdf):


    os.makedirs(modelfolder+"/results/SubregionAnalysis_MatchedDist_ConfMatrix/", exist_ok=True)
    os.makedirs(modelfolder+"/results/SubregionAnalysis_MatchedDist_ReportDistributions/", exist_ok=True)


    s = []
    ids = []
    f1nn = []
    f124h = []
    l = []
    ltrain = []
    pnn = []
    p24h = []
    mae = []
    mae24h = []
    correct = []
    correct24h = []


    for subregion in tqdm(testdf["subregion"].unique()):
        
        subregionname  = subregionsdf.loc[subregionsdf["_id"] == subregion]["name"].item()

        test_filt = testdf.copy()
        test_filt = test_filt.loc[test_filt["subregion"] == subregion]

        if len(test_filt) > 1:
    
            train_filt = traindf.copy()
            train_filt = train_filt.loc[train_filt["subregion"] == subregion]

            outputsval_nn = np.array(test_filt["nn"]).squeeze()

            outputsval_nn = np.where(outputsval_nn<0,0,outputsval_nn)
            outputsval_nn = np.where(outputsval_nn>=4,4,outputsval_nn)

            targetsval = np.array(test_filt["surf_condition_num_5pt"]).squeeze().astype(int)

            fulldist = np.array(train_filt["surf_condition_num_5pt"]).squeeze().astype(int)

            binwidths = []
            binwidth=0
            for i in range(4):
                leng = len(train_filt.copy().loc[train_filt["surf_condition_num_5pt"] == i])
                if leng>0:
                    binwidth = binwidth + (leng / len(train_filt))
                else:
                    binwidth = binwidth + 1e-6

                binwidths.append(np.quantile(fulldist, binwidth))   


            output_matcheddist = []
            for o in outputsval_nn:
                if o < binwidths[0]:
                    o = 0.
                elif o < binwidths[1]:
                    o = 1.
                elif o < binwidths[2]:
                    o = 2.
                elif o < binwidths[3]:
                    o = 3.
                elif o >= binwidths[3]:
                    o = 4.
                output_matcheddist.append(o)

            outputsval_nn = np.array(output_matcheddist).astype(int)

            # binwidths = []
            # binwidth=0.
            # binwidths.append(binwidth)
            # for i in range(4):
            #     leng = len(train_filt.copy().loc[train_filt["surf_condition_num_5pt"] == i])
            #     if leng>0:
            #         binwidth = binwidth + (leng / len(train_filt))
            #     else:
            #         binwidth = binwidth + 1e-12
            #     binwidths.append(binwidth)    
            # binwidths.append(1.)
            # # bin data based on distribution of values
            # test_filt["binnedoutput"] = pd.qcut(test_filt['nn'], binwidths, labels=False)
            # outputsval_nn = np.array(test_filt["binnedoutput"]).astype(int)

            outputsval = test_filt["forecast_24h_num_5pt"]
            
            percentcorrect_nn = (outputsval_nn == targetsval).sum()
            percentcorrect_24 = (outputsval == targetsval).sum()
            correct.append(percentcorrect_nn / len(outputsval_nn) * 100)
            correct24h.append(percentcorrect_24 / len(outputsval) * 100)

            s.append(subregionname)
            l.append(len(test_filt))
            ltrain.append(len(train_filt))
            ids.append(subregion)
            
            mae.append(np.mean(abs(outputsval_nn-targetsval)))
            mae24h.append(np.mean(abs(outputsval-targetsval)))
            
            f1nn.append(f1_score(targetsval.ravel().astype(int).flatten(), np.round(outputsval_nn), average='weighted'))
            pnn.append(scipy.stats.pearsonr(outputsval_nn, targetsval)[0])
            
            f124h.append(f1_score(targetsval.ravel().astype(int).flatten(), np.round(outputsval), average='weighted'))
            p24h.append(scipy.stats.pearsonr(outputsval, targetsval)[0])

            bin_labels=reduced_class_names
            
            
            plt.figure(figsize=(21,19))
            ax1 = plt.subplot(2,2,1)
            ax1.set_title('f1: ' + str(np.round(f1_score(targetsval.ravel().astype(int).flatten(), np.round(outputsval_nn), average='weighted'), 2)))
            cm = confusion_matrix(targetsval, np.round(outputsval_nn), normalize='true', labels=np.arange(0,len(bin_labels)))
            cm_rounded = cm.round(decimals=2)
            cm = pd.DataFrame(cm, index=[i for i in bin_labels], columns=[i for i in bin_labels])
            cm_rounded = pd.DataFrame(cm_rounded, index=[i for i in bin_labels], columns=[i for i in bin_labels])
            sn.set(font_scale=1.4)
            sn.heatmap(cm_rounded, annot=True, annot_kws={"size": 10}, cmap="viridis", vmin=0., vmax=1.)
            plt.ylabel('Human')

            ax2 = plt.subplot(2,2,2)
            ax2.set_title('f1: ' + str(np.round(f1_score(targetsval.ravel().astype(int).flatten(), np.round(outputsval), average='weighted'), 2)))
            cm = confusion_matrix(targetsval, np.round(outputsval), normalize='true', labels=np.arange(0,len(bin_labels)))
            cm_rounded = cm.round(decimals=2)
            cm = pd.DataFrame(cm, index=[i for i in bin_labels], columns=[i for i in bin_labels])
            cm_rounded = pd.DataFrame(cm_rounded, index=[i for i in bin_labels], columns=[i for i in bin_labels])
            sn.set(font_scale=1.4)
            sn.heatmap(cm_rounded, annot=True, annot_kws={"size": 10}, cmap="viridis", vmin=0., vmax=1.)

            ax3 = plt.subplot(2,2,3)
            cm = confusion_matrix(targetsval, np.round(outputsval_nn), normalize=None, labels=np.arange(0,len(bin_labels)))
            cm_rounded = cm.round(decimals=3)
            cm = pd.DataFrame(cm, index=[i for i in bin_labels], columns=[i for i in bin_labels])
            cm_rounded = pd.DataFrame(cm_rounded, index=[i for i in bin_labels], columns=[i for i in bin_labels])
            sn.set(font_scale=1.4)
            sn.heatmap(cm_rounded, annot=True, annot_kws={"size": 10}, cmap="viridis", vmin=0., vmax=200., fmt='d')
            plt.xlabel('Neural Network')
            plt.ylabel('Human')

            ax4 = plt.subplot(2,2,4)
            cm = confusion_matrix(targetsval, np.round(outputsval), normalize=None, labels=np.arange(0,len(bin_labels)))
            cm_rounded = cm.round(decimals=3)
            cm = pd.DataFrame(cm, index=[i for i in bin_labels], columns=[i for i in bin_labels])
            cm_rounded = pd.DataFrame(cm_rounded, index=[i for i in bin_labels], columns=[i for i in bin_labels])
            sn.set(font_scale=1.4)
            sn.heatmap(cm_rounded, annot=True, annot_kws={"size": 10}, cmap="viridis", vmin=0., vmax=200., fmt='d')
            plt.xlabel('Human Forecast')

            plt.suptitle(subregionname + ", n_train=" + str(len(train_filt)) + ', n_test=' + str(len(test_filt)))

            ax1.set_xticklabels([])
            ax2.set_xticklabels([])
            ax2.set_yticklabels([])
            ax4.set_yticklabels([])

            plt.tight_layout()
            plt.subplots_adjust(top=0.9)
            plt.savefig(modelfolder+"/results/SubregionAnalysis_MatchedDist_ConfMatrix/"+subregionname.replace('/','_')+".png")
            plt.close()
            
            targetcomplete = np.concatenate((np.array(test_filt["surf_condition_num_5pt"]), np.array(train_filt["surf_condition_num_5pt"])))
            forecastcomplete = np.concatenate((np.array(test_filt["forecast_24h_num_5pt"]), np.array(train_filt["forecast_24h_num_5pt"])))

            ratings = ['FLAT_TO_VPOOR','POOR','POOR_TO_FAIR','FAIR','FAIR_TO_GOOD+']
            data = np.concatenate((targetcomplete, forecastcomplete, outputsval_nn))
            labels = ['Human Reports']*len(targetcomplete)
            labels.extend(['Human Forecasts']*len(forecastcomplete))
            labels.extend(['Neural Network']*len(outputsval_nn))
            datadf = pd.DataFrame({"data": data, "method": labels})
            plt.figure()
            ax = sn.displot(data=datadf, 
                            x='data', 
                            hue='method', 
                            bins=np.arange(-0.5,5.5), 
                            multiple='dodge', 
                            shrink=0.5, 
                            stat='probability', 
                            common_norm=False, 
                            aspect=3)
            plt.xticks(rotation=0)
            plt.xlabel('Rating')
            plt.ylabel('Percentage of Reports')
            plt.title(subregionname)
            ax.set(xticks=np.arange(0,5))
            ax.set_xticklabels(ratings)
            plt.savefig(modelfolder+"/results/SubregionAnalysis_MatchedDist_ReportDistributions/"+subregionname.replace('/','_')+".png")
            plt.close()
            plt.close('all')


    df = pd.DataFrame({"Spot": s, "SpotId": ids, "NoOfReports_test": l, "NoOfReports_train": ltrain, 
                    "MAE-NN": mae, "MAE-24h": mae24h,
                    "correct-NN": correct, "correct-24h": correct24h,
                    "f1-NN": np.round(f1nn, 2), "f1-24h": np.round(f124h, 2), "f1-diff": np.round(np.array(f1nn)-np.array(f124h), 2),
                    "p-NN": np.round(pnn, 2), "p-24h": np.round(p24h, 2), "p-diff": np.round(np.array(pnn)-np.array(p24h), 2)
                    })
    df = df.sort_values(by=['NoOfReports_train'], ascending=False)
    # print("Number of Spots:", len(df))
    # print("Spots with f1-NN>0.7:", len(df.loc[df["f1-NN"] > 0.7]))
    # print("Spots with f1-24h>0.7:", len(df.loc[df["f1-24h"] > 0.7]))
    # print("Spots with p-NN>0.8:", len(df.loc[df["p-NN"] > 0.8]))
    # print("Spots with p-24h>0.8:", len(df.loc[df["p-24h"] > 0.8]))
    # print(tabulate(df, headers='keys', showindex=False, tablefmt='github'))
    df.to_csv(modelfolder+"/results/subregionanalysis_matcheddist.csv", index=False)
    print('done')



def run_subregionreports(testdf, traindf, modelfolder, subregionsdf):


    os.makedirs(modelfolder+"/results/SubregionAnalysis_ConfMatrix/", exist_ok=True)
    os.makedirs(modelfolder+"/results/SubregionAnalysis_ReportDistributions/", exist_ok=True)


    s = []
    ids = []
    f1nn = []
    f124h = []
    l = []
    ltrain = []
    pnn = []
    p24h = []
    mae = []
    mae24h = []
    correct = []
    correct24h = []


    for subregion in tqdm(testdf["subregion"].unique()):
        
        subregionname  = subregionsdf.loc[subregionsdf["_id"] == subregion]["name"].item()

        test_filt = testdf.copy()
        test_filt = test_filt.loc[test_filt["subregion"] == subregion]
 
        if len(test_filt)> 1:

            train_filt = traindf.copy()
            train_filt = train_filt.loc[train_filt["subregion"] == subregion]

            outputsval_nn = np.round(np.array(test_filt["nn"])).squeeze().astype(int)

            outputsval_nn = np.where(outputsval_nn<0,0,outputsval_nn)
            outputsval_nn = np.where(outputsval_nn>=4,4,outputsval_nn)

            targetsval = np.array(test_filt["surf_condition_num_5pt"]).squeeze().astype(int)

            outputsval = test_filt["forecast_24h_num_5pt"]
            
            percentcorrect_nn = (outputsval_nn == targetsval).sum()
            percentcorrect_24 = (outputsval == targetsval).sum()
            correct.append(percentcorrect_nn / len(outputsval_nn) * 100)
            correct24h.append(percentcorrect_24 / len(outputsval) * 100)

            s.append(subregionname)
            l.append(len(test_filt))
            ltrain.append(len(train_filt))
            ids.append(subregion)
            
            mae.append(np.mean(abs(outputsval_nn-targetsval)))
            mae24h.append(np.mean(abs(outputsval-targetsval)))
            
            f1nn.append(f1_score(targetsval.ravel().astype(int).flatten(), np.round(outputsval_nn), average='weighted'))
            pnn.append(scipy.stats.pearsonr(outputsval_nn, targetsval)[0])
            
            f124h.append(f1_score(targetsval.ravel().astype(int).flatten(), np.round(outputsval), average='weighted'))
            p24h.append(scipy.stats.pearsonr(outputsval, targetsval)[0])

            bin_labels=reduced_class_names
            
            
            plt.figure(figsize=(21,19))
            ax1 = plt.subplot(2,2,1)
            ax1.set_title('f1: ' + str(np.round(f1_score(targetsval.ravel().astype(int).flatten(), np.round(outputsval_nn), average='weighted'), 2)))
            cm = confusion_matrix(targetsval, np.round(outputsval_nn), normalize='true', labels=np.arange(0,len(bin_labels)))
            cm_rounded = cm.round(decimals=2)
            cm = pd.DataFrame(cm, index=[i for i in bin_labels], columns=[i for i in bin_labels])
            cm_rounded = pd.DataFrame(cm_rounded, index=[i for i in bin_labels], columns=[i for i in bin_labels])
            sn.set(font_scale=1.4)
            sn.heatmap(cm_rounded, annot=True, annot_kws={"size": 10}, cmap="viridis", vmin=0., vmax=1.)
            plt.ylabel('Human')

            ax2 = plt.subplot(2,2,2)
            ax2.set_title('f1: ' + str(np.round(f1_score(targetsval.ravel().astype(int).flatten(), np.round(outputsval), average='weighted'), 2)))
            cm = confusion_matrix(targetsval, np.round(outputsval), normalize='true', labels=np.arange(0,len(bin_labels)))
            cm_rounded = cm.round(decimals=2)
            cm = pd.DataFrame(cm, index=[i for i in bin_labels], columns=[i for i in bin_labels])
            cm_rounded = pd.DataFrame(cm_rounded, index=[i for i in bin_labels], columns=[i for i in bin_labels])
            sn.set(font_scale=1.4)
            sn.heatmap(cm_rounded, annot=True, annot_kws={"size": 10}, cmap="viridis", vmin=0., vmax=1.)

            ax3 = plt.subplot(2,2,3)
            cm = confusion_matrix(targetsval, np.round(outputsval_nn), normalize=None, labels=np.arange(0,len(bin_labels)))
            cm_rounded = cm.round(decimals=3)
            cm = pd.DataFrame(cm, index=[i for i in bin_labels], columns=[i for i in bin_labels])
            cm_rounded = pd.DataFrame(cm_rounded, index=[i for i in bin_labels], columns=[i for i in bin_labels])
            sn.set(font_scale=1.4)
            sn.heatmap(cm_rounded, annot=True, annot_kws={"size": 10}, cmap="viridis", vmin=0., vmax=200., fmt='d')
            plt.xlabel('Neural Network')
            plt.ylabel('Human')

            ax4 = plt.subplot(2,2,4)
            cm = confusion_matrix(targetsval, np.round(outputsval), normalize=None, labels=np.arange(0,len(bin_labels)))
            cm_rounded = cm.round(decimals=3)
            cm = pd.DataFrame(cm, index=[i for i in bin_labels], columns=[i for i in bin_labels])
            cm_rounded = pd.DataFrame(cm_rounded, index=[i for i in bin_labels], columns=[i for i in bin_labels])
            sn.set(font_scale=1.4)
            sn.heatmap(cm_rounded, annot=True, annot_kws={"size": 10}, cmap="viridis", vmin=0., vmax=200., fmt='d')
            plt.xlabel('Human Forecast')

            plt.suptitle(subregionname + ", n_train=" + str(len(train_filt)) + ', n_test=' + str(len(test_filt)))

            ax1.set_xticklabels([])
            ax2.set_xticklabels([])
            ax2.set_yticklabels([])
            ax4.set_yticklabels([])

            plt.tight_layout()
            plt.subplots_adjust(top=0.9)
            plt.savefig(modelfolder+"/results/SubregionAnalysis_ConfMatrix/"+subregionname.replace('/','_')+".png")
            plt.close()
            
            targetcomplete = np.concatenate((np.array(test_filt["surf_condition_num_5pt"]), np.array(train_filt["surf_condition_num_5pt"])))
            forecastcomplete = np.concatenate((np.array(test_filt["forecast_24h_num_5pt"]), np.array(train_filt["forecast_24h_num_5pt"])))

            ratings = ['FLAT_TO_VPOOR','POOR','POOR_TO_FAIR','FAIR','FAIR_TO_GOOD+']
            data = np.concatenate((targetcomplete, forecastcomplete, outputsval_nn))
            labels = ['Human Reports']*len(targetcomplete)
            labels.extend(['Human Forecasts']*len(forecastcomplete))
            labels.extend(['Neural Network']*len(outputsval_nn))
            datadf = pd.DataFrame({"data": data, "method": labels})
            plt.figure()
            ax = sn.displot(data=datadf, 
                            x='data', 
                            hue='method', 
                            bins=np.arange(-0.5,5.5), 
                            multiple='dodge', 
                            shrink=0.5, 
                            stat='probability', 
                            common_norm=False, 
                            aspect=3)
            plt.xticks(rotation=0)
            plt.xlabel('Rating')
            plt.ylabel('Percentage of Reports')
            plt.title(subregionname)
            ax.set(xticks=np.arange(0,5))
            ax.set_xticklabels(ratings)
            plt.savefig(modelfolder+"/results/SubregionAnalysis_ReportDistributions/"+subregionname.replace('/','_')+".png")
            plt.close()
            plt.close('all')


    df = pd.DataFrame({"Spot": s, "SpotId": ids, "NoOfReports_test": l, "NoOfReports_train": ltrain, 
                    "MAE-NN": mae, "MAE-24h": mae24h,
                    "correct-NN": correct, "correct-24h": correct24h,
                    "f1-NN": np.round(f1nn, 2), "f1-24h": np.round(f124h, 2), "f1-diff": np.round(np.array(f1nn)-np.array(f124h), 2),
                    "p-NN": np.round(pnn, 2), "p-24h": np.round(p24h, 2), "p-diff": np.round(np.array(pnn)-np.array(p24h), 2)
                    })
    df = df.sort_values(by=['NoOfReports_train'], ascending=False)
    # print("Number of Spots:", len(df))
    # print("Spots with f1-NN>0.7:", len(df.loc[df["f1-NN"] > 0.7]))
    # print("Spots with f1-24h>0.7:", len(df.loc[df["f1-24h"] > 0.7]))
    # print("Spots with p-NN>0.8:", len(df.loc[df["p-NN"] > 0.8]))
    # print("Spots with p-24h>0.8:", len(df.loc[df["p-24h"] > 0.8]))
    # print(tabulate(df, headers='keys', showindex=False, tablefmt='github'))
    df.to_csv(modelfolder+"/results/subregionanalysis.csv", index=False)
    print('done')