from tools.process_data import process_data, generate_dataset_fromspectra, generate_loader
from torch.utils.data import DataLoader
from tools.dataloaders import CustomDataset
from tools.nn_architectures import MLP, MLP_subregionembedding
import pandas as pd
from yaml import load, Loader
import numpy as np
import pickle
import torch
import os
from torch import nn
from tools.loops import validate_model
import argparse

def run_validation(modelyaml, data, outfolder, run_on_cuda, tuned=False, fromdf=False):

    #open yaml file
    with open(modelyaml,'r') as f:
        params = load(f, Loader=Loader)

    #load scaler
    if not params["noscaling"]:
        with open(outfolder+"/scaler.pkl", 'rb') as fp:
            scaler = pickle.load(fp)
    else:
        scaler = None

    test_loader = generate_loader(data, scaler, params, outfolder, trainmode=False, fromdf=fromdf)

    #cuda yes/no
    if not torch.cuda.is_available():
        use_cuda=False
    else:
        use_cuda = run_on_cuda

    inputsize = params["inputsize"]


    if params["use_subregion"]:
        model = MLP_subregionembedding(in_features=inputsize, 
                out_features=params["out_features"], 
                hidden=params["hidden"],
                dropout=params["dropout"], 
                embed_len=params["embed_len"], 
                embed_dim=params["embed_dim"])
    else:
        model = MLP(in_features=inputsize, 
                    out_features=params["out_features"], 
                    hidden=params["hidden"],
                    dropout=params["dropout"], 
                    lastlayer=params["lastlayer"])
    #define loss function
    criterion = nn.MSELoss(reduction="mean")

    #load weights
    if tuned:
        modelweight = torch.load(outfolder+"/weights_tuned.pth", map_location=torch.device("cpu"))
    else:
        modelweight = torch.load(outfolder+"/weights.pth", map_location=torch.device("cpu"))
    model.load_state_dict(modelweight)

    #run validation
    valid_loss, outputs, targets = validate_model(model, test_loader, criterion, use_cuda, m_eval=True, use_subregion=params["use_subregion"])

    return np.array(outputs).squeeze()


def parse_args():
    parser = argparse.ArgumentParser(description='Validation script for ratings and surf heights models')
    parser.add_argument('--modelyaml', type=str, help='looking up yaml config')
    parser.add_argument('--csv', type=str, help='data for valdation')
    parser.add_argument('--outfolder', type=str, help='output folder for results')
    parser.add_argument('--cuda', action='store_true', help='run on gpu yes/no')
    arguments = parser.parse_args()
    return arguments

if __name__ == '__main__':

    # get parsed arguments
    args = parse_args()

    #run validation
    output = run_validation(args.modelyaml, args.csv, args.outfolder, args.run_on_cuda)

    #save output variables
    with open(args.outfolder+"/validation_output.pkl", 'wb') as fp:
            pickle.dump(output, fp)





    