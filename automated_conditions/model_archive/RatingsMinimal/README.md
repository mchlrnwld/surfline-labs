## Running

1. Create and activate the Conda environment:
   ```sh
   conda env create -f environment.yml
   conda activate forecast_torchserve
   ```

2. Download trained model from S3:
    ```sh
   aws s3 cp s3://wt-ml-artifacts/models/AutomatedConditions/RatingsMinimal/weights.pth ./  --profile surfline-dev
   aws s3 cp s3://wt-ml-artifacts/models/AutomatedConditions/RatingsMinimal/scaler.pkl ./  --profile surfline-dev
   ```

3. Package model using torchserve
    ```sh
    torch-model-archiver \
      --model-name RatingsMinimal \
      --version 1.0 \
      --serialized-file weights.pth \
      --handler surf_rating_handler \
      --model-file nn_architecture.py \
      --extra-files "util.py,scaler.pkl" \
      --requirements-file "requirements.txt" -f
    ```

4. Upload packaged model to S3
    ```sh
   aws s3 cp RatingsMinimal.mar s3://wt-ml-artifacts/models/AutomatedConditions/RatingsMinimal/ --profile surfline-dev
   ```

