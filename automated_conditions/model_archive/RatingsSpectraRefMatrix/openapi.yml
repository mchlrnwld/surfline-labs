openapi: 3.0.0
info:
  title: RatingsSpectraRefMatrix
  description: PyTorch machine learning model used in the forecast platform using spectra (calculated from forecast data), wind speed and direction, and spots-specific offshoredirection
  version: "1.0"
components:
  schemas:
    Forecast:
      description: Surfline forecast data
      type: object
      properties:
        surf:
          $ref: "#/components/schemas/SurfForecast"
        swells:
          $ref: "#/components/schemas/SwellForecast"
        wind:
          $ref: "#/components/schemas/WindForecast"
    Surf:
      description: Surf conditions
      type: object
      properties:
        breakingWaveHeightMax:
          type: number
        breakingWaveHeightMin:
          type: number
    SurfForecast:
      description: Array of surf conditions ordered by time
      type: object
      properties:
        data:
          type: array
          items:
            $ref: "#/components/schemas/SurfHour"
    SurfHour:
      description: Surf conditions at a single point in time
      type: object
      properties:
        timestamp:
          type: integer
        surf:
          $ref: "#/components/schemas/Surf"
    Swell:
      description: Swell conditions
      type: object
      properties:
        combined:
          type: object
          properties:
            direction:
              type: number
            height:
              type: number
            period:
              type: number
        components:
          type: array
          items:
            type: object
            properties:
              direction:
                type: number
              height:
                type: number
              period:
                type: number
              spread:
                type: number
    SwellForecast:
      description: Array of swell conditions ordered by time
      type: object
      properties:
        data:
          type: array
          items:
            $ref: "#/components/schemas/SwellHour"
    SwellHour:
      description: Swell conditions at a single point in time
      type: object
      properties:
        timestamp:
          type: integer
        swells:
          $ref: "#/components/schemas/Swell"
    Wind:
      description: Wind conditions
      type: object
      properties:
        speed:
          type: number
        direction:
          type: number
    WindForecast:
      description: Array of wind conditions ordered by time
      type: object
      properties:
        data:
          type: array
          items:
            $ref: "#/components/schemas/WindHour"
    WindHour:
      description: Wind conditions at a single point in time
      type: object
      properties:
        timestamp:
          type: integer
        wind:
          $ref: "#/components/schemas/Wind"
paths:
  /predictions/onlylotus:
    post:
      summary: Returns a surf rating forecast.
      description: Request surf rating predictions from the RatingsSpectraRefMatrix PyTorch model
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                offshore:
                  description: Offshore cardinal direction at a surf spot
                  type: integer
                forecast:
                  $ref: "#/components/schemas/Forecast"
                refrationmatrix:
                  description: Spectral refraction matrix
                  type: array
                  items:
                    type: array
      responses:
        '200':
          description: A JSON array surf ratings
          content:
            application/json:
              schema:
                type: array
                items:
                  type: integer
                  