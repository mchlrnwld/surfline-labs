import logging
import pickle

import numpy as np
import torch
from ts.torch_handler.base_handler import BaseHandler

from nn_architecture import MLP
from util import (
    generate_array,
    modify_lotus_angles,
    build_features,
)



def smooth_timeseries(vals, maxchange: float = 0.32):
    """
    Transforms a numpy array of sequential 1h ratings into a new numpy array by smoothing
    :param vals: numpy array containing floating point ratings
    :param maxchange: floating point value, defines maximum change to ignore change in ratings
    :returns: numpy array of smoothed data
    """

    # calculate change in values
    valschange = np.diff(vals, n=1, prepend=vals[0])
    valschangeabs = abs(valschange)
    valsprev = np.concatenate(([vals[0]], vals[:-1]))

    # calculate change in ratings
    valsint = np.round(vals).astype(int)
    valschangeint = np.diff(valsint, n=1, prepend=valsint[0])

    a = vals.copy()

    counter = 0

    # while loop:
    # - no change in ratings with absolute change smaller than maxchange
    while len(a[((valschangeint != 0) & (valschangeabs < maxchange))]) > 0:

        counter = counter + 1
        if counter == 100:
            return a

        a = np.where((valschangeint != 0) & (
            valschangeabs < maxchange), valsprev, a)

        # recalculate changes in array
        valschange = np.diff(a, n=1, prepend=a[0])
        valschangeabs = abs(valschange)
        valsprev = np.concatenate(([a[0]], a[:-1]))
        valsint = np.round(a).astype(int)
        valschangeint = np.diff(valsint, n=1, prepend=valsint[0])

    return a

logger = logging.getLogger(__name__)


class SurfRatingHandler(BaseHandler):
    """
    https://github.com/pytorch/serve/blob/master/docs/custom_service.md#advanced-custom-handlers
    """

    def __init__(self):
        super().__init__()
        self._context = None
        self.initialized = False
        self.explain = False
        self.target = 0

    def initialize(self, context):
        """
        Initialize model. This will be called during model loading time
        :param context: Initial context contains model server system properties.
        :return:
        """
        self.device = "cpu"

        # https://pytorch.org/tutorials/beginner/basics/buildmodel_tutorial.html#define-the-class
        self.model = MLP(in_features=4, hidden=32, dropout=0, lastlayer=4)

        # https://pytorch.org/tutorials/beginner/basics/saveloadrun_tutorial.html#saving-and-loading-model-weights
        modeldict = torch.load(
            "weights.pth", map_location=torch.device("cpu")
        )
        self.model.load_state_dict(modeldict)

        # https://pytorch.org/docs/stable/generated/torch.nn.Module.html#torch.nn.Module.eval
        self.model.eval()

        self.initialized = True

        logger.info(f'Completed initialization of weights.pth')

    def preprocess(self, data):
        request_data = data[0].get("data")
        if request_data is None:
            request_data = data[0].get("body")

        features = build_features(request_data['forecast'])

        tensors = []
        with open("scaler.pkl", 'rb') as f:
            scaler = pickle.load(f)

            for feature in features:
                # Dictionary with angles changed
                lotus_dict = modify_lotus_angles(
                    feature, request_data['offshore']
                )

                # Returns np.array of features
                x = generate_array(lotus_dict)

                x_scaled = scaler.transform(x)
                x_tensor = torch.tensor(x_scaled, dtype=torch.float32)

                tensors.append(x_tensor)

        return tensors

    def inference(self, model_inputs, *args, **kwargs):

        with torch.no_grad():
            outputs = [self.model(model_input) for model_input in model_inputs]

        return outputs

    def postprocess(self, inference_outputs):

        inference_outputs_smoothed = smooth_timeseries(
            np.array(inference_outputs).squeeze())

        inference_outputs_smoothed = inference_outputs_smoothed.tolist()

        return [
            [
                np.round(inference_output, 2) + 1
                for inference_output in inference_outputs_smoothed
            ]
        ]
