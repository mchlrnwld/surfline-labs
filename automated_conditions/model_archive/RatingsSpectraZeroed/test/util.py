import backoff
import logging
import os
import requests
import time
from typing import Dict

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler())
logging.getLogger('backoff').addHandler(logging.StreamHandler())


@backoff.on_exception(
    backoff.constant,
    requests.exceptions.RequestException,
    interval=1,
    max_time=180,
    jitter=None
)
def wait_for_torchserve_initialization():
    requests.get(f"{os.environ['SURF_RATINGS_API']}/ping")


class PointOfInterest:
    def __init__(self, data: Dict):
        self.latitude = data['lat']
        self.longitude = data['lon']
        self.pointOfInterestId = data['pointOfInterestId']
        self.surfSpotConfiguration = SurfSpotConfiguration(
            data['surfSpotConfiguration'])


class Spot:
    def __init__(self, data: Dict):
        self.best = data['best']
        self.pointOfInterestId = data['pointOfInterestId']
        self.spotId = data['_id']


class SurfSpotConfiguration:
    def __init__(self, data: Dict):
        self.offshoreDirection = data['offshoreDirection']
        self.optimalSwellDirection = data['optimalSwellDirection']
        self.spectralRefractionMatrix = data['spectralRefractionMatrix']


def get_point_of_interest_by_id(point_of_interest_id: str) -> PointOfInterest:
    query = f"""
    query {{
        pointOfInterest(pointOfInterestId: "{point_of_interest_id}") {{
            pointOfInterestId
            name
            lat
            lon
            surfSpotConfiguration {{
                offshoreDirection
                optimalSwellDirection
                spectralRefractionMatrix
            }}
        }}
    }}
    """
    resp = requests.post(os.environ['GRAPHQL_GATEWAY'], json={"query": query})

    if resp.status_code != 200:
        raise Exception(
            f'Error getting point of interest ({resp.status_code})'
        )

    return PointOfInterest(resp.json()['data']['pointOfInterest'])


def get_spot_by_point_of_interest_id(point_of_interest_id: str) -> Spot:
    query = f"""
    query{{
        spot(pointOfInterestId: "{point_of_interest_id}") {{
            ... on SLSpot {{
                _id
                pointOfInterestId
                best {{
                    windDirection {{
                        min
                        max
                    }}
                    swellPeriod {{
                        min
                        max
                    }}
                    swellWindow {{
                        min
                        max
                    }}
                    sizeRange {{
                        min
                        max
                    }}
                }}
            }}
        }}
    }}
    """
    resp = requests.post(os.environ['GRAPHQL_GATEWAY'], json={"query": query})

    if resp.status_code != 200:
        raise Exception(f'Error getting spot ({resp.status_code})')

    return Spot(resp.json()['data']['spot'][0])


# def get_tide(point_of_interest: PointOfInterest, weather) -> Dict[str, object]:
#     current_time = int(time.time())
#     time_start = str(current_time)
#     time_end = str(current_time + 72 * 60 * 60)
#     resp = requests.get(
#         os.path.join(
#             os.environ['TIDE_SERVICE'],
#             f'data?lat={str(point_of_interest.latitude)}',
#             f'&lon={str(point_of_interest.longitude)}',
#             f'&start={time_start}',
#             f'&end={time_end}',
#         )
#     )

#     return resp.json()


def pull_sds_data(poi_id):
    """
    Used to pull forecast data from the science data service using the graphql
    query.
    Args:
        poi_id: point of interest id for the spot to pull forecast data for
    Returns:
        the pulled sds data in dict form
    """
    query = f"""
    query {{
        surfSpotForecasts(pointOfInterestId: "{poi_id}") {{
            surf (waveHeight: "ft", interval: 3600) {{
                data {{
                    timestamp
                    surf {{
                      breakingWaveHeightMin
                      breakingWaveHeightMax
                    }}
                }}
            }}
            swells (interval: 3600) {{
                data {{
                    timestamp
                    swells {{
                        combined {{
                            height
                            period
                            direction
                        }}
                        components {{
                            height
                            period
                            direction
                            spread
                        }}
                    }}
                }}
            }}
            weather(agency: "Wavetrak", model: "Blended", grid: "Blended", interval: 3600) {{
                data {{
                    timestamp
                    weather {{
                        conditions
                    }}
                }}
            }}
            wind(agency: "Wavetrak", model: "Blended", grid: "Blended", windSpeed: "knot", interval: 3600) {{
                data {{
                    timestamp
                    wind {{
                        speed
                        direction
                    }}
                }}
            }}
        }}
    }}
    """
    resp = requests.post(os.environ['GRAPHQL_GATEWAY'], json={"query": query})

    if resp.status_code != 200:
        raise Exception(f'Error pulling SDS data ({resp.status_code})')

    return resp.json()['data']['surfSpotForecasts']
