import math
import numpy as np
from approximate_spectra import approximate_spectra_from_swell_partitions
from swell_partition import SwellPartition
from frequency_calculations import (
    create_discrete_frequency_steps,
    create_frequency_distribution,
)
from scipy.interpolate import RectBivariateSpline

def get_rel_angles(angles):
    angles = angles % 360
    angles = np.where(angles > 180, angles - 360, angles)
    return angles


def modify_lotus_angles(lotusdict: dict, offshoredirection):
    
    key = "LotusWindDir_deg"
    rel = np.array(
        get_rel_angles(np.array(lotusdict[key]) - offshoredirection)
    )
    lotusdict[key + "_rel_sin"] = np.sin(
        2 * math.pi * (rel.astype(float) + 180) / 360
    )
    lotusdict[key + "_rel_cos"] = np.cos(
        2 * math.pi * (rel.astype(float) + 180) / 360
    )

    print(f'LOTUSDICT: {lotusdict}')

    return lotusdict

def generate_array(lotusdict, offshoredirection, refractionmatrix):

    frequencies = np.array(
    [
        0.03500000014901161,
        0.03849999979138374,
        0.04234999790787697,
        0.046585001051425934,
        0.05124350264668465,
        0.05636785179376602,
        0.062004633247852325,
        0.0682050958275795,
        0.07502561062574387,
        0.08252817392349243,
        0.09078099578619003,
        0.0998590961098671,
        0.10984501242637634,
        0.1208295151591301,
        0.13291247189044952,
        0.14620372653007507,
        0.16082410514354706,
        0.1769065260887146,
        0.19459718465805054,
        0.21405690908432007,
        0.23546260595321655,
        0.25900885462760925,
        0.28490975499153137,
        0.3134007453918457,
        0.3447408080101013,
        0.379214882850647,
        0.41713640093803406,
        0.45885002613067627,
        0.5047350525856018,
    ]
    )

    directions = np.array(
    [
        0.0,
        10.0,
        20.0,
        30.0,
        40.0,
        50.0,
        60.0,
        70.0,
        80.0,
        90.0,
        100.0,
        110.0,
        120.0,
        130.0,
        140.0,
        150.0,
        160.0,
        170.0,
        180.0,
        190.0,
        200.0,
        210.0,
        220.0,
        230.0,
        240.0,
        250.0,
        260.0,
        270.0,
        280.0,
        290.0,
        300.0,
        310.0,
        320.0,
        330.0,
        340.0,
        350.0,
    ]
)   

    #specific values for generating spectrum
    minimum_frequency = 0.035
    maximum_frequency = 0.5
    direction_bin_size = 10

    direction_bin_size2 = 15
    lola_directions = np.arange(direction_bin_size2, 360 + direction_bin_size2, direction_bin_size2,)
    minimum_frequency2 = 0.0418
    maximum_frequency2 = 0.411
    lola_frequencies = create_frequency_distribution(minimum_frequency2, maximum_frequency2)
    adjusted_directions = np.concatenate([directions[1:], np.array([360])])

    discrete_frequency_steps = create_discrete_frequency_steps(lola_frequencies)
    discrete_frequency_steps = np.tile(discrete_frequency_steps,(24,1)).T
    discrete_frequency_steps = np.array(discrete_frequency_steps)

    wind = np.array([lotusdict["LotusWindSpeed_ms"], lotusdict["LotusWindDir_deg_rel_sin"], lotusdict["LotusWindDir_deg_rel_cos"]])      

    #collect swell partition data

    hs_part = [lotusdict["LotusSigHPart0_mt"], 
                lotusdict["LotusSigHPart1_mt"], 
                lotusdict["LotusSigHPart2_mt"], 
                lotusdict["LotusSigHPart3_mt"], 
                lotusdict["LotusSigHPart4_mt"], 
                lotusdict["LotusSigHPart5_mt"]]
    
    tp_part = [lotusdict["LotusTPPart0_sec"], 
                lotusdict["LotusTPPart1_sec"], 
                lotusdict["LotusTPPart2_sec"], 
                lotusdict["LotusTPPart3_sec"], 
                lotusdict["LotusTPPart4_sec"], 
                lotusdict["LotusTPPart5_sec"]]
    
    pdir_part = [lotusdict["LotusPdirPart0_deg"], 
                    lotusdict["LotusPdirPart1_deg"], 
                    lotusdict["LotusPdirPart2_deg"], 
                    lotusdict["LotusPdirPart3_deg"], 
                    lotusdict["LotusPdirPart4_deg"], 
                    lotusdict["LotusPdirPart5_deg"]]
    
    dspread_part = [lotusdict["LotusSpreadPart0_deg"], 
                    lotusdict["LotusSpreadPart1_deg"], 
                    lotusdict["LotusSpreadPart2_deg"], 
                    lotusdict["LotusSpreadPart3_deg"], 
                    lotusdict["LotusSpreadPart4_deg"], 
                    lotusdict["LotusSpreadPart5_deg"]]

    swell_partitions = [
        SwellPartition(height, period, direction, spread)
        for height, period, direction, spread in zip(
            hs_part, tp_part, pdir_part, dspread_part
            )
        ]
        
    #calculate spectra from swell partitions
    valid_spectra_wave_energy = approximate_spectra_from_swell_partitions(
            swell_partitions=swell_partitions,
            minimum_frequency=minimum_frequency,
            maximum_frequency=maximum_frequency,
            direction_bin_size=direction_bin_size)

    
    adjusted_spectra_wave_energy = np.concatenate((valid_spectra_wave_energy[:, 1:], valid_spectra_wave_energy[:, 0][:, None]),axis=1,)
    spline_interpolation = RectBivariateSpline(frequencies, adjusted_directions, adjusted_spectra_wave_energy)
    lola_spectra = spline_interpolation(lola_frequencies, lola_directions)
    lola_spectra[lola_spectra < 0] = 0

    refractionmatrix = np.array(refractionmatrix)

    def_ref_matrix_expanded = np.ones((24,25))
    def_ref_matrix_expanded[:24,:18] = refractionmatrix
    for i in np.arange(18,25):
        def_ref_matrix_expanded[:,i] = refractionmatrix[:,17]
    def_ref_matrix_expanded = def_ref_matrix_expanded.T
    def_ref_matrix_expanded = np.where(def_ref_matrix_expanded>0,1,0)

    transformed_spectra = discrete_frequency_steps * (direction_bin_size2 * np.pi / 180) * (lola_spectra * def_ref_matrix_expanded)

    offshorebin = int(np.round(offshoredirection / 15))

    if offshorebin == 0:
        transformed_spectra_before = transformed_spectra[:,offshorebin+12:]
        transformed_spectra_after = transformed_spectra[:,:offshorebin+12]

    elif offshoredirection < 180:
        transformed_spectra_before_a = transformed_spectra[:,:offshorebin]
        transformed_spectra_before_b = transformed_spectra[:,offshorebin+12:]
        transformed_spectra_after = transformed_spectra[:,offshorebin:offshorebin+12]
        transformed_spectra_before = np.concatenate((transformed_spectra_before_b, transformed_spectra_before_a), axis=1)
        
    else:
        transformed_spectra_before = transformed_spectra[:,offshorebin-12:offshorebin]
        transformed_spectra_after_a = transformed_spectra[:,offshorebin:]
        transformed_spectra_after_b = transformed_spectra[:,:offshorebin-12]
        transformed_spectra_after = np.concatenate((transformed_spectra_after_a, transformed_spectra_after_b), axis=1)

    transformed_spectra_rel = np.concatenate((transformed_spectra_before, transformed_spectra_after), axis=1)

    features = np.concatenate((wind, transformed_spectra_rel.flatten()))
    x = np.expand_dims(features, axis=0)
 
    return x


def build_features(forecast):
    """
    Builds feature set from forecast data.

    The features are determined by the model, and if the model were to change
    this would most likely need to be updated.

    Args:
        sds_data: dict containing forecast data with top level keys:
                  surf, swells, wind, and weather

    Returns:
        array containing features for each timestamp in the forecast data
    """

    # data = sds_data["data"]["surfSpotForecasts"]

    features_set = [
        {
            "LotusSigH_mt": forecast["swells"]["data"][i]["swells"]["combined"][
                "height"
            ]
                            / 3.2808,
            "LotusMinBWH_mt": forecast["surf"]["data"][i]["surf"][
                "breakingWaveHeightMin"
            ]
                              / 3.2808,
            "LotusMaxBWH_mt": forecast["surf"]["data"][i]["surf"][
                "breakingWaveHeightMax"
            ]
                              / 3.2808,
            "LotusWindSpeed_ms": forecast["wind"]["data"][i]["wind"]["speed"]
                                 / 1.9438444924406,
            "LotusWindDir_deg": forecast["wind"]["data"][i]["wind"][
                "direction"
            ],
            "LotusSigHPart0_mt": forecast["swells"]["data"][i]["swells"][
                "components"
            ][0]["height"]
                                 / 3.2808,
            "LotusTPPart0_sec": forecast["swells"]["data"][i]["swells"][
                "components"
            ][0]["period"],
            "LotusPdirPart0_deg": forecast["swells"]["data"][i]["swells"][
                "components"
            ][0]["direction"],
            "LotusSpreadPart0_deg": forecast["swells"]["data"][i]["swells"][
                "components"
            ][0]["spread"],
            "LotusSigHPart1_mt": forecast["swells"]["data"][i]["swells"][
                "components"
            ][1]["height"]
                                 / 3.2808
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 1
            else 0,
            "LotusTPPart1_sec": forecast["swells"]["data"][i]["swells"][
                "components"
            ][1]["period"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 1
            else 0,
            "LotusPdirPart1_deg": forecast["swells"]["data"][i]["swells"][
                "components"
            ][1]["direction"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 1
            else 0,
            "LotusSpreadPart1_deg": forecast["swells"]["data"][i]["swells"][
                "components"
            ][1]["spread"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 1
            else 0,
            "LotusSigHPart2_mt": forecast["swells"]["data"][i]["swells"][
                "components"
            ][2]["height"]
                                 / 3.2808
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 2
            else 0,
            "LotusTPPart2_sec": forecast["swells"]["data"][i]["swells"][
                "components"
            ][2]["period"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 2
            else 0,
            "LotusPdirPart2_deg": forecast["swells"]["data"][i]["swells"][
                "components"
            ][2]["direction"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 2
            else 0,
            "LotusSpreadPart2_deg": forecast["swells"]["data"][i]["swells"][
                "components"
            ][2]["spread"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 2
            else 0,
            "LotusSigHPart3_mt": forecast["swells"]["data"][i]["swells"][
                "components"
            ][3]["height"]
                                 / 3.2808
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 3
            else 0,
            "LotusTPPart3_sec": forecast["swells"]["data"][i]["swells"][
                "components"
            ][3]["period"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 3
            else 0,
            "LotusPdirPart3_deg": forecast["swells"]["data"][i]["swells"][
                "components"
            ][3]["direction"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 3
            else 0,
            "LotusSpreadPart3_deg": forecast["swells"]["data"][i]["swells"][
                "components"
            ][3]["spread"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 3
            else 0,
            "LotusSigHPart4_mt": forecast["swells"]["data"][i]["swells"][
                "components"
            ][4]["height"]
                                 / 3.2808
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 4
            else 0,
            "LotusTPPart4_sec": forecast["swells"]["data"][i]["swells"][
                "components"
            ][4]["period"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 4
            else 0,
            "LotusPdirPart4_deg": forecast["swells"]["data"][i]["swells"][
                "components"
            ][4]["direction"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 4
            else 0,
            "LotusSpreadPart4_deg": forecast["swells"]["data"][i]["swells"][
                "components"
            ][4]["spread"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 4
            else 0,
            "LotusSigHPart5_mt": forecast["swells"]["data"][i]["swells"][
                "components"
            ][5]["height"]
                                 / 3.2808
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 5
            else 0,
            "LotusTPPart5_sec": forecast["swells"]["data"][i]["swells"][
                "components"
            ][5]["period"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 5
            else 0,
            "LotusPdirPart5_deg": forecast["swells"]["data"][i]["swells"][
                "components"
            ][5]["direction"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 5
            else 0,
            "LotusSpreadPart5_deg": forecast["swells"]["data"][i]["swells"][
                "components"
            ][5]["spread"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 5
            else 0,
        }
        for i in range(0, len(forecast["swells"]["data"]))
    ]

    return features_set
