import math
import numpy as np
import six


def get_rel_angles(angles):
    angles = angles % 360
    angles = np.where(angles > 180, angles - 360, angles)
    return angles


def modify_lotus_angles(lotusdict: dict, offshoredirection):
    # all of this needed for sorting swell partitions by SigH
    ######
    sighlist = [
        lotusdict["LotusSigHPart" + str(i) + "_mt"] for i in np.arange(0, 6)
    ]
    pdirlist = [
        lotusdict["LotusPdirPart" + str(i) + "_deg"] for i in np.arange(0, 6)
    ]
    tppartlist = [
        lotusdict["LotusTPPart" + str(i) + "_sec"] for i in np.arange(0, 6)
    ]
    spreadlist = [
        lotusdict["LotusSpreadPart" + str(i) + "_deg"] for i in np.arange(0, 6)
    ]

    sorted_order = np.argsort(np.nan_to_num(np.array(sighlist)))[::-1]

    sorted_sighlist = [sighlist[i] for i in sorted_order]
    sorted_pdirlist = [pdirlist[i] for i in sorted_order]
    sorted_tppartlist = [tppartlist[i] for i in sorted_order]
    sorted_spreadlist = [spreadlist[i] for i in sorted_order]

    (
        lotusdict["LotusSigHPart0_mt"],
        lotusdict["LotusSigHPart1_mt"],
        lotusdict["LotusSigHPart2_mt"],
        lotusdict["LotusSigHPart3_mt"],
        lotusdict["LotusSigHPart4_mt"],
        lotusdict["LotusSigHPart5_mt"],
    ) = sorted_sighlist
    (
        lotusdict["LotusPdirPart0_deg"],
        lotusdict["LotusPdirPart1_deg"],
        lotusdict["LotusPdirPart2_deg"],
        lotusdict["LotusPdirPart3_deg"],
        lotusdict["LotusPdirPart4_deg"],
        lotusdict["LotusPdirPart5_deg"],
    ) = sorted_pdirlist
    (
        lotusdict["LotusTPPart0_sec"],
        lotusdict["LotusTPPart1_sec"],
        lotusdict["LotusTPPart2_sec"],
        lotusdict["LotusTPPart3_sec"],
        lotusdict["LotusTPPart4_sec"],
        lotusdict["LotusTPPart5_sec"],
    ) = sorted_tppartlist
    (
        lotusdict["LotusSpreadPart0_deg"],
        lotusdict["LotusSpreadPart1_deg"],
        lotusdict["LotusSpreadPart2_deg"],
        lotusdict["LotusSpreadPart3_deg"],
        lotusdict["LotusSpreadPart4_deg"],
        lotusdict["LotusSpreadPart5_deg"],
    ) = sorted_spreadlist
    ######

    for i in range(6):
        if (
            (lotusdict["LotusPdirPart" + str(i) + "_deg"] == 0)
            & (lotusdict["LotusSigHPart" + str(i) + "_mt"] == 0)
            & (lotusdict["LotusTPPart" + str(i) + "_sec"] == 0)
            & (lotusdict["LotusSpreadPart" + str(i) + "_deg"] == 0)
        ):
            lotusdict["LotusPdirPart" + str(i) + "_deg"] = np.nan
            lotusdict["LotusSigHPart" + str(i) + "_mt"] = np.nan
            lotusdict["LotusTPPart" + str(i) + "_sec"] = np.nan
            lotusdict["LotusSpreadPart" + str(i) + "_deg"] = np.nan

    keys = [
        "LotusWindDir_deg",
        "LotusPdirPart0_deg",
        "LotusPdirPart1_deg",
        "LotusPdirPart2_deg",
        "LotusPdirPart3_deg",
        "LotusPdirPart4_deg",
        "LotusPdirPart5_deg",
    ]

    for key in keys:
        rel = np.array(
            get_rel_angles(np.array(lotusdict[key]) - offshoredirection)
        )
        lotusdict[key + "_rel_sin"] = np.sin(
            2 * math.pi * (rel.astype(float) + 180) / 360
        )
        lotusdict[key + "_rel_cos"] = np.cos(
            2 * math.pi * (rel.astype(float) + 180) / 360
        )

    print(f'LOTUSDICT: {lotusdict}')

    return lotusdict


def _construct_key(previous_key, separator, new_key, replace_separators=None):
    """
    Returns the new_key if no previous key exists, otherwise concatenates
    previous key, separator, and new_key
    :param previous_key:
    :param separator:
    :param new_key:
    :param str replace_separators: Replace separators within keys
    :return: a string if previous_key exists and simply passes through the
    new_key otherwise
    """
    if replace_separators is not None:
        new_key = str(new_key).replace(separator, replace_separators)
    if previous_key:
        return u"{}{}{}".format(previous_key, separator, new_key)
    else:
        return new_key


def flatten(
    nested_dict,
    separator="_",
    root_keys_to_ignore=None,
    replace_separators=None,
):
    """
    Flattens a dictionary with nested structure to a dictionary with no
    hierarchy
    Consider ignoring keys that you are not interested in to prevent
    unnecessary processing
    This is specially true for very deep objects
    :param nested_dict: dictionary we want to flatten
    :param separator: string to separate dictionary keys by
    :param root_keys_to_ignore: set of root keys to ignore from flattening
    :param str replace_separators: Replace separators within keys
    :return: flattened dictionary
    """
    assert isinstance(nested_dict, dict), "flatten requires a dictionary input"
    assert isinstance(separator, six.string_types), "separator must be string"

    if root_keys_to_ignore is None:
        root_keys_to_ignore = set()

    if len(nested_dict) == 0:
        return {}

    # This global dictionary stores the flattened keys and values and is
    # ultimately returned
    flattened_dict = dict()

    def _flatten(object_, key):
        """
        For dict, list and set objects_ calls itself on the elements and for
        other types assigns the object_ to
        the corresponding key in the global flattened_dict
        :param object_: object to flatten
        :param key: carries the concatenated key for the object_
        :return: None
        """
        # Empty object can't be iterated, take as is
        if not object_:
            flattened_dict[key] = object_
        # These object types support iteration
        elif isinstance(object_, dict):
            for object_key in object_:
                if not (not key and object_key in root_keys_to_ignore):
                    _flatten(
                        object_[object_key],
                        _construct_key(
                            key,
                            separator,
                            object_key,
                            replace_separators=replace_separators,
                        ),
                    )
        elif isinstance(object_, (list, set, tuple)):
            for index, item in enumerate(object_):
                _flatten(
                    item,
                    _construct_key(
                        key,
                        separator,
                        index,
                        replace_separators=replace_separators,
                    ),
                )
        # Anything left take as is
        else:
            flattened_dict[key] = object_

    _flatten(nested_dict, None)
    return flattened_dict


# def transform_spot_data(spot_data: dict, offshore_direction: int) -> dict:

#     spotinfo_flattened = flatten(spot_data, ".")

#     datadict = {}

#     datadict["spot_offshoredirection_deg"] = offshore_direction

#     key = "spot_offshoredirection_deg"
#     datadict[key + "_sin"] = np.sin(
#         2 * math.pi * np.array(datadict[key]) / 360
#     )
#     datadict[key + "_cos"] = np.cos(
#         2 * math.pi * np.array(datadict[key]) / 360
#     )

#     return datadict


def generate_array(lotusdict):

    features = np.array(
        [
            lotusdict["LotusSigH_mt"],
            lotusdict["LotusWindSpeed_ms"],
            lotusdict["LotusWindDir_deg_rel_sin"],
            lotusdict["LotusWindDir_deg_rel_cos"],
            lotusdict["LotusMinBWH_mt"],
            lotusdict["LotusMaxBWH_mt"],
            lotusdict["LotusSigHPart0_mt"],
            lotusdict["LotusTPPart0_sec"],
            lotusdict["LotusPdirPart0_deg_rel_sin"],
            lotusdict["LotusPdirPart0_deg_rel_cos"],
            lotusdict["LotusSpreadPart0_deg"],
            lotusdict["LotusSigHPart1_mt"],
            lotusdict["LotusTPPart1_sec"],
            lotusdict["LotusPdirPart1_deg_rel_sin"],
            lotusdict["LotusPdirPart1_deg_rel_cos"],
            lotusdict["LotusSpreadPart1_deg"],
            lotusdict["LotusSigHPart2_mt"],
            lotusdict["LotusTPPart2_sec"],
            lotusdict["LotusPdirPart2_deg_rel_sin"],
            lotusdict["LotusPdirPart2_deg_rel_cos"],
            lotusdict["LotusSpreadPart2_deg"],
            lotusdict["LotusSigHPart3_mt"],
            lotusdict["LotusTPPart3_sec"],
            lotusdict["LotusPdirPart3_deg_rel_sin"],
            lotusdict["LotusPdirPart3_deg_rel_cos"],
            lotusdict["LotusSpreadPart3_deg"],
            lotusdict["LotusSigHPart4_mt"],
            lotusdict["LotusTPPart4_sec"],
            lotusdict["LotusPdirPart4_deg_rel_sin"],
            lotusdict["LotusPdirPart4_deg_rel_cos"],
            lotusdict["LotusSpreadPart4_deg"],
            lotusdict["LotusSigHPart5_mt"],
            lotusdict["LotusTPPart5_sec"],
            lotusdict["LotusPdirPart5_deg_rel_sin"],
            lotusdict["LotusPdirPart5_deg_rel_cos"],
            lotusdict["LotusSpreadPart5_deg"],
        ]
    )

    x = np.expand_dims(features, axis=0)

    return x


def build_features(forecast):
    """
    Builds feature set from forecast data.

    The features are determined by the model, and if the model were to change
    this would most likely need to be updated.

    Args:
        sds_data: dict containing forecast data with top level keys:
                  surf, swells, wind, and weather

    Returns:
        array containing features for each timestamp in the forecast data
    """

    # data = sds_data["data"]["surfSpotForecasts"]

    features_set = [
        {
            "LotusSigH_mt": forecast["swells"]["data"][i]["swells"]["combined"][
                "height"
            ]
                            / 3.2808,
            "LotusMinBWH_mt": forecast["surf"]["data"][i]["surf"][
                "breakingWaveHeightMin"
            ]
                              / 3.2808,
            "LotusMaxBWH_mt": forecast["surf"]["data"][i]["surf"][
                "breakingWaveHeightMax"
            ]
                              / 3.2808,
            "LotusWindSpeed_ms": forecast["wind"]["data"][i]["wind"]["speed"]
                                 / 1.9438444924406,
            "LotusWindDir_deg": forecast["wind"]["data"][i]["wind"][
                "direction"
            ],
            "LotusSigHPart0_mt": forecast["swells"]["data"][i]["swells"][
                "components"
            ][0]["height"]
                                 / 3.2808,
            "LotusTPPart0_sec": forecast["swells"]["data"][i]["swells"][
                "components"
            ][0]["period"],
            "LotusPdirPart0_deg": forecast["swells"]["data"][i]["swells"][
                "components"
            ][0]["direction"],
            "LotusSpreadPart0_deg": forecast["swells"]["data"][i]["swells"][
                "components"
            ][0]["spread"],
            "LotusSigHPart1_mt": forecast["swells"]["data"][i]["swells"][
                "components"
            ][1]["height"]
                                 / 3.2808
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 1
            else 0,
            "LotusTPPart1_sec": forecast["swells"]["data"][i]["swells"][
                "components"
            ][1]["period"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 1
            else 0,
            "LotusPdirPart1_deg": forecast["swells"]["data"][i]["swells"][
                "components"
            ][1]["direction"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 1
            else 0,
            "LotusSpreadPart1_deg": forecast["swells"]["data"][i]["swells"][
                "components"
            ][1]["spread"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 1
            else 0,
            "LotusSigHPart2_mt": forecast["swells"]["data"][i]["swells"][
                "components"
            ][2]["height"]
                                 / 3.2808
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 2
            else 0,
            "LotusTPPart2_sec": forecast["swells"]["data"][i]["swells"][
                "components"
            ][2]["period"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 2
            else 0,
            "LotusPdirPart2_deg": forecast["swells"]["data"][i]["swells"][
                "components"
            ][2]["direction"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 2
            else 0,
            "LotusSpreadPart2_deg": forecast["swells"]["data"][i]["swells"][
                "components"
            ][2]["spread"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 2
            else 0,
            "LotusSigHPart3_mt": forecast["swells"]["data"][i]["swells"][
                "components"
            ][3]["height"]
                                 / 3.2808
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 3
            else 0,
            "LotusTPPart3_sec": forecast["swells"]["data"][i]["swells"][
                "components"
            ][3]["period"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 3
            else 0,
            "LotusPdirPart3_deg": forecast["swells"]["data"][i]["swells"][
                "components"
            ][3]["direction"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 3
            else 0,
            "LotusSpreadPart3_deg": forecast["swells"]["data"][i]["swells"][
                "components"
            ][3]["spread"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 3
            else 0,
            "LotusSigHPart4_mt": forecast["swells"]["data"][i]["swells"][
                "components"
            ][4]["height"]
                                 / 3.2808
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 4
            else 0,
            "LotusTPPart4_sec": forecast["swells"]["data"][i]["swells"][
                "components"
            ][4]["period"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 4
            else 0,
            "LotusPdirPart4_deg": forecast["swells"]["data"][i]["swells"][
                "components"
            ][4]["direction"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 4
            else 0,
            "LotusSpreadPart4_deg": forecast["swells"]["data"][i]["swells"][
                "components"
            ][4]["spread"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 4
            else 0,
            "LotusSigHPart5_mt": forecast["swells"]["data"][i]["swells"][
                "components"
            ][5]["height"]
                                 / 3.2808
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 5
            else 0,
            "LotusTPPart5_sec": forecast["swells"]["data"][i]["swells"][
                "components"
            ][5]["period"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 5
            else 0,
            "LotusPdirPart5_deg": forecast["swells"]["data"][i]["swells"][
                "components"
            ][5]["direction"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 5
            else 0,
            "LotusSpreadPart5_deg": forecast["swells"]["data"][i]["swells"][
                "components"
            ][5]["spread"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 5
            else 0,
        }
        for i in range(0, len(forecast["swells"]["data"]))
    ]

    return features_set
