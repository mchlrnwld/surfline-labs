import torch
import torch.nn as nn


class MLP(nn.Module):
    def __init__(self, out_features=1, in_features=61, hidden=2048):
        super().__init__()

        self.hidden = hidden

        j = []
        hid = hidden
        while hid > 32:
            hid = hid / 2
            j.append(int(hid))
        self.j = j

        self.fc1 = nn.Linear(in_features=in_features, out_features=hidden)

        layers = []
        for j in self.j:
            layers.append(nn.Linear(in_features=j * 2, out_features=j))
        self.layers = torch.nn.ModuleList(layers)

        self.output = nn.Linear(in_features=32, out_features=out_features)

    def forward(self, X):

        # Pass through dense layers
        vector = nn.LeakyReLU()(self.fc1(X))

        for l in self.layers:
            vector = nn.LeakyReLU()(l(vector))

        # Output layer
        pred = self.output(vector)

        return pred
