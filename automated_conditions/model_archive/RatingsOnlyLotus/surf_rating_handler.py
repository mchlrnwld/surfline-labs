import logging
import pickle

import numpy as np
import torch
from ts.torch_handler.base_handler import BaseHandler

from nn_architecture import MLP
from util import (
    generate_array,
    modify_lotus_angles,
    build_features,
)

logger = logging.getLogger(__name__)


class SurfRatingHandler(BaseHandler):
    """
    https://github.com/pytorch/serve/blob/master/docs/custom_service.md#advanced-custom-handlers
    """

    def __init__(self):
        super().__init__()
        self._context = None
        self.initialized = False
        self.explain = False
        self.target = 0

    def initialize(self, context):
        """
        Initialize model. This will be called during model loading time
        :param context: Initial context contains model server system properties.
        :return:
        """
        self.device = "cpu"

        # https://pytorch.org/tutorials/beginner/basics/buildmodel_tutorial.html#define-the-class
        self.model = MLP(in_features=36)

        # https://pytorch.org/tutorials/beginner/basics/saveloadrun_tutorial.html#saving-and-loading-model-weights
        modeldict = torch.load(
            "weights.pth", map_location=torch.device("cpu")
        )
        self.model.load_state_dict(modeldict)

        # https://pytorch.org/docs/stable/generated/torch.nn.Module.html#torch.nn.Module.eval
        self.model.eval()

        self.initialized = True

        logger.info(f'Completed initialization of weights.pth')

    def preprocess(self, data):
        request_data = data[0].get("data")
        if request_data is None:
            request_data = data[0].get("body")

        features = build_features(request_data['forecast'])

        tensors = []
        with open("scaler.pkl", 'rb') as f:
            scaler = pickle.load(f)

            for feature in features:
                # Dictionary with angles changed
                lotus_dict = modify_lotus_angles(
                    feature, request_data['offshore']
                )

                # Returns np.array of features
                x = generate_array(lotus_dict)

                x_scaled = scaler.transform(x)
                x_scaled = np.where(np.isnan(x_scaled), scaler.mean_, x_scaled)
                x_tensor = torch.tensor(x_scaled, dtype=torch.float32)

                tensors.append(x_tensor)

        return tensors

    def inference(self, model_inputs, *args, **kwargs):

        with torch.no_grad():
            outputs = [self.model(model_input) for model_input in model_inputs]

        return outputs

    def postprocess(self, inference_outputs):
        return [
            [
                np.round(np.asscalar(inference_output.numpy().squeeze()),2) + 1
                for inference_output in inference_outputs
            ]
        ]
