# PyTorch Model Testing
Smoke testing the `onlylotus` PyTorch model deployed in `surf-ratings-api` ([TorchServe](https://pytorch.org/serve/))

## Files
- `.env.sample` - example environment variables for running the tests
- `environment.yml` - Conda environment for running tests
- `main.py` - python script to request predictions
- `util.py` - utility classes and functions

## Running
1. Copy `.env.sample` to `.env` and fill in the necessary variables.
2. Create and activate the Conda environment:
     ```sh
     $ conda env create -f environment.yml
     $ conda activate test_finetuned_v3
     ```
3. Execute the following command:
    ```sh
    $ env $(xargs < .env) python main.py
    ```

