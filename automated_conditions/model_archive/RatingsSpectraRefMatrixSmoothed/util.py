import math
import numpy as np
from approximate_spectra import approximate_spectra_from_swell_partitions
from swell_partition import SwellPartition


def get_rel_angles(angles):
    angles = angles % 360
    angles = np.where(angles > 180, angles - 360, angles)
    return angles


def modify_lotus_angles(lotusdict: dict, offshoredirection):
    
    key = "LotusWindDir_deg"
    rel = np.array(
        get_rel_angles(np.array(lotusdict[key]) - offshoredirection)
    )
    lotusdict[key + "_rel_sin"] = np.sin(
        2 * math.pi * (rel.astype(float) + 180) / 360
    )
    lotusdict[key + "_rel_cos"] = np.cos(
        2 * math.pi * (rel.astype(float) + 180) / 360
    )

    print(f'LOTUSDICT: {lotusdict}')

    return lotusdict

def generate_array(lotusdict, refractionmatrix):

    minimum_frequency = 0.035
    maximum_frequency = 0.5
    direction_bin_size = 10

    wind = np.array([lotusdict["LotusWindSpeed_ms"], lotusdict["LotusWindDir_deg_rel_sin"], lotusdict["LotusWindDir_deg_rel_cos"]])

    sighlist = [
        lotusdict["LotusSigHPart" + str(i) + "_mt"] for i in np.arange(0, 6)
    ]
    pdirlist = [
        lotusdict["LotusPdirPart" + str(i) + "_deg"] for i in np.arange(0, 6)
    ]
    tppartlist = [
        lotusdict["LotusTPPart" + str(i) + "_sec"] for i in np.arange(0, 6)
    ]
    spreadlist = [
        lotusdict["LotusSpreadPart" + str(i) + "_deg"] for i in np.arange(0, 6)
    ]

    swell_partitions = [
            SwellPartition(height, period, direction, spread)
            for height, period, direction, spread in zip(
                sighlist, tppartlist, pdirlist, spreadlist
                )
            ]

    valid_spectra_wave_energy = approximate_spectra_from_swell_partitions(
            swell_partitions=swell_partitions,
            minimum_frequency=minimum_frequency,
            maximum_frequency=maximum_frequency,
            direction_bin_size=direction_bin_size)

    # valid_spectra_wave_energy = np.sum(valid_spectra_wave_energy, axis=1).squeeze()
        
    spectra = valid_spectra_wave_energy.flatten()

    ref_matrix = np.array(refractionmatrix).flatten()

    features = np.concatenate((wind, ref_matrix, spectra))

    x = np.expand_dims(features, axis=0)

    return x


def build_features(forecast):
    """
    Builds feature set from forecast data.

    The features are determined by the model, and if the model were to change
    this would most likely need to be updated.

    Args:
        sds_data: dict containing forecast data with top level keys:
                  surf, swells, wind, and weather

    Returns:
        array containing features for each timestamp in the forecast data
    """

    # data = sds_data["data"]["surfSpotForecasts"]

    features_set = [
        {
            "LotusSigH_mt": forecast["swells"]["data"][i]["swells"]["combined"][
                "height"
            ]
                            / 3.2808,
            "LotusMinBWH_mt": forecast["surf"]["data"][i]["surf"][
                "breakingWaveHeightMin"
            ]
                              / 3.2808,
            "LotusMaxBWH_mt": forecast["surf"]["data"][i]["surf"][
                "breakingWaveHeightMax"
            ]
                              / 3.2808,
            "LotusWindSpeed_ms": forecast["wind"]["data"][i]["wind"]["speed"]
                                 / 1.9438444924406,
            "LotusWindDir_deg": forecast["wind"]["data"][i]["wind"][
                "direction"
            ],
            "LotusSigHPart0_mt": forecast["swells"]["data"][i]["swells"][
                "components"
            ][0]["height"]
                                 / 3.2808,
            "LotusTPPart0_sec": forecast["swells"]["data"][i]["swells"][
                "components"
            ][0]["period"],
            "LotusPdirPart0_deg": forecast["swells"]["data"][i]["swells"][
                "components"
            ][0]["direction"],
            "LotusSpreadPart0_deg": forecast["swells"]["data"][i]["swells"][
                "components"
            ][0]["spread"],
            "LotusSigHPart1_mt": forecast["swells"]["data"][i]["swells"][
                "components"
            ][1]["height"]
                                 / 3.2808
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 1
            else 0,
            "LotusTPPart1_sec": forecast["swells"]["data"][i]["swells"][
                "components"
            ][1]["period"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 1
            else 0,
            "LotusPdirPart1_deg": forecast["swells"]["data"][i]["swells"][
                "components"
            ][1]["direction"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 1
            else 0,
            "LotusSpreadPart1_deg": forecast["swells"]["data"][i]["swells"][
                "components"
            ][1]["spread"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 1
            else 0,
            "LotusSigHPart2_mt": forecast["swells"]["data"][i]["swells"][
                "components"
            ][2]["height"]
                                 / 3.2808
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 2
            else 0,
            "LotusTPPart2_sec": forecast["swells"]["data"][i]["swells"][
                "components"
            ][2]["period"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 2
            else 0,
            "LotusPdirPart2_deg": forecast["swells"]["data"][i]["swells"][
                "components"
            ][2]["direction"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 2
            else 0,
            "LotusSpreadPart2_deg": forecast["swells"]["data"][i]["swells"][
                "components"
            ][2]["spread"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 2
            else 0,
            "LotusSigHPart3_mt": forecast["swells"]["data"][i]["swells"][
                "components"
            ][3]["height"]
                                 / 3.2808
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 3
            else 0,
            "LotusTPPart3_sec": forecast["swells"]["data"][i]["swells"][
                "components"
            ][3]["period"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 3
            else 0,
            "LotusPdirPart3_deg": forecast["swells"]["data"][i]["swells"][
                "components"
            ][3]["direction"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 3
            else 0,
            "LotusSpreadPart3_deg": forecast["swells"]["data"][i]["swells"][
                "components"
            ][3]["spread"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 3
            else 0,
            "LotusSigHPart4_mt": forecast["swells"]["data"][i]["swells"][
                "components"
            ][4]["height"]
                                 / 3.2808
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 4
            else 0,
            "LotusTPPart4_sec": forecast["swells"]["data"][i]["swells"][
                "components"
            ][4]["period"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 4
            else 0,
            "LotusPdirPart4_deg": forecast["swells"]["data"][i]["swells"][
                "components"
            ][4]["direction"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 4
            else 0,
            "LotusSpreadPart4_deg": forecast["swells"]["data"][i]["swells"][
                "components"
            ][4]["spread"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 4
            else 0,
            "LotusSigHPart5_mt": forecast["swells"]["data"][i]["swells"][
                "components"
            ][5]["height"]
                                 / 3.2808
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 5
            else 0,
            "LotusTPPart5_sec": forecast["swells"]["data"][i]["swells"][
                "components"
            ][5]["period"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 5
            else 0,
            "LotusPdirPart5_deg": forecast["swells"]["data"][i]["swells"][
                "components"
            ][5]["direction"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 5
            else 0,
            "LotusSpreadPart5_deg": forecast["swells"]["data"][i]["swells"][
                "components"
            ][5]["spread"]
            if len(forecast["swells"]["data"][i]["swells"]["components"]) > 5
            else 0,
        }
        for i in range(0, len(forecast["swells"]["data"]))
    ]

    return features_set
