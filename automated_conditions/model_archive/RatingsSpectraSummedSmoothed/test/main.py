import json
import logging
import os
import requests
from timeit import default_timer

from util import (
    get_spot_by_point_of_interest_id,
    get_point_of_interest_by_id,
    pull_sds_data,
    wait_for_torchserve_initialization
)

logger = logging.getLogger(__name__)


def main():
    point_of_interest_id = os.environ['POINT_OF_INTEREST_ID']

    start = default_timer()
    poi = get_point_of_interest_by_id(point_of_interest_id)
    forecast = pull_sds_data(point_of_interest_id)
    elapsed = default_timer() - start
    logger.info(f'Assembled request in {elapsed}s.')

    with open("request.json", "w") as outfile:
        json.dump(
            {
                "offshore": poi.surfSpotConfiguration.offshoreDirection,
                "refractionmatrix": poi.surfSpotConfiguration.spectralRefractionMatrix,
                "forecast": forecast
            },
            outfile,
            indent=2)

    wait_for_torchserve_initialization()

    start = default_timer()
    response = requests.post(
        f"{os.environ['SURF_RATINGS_API']}/predictions/RatingsSpectraSummed",
        json={
            "forecast": forecast,
            "offshore": poi.surfSpotConfiguration.offshoreDirection,
            "refractionmatrix": poi.surfSpotConfiguration.spectralRefractionMatrix
        },
    )
    elapsed = default_timer() - start
    logger.info(f'Completed request in {elapsed}s.')
    logger.info(f'Response: {response.json()}')
    result = response.json()
    logger.info(f'Response length: {len(result)}')
    assert len(result) == len(forecast['surf']['data'])


if __name__ == '__main__':
    logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler())
    main()
