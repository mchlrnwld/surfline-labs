# TorchServe Model Packaging

Packaging a specific model to be registered in the Forecast Engineering [TorchServe API](https://pytorch.org/serve/)

## Structure of this folder

- `finetuned_v3` - all files necessary to create the model archive for the finetuned_v3 model and run a simple test
- `model-store` - folder to hold `.mar` files for local testing (our production API uses an s3 bucket instead)
- `config.properties` - TorchServe configuration for running locally
- `Dockerfile` - blueprint for building TorchServe locally
- `environment.yml` - Conda environment for packaging models and running tests

## Running TorchServe locally

1. Create the docker image:
   ```sh
   $ cd <repository_root>/src/lib/model_archives
   $ docker build -t forecast_torchserve .
   ```
2. Run a docker container:
   ```sh
   $ docker run -d --rm -p 8080:8080 -p 8081:8081 forecast_torchserve
   ```
3. Say hi to the container to make sure it is working:
   ```sh
   $ curl http://localhost:8080/ping
   ```
4. Ask TorchServe which models it is running to make sure your model is there:
   ```sh
   $ curl http://localhost:8081/models
   ```
5. Look at the TorchServe logs if the container shows unhealthy / partial health or if your model is not available:
   ```sh
   $ docker container ls
   $ docker logs -f <your container id or name>
   ```

## Run torchserve tests using docker
1. Execute the following command:
    ```sh
    $ docker-compose -f docker-compose.test.yml up --build
    ```