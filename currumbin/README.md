# Currumbin Analysis

This project contains stub data for the Currumbin surfer tracking trial and 
instructions on using and processing that data.

The code contained here is provided as-is and remains the IP of
Surfline / Wavetrak inc. - it can be used by partners for the analysis of data
but no part of the code or it's derivatives can be redistributed in any form.

The code includes a docker container that builds all dependencies. If you want
to use that you can simply:

```
make build
make notebook
```

Although you'll need `docker` and `make` installed.

The stub data is all in `/src/data` and the main documention / example is in
the notebook `/src/basic_data_usage.ipynb`