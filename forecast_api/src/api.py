from os import environ, lseek
from flask import Flask

import flask.scaffold
flask.helpers._endpoint_from_view_func = flask.scaffold._endpoint_from_view_func
from flask_restful import Api, Resource, request

from marshmallow import Schema, fields

import lib.feature_helpers as feature_helpers
import lib.sds_helpers as sds_helpers
import lib.sql_helpers as sql_helpers
from flask_cors import CORS
from lib.error import handle_error
from lib.model import ConditionsModel
from lib.spot import Spot

from ipdb import set_trace as st

class ForecastQuerySchema(Schema):
    """
    Contains the schema for the forecast api query.

    Just contains spot_id for now, but can be updated in the future.
    """

    spot_id = fields.Str(required=True)


app = Flask(__name__)
CORS(app)
api = Api(app)
schema = ForecastQuerySchema()


class Forecast(Resource):
    """
    Forecast Resource contains the methods for interacting with the
    forecast api.
    """

    def get(self):
        """
        Uses the spot_id parameter passed in the query to pull forecast data
        and make predictions on the conditions.

        Returns:
            sds_data with added conditions ratings
        """
        errors = schema.validate(request.args)
        if errors:
            handle_error(400, str(errors))

        spot = Spot(**request.args)
        sds_data = sds_helpers.pull_sds_data(spot.poi_id)
        tidedata = sds_helpers.get_tide(spot.latitude, spot.longitude, sds_data)
        features = feature_helpers.build_features(spot, sds_data, tidedata)
        condition_preds, algo_condition_preds, smoothed_preds, wind_dir_norm, wind_type_vals, surf_heights = conditions_model.predict(
            features)
        message_data = sds_helpers.add_predictions(
            condition_preds, algo_condition_preds, smoothed_preds, spot.offshore_dir, wind_dir_norm, wind_type_vals, surf_heights, sds_data)

        return message_data


class Health(Resource):
    """
    Health Resource to be used for health checks on the API.

    Works with both the root url / and /health
    """

    def get(self):
        """
        Returns 200 to signal server is live.
        """
        return {"status": 200, "message": "OK", "version": "unknown"}


class Highlights(Resource):
    """
    Forecast Resource contains the methods for interacting with the
    forecast api.
    """

    def get(self):
        """
        Uses the spot_id parameter passed in the query to pull forecast data
        and make predictions on the conditions.

        Returns:
            sds_data with added conditions ratings
        """
        clips_data = sql_helpers.pull_highlight_clips()
        return clips_data


api.add_resource(Health, "/", "/health")
api.add_resource(Forecast, "/forecast", endpoint="spot")
api.add_resource(Highlights, "/highlights")

if __name__ == "__main__":
    conditions_model = ConditionsModel()
    app.run(host="0.0.0.0", use_reloader=True, port=90, debug=False)
