"""
This model in fact is /two/ models, it returns both the results of Francisco's
ratings algorithm, and the corrected results for that spot specific model.

If you have chosen to load a spot that doesn't have an available error correction
model, you're going to get an 'n/a'/bad result for the error correction and a fine
result for Francisco's algorithm.
"""

import os
import json
import pandas as pd
import numpy as np

# import lib.config as config
# from lib.error import handle_error

from tensorflow.keras.models import load_model
# from tensorflow.nn import leaky_relu
import tensorflow as tf


def load_classifier(SpotID):
    try:
        loaded_model = load_model(os.path.join(os.getcwd(), f'lib/models/{SpotID}.model'),
                                  custom_objects={'leaky_relu': tf.nn.leaky_relu})
        # loaded_model = tf.saved_model.load(export_dir=os.path.join(os.getcwd(),
        #                                                            f'lib/models/{SpotID}.model'),
        #                                    tags=None)
    except BaseException:
        raise IOError(f'Could not find model file for {SpotID}.')

    # loaded_model.compile(optimizer='rmsprop',
    #               loss='mean_squared_error',
    #               metrics=['accuracy'])

    return loaded_model


def bulk_algo_rating(wind_dir, spot_offshore_direction, wind_speed_ms,
                     min_BWH_mt, max_BWH_mt, wind_type):
    # Load rating lookup tables
    standard = pd.read_csv('lib/models/algo_rating_tables/rating_list_standard.csv')
    low = pd.read_csv('lib/models/algo_rating_tables/rating_list_low.csv')
    nshore = pd.read_csv('lib/models/algo_rating_tables/rating_list_nshore.csv')
    sshore = pd.read_csv('lib/models/algo_rating_tables/rating_list_sshore.csv')

    algo_preds = []
    for entry in range(0, len(wind_dir)):
        wt = wind_type  # Just one wind type per call b/c 1 spot per call here
        if wt == 'low':
            table = low
        elif wt == 'standard':
            table = standard
        elif wt == 'nshore':
            table = nshore
        elif wt == 'sshore':
            table = sshore
        else:
            table = standard

        wind_dir_norm = int(
            get_dir_diff(
                wind_dir[entry],
                spot_offshore_direction[entry]))
        wind_speed_kts = int(wind_speed_ms[entry])  # actually already in kts
        minBWH_ft = int(min_BWH_mt[entry])  # these are actually already in ft
        maxBWH_ft = int(max_BWH_mt[entry])
        # Min spread of 1', i.e. bumping  4'-4' to 4'-5' as default
        if minBWH_ft == maxBWH_ft:
            maxBWH_ft += 1
        # Francisco says use the top of the range for a larger spread
        if maxBWH_ft - minBWH_ft > 1:
            minBWH_ft = maxBWH_ft - 1  # dumb way to write this but flexible

        if minBWH_ft > 8:
            sub = table.loc[table.Min_BWH_ft == 8]
        else:
            sub = table.loc[table.Min_BWH_ft >= minBWH_ft]
        if maxBWH_ft > 8:
            sub = sub.loc[sub.Max_BWH_ft > maxBWH_ft]
        else:
            sub = sub.loc[sub.Max_BWH_ft <= maxBWH_ft]
        sub = sub.loc[(sub['Max_WindSpeed_kts'] >= wind_speed_kts) &
                      (sub['Min_WindSpeed_kts'] <= wind_speed_kts) &
                      (sub['Max_WindDir_deg'] > wind_dir_norm) &
                      (sub['Min_WindDir_deg'] <= wind_dir_norm)]

        if sub.shape[0] != 1:
            if sub.shape[0] != 2:
                print(wt, minBWH_ft, maxBWH_ft, wind_speed_kts, wind_dir_norm)
                print('Narrowing failed, too many or few matches', sub.shape[0])
                break
        # Added 'max' to handle >1' BWH spread
        rat = max(sub.Rating.values)

        algo_preds.append(rat)

    return algo_preds


def get_dir_diff(wind_dir, offshore_dir):
    dir_diff = wind_dir - offshore_dir
    dir_diff = (dir_diff + 180) % 360 - 180
    return dir_diff


def smooth(predictions, wind_dir,
           wind_speed_kts, min_BWH_ft, max_BWH_ft):
    """
    Smoothes algorithm- or model-predicted results to account for small
    changes in conditions causing large variances.

    Args:
        predictions: A list of predicted ratings (ints).
        wind_dir: List of wind direction values associated with those
            predictions.
        wind_speed_kts: List of associated wind speed values, in knots.
        min_BWH_ft: List of associated minimum breaking wave height
            values, in feet.
        max_BWH_ft: List of associated maximum breaking wave height values,
            in feet.

    Returns:
        A list of predicted rating values with less sensitivity to small
        changes in conditions.
    """
    # Do same pre-processing as for inference
    wind_speed_kts = wind_speed_kts.astype(int)
    min_BWH_ft = min_BWH_ft.astype(int)
    max_BWH_ft = max_BWH_ft.astype(int)

    smoothed = []
    cache_dir = 0
    cache_speed = 0
    cache_rating = 1
    dirflag, spdflag, bwhflag = False, False, False
    for i in range(0, len(wind_dir) - 1):
        if i == 0:
            smoothed.append(predictions[i])
            cache_rating = predictions[i]
            cache_dir, cache_speed = wind_dir[i], wind_speed_kts[i]
            continue
        elif predictions[i] == cache_rating:  # No change from cached rating
            smoothed.append(predictions[i])
            cache_rating = predictions[i]
            cache_dir, cache_speed = wind_dir[i], wind_speed_kts[i]
            continue
        else:  # !! Change in rating
            # Macro: What kind of change?
            levelup = (cache_rating < predictions[i])
            bigger = (min_BWH_ft[i] > min_BWH_ft[i - 1]
                      ) or (max_BWH_ft[i] > max_BWH_ft[i - 1])

            # ! Increase in surf height, increase in rating OK
            if (min_BWH_ft[i] != min_BWH_ft[i - 1]
                ) or (max_BWH_ft[i] != max_BWH_ft[i - 1]):
                if bigger and levelup:
                    smoothed.append(predictions[i])
                    cache_rating = predictions[i]
                    cache_dir, cache_speed = wind_dir[i], wind_speed_kts[i]
                    continue

            # Check if changes in features were small!
            if np.abs(wind_dir[i] - cache_dir) <= 5:
                dirflag = True
            if np.abs(wind_speed_kts[i] - cache_speed) <= 5:
                spdflag = True
            if np.abs(min_BWH_ft[i] - min_BWH_ft[i - 1]
                      ) <= 1 or np.abs(max_BWH_ft[i] - max_BWH_ft[i - 1]) <= 1:
                bwhflag = True

            # If it was just a small changes all around, hold at last rating
            if dirflag and spdflag and bwhflag:
                smoothed.append(cache_rating)
                # Do not update cache yet
                dirflag, spdflag, bwhflag = False, False, False
                continue
            else:  # Otherwise, allow change & update cached values
                smoothed.append(predictions[i])
                cache_rating = predictions[i]
                cache_dir, cache_speed = wind_dir[i], wind_speed_kts[i]
                dirflag, spdflag, bwhflag = False, False, False
                continue

    return smoothed


def predict(features, wind_type_dict):
    # raise IOError(f'{tf.__version__}')

    SpotID = features.SpotId.values[0]
    if SpotID == '5842041f4e65fad6a7708890':
        pipe = True
    else:
        pipe = False

    try:
        clf = load_classifier(SpotID)
        no_model = False
    except IOError:
        no_model = True

    try:
        wind_type = wind_type_dict[SpotID]
    except KeyError:
        wind_type = 'standard'

    f = features
    # Get algorithmic prediction
    SHWRating = bulk_algo_rating(f['GFS_wind_dir'].values,
                                 f['offshore_dir'].values,
                                 f['GFS_wind_speed'].values,
                                 f['human_report_surf_min_ft'].values,
                                 f['human_report_surf_max_ft'].values,
                                 wind_type)
    f['SHWRating'] = SHWRating

    # savespot = os.path.join(os.getcwd(), 'one_pg_algo_debug.csv')
    # f.to_csv(savespot, index=False)

    smoothed_pred = smooth(SHWRating, f['GFS_wind_dir'].values, #f['offshore_dir'].values,
                           f['GFS_wind_speed'].values,
                           f['human_report_surf_min_ft'].values,
                           f['human_report_surf_max_ft'].values,
                           wind_type)
    smoothed_pred = np.asarray(smoothed_pred)

    # Making sure everything is in the correct training order...
    f = f[['LotusSigH',
           'GFS_crossshore_comp', 'GFS_onshore_comp',
           'LotusSigH_part1', 'LotusSigH_part2',
           'LotusSigH_part3', 'LotusSigH_part4',
           'LotusPdir_part1', 'LotusPdir_part2',
           'LotusPdir_part3', 'LotusPdir_part4',
           'LotusSpred_part1', 'LotusSpred_part2',
           'LotusSpred_part3', 'LotusSpred_part4',
           'LotusTp_part1', 'LotusTp_part2',
           'LotusTp_part3', 'LotusTp_part4',
           'SHWRating']]

    f = f.to_numpy()
    # Get model predictions
    no_model = True
    if no_model:
        pred = [0] * f.shape[0]
    else:
        pred = clf.predict(f)
        pred = pred.flatten().astype(int)
        # Error correction postprocessing
        if not pipe:
            pred = [-1 if i < -1 else i for i in pred]
            pred = [1 if i > 1 else i for i in pred]
        # raise IOError(f'pred min: {min(pred)}  pred max: {max(pred)}')
        pred = (np.array(SHWRating) + np.array(pred)).tolist()
        pred = [5 if p > 5 else p for p in pred]
        pred = [1 if p < 1 else p for p in pred]

    # Results to return
    predictions = np.asarray(pred)
    algo_predictions = np.asarray(SHWRating)
    wind_dir_norm = [
        get_dir_diff(
            wind_dir,
            features['offshore_dir'].values[0]) for wind_dir in features['GFS_wind_dir'].values]
    wind_type_vals = np.repeat(wind_type, len(wind_dir_norm))
    return predictions, algo_predictions, smoothed_pred, wind_dir_norm, wind_type_vals
