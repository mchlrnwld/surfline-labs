import mysql.connector

import lib.config as config
from lib.error import handle_error


def pull_highlight_clips():
    db = mysql.connector.connect(
        host=config.HIGHLIGHTS_DB_HOST,
        user=config.HIGHLIGHTS_DB_USER,
        password=config.HIGHLIGHTS_DB_PASS,
        database=config.HIGHLIGHTS_DB,
    )

    response_message = config.HIGHLIGHTS_RESPONSE

    # Build camera_ids list
    for spot in response_message:
        for cam in spot['cameras']:
            sql_resp = query_highlights_for_cam(cam['camera_id'], db)
            cam['clips'] = [
                {
                    'highlight_clip_url': f'{config.HIGHLIGHTS_URL}{row[2]}',
                    'original_clip_url': f'{config.HIGHLIGHTS_ORIGINAL_URL}{row[3]}.mp4',
                    'thumbnail_url': f'{config.HIGHLIGHTS_URL}{row[4]}',
                    'gif_url': f'{config.HIGHLIGHTS_URL}{row[5]}',
                    'timestamp': row[6]
                }
                for row in sql_resp
            ]

    return response_message


def query_highlights_for_cam(cam_id, db):
    cursor = db.cursor()
    try:
        cursor.execute(
            f"SELECT * FROM highlight_clip WHERE camera_id='{cam_id}' "
            'ORDER BY timestamp DESC LIMIT 6'
        )
    except Exception as e:
        handle_error(
            500, f'Exception occurred while pulling higlights from MySQL: {e}'
        )

    return cursor.fetchall()
