import requests
import lib.config as config
from lib.error import handle_error


class Spot:
    """
    Spot to interface with the data pulled from the spots-api for a given spot.

    Args:
        spot_id: spot id to pull data for

    Attributes:
        id: spot_id for the spot
        poi_id: point of interest id for the spot
        offshore_dir: angle for the offshore direction for the spot
    """

    def __init__(self, spot_id):
        self.id = spot_id
        self._pull_spot_data()

    def _pull_spot_data(self):
        """
        Private function that pulls spot data from the spots-api to build out
        the Spot object.
        """
        resp = requests.get(f"{config.SPOTS_API}{self.id}")

        if resp.status_code != 200:
            handle_error(resp.status_code, "Error pulling data from spot api.")

        spot_data = resp.json()

        self.longitude = spot_data["location"]["coordinates"][0]
        self.latitude = spot_data["location"]["coordinates"][1]

        self.poi_id = spot_data["pointOfInterestId"]
        self.offshore_dir = spot_data["offshoreDirection"]
