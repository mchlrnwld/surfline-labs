import requests
import numpy as np
import lib.config as config
from lib.error import handle_error
import time

def get_tide(lat: float, lon: float, sds_data) -> dict:

    timestamps = [
        entry["timestamp"]
        for entry in sds_data["data"]["surfSpotForecasts"]["surf"]["data"]
    ]

    tidedict = {}

    currenttime = int(time.time())

    timestart = str(currenttime)
    timeend = str(currenttime + 72*60*60)

    resp = requests.get(config.TIDE_API+"data?lat="+str(lat)+"&lon="+str(lon)+"&start="+timestart+"&end="+timeend)
    resp = resp.json()

    if resp.get("port", np.nan) == np.nan:

        tidedict["port_distance"] = None
        tidedict["port_distanceunit"] = None
        tidedict["port_minheight"] = None
        tidedict["port_maxheight"] = None
        tidedict["tide_level"] = None
        tidedict["tide_time"] = None
        
    else:
        tidedict["port_distance"] = resp["port"]["distance"]
        tidedict["port_distanceunit"] = resp["port"]["distance_unit"]
        tidedict["port_minheight"] = resp["port"]["maxHeight"]
        tidedict["port_maxheight"]= resp["port"]["minHeight"]

        if not resp.get("levels"):
            tidedict["tide_level"] = None
            tidedict["tide_time"] = None
        else:
            levels = [level["shift"] for level in resp["levels"]]
            leveltimes = [level["time"] for level in resp["levels"]]

            tidelevels =  []
            tidetimes = []
            for timestamp in timestamps:
                timediff = abs(timestamp - np.array(leveltimes))
                closesttidetime = np.argmin(timediff)   
                if timediff[closesttidetime] < 3600:
                    tidelevels.append(levels[closesttidetime])
                    tidetimes.append(leveltimes[closesttidetime])
                else:
                    tidelevels.append(None)
                    tidetimes.append(None)

            tidedict["tide_level"] = tidelevels
            tidedict["tide_time"] = tidetimes

    return tidedict

def pull_sds_data(poi_id):
    """
    Used to pull forecast data from the science data service using the graphql
    query.

    Args:
        poi_id: point of interest id for the spot to pull forecast data for

    Returns:
        the pulled sds data in dict form
    """
    query = f"""
    query {{
        surfSpotForecasts(pointOfInterestId: "{poi_id}") {{
            surf (waveHeight: "ft", interval: 3600) {{
                data {{
                    timestamp
                    surf {{
                      breakingWaveHeightMin
                      breakingWaveHeightMax
                    }}
                }}
            }}
            swells (interval: 3600) {{
                data {{
                    timestamp
                    swells {{
                        combined {{
                            height
                            period
                            direction
                        }}
                        components {{
                            height
                            period
                            direction
                            spread
                        }}
                    }}
                }}
            }}
            weather(agency: "Wavetrak", model: "Blended", grid: "Blended", interval: 3600) {{
                data {{
                    timestamp
                    weather {{
                        conditions
                    }}
                }}
            }}
            wind(agency: "Wavetrak", model: "Blended", grid: "Blended", windSpeed: "knot", interval: 3600) {{
                data {{
                    timestamp
                    wind {{
                        speed
                        direction
                    }}
                }}
            }}
        }}
    }}
    """
    resp = requests.post(config.SDS_API, json={"query": query})

    if resp.status_code != 200:
        handle_error(resp.status_code, "Error pulling SDS data.")

    return resp.json()


def add_predictions(conditions_list, algo_conditions_list, smoothed_conditions_list,
                    offshore_dir, wind_dir_norm, wind_type_vals, surf_heights,
                    sds_data):
    timestamps = [
        entry["timestamp"]
        for entry in sds_data["data"]["surfSpotForecasts"]["surf"]["data"]
    ]

    conditions_data = [
        {"timestamp": timestamps[i],
         "condition": conditions_list[i],
         "algocondition": algo_conditions_list[i],
         "predcondition": conditions_list[i],
         "smoothcondition": smoothed_conditions_list[i],
         "wind_dir_norm": wind_dir_norm[i],
         "offshore_dir": offshore_dir,
         "wind_type": wind_type_vals[i],
         "surf_height": surf_heights[i]}
        for i in range(0, len(timestamps))
    ]

    sds_data["data"]["surfSpotForecasts"]["conditions"] = {
        "data": conditions_data
    }

    return sds_data
