# Optimal Conditions Classifier
import pandas as pd
import numpy as np
from statistics import mean
"""
Need as input:
- first partition period
- first partition direction
- wave height
- wind speed
- wind direction
- tide

"""


def assign_bin_number(rating):
    if rating == 0:  # FLAT
        return rating
    if rating == 1:  # RED
        return 10
    if rating == 2:  # YELLOW
        return 11
    if rating == 3:  # GREEN
        return 12


def check_wind(opt, wind_dir, wind_speed):
    # Check wind direction
    for i in range(1, 3):
        low = opt[f'wind_dir_{i}_min']
        high = opt[f'wind_dir_{i}_max']
        if not np.isnan(low):
            if low <= wind_dir <= high:
                dir_score = 3
                break
            else:
                dir_score = 2
        else:
            dir_score = 2

    # Check wind speed
    # TODO : MSW style wind speed assessment?
    if wind_speed > 30:
        speed_score = 1
    elif wind_speed < 4:
        # If the wind is that light it prolly doesn't matter the direction...
        return [3, 3]
        # speed_score = 3
    else:
        speed_score = 2

    return [dir_score, speed_score]


def check_surf(opt, surf_height):
    if opt['size_min'] <= surf_height <= opt['size_max']:
        return [3]
    elif surf_height < 1:
        return [1]
    else:
        return [2]


def check_swell(opt, period, direction):
    # Check swell direction
    for i in range(1, 3):
        low = opt[f'swell_dir_{i}_min']
        high = opt[f'swell_dir_{i}_max']
        if not np.isnan(low):
            if low <= direction <= high:
                dir_score = 3
                break
            else:
                dir_score = 2
        else:
            dir_score = 2
    # Check swell period
    if opt['swell_per_min'] <= period <= opt['swell_per_max']:
        per_score = 3
    else:
        per_score = 2
    return [per_score, dir_score]


def predict(features):
    predictions = []
    try:
        opt_cond = pd.read_csv('lib/highlighted_cond.csv')
    except Exception:
        raise IOError('Could not load csv')

    for f in features:
        opt = opt_cond.loc[opt_cond.spot_id == f['SpotId']].to_dict('records')[0]

        scores = []
        wind = check_wind(opt, f['GFS_wind_dir_norm'], f['GFS_wind_speed'])
        surf = check_surf(opt, f['human_report_surf_max_ft'])
        swell = check_swell(opt, f['LotusTp_part1'], f['LotusPdir_norm_part1'])

        scores = wind + surf + swell
        rating = np.round(mean(scores))
        if 0 in scores:
            rating = 0

        predictions.append(assign_bin_number(rating))
    predictions = np.asarray(predictions)
    return predictions
