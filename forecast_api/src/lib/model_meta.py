import torch
import numpy as np
import pickle
import pandas as pd
import os
import json
from .data_wrangling import get_rel_wind_direction, get_spotdata, get_lotus_rating, modify_lotus_angles, generate_array, generate_array_lotusonly
from .nn_architectures import MLP_orig_param
from .model_err import smooth

from ipdb import set_trace as st


def single_predict(spotid, features, wind_type_dict, tidedict):
    # Path to model accessories
    metamodel_path = os.path.join(os.getcwd(), 'lib', 'ratings_inference/')
    spotdict = get_spotdata(spotid)

    lotusdict = features
    # Dictionary with angles changed
    lotusdict = modify_lotus_angles(
        lotusdict, spotdict["spot_offshoredirection_deg"])

    # Returns a max(column.values)
    lotusrating = np.asscalar(
        get_lotus_rating(
            spotid,
            lotusdict,
            spotdict["spot_offshoredirection_deg"]))


    # Returns np.array of features
    X = generate_array(spotdict, lotusdict, tidedict)

    with open(os.path.join(metamodel_path, "scaler_v4.pkl"), 'rb') as f:
    # with open(os.path.join(metamodel_path, "scaler_v3_shuffled.pkl"), 'rb') as f:
        scaler = pickle.load(f)
    X_scaled = scaler.transform(X)
    X_scaled = np.where(np.isnan(X_scaled), scaler.mean_, X_scaled)
    X_tensor = torch.tensor(X_scaled, dtype=torch.float32)

    model = MLP_orig_param()
    modeldict = torch.load(os.path.join(metamodel_path, "finetuned_v4.pth"), map_location=torch.device("cpu"))
    model.load_state_dict(modeldict)
    model.eval()
    with torch.no_grad():
        output = model(X_tensor)
    prediction = np.round(np.asscalar(output.numpy().squeeze()))

    model_surfheight = MLP_orig_param(out_features=2)
    modeldict_surfheight = torch.load(os.path.join(metamodel_path, "weights_surfheight_v4.pth"), map_location=torch.device("cpu"))
    model_surfheight.load_state_dict(modeldict_surfheight)
    model_surfheight.eval()
    with torch.no_grad():
        output_surfheight = model_surfheight(X_tensor)

    prediction_surfheight = output_surfheight.numpy().squeeze()

    with open("wind_type.json") as f:
        winddata = json.load(f)
        wind_type = winddata[spotid]

    wind_dir_norm = np.asscalar(
        get_rel_wind_direction(
            lotusdict,
            spotdict['spot_offshoredirection_deg']).squeeze())
    # wind_type_vals = np.repeat(windtype, len(features))

    return prediction, lotusrating, wind_dir_norm, wind_type, prediction_surfheight

def predict(features, wind_type_dict):
    spotid, feature_dict, tide_dict = features

    data = pd.DataFrame(feature_dict)

    predictions, lotusratings, wind_dir_norm, wind_type_vals, surfheights = [], [], [], [], []
    for featdict, tidedict in zip(feature_dict, tide_dict):
        pred, lotus, wdn, wtv, ht = single_predict(
            spotid, featdict, wind_type_dict, tidedict)
        predictions.append(pred)
        lotusratings.append(lotus)
        wind_dir_norm.append(wdn)
        wind_type_vals.append(wtv)
        surfheights.append(ht)

    # print('len predictions: ', len(predictions))
    # smoothed_predictions = predictions
    smoothed_predictions = smooth(predictions, data['LotusWindDir_deg'], data['LotusWindSpeed_ms'] * 1.94,
                                  data['LotusMinBWH_mt'] * 3.28, data['LotusMaxBWH_mt'] * 3.28)
    # assert len(smoothed_predictions) == len(predictions)
    while len(smoothed_predictions) < len (predictions):
        smoothed_predictions.append(10)

    return np.asarray(predictions).squeeze(), np.asarray(lotusratings).squeeze(
    ), np.asarray(smoothed_predictions).squeeze(), wind_dir_norm, wind_type_vals, np.asarray(surfheights).squeeze()
