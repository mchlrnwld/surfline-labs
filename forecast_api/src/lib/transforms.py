import os
import numpy as np
import pandas as pd

def bulk_algo_rating(wind_dir, spot_offshore_direction, wind_speed_ms,
                     min_BWH_mt, max_BWH_mt, wind_type):
    # Load rating lookup tables
    table_dir = os.path.join(os.getcwd(), 'lib', 'ratings_inference', 'algo_rating_tables')
    standard = pd.read_csv(f'{table_dir}/rating_list_standard.csv')
    low = pd.read_csv(f'{table_dir}/rating_list_low.csv')
    nshore = pd.read_csv(f'{table_dir}/rating_list_nshore.csv')
    sshore = pd.read_csv(f'{table_dir}/rating_list_sshore.csv')

    counter = 0
    algo_preds = []

    wt = wind_type
    if wt == 'low':
        table = low
    elif wt == 'standard':
        table = standard
    elif wt == 'North Shore Oahu':
        table = nshore
    elif wt == 'South Shore Oahu':
        table = sshore
    else:
        table = standard

    wind_dir_norm = int(
        get_dir_diff(
            wind_dir,
            spot_offshore_direction))
    wind_speed_kts = int(wind_speed_ms * 1.94)
    minBWH_ft = int(min_BWH_mt * 3.28)
    maxBWH_ft = int(max_BWH_mt * 3.28)
    # Min spread of 1', i.e. bumping  4'-4' to 4'-5' as default
    try:
        if minBWH_ft > maxBWH_ft:
            if minBWH_ft - maxBWH_ft == 1:
                minBWH_ft = maxBWH_ft
    except BaseException:
        print('min', minBWH_ft, 'max', max_BWH_mt)
        raise Exception
    if minBWH_ft == maxBWH_ft:
        maxBWH_ft += 1
    if maxBWH_ft - minBWH_ft > 1:  # take care of 4'-6', for instance
        minBWH_ft = maxBWH_ft - 1

    if minBWH_ft > 8:
        sub = table.loc[table.Min_BWH_ft == 8]
    else:
        sub = table.loc[table.Min_BWH_ft >= minBWH_ft]
    if maxBWH_ft > 8:
        sub = sub.loc[sub.Max_BWH_ft > maxBWH_ft]
    else:
        sub = sub.loc[sub.Max_BWH_ft <= maxBWH_ft]

    sub = sub.loc[(sub['Max_WindSpeed_kts'] >= wind_speed_kts) &
                    (sub['Min_WindSpeed_kts'] <= wind_speed_kts) &
                    (sub['Max_WindDir_deg'] > wind_dir_norm) &
                    (sub['Min_WindDir_deg'] <= wind_dir_norm)]


    if sub.shape[0] != 1:
        if sub.shape[0] != 2:
            print(wt, minBWH_ft, maxBWH_ft, wind_speed_kts, wind_dir_norm)
            print('Narrowing failed, too many or few matches', sub.shape[0])

    # Added 'max' to handle >1' BWH spread
    rat = max(sub.Rating.values)

    return rat


def get_dir_diff(wind_dir, offshore_dir):
    dir_diff = np.array(wind_dir) - offshore_dir
    dir_diff = (dir_diff + 180) % 360 - 180
    return dir_diff


def calculate_ratings_from_model(
        wind_dir_norm, wind_speed, min_BWH, max_BWH, peak_period=0):
    # Taken from calculate_ratings_version1.2.py from Francisco
    """
    Method to calculate the ratings based on Lotus output data. The method was
    based on the results obtained from the report observations. For the period
    between 2012 and 2021, forecasters report observations were overlapped with
    NAM model data and created a correlation matrix between Mean breaking wave
    height, wind speed, wind direction and orientation to the spot's "Natural"
    offshore direction.

    From that initial data exploration, the boundaries for the ratings have
    been defined, based on the probability of occurence of each specific set
    of conditions.

    The rating goes from 1 to 5, following this cookbook:

        1 . Don't go - [Flat > Very Poor]
        2 . Maybe if you are really keen - [Poor]
        3 . Marginal - [Poor to Fair]
        4 . Enjoyable - [Fair]
        5 . Good - [Fair to Good +]

    -----------------------------------------

    Input Parameters:

    wind_dir_norm = wind direction normalized to spot's offshore direction
                    as 0 [degrees]
    wind_speed = wind speed [knots]
    min_BWH = min breaking wave height [ft]
    max_BWH = max breaking wave height [ft]
    peak_period = peak period for the partition that dominates the BWH [sec]
    Output:

    rating [1-5]

    """

    rating = 1
    dir_diff = wind_dir_norm

    mean_BWH = np.mean([min_BWH, max_BWH])

    # For Mean BWH below 1 ft
    if mean_BWH < 1:
        pass  # Rating is always 1

    # For Mean BWH between 1 and 2 ft
    if mean_BWH >= 1 and mean_BWH < 2:

        if wind_speed <= 15:
            rating = 2
        if wind_speed <= 20 and dir_diff >= -120 and dir_diff <= 120:
            rating = 2

        if wind_speed <= 10 and peak_period >= 15:
            rating = 3

        if wind_speed <= 10 and dir_diff >= -90 and dir_diff <= 90:
            rating = 3
        if wind_speed <= 10 and dir_diff >= -10 and dir_diff <= 10:
            rating = 3

    # For Mean BWH between 2 and 3 ft
    if mean_BWH >= 2 and mean_BWH < 3:

        if wind_speed <= 20:
            rating = 2
        if wind_speed <= 30 and dir_diff >= -120 and dir_diff <= 120:
            rating = 2

        if wind_speed <= 10:
            rating = 3
        if wind_speed <= 20 and dir_diff >= -60 and dir_diff <= 60:
            rating = 3

        if wind_speed <= 10 and peak_period >= 15:
            rating = 4

        if wind_speed <= 5 and dir_diff >= -70 and dir_diff <= 70:
            rating = 4
        if wind_speed <= 10 and dir_diff >= -50 and dir_diff <= 50:
            rating = 4
        if wind_speed <= 15 and dir_diff >= -40 and dir_diff <= 40:
            rating = 4
        if wind_speed <= 20 and dir_diff >= -20 and dir_diff <= 20:
            rating = 4

    # For Mean BWH between 3 and 4 ft
    if mean_BWH >= 3 and mean_BWH < 4:

        if wind_speed <= 30:
            rating = 2
        if wind_speed <= 30 and dir_diff >= -120 and dir_diff <= 120:
            rating = 2

        if wind_speed <= 10:
            rating = 3
        if wind_speed <= 30 and dir_diff >= -60 and dir_diff <= 60:
            rating = 3

        if wind_speed <= 10 and peak_period >= 15:
            rating = 4

        if wind_speed <= 5:
            rating = 4
        if wind_speed <= 10 and dir_diff >= -60 and dir_diff <= 60:
            rating = 4
        if wind_speed <= 15 and dir_diff >= -50 and dir_diff <= 50:
            rating = 4
        if wind_speed <= 25 and dir_diff >= -40 and dir_diff <= 40:
            rating = 4

        if wind_speed <= 10 and dir_diff >= -20 and dir_diff <= 20 and peak_period >= 15:
            rating = 5

    # For Mean BWH between 4 and 5 ft
    if mean_BWH >= 4 and mean_BWH < 5:

        if wind_speed <= 25:
            rating = 2
        if wind_speed <= 30 and dir_diff >= -120 and dir_diff <= 120:
            rating = 2

        if wind_speed <= 10:
            rating = 3
        if wind_speed <= 30 and dir_diff >= -60 and dir_diff <= 60:
            rating = 3

        if wind_speed <= 5:
            rating = 4
        if wind_speed <= 10 and dir_diff >= -60 and dir_diff <= 60:
            rating = 4
        if wind_speed <= 15 and dir_diff >= -50 and dir_diff <= 50:
            rating = 4
        if wind_speed <= 25 and dir_diff >= -40 and dir_diff <= 40:
            rating = 4

        if wind_speed <= 10 and dir_diff >= -30 and dir_diff <= 30:
            rating = 5
        if wind_speed <= 15 and dir_diff >= -20 and dir_diff <= 20:
            rating = 5
        if wind_speed <= 20 and dir_diff >= -10 and dir_diff <= 10:
            rating = 5

    # For Mean BWH between 5 and 6 ft
    if mean_BWH >= 5 and mean_BWH < 6:

        if wind_speed <= 20:
            rating = 2
        if wind_speed <= 30 and dir_diff >= -120 and dir_diff <= 120:
            rating = 2

        if wind_speed <= 10:
            rating = 3
        if wind_speed <= 30 and dir_diff >= -60 and dir_diff <= 60:
            rating = 3

        if wind_speed <= 5:
            rating = 4
        if wind_speed <= 10 and dir_diff >= -60 and dir_diff <= 60:
            rating = 4
        if wind_speed <= 15 and dir_diff >= -50 and dir_diff <= 50:
            rating = 4
        if wind_speed <= 25 and dir_diff >= -40 and dir_diff <= 40:
            rating = 4

        if wind_speed <= 10 and dir_diff >= -40 and dir_diff <= 40:
            rating = 5
        if wind_speed <= 15 and dir_diff >= -30 and dir_diff <= 30:
            rating = 5
        if wind_speed <= 25 and dir_diff >= -10 and dir_diff <= 10:
            rating = 5

    # For Mean BWH between 6 and 7 ft
    if mean_BWH >= 6 and mean_BWH < 7:

        if wind_speed <= 15:
            rating = 2
        if wind_speed <= 25 and dir_diff >= -100 and dir_diff <= 100:
            rating = 2

        if wind_speed <= 10:
            rating = 3
        if wind_speed <= 30 and dir_diff >= -60 and dir_diff <= 60:
            rating = 3

        if wind_speed <= 5:
            rating = 4
        if wind_speed <= 10 and dir_diff >= -60 and dir_diff <= 60:
            rating = 4
        if wind_speed <= 15 and dir_diff >= -50 and dir_diff <= 50:
            rating = 4
        if wind_speed <= 25 and dir_diff >= -40 and dir_diff <= 40:
            rating = 4

        if wind_speed <= 10 and dir_diff >= -30 and dir_diff <= 30:
            rating = 5
        if wind_speed <= 15 and dir_diff >= -20 and dir_diff <= 20:
            rating = 5
        if wind_speed <= 20 and dir_diff >= -10 and dir_diff <= 10:
            rating = 5

    # For Mean BWH between 7 and 8 ft
    if mean_BWH >= 7 and mean_BWH < 8:

        if wind_speed <= 15:
            rating = 2
        if wind_speed <= 25 and dir_diff >= -100 and dir_diff <= 100:
            rating = 2

        if wind_speed <= 10 and dir_diff >= -120 and dir_diff <= 120:
            rating = 3
        if wind_speed <= 20 and dir_diff >= -50 and dir_diff <= 50:
            rating = 3

        if wind_speed <= 5:
            rating = 4
        if wind_speed <= 10 and dir_diff >= -50 and dir_diff <= 50:
            rating = 4
        if wind_speed <= 15 and dir_diff >= -30 and dir_diff <= 30:
            rating = 4
        if wind_speed <= 25 and dir_diff >= -20 and dir_diff <= 20:
            rating = 4

        if wind_speed <= 10 and dir_diff >= -20 and dir_diff <= 20:
            rating = 5
        if wind_speed <= 15 and dir_diff >= -10 and dir_diff <= 10:
            rating = 5

    # For Mean BWH above 8 ft
    if mean_BWH >= 8:

        if wind_speed <= 105:
            rating = 2
        if wind_speed <= 20 and dir_diff >= -90 and dir_diff <= 90:
            rating = 2

        if wind_speed <= 10 and dir_diff >= -80 and dir_diff <= 80:
            rating = 3
        if wind_speed <= 20 and dir_diff >= -40 and dir_diff <= 40:
            rating = 3

        if wind_speed <= 5:
            rating = 4
        if wind_speed <= 10 and dir_diff >= -40 and dir_diff <= 40:
            rating = 4
        if wind_speed <= 15 and dir_diff >= -30 and dir_diff <= 30:
            rating = 4
        if wind_speed <= 20 and dir_diff >= -20 and dir_diff <= 20:
            rating = 4

        if wind_speed <= 10 and dir_diff >= -10 and dir_diff <= 10:
            rating = 5

    return rating
