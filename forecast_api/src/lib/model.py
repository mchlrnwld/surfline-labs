import os
import sys
import pandas as pd
import xgboost as xgb
import json

import lib.config as config
from lib.error import handle_error
# from lib.optimal_conditions_classifier import predict as optpred
# from lib.model_dtclf import predict as dtpredict
# from lib.model_err import predict as errpredict
from lib.model_meta import predict as metapredict

from ipdb import set_trace as st


class ConditionsModel:
    """
    ConditionsModel is used to initialize/load the model, and then make
    predictions with that model.

    Attributes:
        model: xgbClassifier model to make predictions
    """

    def __init__(self):
        self.model = xgb.XGBClassifier()
        # self.model.load_model(
            # os.path.join(os.getcwd(), f"models/{config.MODEL_FILE}")
        # )
        with open('wind_type.json', 'r') as f:
            self.wind_type_dict = json.load(f)

    def predict(self, features_set):
        """
        Makes predictions on an array of features. Uses the bin_labels to
        map the prediction output to a conditions enum.

        Args:
            features_set: array of features to make predictions on

        Returns:
            array of condition results
        """
        bin_labels = [
            "FLAT",
            "VERY_POOR",
            "POOR",
            "POOR_TO_FAIR",
            "FAIR",
            "FAIR_TO_GOOD",
            "GOOD",
            "VERY_GOOD",
            "GOOD_TO_EPIC",
            "EPIC",
            "N/A",
            "RED",
            "YELLOW",
            "GREEN"
        ]

        # pred_data = pd.DataFrame([entry for entry in features_set])
        # Array of features
        pred_data = features_set
        # Previous: 
        # pred_data = pd.DataFrame(features_set)

        try:
            # pred_results = self.model.predict(pred_data)
            # pred_results = optpred(pred_data)
            pred_results, alg_results, smoothed_results, wind_dir_norm, wind_type_vals, surf_heights = metapredict(
                pred_data, self.wind_type_dict)

        except Exception as exc:
            raise RuntimeError("Error making predictions with model.") from exc


        print('pred results dtype', pred_results.dtype, file=sys.stdout)
        # Convert numbers to bin labels
        # st()
        conditions_list = [
            bin_labels[int(res)] for res in pred_results.tolist()
        ]
        alg_conditions_list = [
            bin_labels[int(res)] for res in alg_results.tolist()
        ] 
        smoothed_conditions_list = [
            bin_labels[int(res)] for res in smoothed_results.tolist()
        ]

        return conditions_list, alg_conditions_list, smoothed_conditions_list, wind_dir_norm, wind_type_vals, surf_heights.tolist()
