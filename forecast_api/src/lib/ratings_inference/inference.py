import torch
import numpy as np
import pickle
from data_wrangling import get_spotdata, get_lotus_rating, modify_lotus_angles, generate_array
from nn_architectures import MLP_orig_param

if __name__ == '__main__':


    spotid = "584204204e65fad6a7709990"

    spotdict = get_spotdata(spotid)

    lotusdict = ...

    lotusdict = modify_lotus_angles(lotusdict, spotdict["spot_offshoredirection_deg"])

    lotusrating = get_lotus_rating(spotid, lotusdict, spotdict["spot_offshoredirection_deg"])

    X = generate_array(spotdict, lotusdict, lotusrating)

    with open("scaler.pkl", 'rb') as f:
        scaler = pickle.load(f)
    X_scaled = scaler.transform(X)

    X_tensor = torch.tensor(X_scaled, dtype=torch.float32)
    
    model = MLP_orig_param()
    modeldict, _ = torch.load("finetuned.pth", map_location=torch.device("cpu"))
    model.load_state_dict(modeldict)
    model.eval()
    with torch.no_grad():
        output = model(X_tensor)

    print(output.numpy().squeeze())

