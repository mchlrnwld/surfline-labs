import numpy as np

from lib.error import handle_error


def build_features(spot, sds_data, tide_data):
    """
    Builds feature set from forecast data.

    The features are determined by the model, and if the model were to change
    this would most likely need to be updated.

    Args:
        sds_data: dict containing forecast data with top level keys:
                  surf, swells, wind, and weather

    Returns:
        array containing features for each timestamp in the forecast data
    """

    data = sds_data["data"]["surfSpotForecasts"]

    try:
        features_set = [
            {
                "LotusSigH_mt": data["swells"]["data"][i]["swells"]["combined"]["height"] / 3.2808,

                "LotusMinBWH_mt": data["surf"]["data"][i]["surf"]["breakingWaveHeightMin"] / 3.2808,
                "LotusMaxBWH_mt": data["surf"]["data"][i]["surf"]["breakingWaveHeightMax"] / 3.2808,

                "LotusWindSpeed_ms": data["wind"]["data"][i]["wind"]["speed"] / 1.9438444924406,
                "LotusWindDir_deg": data["wind"]["data"][i]["wind"]["direction"],

                "LotusSigHPart0_mt": data["swells"]["data"][i]["swells"]["components"][0]["height"] / 3.2808,
                "LotusTPPart0_sec": data["swells"]["data"][i]["swells"]["components"][0]["period"],
                "LotusPdirPart0_deg": data["swells"]["data"][i]["swells"]["components"][0]["direction"],
                "LotusSpreadPart0_deg": data["swells"]["data"][i]["swells"]["components"][0]["spread"],

                "LotusSigHPart1_mt": data["swells"]["data"][i]["swells"]["components"][1]["height"] / 3.2808 if len(data["swells"]["data"][i]["swells"]["components"]) > 1 else 0,
                "LotusTPPart1_sec": data["swells"]["data"][i]["swells"]["components"][1]["period"] if len(data["swells"]["data"][i]["swells"]["components"]) > 1 else 0,
                "LotusPdirPart1_deg": data["swells"]["data"][i]["swells"]["components"][1]["direction"] if len(data["swells"]["data"][i]["swells"]["components"]) > 1 else 0,
                "LotusSpreadPart1_deg": data["swells"]["data"][i]["swells"]["components"][1]["spread"] if len(data["swells"]["data"][i]["swells"]["components"]) > 1 else 0,

                "LotusSigHPart2_mt": data["swells"]["data"][i]["swells"]["components"][2]["height"] / 3.2808 if len(data["swells"]["data"][i]["swells"]["components"]) > 2 else 0,
                "LotusTPPart2_sec": data["swells"]["data"][i]["swells"]["components"][2]["period"] if len(data["swells"]["data"][i]["swells"]["components"]) > 2 else 0,
                "LotusPdirPart2_deg": data["swells"]["data"][i]["swells"]["components"][2]["direction"] if len(data["swells"]["data"][i]["swells"]["components"]) > 2 else 0,
                "LotusSpreadPart2_deg": data["swells"]["data"][i]["swells"]["components"][2]["spread"] if len(data["swells"]["data"][i]["swells"]["components"]) > 2 else 0,

                "LotusSigHPart3_mt": data["swells"]["data"][i]["swells"]["components"][3]["height"] / 3.2808 if len(data["swells"]["data"][i]["swells"]["components"]) > 3 else 0,
                "LotusTPPart3_sec": data["swells"]["data"][i]["swells"]["components"][3]["period"] if len(data["swells"]["data"][i]["swells"]["components"]) > 3 else 0,
                "LotusPdirPart3_deg": data["swells"]["data"][i]["swells"]["components"][3]["direction"] if len(data["swells"]["data"][i]["swells"]["components"]) > 3 else 0,
                "LotusSpreadPart3_deg": data["swells"]["data"][i]["swells"]["components"][3]["spread"] if len(data["swells"]["data"][i]["swells"]["components"]) > 3 else 0,

                "LotusSigHPart4_mt": data["swells"]["data"][i]["swells"]["components"][4]["height"] / 3.2808 if len(data["swells"]["data"][i]["swells"]["components"]) > 4 else 0,
                "LotusTPPart4_sec": data["swells"]["data"][i]["swells"]["components"][4]["period"] if len(data["swells"]["data"][i]["swells"]["components"]) > 4 else 0,
                "LotusPdirPart4_deg": data["swells"]["data"][i]["swells"]["components"][4]["direction"] if len(data["swells"]["data"][i]["swells"]["components"]) > 4 else 0,
                "LotusSpreadPart4_deg": data["swells"]["data"][i]["swells"]["components"][4]["spread"] if len(data["swells"]["data"][i]["swells"]["components"]) > 4 else 0,

                "LotusSigHPart5_mt": data["swells"]["data"][i]["swells"]["components"][5]["height"] / 3.2808 if len(data["swells"]["data"][i]["swells"]["components"]) > 5 else 0,
                "LotusTPPart5_sec": data["swells"]["data"][i]["swells"]["components"][5]["period"] if len(data["swells"]["data"][i]["swells"]["components"]) > 5 else 0,
                "LotusPdirPart5_deg": data["swells"]["data"][i]["swells"]["components"][5]["direction"] if len(data["swells"]["data"][i]["swells"]["components"]) > 5 else 0,
                "LotusSpreadPart5_deg": data["swells"]["data"][i]["swells"]["components"][5]["spread"] if len(data["swells"]["data"][i]["swells"]["components"]) > 5 else 0,
                }
            for i in range(0, len(data["swells"]["data"]))
        ]

        tide_dataset = [
            {"port_minheight": tide_data["port_minheight"], 
            "port_maxheight": tide_data["port_maxheight"],
            "tide_level": tide_data["tide_level"][i]}
            for i in range(0, len(data["swells"]["data"]))
        ]
        
    except Exception as e:
        handle_error(
            500,
            f'Exception while building feature set: {e}'
        )

    return spot.id, features_set, tide_dataset


def get_relative_angle(angle, shore_dir):
    """
    Calculates the relative angle of the shore direction.
    """
    a = 360 - shore_dir
    b = (angle + a) % 360
    return b


def flip_180(angle):
    return angle - 180 if angle >= 180 else angle + 180
