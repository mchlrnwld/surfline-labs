import os

SPOTS_API = os.environ["SPOTS_API"]
SDS_API = os.environ["SDS_API"]
TIDE_API=os.environ["TIDE_API"]
MODEL_FILE = os.environ["MODEL_FILE"]
HIGHLIGHTS_URL = os.environ["HIGHLIGHTS_URL"]
HIGHLIGHTS_ORIGINAL_URL = os.environ["HIGHLIGHTS_ORIGINAL_URL"]
HIGHLIGHTS_DB_HOST = os.environ["HIGHLIGHTS_DB_HOST"]
HIGHLIGHTS_DB = os.environ["HIGHLIGHTS_DB"]
HIGHLIGHTS_DB_USER = os.environ["HIGHLIGHTS_DB_USER"]
HIGHLIGHTS_DB_PASS = os.environ["HIGHLIGHTS_DB_PASS"]

HIGHLIGHTS_RESPONSE = [
    {
        'spot_id': '5842041f4e65fad6a7708827',
        'name': 'HB Pier, Northside',
        'timezone': 'America/Los_Angeles',
        'cameras': [
            {
                'camera_id': '583499c4e411dc743a5d5296',
                'name': 'OC - HB Pier, Northside',
                'clips': [],
            }
        ],
    },
    {
        'spot_id': '5842041f4e65fad6a770882e',
        'name': 'Salt Creek',
        'timezone': 'America/Los_Angeles',
        'cameras': [
            {
                'camera_id': '58349fe73421b20545c4b57e',
                'name': 'OC - Salt Creek',
                'clips': [],
            }
        ],
    }
]
