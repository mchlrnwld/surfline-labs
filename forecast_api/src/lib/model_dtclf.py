import os
import json
import pickle
import math
import pandas as pd
import numpy as np

import lib.config as config
from lib.error import handle_error

from sklearn.tree import DecisionTreeClassifier


def get_spot_vocab():
    try:
        jsonpath = os.path.join(os.getcwd(), "models/spot_vocabulary_dict.json")
        with open(jsonpath) as j:
            vocab = json.load(j)
    except:
        raise IOError('Could not find spot vocabulary.')
    return vocab


def load_classifier():
    try:
        clfpkl = os.path.join(os.getcwd(), "models/dtclf.pkl")
        clf = pickle.load(open(clfpkl, 'rb'))
    except:
        raise IOError('Could not find pickled decision tree.')
    return clf


def trig_encode_angles(dat):
    # Find which columns need encoding.
    cols = list(dat.columns)
    angle_columns = [a for a in cols if 'dir' in a]

    for col in angle_columns:
        dat[f'{col}_sin'] = dat[col].astype(float).apply(math.sin)
        dat[f'{col}_cos'] = dat[col].astype(float).apply(math.cos)
    dat.drop(angle_columns, axis=1, inplace=True)

    return dat


def predict(features):
    clf = load_classifier()
    predictions = []
    spot_vocab = get_spot_vocab()
    for f in features:
        # integer-encode spot IDs
        f['SpotId'] = spot_vocab[f['SpotId']]
        cols = list(f.keys())
        # f = pd.Series(f).to_frame(columns=cols)
        f = pd.DataFrame([f])
        f = trig_encode_angles(f)

        # Making sure everything is in the correct training order...
        f = f[['SpotId', 'LotusSigH', 'LotusSigH_part1', 'LotusTp_part1',
               'LotusSpred_part1', 'LotusSigH_part2', 'LotusTp_part2',
               'LotusSpred_part2', 'LotusSigH_part3', 'LotusTp_part3',
               'LotusSpred_part3', 'LotusSigH_part4', 'LotusTp_part4',
               'LotusSpred_part4', 'GFS_wind_speed', 'GFS_onshore_comp',
               'GFS_crossshore_comp', 'human_report_surf_max_ft',
               'LotusPdir_norm_part1_sin', 'LotusPdir_norm_part1_cos',
               'LotusPdir_norm_part2_sin', 'LotusPdir_norm_part2_cos',
               'LotusPdir_norm_part3_sin', 'LotusPdir_norm_part3_cos',
               'LotusPdir_norm_part4_sin', 'LotusPdir_norm_part4_cos',
               'GFS_wind_dir_norm_sin', 'GFS_wind_dir_norm_cos']]
        f = f.to_numpy()
        pred = clf.predict(f)
        pred = int(pred) - 1
        predictions.append(pred)
    predictions = np.asarray(predictions)
    return predictions




