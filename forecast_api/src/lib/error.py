from flask_restful import abort


def handle_error(error_code, message):
    """
    Basic error handling method.
    """
    abort(error_code, message=message)
