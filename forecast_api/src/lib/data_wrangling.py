import numpy as np
import os
import pandas as pd
import json
import math
import requests
from .flatten import flatten
from .transforms import bulk_algo_rating
metamodel_dir = 'ratings_inference/'


def find_matching_tide(resp: dict, time: int) -> dict:

    tidedict = {}

    if resp.get("port", np.nan) == np.nan:

        tidedict["port_distance"] = None
        tidedict["port_distanceunit"] = None
        tidedict["port_minheight"] = None
        tidedict["port_maxheight"] = None
        tidedict["tide_level"] = None
        tidedict["tide_time"] = None
        
    else:
        tidedict["port_distance"] = resp["port"]["distance"]
        tidedict["port_distanceunit"] = resp["port"]["distance_unit"]
        tidedict["port_minheight"] = resp["port"]["maxHeight"]
        tidedict["port_maxheight"]= resp["port"]["minHeight"]

        if not resp.get("levels"):
            tidedict["tide_level"] = None
            tidedict["tide_time"] = None
        else:
            levels = [level["shift"] for level in resp["levels"]]
            leveltimes = [level["time"] for level in resp["levels"]]

            timediff = abs(time - np.array(leveltimes))
            closesttidetime = np.argmin(timediff)   
            if timediff[closesttidetime] < 3600:
                tide_level = levels[closesttidetime]
                tide_time = leveltimes[closesttidetime]
            else:
                tide_level = None
                tide_time = None

            tidedict["tide_level"] = tide_level
            tidedict["tide_time"] = tide_time

    return tidedict

def get_rel_angles(angles):
    angles =  angles % 360
    angles = np.where(angles>180,angles-360,angles)
    return angles


def get_spotdata(spotid: str) -> dict:
    # assert os.path.isfile(f"./individual_spots/{spotid}.json"), "Couldn't find spot json file"

    with open(os.path.join(os.getcwd(), f"lib/individual_spots/{spotid}.json")) as f:
                spotinfo = json.load(f)

    spotinfo_flattened = flatten(spotinfo, ".")

    datadict = {}

    offshoredirection = spotinfo_flattened.get("offshoreDirection", np.nan)
    bestwinddirectionmin = spotinfo_flattened.get("best.windDirection.0.min", np.nan)
    bestwinddirectionmax = spotinfo_flattened.get("best.windDirection.0.max", np.nan)
    bestswellwindow0min = spotinfo_flattened.get("best.swellWindow.0.min", np.nan)
    bestswellwindow0max = spotinfo_flattened.get("best.swellWindow.0.max", np.nan)
    bestswellwindow1min = spotinfo_flattened.get("best.swellWindow.1.min", np.nan)
    bestswellwindow1max = spotinfo_flattened.get("best.swellWindow.1.max", np.nan)
    bestswellwindow2min = spotinfo_flattened.get("best.swellWindow.2.min", np.nan)
    bestswellwindow2max = spotinfo_flattened.get("best.swellWindow.2.max", np.nan)
    bestsizemin = spotinfo_flattened.get("best.sizeRange.0.min", np.nan)
    bestsizemax = spotinfo_flattened.get("best.sizeRange.0.max", np.nan)
    bestswellperiodmin = spotinfo_flattened.get("best.swellPeriod.0.min", np.nan)
    bestswellperiodmax = spotinfo_flattened.get("best.swellPeriod.0.max", np.nan)

    datadict["spot_offshoredirection_deg"] = offshoredirection
    datadict["spot_bestwinddirection_max_deg"] = bestwinddirectionmin
    datadict["spot_bestwinddirection_min_deg"] = bestwinddirectionmax
    datadict["spot_bestswellwindow0_min_deg"] = bestswellwindow0min
    datadict["spot_bestswellwindow0_max_deg"] = bestswellwindow0max

    if bestswellwindow1min != np.nan:
        datadict["spot_bestswellwindow1_min_deg"] = bestswellwindow1min
        datadict["spot_bestswellwindow1_max_deg"] = bestswellwindow1max
    else:
        datadict["spot_bestswellwindow1_min_deg"] = bestswellwindow0min
        datadict["spot_bestswellwindow1_max_deg"] = bestswellwindow0max
    if bestswellwindow2min != np.nan:
        datadict["spot_bestswellwindow2_min_deg"] = bestswellwindow2min
        datadict["spot_bestswellwindow2_max_deg"] = bestswellwindow2max
    else:
        datadict["spot_bestswellwindow2_min_deg"] = bestswellwindow0min
        datadict["spot_bestswellwindow2_max_deg"] = bestswellwindow0max

    #shuffled model stuff
    # datadict["spot_bestswellwindow1_min_deg"] = bestswellwindow1min
    # datadict["spot_bestswellwindow1_max_deg"] = bestswellwindow1max
    # datadict["spot_bestswellwindow2_min_deg"] = bestswellwindow2min
    # datadict["spot_bestswellwindow2_max_deg"] = bestswellwindow2max

    datadict["spot_bestsize_min"] = bestsizemin / 3.2808
    datadict["spot_bestsize_max"] = bestsizemax / 3.2808
    datadict["spot_bestswellperiod_min"] = bestswellperiodmin
    datadict["spot_bestswellperiod_max"] = bestswellperiodmax

    key = "spot_offshoredirection_deg"
    datadict[key+"_sin"] = np.sin(2 * math.pi * np.array(datadict[key]) / 360)
    datadict[key+"_cos"] = np.cos(2 * math.pi * np.array(datadict[key]) / 360)

    keys = ["spot_bestwinddirection_max_deg", "spot_bestwinddirection_min_deg", 
            "spot_bestswellwindow0_min_deg", "spot_bestswellwindow0_max_deg",
            "spot_bestswellwindow1_min_deg", "spot_bestswellwindow1_max_deg",
            "spot_bestswellwindow2_min_deg", "spot_bestswellwindow2_max_deg"]
    for key in keys:
        rel = get_rel_angles(datadict[key] - offshoredirection)
        datadict[key+"_rel_sin"] = np.sin(2 * math.pi * (rel.astype(float) + 180) / 360)
        datadict[key+"_rel_cos"] = np.cos(2 * math.pi * (rel.astype(float) + 180) / 360)

    return datadict


def get_lotus_rating(spotid: str, lotusdict: dict, offshoredirection: float):

    with open("wind_type.json") as f:
        winddata = json.load(f)

    windtype = winddata.get(spotid)

    return bulk_algo_rating(np.array(lotusdict["LotusWindDir_deg"]), 
                                                        offshoredirection, 
                                                        np.array(lotusdict["LotusWindSpeed_ms"]),
                                                        np.array(lotusdict["LotusMinBWH_mt"]),
                                                        np.array(lotusdict["LotusMaxBWH_mt"]),
                                                        windtype
                                                        )


def modify_lotus_angles(lotusdict: dict, offshoredirection) -> dict:


    #all of this needed for sorting swell partitions by SigH
    ######
    sighlist = [lotusdict["LotusSigHPart"+str(i)+"_mt"] for i in np.arange(0,6)]
    pdirlist = [lotusdict["LotusPdirPart"+str(i)+"_deg"] for i in np.arange(0 ,6)]
    tppartlist = [lotusdict["LotusTPPart"+str(i)+"_sec"] for i in np.arange(0,6)]
    spreadlist = [lotusdict["LotusSpreadPart"+str(i)+"_deg"] for i in np.arange(0,6)]
    
    sorted_order = np.argsort(np.nan_to_num(np.array(sighlist)))[::-1]
    
    sorted_sighlist = [sighlist[i] for i in sorted_order]
    sorted_pdirlist = [pdirlist[i] for i in sorted_order]
    sorted_tppartlist = [tppartlist[i] for i in sorted_order]
    sorted_spreadlist = [spreadlist[i] for i in sorted_order]

    lotusdict["LotusSigHPart0_mt"], lotusdict["LotusSigHPart1_mt"], lotusdict["LotusSigHPart2_mt"], lotusdict["LotusSigHPart3_mt"], lotusdict["LotusSigHPart4_mt"], lotusdict["LotusSigHPart5_mt"] = sorted_sighlist
    lotusdict["LotusPdirPart0_deg"], lotusdict["LotusPdirPart1_deg"], lotusdict["LotusPdirPart2_deg"], lotusdict["LotusPdirPart3_deg"], lotusdict["LotusPdirPart4_deg"], lotusdict["LotusPdirPart5_deg"] = sorted_pdirlist
    lotusdict["LotusTPPart0_sec"], lotusdict["LotusTPPart1_sec"], lotusdict["LotusTPPart2_sec"], lotusdict["LotusTPPart3_sec"], lotusdict["LotusTPPart4_sec"], lotusdict["LotusTPPart5_sec"] = sorted_tppartlist
    lotusdict["LotusSpreadPart0_deg"], lotusdict["LotusSpreadPart1_deg"], lotusdict["LotusSpreadPart2_deg"], lotusdict["LotusSpreadPart3_deg"], lotusdict["LotusSpreadPart4_deg"], lotusdict["LotusSpreadPart5_deg"] = sorted_spreadlist
    ######

    for i in range(6):
        if (lotusdict["LotusPdirPart"+str(i)+"_deg"] == 0) & (lotusdict["LotusSigHPart"+str(i)+"_mt"] == 0) & (lotusdict["LotusTPPart"+str(i)+"_sec"] == 0) & (lotusdict["LotusSpreadPart"+str(i)+"_deg"] == 0):
            lotusdict["LotusPdirPart"+str(i)+"_deg"] = np.nan
            lotusdict["LotusSigHPart"+str(i)+"_mt"] = np.nan
            lotusdict["LotusTPPart"+str(i)+"_sec"] = np.nan
            lotusdict["LotusSpreadPart"+str(i)+"_deg"] = np.nan

    keys = ["LotusWindDir_deg", 
        "LotusPdirPart0_deg", 
        "LotusPdirPart1_deg", 
        "LotusPdirPart2_deg", 
        "LotusPdirPart3_deg", 
        "LotusPdirPart4_deg", 
        "LotusPdirPart5_deg"
        ]
    for key in keys:
        rel = np.array(get_rel_angles(np.array(lotusdict[key]) - offshoredirection))
        lotusdict[key+"_rel_sin"] = np.sin(2 * math.pi * (rel.astype(float) + 180) / 360)
        lotusdict[key+"_rel_cos"] = np.cos(2 * math.pi * (rel.astype(float) + 180) / 360)

    return lotusdict


def get_rel_wind_direction(lotusdict: dict, offshoredirection) -> np.array:
    rel = get_rel_angles(lotusdict['LotusWindDir_deg'] - offshoredirection)
    return rel


def generate_array(spotdict, lotusdict, tidedict):

    features = np.array([
        spotdict["spot_bestwinddirection_min_deg_rel_sin"], 
        spotdict["spot_bestwinddirection_min_deg_rel_cos"],
        spotdict["spot_bestwinddirection_max_deg_rel_sin"], 
        spotdict["spot_bestwinddirection_max_deg_rel_cos"],
        spotdict["spot_bestswellwindow0_min_deg_rel_sin"], 
        spotdict["spot_bestswellwindow0_min_deg_rel_cos"],
        spotdict["spot_bestswellwindow0_max_deg_rel_sin"], 
        spotdict["spot_bestswellwindow0_max_deg_rel_cos"],
        spotdict["spot_bestswellwindow1_min_deg_rel_sin"],
        spotdict["spot_bestswellwindow1_min_deg_rel_cos"],
        spotdict["spot_bestswellwindow1_max_deg_rel_sin"],
        spotdict["spot_bestswellwindow1_max_deg_rel_cos"],
        spotdict["spot_bestswellwindow2_min_deg_rel_sin"],
        spotdict["spot_bestswellwindow2_min_deg_rel_cos"],
        spotdict["spot_bestswellwindow2_max_deg_rel_sin"],
        spotdict["spot_bestswellwindow2_max_deg_rel_cos"],
        spotdict["spot_offshoredirection_deg_sin"], 
        spotdict["spot_offshoredirection_deg_cos"], 
        spotdict["spot_bestsize_min"], 
        spotdict["spot_bestsize_max"], 
        spotdict["spot_bestswellperiod_min"], 
        spotdict["spot_bestswellperiod_max"], 
        tidedict["port_minheight"],
        tidedict["port_maxheight"],
        tidedict["tide_level"],
        lotusdict["LotusSigH_mt"],
        lotusdict["LotusWindSpeed_ms"], 
        lotusdict["LotusWindDir_deg_rel_sin"], 
        lotusdict["LotusWindDir_deg_rel_cos"], 
        lotusdict["LotusMinBWH_mt"], 
        lotusdict["LotusMaxBWH_mt"],
        lotusdict["LotusSigHPart0_mt"], 
        lotusdict["LotusTPPart0_sec"], 
        lotusdict["LotusPdirPart0_deg_rel_sin"], 
        lotusdict["LotusPdirPart0_deg_rel_cos"], 
        lotusdict["LotusSpreadPart0_deg"],
        lotusdict["LotusSigHPart1_mt"], 
        lotusdict["LotusTPPart1_sec"], 
        lotusdict["LotusPdirPart1_deg_rel_sin"], 
        lotusdict["LotusPdirPart1_deg_rel_cos"], 
        lotusdict["LotusSpreadPart1_deg"],
        lotusdict["LotusSigHPart2_mt"], 
        lotusdict["LotusTPPart2_sec"], 
        lotusdict["LotusPdirPart2_deg_rel_sin"], 
        lotusdict["LotusPdirPart2_deg_rel_cos"], 
        lotusdict["LotusSpreadPart2_deg"],
        lotusdict["LotusSigHPart3_mt"], 
        lotusdict["LotusTPPart3_sec"], 
        lotusdict["LotusPdirPart3_deg_rel_sin"], 
        lotusdict["LotusPdirPart3_deg_rel_cos"], 
        lotusdict["LotusSpreadPart3_deg"],
        lotusdict["LotusSigHPart4_mt"], 
        lotusdict["LotusTPPart4_sec"], 
        lotusdict["LotusPdirPart4_deg_rel_sin"],
        lotusdict["LotusPdirPart4_deg_rel_cos"], 
        lotusdict["LotusSpreadPart4_deg"],
        lotusdict["LotusSigHPart5_mt"], 
        lotusdict["LotusTPPart5_sec"], 
        lotusdict["LotusPdirPart5_deg_rel_sin"], 
        lotusdict["LotusPdirPart5_deg_rel_cos"], 
        lotusdict["LotusSpreadPart5_deg"]
        ])

    x = np.expand_dims(features, axis=0)

    return x


def generate_array_lotusonly(lotusdict):

    features = np.array([
        lotusdict["LotusSigH_mt"],
        lotusdict["LotusWindSpeed_ms"], 
        lotusdict["LotusWindDir_deg_rel_sin"], 
        lotusdict["LotusWindDir_deg_rel_cos"], 
        lotusdict["LotusMinBWH_mt"], 
        lotusdict["LotusMaxBWH_mt"],
        lotusdict["LotusSigHPart0_mt"], 
        lotusdict["LotusTPPart0_sec"], 
        lotusdict["LotusPdirPart0_deg_rel_sin"], 
        lotusdict["LotusPdirPart0_deg_rel_cos"], 
        lotusdict["LotusSpreadPart0_deg"],
        lotusdict["LotusSigHPart1_mt"], 
        lotusdict["LotusTPPart1_sec"], 
        lotusdict["LotusPdirPart1_deg_rel_sin"], 
        lotusdict["LotusPdirPart1_deg_rel_cos"], 
        lotusdict["LotusSpreadPart1_deg"],
        lotusdict["LotusSigHPart2_mt"], 
        lotusdict["LotusTPPart2_sec"], 
        lotusdict["LotusPdirPart2_deg_rel_sin"], 
        lotusdict["LotusPdirPart2_deg_rel_cos"], 
        lotusdict["LotusSpreadPart2_deg"],
        lotusdict["LotusSigHPart3_mt"], 
        lotusdict["LotusTPPart3_sec"], 
        lotusdict["LotusPdirPart3_deg_rel_sin"], 
        lotusdict["LotusPdirPart3_deg_rel_cos"], 
        lotusdict["LotusSpreadPart3_deg"],
        lotusdict["LotusSigHPart4_mt"], 
        lotusdict["LotusTPPart4_sec"], 
        lotusdict["LotusPdirPart4_deg_rel_sin"],
        lotusdict["LotusPdirPart4_deg_rel_cos"], 
        lotusdict["LotusSpreadPart4_deg"],
        lotusdict["LotusSigHPart5_mt"], 
        lotusdict["LotusTPPart5_sec"], 
        lotusdict["LotusPdirPart5_deg_rel_sin"], 
        lotusdict["LotusPdirPart5_deg_rel_cos"], 
        lotusdict["LotusSpreadPart5_deg"]
        ])

    x = np.expand_dims(features, axis=0)

    return x
