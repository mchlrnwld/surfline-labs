#!/usr/bin/python
import json
import logging
import os
import subprocess
from pathlib import Path  # python3 only

import boto3

from dotenv import load_dotenv

# Load prod .env file
env_path = Path('.') / '.env.production'
load_dotenv(dotenv_path=env_path, verbose=True)

_region_name = 'us-west-2'

logging.basicConfig()
_logger = logging.getLogger()
_logger.setLevel(logging.INFO)


def runCommands(cwd, commands):
    for cmd in commands:
        print('Executing: `' + cmd + '`')
        process = subprocess.Popen(
            cmd,
            stdout=subprocess.PIPE,
            shell=True,
            cwd=cwd,
            universal_newlines=True,
        )

        lastLine = False

        for stdout_line in iter(process.stdout.readline, ""):
            lastLine = stdout_line
            print(stdout_line.strip())

        process.stdout.close()
        rc = process.wait()

        if rc:
            raise subprocess.CalledProcessError(rc, cmd)

    if isinstance(lastLine, str):
        return lastLine.strip()


def replace_all(text, dic):
    for i, j in dic.items():
        text = text.replace(i, j)

    return text


def loadTaskDef():
    config_path = 'aws-taskdef.json'

    _logger.info(f'Loading service config: {config_path}')

    with open(config_path, 'r') as f:
        # Replace template params with real values.
        json_str = replace_all(
            f.read(),
            {
                '${ENV}': os.getenv('ENV'),
                '${SPOTS_API}': os.getenv('SPOTS_API'),
                '${SDS_API}': os.getenv('SDS_API'),
                '${TIDE_API}': os.getenv('TIDE_API'),
                '${MODEL_FILE}': os.getenv('MODEL_FILE'),
                '${HIGHLIGHTS_URL}': os.getenv('HIGHLIGHTS_URL'),
                '${HIGHLIGHTS_ORIGINAL_URL}': os.getenv('HIGHLIGHTS_ORIGINAL_URL'),
                '${HIGHLIGHTS_DB_HOST}': os.getenv('HIGHLIGHTS_DB_HOST'),
                '${HIGHLIGHTS_DB}': os.getenv('HIGHLIGHTS_DB'),
                '${HIGHLIGHTS_DB_USER}': os.getenv('HIGHLIGHTS_DB_USER'),
                '${HIGHLIGHTS_DB_PASS}': os.getenv('HIGHLIGHTS_DB_PASS')
            },
        )

    task_def = json.loads(json_str)
    _logger.info('Loaded config.')

    return task_def


def registerTask():
    serviceDef = loadTaskDef()
    ecs_cluster_name = 'sl-labs-prototype-cluster'
    ecs_service_name = 'sl-labs-foreast-api'
    image_tag = '665294954271.dkr.ecr.us-west-1.amazonaws.com/sl-labs-forecast-api:latest'

    serviceDef['containerDefinitions'][0]["image"] = image_tag

    ecs = boto3.client('ecs', region_name='us-west-1')

    _logger.info('Register new task def for: %s' % serviceDef['family'])

    response = ecs.register_task_definition(
        family=serviceDef['family'],
        taskRoleArn=serviceDef['taskRoleArn'],
        executionRoleArn=serviceDef['executionRoleArn'],
        containerDefinitions=serviceDef['containerDefinitions'],
        networkMode=serviceDef['networkMode'],
        requiresCompatibilities=serviceDef['requiresCompatibilities'],
        cpu=serviceDef['cpu'],
        memory=serviceDef['memory'],
    )

    # This is safe, register_task_definition will raise exception if it fails.
    newTaskArn = response["taskDefinition"]["taskDefinitionArn"]

    _logger.info('Registered new task def: %s' % newTaskArn)

    response = ecs.update_service(
        cluster=ecs_cluster_name,
        service=ecs_service_name,
        taskDefinition=newTaskArn,
    )

    _logger.info('Service update triggered. Waiting for service steady state.')

    waiter = ecs.get_waiter('services_stable')

    waiter.wait(
        cluster=response['service']['clusterArn'],
        services=[ecs_service_name],
        WaiterConfig={
            # Check status evey 7 seconds.
            'Delay': 7
        },
    )


def main():
    registerTask()


main()
