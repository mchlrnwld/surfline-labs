# Forecast API

Forecast API uses a xgboost model to predict condition ratings based on SDS forecast data.

API returns SDS with attached condition ratings, using user requested spot_id.

API is currently hosted here: `http://sl-labs-forecast-api-alb-1142634910.us-west-1.elb.amazonaws.com`.

A request can be made for a spot like so: `http://sl-labs-forecast-api-alb-1142634910.us-west-1.elb.amazonaws.com/forecast?spot_id=58bdfa7882d034001252e3d8`

## Development

1. Copy `.env.sample` to `.env` and add file name and extension for model to be used e.g) pftc_no_breaking.model

2. Run `mkdir models` and add the model to this directory. Model can be found in s3 here: `s3://wt-data-lake-dev/science/ml_surfzone/model_files`

2. Run `make build`

3. Run `make run`

4. `curl http://localhost/forecast?spot_id=5842041f4e65fad6a770888a` to test that you get data back.


## Deployment

Container is deployed to an ECS cluster (see Infrastructure below).

There is a python script to make updating easy.

```python
# Create virtual env
virtualenv venv

# Activate it
source venv/bin/activate

# Install deps
pip install -r requirements.txt

# Create production .env file, these params will get templated into aws-taskdef before deploy.
cp .env.sample .env.production

# Run the deployment script
make deploy
```

## Infrastructure

Service runs on a Fargate backed cluster in surfline-dev.

Created via (https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ECS_AWSCLI_Fargate.html):

```bash
# Create cluster, already crated.
aws ecs create-cluster --cluster-name sl-labs-prototype-cluster

aws ecs register-task-definition --cli-input-json file://aws-taskdef.json

# Get available task defs
aws ecs list-task-definitions --family-prefix sl-labs-foreast-api

# Create service (private subnets).
# Manually create target group (ip type for Fargate) and ALB first via console (in that order: https://aws.amazon.com/premiumsupport/knowledge-center/create-alb-auto-register/).
# Below doesn't work can't find containerName in task def (but it does match the task def?). Create in console.
aws ecs create-service --cluster sl-labs-prototype-cluster --service-name sl-labs-foreast-api --task-definition sl-labs-foreast-api:1 --desired-count 1 --launch-type "FARGATE" --network-configuration "awsvpcConfiguration={subnets=[subnet-f2d458ab,subnet-0909466c],securityGroups=[sg-0d3bbac5a551ca609, sl-labs-forecast-api-alb-sg]}" --load-balancers "targetGroupArn=arn:aws:elasticloadbalancing:us-west-1:665294954271:targetgroup/sl-labs-forecast-api-tg/d059059891421aa5,containerName=sl-labs-forecast-api,containerPort=80"

# List services on cluster
aws ecs list-services --cluster sl-labs-prototype-cluster

# Describe running services
aws ecs describe-services --cluster sl-labs-prototype-cluster --services sl-labs-foreast-api
```
