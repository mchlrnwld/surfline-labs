/**
 * Search class implemented as a component. This class loads API data into our state
 * which then populates a dropdown menu.
 */

import React, { Component } from 'react'
import {Select, TYPE} from 'baseui/select'
import {ListItemLabel, MenuAdapter} from 'baseui/list'
import axios from 'axios'

class Search extends Component {
    // Get our router from the Nav component
    constructor(props) {
        super();
        this.router = props.router
    }

    // Load in our default state
    state = {
        query: '',
        results: [],
        value: '',
        loading: false
    }

    // Get our query params from the user input
    handleInputChange = (value) => {
        this.setState({
            loading: true,
            query: value
        }, () => {
            if (this.state.query) {
                this.getInfo()
            } else {
                this.setState({
                    results: [],
                    loading: false
                })
            }
        })
    }

    // Get spot data from the Surfline search API
    getInfo = () => {
        axios.get(`https://services.surfline.com/search/site?q=${this.state.query}&querySize=10&suggestionSize=10&newsSearch=false`)
            .then(({data}) => {
                let payload = data[0].suggest['spot-suggest'][0].options
                this.handleResults(payload)
            })
            .catch((error) => {
                alert(error);
            })
    }

    // Process the API result payload into a format which can be read by the "Select" BaseUI component
    handleResults = (results) => {
        let options = []

        // Shrink results list to fit menu within the page
        results = results.slice(0, 8)

        results.map(r => (
            options.push({label: r.text, spotId: r._id, breadCrumbs: r._source.breadCrumbs.join(' > ')})
        ))
        this.setState({
            results: options,
            loading: false
        })
    }

    // Change to the selected spot graph
    handleSelectedChange = (value) => {
        this.router.push(`/forecast/[spotId]`, `/forecast/${value[0].spotId}`)
    }

    render() {
        return (
            <div>
                <Select
                    type={TYPE.search}
                    isLoading={this.state.loading}
                    options={this.state.results}
                    onChange={({value}) => this.handleSelectedChange(value)}
                    onInputChange={event => {
                        const target = event.target;
                        this.handleInputChange(target.value);
                    }}
                    placeholder='Search spots...'
                    overrides={{
                        DropdownListItem: {
                            component: React.forwardRef((props, ref) => (
                                <MenuAdapter {...props} ref={ref}>
                                    <ListItemLabel description={props.item.breadCrumbs}>
                                        {props.children}
                                    </ListItemLabel>
                                </MenuAdapter>
                            ))
                        }
                    }}
                />
            </div>
        )
    }
}

export default Search