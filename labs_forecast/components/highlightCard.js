/**
 * Highlight cards which are individual col-4 video players. Each card
 * has tabs that let you switch between the highlight and the original clip.
 * Note --Subject to change-- we are using col-4 because the API payload will
 * be 6 results, hence we are using a 2x3 grid.
 */

import React from 'react'
import {Tab, Tabs} from 'baseui/tabs-motion/index'

const HighlightCard = (props) => {
    const [activeKey, setActiveKey] = React.useState('0');

    return (
        <div className='col-4'>
            <Tabs
                activeKey={activeKey}
                onChange={({ activeKey }) => {
                    setActiveKey(activeKey);
                }}
                activateOnFocus
            >
                <Tab title='Highlights'>
                    <video className='gridClip' controls>
                        <source src={props.clip.highlight_clip_url} type='video/mp4' />
                    </video>
                </Tab>
                <Tab title='Original'>
                    <video className='gridClip' controls>
                        <source src={props.clip.original_clip_url} type='video/mp4' />
                    </video>
                </Tab>
                <Tab title={props.time} disabled />
            </Tabs>
        </div>
    )
};

export default HighlightCard