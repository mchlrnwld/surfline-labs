/**
 * Layout component which has the structure for our application pages. Currently
 * only includes a header which is a navigation panel.
 */

import React from 'react'
import Nav from '../components/nav'

const Layout = (props) => {
    return (
        <div>
            <Nav />
            <div>
                {props.children}
            </div>
        </div>
    )
};

export default Layout