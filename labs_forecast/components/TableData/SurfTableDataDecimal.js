import React, { Component } from 'react'
import UnitText from './components/UnitText'
import { StatefulTooltip } from 'baseui/tooltip'

class SurfTableDataDecimal extends Component {
  render() {
    return (
      <td style={{ backgroundColor: '#23b1e5' }}>
        <div>
          <span>
            <StatefulTooltip
              accessibilityType={'tooltip'}
              content={this.props.min + ", " + this.props.max}
            >
                <span style={{ color: 'white' }}>
                    {(this.props.min).toFixed(1)} - {(this.props.max).toFixed(1)}
                </span>
                <UnitText text='ft' color='white'/>
            </StatefulTooltip>
          </span>
        </div>
      </td>
    );
  }
}

export default SurfTableDataDecimal
