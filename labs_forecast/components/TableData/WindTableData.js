import React, { Component } from 'react'
import { StatefulTooltip } from 'baseui/tooltip'
import UnitText from './components/UnitText'
import arrow from '../../public/wind_direction_arrow.svg'
import mapDirection from '../../utils/mapDirection'

class WindTableData extends Component {
  render() {
    return (
      <td>
        <div>
          <span>
            {this.props.speed.toFixed(0)}
          </span>
          <UnitText text='kts &nbsp;' />
          <span>
            <StatefulTooltip
              accessibilityType={'tooltip'}
              content={this.props.direction.toFixed(0) + "° " + mapDirection(this.props.direction)}
            >
              <img
                src={arrow}
                alt="wind_direction_arrow"
                style={{
                  transform: `rotate(${this.props.direction}deg)`,
                  height: "15%",
                  width: "15%",
                }}
              />
            </StatefulTooltip>
          </span>
          <span>
            {this.props.norm_direction.toFixed(0)}
          </span>
          <UnitText text='deg &nbsp;' />
        </div>
      </td>
    );
  }
}

export default WindTableData
