import React, { Component } from 'react'

class UnitText extends Component {
  render() {
    return (
      <span
        style= {{
          fontSize: '70%',
          textTransform: 'lowercase',
          lineHeight: '1em',
          color: this.props.color ? this.props.color : 'black'
        }}
      >
        {this.props.text}
      </span>
    )
  }
}

export default UnitText
