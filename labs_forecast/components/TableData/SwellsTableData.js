import React, { Component } from 'react'
import { StatefulTooltip } from 'baseui/tooltip'
import UnitText from './components/UnitText'
import arrow from '../../public/swell_direction_arrow.svg'
import mapDirection from '../../utils/mapDirection'

class SwellsTableData extends Component {
  render() {
    if (this.props.swell == undefined) {
      return (
        <td className='background-gray-lighter' />
      )
    }

    return (
      <td className='background-gray-lighter'>
        <div>
          <span>
            {(this.props.swell.height * 3.28).toFixed(1)}
          </span>
          <UnitText text='ft &nbsp;' />
          <span>
            {this.props.swell.period}
          </span>
          <UnitText text='s &nbsp;' />
          <span>
            <StatefulTooltip
              accessibilityType={'tooltip'}
              content={this.props.swell.direction.toFixed(0) + "\u00B0 " + mapDirection(this.props.swell.direction)}
            >
              <img
                src={arrow}
                alt="swell_direction_arrow"
                style={{
                  transform: `rotate(${this.props.swell.direction}deg)`,
                  height: '15%',
                  width: '15%'
                }}
              />
            </StatefulTooltip>
          </span>
        </div>
      </td>
    );
  }
}

export default SwellsTableData
