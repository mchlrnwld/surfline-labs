import React, { Component } from 'react'
import { Tag, KIND, VARIANT } from 'baseui/tag';

let conditions_map = new Map([
  ['FLAT', '#A9A9A9'],
  ['N/A', '#A9A9A9'],
  ['VERY_POOR', '#555555'],
  ['POOR', '#42A5FC'],
  ['POOR_TO_FAIR', '#42A5FC'],
  ['FAIR', '#23D737'],
  ['FAIR_TO_GOOD', '#23D737'],
  ['GOOD', '#FFBE00'],
  ['VERY_GOOD', '#FFBE00'],
  ['GOOD_TO_EPIC', '#FF0000'],
  ['EPIC', '#FF0000'],
  ['FLAT', '#A9A9A9'],
  ['RED', '#FF0000'],
  ['YELLOW', '#F2DA00'],
  ['GREEN', '#23D737']
]);


class ConditionsTag extends Component {
  render() {
    return (
      <Tag
        color={conditions_map.get(this.props.rating)}
        variant={VARIANT.solid}
        kind={KIND.custom}
        onClick={() => {}}
        closeable={false}
      >
        {this.props.rating.replace(/_/g, ' ')}
      </Tag>
    );
  }
}

export default ConditionsTag
