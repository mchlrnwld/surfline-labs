/**
 * Highlight clips component which displays highlight clips on the home page.
 */

import React from 'react'
import axios from 'axios'
import {Spinner} from 'baseui/spinner/index'
import HighlightCard from './highlightCard'
import HighlightGrid from './highlightGrid'

const HighlightClips = (props) => {
    const [error, setError] = React.useState(null);
    const [loaded, setLoaded] = React.useState(false);
    const [spots, setSpots] = React.useState([]);
    let clips = [];
    let containers = [];

    React.useEffect(() => {
        // Load in API highlight clips data
        axios.get(`https://labs-api.sandbox.surfline.com/highlights`, {
        })
            .then(({data}) => {
                setLoaded(true);
                setSpots(data);
            })
            .catch((error) => {
                console.log(error);
                setLoaded(true);
                setError(error);
            })
    });

    // Iterate through all spots
    spots.forEach(spot => {
        if (spot.cameras.length > 0) {
            // Iterate through all cameras at a spot
            spot.cameras.forEach(camera => {
                if (camera.clips.length > 0) {
                    // Sort by increasing timestamp
                    camera.clips.sort(function(x, y) {
                        return y.timestamp - x.timestamp;
                    });

                    // Iterate through all clips for a camera at a spot
                    camera.clips.forEach(clip => {
                        let date = new Date(clip.timestamp * 1000);
                        let time = date.toLocaleString('en-US', { timeZone: spot.timezone });

                        clips.push(
                            <HighlightCard spot={spot} time={time} clip={clip} />
                        );
                    });
                }

                if (camera.clips.length !== 0) {
                    containers.push(
                        <HighlightGrid spot={spot} clips={clips} camera={camera} />
                    );
                }
                clips = [];
            });
        }
    });


    if (error) {
        return (
            <div>
                Error: {error.message}
            </div>
        );
    } else if (!loaded) {
        return (
            <Spinner className='forecast-spinner' />
        );
    } else {
        return containers;
    }
};

export default HighlightClips