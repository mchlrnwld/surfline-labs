/**
 * Grid of highlight clips (currently set to a 2x3).
 */

import React from 'react'

const HighlightGrid = (props) => {

    return (
        <div className='container-fluid'>
            <h3 className='highlightClipHeader'>
                {`${props.spot.name} - ${props.camera.name}`}
            </h3>
            <div className='row'>
                {props.clips.slice(0, 3)}
            </div>
            <div className='row'>
                {props.clips.slice(3, 6)}
            </div>
        </div>
    )
};

export default HighlightGrid