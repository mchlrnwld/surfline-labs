/**
 * Nav component which has a search bar that auto-fills suggestions. Upon
 * selection of a spot, a loading bar will appear until the request is complete.
 */

import * as React from 'react'
import {
    HeaderNavigation,
    ALIGN,
    StyledNavigationItem as NavigationItem,
    StyledNavigationList as NavigationList,
} from 'baseui/header-navigation'
import {StyledLink as Link} from 'baseui/link'
import {useRouter} from 'next/router'
import Search from '../components/search'

const Nav = (props) => {
    const router = useRouter()
    return (
        <HeaderNavigation>
            <NavigationList $align={ALIGN.left}>
                <NavigationItem style={{fontWeight: '600', fontSize: '26px'}}>
                    <Link href='/' style={{textDecoration: 'none'}}>
                        Surfline Labs
                    </Link>
                </NavigationItem>
            </NavigationList>
            <NavigationList>
                <NavigationItem style={{width: '82vw'}}>
                    <Search {...props} router={router}/>
                </NavigationItem>
            </NavigationList>
        </HeaderNavigation>
    )
};

export default Nav