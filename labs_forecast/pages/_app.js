/**
 * Wrap app in special components such as Styletron.
 */

import App from 'next/app'
import { Provider as StyletronProvider } from 'styletron-react'
import { styletron, debug } from '../styletron'
import Router from 'next/router'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.css'

// Load progress bar on route changes
Router.events.on('routeChangeStart', () => NProgress.start())
Router.events.on('routeChangeComplete', () => NProgress.done())
Router.events.on('routeChangeError', () => NProgress.done());

export default class MyApp extends App {
  render() {
    const { Component, pageProps } = this.props
    return (
        <StyletronProvider value={styletron} debug={debug} debugAfterHydration>
            <Component {...pageProps} />
        </StyletronProvider>
    )
  }
}
