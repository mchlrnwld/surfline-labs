/**
 * Default home page located at /.
 */

import React from 'react'
import Layout from '../components/layout'
import HighlightClips from '../components/highlightClips'

export default function Home() {
  return (
      <div>
        <Layout />
        <HighlightClips />
      </div>
  )
}
