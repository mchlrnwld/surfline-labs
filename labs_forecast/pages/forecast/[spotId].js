/**
 * Dynamic route which allows us to follow the route "/forecast/[spotId]".
 */

import React from 'react'
import axios from 'axios'
import {useRouter} from 'next/router'
import {Spinner} from 'baseui/spinner'
import Layout from '../../components/layout'
import ConditionsTag from '../../components/conditionsTag'
import SwellsTableData from '../../components/TableData/SwellsTableData'
import WindTableData from '../../components/TableData/WindTableData'
import SurfTableData from '../../components/TableData/SurfTableData'
import SurfTableDataDecimal from '../../components/TableData/SurfTableDataDecimal'

const forecastTables = () => {
    const router = useRouter();
    const [prevSpotId, setPrevSpotId] = React.useState({});
    const [spotName, setSpotName] = React.useState('');
    const [result, setResult] = React.useState({});
    const [offset, setOffset] = React.useState(0);

    // Helper array which translates timestamp indexes to words
    const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

    // Selected spotId
    const { spotId } = router.query;

    // tbodys to put into our table
    let tbodys = [];

    // Fetch API data
    if (spotId !== prevSpotId) {
        // Get spot forecast information
        // axios.get(`https://labs-api.sandbox.surfline.com/forecast?spot_id=${spotId}`, {
        axios.get(`http://192.168.101.50:90/forecast?spot_id=${spotId}`, {
        })
            .then(({data}) => {
                setResult(data);
                setPrevSpotId(spotId);
            })
            .catch((error) => {
                console.log(error);
            })

        // Get spot timezone information
        axios.get(`https://services.surfline.com/kbyg/spots/reports?spotId=${spotId}`, {
        })
            .then(({data}) => {
                setOffset(data.associated.utcOffset);
                setSpotName(data.spot.name);
            })
            .catch((error) => {
                console.log(error);
            })
    }

    // Make sure our API data is not null
    if (!(Object.keys(result).length === 0 && result.constructor === Object)) {
        try {
            let surfSpotForecasts = result.data.surfSpotForecasts.surf.data;

            // Get today's date at the spot so we start with that forecast day
            let today = new Date();

            // Adjust to spots local offset, and then negate the browsers local offset
            today.setTime(today.getTime() + (offset * 60 * 60 * 1000));
            today.setTime(today.getTime() + (today.getTimezoneOffset() * 60 * 1000));

            let yesterday = today.getDay() - 1

            // Save the previous forecast day so we know when to make a new table
            let prevDay = new Date(surfSpotForecasts[0].timestamp * 1000);

            // Adjust to spots local offset, and then negate the browsers local offset
            prevDay.setTime(prevDay.getTime() + (offset * 60 * 60 * 1000));
            prevDay.setTime(prevDay.getTime() + (prevDay.getTimezoneOffset() * 60 * 1000));

            // Create array of trs to put into a tbody
            let rows = [];

            // Create a 16 day forecast with timestamps in each forecast day
            for (let i = 0; i < surfSpotForecasts.length; i++) {
                let currentDay = new Date(surfSpotForecasts[i].timestamp * 1000);

                // Adjust to spots local offset, and then negate the browsers local offset
                currentDay.setTime(currentDay.getTime() + (offset * 60 * 60 * 1000));
                currentDay.setTime(currentDay.getTime() + (currentDay.getTimezoneOffset() * 60 * 1000));

                // Timestamp to display on table
                let timestamp = currentDay.toLocaleString('en-US', { hour: 'numeric', hour12: true });
                let timestampClassName = 'td-timestamp';

                if (timestamp === '12 PM') {
                    timestamp = 'Noon';
                    timestampClassName = 'td-timestamp-noon';
                }

                // Grab each type of forecast data
                let surfData = result.data.surfSpotForecasts.surf.data;
                let swellsData = result.data.surfSpotForecasts.swells.data;
                let windData = result.data.surfSpotForecasts.wind.data;
                let conditionsData = result.data.surfSpotForecasts.conditions.data;

                // Build forecast row component
                let forecastTableRow = (
                  <tr>
                      <td className={timestampClassName}>
                          <small>
                              {timestamp}
                          </small>
                      </td>
                      <SurfTableData
                          min={surfData[i].surf.breakingWaveHeightMin}
                          max={surfData[i].surf.breakingWaveHeightMax}
                      />
                      <SurfTableDataDecimal
                          min={conditionsData[i].surf_height[0]}
                          max={conditionsData[i].surf_height[1]}
                      />
                      <td>
                          <ConditionsTag rating={conditionsData[i].predcondition} />
                      </td>
                      <td>
                          <ConditionsTag rating={conditionsData[i].smoothcondition} />
                      </td>
                      <td>
                          <ConditionsTag rating={conditionsData[i].algocondition} />
                      </td>
                      {/* <td>
                          <ConditionsTag rating={conditionsData[i].smoothcondition} />
                      </td> */}
                      {[0, 1, 2, 3, 4, 5].map((index) => (typeof swellsData[i].swells.components[index] !== 'undefined') ? <SwellsTableData swell={swellsData[i].swells.components[index]} /> : <SwellsTableData />)}
                      <WindTableData
                          speed={windData[i].wind.speed}
                          direction={windData[i].wind.direction}
                          offshore={conditionsData[i].offshore_dir}
                          norm_direction={conditionsData[i].wind_dir_norm}
                          wind_type={conditionsData[i].wind_type}
                      />
                  </tr>
                );

                // Francisco-style offshore angle for display
                let offshore = conditionsData[0].offshore_dir
                if (offshore > 180){
                    offshore = offshore - 180
                } else {
                    offshore = offshore + 180
                }

                // If we are at the next day, save our current table, and reset
                if (currentDay.getDay() !== prevDay.getDay()) {
                    let tbody = (
                        <tbody>
                            <tr>
                                <th className='table-full-row background-gray-darker' colSpan={13}>
                                    <h6 className='timestamp-am-pm'>
                                        <text style={{textAlign: 'left'}}>
                                            {`${days[prevDay.getDay()]} `}
                                            <small>
                                                {`${prevDay.getMonth() + 1}/${prevDay.getDate()}  ------   `}
                                            </small>
                                        </text>
                                        <text style={{textAlign: 'right'}}>
                                            {` Beach Angle: `}
                                            <small>
                                                {`${conditionsData[0].offshore_dir}°  `}
                                            </small>
                                        </text>
                                        <text style={{textAlign: 'right'}}>
                                            {` Offshore: `}
                                            <small>
                                                {`${offshore}°        `}
                                            </small>
                                        </text>
                                        <text style={{textAlign: 'right'}}>
                                            {` Algo: `}
                                            <small>
                                                {`${conditionsData[0].wind_type}`}
                                            </small>
                                        </text>
                                    </h6>
                                </th>
                            </tr>
                            {rows}
                            <tr>
                                <td className='fill-columns' colSpan={22}>
                                    <div className='margin-top-tbody' />
                                </td>
                            </tr>
                        </tbody>
                    );

                    // Only add a tbody if the timestamp is not yesterday
                    if (prevDay.getDay() !== yesterday) {
                        // Push one forecast day into array of 16 forecasts
                        tbodys.push(tbody);
                    }

                    // Reset the tr list for our new forecast table
                    rows = [];

                    // Push the current day into the next forecast tables list of trs, as it will be skipped due to the if condition
                    rows.push(forecastTableRow);
                } else {
                    // Create array of trs, (all timestamps for a forecast day) which will go inside of a singular tbody
                    rows.push(forecastTableRow);
                }
                // Set the previous day so we know when to make a new table
                prevDay = currentDay;
            }
        } catch (error) {
            alert(error);
        }
    }

    // Load a spinner while we fetch API data
    if (spotId !== prevSpotId || prevSpotId === {}) {
        return (
            <Layout>
                <Spinner className='forecast-spinner' />
            </Layout>
        )
    } else {
        return (
            <Layout>
                <h3 className='title'>
                    {spotName}
                </h3>
                <table className='table'>
                    <thead>
                    <tr>
                        <th className='table-text' />
                        <th className='table-text'>
                            lotus ht
                        </th>
                        <th className='table-text'>
                            model ht
                        </th>
                        <th className='table-text'>
                            model
                        </th>
                        <th className='table-text'>
                            smoothed
                        </th>
                        <th className='table-text'>
                            algo
                        </th>
                        <th className='table-text'>
                            swells
                        </th>
                        <th className='table-text'>

                        </th>
                        <th className='table-text'>

                        </th>
                        <th className='table-text'>

                        </th>
                        <th className='table-text'>

                        </th>
                        <th className='table-text'>

                        </th>
                        <th className='table-text'>
                            wind
                        </th>
                    </tr>
                    </thead>
                    {tbodys}
                </table>
            </Layout>
        )
    }

}

export default forecastTables
