# First run Zane's plot_validation_hts to get an {alias}.all_data.pkl
# This code then reads that pkl and attempts to do the proper wave statistics.
# Basically a wrapper for some stuff in daily_sigH and other stuff
#
import os
import pickle
import argparse

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import daily_sigH
from collections import defaultdict
from sl_camera_sensor.utils.logger import getLogger
from ipdb import set_trace

log = getLogger(__name__)

def get_sigh_dists(df, group_interval='600min'):
    hdat = defaultdict(list)
    df = df.set_index(pd.to_datetime(df['timestamp'], unit='s'))
    for time, time_grp in df.groupby(pd.Grouper(freq=group_interval)):
        max_hts = []
        for wave_num, grp in time_grp.groupby('track_num'):
            hts = []
            for row_num, row in grp.iterrows():
                if pd.isna(row.ft) or row.fit < 0.96:  # and row.ncycl > 1.1: 
                    continue
                if isinstance(row['ft'], str):
                    hts.extend(list(map(float, row['ft'].split(';'))))
                elif isinstance(row['ft'], float):
                    hts.append(row['ft'])
            if len(hts) > 0:
                # Look at the average of the largest 3. this is fn(FPS).
                max_hts.append(np.mean(sorted(hts)[::-1][:3]))
        top_third = sorted(max_hts)[::-1][:len(max_hts)//3]
        hdat['dists'].append(top_third)
        hdat['fulldist'].append(max_hts)
        hdat['rolling_time'].append(time.to_pydatetime())
        hdat['sigh'].append(np.mean(top_third))
        dsigh = 0.5324 * np.std(top_third)
        hdat['dsigh'].append(0 if dsigh != dsigh else dsigh)
    return hdat
            
def main(args):
    pkl_file = f'{args.alias}/{args.alias}.all_data.pkl'
    if not os.path.isfile(pkl_file):
        raise ValueError(f'pkl file doesnt exist: {pkl_file}')
    with open(pkl_file, 'rb') as f:
        all_data = pickle.load(f)

    heights = {}
    for idate in all_data.keys():
        idate_data = all_data[idate]
        # Make a new dict for plot_hists

        rpt_times = ['morning', 'afternoon']
        for rpt_time in rpt_times:
            key = idate + ' ' + rpt_time
            rpt = idate_data[rpt_time]['report']
            trax_list = idate_data[rpt_time]['heights']
            if len(trax_list) == 0:
                continue
            rpt['tot_hr'] = len(trax_list) * 10 / 60
            # CANNOT CONCAT HERE SINCE EACH DF WILL START AT WAVE_NUM=1!!!!!
            for i, itrack in enumerate(trax_list):
                if i > 0:
                    itrack.track_num += trax_list[i-1].track_num.max()
            Hdat = get_sigh_dists(pd.concat(trax_list))
            Hdat['rpt'] = rpt
            #fits = pd.concat(trax_list).fit.values
            #fit_stat = len(np.where(fits>=0.96)[0]) / len(np.where(fits<0.96)[0])
            if rpt['tot_hr'] < .5:
                print(f'Omitting low duration point for {key}')
            else:
                heights[key] = Hdat  #(hts, sigH, rpt, fit_stat)
            # Show me what the rewinds are that correspond to this timeframe
            if not args.silent:
                print('='*80)
                print(idate, rpt_time, ':')
                print('\n'.join(idate_data[rpt_time]['vids']))
    
    plot_timeseries(heights, args)
    #daily_sigH.plot_hists(heights, save_name=args.alias+'-hists.jpg')

    with open(f'{args.alias}-heights.pkl','wb') as f:
        pickle.dump(heights, f)
    
def plot_timeseries(heights, args):
    alldat = defaultdict(list)
    plt.figure(figsize=(7, 4))
    for k, v in heights.items():
        #hts, sigH, rpt, fs = v
        #fitstat.append(fs)
        rpt = v['rpt']
        dt = rpt['datetime']
        alldat['times'].append(dt)
        alldat['sigh_times'].extend(v['rolling_time'])
        alldat['sigh'].extend(v['sigh'])
        alldat['dsigh'].extend(v['dsigh'])
        alldat['lows'].append(rpt['min'])
        alldat['highs'].append(rpt['max'])
        iocc = rpt['occasional']
        alldat['occ'].append(iocc if iocc is not None and iocc>0 else rpt['max'])
        alldat['tides'].append(rpt['tide'])

    plt.plot(alldat['times'], alldat['tides'], '--', label='tide', alpha=0.4, color='g')
    rpt_mean = np.mean(list(zip(alldat['lows'], alldat['highs'])), axis=1)
    plt.errorbar(alldat['times'], rpt_mean,
                 [np.abs(rpt_mean-alldat['lows']), np.abs(rpt_mean-alldat['occ'])],
                 label='Human Reported Height', capsize=5, marker='X',
                 color='orange')
    sigh = np.array(alldat['sigh'])
    plt.errorbar(alldat['sigh_times'], sigh,
                 yerr=alldat['dsigh'],
                 #[-np.array(alldat['dsigh']), alldat['dsigh']],
                 capsize=3, label='Robotic $H_{1/3}$', marker=".", ls='none',
                 color='b')

    #plt.plot(times, sigh, '-o', label='Robotic $H_{1/3}$')
    #plt.plot(times, fitstat, marker='*', label='fits')
    plt.xlabel('Date')#, fontsize=14)
    plt.ylabel('Feet')#, fontsize=14)
    plt.title(args.alias)#, fontsize=14)
    plt.xticks(rotation=45)
    plt.legend()
    plt.tight_layout()
    plt.savefig(f'{args.alias}-timeseries.jpg')
    log.info(f'wrote {args.alias}-timeseries.jpg')
    #plt.figure(figsize=(7,5))
    #plt.hist(np.array(meas) - np.array(report), bins=6)
    #plt.xlabel('measured - report')
    #plt.tight_layout()
    #plt.savefig('dsigH.jpg')
    set_trace()
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--alias', default='wc-hbpierns')
    parser.add_argument('-s', '--silent', action='store_true')
    args = parser.parse_args()

    main(args)
