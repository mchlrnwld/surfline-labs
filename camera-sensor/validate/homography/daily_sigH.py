# Extract significant wave height from wavetracks
#
import os
import pickle
import argparse
import datetime
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from sl_camera_sensor.utils.cam import video_utils as vu
from sl_camera_sensor.utils.logger import getLogger
from labs_utils import reports, spots
from ipdb import set_trace

log = getLogger(__name__)

HEIGHTS_DF = pd.read_csv(
    '/archived-rewinds/main-data/wc-hbpierns/2021/01/wc-hbpierns-wave-heights-202101.csv'
    )
#print('READING HEIGHTS FROM HB CSV')

def plot_hists(heights, save_name=None):
    """ Feed this a heights dict """
    nplots = len(heights)
    nx = int(np.ceil(np.sqrt(nplots)))
    fig, axes = plt.subplots(nx, nx, figsize=(20,15))
    axes = axes.flatten()
    for i, (ts, dat) in enumerate(heights.items()):
        ax = axes[i]
        if i == nx*2:
            ax.set_ylabel('Count', fontsize=12)
            ax.set_xlabel('Measured Wave Height (ft)', fontsize=12)
        date = ts if isinstance(ts, str) else datetime.datetime.utcfromtimestamp(ts).strftime('%Y-%m-%d %H:%M') 
        hts, sigH, rpt, fs = dat
        ax.set_title(f'{date}, {rpt["tot_hr"]:.1f} hours (fs={fs:.1f})')
        ax.tick_params(axis='both', which='major', labelsize=12)
        nbins = int((max(hts) - min(hts)) * 3) - 1
        ax.hist(hts, bins=nbins)
        # Draw a line at the max 1/3 point?
        tide = rpt['tide']
        rpt_min, rpt_max, rpt_occ = rpt['min'], rpt['max'], rpt['occasional']
        #conditions = rpt['conditions']
        txt = (f'report: {rpt_min}-{rpt_max} occ {rpt_occ} ft\n'#{conditions},'
               '$H_{1/3}$:'+f' {sigH:.1f} ft\n tide: {tide:.1f} ft'
        )        
        plt.text(.7,.8,txt,fontsize=12, ha='center', va='center', transform=ax.transAxes)
    plt.subplots_adjust(hspace=.4)
    #fig.delaxes(axes[i:])
    #plt.tight_layout()
    #plt.ion()
    if save_name is None:
        save_name = f'{date.split(" ")[0]}.jpg'
    plt.savefig(save_name, bbox_inches='tight')
    log.info(f'Wrote {save_name}')
    return fig, axes

def get_rpt(args, spotID, timestamp):
    timestamp = datetime.datetime.utcfromtimestamp(timestamp)
    rpt = reports.get_report(spotID, timestamp)
    #hts = rpt['waveHeight'] # NO!
    hts = {}
    hts['timestamp'] = datetime.datetime.strptime(
        rpt['updatedAt']['$date'].split('.')[0], '%Y-%m-%dT%H:%M:%S').timestamp()
    hts['tide'] = reports.get_tide(spotID, timestamp)
    height_times = HEIGHTS_DF['valid_time.1'].values
    dt = np.abs(timestamp.timestamp() - height_times)
    if dt.min() / 3600 > args.delta_hr_report:
        print('no heights withing specified hour delta')
    hmin, hmax, hocc = HEIGHTS_DF.iloc[dt.argmin()][
        ['surf_minimum_ft', 'surf_maximum_ft', 'surf_occasional_ft']].values
    hts['min'] = hmin
    hts['max'] = hmax
    hts['occasional'] = hocc
    return hts

def get_sigH_from_df(df, max_delta_hr, use_max_hts=False):
    # First check that the dataframe time is >= 25m ish
    if df['timestamp'].max() - df['timestamp'].min() < 25:
        print("Can't calculate sigH from less than 25m")
        return
    # If big time gaps, also reject
    if np.diff(df['timestamp']).max() / 3600 > max_delta_hr:
        print(
            'More than 10m deltas in df. Need contiguous samples'
        )
        return    
    # Look at heights above some y-value
    y0s = df.groupby('track_num')['y'].min().values
    y1s = df.groupby('track_num')['y'].max().values
    # openCV bug where some contours are rotated and coords get crazy.
    k = np.where((y0s>0) & (y1s<720))
    y0s = y0s[k]
    y1s = y1s[k]
    avg_len = (y1s-y0s).mean()
    miny = y0s.mean() + avg_len * 0.5
    max_hts = []
    for _, row in df.iterrows():
        if isinstance(row['ft'], str):
            max_hts.append(np.max([float(x) for x in row['ft'].split(';')]))
        else:
            max_hts.append(np.nan)
    df['max_hts'] = max_hts
    max_hts = sorted(df['max_hts'][df['y'] <= miny].dropna().values)
    hts = df['ft'][df['y'] <= miny].dropna()
    try:
        float_hts = []
        for hh in hts:
            if isinstance(hh, str):
                float_hts += [float(h) for h in hh.split(';')]
            elif isinstance(hh, float):
                float_hts.append(hh)
            else:
                print("UNKNOWN TYPE IN HEIGHTS")
        #hts = sorted([float(h) for hh in hts for h in hh.split(';')])
    except:
        set_trace()

    # Remove any of the massive heights from corrupted frames
    hts = np.asarray(float_hts)
    max_hts = np.asarray(list(map(float, max_hts)))
    hts = hts[np.where(hts<10)]
    max_hts = max_hts[np.where(max_hts<10)]
    
    if use_max_hts:
        sigH = np.mean(max_hts[len(max_hts)//3:])
    else:
        sigH = np.mean(hts[len(hts)//3:])

    return hts, sigH, df['timestamp'].iloc[0]

def get_heights_dict(args, df, max_delta_hr, spotID):
    heights = {}
    dt = df['timestamp'].max() - df['timestamp'].min()
    print(f'Making dict element from {dt/3600:.1f} hrs')
    if 'ft' not in df.columns:
        print('no ft column')
        return
    sigH_dat = get_sigH_from_df(df, max_delta_hr)
    if sigH_dat is None:
        print('No sigH dat')
        return 
    hts, sigH, ts = sigH_dat
    if len(hts) == 0:
        print('No heights')
        set_trace()
        return
    rpt = get_rpt(args, spotID, df['timestamp'].iloc[0])
    if np.abs(rpt['timestamp'] - df['timestamp'].max()) > 6400:
        rpt = get_rpt(args, spotID, df['timestamp'].max())
        #rpt_min, rpt_max, rpt_occ = rpt['min'], rpt['max'], rpt['occasional']
    rpt['tot_hr'] = (df['timestamp'].max() - df['timestamp'].min()) / 3600
    heights[ts] = (hts, sigH, rpt)
    if len(hts) == 0:
        print('empty heights in dict')
    return heights

def daily_heights(args, wavetrax, max_delta_hr=3):
    
    if args.spotid is None:
        alias = vu.get_alias(wavetrax[0])
        spotID = spots.get_spotid(alias)
    else:
        spotID = args.spotid
    rpt = None
    all_heights = {}  # Store all the heights stuff in here.
    dt, i = 0, 1
    try:
        df = pd.read_csv(wavetrax[0])
    except:
        set_trace()
    heights = {}
    while dt < max_delta_hr:
        if i >= len(wavetrax) - 1:
            break
        try:
            idf = pd.read_csv(wavetrax[i])
        except:
            set_trace()
        t0 = df['timestamp'].max()
        t1 = idf['timestamp'].min()
        #df = pd.conca([df, idf])
        if t1 < t0:
            raise ValueError('trax not sorted')
        if (t1 - t0) / 3600 > max_delta_hr:
            if len(df.dropna()) == 0:
                print(f'No feet in df for {args.dir}')
                df = idf
                i += 1
            else:
                heights_dict = get_heights_dict(args, df, max_delta_hr, spotID)
                if heights_dict is not None:
                    all_heights.update(heights_dict)
                df = idf
                i += 1
        else:
            df = pd.concat([df, idf])
            i += 1
    if len(df) > 0:
        hd = get_heights_dict(args, df, max_delta_hr, spotID)
        if hd is not None:
            all_heights.update(hd)
    
    if len(all_heights) > 0:
        pass
        #set_trace()        
    return all_heights


def get_trax_list_around_hour(args, target_hr):
    wavetrax = np.array(
        sorted([
            os.path.join(args.dir, f) for f in
            os.listdir(args.dir) if 'wavetracks' in f
        ])
    )
    if len(wavetrax) == 0:
        print(f'no trax found in {args.dir}')
        return
    track_times = [
        datetime.datetime.utcfromtimestamp(vu.get_vid_timestamp(w))
        for w in wavetrax
    ]
    track_hrs = np.array([t.hour + t.minute / 60 for t in track_times])
    close = np.where(np.abs(track_hrs - target_hr) <= args.delta_hr_report)
    return wavetrax[close]
    

def main(args):
    day_dirs = [os.path.join(args.dir, f) for f in os.listdir(args.dir)
                if os.path.isdir(os.path.join(args.dir, f))
    ]
    all_heights = {}
    print('skipping < 10')
    for day_dir in sorted(day_dirs):
        day = day_dir[-2:]
        if day.isdigit():
            if int(day_dir[-2:]) < 10:
                continue
        else:
            continue
        args.dir = day_dir
        # Get the list of tracks in the morning and afternoon.
        morning_hr = 15
        afternoon_hr = 21
        morning_trax = get_trax_list_around_hour(args, morning_hr)
        afternoon_trax = get_trax_list_around_hour(args, afternoon_hr)
        if morning_trax is None or len(morning_trax) == 0:
            print(f'No morning trax for {args.dir}')
            morning_heights = None
        else:
            morning_heights = daily_heights(args, morning_trax)
        if afternoon_trax is None or len(afternoon_trax) == 0:
            print(f'No afternoon trax for {args.dir}')
            afternoon_heights = None
        else:
            afternoon_heights = daily_heights(args, afternoon_trax)
            
        if morning_heights is not None and len(morning_heights) > 0:
            all_heights.update(morning_heights)
        else:
            print(f'No morning trax for {args.dir}')
        if afternoon_heights is not None and len(afternoon_heights) > 0:
            all_heights.update(afternoon_heights)
        else:
            print(f'No afternoon trax for {args.dir}')
            
    with open('wc-hbpierns-hts.pkl', 'wb') as f:
        pickle.dump(all_heights, f)
    plot_hists(all_heights, save_name='multi-day.jpg')


def plot_from_pkl():
    # Make different plots from the pickle you wrote out above ^^
    with open('wc-hbpierns-hts.pkl', 'rb') as f:
        dat = pickle.load(f)
    times, meas, report, tide = [], [], [], []
    #fig, ax = plt.subplots(figsize=(7, 5))
    plt.figure(figsize=(14,5))
    for k, v in dat.items():
        times.append(datetime.datetime.utcfromtimestamp(k))
        hts, sigh, rpt = v
        meas.append(sigh)
        rpt_hts = [rpt['min'], rpt['max']]
        #if rpt['occasional'] > 0:
        #    rpt_hts.append(rpt['occasional'])
        report.append(np.mean(rpt_hts))
        tide.append(rpt['tide'])
        
    plt.plot(times, meas, '-o', label='measured sigH')
    plt.plot(times, report, '-o', label='reported height')
    plt.plot(times, tide, '--', label='tide', alpha=0.5)
    plt.xlabel('date')
    plt.ylabel('ft')
    plt.xticks(rotation=45)
    plt.legend()
    plt.tight_layout()
    plt.savefig('timeseries.jpg')
    
    plt.figure(figsize=(7,5))
    plt.hist(np.array(meas) - np.array(report), bins=6)
    plt.xlabel('measured - report')
    plt.tight_layout()
    plt.savefig('dsigH.jpg')
        
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--dir', help='dir full of wavetrack files.',
                        default='/archived-rewinds/main-data/wc-hbpierns/2021/01')
    parser.add_argument('-s', '--spotid', default='5842041f4e65fad6a7708827')
    parser.add_argument('-w', '--window', default=3, help='this many rewinds used to get sigH')
    parser.add_argument('--days', default=True, action='store_true',
                        help='Pass directory above to parse days')
    parser.add_argument('--delta_hr_report',
                        help='how many hours +/- about report to consider trax.',
                        type=int, default=1
    )
    parser.add_argument('--postplot', action='store_true')
    args = parser.parse_args()
    if args.postplot:
        plot_from_pkl()
    else:
        main(args)
