import os
import argparse
import pandas as pd
import pickle
import numpy as np
import datetime
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from ipdb import set_trace

from labs_utils import reports, spots
from sl_camera_sensor.utils.logger import getLogger


log = getLogger(__name__)


def parse_timestamp(ts):
    """
    Return datetime object from string timestamp convention. Works with old and new
    format (with/without milliseconds).
    Args:
        ts (str): Timestamp to parse.

    Returns:
        dt (datetime.datetime): Datetime.
    """

    # Parse string based on new or old format (with/without milliseconds).
    fmt = "%Y%m%dT%H%M%S%f" if len(ts) == 18 else "%Y%m%dT%H%M%S"
    dt = datetime.datetime.strptime(ts, fmt)
    return dt


def match_reports_to_wavetracks(
        wavetracks, fit_thresh, rpt_a_dt, rpt_b_dt, hrs_before, hrs_after
):
    """
    Match reports to wavetracks based on time parameters.
    Args:
        wavetracks (list): List of wavetrack files.
        fit_thresh (flaot): Threshold to clip fits above.
        rpt_a_dt (datetime.datetime): Datetime of rpt a.
        rpt_b_dt (datetime.datetime): Datetime of rpt b.
        hrs_before (float): Hours before each report you want wavetracks.
        hrs_after (float: Hours after each report you want wavetracks.

    Returns:
        rpt_a_hts (list): List of wavetrack dataframes corresponding to rpt a.
        rpt_b_hts (list): List of wavetrack dataframes corresponding to rpt b.
    """
    rpt_a_t0 = rpt_a_dt - datetime.timedelta(hours=hrs_before)
    rpt_a_t1 = rpt_a_dt + datetime.timedelta(hours=hrs_after)
    rpt_b_t0 = rpt_b_dt - datetime.timedelta(hours=hrs_before)
    rpt_b_t1 = rpt_b_dt + datetime.timedelta(hours=hrs_after)

    rpt_a_hts = []
    rpt_b_hts = []
    vid_names_a, vid_names_b = [], []  # Store which rewinds these come from too
    for f in wavetracks:
        df = pd.read_csv(f)
        # Clip all fits above threshold
        #df = df.loc[df['fit'] <= 0.95]  #fit_thres]
        dt = parse_timestamp(f.split('.')[2])
        if 'ft' not in df.columns:
            log.info(f'No ft column found in {f}. Try running wave height stack.')
            continue
        if rpt_a_t0 <= dt <= rpt_a_t1:
            rpt_a_hts.append(df)
            vid_names_a.append(f.replace('wavetracks', 'stack').replace('.csv','.mp4'))
        elif rpt_b_t0 <= dt <= rpt_b_t1:
            rpt_b_hts.append(df)
            vid_names_b.append(f.replace('wavetracks', 'stack').replace('.csv','.mp4'))

    if not rpt_a_hts:
        log.info(f'No wavetrack files found to match report at {rpt_a_dt}.')
    if not rpt_b_hts:
        log.info(f'No wavetrack files found to match report at {rpt_b_dt}.')
    return rpt_a_hts, rpt_b_hts, vid_names_a, vid_names_b


def get_miny(df):
    """
    Get a minimum y cutoff value for waves (adjustable).
    Args:
        df (pd.DataFrame): Dataframe to calculate miny.

    Returns:
        miny (float): Cutoff value.
    """
    # Look at heights above some y-value
    y0s = df.groupby('track_num')['y'].min().values
    y1s = df.groupby('track_num')['y'].max().values
    # openCV bug where some contours are rotated and coords get crazy.
    k = np.where((y0s > 0) & (y1s < 720))
    y0s = y0s[k]
    y1s = y1s[k]
    avg_len = (y1s - y0s).mean()
    miny = y0s.mean() + avg_len * 0.5
    return miny


def calculate_sigh(dfs):
    """
    Pass this a list of all wavetrack dataframes about a report time. Modifiable
    method to calculate sigH using max height per wave.
    Args:
        dfs (list): List of dataframes that will be used to calculate sigh.

    Returns:
        float: sigh value.
    """
    if not dfs:
        return None
    max_hts = []
    for df in dfs:
        df['max_per_row'] = df['ft'].apply(
            lambda row: np.max([float(x) for x in str(row).split(';')])
            if row is not None else None
        )
        miny = get_miny(df)
        # df = df[df['y'] <= miny]  # Remove mushy waves that are past miny.
        # TODO: Remove large hts from corrupted waves:
        #df = df[df['fit'] >= 0.95] this was already done...
        max_grouped = df[['track_num', 'max_per_row']].groupby('track_num').max()
        max_ht_per_wave = max_grouped.dropna().values
        if max_ht_per_wave.size > 0:
            max_hts.extend(max_ht_per_wave)

    if not max_hts:
        if len(dfs) > 0:
            start = datetime.datetime.utcfromtimestamp(dfs[0]['timestamp'].values[0])
            end = datetime.datetime.utcfromtimestamp(dfs[-1]['timestamp'].values[-1])
            log.info(f'No wave heights measured from {start} to {end}.')
        return None
    max_hts = np.sort(np.asarray(max_hts))[::-1]  # Sort descending
    sigH = np.mean(max_hts[:len(max_hts) // 3])
    return sigH


def get_max_hts(dfs):
    """
    Given a list of wavetrack dataframes, get the max height per wave and
    corresponding datetime.
    Args:
        dfs (list): List of dataframes

    Returns:
        dict, list
    """
    max_hts = {}
    fits = {}
    ys = {}
    for df in dfs:
        df['max_per_row'] = df['ft'].apply(
            lambda row: np.max([float(x) for x in str(row).split(';')])
            if row is not None else None
        )
        df = df.sort_values('max_per_row', ascending=False)
        filtered = df.drop_duplicates(['track_num'], keep='first')
        filtered['datetime'] = filtered['timestamp'].copy().apply(
            lambda x: datetime.datetime.utcfromtimestamp(x)
        )
        filtered = filtered[filtered['max_per_row'].notna()].sort_values(by='datetime')
        np_datetimes = filtered['datetime'].values
        dts = [
            datetime.datetime.strptime(str(d).split('.')[0], '%Y-%m-%dT%H:%M:%S')
            for d in np_datetimes
        ]
        fits.update(
            zip(list(dts), list(filtered['fit']))
        )
        max_hts.update(
            zip(list(dts), list(filtered['max_per_row']))
        )
        ys.update(
            zip(list(dts), list(filtered['y']))
        )
    return max_hts, fits, ys


def plot_sigh_timeseries(plots_dir, alias, dts, sighs, rpts):
    """
    Plot a time series of sigH values compared to report values over different tides.
    Args:
        plots_dir (str): Path to directory where plots will be saved.
        alias (str): Alias of spot.
        dts (list): List of datetimes of reports.
        sighs (list): List of calculated sighs corresponding to report times.
        rpts (list): List of reports.

    Returns:
        None
    """
    populated_idxs = np.where(np.array(sighs) != None)
    sighs = np.array(sighs)[populated_idxs]
    dts = np.array(dts)[populated_idxs]
    rpts = np.array(rpts)[populated_idxs]
    mean_rpt_hts = [np.mean([rpt['min'], rpt['max']]) for rpt in rpts]
    tides = [rpt['tide'] for rpt in rpts]

    plt.figure(figsize=(14, 5))
    plt.plot(dts, sighs, '-o', label=r'Measured $H_{1/3}$')
    plt.plot(dts, mean_rpt_hts, '-o', label='Mean reported height')
    plt.plot(dts, tides, '--', label='Tide', alpha=0.5)
    plt.xlabel('date')
    plt.ylabel('ft')
    plt.title('Measured sigH vs Mean Reported Height')
    plt.xticks(rotation=45)
    plt.legend()
    plt.tight_layout()
    plt.savefig(os.path.join(plots_dir, alias, f'{alias}.sigh.timeseries.jpg'))
    plt.close()

    plt.title('Difference of Measured sigH vs Mean Reported Height')
    plt.figure(figsize=(7, 5))
    plt.hist(np.array(sighs) - np.array(mean_rpt_hts), bins=6)
    plt.xlabel('measured - report')
    plt.tight_layout()
    plt.savefig(os.path.join(plots_dir, alias, f'{alias}.sigh.difference.jpg'))
    plt.close()


def plot_max_ft_timeseries(plots_dir, alias, all_data) -> None:
    """
    Plot a time series of sigH values compared to report values over different tides.
    Args:
        plots_dir (str): Path to directory where plots will be saved.
        alias (str): Alias of spot.
        all_data (dict): Dictionary of all data.

    Returns:
        None
    """
    for date_str in sorted(all_data):
        mean_rpt_ht = np.mean(
            [all_data[date_str]['morning']['report']['min'],
             all_data[date_str]['morning']['report']['max']]
        )
        sigh = all_data[date_str]['morning']['sigH']
        dfs = all_data[date_str]['morning']['heights']
        max_hts, fits, ys = get_max_hts(dfs)
        if max_hts:
            plt.figure(figsize=(14, 5))
            times = [
                datetime.datetime.strptime(f'{dt.hour}:{dt.minute}:{dt.second}',
                                           "%H:%M:%S")
                for dt in max_hts.keys()
            ]
            plt.plot(
                times, list(max_hts.values()), '-o',
                label='Max measured height per wave'
            )
            plt.gca().xaxis.set_major_formatter(mdates.DateFormatter("%H:%M:%S"))
            plt.hlines(
                y=mean_rpt_ht, xmin=times[0], xmax=times[-1],
                linestyles='--', label='Mean reported height', alpha=0.25, color='r'
            )
            plt.hlines(
                y=sigh, xmin=times[0], xmax=times[-1],
                linestyles='--', label=r'Measured $H_{1/3}$', alpha=0.75, color='r'
            )
            plt.xlabel('hour (UTC)')
            plt.ylabel('ft')
            plt.title(f'{alias} {date_str} Morning Maximum Height per Wave')
            plt.xticks(rotation=45)
            plt.legend()
            plt.tight_layout()
            save_date = ''.join(
                [s.zfill(2) if len(s) == 1 else s for s in date_str.split('/')]
            )
            save_name = os.path.join(
                plots_dir, alias, f'{alias}.{save_date}.morning.hts.jpg'
            )
            plt.savefig(save_name)
            plt.close()

            plt.scatter(list(max_hts.values()), list(ys.values()))
            plt.xlabel('Max Measured Height Per Wave')
            plt.ylabel('Y Coordinate')
            plt.title(f'{alias} {date_str} Morning Wave Locations')
            save_name = os.path.join(
                plots_dir, alias, f'{alias}.{save_date}.morning.locations.jpg'
            )
            plt.savefig(save_name)
            plt.close()

            '''
            # Create scatter of fit v height
            plt.scatter(list(fits.values()), list(max_hts.values()))
            plt.xlabel('Fit')
            plt.ylabel('Max Wave Height')
            plt.title(f'{alias} {date_str} Morning Wave Fits')
            save_name = os.path.join(
                plots_dir, alias, f'{alias}.{save_date}.morning.fit.jpg'
            )
            plt.savefig(save_name)
            plt.close()
            '''

        # Same for afternoon
        mean_rpt_ht = np.mean(
            [all_data[date_str]['afternoon']['report']['min'],
             all_data[date_str]['afternoon']['report']['max']]
        )
        sigh = all_data[date_str]['afternoon']['sigH']
        dfs = all_data[date_str]['afternoon']['heights']
        max_hts, fits, ys = get_max_hts(dfs)
        if max_hts:
            plt.figure(figsize=(14, 5))
            times = [
                datetime.datetime.strptime(f'{dt.hour}:{dt.minute}:{dt.second}',
                                           "%H:%M:%S")
                for dt in max_hts.keys()
            ]
            plt.plot(
                times, list(max_hts.values()), '-o',
                label='Max measured height per wave'
            )
            plt.gca().xaxis.set_major_formatter(mdates.DateFormatter("%H:%M:%S"))
            plt.hlines(
                y=mean_rpt_ht, xmin=times[0], xmax=times[-1],
                linestyles='--', label='Mean reported height', alpha=0.25, color='r'
            )
            plt.hlines(
                y=sigh, xmin=times[0], xmax=times[-1],
                linestyles='--', label=r'Measured $H_{1/3}$', alpha=0.75, color='r'
            )
            plt.xlabel('hour (UTC)')
            plt.ylabel('ft')
            plt.title(f'{alias} {date_str} Afternoon Maximum Height per Wave')
            plt.xticks(rotation=45)
            plt.legend()
            plt.tight_layout()
            save_date = ''.join(
                [s.zfill(2) if len(s) == 1 else s for s in date_str.split('/')]
            )
            save_name = os.path.join(
                plots_dir, alias, f'{alias}.{save_date}.afternoon.hts.jpg'
            )
            plt.savefig(save_name)
            plt.close()

            plt.scatter(list(max_hts.values()), list(ys.values()))
            plt.xlabel('Max Measured Height Per Wave')
            plt.ylabel('Y Coordinate')
            plt.title(f'{alias} {date_str} Afternoon Wave Locations')
            save_name = os.path.join(
                plots_dir, alias, f'{alias}.{save_date}.afternoon.locations.jpg'
            )
            plt.savefig(save_name)
            plt.close()

            '''
            # Create scatter of fit v height
            plt.scatter(list(fits.values()), list(max_hts.values()))
            plt.xlabel('Fit')
            plt.ylabel('Max Wave Height')
            plt.title(f'{alias} {date_str} Afternoon Wave Fits')
            save_name = os.path.join(
                plots_dir, alias, f'{alias}.{save_date}.afternoon.fit.jpg'
            )
            plt.savefig(save_name)
            plt.close()
            '''


def main(
        base_dir, plots_dir, fit_thresh, min_date, max_date,
        rpt_a_hr, rpt_b_hr, hrs_before, hrs_after
):
    """
    Get all report data and compare with heights of waves in wavetrack files. Plots
    and saves all data to plot_dir.
    Args:
        base_dir (str): Base directory. Should be an input of the form
        /path/to/validation-data/<alias>. Subdirectories should be organized by date
        like <alias>/<year>/<month>/<day>/...
        plots_dir (str): Path to directory where plots and pkl files will be saved
        fit_thresh (float): Threshold to clip waves with bad fit.
        min_date (str): Minimum date to include in plotting. Include string of the
        form '2020/01/01'
        max_date (str): Maximum date to include in plotting. Include string of the
        form '2020/01/01'
        rpt_a_hr (int): Hour of report a.
        rpt_b_hr (int): Hour of report b.
        hrs_before (float): # of hours before each report about which you want to
        look at wave height data.
        hrs_after (float): # of hours after each report about which you want to look at
        wave height data.

    Returns:
        all_data (dict): Dictionary of all relevant data.
    """
    if min_date and not isinstance(min_date, datetime.datetime):
        try:
            min_date = datetime.datetime.strptime(min_date, '%Y/%m/%d')
            max_date = datetime.datetime.strptime(max_date, '%Y/%m/%d')
        except ValueError:
            raise ValueError('Input dates as %Y/%m/%d, ex 2021/01/31. Or use '
                             'datetime.datetime object.')
    split_path = base_dir.rstrip('/').split(os.sep)
    if '-' not in split_path[-1]:
        raise ValueError('base_dir should end with alias')
    alias = split_path[-1]
    #alias = split_path[-2] if not split_path[-1] else split_path[-1]
    spotid = spots.get_spotid(alias)
    all_data = {}
    dts, sighs, rpts = [], [], []
    for root, dirs, files in os.walk(base_dir):
        dirs.sort()  # Traverse directories in temporal order
        wavetracks = [
            os.path.join(root, f) for f in files
            if ('wavetracks' in f) and ('.1fps' in f)
        ]
        if not wavetracks:
            continue
        year, month, day = [int(e) for e in root.split(os.sep)[-3:]]
        rpt_a_dt = datetime.datetime(year=year, month=month, day=day, hour=rpt_a_hr)
        rpt_b_dt = datetime.datetime(year=year, month=month, day=day, hour=rpt_b_hr)
        if min_date and (max_date < rpt_a_dt < min_date):
            continue
        rpt_a_hts, rpt_b_hts, vids_a, vids_b = match_reports_to_wavetracks(
            wavetracks, fit_thresh, rpt_a_dt, rpt_b_dt, hrs_before, hrs_after
        )
        rpt_a = reports.get_wave_hts(spotid, rpt_a_dt)
        rpt_b = reports.get_wave_hts(spotid, rpt_b_dt)
        all_data[f'{year}/{month}/{day}'] = {
            'morning': {'report': None, 'heights': None},
            'afternoon': {'report': None, 'heights': None}
        }
        all_data[f'{year}/{month}/{day}']['morning']['report'] = rpt_a
        all_data[f'{year}/{month}/{day}']['morning']['heights'] = rpt_a_hts
        try:
            all_data[f'{year}/{month}/{day}']['morning']['sigH'] = calculate_sigh(rpt_a_hts)
            all_data[f'{year}/{month}/{day}']['afternoon']['sigH'] = calculate_sigh(rpt_b_hts)
        except:
            log.info('Cannot calculate sigH for {year}-{month}-{day}')
        all_data[f'{year}/{month}/{day}']['afternoon']['report'] = rpt_b
        all_data[f'{year}/{month}/{day}']['afternoon']['heights'] = rpt_b_hts
        # Add paths to videos so we can sanity check
        all_data[f'{year}/{month}/{day}']['morning']['vids'] = vids_a
        all_data[f'{year}/{month}/{day}']['afternoon']['vids'] = vids_b

        dts.append(rpt_a_dt)
        sighs.append(all_data[f'{year}/{month}/{day}']['morning']['sigH'])
        rpts.append(rpt_a)

        dts.append(rpt_b_dt)
        sighs.append(all_data[f'{year}/{month}/{day}']['afternoon']['sigH'])
        rpts.append(rpt_b)

    # Make plotting/pkl directory if it doesn't already exist.
    if plots_dir is None:
        plots_dir = os.getcwd()
    os.makedirs(os.path.join(plots_dir, alias), exist_ok=True)
    plot_sigh_timeseries(plots_dir, alias, dts, sighs, rpts)
    plot_max_ft_timeseries(plots_dir, alias, all_data)

    with open(os.path.join(plots_dir, alias, f'{alias}.all_data.pkl'), 'wb') as f:
        pickle.dump(all_data, f)

    return all_data


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-b', '--base_dir', dest='base_dir', default=None, type=str, required=True,
        help='Specify path to base spot directory of validation data. Should be an '
             'input of the form /path/to/validation-data/<alias>. Subdirectories '
             'should be organized by date like <alias>/<year>/<month>/<day>/...'
    )
    parser.add_argument(
        '-p', '--plots_dir', dest='plots_dir', default=None, type=str, required=False,
        help='Specify path to directory where we want to store the plots. If it does '
             'not exist, it will be created. NOTE: Will automatically create alias '
             'subdirectory to store plots, so don\'t include this.'
    )
    parser.add_argument(
        '-f', '--fit_thresh', dest='fit_thresh', default=1000, type=float,
        required=False, help='Specify threshold to clip waves with bad fit.'
    )
    parser.add_argument(
        '--min_date', dest='min_date', default=None, type=str, required=False,
        help='Specify minimum date to include in plotting. Include string of the form '
             '2020/01/01'
    )
    parser.add_argument(
        '--max_date', dest='max_date', default=None, type=str, required=False,
        help='Specify maximum date to include in plotting. Include string of the form '
             '2020/01/01'
    )
    parser.add_argument(
        '--rpt_a_hr', default=15, type=int, help='Hour of report a in UTC.'
    )
    parser.add_argument(
        '--rpt_b_hr', default=20, type=int, help='Hour of report b in UTC.'
    )
    parser.add_argument(
        '--hrs_before', default=1, type=float,
        help='# of hours before each report about which you want to look at wave '
             'height data.'
    )
    parser.add_argument(
        '--hrs_after', default=1, type=float,
        help='# of hours after each report about which you want to look at wave '
             'height data.'
    )
    args = parser.parse_args()
    main(
        args.base_dir, args.plots_dir, args.fit_thresh, args.min_date, args.max_date,
        args.rpt_a_hr, args.rpt_b_hr, args.hrs_before, args.hrs_after
    )
