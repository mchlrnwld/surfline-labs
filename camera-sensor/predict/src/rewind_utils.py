""" Various utilities for neural network pre- and
post-processing of cam rewinds.
"""
import os
import cv2
import math

import numpy as np

from sl_camera_sensor.utils import logger
from PIL import Image
from datetime import datetime
from pathlib import Path

_logger = logger.getLogger(__name__)


def generate_frames(
    rewind_file_key,
    input_video,
    video_start_timestamp,
    fps=None,
    target_frame_size=(1280, 720),
):
    """
    Saves frames and timestamps at fps
    resizes frames to target size, saves frames to images.
    """
    frames = []

    # get time increment to move frame pointer by every frame pull
    time_inc = float(1 / fps)

    resize_width, resize_height = target_frame_size[0], target_frame_size[1]

    # Open video file as OpenCV object.
    cap = cv2.VideoCapture(input_video)
    native_fps = cap.get(cv2.CAP_PROP_FPS)
    frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

    _logger.info(f"{frame_count} frames in {input_video} at {native_fps} fps")

    batch_size = math.ceil(frame_count * (fps / native_fps))
    _logger.info(f"Numer of frames to return: {batch_size}")

    frames_dir = "/usr/src/app/frames"
    Path(frames_dir).mkdir(parents=True, exist_ok=True)
    _logger.info(f"Output frames to: {frames_dir}")

    # Determine source video frame size.
    if cap.isOpened():
        source_width = cap.get(cv2.CAP_PROP_FRAME_WIDTH)
        source_height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)

    _logger.info(
        f"Source size: {source_width}x{source_height} target: {resize_width}x{resize_height}"
    )

    # Iterate the required batch_size of frames (determined using the passed fps and frame count of video file).
    for i in range(batch_size):
        flag, frame = cap.read()

        if not flag:
            break

        vid_timestamp = cap.get(cv2.CAP_PROP_POS_MSEC) / 1000.0

        # If we have a frame add it to the return array.
        if frame is not None:
            # Timestamp will be the video start timestamp (UTC) of when the rewind
            # file was clipped and begins plus the current seconds we're through this video
            # as determined by line 62.
            ts = video_start_timestamp + vid_timestamp

            # If the frame is larger than our target size then resize it down now.
            if source_width is not resize_width and source_height is not resize_height:
                frame = np.array(
                    Image.fromarray(frame).resize(
                        (resize_width, resize_height), Image.LANCZOS
                    )
                )

            # Write frame data to disk.
            dt_str = datetime.utcfromtimestamp(ts).strftime("%Y%m%dT%H%M%S%f")
            original_frame_path = (
                f"{frames_dir}/{rewind_file_key}-{dt_str}-original.jpg"
            )
            cv2.imwrite(original_frame_path, frame)

            frames.append(
                {
                    "timestamp": ts,
                    "image_path": original_frame_path,
                    "frame": bgr2rgb(frame),
                }
            )
        else:
            break

        # Advance video "pointer" within opencv video object to the next frame we'll want
        # without iterating/decoding each frame to get there.
        cap.set(cv2.CAP_PROP_POS_MSEC, (vid_timestamp + time_inc) * 1000.0)

    return frames


def get_frame_batch(
    input_video,
    video_start_timestamp,
    batch_size,
    at_timestamps=None,
    max_frames=None,
    unix_time=True,
    fps=None,
    target_frame_size=(1280, 720),
):
    """ 10 minutes of video is ~64g of ram in memory.
    This generator will return batches of frames,
    since we might not have enough memory to process
    a whole movie in a single pass.
    :param video_start_timestamp: time in seconds that the video begins.
    Timestamps will be offset by this value, otherwise they'll start at 0+fps.
    :at_timestamps: floating point input array where only frames at these
    timestamps will be returned.
    :returns: frames, timestamps tuple of lists.
    """
    if at_timestamps:
        raise NotImplementedError("at_timestamps not implemented here.")

    if not os.path.isfile(input_video):
        raise ValueError("Input video '{}' does not exist.".format(input_video))

    resize_width, resize_height = target_frame_size[0], target_frame_size[1]

    max_frames = max_frames or 36000

    cap = cv2.VideoCapture(input_video)
    native_fps = cap.get(cv2.CAP_PROP_FPS)
    _logger.info(
        "{0} frames in {1} at {2:.3f} fps".format(
            int(cap.get(cv2.CAP_PROP_FRAME_COUNT)), input_video, native_fps
        )
    )

    if not fps:
        fps = native_fps

    if not at_timestamps:
        _logger.info(
            "Extracting up to {0} frames in batches of "
            "{1} at ~{2:.3f} fps.".format(max_frames, batch_size, fps)
        )
    else:
        if fps != native_fps:
            raise ValueError(
                "Functionality to input both output fps and"
                " at_timestamps is not supported."
            )

    # Determine source video frame size.
    if cap.isOpened():
        source_width = cap.get(cv2.CAP_PROP_FRAME_WIDTH)
        source_height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)

    _logger.info(
        f"Source size: {source_width}x{source_height} target: {resize_width}x{resize_height}"
    )

    cnt = 0
    frames, timestamps = [], []
    timestamp = (cap.get(cv2.CAP_PROP_POS_MSEC) / 1000.0) + video_start_timestamp
    flag, frame = cap.read()  # Put this after timestamp calc!
    # start_time = video_start_timestamp if unix_time else 0.
    if fps > native_fps * 1.1:
        _logger.warning(
            "Requested input fps is " "higher than native ({native_fps}fps)."
        )
    while flag:
        if (cnt > 0) and (len(timestamps) == batch_size):  # (cnt % batch_size) == 0):
            yield frames, timestamps
            frames, timestamps = [], []

        if cnt > max_frames - 1:
            break

        if cnt % round(native_fps / float(fps)) == 0:
            # Check input source size matches target size if not resize. Model is trained at 720p, some cams are bigger.
            if source_width is not resize_width and source_height is not resize_height:
                frame = np.array(
                    Image.fromarray(frame).resize(
                        (resize_width, resize_height), Image.NEAREST
                    )
                )

            frames.append(bgr2rgb(frame))
            timestamps.append(timestamp)

        timestamp = (cap.get(cv2.CAP_PROP_POS_MSEC) / 1000.0) + video_start_timestamp
        flag, frame = cap.read()

        cnt += 1
        # time.sleep(1./30 * batch_size)  # Emulate a slow stream (15fps)

    yield frames, timestamps


def run_batch_inference(
    input_video,
    video_start_timestamp,
    detection_bot,
    frames_batch_size=300,
    max_frames=36000,
    fps=None,
    target_frame_size=(1280, 720),
):
    """ Calls the generator above and passes each batch
    through the detection_bot, and returns detections.

    :param input_video: string of input video name
    :param detection_bot: DetectionModel instance.
    :returns: detection dictionaries with temporal component added.
    If check_poor_light is set in the detection_config, then 
    -1 will be returned instead of detection dicts in the
    event bad lighting is detected.
    """
    # Extract frames and timestamps in batches. Run the whole
    # thing through the GPU prior to extracting tracks.
    t0 = time.time()
    frame_count = 0

    detections, timestamps = [], []
    batches = get_frame_batch(
        batch_size=frames_batch_size,
        max_frames=max_frames,
        input_video=input_video,
        video_start_timestamp=video_start_timestamp,
        fps=fps,
        target_frame_size=target_frame_size,
    )
    for batch_frames, batch_timestamps in batches:
        if len(batch_frames) == 0:
            break

        frame_count += len(batch_frames)

        batch_detections = detection_bot.predict(batch_frames)

        if batch_detections == -1:
            _logger.info("Bad lighting detected in {}".format(input_video))
            return -1  # Poor light.

        detections += batch_detections
        timestamps += batch_timestamps

    # Add time component to detection dictionaries.
    for i in range(len(detections)):
        detections[i]["timestamp"] = timestamps[i]

    te = time.time()
    _logger.info(
        "Frame extraction and inference took {0:.0f}m {1:.0f}s".format(
            *divmod(te - t0, 60)
        )
    )
    time_fps = (te - t0) / frame_count
    _logger.info(f"Processed {frame_count} frames at {time_fps} FPS")

    del batches
    return detections


def get_fps(video_file):
    """
    Return fps from input video file string.

    :param video_file: string name of video file you want to inspect.
    :return: float of fps.
    """
    return cv2.VideoCapture(video_file).get(cv2.CAP_PROP_FPS)


def bgr2rgb(frame):
    """ OpenCV convention is BGR, but network wants RGB.
    :param frame: BGR numpy array                       
    :return: RBG numpy array.
    """
    return cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)


def rgb2bgr(frame):
    """ Roll color axes from RGB order to BGR OpenCV convention.                                             
    :param frame: RGB numpy array
    :return: RGB numpy array.                                                                               
    """
    return cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
