#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
import argparse
import boto3
import os
import datetime
import rewind_utils
import pickle

# import MySQLdb as mysql
import mysql.connector
from common import timer, logger
from sl_camera_sensor.lib.localization.localize_dets import LocalizeDets
from sl_camera_sensor.lib.crowds.clip_above_thresh import clip_above_thresh
from sl_camera_sensor.lib.wave_tracking.wave_tracker_iou import WaveTracker
from sl_camera_sensor.nets.segmentation.segmentation_model import SegmentationModel
from sl_camera_sensor.nets.detection.detection_model import DetectionModel
from utils.RewindManager import RewindManager
from utils.ModelManager import ModelManager
from highlights.HighlightsVideo import HighlightsVideo

_logger = logger.getLogger()

_env = os.environ["ENV"]

_logger.info(f"ENV: {_env}")

if _env == "LOCAL":
    _bucketName = "prototype-ml-cam-local"
else:
    _bucketName = "prototype-ml-cam-dev"


def getWaveTracks(
    seg_bot,
    video,
    video_start_timestamp,
    fps,
    target_frame_size,
    threshold=0.25,
    det_bot=None,
    run_detections=False,
):
    """ This runs 50% of our vision stack. Can add vis and texture if we want. """
    if run_detections:
        if det_bot is None:
            raise ValueError("Requested run object detection but det_bot is None")
        localizer = LocalizeDets(
            dets_label_map=det_bot.label_map,
            mask_label_map=seg_bot.label_map,
            target_size=target_frame_size[::-1],
        )

    wave_tracker = WaveTracker(
        fps, seg_bot.label_map, target_size=target_frame_size[::-1]
    )

    batches = rewind_utils.get_frame_batch(video, video_start_timestamp, 8, fps=fps)

    start = timer.start()
    for batch in batches:
        frames, timestamps = batch
        # run the two deep nets
        masks = seg_bot.predict(frames, output_size=target_frame_size[::-1])
        if run_detections:
            dets = det_bot.predict(frames)
            # clip the dets
            dets = clip_above_thresh(dets, threshold)
            # Localize the dets to fix false surfers on sand -> beach people
            dets = localizer(dets, masks[0])
            # Update the tracker. Include the dets to populate surfing flag in tracks col
            for mask, det, timestamp in zip(masks, dets, timestamps):
                wave_tracker(
                    mask,
                    dets=det,
                    dets_label_map=det_bot.label_map,
                    timestamp=timestamp,
                )
        else:
            for mask, timestamp in zip(masks, timestamps):
                wave_tracker(mask, timestamp=timestamp)

    wave_tracks = wave_tracker.pop_tracks(return_blobs=False)
    timer.checkpoint("Wave tracking stack took:", start)
    return wave_tracks


def makeHighlightsVideo(
    video_file,
    video_start_timestamp,
    tracks,
    target_frame_size,
    max_duration,
    start_buffer,
    end_buffer,
):
    hv = HighlightsVideo(
        video_file,
        video_start_timestamp,
        tracks,
        target_frame_size,
        max_duration,
        start_buffer,
        end_buffer,
    )

    clips = hv.generateClips(True)

    return clips


def uploadClips(cameraId, clips):
    keys = []

    for clip in clips:
        # Start and end times for a concatenated set of clips basically just gives the 10m interval.
        start = datetime.datetime.utcfromtimestamp(clip["start"]).strftime(
            "%Y%m%dT%H%M%S%f"
        )
        end = datetime.datetime.utcfromtimestamp(clip["end"]).strftime(
            "%Y%m%dT%H%M%S%f"
        )
        path = clip["clip_path"]
        thumb_path = clip["thumb_path"]
        gif_path = clip["gif_path"]

        objectKey = f"highlights-sets/{cameraId}-{start}-{end}.mp4"
        _logger.info(
            f"Uploaded highlight clip {path} to: s3://{_bucketName}/{objectKey}"
        )
        s3 = boto3.resource("s3")
        s3.Object(_bucketName, objectKey).upload_file(
            path, ExtraArgs={"ACL": "public-read", "ContentType": "video/mp4"}
        )

        thumbObjectKey = f"highlights-sets/{cameraId}-{start}-{end}.jpg"
        _logger.info(
            f"Uploaded highlight thumbnail {thumb_path} to: s3://{_bucketName}/{thumbObjectKey}"
        )
        s3 = boto3.resource("s3")
        s3.Object(_bucketName, thumbObjectKey).upload_file(
            thumb_path, ExtraArgs={"ACL": "public-read", "ContentType": "image/jpeg"}
        )

        gifObjectKey = f"highlights-sets/{cameraId}-{start}-{end}.gif"
        _logger.info(
            f"Uploaded highlight gif {gif_path} to: s3://{_bucketName}/{gifObjectKey}"
        )
        s3 = boto3.resource("s3")
        s3.Object(_bucketName, gifObjectKey).upload_file(
            gif_path, ExtraArgs={"ACL": "public-read", "ContentType": "image/gif"}
        )

        keys.append(
            {
                "objectKey": objectKey,
                "thumbObjectKey": thumbObjectKey,
                "gifObjectKey": gifObjectKey,
                **clip,
            }
        )

    return keys


def uploadAnnotatedVideo(filename, annotatedVideoPath):
    newKey = f"annotated/{filename}-annotated.mp4"

    _logger.info(f"Uploaded annotated video to: s3://{_bucketName}/{newKey}")
    s3 = boto3.resource("s3")
    s3.Object(_bucketName, newKey).upload_file(
        annotatedVideoPath, ExtraArgs={"ACL": "public-read", "ContentType": "video/mp4"}
    )

    return newKey


def saveData(
    cameraId,
    objectKey,
    highlightObjectKey,
    thumbObjectKey,
    gifObjectKey,
    startTimestamp,
):

    # db = mysql.connect(
    db = mysql.connector.connect(
        host=os.environ["AURORA_HOST"],
        user=os.environ["AURORA_USER"],
        password=os.environ["AURORA_PASSWORD"],
        database=os.environ["AURORA_DB"],
    )

    cursor = db.cursor()

    sql = "INSERT INTO highlight_clip (id, camera_id, highlight_object_key, original_object_key, thumb_object_key, gif_object_key, timestamp) VALUES (NULL, %s, %s, %s, %s, %s, %s)"
    vals = (
        cameraId,
        highlightObjectKey,
        objectKey,
        thumbObjectKey,
        gifObjectKey,
        startTimestamp,
    )
    cursor.execute(sql, vals)

    db.commit()

    if cursor.rowcount is 1:
        cursor.lastrowid
        _logger.info(f"Added {cursor.rowcount} rows to highlight_clip table.")
    else:
        raise Exception("Unable to insert highlight_clip into DB.")


def str2bool(v):
    if v.lower() in ("yes", "true", "t", "y", "1"):
        return True
    elif v.lower() in ("no", "false", "f", "n", "0"):
        return False
    else:
        raise argparse.ArgumentTypeError("Boolean value expected.")


def main(argv):
    parser = argparse.ArgumentParser(description="Process video.")

    parser.add_argument(
        "--rewind",
        "-v",
        default=None,
        type=str,
        help="Full path to the .mp4 rewind you want to process.",
    )

    parser.add_argument(
        "--fps",
        "-fps",
        default=1.0,
        type=float,
        help="min fps for wave tracking to work. It's probably lower than 1.5 even.",
    )

    parser.add_argument(
        "--downloadmodel",
        "-dm",
        default=False,
        type=str2bool,
        help="Force downloading the model from S3.",
    )

    parser.add_argument(
        "--tracksfile",
        "-tf",
        default=False,
        type=str2bool,
        help="Use this wave tracks file vs create a new one. Useful for skipping inference.",
    )

    parser.add_argument(
        "--dumptracks",
        "-dd",
        default=False,
        type=str2bool,
        help="Dump wavetracks file after creating.",
    )

    parser.add_argument(
        "--saveoutput",
        "-so",
        default=True,
        type=str2bool,
        help="Save output to Aurora and S3.",
    )

    parser.add_argument(
        "--highlights",
        "-hv",
        default=False,
        type=str2bool,
        help="Create highlights video file.",
    )

    parser = parser.parse_args()

    if not parser.rewind:
        raise ValueError("Missing input rewind video.")

    _logger.info(f"Started job on object: {parser.rewind}")

    tmp_video_path = "/tmp/input.mp4"

    # Download rewind.
    rm = RewindManager(parser.rewind)
    filename = rm.get_filename()
    cameraId = rm.get_camera_id()
    video_start_timestamp = rm.get_video_start_timestamp()
    rm.get_video(tmp_video_path)
    detection_fps = parser.fps

    # Configure inference model.
    # modelName = os.environ['MODEL_NAME']
    mm = ModelManager(os.environ["GRAPHS_DIR"])  # modelName)
    mm.get_model(
        os.environ["MODEL_STACK_ARCHIVE"],
        os.environ["GRAPHS_DIR"],
        overwrite=parser.downloadmodel,
    )

    # Loading the model takes a bit of time, if we don't need it don't do that thing.
    run_detections = str2bool(os.environ["RUN_DETECTIONS"])
    if not parser.tracksfile:
        # Initialize the tensorflow model detection_model.DetectionModel
        det_bot = None
        if run_detections:
            det_bot = DetectionModel()
        seg_bot = SegmentationModel(model_name=os.environ["SEGMENTATION_MODEL_NAME"])

    # Configure target frame size for detection model.
    target_frame_size = (1280, 720)
    if "DETECTION_SIZE_WIDTH" in os.environ and "DETECTION_SIZE_HEIGHT" in os.environ:
        target_frame_size = (
            int(os.environ["DETECTION_SIZE_WIDTH"]),
            int(os.environ["DETECTION_SIZE_HEIGHT"]),
        )

    if parser.tracksfile:
        # Skip inference.
        wavetracks = pickle.load(open(f"/usr/src/app/{filename}.wavetracks.pkl", "rb"))
        _logger.info(f"Using existing detections file.")
    else:
        if not run_detections:
            _logger.info("Not running object detection.")
        wavetracks = getWaveTracks(
            seg_bot,
            tmp_video_path,
            video_start_timestamp,
            detection_fps,
            target_frame_size,
            det_bot=det_bot,
            run_detections=run_detections,
        )

        if parser.dumptracks:
            wavetracks_dump_file_path = f"{filename}.wavetracks.pkl"
            with open(wavetracks_dump_file_path, "wb") as f:
                pickle.dump(wavetracks, f)
            _logger.info(f"Dumping wavetracks: {wavetracks_dump_file_path}")

    highlightClips = None
    if parser.highlights:
        highlightClips = makeHighlightsVideo(
            tmp_video_path,
            video_start_timestamp,
            wavetracks,
            target_frame_size=target_frame_size,
            max_duration=float(os.environ["MAX_DURATION"]),
            start_buffer=float(os.environ["START_BUFFER"]),
            end_buffer=float(os.environ["END_BUFFER"]),
        )

    # Save data.
    if parser.saveoutput:
        _logger.info("Saving output to Aurora and S3.")
        start = timer.start()

        # since these are concatenated, clips will always be lenth 1.
        clips = uploadClips(cameraId, highlightClips)[0]

        saveData(
            cameraId,
            filename,
            clips["objectKey"],
            clips["thumbObjectKey"],
            clips["gifObjectKey"],
            video_start_timestamp,
        )

        timer.checkpoint("Saving data and uploading files took:", start)
    else:
        _logger.info("Skipping save output.")


if __name__ == "__main__":
    main(sys.argv)
