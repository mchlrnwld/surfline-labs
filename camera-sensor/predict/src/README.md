# CV Inference Stack

Install the inference package with
` pip install --no-cache-dir -e .`

`src/` contains entrypoints for various products that depend on our computer vision stack, `sl_camera_sensor`.

## Smart Highlights 

App will download rewind from S3 run segmentation model + wave tracking to output smart highlights clips to S3.
This is set up to run inference on TensorRT models on either TitanV or G4 GPUs only, so running
locally on CPUs is not supported. Python3.7 is also a requirement.

# Development

You need two additional files to build the docker container: `tensorflow-2.3.0-cp37-cp37m-linux_x86_64.whl`, which is a Surfline-specific precompiled tensorflow binary that you can ask Ketron for. You also need `TensorRT-7.0.0.11.Ubuntu-18.04.x86_64-gnu.cuda-10.2.cudnn7.6.tar.gz` which you can also get from Ketron, or the Nvidia developer site.

```bash
# To build the container.
make build_gpu

# Will start container and enter
make enter_gpu
```

## JOB
Smart Highlights with wave tracking, which downloads video, extracts frames, runs inference + wave tracking, and uploads a still, gif, and mp4 clip.

The entrypoint is `entrypoint_highlights.py`.

### Smart Highlights

```bash
# Will execute using the test rewind in the src folder.
# --tracksfile true means don't run inference but load wavetracks from the wavetracks.pkl file in the src folder (to save time testing).
# --downloadmodel false means use the locally mounted model, if set to true the model will always be downloaded from S3 (`s3://sl-prototype-artifacts-dev/vision_stack/g4dn`).
# --saveoutput false prevents data being uploaded to S3 and Aurora.
# --highlights true will create a highlight video file containing the 1 sigma outlier waves from the source video

python3.7 /usr/src/app/entrypoint_highlights.py --rewind file://usr/src/app/58a37530c9d273fd4f581beb-20200221T185114298.mp4 --detectionsfile true --downloadmodel false --saveoutput false --highlights true --fps 1
```

# .ENV and Potential Algo Bugs 
At the moment, desirable wave events are extracted as 1-sigma `y-coord` outliers, and returned sequentially in time. These waves are typically `surfed`, but to add that dependency explicitly, you can set `RUN_DETECTIONS=true` in the `.env`, which will also run inference on the object detection network. On G4 instances the wavetracking stack takes about 1m40s to process (at 1fps) 10m of video - incorporating object detection will increase compute time by ~50%ish. If we decide we want surfed waves to take precedence, it will be easy to add.

Other `env` params that are specific to the output clips are `MAX_DURATION`, which is a hard limit in seconds of the output clip; `START_BUFFER` and `END_BUFFER`, which define the temporal padding of the individual wavetracks. Currently the algorithm ignores times when either `WhiteWater` or `Wave` are at 100%, so a `START_BUFFER` of ~1 will pad the clips so users can see just before the waves break.


### Generating a TRT Model
Pre-compiled TRT graphs are at `s3://sl-prototype-artifacts-dev/vision_stack/g4dn`. It's not straightforward to export our segmentation graph to TRT engines, so for GPUs other than G4's or TitanV's, ask Ketron.

# Deployment

Push the docker image to ECR with 
```bash

export AWS_PROFILE=surfline-dev
make deploy_gpu

```

I'm then triggering the job to run that ECR container on AWS batch with the lambda function at https://github.com/Surfline/prototype-ml-cam/blob/master/prototype-ml-video-pipeline-job-trigger/src/lambda_function.py

# Viewing Database

The worker puts data into an RDS Aurora DB. To view use phpMyAdmin. You will need to be connected to the `surfline-dev` VPC.

```bash
docker run -d -e PMA_HOST=prototype-ml-cam-local.cluster-cvpipfqer4dd.us-west-1.rds.amazonaws.com -p 8000:80 phpmyadmin/phpmyadmin
```

# Random Notes

Inclination angles will effect the lower-limit fps that we can run. The higher the inclination angle, the larger fps we need. For most cams,
1-1.5fps should be fine.

I am confident this model + algo will work on any "decent" camera.