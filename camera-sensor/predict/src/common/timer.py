import time
from common import logger

log = logger.getLogger()


def start():
    return time.time()


def checkpoint(name, start):
    end = time.time()
    log.info("%s %2.2fs (%3.2fms)" % (name, (end - start), (end - start) * 1000))
