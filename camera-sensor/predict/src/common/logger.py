import sys
import logging


def getLogger():
    logging.basicConfig(stream=sys.stdout, level=logging.INFO)
    return logging.getLogger()
