# -*- coding: utf-8 -*-
import os
import subprocess
import datetime
import time
import math
import pandas as pd
import numpy as np
from common import logger, timer
from PIL import Image

logger = logger.getLogger()
GPU_ENABLED = False

if "GPU" in os.environ and os.environ["GPU"].upper() == "TRUE":
    GPU_ENABLED = True

logger.info(f"GPU enabled ffmpeg: {GPU_ENABLED}")


class HighlightsVideo:
    def __init__(
        self,
        video_path,
        video_start_timestamp,
        tracks,
        target_frame_size,
        max_duration,
        start_buffer,
        end_buffer,
    ):
        """
      video_path - source video file.
      video_start_timestamp - UNIX timestamp for the start of this file.
      tracks - tracks list
      clips_to_make - how many clips to make per source file
    """
        self.__video_path = video_path
        self.__tracks_df = tracks
        self.__video_start_timestamp = video_start_timestamp
        self.__target_frame_size = target_frame_size
        self.__max_duration = max_duration
        self.__start_buffer = start_buffer
        self.__end_buffer = end_buffer

    def __datetime_to_timestamp(self, dt):
        """
      Returns datetime object as epoch in seconds with millisecond precision as floating point value i.e. 1579273272.823
      https://stackoverflow.com/questions/8160246/datetime-to-unix-timestamp-with-millisecond-precision
    """
        return (time.mktime(dt.timetuple()) * 1e3 + dt.microsecond / 1e3) / 1000.0

    def generateClips(self, encode_video=True):
        # ==== Condense the list of tracks into clip ranges.
        # i.e. take overlappings tracks and just make one clip vs clip per track.
        wave_timestamps = self.__get_timestamps_from_tracks(self.__tracks_df)

        # Convert that np array to a df
        df = pd.DataFrame.from_dict(
            {"start": wave_timestamps[:, 0], "end": wave_timestamps[:, 1]}
        )

        tracks_count = len(df)

        if tracks_count == 0:
            return self.__tracks_df.to_dict("records")

        # Offset to get seconds relative to start time of vid
        df -= self.__video_start_timestamp

        # Recalculate length so it matches the new clip length (previously length with the track length).
        df["length"] = df["end"] - df["start"]

        clips_count = len(df)
        logger.info(f"Generating {clips_count} clips")

        start = timer.start()
        t0 = time.time()
        # === Format clip ranges for ffmpeg
        clip_paths = []
        thumb_paths = []
        for index, row in df.iterrows():
            start, end = row[["start", "end"]]
            clip_path = f"/usr/src/app/clip-{index}.mp4"
            clip_paths.append(clip_path)
            thumb_path = f"/usr/src/app/clip-{index}.jpg"
            thumb_paths.append(thumb_path)

            if encode_video:
                self.__trim_clip(clip_path, start, end)

                # Frame we want the thumbnail for.
                (end - start) / 2

                # Create thumbnail.
                # self.__create_thumbnail(clip_path, thumb_path, thumb_offset)

        timer.checkpoint(f"Generating clips took: ", start)
        logger.info(f"Generating clips took: {time.time()-t0:.1f}")
        df["clip_path"] = clip_paths
        df["thumb_path"] = thumb_paths

        # Concatenate all these clips into a single mp4, gif, and jpg.
        df = self.__concat_clips(df)
        return self.__df_to_list(df)

    def __df_to_list(self, df):
        return df.to_dict("records")

    def __concat_clips(self, df):
        """ Concatenate a list of clips into a single one, and modify the incoming df
    to reflect that change
    """
        cmd = ["ffmpeg"]
        if GPU_ENABLED:
            # Tells ffmpeg to use CPU decoding.
            cmd = cmd + ["-hwaccel", "cuda", "-c:v", "h264_cuvid"]

        clips = "|".join(df["clip_path"].values)
        output_path = os.path.join(
            os.path.dirname(df["clip_path"].values[0]), "clips-full.mp4"
        )
        cmd = cmd + ["-y", "-i", f"concat:{clips}", "-codec", "copy", output_path]

        ok_msg = f"Concatenating {len(df)} clips to {output_path}"
        err_msg = "Could not concatenate clips"
        self.__run_subprocess(cmd, ok_msg, err_msg)

        # Now concatenate the df, make a new thumbnail, and a gif.
        # thumbnail - first wave is always the largest?
        thumb_offset = df["start"].iloc[0]  # + os.environ['START_BUFFER']
        thumb_path = output_path.replace(".mp4", ".jpg")
        self.__create_thumbnail(df["clip_path"].iloc[0], thumb_path, thumb_offset)
        # jif
        gif_path = output_path.replace(".mp4", ".gif")
        self.__create_gif(df["clip_path"].iloc[0], gif_path)
        # df
        catdf = pd.DataFrame.from_dict(
            {
                "length": [df["length"].sum()],
                "clip_path": [output_path],
                "thumb_path": [thumb_path],
                "gif_path": [gif_path],
                "start": [df["start"].iloc[0]],
                "end": [df["end"].iloc[-1]],
            }
        )
        return catdf

    def __create_gif(self, input_path, output_path, duration_seconds=4):
        cmd = ["ffmpeg"]
        if GPU_ENABLED:
            # Tells ffmpeg to use CPU decoding.
            cmd = cmd + ["-hwaccel", "cuda", "-c:v", "h264_cuvid"]
        cmd = cmd + [
            "-y",
            "-ss",
            "0",
            "-t",
            str(duration_seconds),
            "-i",
            input_path,
            "-vf",
            "fps=10,scale=200:-1:flags=lanczos,split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse",
            "-loop",
            "0",
            output_path,
        ]

        ok_msg = f"Making {duration_seconds}s gif from full clip"
        err_msg = "Could not generate gif"
        self.__run_subprocess(cmd, ok_msg, err_msg)

    def __create_thumbnail(self, input_path, output_path, thumb_offset):
        cmd = ["ffmpeg"]

        if GPU_ENABLED:
            # Tells ffmpeg to use CPU decoding.
            cmd = cmd + ["-hwaccel", "cuda", "-c:v", "h264_cuvid"]

        cmd = cmd + [
            "-i",
            input_path,
            "-ss",
            "0",
            "-to",
            str(thumb_offset),
            "-vframes",
            "1",
            "-y",
            output_path,
        ]

        ok_msg = f"Creating thumbnail at {output_path} from {input_path}, frame at: {thumb_offset}."
        err_msg = "Could not create thumbnail from {input_path}"
        self.__run_subprocess(cmd, ok_msg, err_msg)

        # Open the image, draw the track on it and save it.
        im = Image.open(output_path)

        # Scale image down.
        # PIL.Image.LANCZOS = a high-quality downsampling filter.
        im = im.resize((200, 113), resample=Image.LANCZOS)
        im.save(output_path, "JPEG")

    def __trim_clip(self, output, start, end=None):
        """
        Given a MP4 video, an output path, a start time,
        and an optional end time; trim the video.
    """

        # ffmpeg command explanation
        """
      Seeking: http://trac.ffmpeg.org/wiki/Seeking
      Note we can't put -ss before -i for faster seek before trimming 
        because we'll miss keyframes in the video and
      won't produce a usable clip at the end.

      -vf format=yuv420p I belive this is 8bit H.264 video.
      -movflags +faststart moves the moov atom to the start of the file 
        which means we seek in the video without downloading/opening the whole video.
    """
        cmd = ["ffmpeg"]

        # Use fast and accurate seek if possible. This will skip through the file using keyframes
        # Basically pass -ss twice before -i and after. Before uses keyframes within the file to
        # jump through the video -ss after then goes frame by frame to find the frame to cut
        # I did run into an issue using the same start time for both the fast and accurate resulted
        # in empty frames at the start or incomplete videos I assume it had trouble with key frames
        # so we'll only use the fast seek if we're beyond 60s (arbitary) of video which is where
        # the main benefit of fast seeking occurs anyway.
        fast_seek_ss = start - 60
        if fast_seek_ss > 0:
            cmd = cmd + ["-ss", str(fast_seek_ss)]

        if GPU_ENABLED:
            # Tells ffmpeg to use CPU decoding.
            cmd = cmd + ["-hwaccel", "cuda", "-c:v", "h264_cuvid"]

        # -copyts is linked to the use of -ss before -i described above. Doesn't matter if -ss is used.
        cmd = cmd + [
            "-i",
            self.__video_path,
            "-copyts",
            "-ss",
            str(start),
            "-to",
            str(end),
            "-vf",
            "format=yuv420p",
            "-preset",
            "fast",
            "-movflags",
            "+faststart",
            "-y",
            output,
        ]

        logger.debug(
            "Trimming {0} from {1} to {2} with command {3}".format(
                self.__video_path, start, end, " ".join(cmd)
            )
        )
        ok_msg = "Clip saved at {}.".format(output)
        err_msg = "Could not trim clip {}".format(output)
        self.__run_subprocess(cmd, ok_msg, err_msg)

    def __calculate_trim_time(self, epoch_start, epoch_end):
        """
        Given a video str epoch time, and two epoch time
        intervals, returns a human (ffmpeg) readable start
        and end trim time.
    """
        # Find where to trim within the video.
        clip_start_trim = abs(self.__video_start_timestamp - epoch_start)
        clip_end_trim = abs(epoch_end - self.__video_start_timestamp)

        trim_start = math.floor(clip_start_trim)
        trim_end = math.ceil(clip_end_trim)

        return trim_start, trim_end

    def __add_time_buffer(self, row, start_buffer_seconds=5, end_buffer_seconds=0):
        row["start"] = row["start"] - datetime.timedelta(seconds=start_buffer_seconds)

        # If start timestamp is now less than the start of the file change it to be the start of the file
        # as we have no frames for this timestamp.
        if time.mktime(row["start"].timetuple()) < self.__video_start_timestamp:
            row["start"] = datetime.datetime.fromtimestamp(self.__video_start_timestamp)

        row["end"] = row["end"] + datetime.timedelta(seconds=end_buffer_seconds)

        return row

    def __run_subprocess(self, cmd, ok_msg=None, err_msg=None):
        """
        Given a list of subprocess commands and flags,
        a success message and an error message, run a
        subprocess thread for that cmd.
    """
        ok_msg = ok_msg or "Command {}: success.".format(" ".join(cmd))
        err_msg = err_msg or "Command {} unsuccess.".format(" ".join(cmd))

        try:
            logger.info("Running subprocess: {0}".format(" ".join(cmd)))
            with open(os.devnull) as devnull:
                subprocess.check_output(cmd, stdin=devnull)
            logger.debug(ok_msg)
        except Exception as e:
            logger.error("{0}: {1}".format(err_msg, e))

    def __get_timestamps_from_tracks(self, tracks):
        """ Find the best wave times via sigma clipping, and return
    those, with overlaps merged properly.
    """
        # Sometimes there will be zero waves detected.
        if len(tracks) <= 1:
            if len(tracks) == 0:
                # Just return a 15 second clip of the lake-like conditions?
                return np.asarray([0, 15])
            else:
                t0 = track["timestamp"].min() - self.__start_buffer
                t1 = track["timestamp"].max() + self.__end_buffer
                return np.asarray([t0, t1])

        # If we're here, we have more than 2 tracks.
        y, timestamps = [], []
        for track_num, track in tracks.groupby("track_num"):
            # Ignore the sometime long period prior to breaking.
            # Adding the wwpct constraint give many more smaller clips
            track = track[
                (track["wpct"] < 1) & (track["wwpct"] < 1)
            ]  # This should also remove boat wake.
            # Accumulate only the start & stop times, and starting position.
            # Pad these, since they will clip pretty tightly.
            try:
                timestamps.append(
                    [
                        track.timestamp.iloc[0] - self.__start_buffer,
                        track.timestamp.iloc[-1] + self.__end_buffer,
                    ]
                )
                y.append(track.y.iloc[0])
            except IndexError:
                # wpct and wwpct are all 1
                pass
        y, timestamps = np.asarray(y), np.asarray(timestamps)
        outliers = np.where(y < (y.mean() - y.std()))[0]
        highlight_times = timestamps[outliers]
        logger.info(f"Found {len(outliers)}/{len(y)} interesting segments")
        # Now we need to clip these further so the max_duration isnt' too long.
        # Question is do we do that before or after overlap concatenation?
        # For now just do it sequentially.
        dts = highlight_times[:, 1] - highlight_times[:, 0]
        # Immediately remove any clips > self.__max_duration
        highlight_times = highlight_times[np.where(dts < self.__max_duration)]
        # Accumulate until total clip will be < max_duration
        upto = 0
        for upto, tot_time in enumerate(
            np.cumsum(highlight_times[:, 1] - highlight_times[:, 0])
        ):
            if tot_time > self.__max_duration:
                break

        if upto == 0:
            logger.info(
                f"First wave is > max_duration ({self.__max_duration}s)."
                " Clip will exceed duration."
            )
            upto = 1

        logger.info(
            f"Keeping {upto}/{len(outliers)} clips in order to "
            f"satisfy {self.__max_duration}s max_duration"
        )
        highlight_times = highlight_times[:upto]

        # Now merge any overlaps
        if upto > 1:
            highlight_times = np.asarray(
                self.__merge_overlaps(highlight_times.tolist())
            )
        # print(highlight_times)
        dts = highlight_times[:, 1] - highlight_times[:, 0]
        logger.info(f"Average clip time is {dts.mean():.1f}s, {dts.sum():.1f}s total")
        return highlight_times

    def __merge_overlaps(self, times, idx=0):
        for i in range(idx, len(times) - 1):
            if times[i][1] >= times[i + 1][0]:
                new_start = times[i][0]
                new_end = times[i + 1][1]
                times[i] = [new_start, new_end]
                del times[i + 1]
                return self.__merge_overlaps(times.copy(), idx=i)
        return times
