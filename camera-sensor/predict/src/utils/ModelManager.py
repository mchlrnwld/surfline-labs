# -*- coding: utf-8 -*-
import os
import boto3
from urllib.parse import urlparse
from common import logger

log = logger.getLogger()


class ModelManager:
    def __init__(self, graphs_dir):
        self._graphs_dir = graphs_dir

    def get_model(self, model_location, destination, overwrite=True):
        if overwrite or os.path.isdir(destination) is False:
            log.info(f"Downloading model from: {model_location}")

            parts = urlparse(model_location)
            s3 = boto3.resource("s3")
            # Download the entire directory.
            bucket = s3.Bucket(parts.netloc)
            for obj in bucket.objects.filter(Prefix=parts.path[1:]):
                output_path = os.path.join(destination, os.path.dirname(obj.key))
                if not os.path.isdir(output_path):
                    os.makedirs(output_path)
                    log.info(f"made model directory: {output_path}")
                bucket.download_file(
                    obj.key, os.path.join(output_path, os.path.basename(obj.key))
                )
                log.info(f"downloading {os.path.join(output_path, obj.key)}")

            if os.path.isdir(destination) is False:
                raise Exception(f"Unable to extract model to {destination}")
            else:
                log.info(f"Extracted models to: {destination}")
        else:
            log.info(f"Model exists already at: {destination}")
