# -*- coding: utf-8 -*-
"""
  Class makes it easier to work with rewind files.
"""
import os
import re
import datetime
import boto3
from urllib.parse import urlparse
from urllib.request import urlretrieve
from shutil import copyfile
from common import logger

log = logger.getLogger()


class RewindManager:
    def __init__(self, video_path):
        self._video_path = video_path
        self._filename = self._get_file_name_from_path(self._video_path)

        self._camera_id = self._get_camera_id_from_filename(self._filename)
        self._video_start_timestamp = self._get_datetime_from_file_name(self._filename)

    """
  === PUBLIC INTERFACE ===
  """

    def get_filename(self):
        return self._filename

    def get_camera_id(self):
        return self._camera_id

    def get_video_start_timestamp(self):
        return self._video_start_timestamp

    def get_video(self, destination):
        parts = urlparse(self._video_path)

        if parts.scheme == "file":
            self._copy_file(f"/{parts.netloc}{parts.path}", destination)
        elif parts.scheme == "s3":
            self._s3_download(parts.netloc, parts.path[1:], destination)
        elif parts.scheme == "http" or parts.scheme == "https":
            self._download_file(self._video_path, destination)
        else:
            raise Exception(f"Protocol not supported for: {self._video_path}")

    """
  === PRIVATE INTERFACE ===
  """

    def _get_file_name_from_path(self, path):
        return os.path.basename(os.path.splitext(path)[0])

    def _get_camera_id_from_filename(self, key):
        stream_name_results = re.search("([A-Za-z0-9]+)", key)

        if stream_name_results:
            return stream_name_results.group(1)
        else:
            raise Exception("Unable to parse stream name from filename.")

    def _timestamp_to_epoch(self, timestamp, timestamp_format="%Y%m%dT%H%M%S%f"):
        """
        Return the linux epoch time to the given
        timestamp format (e.g. 20180227T201716).
    """
        dt = datetime.datetime.strptime(timestamp, timestamp_format)
        return dt.timestamp()

    def _get_datetime_from_file_name(self, key):
        key_search = re.search("-([A-Za-z0-9]+)", key)

        if key_search:
            date_string = key_search.group(1)
            return self._timestamp_to_epoch(date_string)
        else:
            raise Exception("Unable to parse time from filename.")

    def _download_file(self, url, dest):
        urlretrieve(url, dest)

    def _s3_download(self, bucket, object_key, destination):
        log.info(f"Download file s3://{bucket}/{object_key} to {destination}")
        s3 = boto3.resource("s3")
        s3.Bucket(bucket).download_file(object_key, destination)
        log.info("Download succeeded.")

    def _copy_file(self, source, destination):
        log.info(f"Using local file copying from {source} to {destination}")
        copyfile(source, destination)
