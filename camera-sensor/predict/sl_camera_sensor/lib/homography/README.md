# Statistical Monocular Homography Fitting

## Approach
We need to accumulate filtered bounding box data at consistent camera states for varying
tides. [Wrongly] assuming the cam state is constant over long periods, the approach
is to build a dataset of filtered bounding box data as a function of tide. We can then
bin those data by tide and test how granular we need to be in terms of tide - ie can we
group by 2 hour windows? I think it's generally not that simple as the tide delta
is stronger mid-tide. Anyway with that database we can test this binning.

##