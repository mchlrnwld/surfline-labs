# This loads an {alias}-tidal-homography.json file and
# finds the nearest homography coefficients for a given tide,
# via interpolation
#
import os
import json
import bisect
import cv2
import numpy as np
from scipy.interpolate import interp1d
from dataclasses import dataclass, field
from sl_camera_sensor.utils import logger
from PIL import Image

log = logger.getLogger(__name__)


import matplotlib.pyplot as plt

@dataclass
class Homography:
    """ 
    Loads, interpolates and finds closes homography coefficients
    for a camera ID or alias at a given tide. One obj per cam.
    """
    alias_or_camID: str
    target_size: tuple = None
    _alias_or_camID: str = field(init=False, repr=False, default=None)
    classes: list = ('raw', 'clip', 'ransac')
    ft_scale: float = 1.33
    has_homograpy: bool = False
    
    def get(self, x, y, target_size=None):
        """
        Get the foot conversion at pixel x, y. For face-on 
        cams it won't matter what x is. 
        
        Args:
            - x, y are pixel values. 
            - target_size: (height, width) tuple. This part might get confusing...
        """
        if not self.has_homography:
            return np.nan
        
        return self.grid[y, x]
         
    @property
    def tide(self):
        return self._tide

    @tide.setter
    def tide(self, tide, target_size=None, fit_type='raw'):
        """ 
        Set the homography coefficients & grid for the nearest input tide 
        via interpolation.

        Args:
          - param tide: float tide in feet you want conversions for
          - fit_type: fitting flavor - 'raw', 'clip', or ransac. 
            Currently raw fits are the best.
        
        Sets: array of 2 coefficients - slope and intercept.
        """
        if self.alias_or_camID is None:
            raise ValueError(
                'You need to set an alias or camID before you can set the '
                'homography tide.'
            )
        if not self.has_homography:
            self._tide = np.nan
            log.info(
                'You are trying to set a tide on a homography '
                'instance that has no homography.'
            )
            return
        
        self.target_size = target_size or self.target_size or self.frame_size
        self.fit_type = fit_type
        if tide < self.tides.min()-10 or tide > self.tides.max()+10:
            log.info(
                f'Requested tide ({tide}) is badly outside of range of fits: '
                f'({self.tides.min()} - {self.tides.max()} ft)'
            )
        self.coefs_ = np.hstack(
            [
                self.coefs[fit_type][i](tide)
                for i in range(len(self.coefs[fit_type]))
            ]
        )
        # Make a grid in the projected basis
        sy, sx = self.frame_size
        y, x = np.meshgrid(np.arange(0, sy), np.arange(0, sx))
        ft_grid_proj = self.ft_scale / (self.coefs_[0] * y + self.coefs_[1])
        #ft_grid_proj = np.flipud(np.moveaxis(ft_grid_proj, 0, -1))
        ft_grid_proj = np.moveaxis(ft_grid_proj, 0, -1)
        assert ft_grid_proj.shape == self.frame_size
        #ft_grid_proj[np.where(ft_grid_proj < 0)] = 0
        
        ft_grid = cv2.warpPerspective(
            ft_grid_proj, self.M, self.frame_size[::-1], flags=cv2.WARP_INVERSE_MAP
        )
        if self.frame_size != self.target_size:
            self._native_ft_grid = ft_grid.copy()
            self.ft_grid = np.array(
                Image.fromarray(ft_grid).resize(self.target_size[::-1], Image.BILINEAR)
            )
            #raise NotImplementedError(
            #    'NEED TO RESIZE GRID *AFTER* DEPROJECTION IF TARGET!=FRAME'
            #)
        
        self.grid = ft_grid.copy()
        self.grid_proj = ft_grid_proj.copy()
        self.__make_wireframe(ft_grid_proj)
        return
        """
        # MOVE TO PLOTS CLASS
        rm = np.where(y.T<450)
        # Really just need to remove the horizon here.
        #rm = np.where(ft_grid_proj <= 0)
        ft_grid_proj[rm] = np.nan
        f, (ax1, ax2) = plt.subplots(2, 1)
        im = ax1.imshow(ft_grid_proj)
        plt.gca().invert_yaxis()
        plt.colorbar(im)
        plt.tight_layout()
        
        #plt.subplots(122)
        ft_grid[rm] = np.nan
        im = ax2.imshow(ft_grid)
        #plt.gca().invert_yaxis()
        plt.tight_layout()
        plt.savefig(os.path.expanduser('~') + '/tgrids.jpg')
        #set_trace()
        """
    
    def __make_wireframe(self, grid_proj, ft_per_line=5, thickness=4):
        """ 
        Input grid should be in the projected (y-normal) basis.

        sets self.wireframe and self.wireframe_proj
        """
        #grid_proj /= 1#2000
        #log.info('descaling')
        sy, sx = self.frame_size
        rows = grid_proj[:, 0][::-1]
        rows = grid_proj[:, 0][::-1]
        csum = np.cumsum(rows / ft_per_line).astype(int)
        yg_idxs = []
        y = np.arange(0, sy)[::-1]
        for unq in np.unique(csum):
            idx = np.where(csum == unq)[0][0]
            if rows[idx] > 0:
                yg_idxs.append(y[idx])
        yg_idxs = np.asarray(yg_idxs)
        wframe = np.zeros((sy, sx))
        #wframe[yg_idxs-thickness:yg_idxs+thickness, :sx] = 0
        for yg in yg_idxs:
            for t in range(thickness):
                wframe[yg-t, :sx] = 255
        wireframe = cv2.warpPerspective(
            wframe, self.M, (sx, sy), flags=cv2.WARP_INVERSE_MAP
        )[..., np.newaxis]
        wireframe_proj = wframe[..., np.newaxis]
        # 2d -> "RGB"
        #wireframe_proj[wireframe_proj != 1] = 255
        #wireframe[wireframe != 1] = 255
        self.wireframe = np.repeat(wireframe, 3, axis=-1).astype(np.uint8)
        self.wireframe_proj = np.repeat(wireframe_proj, 3, axis=-1).astype(np.uint8)
        
    @property
    def alias_or_camID(self):
        return self._alias

    @alias_or_camID.setter
    def alias_or_camID(self, inp):
        if inp is None:
            self._alias = inp
            return
        if '-' in inp:
            self._alias = inp
            self._camID = None
        else:
            raise NotImplementedError('labs-utils to lookup alias from ID?')
            # TODO this is actually backwards. I should be saving hom files by camID. 
        cwd = os.path.dirname(os.path.realpath(__file__))
        hom_fname = f'{inp}-tidal-homography.json'
        hom_file = os.path.join(cwd, hom_fname)
        if not os.path.isfile(hom_file):
            self.has_homography = False
            log.info(f'No homography json found for {inp}')
            return
        with open(hom_file, 'r') as f:
            coef_dict = json.load(f)
        if 'projection_matrix' not in coef_dict or 'frame_size' not in coef_dict:
            raise ValueError(
                'Homography solutions now must have '
                '"projection_matrix" and "frame_size" keys.'
            )
        self.M = np.asarray(coef_dict.pop('projection_matrix'))
        self.frame_size = tuple(coef_dict.pop('frame_size'))
        if len(coef_dict) < 2:
            raise ValueError("The fitted homography has only one tide but needs at least two.")
        self.coef_dict = self._interpolate(coef_dict)
        log.info(f'Loaded homography for {inp}')
        self.has_homography = True
        
    def _interpolate(self, coef_dict):
        coefs = {k: [] for k in self.classes}
        for tide, vals in coef_dict.items():
            icoefs = vals['coefs']
            for k in self.classes:
                if k in coef_dict[tide]['coefs']:
                    coefs[k].append([float(tide)] + coef_dict[tide]['coefs'][k])
        coefs = {k: np.vstack(v) for k, v in coefs.items() if len(v) > 0}
        t0, t1 = coefs['raw'][:, 0][[0, -1]]
        self.tides = np.linspace(t0, t1, round((t1-t0)/0.1))
        self.coefs = {}
        for k, v in coefs.items():
            a = interp1d(v[:, 0], v[:, 1], fill_value='extrapolate')  # (self.tides)
            b = interp1d(v[:, 0], v[:, 2], fill_value='extrapolate')  # (self.tides)
            self.coefs[k] = (a, b)  # np.vstack((a, b)).T            
