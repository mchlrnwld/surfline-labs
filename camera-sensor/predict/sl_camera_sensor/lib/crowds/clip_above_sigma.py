# Lower sigma clipping to omit obscured frames.
# You should have done a lower threshold clip prior
# to this.
#
import numpy as np


def clip_above_sigma(vals):
    """ 
        One sigma lower threshold clipping. Tries to account for and remove
        counts from occulted frames. This should be called separately
        for water and beach people.
        :param vals: raw counts above some confidence threshold (0.25)
        with no less than 5 samples. So if inference is one frame per minute,
        accumulate thresholded counts for 5 - 20 minutes and pass them here
        to get a better count statistic for that time period.
        :returns: averaged crowd counts over time period of 'vals'. 
        """
    vals = np.asarray(vals)
    return (
        0 if np.all(vals == 0) else np.mean(vals[vals > (np.mean(vals) - np.std(vals))])
    )
