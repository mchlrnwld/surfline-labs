# Object to store a SurferSurfing object used in surfer_tracking.
# We're querying some of these methods in the SurferTracking class
# eg when we decide whether or not to stitch to existing tracks.
#
import numpy as np
from collections import defaultdict
from typing import Optional
from sl_camera_sensor.utils.logger import getLogger

log = getLogger(__name__)


class Surfer:
    def __init__(
        self,
        wave_num: int,
        locs: tuple,
        timestamp,
        wave_info: Optional[dict] = None,
        check: Optional = False,
        ID: Optional[int] = None,
        **kwargs,
    ) -> None:
        self.timestamps = [timestamp]
        self.locs = np.asarray(locs)[np.newaxis, :]
        self.loc = locs
        self.timestamp = timestamp
        self.hits = 1
        self.ID = ID
        self._dt = None
        self._dt_timestamp = 0
        self._x = None
        self._y = None
        self._x_timestamp = 0
        self._y_timestamp = 0
        self.dx = 0
        self.dy = 0  # total displacements
        self.check = check  # Do we need to check this on next iter (SurferTracker)?
        # Store the percents of each mask attribute contained within the
        # surfer bbox.
        self.wave_infos = [wave_info]
        self.wave_num = wave_num
        self.update_state_every = 3  # Update the surfing state every 3 seconds
        for k, v in kwargs.items():
            setattr(k, v)
        # Add fps to base class??
        # fps = self.fps if hasattr(self, 'fps') else None
        # super().__init__(fps)

    def get_track(
        self,
        target_size=None,
        return_df: Optional[bool] = None,
        with_crop: Optional[bool] = False,
    ):
        """ Return (x,y) tuples and timestamps. 
        :param target_size: target frame size as height, width,
        otherwise this returns normalized coords.
        :param 
        """
        if self.hits < 2:
            return None
        if target_size is not None:
            x = Surfer.norm_coords(self.x.copy(), target_size[1])
            y = Surfer.norm_coords(self.y.copy(), target_size[0]) + 5
        else:
            x, y = self.x.copy(), self.y.copy()
        try:
            assert len(x) == len(y) == len(self.timestamps) == len(self.wave_infos)
        except AssertionError:
            log.critical(
                f"Issue with track {self.ID} on wave {self.wave_num}: "
                "incorrectly shaped wave_info | track | timestamps. "
                "This is a bug and your request is rejected."
            )
            return
        track = {
            "startTimestamp": self.timestamps[0],
            "endTimestamp": self.timestamps[-1],
            "timestamps": self.timestamps,
            "direction": self.get_avg_direction_string(),
            "track": np.dstack((x, y)).squeeze(),
            "ID": self.ID,
            "wave_num": self.wave_num
            #'wave_info': self.__unpack_wave_info()
        }  # add arclen?
        # Don't save nested dicts - unpack the wave info so each key in there
        # is a key in the track dict.
        wave_info = self.__unpack_wave_info()
        for k, v in wave_info.items():
            key = (
                "pct" + k
                if k not in ["crop", "dLip", "dTrough", "blob_bbox_ratio", "dPocket"]
                else k
            )
            if key == "crop":
                if with_crop:
                    track[key] = v
            else:
                track[key] = v
        if return_df:
            # Return a dataframe instead of a json/dict
            track = self.__track_to_df(track)
        return track

    def __track_to_df(self, track):
        # Remove single element vals
        del track["startTimestamp"]
        del track["endTimestamp"]
        del track["direction"]
        del track["crop"]
        x = track["track"][:, 0]
        y = track["track"][:, 1]
        track["x"] = x
        track["y"] = y
        try:
            df = pd.DataFrame(track)
        except:
            log.critical("can't convert track to dataframe")
            df = None
        return df

    def __unpack_wave_info(self):
        """ Wave info is a list of dicts, convert that to a dict of lists,
        remove any key where all the values are zero, and repalce
        Nones with -1 values in the remaining lists.
        """
        unpacked = defaultdict(list)
        for wdict in self.wave_infos:
            if wdict is None:
                for k, v in unpacked.items():
                    unpacked[k].append(-1)
            else:
                for k, v in wdict.items():
                    unpacked[k].append(v)
        del_keys = []
        for k, v in unpacked.items():
            if k != "crop" and np.all(np.asarray(v) == 0):
                del_keys.append(k)
        for k in del_keys:
            del unpacked[k]
        return dict(unpacked)

    def __repr__(self):
        return (
            (
                f"Surfer(loc:({self.x[-1]:.4f},{self.y[-1]:.4f}), hits:{self.hits}, "
                f"inst_vel:{self.get_inst_velocity()}, "
                f"dir:{self.get_avg_direction_string()}, "
                f"ang:{self.get_avg_direction()}), "
                f"wave_num:{self.wave_num}"
            )
            if self.hits > 1
            else f"Surfer(loc:{self.x[-1]:.4f},{self.y[-1]:.4f}, hits:{self.hits})"
        )

    def __len__(self):
        return self.hits

    @property
    def dt(self):
        return self._dt

    @dt.getter
    def dt(self):
        if self._dt is None or self._dt_timestamp != self.timestamp:
            self._dt = np.diff(self.timestamps)
        return self._dt

    @property
    def x(self):
        return self._x

    @x.getter
    def x(self):
        # if self._x is None or self._x_timestamp != self.timestamp:
        self._x = np.mean(self.locs[:, [1, 3]], axis=1).astype(np.float16)
        # self._x_timestamp = self.timestamp
        return self._x

    @property
    def y(self):
        return self._y

    @y.getter
    def y(self):
        # if self._y is None or self._y_timestamp != self.timestamp:
        self._y = np.mean(self.locs[:, [0, 2]], axis=1).astype(np.float16)
        # self._y_timestamp = self.timestamp
        return self._y

    def get_avg_speed(self):
        return np.sqrt(
            (np.diff(self.x) / self.dt) ** 2 + (np.diff(self.y) / self.dt) ** 2
        ).mean()

    def get_inst_speed(self):
        return np.sqrt(
            (np.diff(self.x[-2:]) / self.dt[-1]) ** 2
            + (np.diff(self.y[-2:]) / self.dt[-1]) ** 2
        ).mean()

    def get_avg_velocity(self):
        vy = np.float16((np.diff(self.y) / self.dt).mean())
        vx = np.float16((np.diff(self.x) / self.dt).mean())
        return (vy, vx)

    def get_inst_velocity(self):
        vy = np.float16((np.diff(self.y[-2:]) / self.dt).mean())
        vx = np.float16((np.diff(self.x[-2:]) / self.dt).mean())
        return (vy, vx)

    def get_inst_direction(self):
        vx, vy = self.get_inst_velocity()
        return np.rad2deg(np.arctan2(vy, vx))

    def get_avg_direction(self):
        vx, vy = self.get_avg_velocity()
        return np.rad2deg(np.arctan2(vy, vx))

    def get_avg_direction_string(self):
        vx, vy = self.get_avg_velocity()
        return "left" if vx > 0 else "right"

    def get_dy(self):
        return np.abs(self.loc[0] - self.locs[0][0])

    def get_dx(self):
        return np.abs(self.loc[1] - self.locs[0][1])

    @staticmethod
    def get_bbox_avg(locs):
        """ y0, x0, y1, x1 -> meany, meanx """
        mean_y = np.mean(locs[:, [0, 2]]).astype(np.float16)
        mean_x = np.mean(locs[:, [1, 3]]).astype(np.float16)
        return np.asarray([mean_y, mean_x])[np.newaxis, :]

    def extend(self, locs, timestamp, wave_info=None):
        """
        Add a hit to this surfer & extend it's track. 
        wave_pct are the percentages of each mask class inside 
        the bbox for this surfer at this timestamp. May also
        include the distance to peak and trough etc. Be amenable to 
        additions there as we expand with new features.
        """
        self.hits += locs.shape[0] if locs.ndim > 1 else 1
        if locs.ndim < 2:
            locs = locs[np.newaxis, :]
        self.locs = np.vstack((self.locs, locs))
        self.timestamps = np.hstack((self.timestamps, timestamp))
        # "dstack" single dict or list of dicts - can update multiple locs at once.
        if isinstance(wave_info, dict) or wave_info is None:
            self.wave_infos.append(wave_info)
        elif isinstance(wave_info, list):
            self.wave_infos += wave_info
        else:
            raise ValueError(f"wave_info type not understood: {type(wave_info)}")
        # Update the latest stuff
        self.loc = self.locs[-1]
        self.timestamp = self.timestamps[-1]
        try:
            assert self.hits == len(self.locs)
        except AssertionError:
            raise ValueError(f"Hits error in Surfer {self.ID} on wave {self.wave_num}")

    @staticmethod
    def norm_coords(vec, size):
        if not isinstance(vec, np.ndarray):
            vec = np.asarray(vec)
        vec *= size
        vec[vec < 0] = 0
        vec[vec >= size] = size - 1
        return vec.astype(np.int)
