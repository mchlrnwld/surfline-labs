# Fairly basic wave tracking algorithm that looks at horizontal grids based on
# wave countour parameters, and groups all waves in the same latitude together.
# The outputs are GridBlob objects (defined in here) or cv2 contour parameters.
#
import time
import bisect
import numpy as np
import cv2

from typing import Optional, Union, List
from collections import defaultdict
from itertools import groupby
from sl_camera_sensor.utils.logger import getLogger
from sl_camera_sensor.utils.cam.sort import Sort, iou as _iou
from sl_camera_sensor.lib.wave_tracking.blobify import Blobify
from ipdb import set_trace

log = getLogger(__name__)


class WaveTracker:
    def __init__(self, fps, label_map, **kwargs):
        self.fps = fps
        self.label_map = label_map
        self.target_size = (576, 1024)
        self.dynamic_grid = True
        # Tracking defaults, some of which are updated dynamically.
        # min number of blobs per group to be considered a wave
        self.min_cnt = np.max([np.ceil(fps * 3), 2])
        self.min_area = 8000  # min number of pix per wave blob
        # After this many no detects it's finished
        self.persist_cnt = np.max([np.ceil(fps * 4), 4])
        # Overwrite any params with kwargs
        for k, v in kwargs.items():
            setattr(self, k, v)
        self.n_grid = 13  # This is not an acceptable user input anymore.
        self.grid = Grid(self.target_size, self.n_grid)
        self.roll_rates = []
        self.count = 1  # Count the total number of waves
        self.finished_cnt = 0

        # Store persistent tracks here.
        self.waves = defaultdict(lambda: defaultdict(list))
        self.finished_waves = {}
        self._cnt = 0
        self._pop_cnt = 0
        self._roll_cnt = 0
        self._filt_cnt = 0
        self.blobber = Blobify(
            self.label_map,
            target_size=self.target_size,
            classes=["Wave", "WhiteWater", "Lip"],
            min_area=self.min_area,
        )
        self._raw = defaultdict(list)

    def reset(self):
        """ 
        If we want to use the same tracking object across multiple
        rewinds, just call this between rewinds.
        """
        lm = self.label_map
        fps = self.fps
        target_size = self.target_size
        dg = self.dynamic_grid
        self.__init__(fps, lm, target_size=target_size, dynamic_grid=dg)

    def wave_count(self) -> int:
        """ Get how many waves the tracker has tracked, so far. """
        return len(self.finished_waves)

    def blobs_to_df(self, track):
        
    def pop_tracks(
        self, filtered: Optional[bool] = False, done: Optional[bool] = False
    ) -> dict:
        """
        Pop off any finished tracks. There is a slight time delay here
        based on self.persistent_cnt. Once the tracker updates
        self.persistent_cnt times.
        
        :param filtered: bool to specify if you want the tracks
        kalman filtered. This only matters if you're looking at percents,
        or you want waves on the same lateral grid separated.
        :param done: Return all the tracks in self.waves,
        independent of self.persistent_cnt. If you're running
        on a rewind for example, call this after the frame 
        generator is exhausted.
        :returns: dict of wave tracks
        """
        # TODO done kwd doesn't work
        finished = {k: v["blobs"] for k, v in self.finished_waves.items()}
        if done:
            for wave_key in self.waves.keys():
                finished[wave_key] = self.waves[wave_key]["blobs"]
        if filtered:
            finished = self._filter_waves(finished)
        return finished

    def batch_update(
        self,
        masks: np.ndarray,
        timestamps: Optional[Union[List, np.ndarray]] = None,
        logits: Optional[bool] = False,
    ) -> None:
        """ 
        Update the tracking state from a list of masks. Input
        a list of timestamps that correspond to each mask if you
        care about storing outputs.
        """
        for mask, timestamp in zip(masks, timestamps):
            self(mask, timestamp=timestamp, logits=logits)

    def update(self, mask, logits=False):
        """ 
        Update the tracking state with a single mask.
        :param mask: segmentation model output.
        :param logits: if it's a raw model output, set as true.
        :returns: updates self.waves
        """
        if isinstance(mask, list):
            self.batch_update(mask, logits)
        else:
            self(mask, logits=logits)

    def __len__(self):
        return self.wave_count()

    def __call__(
        self,
        mask: np.ndarray,
        logits: Optional[bool] = False,
        timestamp: Optional[Union[List, np.ndarray]] = None,
        return_contours: Optional[bool] = True,
        store_sub_blobs=True,
    ) -> None:
        """
        Make contours from this mask, combining all the
        attributes that make up a wave. If you want to also store the 
        individual attributes that make up the wave, set store_sub_blobs=True,
        and each GridBlob will hold those contours in a sub_blobs kwd.
        
        :param mask: a single segmentation mask nd array
        :param logits: if your mask needs an argmax applied, ie
        it's dimension is [n_class, H, W], set this as true.
        :param return contours: get back the wave conturs for this mask.
        if False this returns nothing and simply updates the tracker.
        You can get finished tracks via pop_tracks(). Use this for annotations.
        :param timestamp: timestamp that accompanies mask. These are important.
        :param sub_blobs: bool to keep track of sub-components.
        """
        self._cnt += 1
        inputs = []
        self.blobber.update(mask, store_sub_blobs=store_sub_blobs, logits=logits)
        wave_blobs = self.blobber.get()

        # First update n_grid dynamically.
        if self.dynamic_grid:
            for _, contour in wave_blobs.items():
                # Done here and below because if we update the grid here
                # the blob's gridnum below will be correct.
                blob = GridBlob(contour, pcts=None)
                self._raw["MA"].append(blob.MA)
            if len(self._raw["MA"]) > 10:
                grid_update = self.grid.check_ngrid(self._raw)
                if grid_update:
                    # just need to offset it
                    shift = self.grid.roll(self._get_grid_offset())
                # Put a limit on how much we store, otherwise the objects
                # will become huge
                for k, v in self._raw.items():
                    self._raw[k] = v[-200:]
                
        # First group all the blobs in the same grid to the same wave group.
        iblobs = defaultdict(list)
        for i, (pcts, contour) in enumerate(wave_blobs.items()):
            if not store_sub_blobs:
                pcts = None  # key won't have pcts in this case
            # Store contour in gridblob object with wave attr pcts (maybe)
            blob = GridBlob(contour, timestamp, pcts=pcts)
            # Find which grid this gridblob lives in.
            if blob.angle == 0:
                blob.angle = 90.0  # I have no idea why this happens sometimes.
            blob.grid = self.grid.find_grid(blob.x, blob.y, blob.angle - 90)
            iblobs[blob.grid].append(blob)  # Each key is a group match of waves
            inputs.append(blob)  # in case we want to return labels

        # Archive all waves that haven't had a grid match in a while
        del_waves = [
            wave_key
            for wave_key in self.waves
            if (self._cnt - self.waves[wave_key]["persist_cnt"]) >= self.persist_cnt
        ]
        for del_wave in del_waves:
            finished = self.waves.pop(del_wave)
            if len(finished["blobs"]) > self.min_cnt:
                self.finished_cnt += 1
                self.finished_waves[self.finished_cnt] = finished
                #print('adding to finished waves')
                #print(self.finished_waves)
                
        # If self.waves is empty, just fill with inputs and return
        if len(self.waves) == 0:
            for grid_num, grid_blob in iblobs.items():
                self._add_wave(self.count, grid_blob)
                self.count += 1
            return None if not return_contours else self._map_inputs(inputs)

        # Loop through the existing waves and check if there are any matches
        # with the current inputs. Just need to check grids, no need for IOUs.
        for wave_key in self.waves.keys():
            wave_grid = self.waves[wave_key]["grid"]
            # Do any wave grids match inputs?
            if wave_grid in iblobs.keys():
                self._add_wave(wave_key, iblobs[wave_grid])
                del iblobs[wave_grid]  # reduce the number of blobs to look at

        # If we have any left over, add new elements to self.waves.
        if len(iblobs) > 0:
            for grid_num, grid_blob in iblobs.items():
                self._add_wave(self.count, grid_blob)
                self.count += 1

        # Update grid to roll with the waves
        shift = self.grid.roll(self.roll_rate())
        if shift:
            for wave_num in self.waves:
                self.waves[wave_num]["grid"] += 1
        return None if not return_contours else self._map_inputs(inputs)

    def roll_rate(self):
        """ 
        Figure out dynamically how fast the waves are 
        rolling down the frame. Still some tweaking here.
        """
        roll_rate = self._get_grid_roll_rate()
        # print(f'rate: {roll_rate}')
        if not (self._roll_cnt % 3):
            grid_offset = self._get_grid_offset()
            if grid_offset != -1:
                # print(f'Offset: {grid_offset}')
                roll_rate -= grid_offset  # // 2
                # roll_rate += abs(grid_offset) #// 2
        # For low fps values this will never be negative. When it is, waves are
        # jumping grids.
        if roll_rate < 1 and self.fps < 0.33:
            roll_rate = np.abs(roll_rate) * 3
        # print(f'roll rate: {roll_rate}')
        # if np.abs(roll_rate) > (self.fps * 13):
        #    roll_rate = np.sign(roll_rate) * 40
        self._roll_cnt += 1
        return roll_rate

    def _map_inputs(self, inputs):
        """ 
        In order to return the contours that correspond to the
        blobs we extracted from the input mask, we just map them
        based on our blobs. Gets contours that correspond to input/last call.
        """
        output = {}
        for wave_key in self.waves:
            group_len = len(self.waves[wave_key]["blobs"])
            for blob in self.waves[wave_key]["blobs"]:
                for ipt in inputs:
                    if blob == ipt:
                        output[(wave_key if group_len > 1 else 0)] = blob
        return output

    def _add_wave(self, wave_num, blob_list):
        """ Add a list of input blobs assigned to wave_num """
        self.waves[wave_num]["blobs"] += blob_list
        self.waves[wave_num]["current_match"] = True
        self.waves[wave_num]["grid"] = blob_list[0].grid
        self.waves[wave_num]["persist_cnt"] = self._cnt

    def perp(self, x, y):
        # Rotate input coords so they are perp to beach.
        if self.grid.rot != 0:
            rot = np.deg2rad(self.grid.rot)
            cos, sin = np.cos(rot), np.sin(rot)
            R = np.array([[cos, -sin], [sin, cos]])
            x, y = R.dot(np.vstack((x, y)))
        return x, y
    
    def _get_grid_roll_rate(self, inst=True):
        """ At what rate is the y axis processing? """
        if inst:
            # Do we want instantaneous roll rate?
            self.roll_rates = []
        for wave_num in self.waves.keys():
            ys = [blob.y for blob in self.waves[wave_num]["blobs"]]
            if len(ys) >= 2:
                self.roll_rates.append(np.mean(np.diff(sorted(ys))))
        self.roll_rates = self.roll_rates[-10:]  # -30:  TBD
        return np.mean(self.roll_rates) if self.roll_rates else 0

    def _get_grid_offset(self):
        """ realign the grid to center the longest breaking wave """
        longest = [(len(v["blobs"]), k) for k, v in self.waves.items()]
        if len(longest) < 1:
            return 0
        longest = max(longest)[1]
        # longest = min(longest)[1]
        wave_loc = self.waves[longest]["blobs"][-1].y
        wave_grid = self.waves[longest]["grid"]
        try:
            med_grid = np.median([self.grid.yg[wave_grid], self.grid.yg[wave_grid - 1]])
        except IndexError:
            # This happens because sometimes on rotated beaches the contour
            # centroid is off the screen. Should be clipped in find_grid call.
            # In other words I think this is fixed.
            log.critical("*** grid yg IndexError ***:")
            print("  yg:\n", self.grid.yg)
            print("  wave_grid:\n", wave_grid)
            log.critical(f"returning wave_loc: {wave_loc}")
            return wave_loc
        offset = int(med_grid - wave_loc)
        # if np.abs(offset) > (self.fps * 25):
        #    offset = np.sign(offset) * 10
        return offset

    def _filter_waves(self, finished_waves):
        """ Apply kalman filtering on wave tracks. This should effectively
        separate a wave with multiple breaking points, which we need
        when we're looking at wave shape conditions, but otherwise this
        step is unnecessary. It adds about 1-3s of compute time, and is
        called when one invokes tracker.pop_tracks(filtered=True')
        """
        if len(finished_waves) <= 0:
            return finished_waves
        filtered_waves = defaultdict(list)
        for wave_num in finished_waves.keys():
            # No need for persistence since gridding does that for us.
            _kalman = Sort(max_age=self.persist_cnt, min_hits=2)
            # Run kalman filter at each timestamp
            for _, group_blobs in groupby(
                finished_waves[wave_num], lambda x: x.timestamp
            ):
                group_blobs = list(group_blobs)
                # Generate bounding boxes for each wave blob
                bboxes = np.vstack(
                    [blob.bbox(*self.target_size[::-1]) for blob in group_blobs]
                )
                bboxes = np.hstack(  # Add a "confidence"
                    (bboxes, np.ones((bboxes.shape[0], 1)) * 0.9)
                )
                filtered_tracks = _kalman.update(bboxes, iou_threshold=0.0)
                # Just add the trackID as wave_num + track_id / 100. These
                # will be the new keys in the filtered_waves output dict.
                if len(bboxes) == 1:
                    if len(filtered_tracks) == 1:
                        track_id = wave_num + filtered_tracks.flatten()[-1] / 100.0
                        filtered_waves[track_id].append(group_blobs[0])
                    else:
                        # In this case the filter deleting waves...
                        pass
                else:
                    # Multiple boxes to be filtered. Kalman doesn't
                    # return the same number of outputs as inputs, so need
                    # to explicitly check unfortunately.
                    for filtered_track in filtered_tracks:
                        ious = np.asarray(
                            [_iou(bbox[:-1], filtered_track[:-1]) for bbox in bboxes]
                        )
                        track_id = wave_num + filtered_track[-1] / 100.0
                        filtered_waves[track_id].append(group_blobs[np.argmax(ious)])

            # Just one last thing to fix here - kalman sometimes effectively skips
            # track numbers, and I want the keys sequential. So fix that.
            self._filt_cnt += 1
            if len(filtered_waves) < 1:
                return filtered_waves
            sorted_filtered_waves = {}
            # TODO probably better to just add two columns here - wave_num, track_num
            subcnt = 0
            for v in filtered_waves.values():
                if len(v) > self.min_cnt:
                    ikey = self._filt_cnt + (subcnt + 1) / 10
                    sorted_filtered_waves[ikey] = v
                    subcnt += 1
            if len(sorted_filtered_waves) < 1:
                self._filt_cnt -= 1

        return sorted_filtered_waves


class GridBlob:
    """ 
    Object to store blobs on our frame grid. These are wave objects,
    which have 'grid' attributes that is essentially the tracking ID.
    The tracker calls this.

    You'll get back lists gridblob objects as tracks. Call print(gridblob)
    on each one to extract pertinent contour params that we save to txt files.
    """

    def __init__(self, contour, timestamp=None, target_size=None, cls=None, pcts=None):
        self.grid = None  # Which grid this blob live in
        self.target_size = target_size
        self.timestamp = timestamp if timestamp is not None else time.time()
        self.contour = contour
        (self.x, self.y), (self.MA, self.ma), self.angle = cv2.fitEllipse(contour)
        self.arclen = cv2.arcLength(contour, True)
        self.sub_blobs = defaultdict(list)  # Store subclass contours here.
        self.area = np.pi * self.MA * self.ma
        self.cls = cls
        self.pcts = self._parse_pcts(pcts)

    def _parse_pcts(self, pcts):
        """ 
        Blobify now returns contours dict with key showing pcts, ex:

        '0_Wave:0.41_WhiteWater:0.57_Lip:0.02'

        where '0_' is the count (to avoid key overloading).
        """
        if pcts is None:
            return None
        vals = pcts.split("_")[1:]
        pcts = {}
        for val in vals:
            k, v = val.split(":")
            pcts[k] = float(v)
        return pcts

    def __str__(self):
        """
        Add an arclen component so we can filter our the awkwardly shaped one
        with an arclen/area threshold (or something similar). Arclengths
        of contours are not recoverable from ellipse params.
        Also add percents from 3 wave classes.
        """
        wpct = np.min([self.pcts["Wave"], 1.0]) if "Wave" in self.pcts else 0.0
        wwpct = (
            np.min([self.pcts["WhiteWater"], 1.0]) if "WhiteWater" in self.pcts else 0.0
        )
        lpct = np.min([self.pcts["Lip"], 1.0]) if "Lip" in self.pcts else 0.0
        rep = (
            f"{self.x:.0f}, {self.y:.0f}, "
            f"{self.MA:.0f}, {self.ma:.0f}, "
            f"{self.angle:.0f}, {self.arclen:.0f}, "
            f"{wpct:.3f}, {wwpct:.3f}, {lpct:.3f}"
        )
        if self.timestamp is not None:
            rep = f"{self.timestamp:.3f}, {rep}"
        return rep

    def bbox(self, xmax, ymax):
        x, y, w, h = cv2.boundingRect(self.contour)
        x0 = max(0, x - w // 2)
        y0 = max(0, y - h // 2)
        x1 = min(x + w // 2, xmax)
        y1 = min(y + h // 2, ymax)
        return np.asarray([x0, y0, x1, y1])


class Grid:
    """ Defines the tracking grid that the waves live in. This 
    is updated dynamically based on the wave contour parameters.
    It rotates itself and reduces the number of grids based on 
    live wave conditions. Again this is called by the tracker.
    This also defines the grid number (track ID) for each gridblob.
    """

    def __init__(self, target_size, n_grid=13):
        self.target_size = target_size  # (height, width)
        self.n_grid = n_grid
        self.xg = None
        self.yg = None
        self.rot = 0  # Assume cam is face-on to start with.
        self._rots = []  # Get self.rot from average of a few blobs
        self._reset_grid()
        self._num_ngrid_checks = 0  # How many times we've checked dynamic n_grid

    def find_grid(self, x, y, angle):
        """ 
        Given a point (x, y), find the [rotated] horizonal
        patch that the point lives in.
        """
        # We need to accumulate some blob angles, so we know how the
        # beach is orientated to the breaking waves.
        if angle == -90.0:
            log.critical("Bad rotation")
            set_trace()
        self._rots.append(angle)
        self._check_rot()
        xpos = bisect.bisect_left(self.xlocs, x)
        xpos = np.max([0, np.min([self.target_size[1], xpos])])
        xpos = xpos - 1 if xpos >= self.n_grid else xpos
        grid_num = bisect.bisect_left(self.yg[:, xpos], y)
        return grid_num

    def check_ngrid(self, raw_waves):
        """ 
        Try to measure the number of grids we need. Default is
        to start at 10, but needs to be slightly adjusted for some spots.
        
        :param waves: dict[dict] of accumulated waves so far. Should be >= 2.
        :returns: updates self.ngrid and resets if it's different than current
        """
        dy = np.max(raw_waves["MA"])
        ngrid = int(self.target_size[0] / dy)
        # print(f'ngrid -> {ngrid}')
        # Force 10 <= ngrid <= 25
        ngrid = np.max([12, np.min([ngrid, 25])])
        # update = True if ngrid > self.n_grid else False
        # self.n_grid = int(np.max([self.n_grid, ngrid]))
        # return update
        self._num_ngrid_checks += 1
        if ngrid != self.n_grid:
            old_ngrid = self.n_grid
            if self._num_ngrid_checks == 1:
                self.n_grid = int(ngrid)
            else:
                # self.n_grid = int(np.max([self.n_grid, ngrid]))
                self.n_grid = int(np.mean([self.n_grid, ngrid]))
            if old_ngrid != self.n_grid:
                log.info(
                    f"Updating ngrid: {old_ngrid} -> {self.n_grid} "
                    f'({len(raw_waves["MA"])} waves)'
                )
            self._reset_grid()
            if self.rot != 0:
                self._rotate(self.rot)
            return True
        return False

    def lines(self):
        """ So we can plot on an image and see what's happening """
        return [
            ([int(self.xg[0][0]), int(self.xg[0][-1])], [int(yg[0]), int(yg[-1])])
            for yg in self.yg
        ][:-1]

    def roll(self, dy):
        shift = False
        # if np.abs(dy) > 10:
        #    dy = 1 * np.sign(dy)
        self.yg += dy
        if self.yg[0][0] > self.buff * 2.5:
            shift_y = np.diff(self.yg[:, 0]).mean()
            bottom = self.yg[0] - shift_y
            self.yg = np.vstack((bottom, self.yg[:-1]))
            shift = True
        return shift

    def _reset_grid(self):
        """ Reset the grid to the default starting position """
        self.xlocs = np.linspace(0, self.target_size[1], self.n_grid)
        self.buff = self.target_size[0] // self.n_grid
        self.ylocs = np.linspace(
            self.buff, self.target_size[0] + self.buff, self.n_grid
        )
        self.xg, self.yg = np.meshgrid(self.xlocs, self.ylocs)
        self.dy = np.mean(np.diff(self.yg[:, 0]))

    def _check_rot(self):
        """ 
        If we've accumulated enough contour angles, check that
        the current grid rotation angle is approximately the same.
        """
        # print(f'Len rots ={len(self._rots)}')
        if len(self._rots) > 20:
            if np.abs(np.mean(self._rots) - self.rot) > 2:
                print(f"Rotating grid: {self.rot:.2f} -> {np.mean(self._rots):.2f}")
                self.rot = np.mean(self._rots)
                self._rotate(self.rot)
            self._rots = self._rots[-20:]  # Reset the avg rotations.

    def _rotate(self, degrees):
        """ Roate the entire grid to match average contour rotations """
        #self._reset_grid()
        theta = np.deg2rad(degrees)
        c, s = np.cos(theta), np.sin(theta)
        R = np.array(((c, -s), (s, c)))
        self.xg, self.yg = np.einsum("ji, mni -> jmn", R, np.dstack([self.xg, self.yg]))
