import numpy as np


class Grid:
    """ Defines the tracking grid that the waves live in. This 
    is updated dynamically based on the wave contour parameters.
    It rotates itself and reduces the number of grids based on 
    live wave conditions. Again this is called by the tracker.
    This also defines the grid number (track ID) for each gridblob.
    """

    def __init__(self, target_size, n_grid=13):
        self.target_size = target_size  # (height, width)
        self.n_grid = n_grid
        self.xg = None
        self.yg = None
        self.rot = 0  # Assume cam is face-on to start with.
        self._rots = []  # Get self.rot from average of a few blobs
        self._reset_grid()
        self._num_ngrid_checks = 0  # How many times we've checked dynamic n_grid

    def find_grid(self, x, y, angle):
        """ 
        Given a point (x, y), find the [rotated] horizonal
        patch that the point lives in.
        """
        # We need to accumulate some blob angles, so we know how the
        # beach is orientated to the breaking waves.
        if angle == -90.0:
            log.critical("Bad rotation")
            set_trace()
        self._rots.append(angle)
        self._check_rot()
        xpos = bisect.bisect_left(self.xlocs, x)
        xpos = np.max([0, np.min([self.target_size[1], xpos])])
        xpos = xpos - 1 if xpos >= self.n_grid else xpos
        grid_num = bisect.bisect_left(self.yg[:, xpos], y)
        return grid_num

    def check_ngrid(self, raw_waves):
        """ 
        Try to measure the number of grids we need. Default is
        to start at 10, but needs to be slightly adjusted for some spots.
        
        :param waves: dict[dict] of accumulated waves so far. Should be >= 2.
        :returns: updates self.ngrid and resets if it's different than current
        """
        dy = np.max(raw_waves["MA"])
        ngrid = int(self.target_size[0] / dy)
        # print(f'ngrid -> {ngrid}')
        # Force 10 <= ngrid <= 25
        ngrid = np.max([12, np.min([ngrid, 25])])
        # update = True if ngrid > self.n_grid else False
        # self.n_grid = int(np.max([self.n_grid, ngrid]))
        # return update
        self._num_ngrid_checks += 1
        if ngrid != self.n_grid:
            old_ngrid = self.n_grid
            if self._num_ngrid_checks == 1:
                self.n_grid = int(ngrid)
            else:
                # self.n_grid = int(np.max([self.n_grid, ngrid]))
                self.n_grid = int(np.mean([self.n_grid, ngrid]))
            if old_ngrid != self.n_grid:
                log.info(
                    f"Updating ngrid: {old_ngrid} -> {self.n_grid} "
                    f'({len(raw_waves["MA"])} waves)'
                )
            self._reset_grid()
            if self.rot != 0:
                self._rotate(self.rot)
            return True
        return False

    def lines(self):
        """ So we can plot on an image and see what's happening """
        return [
            ([int(self.xg[0][0]), int(self.xg[0][-1])], [int(yg[0]), int(yg[-1])])
            for yg in self.yg
        ][:-1]

    def roll(self, dy):
        shift = False
        # if np.abs(dy) > 10:
        #    dy = 1 * np.sign(dy)
        self.yg += dy
        if self.yg[0][0] > self.buff * 2.5:
            shift_y = np.diff(self.yg[:, 0]).mean()
            bottom = self.yg[0] - shift_y
            self.yg = np.vstack((bottom, self.yg[:-1]))
            shift = True
        return shift

    def _reset_grid(self):
        """ Reset the grid to the default starting position """
        self.xlocs = np.linspace(0, self.target_size[1], self.n_grid)
        self.buff = self.target_size[0] // self.n_grid
        self.ylocs = np.linspace(
            self.buff, self.target_size[0] + self.buff, self.n_grid
        )
        self.xg, self.yg = np.meshgrid(self.xlocs, self.ylocs)
        self.dy = np.mean(np.diff(self.yg[:, 0]))

    def _check_rot(self):
        """ 
        If we've accumulated enough contour angles, check that
        the current grid rotation angle is approximately the same.
        """
        # print(f'Len rots ={len(self._rots)}')
        if len(self._rots) > 20:
            if np.abs(np.mean(self._rots) - self.rot) > 2:
                print(f"Rotating grid: {self.rot:.2f} -> {np.mean(self._rots):.2f}")
                self.rot = np.mean(self._rots)
                self._rotate(self.rot)
            self._rots = self._rots[-20:]  # Reset the avg rotations.

    def _rotate(self, degrees):
        """ Roate the entire grid to match average contour rotations """
        # self._reset_grid()
        theta = np.deg2rad(degrees)
        c, s = np.cos(theta), np.sin(theta)
        R = np.array(((c, -s), (s, c)))
        self.xg, self.yg = np.einsum("ji, mni -> jmn", R, np.dstack([self.xg, self.yg]))
