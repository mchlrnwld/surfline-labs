import datetime
import time
from collections import defaultdict

import cv2
import numpy as np
from scipy.signal import savgol_filter
from scipy.interpolate import UnivariateSpline
from ipdb import set_trace

class WaveBlob:
    """
    Object to store blobs on our frame grid. These are wave objects,
    which have 'grid' attributes that is essentially the tracking ID.
    The tracker calls this.

    You'll get back lists gridblob objects as tracks. Call print(gridblob)
    on each one to extract pertinent contour params that we save to txt files.
    """

    def __init__(self, contour, **kwargs):
        self.contour = contour
        (self.x, self.y), (self.MA, self.ma), self.angle = cv2.fitEllipse(
            contour
        )
        hull = cv2.convexHull(contour, returnPoints=False)
        self.area = cv2.contourArea(contour)
        try:
            defects = cv2.convexityDefects(contour, hull)
            self.fit = 1 - (np.std(defects[..., -1]) / self.area)
        except cv2.error:
            # Catch cv2.error: (-5:Bad argument) The convex hull indices are not monotonous,
            # which can be in the case when the input contour contains self-intersections
            # in function 'convexityDefects
            self.fit = -1
        self.arclen = cv2.arcLength(contour, True)
        self.aoa = 10.0 * self.arclen / self.area
        # Supplementary contour metrics.

        # self.k = np.nan
        # self.ncycl = np.nan
        # x, y = np.squeeze(contour).T
        # #x, y = np.append(x, x[0]), np.append(y, y[0])  # closure
        # window = np.min([17, len(x)])
        # if not window % 2:
        #     window -= 1
        # x, y = savgol_filter(x, window, 3), savgol_filter(y, window, 3)
        # theta = np.arctan2(np.diff(y), np.diff(x))
        # self.ncycl = np.sum(np.abs(np.diff(theta))) / 2 / np.pi / 10.0
        # x, y = np.squeeze(contour).T
        # std = np.ones_like(x) * 1e-6
        # t = np.arange(len(x))
        # fx = UnivariateSpline(t, x, k=5, w=1 / np.sqrt(std))
        # fy = UnivariateSpline(t, y, k=5, w=1 / np.sqrt(std))
        # x = fx(t)
        # y = fy(t)
        # dx = fx.derivative(1)(t)
        # d2x = fx.derivative(2)(t)
        # dy = fy.derivative(1)(t)
        # d2y = fy.derivative(2)(t)

        # k = (dx * d2y - dy * d2x) / (dx ** 2 + dy ** 2) ** (2.0 / 3.0)
        # self.k = np.sum(np.abs(k)) / 100.0
        
        #x, y = np.squeeze(contour).T
        #dy, dx = np.diff(y), np.diff(x)
        #d2x, d2y = np.diff(dx), np.diff(dy)
        #dx, dy = dx[1:], dy[1:]

        
        #self.grad = np.sum(np.abs(np.diff(tan - np.min(tan)))) / np.pi / 2      # lim 9
        #self.grad = len(np.where(np.diff(tan - np.min(tan)) // round(2*np.pi,2))[0])
        #self.grad = len(np.where(np.diff((tan - np.min(tan))//np.pi))[0])
        #self.grad = len(np.where(np.diff(np.cumsum(np.diff(tan))//np.pi))[0])
        #set_trace()
        #theta = np.arctan(np.diff(y) / np.diff(x))
        #self.grad = np.abs(np.diff(theta)).sum()  #np.abs(theta // np.pi).sum()

        self.timestamp = time.time()
        self.sub_blobs = defaultdict(list)  # Store subclass contours here?
        self.mask = None  # Binary mask of wave
        self.surfers = []  # Localize dets/mask fusion.
        self.pockets = []
        #self.hmask = []  # height/pocket mask to omit inflatons
        self.offshore = 0  # Offshore flag from offshore blob associations.
        self.grid = None  # Which grid this blob live in
        self.col_names = (
            "track_num,timestamp,x,y,MA,ma,angle,fit,aoa,"#ncycl,k,"#hmask,"#area,arclen,"
            "wpct,wwpct,lpct,surfer,pockets,slices,ft,ofsh\n"
        )
        self.rep = (
            "{self.timestamp:.3f},"
            "{self.x:.0f},{self.y:.0f},"
            "{self.MA:.2f},{self.ma:.2f},"
            "{self.angle:.0f},{self.fit:.2f},{self.aoa:.2f},"#{self.ncycl:.2f},{self.k:.2f},"
            "{self.wpct:.3f},{self.wwpct:.3f},{self.lpct:.3f},"
            '"{self.surfer_locs}","{self.pocket_locs}","{self.slice_locs}",'
            '"{self.ft_fmt}",{self.offshore}'
        )
        for k, v in kwargs.items():
            if isinstance(v, datetime.datetime):
                v = v.timestamp()
            setattr(self, k, v)

    @property
    def wave_info(self):
        return self._wave_info

    @wave_info.setter
    def wave_info(self, inp):
        self._wave_info = inp if inp is not None and len(inp) > 0 else None

    @property
    def pcts(self):
        return self._pcts

    @pcts.setter
    def pcts(self, inp):
        self._pcts = self._parse_pcts(inp)

    def _parse_pcts(self, pcts):
        """
        Blobify now returns contours dict with key showing pcts, ex:

        '0_Wave:0.41_WhiteWater:0.57_Lip:0.02'

        where '0_' is the count (to avoid key overloading).
        """
        if pcts is None:
            return None
        vals = pcts.split("_")[1:]
        pcts = {}
        for val in vals:
            k, v = val.split(":")
            pcts[k] = float(v)
        return pcts

    def get_col_names(self):
        return self.col_names

    def add_attr(self, name, val, fmt=".1f"):
        if isinstance(name, self):
            raise ValueError(
                f"Cannot overwrite {name} attribute in wave blob self."
            )
        self.col_names = self.col_names.strip() + "," + name.strip() + "\n"
        setattr(self, name, val)
        self.rep += ", {self." + name + ":" + fmt + "}"

    def __str__(self):
        """
        Add an arclen component so we can filter our the awkwardly shaped waves
        with an arclen/area threshold (or something similar). Arc lengths
        of contours are not recoverable from ellipse params.
        Also add percents from 3 wave classes, SurferSurfing locations
        on this wave, offshore flag, and perhaps a shape/closeout flag?
        """
        self.wpct = np.min([self.pcts["Wave"], 1.0]) if "Wave" in self.pcts else 0.0
        self.wwpct = (
            np.min([self.pcts["WhiteWater"], 1.0])
            if "WhiteWater" in self.pcts
            else 0.0
        )
        self.lpct = np.min([self.pcts["Lip"], 1.0]) if "Lip" in self.pcts else 0.0
        self.surfer_locs = ";".join(
            [
                f"({loc[0]:.4f},{loc[1]:.4f},{loc[2]:.4f},{loc[3]:.4f})".replace(
                    " ", ""
                )
                for loc in self.surfers
            ]
        )
        self.pocket_locs = ";".join(
            [
                f"({loc[0]:.4f},{loc[1]:.4f})".replace(" ", "")
                for loc in self.pockets
            ]
        )
        self.slice_locs = ";".join(
            [
                f"({loc[0]:.4f},{loc[1]:.4f},{loc[2]:.4f})".replace(" ", "")
                for loc in self.slices
            ]
        )
        #try:
        #    self.hmask_fmt = ";".join([f"{ipm:.0f}".replace(" ", "") for ipm in self.hmask])
        #except:
        #    set_trace()
        self.ft_fmt = ";".join([f"{ift:.1f}".replace(" ", "") for ift in self.ft])
        if isinstance(self.timestamp, datetime.datetime):
            self.timestamp = self.timestamp.timestamp()
        return eval(f'f"""{self.rep}"""')
