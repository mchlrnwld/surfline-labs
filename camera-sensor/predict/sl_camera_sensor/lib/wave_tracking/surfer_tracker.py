import os
import numpy as np
import pandas as pd
import torch
import pickle
import datetime

from torchvision.ops import boxes as box_ops
from collections import defaultdict
from typing import List, Optional, Union
from scipy.spatial.distance import cdist
from scipy.optimize import linear_sum_assignment as lsa

# from .surfer import Surfer
from sl_camera_sensor.lib.wave_tracking.surfer import Surfer
from sl_camera_sensor.lib.localization.localize_dets import LocalizeDets
from sl_camera_sensor.utils.logger import getLogger
from ipdb import set_trace

log = getLogger(__name__)


class SurferTracker:
    def __init__(self, fps, target_size=None, label_map=None, store_crops=True, min_hits=None):
        self.fps = float(fps)
        self.target_size = target_size
        self.label_map = label_map
        self.store_crops = store_crops
        # Tracks will be stored as a function of wave_num, where each item is a list
        # of Surfer objects, as each wave can have multiple surfers.
        self.waves = defaultdict(list)
        # accumulate and average direction the wave is propagating
        self.wave_angles = []  # We don't need this for every wave do we??
        self.cnt = 0
        self.surfer_count = 0
        self.min_hits = np.max([3, self.fps / 2]) if min_hits is None else min_hits # At 1fps 3 hits is a ~2s wave
        self.relaxation_time = 6  # seconds to consider a wave finished.
        self.finished = {}
        self._finished_cnt = 0
        self._low_hit_cnt = 0
        self.current_updates = []
        self.current_timestamp = 0.0
        self._switches = []  # keep track of identity switches
        self.dr_thresh = 0.15 if self.fps < 5 else 0.1  # sanity thresholding
        # At 2 this is pretty spazzy when >10fps ish
        self.min_hits_extend = 2 if self.fps < 5 else 3  # barrels

    @property
    def label_map(self):
        return self._label_map

    @label_map.setter
    def label_map(self, inp):
        self._label_map = inp
        if inp is not None:
            self.map_label = {v: k for k, v in self._label_map.items()}
        else:
            self.map_label = None

    def __len__(self):
        return self._finished_cnt

    def get_current_timestamp(self):
        return self.current_timestamp

    def get_total_hits(self):
        return [
            track.hits for waves in surfer_tracker.tracks.values() for track in waves
        ]

    def get_live_surfers(self):
        """ Get a list of the surfer objects, instead of tracks, 
        so you can access the object defs
        """
        return [
            surfer for (wave_num, surfers) in self.waves.items() for surfer in surfers
        ]

    def get_live_tracks(
        self, target_size: Optional[tuple] = None, return_df: Optional[bool] = False
    ) -> List:
        return [
            t.get_track(return_df=return_df, target_size=target_size)
            for t in self.get_live_surfers()
        ]

    def pop_tracks(self, done=False, target_size=None):
        """ This returns tracks that are finished only!
        If you're streaming and you want the current tracks,
        call get_live_tracks()
        """
        # Filter out the remaining waves to finished if we're done.
        if done:
            self.__pop_finished(self.waves.keys(), target_size)
            log.info(
                f"{self._low_hit_cnt} / {self._finished_cnt + self._low_hit_cnt} "
                "had low hits."
            )
        delkeys = list(self.finished.keys())
        finished = self.finished.copy()
        for k in delkeys:
            del self.finished[k]
        return finished

    def __call__(
        self, wavetracks: Union[pd.Series, pd.DataFrame], **kwargs
    ) -> Union[None, List[Surfer]]:
        """
        wave_info: Optional[List[dict]]=None,
        mask: Optional[np.ndarray]=None,
        bin_masks: Optional[Dict[int, np.ndarray]]=None,
        return_surfers: Optional[bool]=False
        """
        # Do we want to return stuff, like if we're streaming rn?
        if wavetracks is None or len(wavetracks) == 0:
            self.current_timestamp += 1.0 / self.fps
            self.__trim_stale_waves()
            if kwargs["return_surfers"]:
                self.__trim_stale_current_updates()
                return self.current_updates
            return None
        if "mask" in kwargs:
            if self.label_map is None:
                raise ValueError(
                    "You've input a mask in the SurferTracker"
                    " but there is no label_map set."
                )
        if isinstance(wavetracks, pd.Series):
            """
            When we're running on completed .csv wavetracks, we just
            iterate through each row, so one update at a time as it's
            ordered by track num.
            """
            self._update(wavetracks, **kwargs)
            self.current_timestamp = wavetracks["timestamp"]
        elif isinstance(wavetracks, pd.DataFrame):
            # When we're streaming, there will be multiple waves per update.
            if len(np.unique(wavetracks["timestamp"].values)) > 1:
                raise ValueError(
                    "SurferTracking call should be over a single timestamp only"
                )
            for i, (idx, wavetrack) in enumerate(wavetracks.iterrows()):
                self._update(wavetrack, **kwargs)
                self.current_timestamp = wavetrack["timestamp"]
        else:
            raise ValueError(
                "only pd.Series and pd.DataFrame types allowed in SurferTracker call."
            )

        self.__trim_stale_waves()
        if "return_surfers" in kwargs and kwargs["return_surfers"]:
            self.__trim_stale_current_updates()
            return self.current_updates

    def _update(self, wavetrack: pd.Series, **kwargs) -> None:
        # Given a single row of a wavetrack DataFrame, update surfer tracks in
        # self.waves. This updates for a single wave only.
        wave_num, surfers, timestamp, wave_angle = wavetrack[
            ["track_num", "surfer", "timestamp", "angle"]
        ].values
        if len(self.wave_angles) < 30:
            self.wave_angles.append(wave_angle)
            self.wave_angle = np.mean(wave_angle)

        if (isinstance(surfers, str) and len(surfers) == 0) or (
            isinstance(surfers, float) and np.isnan(surfers)
        ):
            self.cnt += 1
            if self.current_timestamp is not None:
                try:
                    if isinstance(self.current_timestamp, datetime.datetime):
                        self.current_timestamp += datetime.timedelta(seconds=1.0/self.fps)
                    #elif isinstance(self.current_timestamp, (float, int)):
                    else:
                        self.current_timestamp += 1.0 / self.fps
                except Exception as e:
                    log.critical(f"Unable to iterate the current timestamp: {e}")
            return

        # Parse surfer positions from the dataframe row. Could be multiple.
        surfer_locs = np.asarray([eval(loc) for loc in surfers.split(";")])
        if len(surfer_locs) == 0:
            log.info("No surfer locs in tracker update.")
            self.__map_inputs(surfer_locs, **kwargs)
            return

        if surfer_locs.ndim < 2:
            surfer_locs = surfer_locs[np.newaxis, :]

        # Process the inputs to get a wave_info dict
        wave_info = self.__get_wave_info(surfer_locs, wave_num, wavetrack, **kwargs)

        # Start adding surfers. If self.waves[wave_num] is empty, we can't just add
        # unfortunately. But if there are no waves, we can.
        if len(self.waves) == 0:
            self.__add_surfers(wave_num, surfer_locs, timestamp, wave_info)
            self.__map_inputs(surfer_locs, **kwargs)
            return
        """
        If we've made it this far, we now need to check whether or not to append 
        to existing tracks or start new ones. Can have multiple tracks per wave,
        as more than one person can surf one wave unfortunately.
        We need not differentiate tracks on separate waves if we're going
        to deal with identity switching in the wave tracker. In that case we just
        do a big linear sum assignment on the entire self.waves state...
        (We do however need to keep track of the wave_nums and track indices
        for each of those wave_nums.)
        """
        track_locs = np.vstack([s.loc for waves in self.waves.values() for s in waves])
        wave_nums, track_nums = self.__get_wave_and_track_arrays()
        dists = cdist(track_locs, surfer_locs)
        wave_track_match, surfer_match = lsa(dists)
        for wavetrack_match_idx, surfer_match_idx in zip(
            wave_track_match, surfer_match
        ):
            """
            Now we need to keep track of the indices properly. That cost matrix is an
            unraveled self.waves dict, basically.

            What is the threshold for adding a new track? The reason we need
            this is the event when someone falls down and another person takes
            off down the wave. It's also a sanity threhold in the sense that we
            think it's unreasonable for missed detections to occur for distances
            larger than this (approx 10% of the frame).
            """
            iwave_num = wave_nums[wavetrack_match_idx]
            itrack_num = track_nums[wavetrack_match_idx]
            if dists[wavetrack_match_idx, surfer_match_idx] < self.dr_thresh:
                if iwave_num != wave_num:
                    # Check that this doesn't connect to a wave behind -__-
                    y0 = surfer_locs[surfer_match_idx, [0, 2]].mean()
                    y1 = self.waves[iwave_num][itrack_num].y[-1]
                    if y0 < y1:
                        continue
                    id_switch = f"{wave_num}->{iwave_num}"
                    if id_switch not in self._switches:
                        log.info(
                            f"Caught an identity switch in wave {wave_num} -> {iwave_num} "
                            f"(dr={dists[wavetrack_match_idx, surfer_match_idx]:0.2f}) "
                            f"{timestamp:.2f}"
                        )
                        self._switches.append(id_switch)
                try:
                    self.__extend_track(
                        iwave_num,
                        itrack_num,
                        surfer_locs[surfer_match_idx],
                        timestamp,
                        wave_info[surfer_match_idx],
                    )
                except Exception as e:
                    log.info(f"Unable to extend track {itrack_num}: {e}")
            else:
                """
                This could be a barrel-type situation with missed intermittent
                detections - direction will be the same and velocity should be high.
                problem is we need to check in the next loop or 2, since we need 2 pts
                to know the velocities. For now just add it, and add a check flag.
                ONLY SET CHECK FLAG IF THIS WAVE_NUM IS THE SAME AS WHAT'S IN THE INPUT WAVETRACK
                Now that we're doing a global comparison I think it's wrong to check
                barrels for separate waves. That will be a nightmare, I think
                """
                check = wave_num == iwave_num
                self.__add_surfer(
                    wave_num,
                    surfer_locs[surfer_match_idx],
                    timestamp,
                    wave_info[surfer_match_idx],
                    check=check,
                )
        """
        Last thing is when there's more surfers than existing tracks.
        Just add those unmatched surfers as new tracks. This can
        happen when there's multiple detections of the same object (low detection thresh),
        in which case it would be wrong to add a new surfer -- check IOUs
        on this wave - if IOUs are > ~0.1 then just drop it, otherwise
        add as new surfer.
        """
        if dists.shape[1] > dists.shape[0]:
            """
            Get surfer indices that weren't matched in lsa.
            Since we may have extended or added new surfers, we need to compute
            wave_nums and track_nums arrays again.
            """
            overlap = np.in1d(np.arange(len(surfer_locs)), surfer_match)
            add_idxs = np.where(~overlap)[0]
            wave_nums, track_nums = self.__get_wave_and_track_arrays()
            check_surfer_locs = surfer_locs[add_idxs]
            check_track_locs = np.vstack(
                [s.loc for waves in self.waves.values() for s in waves]
            )
            ious = self.iou(
                check_track_locs[:, [1, 0, 3, 2]], check_surfer_locs[:, [1, 0, 3, 2]]
            )
            ok_to_add = np.where(ious <= 0.1)[
                0
            ]  # TODO not totally sure about this thresh in crowds
            if len(ok_to_add) > 0:
                for ok_add_idx in ok_to_add:
                    try:
                        self.__add_surfer(
                            wave_num,
                            check_surfer_locs[ok_add_idx],
                            timestamp,
                            wave_info[add_idxs[ok_add_idx]],
                        )
                    except IndexError:
                        # This is fine, it'll catch this surfer in the next call
                        pass

        self.__map_inputs(surfer_locs, **kwargs)

    def __get_wave_and_track_arrays(self):
        """ This keeps track of wave and track indices for our cost matrices
        in LSA.
        """
        wave_nums = [
            wave_key
            for (wave_key, wave_tracks) in self.waves.items()
            for track_num in range(len(wave_tracks))
        ]
        track_nums = [
            track_num
            for (wave_key, wave_tracks) in self.waves.items()
            for track_num in range(len(wave_tracks))
        ]
        return wave_nums, track_nums

    def __extend_track(self, wave_num, track_num, surfer_loc, timestamp, wave_info):
        """ If we're extending an existing track and the check flag is
         true, we need to compare this length 2+ track with all
         the other tracks on this wave that are going in the same direction,
         assuming the velocity of this short track is high velocity.
         should be high and direction of travel should be the same.
        
         Update: those assumtions generally don't hold.
        """
        self.waves[wave_num][track_num].extend(surfer_loc, timestamp, wave_info)
        if (
            self.waves[wave_num][track_num].check
            and self.waves[wave_num][track_num].hits >= self.min_hits_extend
        ):
            # Set to false since we only need to check once.
            self.waves[wave_num][track_num].check = False
            isurfer = self.waves[wave_num][track_num]
            # Check against every existing track.
            for i in range(len(self.waves[wave_num])):
                if i == track_num:
                    continue  # don't compare with itself obv.
                itrack = self.waves[wave_num][i]
                if itrack.hits < 2:
                    continue  # can't be a continuation of a new track...
                """
                if itrack.get_avg_direction_string() != isurfer.get_inst_direction_string():
                   continue
                if isurfer.get_inst_speed() < itrack.get_avg_speed() / 2.5:
                   #< itrack.get_inst_speed() / 2.:
                   # TODO param here?
                   continue
                """
                # Check if it's super far away.
                dr = cdist(
                    np.expand_dims(isurfer.loc, axis=0),
                    np.expand_dims(itrack.loc, axis=0),
                )[0][0]
                dt = timestamp - itrack.timestamp
                if (dr / dt) > itrack.get_inst_speed() * 1.1:
                    continue  # Too fast -- is this actually doing what I think it is?

                # If we've made it this far then it must be a match?
                # Remove this tracklet and extend initial track.
                try:
                    self.waves[wave_num][i].extend(
                        isurfer.locs, isurfer.timestamps, isurfer.wave_infos
                    )
                except:
                    print("failed stitch")

                del self.waves[wave_num][track_num]
                break

    def __add_surfer(self, wave_num, surfer_loc, timestamp, wave_info, check=False):
        """ Surfer loc is a single surfersurfing bbox location. This adds a new surfer 
        surfing track to an existing wave. 
        """
        self.surfer_count += 1
        self.waves[wave_num].append(
            Surfer(
                wave_num,
                surfer_loc,
                timestamp,
                wave_info,
                check=check,
                ID=self.surfer_count,
            )
        )

    def __add_surfers(self, wave_num, surfer_locs, timestamp, wave_info):
        """ Surfer locs is a multidimensional [N, 4] array. """
        if surfer_locs.ndim < 2:
            raise ValueError(
                f"Wrong surfer_locs shape in __add_surfers: {surfer_locs.shape}"
            )
        for i in range(len(surfer_locs)):
            try:
                self.__add_surfer(wave_num, surfer_locs[i], timestamp, wave_info[i])
            except Exception as e:
                print("add surfers failed: ", e)

    def _extend_wave_info(self, a, b):
        if len(a) == 0:
            return b
        elif len(b) == 0:
            return a
        assert len(a) == len(b)
        return [{**aa, **bb} for aa, bb in zip(a, b)]

    def __get_wave_info(self, surfer_locs, wave_num, wavetrack, **kwargs):
        """ Build a dictionary of wave info attributes. You can store
        segmentation mask crops in the surfer tracks if you want, for example.
        """
        # If we've been passed a mask, overwite any 'wave_info' kwd passed.
        if "mask" in kwargs:
            # Actually do the cropping here to extract wave info.
            wave_info = LocalizeDets.localize_mask_inside_surfer(
                surfer_locs.copy(),
                kwargs["mask"],
                self.map_label,
                store_crops=self.store_crops,
            )
        elif "wave_info" not in kwargs:
            wave_info = [{}] * len(surfer_locs)
        else:
            wave_info = kwargs["wave_info"]

        """
        Get the surfer's distance from peak to trough, and ratio
        of wave height to surfersurfing height, using the binary mask
        explicitly, not cv2.fitcontour params. Ie this may be slightly noisy.
        TODO add some averaging to this, instead of slicing a single column.
        """
        if "bin_masks" in kwargs:
            if kwargs["bin_masks"][wave_num] is None:
                log.critical(
                    "There is no binary mask passed for SurferSurfing "
                    f"on wave {wave_num}."
                )
            if "bin_masks" in kwargs and wave_num in kwargs["bin_masks"]:
                lip_trough = LocalizeDets.localize_lip_trough_height(
                    surfer_locs.copy(), kwargs["bin_masks"][wave_num]
                )
                wave_info = self._extend_wave_info(lip_trough, wave_info)
        if "pockets" in wavetrack:
            """ Distance to pocket. TODO perhaps change this method from dilation 
            addition to contour subtraction, as I did in the shoreline estimation.
            """
            pocket_locs = wavetrack["pockets"]
            if (isinstance(pocket_locs, str) and len(pocket_locs) > 0) or (
                isinstance(pocket_locs, float) and not np.isnan(pocket_locs)
            ):
                pocket_locs = [eval(p) for p in pocket_locs.split(";")]
                pocket_dists = LocalizeDets.get_dist_to_pocket(
                    surfer_locs.copy(), np.vstack(pocket_locs)
                )
                if len(wave_info) > 0:
                    assert len(pocket_dists) == len(wave_info)
                """
                Couple ways to get the sign on this distance. First is the obvious,
                which is looking if pocket is to the left or right of the x-coord
                and someone going left with the pocket on the left will be +, pocket
                on the right will be -. Other is to look if they're in whitewater
                or not, which right now I like better.
                """
                if not isinstance(wave_info, list):
                    raise ValueError("wave_info should be a list.")
                # This will only work if masks are passed too :(
                # Should probably just do this the Euclidian way.
                if 'pctWhiteWater' in wave_info:
                    for pctww in (wave_info['pctWhiteWater']):
                        if pctww > 0.65:
                            pocket_dists[i]["dPocket"] *= np.float16(-1)
                wave_info = self._extend_wave_info(pocket_dists, wave_info)
        return wave_info

    def __map_inputs(self, surfer_locs, **kwargs) -> None:
        """
        This updates self.current updates, but only necessary if you're streaming
        and you want the returned tracks for each update. If return_surfers isn't
        set in the call, this will do nothing. 
        """
        if "return_surfers" in kwargs and kwargs["return_surfers"]:
            if not np.all(np.isnan(surfer_locs)):
                for wave_num, surfers in self.waves.items():
                    for surfer in surfers:
                        for surfer_loc in surfer_locs:
                            if np.all(surfer_loc == surfer.loc):
                                if surfer not in self.current_updates:
                                    self.current_updates.append(surfer)

    def __trim_stale_waves(self):
        # Figure out which ones are finished and move them out of self.waves.
        del_waves = []
        for wave_key, wave in self.waves.items():
            """ 
            Look at each surfer in this wave, not just the last one,
            since we don't know when any of them were last updated.
            """
            latest_timestamp = np.max([sfr.timestamp for sfr in wave])
            if (self.current_timestamp - latest_timestamp) > self.relaxation_time:
                del_waves.append(wave_key)
        self.__pop_finished(del_waves, target_size=None)

    def __trim_stale_current_updates(self):
        """ When we make a call, the current updates won't be updated
        if the input dataframe is empty, so we need to make sure
        what is currently in self.current_updates isn't too old.
        Basically just doing self.__trim_stale_waves() but
        on self.current_updates instead of self.waves
        """
        keepers = []
        for i, surfer in enumerate(self.current_updates):
            if (self.current_timestamp - surfer.timestamp) > self.relaxation_time:
                # log.info(f'removing stale surfer ({surfer.ID}) from current updates')
                pass
            else:
                keepers.append(surfer)
        self.current_updates = keepers

    def __pop_finished(self, finished_keys, target_size=None):
        if target_size is None:
            target_size = self.target_size
        for del_wave in list(finished_keys).copy():
            finished_wave = self.waves.pop(del_wave)
            ##print(f'wave {del_wave} is finished')
            # multiple tracks per wave!
            for track in finished_wave:
                if len(track) >= self.min_hits:
                    self._finished_cnt += 1
                    self.finished[self._finished_cnt] = track.get_track(
                        target_size=target_size
                    )
                else:
                    self._low_hit_cnt += 1

    def iou(self, box1, box2):
        """ should be ordered x0, y0, x1, y1. 
        https://detectron2.readthedocs.io/en/latest/_modules/torchvision/ops/boxes.html
        """
        box1 = torch.tensor(np.vstack([*box1]), dtype=torch.float)
        box2 = torch.tensor(np.vstack([*box2]), dtype=torch.float)
        iou = box_ops.box_iou(box1, box2)
        return iou.numpy()

    @staticmethod
    def get_fps_from_wavetracks(wavetracks):
        """ Don't use diffs in here since there may be gaps in tracks """
        rates = []
        for _, grp in wavetracks.groupby("track_num"):
            rates.append(
                len(grp) / (grp["timestamp"].iloc[-1] - grp["timestamp"].iloc[0])
            )
        return np.mean(rates).astype(np.float16)

    @classmethod
    def process_complete_tracks(cls, wavetracks: Union[pd.DataFrame, str]) -> None:
        """ Processes the full list of wavetracks and updates self.waves """
        if isinstance(wavetracks, str):
            save_name = wavetracks.replace(".wavetracks", ".surfertracks").replace(
                ".csv", ".pkl"
            )
            if not os.path.isfile(wavetracks):
                raise ValueError(f"Input wavetracks file does not exist: {wavetracks}")
            wavetracks = pd.read_csv(wavetracks)
        fps = SurferTracker.get_fps_from_wavetracks(wavetracks)
        surfer_tracker = cls(fps, min_hits=2)
        for i, row in wavetracks.iterrows():
            surfer_tracker(row)
        with open(save_name, "wb") as f:
            pickle.dump(surfer_tracker.pop_tracks(done=True), f)
        log.info(f"Wrote {save_name}")

if __name__ == "__main__":
    # tracks_file = 'wc-hbpierns.wavetracks.20210120T162505494.25.0fps.csv'  # Good one!
    tracks_file = "hi-pipeline.wavetracks.20210215T211610030.25.0fps.csv"
    tracks_dir = "/data/stv4/segmentation"
    SurferTracker.process_complete_tracks(os.path.join(tracks_dir, tracks_file))
