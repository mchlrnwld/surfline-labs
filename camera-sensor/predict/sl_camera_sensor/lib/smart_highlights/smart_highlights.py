# Extract timestamps for smart highlights from 10m rewinds.
#
import os
import time
import pickle
import argparse
import numpy as np
import sl_camera_sensor.utils.cam.video_utils as vu

from typing import Optional, List

from sl_camera_sensor.lib.wave_tracking.wave_tracker_iou import WaveTracker
from sl_camera_sensor.utils.logger import getLogger
from sl_camera_sensor.nets.segmentation.segmentation_model import SegmentationModel
from sl_camera_sensor.utils.cam.vid_trim import vid_trim
from ipdb import set_trace

log = getLogger(__name__)


class SmartHighlights:
    """ The full pipeline for this algorithm, starting from a rewind,
    is the following:
        1) Frame extraction at specified fps (1/6s minimum).
        2) Semantic segmentation on each frame.
        3) Segmentation maps -> blobs/contours.
        4) Beach normal directional component, assuming shore is ~straight.
        5) Wave positions perpendicular from shoreline.
        6) Sigma clipping temporal histogram of 5) to extract timestamps

    By default this uses the wave tracker. We can easily get start times
    without it, but end times are equally important, and vary quite a lot
    between spots.

    Run as:
    sh = SmartHighlights(fps=0.5, make_highlight_clip=True, unix_times=True)
    timestamps = sh.from_rewind("path/to/10m_rewind.mp4")
    """

    def __init__(
        self,
        fps: Optional[float] = 1.0 / 2,
        seg_bot: Optional[SegmentationModel] = None,
        make_highlight_clip: Optional[bool] = True,
        max_duration: Optional[int] = 30,
        unix_time: Optional[bool] = False,
        batch_size: Optional[int] = 5,
    ) -> None:
        """ 
        :param fps: We think 6s is the minimum wave period we'll 
        want to pick up, so 0.166 is the minimum hz we can run at.
        :param seg_bot: Segmentation model instance. If none is passed,
        this will construct one and store it in self after you call
        .from_rewind().
        :param make_highlight_clip: True to clip the input rewind to make
        a smart hightlight video from it. Name will have .highlights in it.
        :param max_duration: maximum duration in seconds of highlights clip.
        This is a hard upper limit, so in general it will be less than this - 
        if you have two waves each 20s long and you set a 30s duration, 
        it will only include the first wave, so the clip will be 20s long.
        :param unix_time: This method returns clip timestamps. Set to True to 
        return Unix timestamps, otherwise these will be rewind-relative.
        :param batch_size: Segmentation model inference batch size. Not super
        important but upper limit will depend on which instance you're running.

        """
        self.fps = fps
        self.seg_bot = seg_bot
        self.tracker = None
        self.make_highlight_clip = make_highlight_clip
        self.max_duration = max_duration
        self.unix_time = unix_time
        self.batch_size = batch_size
        self.cnt = 0

    def from_rewind(self, rewind_path: str) -> List[tuple]:
        """ 
        Run the full pipeline as described in constructor.
        :param rewind_path: full path string to 10m rewind
        :returns: list of tuples with (start, stop) timestamps and optionally
        writes clipped rewind to disk.
        """
        if not os.path.isfile(rewind_path):
            raise ValueError("Input rewind does not exist: " f"{rewind_path}")

        batches = vu.get_frame_batch(rewind_path, 0, self.batch_size, fps=self.fps)
        # Load a segmentation model instance if one doesn't exist.
        if self.seg_bot is None:
            self.seg_bot = SegmentationModel()
            self.tracker = WaveTracker(
                self.fps, self.seg_bot.label_map, dynamic_grid=True
            )
        if self.cnt > 0:
            # Reset tracker after processing each rewind
            self.tracker.reset()

        t0 = time.time()
        for i, (frames, timestamps) in enumerate(batches):
            if len(frames) == 0:
                break
            masks = self.seg_bot.predict(frames, logits=False)
            self.tracker.batch_update(masks, timestamps)

        log.info(f"Processing time: {time.time() - t0:.0f}s")
        # Extract tracks from tracker and apply thresholds to
        # filter out significant wave events.
        # TODO optionally write tracks to disk. Francisco can then maybe
        # get live wave heights from them.
        tracks = self.tracker.pop_tracks(filtered=False)
        timestamps = self._get_timestamps_from_tracks(tracks, self.max_duration)
        if len(timestamps) == 0:
            return []
        # Clip the input rewind around these timestamps if requested
        if self.make_highlight_clip:
            try:
                highlight_name = rewind_path.replace(".mp4", ".highlights.mp4")
                vid_trim(
                    rewind_path,
                    timestamps[:, 0],
                    timestamps[:, 1],
                    output_name=highlight_name,
                    unix_time=False,
                    force_key_frames=False,
                )
            except ValueError:
                log.info("Nonsequential timestamp error")
                set_trace()

        if unix_time:
            vid_timestamp = vu.get_vid_timestamp(rewind_path)
            timestamps += vid_timestamp

        self.cnt += 1
        return timestamps

    def _get_timestamps_from_tracks(self, tracks):
        y, timestamps = [], []
        for track_num, track in tracks.items():
            # Accumulate only the start & stop times, and starting position.
            timestamps.append([track[0].timestamp, track[-1].timestamp])
            y.append(track[0].y)
        y, timestamps = np.asarray(y), np.asarray(timestamps)
        outliers = np.where(y < (y.mean() - y.std()))[0]
        highlight_times = timestamps[outliers]
        log.info(f"Found {len(outliers)}/{len(y)} interesting segments")
        # Now we need to clip these further so the max_duration isnt' too long.
        # Question is do we do that before or after overlap concatenation?
        # For now just do it sequentially.
        dts = highlight_times[:, 1] - highlight_times[:, 0]
        # Immediately remove any clips > self.max_duration
        highlight_times = highlight_times[np.where(dts < self.max_duration)]
        # Accumulate until total clip will be < max_duration
        for upto, tot_time in enumerate(
            np.cumsum(highlight_times[:, 1] - highlight_times[:, 0])
        ):
            if tot_time > self.max_duration:
                break
        if upto == 0:
            log.info(
                "First wave is > max_duration ({self.max_duration}s)."
                " Clip will exceed duration."
            )
            upto = 1
        log.info(
            f"Keeping {upto}/{len(outliers)} to "
            f"satisfy {self.max_duration}s max_duration"
        )
        highlight_times = highlight_times[:upto]

        # Now merge any overlaps
        if upto > 1:
            highlight_times = np.asarray(
                SmartHighlights._merge_overlaps(highlight_times.tolist())
            )
        print(highlight_times)
        dts = highlight_times[:, 1] - highlight_times[:, 0]
        log.info(f"Average clip time is {dts.mean():.1f}s, {dts.sum():.1f}s total")
        return highlight_times

    @staticmethod
    def _merge_overlaps(times, idx=0):
        for i in range(idx, len(times) - 1):
            if times[i][1] > times[i + 1][0]:
                new_start = times[i][0]
                new_end = times[i + 1][1]
                times[i] = [new_start, new_end]
                del times[i + 1]
                return SmartHighlights._merge_overlaps(times.copy(), idx=i)
        return times


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-d", "--directory", help="process all rewinds in a directory", default=None
    )
    parser.add_argument(
        "-i", "--input", help="process a single rewind input", default=None
    )
    parser.add_argument("-f", "--fps", default=0.5)
    args = parser.parse_args()
    if args.input is None and args.directory is None:
        raise ValueError("Specify directory path or file path to rewind(s)")

    if args.directory:
        rewind_list = [
            os.path.join(args.directory, f)
            for f in os.listdir(args.directory)
            if f.endswith(".mp4")
        ]
    else:
        rewind_list = [args.input]

    sh = SmartHighlights(fps=args.fps)
    for rewind_path in rewind_list:
        ts = sh.from_rewind(rewind_path)
        if len(ts) > 0:
            with open(rewind_path.replace(".mp4", ".highlights.txt"), "wb") as f:
                pickle.dump(ts, f)
