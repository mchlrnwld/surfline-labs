# Smart Highlights

We can get start and stop times of breaking waves from 10m rewinds using segmentation model outputs
and the wave tracker. Stop times are equally as important as start times, which is why
we need tracking.

```python
highligher = SmartHighlights(fps=0.5)
timestamps = highlighter.from_rewind('/path/to/wc-hbpierns.mp4', max_duration=40, make_highlight_clip=True)
```
which will return a list of tuples of start and stop times, up to a maximum duration of 40s.
It would also write to disk `wc-hbpierns.highlights.mp4`, which clips the input video at the
nearest key frames, so requires no additional overhead.

Few different ways to filter and present these timestamps. Currently they are ordered in time, but
could also be ordered by height, duration, merged overlap duration (sets), or some combination of those.

In some conditions the segmenation model detects waves many seconds before they break. One
approach to minimize the duration of the highlight clips is to clip the timestamps when the waves
are < 100% `Wave` class, and < 100% `WhiteWater` class.
