# Delineates regions, connects them, and possibly keeps track of instantaneous
# and average values for those that are dynamic.
#
import cv2
import numpy as np
import pandas as pd

from collections import deque, defaultdict
from typing import Optional, Union, List
from sl_camera_sensor.lib.wave_tracking.blobify import Blobify
from sl_camera_sensor.lib.localization.localize_dets import LocalizeDets
# Just make a base class ^^
from sl_camera_sensor.lib.wave_tracking.wave_blob import WaveBlob
from sl_camera_sensor.nets.segmentation.segmentation_model import SegmentationModel
from sl_camera_sensor.utils.logger import getLogger
from scipy.spatial.distance import cdist
from scipy.interpolate import interp1d, UnivariateSpline
from scipy.ndimage.measurements import label as label_components
from ipdb import set_trace

log = getLogger(__name__)


class LocalizeMask:
    def __init__(self, label_map=None, target_size=None):
        self.label_map = label_map or SegmentationModel.get_label_map()
        self.target_size = target_size
        self.width = None
        self.height = None
        self._shorelines = None  # Historical shorelines
        self._horizons = None  # Historical horizons. Ideally there is no variance here :)
        self.blobbers = {'sea': None, 'shore': None, 'sky': None}
        self._cnt = 0
        self.available_regions = {'shoreline', 'horizon'}
        self._shoreline_intersects = ['sea','shore']
        self._horizon_intersects = ['sea', 'sky']
        self._prev_mask = None
        
    def __init_blobbers(self):
        """
        Need to know the frame size prior to initializing the blobbers,
        do this after the first call, when self.width, self.height are set.
        """
        self.blobbers = {}
        self.blobbers['shore'] = Blobify(
            self.label_map, classes=['Shore', 'Thing'],  # TODO do we want thing in here?
            min_area=500, target_size=self.target_size
        )
        self.blobbers['sea'] = Blobify(
            self.label_map,
            classes=['Water', 'WhiteWater',
                     'Wave', 'Lip', 'Offshore'],
            min_area=1000, target_size=self.target_size
        )
        self.blobbers['sky'] = Blobify(
            self.label_map, classes=['Sky'],
            min_area=1000, target_size=self.target_size
        )
        self.regions = list(self.blobbers.keys())

    def __call__(self, mask):
        self.shoreline = self._get_intersection(mask, 'shore')
        self.horizon = self._get_intersection(mask, 'sky')
        
    def __check_mask(self, mask, logits=False):
        if logits:
            mask = np.argmax(mask, axis=0)
        if self.height is None:
            if self.target_size is None:
                self.height, self.width = mask.shape[:2]
            else:
                self.height, self.width = self.target_size
            self.target_size = (self.height, self.width)
            self.x = np.linspace(
                0, self.width-1, self.width, dtype=int)
        if np.any(list(self.blobbers.values())) is None:
            self.__init_blobbers()
        if self._prev_mask is None or np.any(mask != self._prev_mask):
            self.blobbers['sea'].update(mask)
            self._prev_mask = mask
            
    def get_shoreline(self, mask):
        self.shoreline = self._get_intersection(mask, 'shore')
        return self.shoreline

    def get_horizon(self, mask):
        self.horizon = self._get_intersection(mask, 'sky')
        return self.horizon
    
    def _get_intersection(self, mask, region):
        dat =  {'fit': -1,
                'raw': np.array([[np.nan, np.nan]]),
                'interp': np.array([[np.nan, np.nan]])
        }
        self.__check_mask(mask)
        self.blobbers[region].update(mask, sort=True)
        # Get the largest contours for each region
        conts = {}
        for key in ['sea', region]:
            # I think we actually want the n-largest, as there could be
            # non-contiguous regions due to foreground occlusion. Def for
            # the shoreline at least.
            iconts = self.blobbers[key].get_n_largest(3)
            # Sometimes there will be no region. If so just return nans?
            if len(iconts) == 0:
                return dat
            conts[key] = np.squeeze(np.vstack(iconts))

        dr = cdist(conts[region], conts['sea'])
        region_idx, sea_idx = np.where(dr <= 1)
        if len(sea_idx) > 0:
            #intersection = np.sort(conts['sea'][sea_idx], axis=0)
            intersection = conts['sea'][sea_idx]
            sorted_x_idxs = np.argsort(intersection[:,0])
            y_raw, y_interp = self._interp(intersection[sorted_x_idxs], self.x)

            #print("ADD CONVOLUTION TO DELINEATIONS")

            dat['raw'] = y_raw.astype(int)
            dat['interp'] = y_interp.astype(int)
            # Figure out "fit" to line if this region is 'horizon'
            if region == 'sky':
                dat['fit'] = self._get_fit(dat['raw'])
            return dat
        
        log.info(f'No {region} present in frame.')
        set_trace()
        return dat
        
    def _interp(self, arr, on, fillna=False):
        y_interp_raw = interp1d(
            arr[:, 0], arr[:, 1],
            assume_sorted=False, bounds_error=False, fill_value='extrapolate'
        )(on)
        rawidx = np.where(~np.isnan(y_interp_raw) & ~np.isinf(y_interp_raw))
        raw = np.vstack((self.x[rawidx], y_interp_raw[rawidx])).T
        # We'll have to just fill in nans with nearest vals.
        y_interp = np.squeeze(y_interp_raw)
        y_interp[np.where(np.isinf(y_interp))] = np.nan
        y_interp = pd.Series(y_interp).fillna(method='bfill')
        y_interp = pd.Series(y_interp).fillna(method='ffill').values
        return raw,  np.vstack((self.x, y_interp)).T

    def _get_fit(self, arr):
        # TODO this is stupid you should look at linearity within
        # the extent of the frame range. Obviously a pool ball is
        # not smooth if you look closely enough.
        x, y = arr[:, 0], arr[:, 1]
        coefs = np.polyfit(x, y, 1)
        p = np.poly1d(coefs)
        return np.float16(abs(p(x)-y).mean())

