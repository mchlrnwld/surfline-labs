# One needs to edit the torchvision classes to return tensors i1nstead of dicts,
# since trtorch needs torch 1.5.0, which doesn't let you override this when
# making a torch script (1.6 does).
#
# $ python export.py --model_name deeplabv3_resnet50 --weights_path /path/to/model.pth
#
# which will output a .pt torchscript and .ts trtorch script
#
import os
import argparse
import trtorch
import torch
try:
    import vision_model
except ImportError:
    pass
from pdb import set_trace


class hack(torch.nn.Module):
    """ If you don't wrap your model with this class you can
    modify the sourcecode to return the 'out' key tensor
    instead of a dict, as an alternative
    """
    def __init__(self, model):
        super().__init__()
        self.model = model

    def forward(self, inp):
        return self.model(inp)['out']

    
def export(model_name, weights_path, batch_size, output_path=None):
    """ Export weights + model -> scripts.
    
    :param model_name: model name to construct model class instance.
    efficientnet_b0, deeplabv3_resnet50 or 101
    :param weights_path: full path to final model checkpoint
    :returns: writes .pt and .ts model scripts to disk, in same dir as weights.
    """
    sy, sx = 576, 1024
    tensor = torch.rand(batch_size, 3, sy, sx).to('cuda')

    # Make a trace if we were given an .pth file
    if weights_path.endswith('.pth'):
        model = vision_model.get(model_name, weights=weights_path, num_classes=9)
        model = hack(model)
        model = model.eval().to('cuda')
        model_trace = torch.jit.trace(model, tensor)
        torch.jit.save(model_trace, weights_path.replace('.pth', '.pt'))
        print(f"Exported jit script to {weights_path.replace('.pth', '.pt')}")
    elif weights_path.endswith('.pt'):
        model_trace = torch.jit.load(weights_path)
    else:
        raise ValueError("only .pt traces and .pth raw weights are accepted for 'weights_path'")
    # Try to make trt trace
    params = { 
         "input_shapes": [(batch_size, 3, sy, sx)], 
         "op_precision": torch.half, # Run with FP16 
         "workspace_size": 1 << 20 
    }
    print(f'Compiling TRT script with input shape: {params["input_shapes"]}...')
    trt_model = trtorch.compile(model_trace, params)
    # Try to pass forward on trt script
    tensor = tensor.half()
    result = trt_model(tensor)
    # Save it!
    ts_save_name = weights_path.replace('.pth', '.ts')
    if output_path is not None:
        if not os.path.isdir(output_path):
            os.makedirs(output_path)
        ts_save_name = os.path.join(output_path, os.path.basename(ts_save_name))
    torch.jit.save(trt_model, ts_save_name)
    print(f"Wrote TRT script to {ts_save_name}")
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--model_name', default='deeplabv3_resnet50', type=str)
    parser.add_argument('--weights_path',
                        default='/data/segmentation/torch/zoo/deeplabv3_resnet50_720p/good-model-subset.pth')
    parser.add_argument('--batch_size', default=2, type=int)
    parser.add_argument('--output_path', default=None)
    args = parser.parse_args()
    export(args.model_name, args.weights_path, args.batch_size, args.output_path)

