# TensorRT Export Scripts

These take `.pth` weight paths or `.pt` jit scripts and compiles
accelerated trt ops in a jit script (`.ts`). At the moment
this is only for segmentation - object detection acceleration
needs to be done in a separate container.

This is slightly confusing as the TensorRT operators have changed
since v7 - methods to resize tensors are no longer consistent
between PyTorch and TensorRT. You will get the following warnings
prior to a message from the script of dissimilar outputs from
a forward pass between the jit script and trt jit script:

```bash
WARNING: [Torch-TensorRT] - There may be undefined behavior using dynamic shape and aten::size
WARNING: [Torch-TensorRT] - Dilation not used in Max pooling converter
WARNING: [Torch-TensorRT TorchScript Conversion Context] - Convolution + generic activation fusion is disable due to incompatible driver or nvrtc
```

The last is suspect but probably not related.

See interpolation inconsistencies in [TensorRT](https://github.com/NVIDIA/Torch-TensorRT/blob/master/core/conversion/converters/impl/interpolate.cpp#L350) compared to [PyTorch](https://github.com/pytorch/vision/blob/main/torchvision/models/segmentation/deeplabv3.py#L82). Note also there is possible some issue with [`align_corners`](https://github.com/NVIDIA/TensorRT/issues/273#issuecomment-616899833)

Could also be an issue: https://github.com/NVIDIA/Torch-TensorRT/blob/master/core/conversion/converters/impl/interpolate.cpp#L343

For legacy models with tensorflow support use the v1.5 export script which
imports trtorch instead of torch_tensorrt - same library, different versions
and renamed namespace. That version of TRT does not have this issue.