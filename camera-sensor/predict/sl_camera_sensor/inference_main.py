# Run the full stack of inference and output predictions to pickles,
# or run subsets of individual models.
# TODO add tracking classes in here?
#
import os
import sys
import time
import argparse
import threading

from sl_camera_sensor.utils import logger
from sl_camera_sensor.utils import video_utils as vu
from sl_camera_sensor.nets.detection import detection_model
from sl_camera_sensor.nets.segmentation import segmentation_model
from sl_camera_sensor.nets.visibility import visibility_model
from sl_camera_sensor.utils.track_from_pkl import track_from_pkl

from ipdb import set_trace
log = logger.getLogger(__name__)


def parse(args):
    parser = argparse.ArgumentParser(description='slcs: Surfline Camera Sensor, [accelerated] inference')
    parser.add_argument(dest='command', help='choose from: stack|detect|segment|vis')
    
    parser.add_argument('-i', '--input', default=None, type=str,
                        help='Path to input rewind or directory containing multiple rewinds',
                        required=False)

    parser.add_argument('--fps', default=1, type=float, help='Inference frame rate')

    parser.add_argument('--extn', default='.mp4', type=str, help='Input file type')

    parser.add_argument('--batch', '-b', default=2, type=int,
                        help='batch size of your static models, they should be consistent'
    )
    parser.add_argument('--det-model-name', '-dmn',
                        default='retinanet-resnet50-720p-bs2-tdv5.plan',
                        type=str, required=False,
                        help=('Detection model name. Can be '
                              'TRT engine/plan, torch script, or TRT-torchscript')
    )
    parser.add_argument('--det-target-size', '-dts',
                        default=(720, 1280), nargs=2,
                        help=('This is model-dependent. If youre running .engine '
                              'or .plan files these will be overwritten.')
    )
    parser.add_argument('--seg-model-name', '-smn',
                        default='deeplabv3-resnet50-720p-tdv7-cu11.pt',
                        type=str, required=False, help='segmentation model name'
    )
    parser.add_argument('--seg-target-size', '-sts',
                        default=(720, 1280), nargs=2,
                        help='This is model-dependent.'
    )
    parser.add_argument('--vis-model-name', '-vmn',
                        default='xgb_multirank_vis.model'
    )
    parser.add_argument('--vis-target-size', '-vts',
                        default=(576, 1024), nargs=2
    )
    parser.add_argument('--graphs-dir', '-gd',
                        default='/data/graphs/',
                        help='Base directory where the above model graphs live.'
    )
    parser.add_argument('--overwrite', '-o',
                        action='store_true',
                        help='overwrite existing neural network outputs'
    )
    parser.add_argument('--track', '-t', action='store_true',
                        help=("Run wave & surfer tracking. If the seg model isn't being "
                              "run, this won't do anything. If the detection model isn't "
                              "being run, this will only do wave tracking")
    )
    parsed_args = parser.parse_args(args)
    parsed_args.det_target_size = tuple(map(int, parsed_args.det_target_size))
    parsed_args.seg_target_size = tuple(map(int, parsed_args.seg_target_size))
    return parsed_args

def get_inputs(args):
    """ 
    Parse args.input, which is either a directory full of rewinds/stills,
    or a single rewind/still
    """
    if os.path.isfile(args.input) and args.input.endswith(args.extn):  # single rewind or still
        return [args.input]
    elif os.path.isdir(args.input):  # directory of rewinds or stills
        mp4_files = [
            os.path.join(args.input, f) for f in os.listdir(args.input)
            if f.endswith(args.extn)
        ]
        if len(mp4_files) == 0:
            raise ValueError(f'Found np {args.extn.replace(".", "")} files in {args.input}')
        return sorted(mp4_files)
    else:
        raise ValueError(f'{args.input} is not a directory or {args.extn} file')

def __load_model(model_name, batch_size, target_size, inst):
    return inst(
        model_name=model_name,
        batch_size=batch_size,
        target_size=target_size
    )

def _load_models(args):
    # TODO add vis model instance
    if args.graphs_dir is not None:
        os.environ['GRAPHS_DIR'] = args.graphs_dir
    det_bot = seg_bot = vis_bot = None
    if args.command in ('stack', 'detect'):
        det_bot = __load_model(
            args.det_model_name, args.batch, args.det_target_size, detection_model.DetectionModel
        )
    if args.command in ('stack', 'segment'):
        seg_bot = __load_model(
            args.seg_model_name, args.batch, args.seg_target_size, segmentation_model.SegmentationModel)
    if args.command in ('stack', 'vis'):
        vis_bot = __load_model(
            args.vis_model_name, args.batch, args.vis_target_size, visibility_model.VisibilityModel)
    return det_bot, seg_bot, vis_bot

def _can_overwrite(args, pkl_name):
    if os.path.isfile(pkl_name):
        if not args.overwrite:
            log.info(f'Not overwriting {pkl_name}, skipping')
            return False
        log.info(f'Overwriting {pkl_name}')
    return True

def _get_save_name(mp4, bot, args):
    if type(bot).__name__ == 'DetectionModel':
        return mp4.replace('.stream.', '.dets.').replace(args.extn,f'.{args.fps:.1f}fps.pkl')
    if type(bot).__name__ == 'SegmentationModel':
        return mp4.replace('.stream.', '.masks.').replace(args.extn,f'.{args.fps:.1f}fps.pkl')
    if type(bot).__name__ == 'VisibilityModel':
        return mp4.replace('.stream.', '.vis.').replace(args.extn,f'.{args.fps/10:.1f}fps.pkl')    

def _run_inference_many(mp4s, bot, args):
    for mp4 in mp4s:
        save_name = _get_save_name(mp4, bot, args)
        if _can_overwrite(args, save_name):
            try:
                t0 = vu.get_vid_timestamp(mp4)
            except ValueError:
                t0 = 0
                log.info(f"Can't parse start timestamp for {vid}")
            preds = vu.run_batch_inference(
                mp4, t0, bot, batch_size=args.batch,
                save_name=save_name,
                fps=args.fps
            )
            
def main():
    """ Entrypoint into the surfline camera sensor world """
    args = parse(sys.argv[1:])
    mp4s = get_inputs(args)
    det_bot, seg_bot, vis_bot = _load_models(args)
    threads = []
    if args.command in ('detect', 'stack'):
        threads.append(
            threading.Thread(
                target=_run_inference_many, args=(mp4s, det_bot, args))
        )
    if args.command in ('segment', 'stack'):
        threads.append(
            threading.Thread(
                target=_run_inference_many, args=(mp4s, seg_bot, args))
        )
    if args.command in ('vis', 'stack'):
        threads.append(
            threading.Thread(
                target=_run_inference_many, args=(mp4s, vis_bot, args))
        )

    log.info(f'Processing {len(mp4s)} rewinds with {len(threads)} models')

    # Start threads
    if len(threads) > 0:
        st = 'thread' if len(threads) == 1 else 'threads'
        log.info(f'Starting {len(threads)} {st}')
        for thread in threads:
            thread.start()
        for thread in threads:
            thread.join()
        
    elif len(threads) == 1:
        # don't run threaded for now.
        log.info('Not threading 1 thread')
        threads[0]._target(threads[0]._args)
            
    if args.track:
        log.info('Tracking is not yet supported.')
        """
        t0 = time.time()
        track_from_pkl(seg_pkl_name)
        log.info(f'Finished tracking in {time.time() - t0:.0f}s')
        """
    del det_bot, seg_bot, vis_bot

if __name__ == '__main__':
    main()
