# TensorRT inference class via the TensorRT python API
# Output box locations assume pytorch models and tf output
# convention, ie x1, y1, x2, y2 -> y1, x1, y2, x2
#
from typing import List

import sys
import os
import ctypes

import numpy as np
import PIL

from PIL import Image
from sl_camera_sensor.utils import logger
from ipdb import set_trace

pycuda_import_fail = False
try:
    import pycuda.driver as cuda
    import pycuda.autoinit
    import tensorrt as trt
    TRT_LOGGER = trt.Logger(trt.Logger.INFO)
except ImportError as err:
    pycuda_import_fail = True

log = logger.getLogger(__name__)


class DetectionModelTRT:

    context = None
    
    def load_model_trt(self, engine_path):
        """ 
        This expects to find a shared object file sitting next
        to this python class, called _C.so. That file is generated
        from the compilations that happen in setup.py when
        the sl_camera_sensor package is installed.
        """
        if pycuda_import_fail:
            raise ImportError(
                'Unable to import pycuda, cannot run TensoRT inference '
                'with the TensorRT API without pycuda'
            )
        lib_so_path = os.path.join(os.path.dirname(__file__), '_C.so')
        if not os.path.isfile(lib_so_path):
            raise ValueError(
                f'Cannot find {lib_so_path} - is '
                'the sl_camera_sensor package installed?'
            )
        self.handle = ctypes.CDLL(lib_so_path, mode=ctypes.RTLD_GLOBAL)
        self.engine_path = engine_path
        
    def _preprocess_trt(self, im, means, stds):
        im = im.resize((self.input_shape[3], self.input_shape[2]), Image.LANCZOS)
        im = np.array(im).astype(np.float32)
        mean = np.array(means, dtype=np.float32)
        std = np.array(stds, dtype=np.float32)
        im = (im / 255.0 - mean) / std
        im = im.transpose(2, 0, 1) # HWC -> CHW
        return im

    def _postprocess_trt(self):
        """ Convert these to tensorflow outputs lol """
        # Reshape and normalize boxes
        self.boxes = self.boxes.reshape(-1, 4)
        sy, sx = self.input_shape[2:]
        self.boxes /= np.array((sx, sy, sx, sy))
        return {
            'detection_scores': self.scores.astype(np.float16),
            'detection_classes': self.classes.astype(np.int),
            'detection_boxes': self.boxes[..., [1,0,3,2]].astype(np.float16),
            'num_detections': len(self.scores)
        }
        
    def predict_trt(
            self,
            image_list: List[PIL.Image.Image],
            means: List,
            stds: List
    ) -> dict:
        """ The input should be a list of PIL Image instances. 
        This is set up to push the local inference context
        to the global GPU context in the event you want to run
        this on multiple streams.
        """
        if self.context is None:
            self.global_context = cuda.Device(0).make_context()
            self._load_engine_trt(self.engine_path)
            self._create_context()
            self.stream = cuda.Stream()
            
        self.global_context.push()
        preds_list = []
        for im in image_list:
            im = self._preprocess_trt(im, means, stds)
            img = cuda.register_host_memory(np.ascontiguousarray(im.ravel()))
            cuda.memcpy_htod_async(self.d_input, img, self.stream)
            self.__predict()
            preds_list.append(self._postprocess_trt())
        self.global_context.pop()
        
        return preds_list
    
    def __predict(self):
        # Run inference
        self.context.execute_async_v2(
            bindings=[int(self.d_input), int(self.d_scores),
                      int(self.d_boxes), int(self.d_classes)],
            stream_handle=self.stream.handle,
        )
        self.stream.synchronize()
        
        # Get back the bounding boxes to self pointers
        cuda.memcpy_dtoh_async(self.scores, self.d_scores, self.stream)
        cuda.memcpy_dtoh_async(self.boxes, self.d_boxes, self.stream)
        cuda.memcpy_dtoh_async(self.classes, self.d_classes, self.stream)
        self.stream.synchronize()
        
    def _load_engine_trt(self, engine_path):
        """ Loads a .engine or .plan, generates an 
        execution context, allocates memory buffers.
        """
        self.trt_runtime = trt.Runtime(TRT_LOGGER)
        with open(engine_path, 'rb') as f:
            engine_data = f.read()
            self.engine = self.trt_runtime.deserialize_cuda_engine(engine_data)

        self.engine_batch_size = self.engine.max_batch_size
        log.info(
            'Loaded cached TensorRT '
            f'engine: {os.path.basename(engine_path)}, '
            f'(batch={self.engine_batch_size})'
        )
        
    def _create_context(self):
        # Create an execution context
        self.context = self.engine.create_execution_context()
        self.context.active_optimization_profile = 0
        shape = self.engine.get_binding_shape(0)
        self.input_shape = (1, shape[1], shape[2], shape[3])
        self.context.set_binding_shape(0, self.input_shape)
        assert self.context.all_binding_shapes_specified

        # Create memory Buffers
        input_nbytes = trt.volume(self.input_shape) * trt.int32.itemsize
        self.d_input = cuda.mem_alloc(input_nbytes)

        # Create device output buffers
        num_det = self.context.get_binding_shape(1)[1]
        self.scores = cuda.pagelocked_empty(num_det, dtype=np.float32)
        self.boxes = cuda.pagelocked_empty(num_det * 4, dtype=np.float32)
        self.classes = cuda.pagelocked_empty(num_det, dtype=np.float32)
        self.d_scores = cuda.mem_alloc(self.scores.nbytes)
        self.d_boxes = cuda.mem_alloc(self.boxes.nbytes)
        self.d_classes = cuda.mem_alloc(self.classes.nbytes)

    def destroy(self):
        self.global_context.pop()
        del self.global_context
