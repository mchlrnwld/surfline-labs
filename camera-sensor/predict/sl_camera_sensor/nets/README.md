<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Neural Network & Decision Tree Inference](#neural-network--decision-tree-inference)
  - [Inference API](#inference-api)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Neural Network & Decision Tree Inference

## Inference API
Each directory has a single class & test, where each class has a `.predict()` method
that takes a single or list of unit8 RGB frames or tensors. Each inference class should
inherit from `./model_base.py`.