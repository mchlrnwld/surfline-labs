# Visibility Model Predictions
We have a decision tree model (XGB) that was trained with angular power spectrum features + some RGB statistics
that can estimate to a pretty high accuracy the percents of these 4 classes: [`Clear`, `Glare`, `Fog`, `Blur`].
It only needs to be run on a single frame every 5 or 10 minutes.

```python
vis_model = sl_camera_sensor.nets.visibility.visibility_model.VisibilityModel()
vis_predictions = vis_model.predict(frame)
```
where `frame` is a RGB unit8 frame (or list of those) and the outputs are a list of probabilities.
For example if you get back
```python
print(vis_predicitons)
array([[4.314e-03, 2.639e-04, 9.951e-01, 4.542e-04]], dtype=float16)
```
then there is a 99.5% chance your frame is foggy. We should store the entire array, since taking
the argmax class doesn't tell us how much of the other classes are present. Usually it's a mix
of the first two elements (`Clear` and `Glare`). This is the nature of class labeling on what is
actually better suited as a regression problem. 

The model class depends on `spec2d.py` to extract features prior to passing through the decision tree.
It loads xgb_clf_crossval0.model by default if no `model_name` is set in the class constructor, and assumes
that `.model` file is in the `$GRAPHS` env directory.