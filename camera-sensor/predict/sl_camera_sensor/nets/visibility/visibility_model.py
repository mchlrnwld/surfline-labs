# Combined 4-class model: clear, glare, fog, rain
#
import PIL
import numpy as np
import xgboost as xgb
from typing import Union, List

from skimage import filters
from PIL import Image
from sl_camera_sensor.nets.model_base import ModelBase
from .spec2d import Spec2D


class VisibilityModel(ModelBase):
    """ 
    Compute 2d power spectrum + RGB hist + blur as features, pass to 
    pretrained XGB classifier, and get back the probabilities for each
    class, ordered as [clear, glare, fog, rain].

    In principle this only needs to be run periodically on a single frame.
    """

    def __init__(self, model_name: str = "xgb_clf_crossval0.model", batch_size=1, nbins: int = 10,
                 target_size=(576, 1024)):
        # Notion of batch size here is essentially meaningless, just being consistent
        # with nn model inference to ctor signature is similar.
        super().__init__()
        self.model, self.label_map = self.load_model("visibility", model_name)
        self._spec = Spec2D(nbins=nbins, target_size=target_size)

    @staticmethod
    def get_label_map():
        return ModelBase._load_label_map("visibility")

    @staticmethod
    def get_map_label():
        return {v: k for k, v in ModelBase._load_label_map("visibility").items()}

    def preprocess(
        self,
        frames: Union[
            np.ndarray,
            List[np.ndarray],
            str,
            List[str],
            PIL.Image.Image,
            List[PIL.Image.Image],
        ],
    ) -> np.ndarray:
        """ 
        Input a RGB frame or list of frames and get back
        visibility features, which are fourier statistics and basic
        RGB, HSV and edge statstics. This will cast your data to unit8.
        
        :param frame: RGB frame (NOT BGR!!)
        :returns: numpy array of features. nbins is defined in ctor 
        for power spectra computation (7 is good). Should be the
        same as model was trained with.
        """
        if isinstance(frames, str):
            rgb = Image.open(frames)
        elif isinstance(frames, np.ndarray):
            if frames.shape[-1] != 3:
                raise ValueError(
                    "Input a RGB tensor; incorrect shape: "
                    f"{frames.shape}. If you've precomputed the "
                    "features just don't do that"
                )
            rgb = Image.fromarray(frames.astype(np.uint8))
        elif isinstance(frames, PIL.Image.Image) or hasattr(frames, "alpha_composite"):
            rgb = frames
        elif isinstance(frames, list):
            return [self.preprocess(f) for f in frames]
        else:
            raise ValueError(
                "Input a np array or string " f"path or PIL Image, not {type(frames)}"
            )
        _, Cl = self._spec.get_power_spec(rgb, rm_monopole=False, rgb=True)
        nprgb = np.array(rgb)
        hists = (
            np.hstack([np.histogram(nprgb[..., i].flatten())[0] for i in range(3)])
            / nprgb.size
            * 100.0
        )
        feature = np.hstack(
            (Cl.flatten(), hists, filters.laplace(nprgb).var() * 10)  # blur
        ).astype(np.float16)
        return feature[np.newaxis]

    def predict(
        self,
        frames: Union[
            np.ndarray,
            List[np.ndarray],
            str,
            List[str],
            PIL.Image.Image,
            List[PIL.Image.Image],
        ],
    ) -> np.ndarray:
        """ Pass in a list or single frame and get back a 4-element array for 
        each frame denoting the probability of each class.
        """
        features = self.preprocess(frames)
        preds = self.model.predict(xgb.DMatrix(np.vstack(features)))
        assert len(preds) == len(features) == len(preds)
        return preds.astype(np.float16)
