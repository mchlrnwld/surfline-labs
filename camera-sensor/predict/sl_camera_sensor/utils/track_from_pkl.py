import os
import datetime
import pickle
import numpy as np

from sl_camera_sensor.lib.crowds.clip_above_thresh import clip_above_thresh
from sl_camera_sensor.lib.wave_tracking.wave_tracker_iou import WaveTracker
from sl_camera_sensor.lib.wave_tracking.surfer_tracker import SurferTracker
from sl_camera_sensor.lib.localization.localize_dets import LocalizeDets
from sl_camera_sensor.nets.segmentation.segmentation_model import SegmentationModel
from sl_camera_sensor.nets.detection.detection_model import DetectionModel
from sl_camera_sensor.utils.pkl_iter import PklIter
from sl_camera_sensor.utils import video_utils as vu
from sl_camera_sensor.utils import logger

from ipdb import set_trace
log = logger.getLogger(__name__)


def track_from_pkl(seg_pkl, det_thresh=0.22, target_size=(720, 1280)):
    """ Given a pickle name of the segmention model outputs, 
    run the wave tracker, and if there's a detection pkl of 
    a similar name, run surfer trackin too
    
    Args:
        - seg_pkl: string path to segmentation model outputs, batched.
    Returns:
        - writes wavetrack and possible surfertrack files to disk, next
          to the seg_pkl.
    """
    mask_iter = PklIter(seg_pkl)    
    det_pkl = seg_pkl.replace('masks','detections')
    has_dets = os.path.isfile(det_pkl)
    dets_iter = PklIter(det_pkl) if has_dets else None
    try:
        fps = float('.'.join(seg_pkl.split('fps')[0].split('.')[-2:]))
        if fps == 0:
            raise ValueError
    except:
        raise ValueError(f'Unable to extract fps from file name: {seg_pkl}')

    # Initialize trackers & localizer
    localizer = LocalizeDets(
        dets_label_map=DetectionModel.get_label_map(),
        mask_label_map=SegmentationModel.get_label_map(),
        target_size=target_size
    )

    wave_tracker = WaveTracker(
        fps, SegmentationModel.get_label_map(), target_size=target_size
    )
    # For homography stuff we need to set an alias, but this can be omitted
    # for later on, since we're storing the pockets and slices in
    # the wavetrack files. tracker class needs to be modified if we omit here.
    wave_tracker.alias = vu.get_alias(seg_pkl)
    set_trace()
    surfer_tracker = SurferTracker(
        fps, label_map=SegmentationModel.get_label_map(), target_size=target_size,
        store_crops=False
    )

    for batch_cnt, masks in enumerate(mask_iter):
        if masks is None:
            break
        batch_size = len(masks)
        if has_dets:
            dets = next(dets_iter)
            dets = clip_above_thresh(dets, det_thresh)
            dets = localizer(dets, masks[0])
            timestamps = [
                datetime.datetime.utcfromtimestamp(det['timestamp']) for det in dets]
        else:
            dets = [[None] * batch_size]
            timestamps = [
                datetime.datetime.utcfromtimestamp(it)
                for it in np.arange(batch_cnt * batch_size, batch_cnt * batch_size + batch_size)
            ]
            
        for mask, det, timestamp in zip(masks, dets, timestamps):
            # Pass detections along with masks
            waves = wave_tracker(
                mask,
                dets=det,
                dets_label_map=DetectionModel.get_label_map(),
                timestamp=timestamp,
                return_contours=True
            )
            # Pass masks along with instantaneous wave tracker dataframe, which has
            # the surfer loc info in there from the call above, assuming dets is not None.
            live_surfers = surfer_tracker(
                WaveTracker.blobs_to_df(waves),
                mask=mask,
                bin_masks=wave_tracker.get_binary_masks(),
                return_surfers=True
            )

    # Save everything to disk
    wt_savename = seg_pkl.replace('masks', 'wavetracks').replace('.pkl','.txt')
    st_savename = seg_pkl.replace('masks','surfertracks').replace('.csv','.pkl')
    wave_tracks = wave_tracker.pop_tracks(return_blobs=False, done=True)
    if len(wave_tracks) > 0:
        wave_tracks.to_csv(wt_savename, index=False)
        log.info(f'Wrote {len(wave_tracks)} wave tracks to {wt_savename}')
    else:
        log.info("Wave tracks are length zero")
        
    if has_dets:
        try:
            surfer_tracks = surfer_tracker.pop_tracks(done=True)
        except Exception as E:
            log.critical('cannot pop surfer tracks')
            print(E)
            
        if len(surfer_tracks) > 0:
            with open(st_savename, 'wb') as f:
                pickle.dump(surfer_tracks, f)
            log.info(f'Wrote {len(surfer_tracks)} surfer tracks to {st_savename}')
        else:
            log.info('Surfer tracks are length zero')
