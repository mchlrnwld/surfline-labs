# I'm often saving raw detection and segmentation masks in batches,
# to avoid accumulating them all over the course of an entire rewind
# and filling memory. This loads those pickle files that were stored
# in batches, by looping over the pickle until it's empty.
#
import os
import pickle
from sl_camera_sensor.utils.logger import getLogger

log = getLogger(__name__)


class PklIter:
    def __init__(self, save_name):
        self.save_name = save_name
        if not os.path.isfile(save_name):
            raise ValueError(f"input pkl doesn't exist: {save_name}")
        self._batches = self.__gen_batches()
        self.batch_cnt = 0
        self.batch_size = None

    def __next__(self):
        try:
            return next(self._batches)
        except StopIteration:
            log.info(
                f"PklIter exhausted for "
                f"{self.save_name} after {self.batch_cnt} batches"
            )

    def __iter__(self):
        return self

    def get_batch(self):
        return next(self)

    def __gen_batches(self):
        with open(self.save_name, "rb") as f:
            while True:
                try:
                    dat = pickle.load(f)
                    # We can infer the batch size
                    if self.batch_size is None:
                        self.batch_size = len(dat)
                    self.batch_cnt += 1
                    yield dat
                except EOFError:
                    self.exhausted = True
                    break
