# This is similar to labs_utils.spots except it only uses the spots-api url which
# is depoyable in prod.
#
import json
import urllib
from urllib.error import HTTPError


def query_url(url: str) -> dict:
    try:
        with urllib.request.urlopen(url) as response:
            data = json.loads(response.read())
        return data
    except HTTPError as e:
        print(f'Bad request for url: {url}')
        print(e)
    return []


def query_spots_api_with_spotid(spotid: str) -> dict:
    """
    Query the spots API to get info on a spot from a spot ID. For
    example you can get the list of cam IDs at that spot with
    the 'cams' key. I don't know who made this API but it's magical.
    """
    url = f"http://spots-api.surfline.com/admin/spots/{spotid}"
    return query_url(url)


def get_camid_from_alias(alias: str) -> str:
    """wc-hbpierns -> "5842041f4e65fad6a7708827'"""
    url = (
        f'http://cameras-service.prod.surfline.com/'
        f'cameras/ValidateAlias?alias={alias}'
    )
    dat = query_url(url)
    if len(dat) > 0:
        return dat['_id']
    else:
        print(f'Not a valid alias: {alias}')


def query_spots_api_with_camid(camid: str) -> dict:
    url = (
        "http://spots-api.prod.surfline.com/admin/spots/"
        f"?select=timezone,name,cams&cams={camid}"
    )
    return query_url(url)


def get_spotid(camid: str) -> str:
    """Input a cam ID and get back the spot ID for that cam."""
    if '-' in camid:
        if camid.startswith('mx-puerto'):
            # TODO why doesn't this work with mx-puertocloseup?
            return '5842041f4e65fad6a7708b43'
        camid = get_camid_from_alias(camid)
    spot_data = query_spots_api_with_camid(camid)
    return spot_data[0]['_id'] if len(spot_data) > 0 else None
