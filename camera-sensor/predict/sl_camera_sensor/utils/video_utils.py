# -*- coding: utf-8 -*-
# Various utilities for neural network pre- and
# post-processing of cam rewinds.
#
import calendar
import cv2
import datetime
import numpy as np
import os
import time
import subprocess
import pickle

from shutil import copyfile
from statistics import mean
from ipdb import set_trace
from sl_camera_sensor.utils import logger, timer

log = logger.getLogger(__name__)


def get_alias(rewind_name):
    name = os.path.basename(rewind_name)
    return name.split('.')[0]

def get_frame_batch(
    input_video,
    video_start_timestamp,
    batch_size,
    at_timestamps=None,
    max_frames=None,
    unix_time=True,
    fps=None):
    """ 10 minutes of video is ~64g of ram in memory.
    This generator will return batches of frames,
    since we might not have enough memory to process
    a whole movie in a single pass.
    :param video_start_timestamp: time in seconds that the video begins.
    Timestamps will be offset by this value, otherwise they'll start at 0+fps.
    :at_timestamps: floating point input array where only frames at these
    timestamps will be returned.
    :returns: frames, timestamps tuple of lists.
    """
    if at_timestamps:
        raise NotImplementedError("at_timestamps not implemented here.")

    if not os.path.isfile(input_video):
        raise ValueError("Input video '{}' does not exist.".format(input_video))

    max_frames = max_frames or 20000

    cap = cv2.VideoCapture(input_video)
    native_fps = cap.get(cv2.CAP_PROP_FPS)
    log.debug(
        "{0} frames in {1} at {2:.3f} fps".format(
            int(cap.get(cv2.CAP_PROP_FRAME_COUNT)), input_video, native_fps
        )
    )
    if not fps:
        fps = native_fps

    if not at_timestamps:
        log.debug(
            "Extracting up to {0} frames in batches of "
            "{1} at ~{2:.3f} fps.".format(max_frames, batch_size, fps)
        )
    else:
        if fps != native_fps:
            raise ValueError(
                "Functionality to input both output fps and"
                " at_timestamps is not supported."
            )
    if fps == 0 or native_fps == 0:
        return False, False
    cnt = 0
    frames, timestamps = [], []
    timestamp = (cap.get(cv2.CAP_PROP_POS_MSEC) / 1000.0) + video_start_timestamp
    flag, frame = cap.read()  # Put this after timestamp calc!
    # start_time = video_start_timestamp if unix_time else 0.
    if fps > native_fps * 1.1:
        log.warning("Requested input fps is " "higher than native ({native_fps}fps).")
    while flag:
        if (cnt > 0) and (len(timestamps) == batch_size):  # (cnt % batch_size) == 0):
            yield frames, timestamps
            frames, timestamps = [], []

        if cnt > max_frames - 1:
            break

        if cnt % round(native_fps / float(fps)) == 0:

            # if frame.shape != (720, 1280, 3):
            #    frame =
            frames.append(bgr2rgb(frame))
            timestamps.append(timestamp)

        timestamp = (cap.get(cv2.CAP_PROP_POS_MSEC) / 1000.0) + video_start_timestamp
        flag, frame = cap.read()
        cnt += 1
        # time.sleep(1./30 * batch_size)  # Emulate a slow stream (15fps)
    yield frames, timestamps

def run_batch_inference(
        input_video,
        video_start_timestamp,
        detection_bot,
        save_name=None,
        batch_size=2,
        max_frames=20000,
        fps=1,
        keyframes=False,
):
    """ Calls the generator above and passes each batch
    through the detection_bot, and returns detections.

    :param input_video: string of input video name
    :param detection_bot: DetectionModel instance.
    :param save_name: write to a pickle for each batch. 
    Can't hold a full video's frames in disk?
    :param batch_size: batch size set in cam_config.yaml.
    :returns: detection dictionaries with temporal component added.
    If check_poor_light is set in the detection_config, then
    -1 will be returned instead of detection dicts in the
    event bad lighting is detected.
    """
    # Do we need to instantiate 'detection_bot':
    if isinstance(detection_bot, tuple):
        model_name, batch_size, target_size, model = detection_bot
        detection_bot = model(
            model_name=model_name,
            batch_size=batch_size,
            target_size=target_size
        )
    pkl_file = open(save_name, 'wb') if save_name is not None else None
    
    if not batch_size:
        batch_size = detection_bot.batch_size
    t0 = time.time()
    detections, timestamps = [], []

    if not keyframes:
        batches = get_frame_batch(
            input_video,
            video_start_timestamp,
            batch_size,
            max_frames=max_frames,
            fps=fps,
        )
    else:
        log.info("Calling get_keyframes without batch_size.")
        batches = get_keyframes(input_video, video_start_timestamp)

    # Benchmarking
    start_frame_extraction = timer.start()
    frame_extraction_times = []
    predict_times = []

    for batch_frames, batch_timestamps in batches:
        # This happens when batch_size % max_frames == 0
        if len(batch_frames) == 0:
            break

        frame_checkpoint = timer.checkpoint(
            "Frame extration for batch took:", start_frame_extraction, silent=True
        )
        frame_extraction_times.append(frame_checkpoint)

        start_predict = timer.start()

        batch_detections = detection_bot.predict(batch_frames)

        predict_checkpoint = timer.checkpoint(
            "Predict for batch took:", start_predict, silent=True
        )
        predict_times.append(predict_checkpoint)

        # Write each batch to a pickle file
        if pkl_file is not None:
            if type(detection_bot).__name__ == 'DetectionModel':
                # Store the timestamp info in the dets dicts
                for i in range(len(batch_detections)):
                    batch_detections[i]['timestamp'] = batch_timestamps[i]
            pickle.dump(batch_detections, pkl_file)
        else:
            detections += batch_detections
            timestamps += batch_timestamps
        
        start_frame_extraction = timer.start()

    avg_frame_extration_time = mean(frame_extraction_times)
    avg_predict_time = mean(predict_times)
    log.info(
        "Avg frame extration time: %1.2fs (%2.2fms)."
        % (avg_frame_extration_time, avg_frame_extration_time * 1000)
    )
    log.info(
        "Avg predict time: %1.2fs (%2.2fms) (%3.1f fps)."
        % (avg_predict_time, avg_predict_time * 1000, float(batch_size)/avg_predict_time)
    )

    # Add time component to detection dictionaries.
    if pkl_file is not None:
        pkl_file.close()
    else:
        for i in range(len(detections)):
            detections[i]["timestamp"] = timestamps[i]
            
    log.info(
        "Frame extraction and inference took {0:.0f}m {1:.0f}s ({2})".format(
            *divmod(time.time() - t0, 60), type(detection_bot).__name__
        )
    )
    # TensorRT threading context cleanup
    #if hasattr(detection_bot, 'destroy'):
    #    set_trace()
    #    detection_bot.destroy()
        
    del batches
    return detections

def get_fps(video_file):
    """
    Return fps from input video file string.

    :param video_file: string name of video file you want to inspect.
    :return: float of fps.
    """
    return cv2.VideoCapture(video_file).get(cv2.CAP_PROP_FPS)

def bgr2rgb(frame):
    """ OpenCV convention is BGR, but network wants RGB.
    :param frame: BGR numpy array
    :return: RBG numpy array.
    """
    return cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

def rgb2bgr(frame):
    """ Roll color axes from RGB order to BGR OpenCV convention.

    :param frame: RGB numpy array
    :return: RGB numpy array.
    """
    return cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)

def sec2min(seconds):
    """
    Helper function to get minutes, seconds from seconds float.
    :param seconds: float input, e.g 124
    :return: (min, sec) tuple, e.g. 2, 4
    """
    m, s = divmod(seconds, 60)
    return m, s

def get_todays_timestamp():
    """
    Get todays date and returna a suitable string.
    :return: today's date str
    """
    return "{date:%Y-%m-%d}".format(date=datetime.datetime.now())

def get_vid_prefix(input_video):
    """ Remove preceeding dir and video extension """

    # Prefix has been added to the cam names - remove them
    pfx = os.path.basename(input_video).replace(".mp4", "").replace(u'\xa0', '')

    if "-" in pfx:
        if pfx.count("-") > 1:
            raise ValueError(
                "Input video naming convention is not supported. "
                "({})".format(input_video)
            )
        #pfx = pfx.rsplit("-")[-1]

    return pfx

def get_verbose_vid_prefix(input_video):
    """ Remove preceeding dir and keep the regional prefix +'-'
    string.
    """
    return os.path.basename(input_video).replace(".mp4", "")

def get_cam_name(input_video):
    """ Presuming naming convention begins with cam_alias """
    return get_vid_prefix(input_video).split(".")[0].replace("cam", "")

def get_verbose_cam_name(input_video):
    return get_verbose_vid_prefix(input_video).split(".")[0].replace("cam", "")

def get_vid_timestamp(input_video):
    """ Return UNIX start time from video name.
    Works with old and new format (with/without milliseconds).
    """
    txt = None
    for ele in get_vid_prefix(input_video).split(".")[::-1]:
        if ele[0].isdigit():
            txt = ele
    if txt is None:
        raise ValueError("Can't parse input: {}".format(input_video))

    # Parse string based on new or old format (with/without millisec).
    fmt = "%Y%m%dT%H%M%S%f" if len(txt) == 18 else "%Y%m%dT%H%M%S"
    start_time = datetime.datetime.strptime(txt, fmt)
    # Calendar assumes UTC in timetuple.
    unix_time = calendar.timegm(start_time.timetuple())
    return unix_time

def encode_h264(input_video, cuda_ffmpeg=None, keyint=3):
    """ H.264 encoding (with cuda?)
    :param input_video: string name of video you want to encode.
    :param cuda_ffmpeg: path to executable ffmpeg command. Compile
    with cuda flags to increase speed by a lot.
    :param keyint: key frames integer. If you want a key frame every 3 seconds
    then just set this to 3 - the code will figure out the correct
    integer here (e.g. if the input video is 30 fps then keyint=90 in
    the ffmpeg call).
    """

    if not os.path.isfile(input_video):
        raise ValueError("Specified input video does not exist {}".format(input_video))

    if not cuda_ffmpeg or not os.path.isfile(cuda_ffmpeg):
        cuda_ffmpeg = "ffmpeg"
        h264_flag = "h264"
        log.info("Encoding with H.264...")
        log.info(
            "Set path to CUDA compiled ffmpeg to considerably "
            "speed up h264 encoding..."
        )
    else:
        h264_flag = "h264_nvenc"
        log.info("Encoding with H.264 (CUDA)...")

    keyint = int(keyint * get_fps(input_video)) // 2
    log.info("Setting keyint_min={}".format(keyint))

    codec_video = input_video.replace(".mp4", ".codec.mp4")
    copyfile(input_video, codec_video)

    os.system(
        "{0} -y -i {4} -vcodec {1} "
        '-x264-params "keyint={2}:min-keyint={2}" '
        # '-g {2} -keyint_min {2} -force_key_frames '
        "{3} -loglevel panic".format(
            #'{3}'.format(
            cuda_ffmpeg,
            h264_flag,
            keyint,
            input_video,
            codec_video,
        )
    )

    # Input video -> output video
    if os.stat(input_video).st_size == 0 or not os.path.isfile(input_video):
        raise ValueError("The output H264 video is nothing!!!")
    os.system(f"rm {codec_video}")
