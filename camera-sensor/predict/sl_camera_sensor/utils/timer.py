# -*- coding: utf-8 -*-
import time
from sl_camera_sensor.utils import logger

log = logger.getLogger(__name__)

def start():
  return time.time()

def checkpoint(name, start, silent=False):
  end = time.time()

  if not silent:
    log.info('%s %2.2fs (%3.2fms)' % (name, (end - start), (end - start) * 1000))

  return (end - start)
