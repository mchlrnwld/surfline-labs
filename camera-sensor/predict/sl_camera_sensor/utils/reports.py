# This is similar to labs_utils.reports except it only uses the spots-api url which
# is depoyable in prod. This doesn't query the spotReportViews as we can't run
# that in prod, as I understand it. So no human reports here atm.
#
import datetime

import numpy as np
import requests
from scipy.interpolate import interp1d

from sl_camera_sensor.utils import spots
from sl_camera_sensor.utils.logger import getLogger

log = getLogger(__name__)


def get_tide(spotid: str, date: datetime.datetime) -> list:
    """
    Get the historical tide of a spot at some timestamp. This varies
    from the version we have running in the surfline-labs repo as it uses
    the tide-service.prod.surfline.com api vs the spotReportViews.json data.

    Args:
        - spotid: 24 char string spotID
        - date: datatime timestamp of the exact time you want. This will interpolate.

    Returns:
        Tide level in feet. If the nearest returned tide timestamp
        is larger than one hour away from the requested timestamp,
        this tries to interpolate given the tide levels +/- 6 hours about input date.
    """
    start = int(date.timestamp()) - 21600
    end = int(date.timestamp()) + 21600
    try:
        port = spots.query_spots_api_with_spotid(spotid)[
            'tideStation'
        ].replace(' ', '%20')
    except KeyError:
        # Instead of raising an error and killing everything, try latlon request,
        # or just continue with the wrong tide.
        log.info(f'spot {spotid} has no tideStation. Trying with lat,lon.')
        lon, lat = spots.query_spots_api_with_spotid(spotid)['location'][
            'coordinates'
        ]
        tides_resp = requests.get(
            f'http://tide-service.prod.surfline.com/data?'
            f'lat={lat}&lon={lon}&start={start}&end={end}'
        ).json()
        if 'error:' in tides_resp:
            log.info(
                "lat,lon request for spot {spotid} didn't work either."
                "Returning a tide of 0, which is wrong."
            )
            return 0
    else:
        tides_resp = requests.get(
            f'http://tide-service.prod.surfline.com/data?'
            f'port={port}&start={start}&end={end}'
        ).json()
    if len(tides_resp['levels']) < 2:
        log.info(
            'Number of returned tides in 12hr period '
            'is too low to interpolate. Returning 0'
        )
        return 0

    times, heights = zip(
        *[(t['time'], t['shift']) for t in tides_resp['levels']]
    )
    try:
        assert np.min(times) <= date.timestamp() <= max(times)
    except AssertionError:
        # In this case, try to interpolate. We're given previous, current
        # and next keys in the tide attribute.
        log.info(
            f"Returned tides are out of range of input date: {date}. Returning 0"
        )
        return 0

    return float(
        interp1d(times, heights, fill_value='extrapolate')(date.timestamp())
    )
