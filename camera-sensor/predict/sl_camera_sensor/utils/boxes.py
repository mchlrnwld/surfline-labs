# Utility functions for drawing bounding boxes on PIL images. These
# first two functions were taken from the Nvidia samples.
#
import numpy as np
import PIL.ImageDraw as ImageDraw
import PIL.ImageFont as ImageFont
import PIL.ImageColor as ImageColor
from PIL import Image
from ipdb import set_trace


def STANDARD_COLORS():
    return [
        'AliceBlue', 'DodgerBlue', 'Salmon',
        'SpringGreen', 'Violet', 'Red', 'Pink',
        'GoldenRod', 'Wheat', 'Purple', 'PaleGreen',
        'SeaShell', 'Lime', 'Orange'
    ]


def draw_bounding_boxes_on_image(image,
                                 boxes,
                                 color=(255, 0, 0),
                                 thickness=5,
                                 display_str_list=()):
    """Draws bounding boxes on image.

    Args:
        image (PIL.Image): PIL.Image object
        boxes (np.array): a 2 dimensional numpy array
            of [N, 4]: (ymin, xmin, ymax, xmax)
            The coordinates are in normalized format between [0, 1]
        color (int, int, int): RGB tuple describing color to draw bounding box
        thickness (int): bounding box line thickness
        display_str_list [str]: list of strings.
            Contains one string for each bounding box.
    Raises:
        ValueError: if boxes is not a [N, 4] array
    """
    boxes_shape = boxes.shape
    if not boxes_shape:
        return
    if len(boxes_shape) != 2 or boxes_shape[1] != 4:
        raise ValueError('boxes must be of size [N, 4]')
    for i in range(boxes_shape[0]):
        draw_bounding_box_on_image(image, boxes[i, 0], boxes[i, 1], boxes[i, 2],
            boxes[i, 3], color, thickness, display_str_list[i])

def draw_bounding_box_on_image(image,
                               ymin,
                               xmin,
                               ymax,
                               xmax,
                               color=(255, 0, 0),
                               thickness=4,
                               display_str='',
                               use_normalized_coordinates=True):
    """Adds a bounding box to an image.

    Bounding box coordinates can be specified in either absolute (pixel) or
    normalized coordinates by setting the use_normalized_coordinates argument.

    The string passed in display_str is displayed above the
    bounding box in black text on a rectangle filled with the input 'color'.
    If the top of the bounding box extends to the edge of the image, the string
    is displayed below the bounding box.

    Args:
        image (PIL.Image): PIL.Image object
        ymin (float): ymin of bounding box
        xmin (float): xmin of bounding box
        ymax (float): ymax of bounding box
        xmax (float): xmax of bounding box
        color (int, int, int): RGB tuple describing color to draw bounding box
        thickness (int): line thickness
        display_str (str): string to display in box
        use_normalized_coordinates (bool): If True, treat coordinates
            ymin, xmin, ymax, xmax as relative to the image. Otherwise treat
            coordinates as absolute
    """
    fill = tuple(color) if not isinstance(color, str) else color
    draw = ImageDraw.Draw(image)
    im_width, im_height = image.size
    if use_normalized_coordinates:
        (left, right, top, bottom) = (xmin * im_width, xmax * im_width,
                                      ymin * im_height, ymax * im_height)
    else:
        (left, right, top, bottom) = (xmin, xmax, ymin, ymax)
    draw.line([(left, top), (left, bottom), (right, bottom),
               (right, top), (left, top)], width=thickness, fill=fill)

    if display_str == '':
        return
    try:
        font = ImageFont.truetype('arial.ttf', 24)
    except IOError:
        font = ImageFont.load_default()
    
    # If the total height of the display string added to the top of the bounding
    # box exceeds the top of the image, move the string below the bounding box
    # instead of above
    display_str_height = font.getsize(display_str)[1]
    # Each display_str has a top and bottom margin of 0.05x
    total_display_str_height = (1 + 2 * 0.05) * display_str_height
    
    if top > total_display_str_height:
        text_bottom = top
    else:
        text_bottom = bottom + total_display_str_height
    
    text_width, text_height = font.getsize(display_str)
    margin = np.ceil(0.05 * text_height)
    draw.rectangle(
        [(left, text_bottom - text_height - 2 * margin), (left + text_width,
            text_bottom)],
        fill=fill)
    
    draw.text(
        (left + margin, text_bottom - text_height - margin),
        display_str,
        fill='black',
        font=font)
    text_bottom -= text_height - 2 * margin

    
def label_frames_from_dict(frames, detections,
                           detection_thresh=0.3,
                           classes=None,
                           skip_scores=False, alpha=True,
                           colors=None, return_pil=False):
    """ Annotate bounding boxes on a list of frames using a list 
    of detection dictionaries from a detection robot.
    
    :param frames: list or tensor of input image frames, NWHC
    :param detections: list of detection dictionaries, with the 
    same shape in the first axis as frames
    :param detection_thresh: don't show any boxes below this threshold
    :param skip_scores: Set to omit the confidence percentage above the 
    boxes. This functionality might broken at the moment.
    :returns: list of annotated frames
    """
    if isinstance(frames, np.ndarray) or isinstance(frames, list):
        if isinstance(frames, np.ndarray):
            if frames.shape[0] < 4:
                if frames.shape[0] == 3:
                    frames = frames[np.newaxis, ...]
                else:
                    raise ValueError(
                        f"Input frames shape {frames.shape} doesn't "
                        "make sense.")
        pil_frames = [Image.fromarray(f).convert('RGBA') for f in frames]
        #else:
        #     pil_frames = frames
    else:
        raise ValueError('Input a list or array of frames')
    alphas = np.linspace(0, 127, len(frames), dtype=np.uint8)
    for det, pf, aph in zip(detections, pil_frames, alphas):
        if len(det) > 0 and 'detection_classes' in det:
            try:
                # Show all the classes if not input.
                if classes is None:
                    clses = np.unique(det['detection_classes'])
                else:
                    clses = classes
            except KeyError:
                raise KeyError("Detection dictionary doesn't have "
                               "'detection_classes' key.")
            except TypeError:
                log.critical('Something is wrong with your dets... not annotating bboxes')
                break
            
            for cls in clses:
                # Make color with alpha channel!!
                color = ImageColor.getrgb(STANDARD_COLORS()[cls])
                if alpha:
                    color += (aph,)
                try:
                    cls_idxs = np.where(
                        (det['detection_classes'] == cls)
                        & (det['detection_scores'] >= detection_thresh))[0]
                    boxes = det['detection_boxes'][cls_idxs]
                    scores = det['detection_scores'][cls_idxs]
                except KeyError:
                    raise KeyError("Detection dictionary doesn't have the "
                                   "correct keys.")
                if not skip_scores:
                    strs = ['{:.0f}%'.format(score * 100) for score in scores]
                else:
                    strs = ['' for _ in range(len(scores))]

                draw_bounding_boxes_on_image(
                    pf,
                    boxes,
                    color=color,
                    thickness=2,
                    display_str_list=strs
                )

    if return_pil:
        return pil_frames
    else:
        return [np.array(pf.convert('RGB')) for pf in pil_frames]


def _get_fade_frame(det, thresh=0.3, frame_size=(720, 1280, 3)):
    """ Get an RGBA PIL frame with boxes overlaid, so we can do a box fade.
    """
    if isinstance(det, list):
        det = det[-1]
    #bframe = Image.new('RGBA', frame_size[::-1], (0,0,0,0))
    bframe = np.zeros(frame_size).astype(np.uint8)
    bfade = label_frames_from_dict([bframe], [det], detection_thresh=thresh,
                                   return_pil=True)
    bfade = bfade[0].convert('RGBA')
    bfade.putalpha(0)
    return bfade
