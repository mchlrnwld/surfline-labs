# Simple method to determine if two frames are similar, using template matching.

import cv2
import os


def similarity(frame_a, frame_b):
    """
    Calculate the similarity between two frames.
    Args:
        frame_a (ndarray): First frame to be compared.
        frame_b (ndarray): Second frame to be compared.

    Returns:
        max_val (float): Measured similarity using cv2 functions.
    """
    res = cv2.matchTemplate(frame_a, frame_b, cv2.TM_CCOEFF_NORMED)
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)

    return max_val


def is_similar(frame_a, frame_b, threshold=0.5):
    """
    Check if two frames are similar based on a threshold.
    Args:
        frame_a: First frame to be compared.
        frame_b: Second frame to be compared.
        threshold: Threshold of similarity.

    Returns:
        bool: True if similar, False if not.
    """
    if similarity(frame_a, frame_b) > threshold:
        return True
    else:
        return False
