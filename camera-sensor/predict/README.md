# Surfline Camera Sensor Inference

This is an accelerated inference package, running object detection and segmentation
with TensorRT. You can also run other nonaccelerated graphs as listed below. The
executable is `slcs` and you can choose to run all the models, or individual ones.
At the moment this only writes the network predictions to disk, so annotations
haven't been integrated yet.

```bash
docker run --gpus all -v /data/:/data/ --ipc=host -it sl-camera-sensor
```

Inside of the docker container you can run `detect`, `segment`, `vis` or `stack` (all 3):
```bash
slcs detect -i /data/rewinds/wc-lowers.stream.20211102T143840016.mp4
```
which will output a detection dictionary pickle next to the rewind.

Run the full stack on a directory of rewinds:
```bash
slcs stack -i /data/path/to/rewinds/ --fps 1 --overwrite --det-model-name retinanet.plan --seg-model-name deeplabv3.ts
```

The full list of options:
```bash
slcs: Surfline Camera Sensor, [accelerated] inference

positional arguments:
  command               choose from: stack|detect|segment|vis

optional arguments:
  -h, --help            show this help message and exit
  -i INPUT, --input INPUT
                        Path to input rewind or directory containing multiple
                        rewinds
  --fps FPS             Infernce frame rate
  --batch BATCH, -b BATCH
                        batch size of your static models, they should be
                        consistent
  --det-model-name DET_MODEL_NAME, -dmn DET_MODEL_NAME
                        Detection model name. Can be TRT engine/plan, torch
                        script, or TRT-torchscript
  --det-target-size DET_TARGET_SIZE, -dts DET_TARGET_SIZE
                        This is model-dependent.
  --seg-model-name SEG_MODEL_NAME, -smn SEG_MODEL_NAME
                        segmentation model name
  --seg-target-size SEG_TARGET_SIZE, -sts SEG_TARGET_SIZE
                        This is model-dependent.
  --graphs-dir GRAPHS_DIR, -gd GRAPHS_DIR
                        Base directory where the above model graphs live.
  --overwrite, -o       overwrite existing neural network outputs
```

Below are the frozen model naming conventions
I've been following. All of your model files should live in `$GRAPHS_DIR` env, defaulted
to `/data/graphs/` on our server.

## Graph Types
• `.pb` - TensorFlow [static] graph. These may or may not be TRT optimized

• `.pt` - Torch jit script. Generally not frozen and may need to be wrapped in `with torch.no_grad()`.

• `.ts` - TRT optimized Torch Script. Inference flow is very similar to `.pt` files.

• `.model` - XG Boost model files.

• `.engine`, `.plan` - Accelerated TensorRT object detection graphs, that require
the TensorRT C++/cuda plugin bindings contained in this package.

• `.onnx` - ONNX files which are typically intermediary and not used for inference, although these are fast.
