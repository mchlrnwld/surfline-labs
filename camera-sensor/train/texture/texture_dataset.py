# Attempt number 25 at conditions approximation from frames.
#
import os
import cv2
import numpy as np
import pandas as pd
import torchvision.transforms as transforms

from PIL import Image
from torch.utils.data import Dataset
from sl_camera_sensor.utils.logger import getLogger

log = getLogger(__name__)


class TextureDataset(Dataset):
    def __init__(
        self,
        base_dir="/data/texture/",
        do_cropping=False,
        target_size=(576, 1024),
        masked=False,
        train=True,
        train_pct=0.86,
    ):
        """ 
        If do_cropping then raw stills are loaded and the segmentation model
        is run on each of them.
        """
        self.target_size = target_size
        self.model = None
        labels = []
        self.base_dir = (
            base_dir
            if "TEXTURE_TRAINING_DIR" not in os.environ
            else os.environ["TEXTURE_TRAINING_DIR"]
        )
        self.root_dirs = [os.path.join(base_dir, sd) for sd in os.listdir(base_dir)]
        for rd in self.root_dirs:
            for dp, _, fn in os.walk(rd):
                if os.path.isfile(os.path.join(dp, "labels.txt")):
                    idf = pd.read_csv(
                        os.path.join(dp, "labels.txt"), delimiter=" ", header=None
                    )
                    idf[0] = dp + "/" + idf[0]
                    labels.append(idf)
        self.df = pd.concat(labels)

        # Drop -1 labels
        pre = len(self.df)
        self.df = self.df[self.df[1] != -1]
        post = len(self.df)
        log.info(
            f"Removed {pre - post} undefined label entries ({(1-post/pre)*100:.1f}%)"
        )
        self.n_class = len(np.unique(self.df[1]))

        self.odir = os.path.join(self.base_dir, "masks")

        # Train a model with both masked and unmasked frames, to compare!
        self.masked = masked
        self.colname = "masked_still" if self.masked else "stills"
        self.train = train

        # Split into train & test.
        n_tot = len(self.df)
        np.linspace(0, n_tot - 1, n_tot).astype(int)
        # Shuffle
        self.df = self.df.sample(frac=1, random_state=19593)
        self.df.columns = ["image", "label"]

        ## gauss
        # self.df['label'] = self.df['label'] + np.random.normal(
        #    loc=0,scale=0.1, size=len(self.df['label'])
        # )
        # self.df['label'] = [np.max([0, x]) for x in self.df['label'].values]
        # print('Gaussainzed the labels')
        ##

        self.n_train = int(n_tot * train_pct)
        self.training = train
        if train:
            self.df = self.df[: self.n_train]
            txt = "train"
        else:
            self.df = self.df[self.n_train :]
            txt = "test"
        txtmask = "masked" if masked else "unmasked"
        log.info(
            f"Loading {len(self.df)} {txt} over {self.n_class} classes ({txtmask})"
        )

    def __len__(self):
        return len(self.df)

    def __getitem__(self, idx):
        row = self.df.iloc[idx]
        image = Image.open(row["image"])
        self.curr_im = row["image"]
        self.curr_label = int(row["label"])
        if self.target_size[::-1] != image.size:
            image = image.resize(self.target_size[::-1], Image.LANCZOS)
        return self.transforms()(image), self.curr_label

    def transforms(self):
        return transforms.Compose(
            [
                transforms.ToTensor(),
                transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
            ]
        )

    def crop_contour(self, contour, frame):
        """ 
        Given a contour and an RGB frame, return a new frame where
        everything outside the contour is zero.
        """
        mask = np.zeros(frame.shape, dtype=np.uint8)
        mask = cv2.fillPoly(mask, pts=[contour], color=(1, 1, 1))
        kernel = np.ones((5, 5), np.uint8)
        mask = cv2.erode(mask, kernel, iterations=2)
        return frame * mask
