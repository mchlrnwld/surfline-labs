# Surface Texture Model Training

This is a pretty classic PyTorch image classification training procedure, loading
frames and labels using a `DataSet` iterator, and backpropagating a shallow 2D conv classifier.

```bash
export TEXTURE_TRAINING_DIR=/data/whatever
chmod +x download_raw.sh; ./download_raw.sh
```

will collect the entire training data set, which currently sits at just shy of 10k frames. Obviously
you only need to do this once.

```bash
python train.py --epochs 10 --lr .0001 --checkpoint_name texture_model.pth
python train.py --eval --checkpoint_name texture_model.pth
python train.py --export --checkpoint_name texture_model.pth
```

where the last line will export a jit script, which can be inferred with `sl_camera_sensor.nets.texture_model`,
after you put it in `graphs/texture`.

## Background
This is a wind magnitude classifier only. A few references that support shallow convolutional networks for texture
sensitivity:

* <https://vision.ece.ucsb.edu/sites/default/files/publications/02ICPRSitaram.pdf>
* https://arxiv.org/pdf/1606.00021.pdf
* https://www.robots.ox.ac.uk/~vgg/publications/2015/Cimpoi15/cimpoi15.pdf

### Dynamic Texture
We started this in the most complicated way, looking to model both wind magnitude and direction.
Vector modeling is a periodic dynamic texture problem, which as of 9/2020 isn't possible with our cams.
A few refs on that method:

* <https://www.researchgate.net/publication/315116628_Convolutional_Neural_Network_on_Three_Orthogonal_Planes_for_Dynamic_Texture_Classification> [caffe code](https://github.com/v-andrearczyk/DT-CNN/blob/master/examples/solve_dyntex%2B%2B.py)
* <https://pdfs.semanticscholar.org/06a5/fb54cc766fa6f6e3616b3856c7c0c0630b17.pdf>
* <https://pdfs.semanticscholar.org/4c5b/cd512f1226c6c353e32a91e149516fcfe60a.pdf>

## Further Improvements
The model as it currently exists is prety good, at 70% accuracy with those failing on the class borders.
We can try to add one additional class if needed, and it may be worth quickly looking at 3D convolutions
on voxels. MobileNets do depthwise convolutions which might also be useful here

* <https://arxiv.org/pdf/1412.0767.pdf>


