import os
import numpy as np

from PIL import Image
from texture_dataset import TextureDataset
from sl_camera_sensor.nets.segmentation.segmentation_model import SegmentationModel
from sl_camera_sensor.lib.wave_tracking.wave_tracker_iou import WaveTracker
from sl_camera_sensor.lib.wave_tracking.blobify import Blobify
from ipdb import set_trace


class MaskTrainingFrames:
    def __init__(self):
        self.seg_bot = SegmentationModel()
        self.td = TextureDataset(train=True,train_pct=1)
        self.tracker = WaveTracker(2, self.seg_bot.label_map,
                                   target_size=(720, 1280),
                                   min_area=200)
        self.sea_blobber = Blobify(
            self.seg_bot.label_map,
            classes=['Water', 'Wave', 'WhiteWater', 'Lip']
        )
        self.sky_blobber = Blobify(
            self.seg_bot.label_map,
            classes=['Sky']
        )
        self.cwidth, self.cheight = 220, 220
        
    def main(self):
        for image_name in self.td.df['image'].values:
            im = np.asarray(Image.open(image_name))
            height, width, _ = im.shape
            seg_mask = self.seg_bot.predict(im)
            seg_mask = np.asarray(
                Image.fromarray(seg_mask.squeeze()).resize((width, height))
            )
            mask = np.zeros(im.shape)
            # Remove everything except water beyond the breaking zone.
            # Wave fitting is alread built into the wave tracker so just use that.
            wave_contours = self.tracker(seg_mask, return_contours=True)
            blob, miny = None, height
            for wcont in wave_contours.values():
                if wcont.y < miny:
                    blob = wcont
                    miny = wcont.y
            masked_im = im.copy()
            if blob is not None:
                y1 = int(blob.y - np.tan(np.deg2rad(90-blob.angle)) * (width-blob.ma))
                y0 = int(blob.y + np.tan(np.deg2rad(90-blob.angle)) * blob.ma)
                wave_boundary = np.linspace(y0, y1, width).astype(int)
                for x, y in enumerate(wave_boundary):
                    masked_im[y:, x, ...] = 0
            masked_im[seg_mask.squeeze()==self.seg_bot.label_map['Shore'], ...] = 0
            masked_im[seg_mask.squeeze()==self.seg_bot.label_map['Sky'], ...] = 0
            masked_im[seg_mask.squeeze()==self.seg_bot.label_map['Thing'], ...] = 0
            
            sim = Image.fromarray(masked_im)
            sim.save(image_name.replace('.jpg','.masked.jpg'))


if __name__ == '__main__':
    MaskTrainingFrames().main()
