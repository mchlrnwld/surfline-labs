#!/bin/bash

# Download the entire raw training set from S3.

if [[ -z "${TEXTURE_TRAINING_DIR}" ]]; then
    echo "Please set TEXTURE_TRAINING_DIR env, which points to where "
    echo "the raw data will be downloaded to."
    exit 1;
fi

echo "Downloading texture training set to $TEXTURE_TRAINING_DIR"

aws s3 cp s3://wt-science-data-labeling-dev/complete/texture $TEXTURE_TRAINING_DIR --recursive
