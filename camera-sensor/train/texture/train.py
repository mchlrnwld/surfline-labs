# Surface texture model training, eval and export script. Uses texture_dataset.py
# dataset iterator to load frames + labels.
#
import os
import argparse
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F

from PIL import Image, ImageDraw
from sl_camera_sensor.utils.logger import getLogger
from torch.utils.data import DataLoader
from texture_dataset import TextureDataset
from sklearn.metrics import classification_report
from ipdb import set_trace
global trt_import_error
trt_import_error = False
try:
    import trtorch
except ImportError:
    trt_import_error = True
    
log = getLogger(__name__)


class Net(nn.Module):
    def __init__(self, n_class=5, regressor=False):
        super(Net, self).__init__()
        self.regressor = False
        self.conv1 = nn.Conv2d(3, 6, 5)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(6, 16, 5)
        self.fc1 = nn.Linear(570768, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, n_class if not regressor else 1)

    def forward(self, x):
        x = self.pool(F.relu(self.conv1(x)))
        x = self.pool(F.relu(self.conv2(x)))
        x = x.view(-1, 570768)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        # x = F.softmax(x)
        return x


def train(
        epochs=5,
        batch_size=20,
        checkpoint_name="./texture-model-tdv0.pth",
        load_ckpt=True,
        lr=1e-4,
        masked=False,
        regressor=False,
):

    dataset = TextureDataset(masked=masked, train=True)
    if regressor:
        checkpoint_name = checkpoint_name.replace(".pth", "-regressor.pth")
    if dataset.masked:
        checkpoint_name = checkpoint_name.replace(".pth", "-masked.pth")

    net = Net(dataset.n_class, regressor=regressor)

    if os.path.isfile(checkpoint_name) and load_ckpt:
        checkpoint = torch.load(checkpoint_name, map_location="cpu")  # actual weights
        net.load_state_dict(checkpoint)
        log.info(f"Loaded weights from {checkpoint_name}")

    device = torch.device("cuda")  # get_device()
    net = net.to(device)

    # Random pred?
    feat, lab = dataset[0]
    feat = feat[np.newaxis, :]
    random_pred = net(feat.to(device))
    log.info(f"Random prediction: {random_pred}")

    # Construct DataLoader from our custom LotusDataset
    dataloader = DataLoader(dataset, batch_size=batch_size, shuffle=True, num_workers=3)

    # Now we can set up a loss funciton and optimizer
    loss_fn = nn.CrossEntropyLoss() if not regressor else nn.MSELoss()
    optimizer = torch.optim.Adam(net.parameters(), lr=5e-6)
    # Add a learning rate scheduler here

    n_batch = len(dataset) // batch_size
    for epoch in range(epochs):
        for ibatch, (features, labels) in enumerate(dataloader):
            features = features.to(device)
            if regressor:
                labels = labels.type(torch.FloatTensor)

            labels = labels.to(device)
            if len(features) == 1:
                features = features[np.newaxis, :]
            optimizer.zero_grad()

            # Step forward through network
            predictions = net(features)

            # Compute loss for this step
            loss = loss_fn(predictions, labels)

            # Compute gradient wrt weights in newtork & update
            loss.backward()
            optimizer.step()

            if not ibatch % 5:
                pct_done = (ibatch + 1.0) / n_batch * 100.0
                log.info(f"loss: {loss:.4f} ({pct_done:.1f}%)")

    # Save the model to disk.
    torch.save(net.state_dict(), checkpoint_name)
    log.info(f"Saved trained net: {checkpoint_name}")


def evaluate(checkpoint_name="texture-model-tdv0.pth", masked=False):
    # Eval dataset
    dataset = TextureDataset(train=False, masked=masked)
    feature, label = dataset[0]

    # Load traind netweights into a netinstance:
    if not os.path.isfile(checkpoint_name):
        raise ValueError(f"Checkpoint not found: {checkpoint_name}")

    net = Net()
    device = torch.device("cuda")
    checkpoint = torch.load(checkpoint_name, map_location="cpu")  # actual weights
    net.load_state_dict(checkpoint)
    log.info(f"Loaded weights from {checkpoint_name}")
    net.to(device)

    # Do inference per spot
    predictions = []
    ground_truths = []
    fails = []
    fail_idxs = []
    softmax = nn.Softmax(dim=1)
    for i in range(len(dataset)):  # len will be updated for only this spot
        feature, label = dataset[i]
        feature = feature[np.newaxis, :]
        feature = feature.to(device)

        # Forward pass
        with torch.no_grad():
            prediction = net(feature)

            # Detach from gpu and store
            predictions.append(softmax(prediction).cpu().numpy())  # .detatch().numpy())
            ground_truths.append(label)

        if predictions[-1].argmax() != label:
            fails.append(dataset.curr_im)
            fail_idxs.append(i)
        # if i > 10:
        #    break

    fail_idxs = np.asarray(fail_idxs)
    predictions = np.asarray(predictions)
    raw_preds = np.squeeze(predictions)
    predictions = np.squeeze(predictions).argmax(axis=1)
    ground_truths = np.asarray(ground_truths)
    cnt = len(np.where(predictions == ground_truths)[0])
    pct = cnt * 100.0 / len(ground_truths)
    print(f"{pct:.0f}% correct lol")

    p = predictions
    g = ground_truths
    print(classification_report(g, p))

    # Make the pdf of failures. Draw on each frame first though.
    sname = "failures.pdf" if dataset.masked else "failures_unmasked.pdf"
    fail_ims = []

    for name, pred, truth, raw in zip(
        fails, p[fail_idxs], g[fail_idxs], raw_preds[fail_idxs]
    ):
        im = Image.open(name).resize((1280, 720), Image.LANCZOS)
        draw = ImageDraw.Draw(im)
        draw.text((10, 30), name, fill=0)  # (255,255,255))
        draw.text((10, 60), f"truth={truth}", fill=0)  # (255,255,255))
        raw = [f"{f:.2f}" for f in raw.astype(np.float16)]
        draw.text((10, 90), f"preds={raw}", fill=0)  # (255,255,255))
        fail_ims.append(im)

    fail_ims[0].save(sname, save_all=True, append_images=fail_ims[1:50])
    # cmd = 'convert ' + ' '.join(fails[:50]) + ' ' + sname
    # os.system(cmd)
    set_trace()

def export(checkpoint_name="texture_model_unmasked.pth"):
    net = Net()
    checkpoint = torch.load(checkpoint_name, map_location="cpu")
    net.load_state_dict(checkpoint)
    net = net.to("cuda").eval()
    inp = torch.rand((1, 3, 576, 1024)).to("cuda")
    model_trace = torch.jit.trace(net.forward, inp)
    torch.jit.save(model_trace, checkpoint_name.replace(".pth", ".pt"))
    log.info(f"Saved {checkpoint_name.replace('.pth','.pt')}")
    if not trt_import_error:
        params = {
            "input_shapes": [(1, 3, 576, 1024)],
            "op_precision": torch.half,  # Run with FP16
            "workspace_size": 1 << 20
        }
        log.info('Compiling TRT script...')
        trt_model = trtorch.compile(model_trace, params)
        tensor = inp.half()
        result = trt_model(tensor)
        # Save it!
        torch.jit.save(trt_model, checkpoint_name.replace('.pth', '.ts'))
        log.info(f"Wrote TRT script to {checkpoint_name.replace('.pth', '.ts')}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--eval", "-e", action="store_true", default=False)
    parser.add_argument("--epochs", default=5, type=int)
    parser.add_argument("--lr", default=1e-4, type=float)
    parser.add_argument(
        "--checkpoint_name", default="texture-model-tdv0.pth", type=str
    )
    parser.add_argument("--export", default=False, action="store_true")
    args = parser.parse_args()
    if args.eval:
        evaluate(args.checkpoint_name)
    elif args.export:
        export(args.checkpoint_name)
    else:
        train(checkpoint_name=args.checkpoint_name, lr=args.lr, epochs=args.epochs)
