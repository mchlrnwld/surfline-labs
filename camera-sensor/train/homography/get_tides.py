import numpy as np
import pickle
import datetime
from typing import Union

from collections import defaultdict
from labs_utils import reports, spots


# TEMP: API is broken so using this for now
spotid_map = {'wc-blackiesclose': '584204204e65fad6a7709115',
 'wc-seventeenthst': '5842041f4e65fad6a77088eb',
 'wc-blackies': '584204204e65fad6a7709115',
 'wc-hbstatebeachn': '584204204e65fad6a770998c',
 'wc-church': '5842041f4e65fad6a770888b',
 'wc-cardiffreefsouth': '5842041f4e65fad6a77088b1',
 'wc-cardiffov': '5842041f4e65fad6a77088b1',
 'wc-hbss': '5842041f4e65fad6a77088ed',
 'wc-elporto42nd': '5a25e409aa1aea001b27be39',
 'wc-dohenysb': '5842041f4e65fad6a77088d7',
 'wc-cstreetclose': '5842041f4e65fad6a7708828',
 'wc-oceanbeachsdpier': '59aedf63b5e8310014bbe375',
 'wc-lowers': '5842041f4e65fad6a770888a',
 'wc-tamarack': '5842041f4e65fad6a7708837',
 'wc-9thstov': '5842041f4e65fad6a7708827',
 'wc-hbpiersclose': '5977abb3b38c2300127471ec',
 'wc-venicebeachclose': '590927576a2e4300134fbed8',
 'wc-hbpierns': '5842041f4e65fad6a7708827',
 'mx-puertocloseup': '5842041f4e65fad6a7708b43'}


def get_tides(
        alias: str,
        start_date: Union[str, datetime.datetime],
        end_date: Union[str, datetime.datetime]
) -> defaultdict:
    if not isinstance(start_date, datetime.datetime):
        try:
            start_date = datetime.datetime.strptime(start_date, '%Y/%m/%d')
            end_date = datetime.datetime.strptime(end_date, '%Y/%m/%d')
        except ValueError:
            raise ValueError('Input start date as %Y/%m/%d, ex 2021/01/31')
    curr_date = start_date
    tides = defaultdict(list)
    # spotid = spots.get_spotid(alias)  # Use this when spotid querying works
    spotid = spotid_map[alias]
    while curr_date <= end_date:
        print(f'Getting tides for {curr_date}')
        for hr in range(24):
            rpt = reports.get_report(spotid, curr_date + datetime.timedelta(hours=hr))
            tide = rpt['tide']['current']
            tides['height'].append(tide['height'])
            tides['timestamp'].append(tide['timestamp'])
            tides['waveHeightMin'].append(rpt['waveHeight']['min'])
            tides['waveHeightMax'].append(rpt['waveHeight']['max'])
            tides['waveHeightOcc'].append(rpt['waveHeight']['occasional'])
        curr_date = curr_date + datetime.timedelta(days=1)
    with open(f'{alias}-tides.pkl', 'wb') as f:
        pickle.dump(tides, f)
    return tides
