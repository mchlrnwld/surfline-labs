# Build a database that can be used to fit homography at various tides.
# This is laid out on disk similarly to a large dictionary with tide height keys
# and attributes of bbox data (filtered for non-wave sitters and near-shoreline
# beach people), rewind name, and a single frame so we know what the cam state
# looks like.
#
import os
import numpy as np
import pandas as pd
import pickle
import shutil
import argparse
from collections import defaultdict
from typing import Optional, Tuple

from PIL import Image
from sl_camera_sensor.nets.segmentation.segmentation_model import SegmentationModel
from sl_camera_sensor.nets.visibility.visibility_model import VisibilityModel
from sl_camera_sensor.nets.detection.detection_model import DetectionModel
from sl_camera_sensor.lib.localization.localize_dets import LocalizeDets
from sl_camera_sensor.lib.localization.localize_mask import LocalizeMask
from sl_camera_sensor.utils.cam import video_utils as vu
from sl_camera_sensor.utils.pkl_iter import PklIter
from sl_camera_sensor.utils.logger import getLogger
from get_tides import get_tides
from ipdb import set_trace

log = getLogger(__name__)


class BuildHomDB:
    """ 
    This will build a statistical homography database in 
    some directory. It'll be laid out as:

    alias/
        tide_{ft:.1f}/
            rewind-prefix/
                frame.jpg
                mask.jpg
                bbox_data.pkl

    where the bbox_data is filtered for SurferSitting on non-waves, and beachpeople
    filtered to be within some close distance to the shoreline.

    Note that if the camera is moving around this db will need to be partitioned
    even further, using the frames in each bottom directory to match with 
    other frames in order to find similar camera states. It needs to be static
    to fit the homography.
    """
    def __init__(
            self,
            alias: str = 'wc-hbpierns',
            db_dir: str = '/data/homography/',
            rewind_dir: str='/archived-rewinds/main-data/wc-hbpierns/2021/01/15',
    ) -> None:
        self.db_dir = db_dir
        self.rewind_dir = rewind_dir  # directory with all the rewinds
        self.max_shore_dist_pix = 50  # TODO add this
        self.fps = 1
        self.alias = alias
        #tides_file = 'wc-hbpierns-tides.pkl'
        #tides_file = 'wc-lowers-tides.pkl'
        if os.path.isfile(f'{alias}-tides.pkl'):
            tides_file = f'{alias}-tides.pkl'
            with open(tides_file, 'rb') as f:  # TODO add this
                self.tides = pickle.load(f)
            log.info(f'Loaded {tides_file}')
        else:
            start_date, end_date = self.get_timeframe_from_dir(rewind_dir)
            self.tides = get_tides(alias, start_date, end_date)
        self.vmod = VisibilityModel()
        dets_label_map = DetectionModel.get_label_map()
        self.mask_localizer = None
        self.dets_label_map = {v:k for k, v in dets_label_map.items()}
        self.counts = defaultdict(int)

    def _init_localizers(self, target_size):
        self.localizer = LocalizeDets(
            DetectionModel.get_label_map(),
            SegmentationModel.get_label_map(),
            target_size=target_size
        )
        self.mask_localizer = LocalizeMask(
            SegmentationModel.get_label_map(),
            target_size=target_size)
        log.info(f'Initializing localizers for target size: {target_size}')
        
    def _get_data_filename(self, rewind_path, istr):
        """ Collects model output files based on input rewind string """
        if 'stream' not in rewind_path:
            raise ValueError(
                'Only look at raw .stream. rewind files as ground truths'
            )
        if not os.path.isfile(rewind_path):
            raise ValueError(f"rewind doesn't exist: {rewind_path}")
        _, extn = os.path.splitext(rewind_path)
        data_filename_approx = rewind_path.replace(
            '.stream.', '.' + istr + '.').replace(
                '.mp4', f'.{self.fps}fps'
        )
        data_filenames = [
            f for f in os.listdir(os.path.dirname(data_filename_approx))
            if f.startswith(os.path.basename(data_filename_approx))
        ]
        if len(data_filenames) == 0:
            #log.critical(f"Data file doesn't exist: {data_filename_approx}")
            return 
        data_filename = os.path.join(
            os.path.dirname(data_filename_approx), data_filenames[0]
        )
        return data_filename

    def get_timeframe_from_dir(
            self,
            rewind_dir: str
    ) -> Tuple[str, str]:
        splt = list(filter(None, rewind_dir.split(os.sep)))

        if splt[-3].isnumeric() and splt[-2].isnumeric() and splt[-1].isnumeric():
            date = os.sep.join(splt[-3:])
            return date, date
        elif splt[-2].isnumeric() and splt[-1].isnumeric():
            days = [f for f in sorted(os.listdir(rewind_dir)) if os.path.isdir(f)]
            year_month = os.sep.join(splt[-2:])
            return f'{year_month}/{days[0]}', f'{year_month}/{days[-1]}'
        elif splt[-1].isnumeric():
            months = [f for f in sorted(os.listdir(rewind_dir)) if os.path.isdir(f)]
            first_day = [
                f for f in sorted(os.listdir(os.path.join(rewind_dir, months[0])))
                if os.path.isdir(f)
            ][0]
            last_day = [
                f for f in sorted(os.listdir(os.path.join(rewind_dir, months[-1])))
                if os.path.isdir(f)
            ][-1]
            start_date = f'{splt[-1]}/{months[0]}/{first_day}'
            end_date = f'{splt[-1]}/{months[-1]}/{last_day}'
            return start_date, end_date
        else:
            raise ValueError('Please input year, month, or day base directory of '
                             'rewinds, i.e. "/path/to/<alias>/<year>/<month>/<day>"')

    def get_nearest_tide(self, timestamp: int) -> float:
        tide_match_idx = np.abs(
            (timestamp - np.array(self.tides['timestamp'])[np.newaxis])
        ).argmin()
        # Check it's within one hour?
        try:
            assert abs(self.tides['timestamp'][tide_match_idx] - timestamp) < 3600
        except AssertionError:
            log.info("Tide is far away")
            set_trace()
        return round(self.tides['height'][tide_match_idx], 1)

    def resize(self, arr):
        """ Assumes masks, ie NEAREST """
        return np.array(
            Image.fromarray(arr).resize(self.target_size[::-1], Image.NEAREST)
        )
    
    def process_rewind(self, rewind_path: str, redo: Optional[bool]=False) -> dict:
        """ 
        Extract bbox data and write them to disk, for a single .stream.
        input rewind, looking for raw network data on disk based on
        what self.fps is set as
        """
        dets_name = self._get_data_filename(rewind_path, 'dets')
        masks_name = self._get_data_filename(rewind_path, 'masks')
        stack_vid_name = self._get_data_filename(rewind_path, 'stack')
        if dets_name is None or masks_name is None:
            #print('Skipping {rewind_path}')
            return
        
        # Get the nearest tide & lay out directory structure
        self.alias = alias =  vu.get_alias(rewind_path)
        vid_prefix = vu.get_vid_prefix(rewind_path)
        ts = vu.get_vid_timestamp(rewind_path)
        tide = self.get_nearest_tide(ts)
        odir = os.path.join(
            *[self.db_dir, alias, f'tide_{tide:.1f}', vid_prefix]
        )
        if not os.path.isdir(odir):
            os.makedirs(odir)
            log.info(f'Made directory: {odir}')

        # Skip this rewind if the data already exists?
        bbox_filename = os.path.join(odir, 'bbox_data.pkl')
        if not redo and os.path.isfile(bbox_filename):
            with open(bbox_filename, 'rb') as f:
                bbox_data = pickle.load(f)
            return bbox_data
        
        dets_iter = PklIter(dets_name)
        masks_iter = PklIter(masks_name)
        frames_iter = vu.get_frame_batch(
            rewind_path, 0, 1, unix_time=False,
            fps=float(self.fps)
        )
        if dets_iter is None or masks_iter is None:
            log.critical('net iters is/are None')
            return

        # Figure out the frame size we want to work with
        frames, _ = next(frames_iter)
        if frames is False:
            return
        frame = frames[0]
        height, width, _ = frame.shape
            
        # Check visibility
        vis = self.vmod.predict(frame)
        if vis[0][0] < 0.68:
            log.info(f'Bad visibility for {rewind_path}')
            return 
        
        sitters = []
        cnt = 0
        while True:
            dets = next(dets_iter)
            masks = next(masks_iter)            
            if dets is None or len(dets) == 0 or masks is None or len(masks) == 0:
                break
            if cnt == 0:
                np.save(os.path.join(odir, 'mask.npy'), masks[0])
                Image.fromarray(frame).save(os.path.join(odir, 'frame.jpg'))
            # I Should check why this is failing, but for now just skip.
            if self.mask_localizer is None:
                self._init_localizers((height, width))
                self.target_size = (height, width)
            # Need to reshape the mask we pass to the localize_sitting class.
            self.mask_localizer(self.resize(masks[0]))
            horizon = self.mask_localizer.horizon
            shoreline = self.mask_localizer.shoreline
            if np.all(np.isnan(horizon['raw'])) or np.all(np.isnan(shoreline['raw'])):
                print('bad shoreline or horizon - skipping. TODO dont skip with only 1 missing')
                shutil.rmtree(odir)
                return

            cnt += 1
            for det, mask in zip(dets, masks):
                if len(det) == 0:
                    continue
                sitting_locs = self.localizer.localize_sitting(
                    det, self.resize(mask), with_beachpeople=True,
                    shoreline=shoreline['interp'],
                    max_shore_dist_pix=self.max_shore_dist_pix
                )
                if len(sitting_locs) > 0:
                    sitters.append(sitting_locs)

        if len(sitters) < 1:
            # They've all been filtered out, or there is no crowd on this day.
            # Either are not uncommon.
            return
        
        sitters = np.vstack(sitters)
        # These are normalized coords - unnormalize for vis purposes.
        sitters = self.localizer._preprocess_locs(sitters, frame)
        deltas = sitters[:, 3] - sitters[:, 1]
        ylocs = sitters[:, 0]
        xlocs = sitters[:, 1]  # Add these so we know what the dist is like at lowers et al.
        # Write out a dict pickle to disk.
        bbox_data = {
            'y_raw': ylocs, 'x_raw': xlocs, 'widths_raw': deltas,
            'horizon': horizon, 'shoreline': shoreline,
            'tide': tide, 'timestamp': ts
        }
        bbox_filename = os.path.join(odir, 'bbox_data.pkl')
        with open(bbox_filename, 'wb') as f:
            pickle.dump(bbox_data, f)
        log.info(f'Wrote {bbox_filename}')
        return bbox_data

    def run(self, rewind_dir=None, fps=1, db_dir=None, redo=False):
        """ Run this class on a full directory and build
        the tidal database.
        """
        self.rewind_dir = rewind_dir or self.rewind_dir
        for root, dirs, files in sorted(os.walk(self.rewind_dir)):
            #day = root.split('2021/01/')[-1]  # for hb...
            #if day != '':
            #    if int(day) < 11:
            #        continue
            
            rewind_paths = sorted([
                os.path.join(root, f) for f in files
                if f.endswith('.mp4') and '.stream.' in f
            ])
            for rewind_path in rewind_paths:
                bbox_data = self.process_rewind(rewind_path, redo=redo)
                if bbox_data is not None:
                    self.counts[bbox_data['tide']] += len(bbox_data['y_raw'])

        # Print a summary of counts for each tide:
        tides = np.array([t for t in self.counts.keys()])
        cnts = np.array([c for c in self.counts.values()])
        cnts = cnts.astype(float) / 100.
        sidx = np.argsort(tides)
        print('   tide: cnt/100')
        for tide, cnt in zip(tides[sidx], cnts[sidx]):
            print(f'{tide}:{"="*round(cnt)}')

            
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-a', '--alias', type=str, required=True, default='wc-hbpierns'
    )
    parser.add_argument(
        '-dbd', '--db_dir', type=str, default='/data/homography'
    )
    parser.add_argument(
        '-d','--rewind_dir', type=str,
        default='/archived-rewinds/main-data/wc-hbpierns/2021/01/15'
    )
    parser.add_argument('-r', '--redo', action='store_true',
                        help='overwrite existing database if one exists'
    )
    args = parser.parse_args()
    builder = BuildHomDB(
        alias=args.alias, db_dir=args.db_dir, rewind_dir=args.rewind_dir
    )
    builder.run(redo=args.redo)
