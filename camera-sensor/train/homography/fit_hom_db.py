import os
import json
import cv2
import argparse
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl
import pickle
mpl.rc('image', cmap='viridis')
# FROM SOMEWHERE IMPORT ZANES SIMILARITY FUNCTION
from collections import defaultdict, OrderedDict
from sl_camera_sensor.lib.homography.homography import Homography
#from sl_camera_sensor.lib.homography.warped_hom_db import WarpedHomDB
from warped_hom_db import WarpedHomDB
from scipy.stats import sigmaclip
from matplotlib.backends.backend_pdf import PdfPages
from sklearn.linear_model import LinearRegression, RANSACRegressor
from scipy.stats import sigmaclip
from PIL import Image
from sl_camera_sensor.utils.logger import getLogger
from ipdb import set_trace


log = getLogger(__name__)

class FitHomDB(WarpedHomDB):
    """ 
    Fit face-on and non-face-on camera homographies using the
    database which was built using the build_hom_db class.
    
    This will go into the db_dir folder, group data by tide,
    and fit the ocean plane using the bounding box distributions
    found therein. This does some additional statistical filtering
    prior to fitting.
    
    For edge-on cams the process is the same, but repeated multiple
    times for each line of sight (LOS). The granularity of the number
    of sight lines is figured out from the data, by looking at how
    the widths of boxes vary at constant y-values. Higher variance
    requires more lines of sight and therefore more historical data.
    
    The granularity of tide binning should be set by the user. 
    """
    def __init__(
            self,
            alias: str,
            db_dir: str = '/data/homography',
            tide_bin_size = 1
    ) -> None:
        self.alias = alias
        self.db_dir = os.path.join(db_dir, alias)
        self.tide_bin_size = 1
        super().__init__()
        
    def get_tide_data(self, tide=None):
        # Fit for only a specific tide?
        tides_df = self._get_tide_bins()
        tide_data = {}
        if tide == 'all' or tide is None:
            for tide_bin, grp in tides_df.groupby('tide_bin'):
                tide_data[f'{grp["tide"].mean():.3f}'] = self._parse_tide_db(grp)
        else:
            # Filter out for all the bins at a certain tide?
            closest_tide = np.abs(1-tides_df['tide'].values[np.newaxis,:]).argmin()
            tide_bin = tides_df['tide_bin'].iloc[closest_tide]
            tides_df = tides_df[tides_df['tide_bin'] == tide_bin]
            avg_tide = tides_df['tide'].mean()
            tide_data[f'{avg_tide:.3f}'] = self._parse_tide_db(tides_df) 
        # Aggregate all the data for each tide, and check homogeneity??
        for k, v in tide_data.items():
            #v = self._check_frame_homogeneity(np.array(v))
            v = self._aggregate_tide_data(v)
            assert len(v['x_raw']) == len(v['y_raw'])
            tide_data[k] = v
        # Save the binning
        bins_min = tides_df.groupby('tide_bin').min().values.flatten()
        bins_max = tides_df.groupby('tide_bin').max().values.flatten()
        self.tide_bins = list(zip(bins_min, bins_max))
        return tide_data

    def _get_avg_horizon(self, all_tide_data):  #, add_horizon=False):
        """ 
        This should be run and averaged on all the data from all
        the different tides.
        """
        horizons = []
        for tide, tide_data in all_tide_data.items():
            if len(tide_data['horizons']) == 0:
                log.info(f'No horizons data at tide {tide}')
            else:
                horizons.append(np.vstack(tide_data['horizons']))
        horizons = np.vstack(horizons)
        horizons = self._clip_delineations(horizons, avg=False)

        horizon_avg = horizons[:, 1].mean() - 10
        horizon_std = horizons[:, 1].std()
        if horizon_std > 20:
            log.info('Horizon variance is large, this is worth investigating. '
                     'Could be foggy frames in your database, or camera roll.')
        
        return all_tide_data, horizons, horizon_avg, horizon_std
            
    def _clip_delineations(self, regions, avg=False):
        """ Given a 2D vector of region delineations, clip it about x.
        You can call with one tide or all tides, eg for horizons you should
        call with all tides, for shorelines not.
        
        Returns:
            regions vector with outliers removed in both x and y
        """
        if regions.ndim == 3:
            regions = np.vstack(regions)
        elif regions.ndim > 3:
            raise ValueError(
                'Regions shaped {regions.shape} not allowed. '
                'should be 2D or 3D'
            )
        unique_xs = np.unique(regions[:, 0])
        clipped_xs = []
        clipped_ys = []
        avg_x, avg_y = [], []
        for x in unique_xs:
            x_idxs = np.where(regions[:, 0] == x)[0]
            cur_ys = regions[:, 1][x_idxs]
            mean = np.mean(cur_ys)
            std = np.std(cur_ys)
            within_range = np.where(
                (cur_ys < (mean + std)) & (cur_ys > (mean - std))
            )[0]
            clipped_x_idxs = x_idxs[within_range]
            clipped_xs.extend(regions[:, 0][clipped_x_idxs])
            clipped_ys.extend(regions[:, 1][clipped_x_idxs])
            avg_x.append(x)
            avg_y.append(np.mean(regions[:, 1][clipped_x_idxs]))
        regions = np.vstack(list(zip(clipped_xs, clipped_ys)))
        avg_regions = np.array((avg_x, avg_y)).T.astype(int)
        assert avg_regions.shape[0] == self.sx
        if np.min(avg_regions[:,1]) < 0:
            set_trace()
        return np.array((avg_x, avg_y)).T.astype(int) if avg else regions

    def _get_horizon_anchor_pts(self, horizons, horizon_avg):
        h_ys = np.random.normal(horizon_avg, 3, size=len(horizons[:, 1]))
        h_ws = np.random.uniform(low=0, high=0.01, size=len(h_ys))
        return h_ys, h_ws
    
    def fit(self, tide=None, clip=0, add_horizon=True, tide_bin_size=None, faceon=False):
        self.tide_bin_size = tide_bin_size or self.tide_bin_size
        all_tide_data = self.get_tide_data(tide=tide)
        
        # Clip further? This will add '*_clip' keys
        if clip > 0:
            all_tide_data = self._clip_tide_data(all_tide_data, clip)

        # Extract these raw values before modifications with horizon
        #xx_raw, yy_raw = tide_data['x_raw'].copy(), tide_data['y_raw'].copy()

        # Trigger some sanity plots in base class. These should also check
        # automatically if this cam needs to be warped.
        #warped_matrices, gif_list = self.is_warped(all_tide_data, self.alias)
        warped_matrices = self.is_warped(all_tide_data, self.alias, faceon)
        #imageio.mimsave('/home/kwynne/t.gif', wframes_for_gif)

        # Figure out the average horizon location and add those
        # points to the data we'll regress on?
        all_tide_data, horizons, horizon, hvar = self._get_avg_horizon(
            all_tide_data#, add_horizon
        )
        
        pdf = PdfPages(f'{self.alias}-hom-fits.pdf')
        pdf_xy = PdfPages(f'{self.alias}-xy.pdf')  # TODO move this to warp class
        frame_size = self._get_frame_size(list(all_tide_data.values())[0])
        self.sy, self.sx = frame_size
        
        tidal_fits = {}
        # Fit for each tide
        for k, tide_data in all_tide_data.items():
            #if args.horizon:
            #    assert len(tide_data['x_raw_no_horiz']) == len(tide_data['y_raw_no_horiz'])

            # Warp? #TODO need to automate warp detection and return identity if not warped.
            M = warped_matrices[k]
            
            is_warped = not self.is_identity(M)

            if is_warped:
                log.info(f'Projecting points for regression ({float(k):.1f} ft)')
                tide_data = self.project_pts(tide_data, M)
            else:
                M = np.eye(3)

            if args.horizon:
                y_raw = tide_data['y_raw'].copy()
                y_hor, w_hor = self._get_horizon_anchor_pts(horizons, horizon)
                for key in ['y_raw', 'widths_raw', 'y_clip','w_clip']:
                    if key in tide_data:
                        arr = w_hor if key.startswith('w') else y_hor
                        tide_data[key] = np.hstack((tide_data[key], arr))

            # Show multiple regressions? regressia?
            fit_raw = self.regress(tide_data['y_raw'], tide_data['widths_raw'])
            fit_ransac = self.regress(
                tide_data['y_raw'], tide_data['widths_raw'], ransac=True
            )
            if clip > 0:
                fit_clip = self.regress(
                    tide_data['y_clip'], tide_data['w_clip'], ransac=False)

            # Make a prediction in our range to oplot
            x_test = np.linspace(horizon, tide_data['y_raw'].max() + 10)[:, np.newaxis]
            y_test = fit_raw.predict(x_test)
            y_test_ransac = fit_ransac.predict(x_test)
            if clip:
                y_test_clip = fit_clip.predict(x_test)

            ## Make some plots of the fits!
            fig, ax1 = plt.subplots(figsize=(7,5))
            plt.hexbin( tide_data['y_raw'], tide_data['widths_raw'],
                        bins='log', mincnt=1, gridsize=50
            )
            if 'y_clip' in tide_data:
                plt.scatter(
                    tide_data['y_clip'],
                    tide_data['w_clip'],
                    s=5, color='red', label='filtered'
                )
            plt.plot(x_test, y_test, '--', color='black', label='regression-raw')
            plt.plot(x_test, y_test_ransac, alpha=0.6, label='regression-ransac')
            if clip > 0:
                plt.plot(x_test, y_test_clip, alpha=0.6, label='regression-clip')
                
            plt.plot(
                [horizon, horizon],
                #[tide_data['widths_raw'].min()-10, tide_data['widths_raw'].max()],
                [-10,100],
                label='horizon',color='blue'
            )
            plt.ylabel('bbox width (pix)', fontsize=14)
            plt.xlabel('y-coord-frame', fontsize=14)
            plt.legend()
            plt.title(f'Avg Tide: {float(k):.2f}ft  ({len(tide_data["y_raw"])} pts)')
            plt.ylim([0, tide_data['widths_raw'].max()])
            sy, sx = frame_size  # self._get_frame_size(tide_data)
            plt.xlim([horizon-10, sy])
            plt.ylim([-2,100])
            plt.tight_layout()
            #plt.savefig(f'tide-{float(k):.0f}.jpg')
            pdf.savefig()
            
            # Show the raw x-y distributions. What does lowers look like?
            fig2, ax2 = plt.subplots(figsize=(7,5))
            if args.horizon:
                yy = y_raw
                xx = tide_data['x_raw']
            else:
                yy = tide_data['y_raw']
                xx = tide_data['x_raw']
            
                #yy = (tide_data['y_raw_no_horiz'] if 'y_raw_no_horiz'
                #  in tide_data else tide_data['y_raw'])
            #ax2.scatter(tide_data['x_raw', yy)
            #ax2.scatter(xx, yy)
            plt.hexbin(xx, yy,
                mincnt=1, bins='log', gridsize=50
            )
            plt.title(f'Avg Tide: {float(k):.2f}ft  ({len(tide_data["y_raw"])} pts)')
            #plt.ylim([0, frame_size[0]])
            #plt.xlim([0, frame_size[1]])
            plt.gca().invert_yaxis()
            plt.colorbar()
            plt.tight_layout()
            pdf_xy.savefig()
            
            tidal_fits[k] = {'raw': fit_raw, 'ransac': fit_ransac.estimator_}
            if clip > 0:
                tidal_fits[k]['clip'] = fit_clip

        # Add distortion matrix & frame size.
        M = warped_matrices[k].astype(float).tolist()
        tidal_fits['projection_matrix'] = M
        tidal_fits['frame_size'] = frame_size
        pdf.close()
        pdf_xy.close()
        log.info(f'Wrote {self.alias}-hom-fits.pdf, {self.alias}-xy.pdf')
        self._write_fits(tidal_fits)
        return tidal_fits

    def _write_fits(self, tidal_fits):
        """ Write the outputs of self.fit() to disk as csv files """
        ddir = '/home/kwynne/surfline-labs/camera-sensor/predict/sl_camera_sensor/lib' \
               '/homography'
        fname = os.path.join(ddir, f'{self.alias}-tidal-homography.json')
        #fname = f'{self.alias}-tidal-homography.json'
        exports = OrderedDict()
        exports['projection_matrix'] = tidal_fits.pop('projection_matrix')
        exports['frame_size'] = tidal_fits.pop('frame_size')
        assert len(self.tide_bins) == len(tidal_fits)
        for i, (tide, fits) in enumerate(tidal_fits.items()):
            if tide == 'projection_matrix':
                continue
            export = {}
            export['coefs'] = {
                k: (float(fits[k].coef_[0]), float(fits[k].intercept_))
                for k in ['raw', 'ransac']
            }
            if 'clip' in fits:
                export['coefs']['clip'] = (
                    float(fits['clip'].coef_[0]),
                    float(fits['clip'].intercept_)
                )
            export['tideBins'] = self.tide_bins[i]
            export['tide'] = float(tide)
            exports[f'{float(tide):.2f}'] = export
            
        #for tide, vals in exports.items():
        #    print(type(tide))
        #    for k, v in vals.items(): print(type(k), type(v))

        with open(fname, 'w') as f:
            json.dump(exports, f, sort_keys=False, indent=4)
        log.info(f'Wrote fit params to {fname}')
        
    def regress(self, x, y, ransac=False):
        """ Do a simple regression on the filtered widths as a function of y """
        if x.ndim == 1:
            x = x[:, np.newaxis]
        if ransac:
            reg = RANSACRegressor().fit(x, y)
        else:
            reg = LinearRegression().fit(x, y)
        return reg

    def _check_frame_homogeneity(self, tide_data):
        # First check with the average horizon location?
        # ugh this function sucks
        # TODO Zane's function should go here.
        horizons = np.array([td['horizon'] for td in tide_data])
        checks = np.hstack(
            (np.where(horizons == 0.0)[0], np.where(np.isinf(horizons))[0])
        )
        keeps = np.array([i for i in range(len(horizons)) if i not in checks])
        hh = horizons[keeps]
        checks2 = np.where(
            (hh > hh.mean() + hh.std()*.6) | (hh < hh.mean() - hh.std()*.6)
        )[0]
        checks = np.hstack((checks, keeps[checks2]))
        keeps = np.array([i for i in range(len(horizons)) if i not in checks])
        
    def _get_tide_bins(self):
        """ 
        Walk through db_dir/alias to build list of tide_data as a funcion
        of binned tide, based on self.tide_bin_size
        """
        if not os.path.isdir(self.db_dir):
            raise ValueError(f"db doesn't exist for {self.alias}: {self.db_dir}")
        tides = sorted([
            float(f.split('_')[-1]) for f in os.listdir(self.db_dir)
            if f.startswith('tide_')
        ])
        nbins = np.ceil((max(tides) - min(tides)) / self.tide_bin_size)
        bins = np.linspace(min(tides), max(tides)+.05, int(nbins))
        bin_idxs = np.digitize(tides, bins)
        tides_df = pd.DataFrame({'tide': tides, 'tide_bin': bin_idxs})
        return tides_df
    
    def _parse_tide_db(self, tides_df):
        # Now parse all the directories as a function of tide bin.
        tide_data = []
        for _, row in tides_df.iterrows():
            ddir = os.path.join(self.db_dir, 'tide_'+ str(row['tide']))
            for rwnd_pfx in os.listdir(ddir):
                tide_data_path = os.path.join(ddir, rwnd_pfx)
                # TODO read frames here?
                bbox_data_path = os.path.join(tide_data_path, 'bbox_data.pkl')
                # Sometimes the bbox data was completely filtered...
                if not os.path.isfile(bbox_data_path):
                    continue
                with open(bbox_data_path, 'rb') as f:
                    td = pickle.load(f)
                td['path'] = tide_data_path
                tide_data.append(td)
        return tide_data

    def _aggregate_tide_data(self, tide_data, delineator_key='interp'):
        """
        Combine all the separate tide data dicts into one. 
        
        :param tide_data: list of dicts
        :delineator_key: use 'interp' or 'raw' keys in the 
        mask delineated vectors of shoreline and horizon.
        :returns: single dict with the same keys and some new ones.
        """
        # TODO this is similar to the place to check longitudinal
        # variance for non-face-on cams.
        ylocs = tide_data[0]['y_raw']
        xlocs = tide_data[0]['x_raw']
        assert len(xlocs) == len(ylocs)
        widths = tide_data[0]['widths_raw']
        shorelines = tide_data[0]['shoreline'][delineator_key][np.newaxis]
        horizons = tide_data[0]['horizon'][delineator_key][np.newaxis]
        #horizons = [h[delineator_key]] #if h['fit'] < 0.5 else []
        #if h['fit'] < 0.5:
        #    log.info('Horizon fits deviate from a straight line, which is unwanted.')
        paths = [tide_data[0]['path']]
        for i, td in enumerate(tide_data):
            if i > 0:
                ylocs = np.hstack((ylocs, td['y_raw']))
                xlocs = np.hstack((xlocs, td['x_raw']))
                widths = np.hstack((widths, td['widths_raw']))
                try:
                    assert len(xlocs) == len(ylocs) == len(widths)
                except AssertionError:
                    print('Unable to aggregate tide data - inconsistent sizing')
                    set_trace()
                paths = np.hstack((paths, td['path']))
                ishoreline = td['shoreline'][delineator_key][np.newaxis]
                #if not np.all(np.isnan(ishoreline)) and ishoreline != 0:
                try:
                    shorelines = np.vstack((shorelines, ishoreline))
                except:
                    set_trace()
                #shorelines.append(ishoreline)
                ihorizon = td['horizon'][delineator_key][np.newaxis]
                horizons = np.vstack((horizons, ihorizon))
                #if ihorizon['fit'] < 0.5:
                #horizons.append(ihorizon)
        assert len(xlocs) == len(ylocs)
        return {
            'y_raw': ylocs, 'x_raw': xlocs, 'widths_raw': widths, 
            'paths': paths, 'shorelines': shorelines, 'horizons': horizons
        }
    
    def _clip_tide_data(self, tide_data, clip, pix_bin_width=3):
        """ 
        Clip the horizontal bins of y. Alternative to ransac.

        :param tide_data: list of dictionaries that all have the 
        same tide and camera states.
        :returns: tide data but with clip_* keys.
        """
        for k, v in tide_data.items():
            # Do a sigma clip on each yloc?
            df = pd.DataFrame({'ylocs': v['y_raw'], 'widths': v['widths_raw']})
            df['bins'] = pd.cut(
                df['ylocs'],
                np.arange(0, df['ylocs'].max() + 0.1, pix_bin_width),
                retbins=False
            )
            #df.dropna(inplace=True)
            y_means, width_means = [], []
            for idx, grp in df.groupby('bins'):
                if len(grp) == 0:
                    continue 
                iy = np.mean(grp['ylocs'].values)
                iwidths = grp['widths']
                # clip those widths
                if len(iwidths) < 5:
                    #iwidth = iwidths
                    continue
                else:
                    iwidths, _, _ = sigmaclip(iwidths, low=clip, high=clip)
                    iwidth = np.mean(iwidths)
                y_means.append(iy)
                width_means.append(iwidth)
                
            v['y_clip'] = np.array(y_means)
            v['w_clip'] = np.array(width_means)
            tide_data[k] = v
            
        return tide_data

    def _get_hom_pts(self, coefs, target_size=(720, 1280), scale=1.33, ft_per_line=5):
        sy, sx = target_size
        y = np.arange(0, sy)[::-1]
        x = np.arange(0, sx)
        w = coefs[0] * y + coefs[1]
        w_ft = scale / w
        yg_idxs = []
        csum = np.cumsum(w_ft / ft_per_line).astype(int)
        for unq in np.unique(csum):
            idx = np.where(csum == unq)[0][0]
            if w[idx] > 0:
                yg_idxs.append(y[idx])
        yg_pts = [[(0, iy), (sx, iy)] for iy in yg_idxs]
        return yg_pts

    def _get_non_water_mask(self, mask, sx, sy):
        mask_label_map = {
            'background': 0, 'Thing': 1,
            'Water': 2, 'Shore': 3, 'Sky': 4,
            'WhiteWater': 5, 'Wave': 6,
            'Offshore': 7, 'Lip': 8
        }
        mask = np.array(Image.fromarray(mask).resize((sx, sy), Image.NEAREST))
        keep_classes = ['Water', 'Wave', 'WhiteWater', 'Offshore']
        keep_classes_ints = [mask_label_map[k] for k in keep_classes]
        non_water = np.where(~np.isin(mask, keep_classes_ints))        
        return non_water
    
    def _draw_on_frame(self, coefs, frame, mask=None):
        sy, sx = frame.shape[:2]
        yg_pts = self._get_hom_pts(coefs, (sy, sx))
        raw_frame = frame.copy()
        for ypts in yg_pts:
            frame = cv2.line(frame, ypts[0], ypts[1], (0, 150, 255), thickness=2)
        if mask is not None:
            non_water = self._get_non_water_mask(mask, sx, sy)
            frame[non_water] = raw_frame[non_water]
        return frame

    def _draw_wireframe_on_frame(self, frame, mask, wireframe):
        sy, sx = frame.shape[:2]
        if mask is not None:
            non_water = self._get_non_water_mask(mask, sx, sy)
            wireframe[non_water] = 0
        #frame *= wireframe.astype(frame.dtype)
        frame[np.where(wireframe == 255)] = 0 #(0, 150, 255)#0.
        return frame
    
    def _display_fits(self):
        """ 
        Make a pdf with all the fits overlaid on each frame at all the 
        different tides.
        """
        cnt = 1
        hom = Homography(self.alias)
        all_dat = self.get_tide_data()
        paths = np.hstack([v['paths'] for v in all_dat.values()])
        tides = [float(p.split('/')[-2].split('tide_')[-1]) for p in paths]
        pdfName = f'{self.alias}-hom-frames.pdf'
        pdf = PdfPages(pdfName)
        tide_cnt = defaultdict(int)
        for i, (tide, path) in enumerate(zip(tides, paths)):
            if round(tide, 1) == 1.7 or tide == 0.1:
                continue
            tide_cnt[tide] += 1
            if tide_cnt[tide] > cnt:
                continue
            frame = np.array(Image.open(os.path.join(path, 'frame.jpg')))
            mask = np.load(os.path.join(path, 'mask.npy'))
            hom.tide = float(tide)
            try:
                coefs = hom.coefs_
            except:
                print('coefs_ problem in hom tide setter')
                set_trace()
            #coefs = hom.get(tide)
            #anno_frame = self._draw_on_frame(coefs, frame, mask)
            anno_frame = self._draw_wireframe_on_frame(frame, mask, hom.wireframe)
            #Image.fromarray(anno_frame).save('/home/zane/test.jpg')
            plt.figure(figsize=(9, 5))
            plt.imshow(anno_frame)
            plt.title(f'tide: {tide} ft - coefs: ({coefs[0]:.4f}, '
                      f'{coefs[1]:.4f}) - {path.split("/")[-1]}')
            plt.axis('off')
            plt.tight_layout()
            pdf.savefig()
            #if tide > 1:
            #    break
        pdf.close()
        os.system(f'convert -delay 1x5 {pdfName} {pdfName.replace(".pdf", ".gif")}')
        log.info(f'Wrote {pdfName}, {pdfName.replace(".pdf", ".gif")}')

    def _make_heatmap_gif(self, tide_bin_size=1, target_size=(720, 1280)):
        """ 
        For non face-on cameras we need to work with meshgrids, so just 
        do that now for the face-on case.
        """
        sy, sx = target_size
        hom = Homography(self.alias)
        self.tide_bin_size = tide_bin_size
        tides_df = self._get_tide_bins()
        tide0 = tides_df['tide'].min()
        tide0 = 1
        tide1 = tides_df['tide'].max()
        tides = np.linspace(tide0, tide1, int(round((tide1 - tide0) / tide_bin_size)+1))
        y, x = np.meshgrid(np.arange(0, sy), np.arange(0, sx))
        for i, tide in enumerate(tides):
            coefs = hom.get(tide)
            z = coefs[0] * y + coefs[1]
            rm = np.where(y < 95)
            z[rm] = -1
            if i == 0:
                zlim = [0, z.max()]
            plt.figure()
            heatmap = plt.pcolor(x, y, np.log(z), vmin=zlim[0], vmax=zlim[1])
            heatmap.cmap.set_under('white')
            plt.gca().invert_yaxis()
            plt.colorbar()
            plt.tight_layout()
            plt.savefig(f'tide_{tide:.2f}_mesh.jpg')
            print(f'finished {tide:.2f} grid')
            
    def _plot_interp_homs(self):
        hom = Homography(self.alias)
        plt.figure(figsize=(9.2, 6))
        colormap = plt.cm.jet
        num_plots = len(hom.tides)
        plt.gca().set_prop_cycle(plt.cycler('color', plt.cm.jet(np.linspace(0, 1, num_plots))))
        
        sy, sx = 720, 1280
        y = np.arange(0, sy)[::-1]
        x = np.arange(0, sx)
        labels = []
        for i, tide in enumerate(hom.tides):
            hom.tide = tide
            coefs = hom.coefs_
            w = coefs[0] * y + coefs[1]
            #y = y[w>0]
            #w = w[w>0]
            plt.plot(y, w)
            labels.append(f'{tide:.2f} ft')
        plt.xlabel('y-coord', fontsize=14)
        plt.ylabel('box width (pix)', fontsize=14)
        plt.legend(labels, ncol=3, loc='lower right',
                   #bbox_to_anchor=[0.5, 1.1], 
                   columnspacing=1.0, labelspacing=0.0,
                   handletextpad=0.0, handlelength=1.5,
                   fancybox=True, shadow=True)
        plt.title('Homography(tide)')
        plt.tight_layout()
        fname = f'{self.alias}-coefs.jpg'
        plt.savefig(fname)
        log.info(f'Wrote {fname}')

        
        
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--alias', default='wc-hbpierns', type=str,
                        help='you can also supply a camID')
    parser.add_argument('-b', '--bin_size', default=1, type=float,
                        help="if this is too small you'll have bad fits")
    parser.add_argument('-hor', '--horizon', action='store_true',
                        help='use the average horizon to inform fits. Not '
                        'to be used with cams that have any significant roll')
    parser.add_argument('-c', '--clip', default=0, type=float,
                        help='clip the data aggressively, even further, '
                        'this many sigma at each y bin. Not to be used '
                        'for side-angle cams unless you have tons of data')
    parser.add_argument('-f', '--faceon', action='store_true',
                        help=('If you know this cam is face-on, set this flag to suppress warping, '
                              'which should in principle detect face-on-ity independently but doesnt atm')
    )
                        
    args = parser.parse_args()
    fitter = FitHomDB(args.alias)
    tidal_fits = fitter.fit(
        clip=args.clip,
        add_horizon=args.horizon,
        tide_bin_size=args.bin_size,
        faceon=args.faceon
    )

    # Display the coefficients that were just written to disk.
    fitter._display_fits()
    fitter._plot_interp_homs()
    #fitter._make_heatmap_gif()
