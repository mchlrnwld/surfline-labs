import os
import cv2
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import pickle

from copy import deepcopy as dc
from collections import defaultdict
from PIL import Image
from matplotlib.backends.backend_pdf import PdfPages
from scipy.stats import sigmaclip
from scipy.signal import savgol_filter
from sl_camera_sensor.lib.localization.localize_mask import LocalizeMask
from sl_camera_sensor.utils.logger import getLogger
from ipdb import set_trace
import imageio

log = getLogger(__name__)

class WarpedHomDB:
    """
    This does some sanity checks on the camera FOV based on the
    pre-filtered bbox positions to verify you don't need
    to consider warping. If you do, it will do that warping/dewarping
    for you.

    The objective here is to acquire the matrix-based transformation
    that is capable of producing a variable warp as you move across
    the frame, using only the bounding box and mask data.
    """
    def is_warped(self, all_tide_data, alias_or_camid, faceon=False):
        """ 
        Do some sanity checks regarding filtered data. Namely look at
        widths as a function of y, as these should be more or less constant.
        One can also look at the shoreline - horizon across the image
        to find non-face-on states. Either one should trigger a warped
        basis projection fitting.

        Args:
            all_tide_data: parsed database as a funtion of tide
        """
        self.alias_or_camid = alias_or_camid
        self.pix_bin_width = 15
        self.sy, self.sx = self._get_frame_size(list(all_tide_data.values())[0])
        
        # Assume it's warped?? How tf to determine robustly? Move
        # that plotting somewhere else and just make a get_projective_matrices()
        # which return identities if not warped.
        # if is_warped:
        matrices = {
            tide_val: (np.eye(3) if faceon else self.fit_warp(tide_val, tide_data))
            for tide_val, tide_data in all_tide_data.items()
        }
        return matrices
        
        widths_pdf = PdfPages(f'{alias_or_camid}-widths.pdf')
        for tide_val, tide_data in all_tide_data.items():
            # Sanity check that the cam isn't rolled too much.
            if self.camera_has_roll(tide_data['horizons']):
                #set_trace()
                log.info(f'Camera is rolled at tide {tide_val}')  # TODO should be indpt of tide
            # Now check if the width distributions across the screen at constant y
            self._plot_widths_across_screen(tide_val, tide_data, widths_pdf)
        widths_pdf.close()
        log.info(f'Wrote {alias_or_camid}-widths.pdf')

    def is_identity(self, M):
        """ Boolean check if input matrix M is identity """
        return np.allclose(M, np.eye(M.shape[0]))

    def deproject_pts(self, tide_data, M):
        """
        replace keys back to the original basis and drop projection keys
        """
        if 'y_raw_deproj' not in tide_data:
            raise ValueError('No points to deproject')
        tide_data['y_raw'] = tide_data['y_raw_deproj']
        tide_data['x_raw'] = tide_data['x_raw_deproj']
        tide_data['widths_raw'] = tide_data['widths_raw_deproj']
        return tide_data
    
    def project_pts(self, tide_data, M):    
        """
        Project a vector of points to a new basis described by 3x3 matrix M
        https://docs.opencv.org/2.4/modules/imgproc/doc/ \
            geometric_transformations.html#warpperspective

        These don't have to include horizon guiding points. If they do the
        they should be unaffected unless your warp fit is wrong. We're going to fit
        the physical homography on these projected points so at some point
        you may want to add the horizon distribution.

        We want to do the fitting on 'y_raw' and 'widths_raw' keys in this
        basis. Overwrite those keys and store the orig in some other keys
        """
        tide_data['y_raw_deproj'] = tide_data['y_raw'].copy()
        tide_data['x_raw_deproj'] = tide_data['x_raw'].copy()
        tide_data['widths_raw_deproj'] = tide_data['widths_raw'].copy()

        x, y, w = tide_data['x_raw'], tide_data['y_raw'], tide_data['widths_raw']
        pts = np.column_stack((x, y))[np.newaxis].astype(np.float32)
        # We need to get the perspective of *boxes*, not individual points.
        # Do that by unstacking boxes and restacking.
        xboxes, yboxes = self._get_boxes_from_pts(pts, w)
        box_pts = np.column_stack((
            xboxes.ravel(), yboxes.ravel()))[np.newaxis].astype(np.float32)
        pts_proj = np.squeeze(cv2.perspectiveTransform(box_pts, M))
        boxes_proj = pts_proj.reshape(len(pts_proj) // 2, 4)
        assert len(boxes_proj) == len(x) == len(y)
        
        # Reset the keys in the tide_data dict and we can fit!
        tide_data['y_raw'] = boxes_proj[:, 1:4:2].mean(axis=1)
        tide_data['x_raw'] = boxes_proj[:, :4:2].mean(axis=1)

        tide_data['widths_raw'] = boxes_proj[:, 2] - boxes_proj[:, 0]
        try:
            assert np.all(tide_data['widths_raw'] > 0)
        except AssertionError:
            log.critical('You have negative widths what universe are you in')
        return tide_data
    
    def bin_at_y(self, tide_data, pix_bin_width=10):
        """
        Make a dataframe of y, x, width, var, binned about y
        """
        self.pix_bin_width = self.pix_bin_width or pix_bin_width

        df = pd.DataFrame(
            {
                'y': tide_data['y_raw'],#_no_horiz'],  # TODO what if no --horizon flag in call?
                'w': tide_data['widths_raw'],#_no_horiz'],
                'x': tide_data['x_raw']#_no_horiz']
            }
        )
        # We need to figure out the max width of this frame for bins
        sy, sx = self._get_frame_size(tide_data)
        df['ybin'] = pd.cut(
            df.y,
            np.arange(0, sy + 0.1, self.pix_bin_width),
            retbins=False
        )
        return df

    def _get_plot_as_arr(self):
        canvas = plt.gca().figure.canvas
        canvas.draw()
        buf = canvas.buffer_rgba()
        arr = np.asarray(buf)
        #Image.fromarray(arr).convert("RGB").save('np-test-fits.jpg')
        return arr

    def _plot_widths_across_screen(self, tide_val, tide_data, pdf, return_np=False):
        """
        Make a plot at a single tide of the average widths across the screen.
        """        
        df = self.bin_at_y(tide_data)
        # Assume horizon is above surf zone...
        hval = np.vstack(tide_data['horizons'])[:, 1].mean()
        dat = defaultdict(list)
        for grp, row in df.groupby('ybin'):
            if len(row) > 0 and row.y.mean() > hval:
                dat['y'].append(row.y.mean())
                #dat['x'].append(row.x.mean())  # doesn't make sense
                dat['w'].append(row.w.mean())
                dat['dw'].append(row.w.std())
                
        if len(dat) > 2:
            fig, axs = plt.subplots(figsize=(7,5))
            plt.scatter(dat['y'], dat['w'])
            plt.errorbar(dat['y'], dat['w'], yerr=dat['dw'], capsize=10)
            plt.title(f'Tide: {float(tide_val):.1f}, y-bin width {self.pix_bin_width}, 1$\sigma$')
            plt.xlabel('y-coord')
            plt.ylabel('avg box width')
            #plt.savefig('/home/kwynne/t.jpg')
            print(f'Added widths plot at tide {tide_val}')
            plt.tight_layout()

        if return_np:
            #plt.axis('off')
            plt.ylabel('Variance (loss)')
            plt.xlim([600, 1100])
            plt.title("")
            return self._get_plot_as_arr()

        pdf.savefig()

    def _get_test_frame(self, tide_data):
        return np.array(
            Image.open(
                os.path.join(tide_data['paths'][0], 'frame.jpg')
            )
        )            

    def _get_test_mask(self, tide_data):
        sz = self._get_frame_size(tide_data)
        mask = np.load(os.path.join(tide_data['paths'][0], 'mask.npy'))
        return np.array(
            Image.fromarray(mask).resize(sz[::-1][:2], Image.NEAREST)
        )              
        
    def _get_frame_size(self, tide_data):
        return self._get_test_frame(tide_data).shape[:2]
                      
    def camera_has_roll(self, horizons, pct_thresh=.8):
        """ 
        Based on some semi-large distributions of horizon measurements,
        get a boolean of whether or not the camera is excessively rolled
        """
        # Compute averages depth-wise
        #h_avg = horizons[...,1].mean(axis=1)
        #h_std = horizons[..., 1].std(axis=1)
        if 2 < horizons.ndim > 3:
            raise ValueError('Horizon vector not understood')
        if horizons.ndim == 3:
            horizons = np.vstack(horizons)
        ylocs = horizons[:, 1]
        clips, _, _ = sigmaclip(ylocs, low=4, high=4)
        self.is_rolled = clips.min() / clips.max() < pct_thresh
        return self.is_rolled
        
    def get_pts_from_vec(self, vec, buff=10):
        """ Give nx2 vector and get back 2x2 pts. """
        p0 = vec[:buff].mean(axis=0)
        p1 =vec[-buff:].mean(axis=0)
        return np.vstack((p0, p1))

    def _get_boxes_from_pts(self, pts=None, widths=None, tide_data=None):
        """ 
        I've distilled the box db too much - TODO reproduce
        locations as tuples. Assume squares for now (no y-widths in db).
        """
        if tide_data is not None:
            w = tide_data['widths_raw']
            y = tide_data['y_raw']
            x = tide_data['x_raw']
        else:
            if widths is None or pts is None:
                raise ValueError('boxes from pts needs associated widths')
            if pts.ndim > 2:
                pts = np.squeeze(pts)
            x, y = pts[:, 0], pts[:, 1]
            w = widths
        try:
            assert len(x) == len(y) == len(w)
        except AssertionError:
            set_trace()
        ylocs = np.array((y - w / 2, y + w / 2)).astype(int)
        xlocs = np.array((x - w / 2, x + w / 2)).astype(int)
        return xlocs.T, ylocs.T

    def _filter_locs(self, xlocs, ylocs, shoreline):
        """ 
        Because we don't have the original bboxes we have to 
        regenerate them from widths and somethimes they go off screen ew.
        """
        k = np.where((ylocs[:, 0] > 0) & (ylocs[:, 1] < self.sy) &
                     (xlocs[:, 0] > 0) & (xlocs[:, 1] < self.sx)
        )[0]
        ylocs, xlocs = ylocs[k], xlocs[k]
        # Also only look at dest pts below the shoreline.
        yshore_at_x = shoreline[xlocs[:,0]][:, 1]
        k = np.where(ylocs[:, 1] >= yshore_at_x)[0]
        if len(k) < 100:
            raise ValueError(
                'You have no beachpeople points below the shoreline lol your '
                'algorithm is broken. Try shoreline walk.'
            )
        return xlocs[k], ylocs[k]
    
    def get_loss(xlocs):
        """ This is essentially our loss function. No this needs to be a function of y. """
        return (xlocs[:, 1] - xlocs[:, 0]).var()

    def _sanity_check_regions(self, test_frame, shore_avg, horiz_avg, tide_val):
        ## TODO move this
        plt.figure()
        plt.imshow(test_frame)
        plt.plot(shore_avg[:, 0], shore_avg[:, 1])
        plt.plot(horiz_avg[:, 0], horiz_avg[:, 1])
        plt.tight_layout()
        savename = f'{self.alias_or_camid}-tide_{float(tide_val):.1f}-sanity-check-regions.jpg'
        plt.savefig(savename)
        log.info(f'Wrote {savename}')
        plt.close()
        
    def fit_warp(self, tide_val, tide_data, warm_up=20):
        """
        Tide data dict at a single tide. Loss value we're trying to
        minimize here is the width variance at each y-bin. There are a 
        couple approaches we can take to fit the projection matrix:

        1) Implement the mapping of pair coords -> matrix in python/torch,
        and do backprop. Prefer not to rewrite openCV code although it's
        just matrix multiplies
        2) Random walk. We know in which direction to walk, so it's really
        just a fine-tuning problem and should converge rapidly.
        """
        log.info(f'Fitting warp matrix for {float(tide_val):.1f} ft tide...')
        test_frame = self._get_test_frame(tide_data)
        
        # Average 3d delineations into single 2d matrices and CLIP THEM.
        # Deal here is the horizons should be clipped all together and
        # shorelines not, but if we have sufficient data it wont' matter
        # since one tide bin will have enough horizon data to be stat sig.
        shore_avg = self._clip_delineations(tide_data['shorelines'], avg=True)
        horiz_avg = self._clip_delineations(tide_data['horizons'], avg=True)

        #shore_avg = tide_data['shorelines'].mean(axis=0)
        #horiz_avg = tide_data['horizons'].mean(axis=0)
        # TODO possibly move this to localize_mask
        #shore_avg[:, 1] = savgol_filter(shore_avg[:, 1], 151, 1)
        #horiz_avg[:, 1] = savgol_filter(horiz_avg[:, 1], 151, 1)
        
        self._sanity_check_regions(test_frame, shore_avg, horiz_avg, tide_val)
        xlocs,ylocs = self._get_boxes_from_pts(tide_data=tide_data)
        xlocs, ylocs = self._filter_locs(xlocs,ylocs, shore_avg) # Sample src pts from these.
        
        spts = self.get_pts_from_vec(shore_avg)
        hpts = self.get_pts_from_vec(horiz_avg) # hpts will never change on our walk.
        src_pts = np.vstack((hpts, spts))

        # Get an initial guess from shoreline location.
        shore_skew = shore_avg.copy()
        shore_skew[:, 1] = shore_skew[:, 1].max()
        s_skew = self.get_pts_from_vec(shore_skew)
        dst_pts = np.vstack((hpts, s_skew)).astype(np.float32)
        M = cv2.getPerspectiveTransform(
            src_pts.astype(np.float32), dst_pts
        )
        return M
        """
        # To make the gif, do a loop.
        step_size = 5
        p0 = src_pts[-1][-1]
        p1 = dst_pts[-1][-1]
        n_steps = (p1-p0)/step_size
        steps = np.linspace(p0, p1, int(n_steps))
        wframes_for_gif = []
        otide_data = dc(tide_data)
        for istep in steps:
            dst_pts[-1][-1] = istep
            # Just a quick test here
            M = cv2.getPerspectiveTransform(
                src_pts.astype(np.float32), dst_pts
            )
            tide_data = self.project_pts(otide_data, M)
            wplot = self._plot_widths_across_screen(tide_val, tide_data, None, return_np=True)
            # convert RGBA to rgb
            wplot = wplot[..., :3]
            wplot = np.array(Image.fromarray(wplot).resize((480, 360), Image.NEAREST))
            frame = self._get_test_frame(tide_data)
            warped_frame = cv2.warpPerspective(frame, M, frame.shape[:2][::-1])
            warped_frame[:360, -480:, :] = wplot
            wframes_for_gif.append(warped_frame)

        # For the last one, include a frame without the plot
        warped_frame = cv2.warpPerspective(frame, M, frame.shape[:2][::-1])
        tup = wframes_for_gif, warped_frame, self._get_test_frame(tide_data), self._get_test_mask(tide_data)
        with open('puerto-gif-data.pkl', 'wb') as f:
            pickle.dump(tup, f)
        print(f'wrote puerto-gif-data.pkl')
        #imageio.mimsave('/home/kwynne/t.gif', wframes_for_gif)
        set_trace()
        """
        #test_pts = np.column_stack((xlocs.ravel(), ylocs.ravel())).astype(np.float32)
        #test_pts_proj = cv2.perspectiveTransform(test_pts[np.newaxis], M)
        
        #return M, wframes_for_gif

    """
        set_trace()
        Ms = {}
        loss = 100
        cnt = 0
        
        set_trace()
        #warm_up_path = np.linspace()
        while loss > 10:
            if cnt < warm_up:
                # Do a warm-up, in other words start walking in the direction we
                # know is correct. This should effectively inhibit degeneracies.
                # Only modify the right two bottom cols of dst_pts
                pass
            #M = cv2.getPerspectiveTransform(src_pts, dst_pts)


        


            def _get_projective_matrix_from_pts(self, shore, horizon):
        s = self.get_pts(shore)
        h = self.get_pts(horizon)
        src_pts = np.vstack((s, h)).astype(np.float32)
        # Force all the shoreline y coords to be the max val.
        shore_skew = shore.copy()
        shore_skew[:, 1] = shore_skew[:, 1].max()
        s_skew = get_pts(shore_skew)
        dst_pts = np.vstack((s_skew, h)).astype(np.float32)
        set_trace()
        M = cv2.getPerspectiveTransform(src_pts, dst_pts)
        return M
"""
