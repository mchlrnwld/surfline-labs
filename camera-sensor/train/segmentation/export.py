# One needs to edit the torchvision classes to return tensors instead of dicts,
# since trtorch needs torch 1.5.0, which doesn't let you override this when
# making a torch script (1.6 does).
#
# $ python export.py --model_name deeplabv3_resnet50 --weights_path /path/to/model.pth
#
# which will output a .pt torchscript and .ts trtorch script
#
import trtorch
import torch
import vision_model
import argparse

def export(model_name, weights_path, batch_size):
    """ Export weights + model -> scripts.
    
    :param model_name: model name to construct model class instance.
    efficientnet_b0, deeplabv3_resnet50 or 101
    :param weights_path: full path to final model checkpoint
    :returns: writes .pt and .ts model scripts to disk, in same dir as weights.
    """
    model = vision_model.get(model_name, weights=weights_path, num_classes=9)
    model = model.eval().to('cuda')
    # Make a trace
    tensor = torch.rand(batch_size, 3, 576, 1024).to('cuda')
    model_trace = torch.jit.trace(model, tensor)
    torch.jit.save(model_trace, weights_path.replace('.pth', '.pt'))
    print(f"Exported jit script to {weights_path.replace('.pth', '.pt')}")
    # Try to make trt trace
    params = { 
         "input_shapes": [(batch_size, 3, 576, 1024)], 
         "op_precision": torch.half, # Run with FP16 
         "workspace_size": 1 << 20 
    }         
    print('Compiling TRT script...')
    trt_model = trtorch.compile(model_trace, params)
    # Try to pass forward on trt script
    tensor = tensor.half()
    result = trt_model(tensor)
    # Save it!
    torch.jit.save(trt_model, weights_path.replace('.pth', '.ts'))
    print(f"Wrote TRT script to {weights_path.replace('.pth', '.ts')}")
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--model_name', default='deeplabv3_resnet50', type=str)
    parser.add_argument('--weights_path',
                        default='/data/segmentation/torch/zoo/deeplabv3_resnet50_576p/model_9.pth')
    parser.add_argument('--batch_size','-b',
                        default=8, type=int)
    args = parser.parse_args()
    export(args.model_name, args.weights_path, args.batch_size)
