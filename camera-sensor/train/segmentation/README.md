# PyTorch Segmentation Model Training

At the moment, models can be trainined in any torch version >= 1.4.0, but exporting
to TRTorch, not surprisingly, requires a very specific combination of CUDA, torch and
torchvision. TRTorch currently needs CUDA 10.2 but none of the frameworks have
precompiled binaries against this version, so the work around is to symlink
the cuda runtime 10.2 to 10.1, which appears to work fine.

In addition, the `torch.jit.script` api in 1.6.0 has a keyword that lets you
script dict outputs, but 1.5 does not. The simplest work around here is to modify
the torchvision source code to return tensors in `.forward` rather than a dict.
Note that you must script the graphs with the same torch version TRTorch wants,
which is currently 1.5. They don't seem to be compatible, so scripting
in 1.6 won't load in 1.5. If you're getting seg faults with `torch.jit.load`
check you versions. I suspect as we see upgrades these work around will dissolve, but that's
how to do it now.

EfficientNet also requires a modified open-source git repo, so at the moment
I'm keeping the training scripts out of here as they'll just pollute this space, and
won't run without a bunch of modified code that can't be easily put in a docker file.

The tweaks to get this running are in the `sl_camera_sensor` predict directory Dockerfile

TODO update accuracies and fps table