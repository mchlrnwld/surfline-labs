<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [camera-sensor](#camera-sensor)
  - [Layout](#layout)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# camera-sensor
A collection of machine learning modules for intelligent surf cameras.

## Layout
```
.
├── README.md                   -- You are here
├── graphs                      -- Trained & validated computational graphs/model weights (DVC pointers)
│  ├── detection 	        -- TensorFlow object detection serialized graphs & label maps
│  │  └── label_map.pbtxt.dvc  
│  │  └── saved_model.pb.dvc
│  ├── segmentation             -- PyTorch model weights & label maps
│  │  └── label_map.pbtxt.dvc
│  │  └── saved_model.pth.dvc
│  └── visiblity                -- XGB .model weights & label map
│     └── label_map.pbtxt.dvc
│     └── saved_model.model.dvc
├── predict                     -- Inference-only code - model inference & postprocesing
│  └── sl_camera_sensor         -- (*) pip package, so we can easily deploy stacks of models
│     ├── nets                  -- Infrence classes, which all take RGB uint8 frames via `.predict()`
│     │  └── detection          -- TF object detection inference class
│     │  └── segmentation       -- Torch semantic segmentation inference class
│     │  └── visibility         -- visibility model inference class
│     ├── utils                 -- Common frame & misc utilities
│     └── lib                   -- Post-inference models, which ingest the outputs of classes in `nets/`
│        └── tracking           -- Surfer et al. tracking (detection)
│        └── crowd_counting     -- Crowd counting methods (detection)
│        └── homography         -- Various R&D homography approaches
│        └── smart_highlights   -- From 10m rewind clips (segmentation & detection)
│        └── wave_shapes        -- Closed-out/peakiness estimation (segmentation)
│        └── wave_heights       -- Francisco's bathymetry? (segmentation)
│        └── set_detection      -- (segmentation)
│        └── shoreline          -- Run-up and overtopping detection (segmentation)
│        └── surface_texture    -- TBD - wind estimation from water surface texture
└── train                       -- Training modules
   ├── detection                -- (*) pip package for TF1.0 object detection training
   │  └── data_collection       -- scripts to collect data from S3 & preprocess
   ├── segmentation             -- (*) pip package for PyTorch1.4 deeplabV3 training
   │  └── data_collection
   └── visibility               -- (*) pip package for XGB visibility model training
      └── data_collection
```

The primary motivation behind the layout above is maximizing the ease of calling multiple
inferences in a stack of calls, (with zero dependencies on raw training data). See ./predict/sl

``` python
vis = sl_camera_sensor.nets.visibility.VisibilityModel().predict(frame)
if vis != 'Clear':
   break
dets = sl_camera_sensor.nets.detection.DetectionModel().predict(frame)
masks = sl_cameara_sensor.nets.detection.SegmentationModel().predict(frame)
crowd = sl_camera_sensor.lib.crowd_counting.CrowdCounting().get_counts(dets)
is_overtopping = sl_camera_sensor.lib.shoreline.Shoreline().get_shoreline(masks)
wave_tracks = sl_camera_snsor.lib.wave_tracking.WaveTracker()(masks).pop_tracks()
```

... etc. In the end, machine surf intelligence will come from the
sum of all our models, so being able to call them
in-line, from a single package, makes network and model inference much easier IMO.
Working from the same package also makes the R&D workflow smoother - when I'm running
two models at once, or building an additional model into a stack, existing models
are often slightly modified. Doing this in multiple packages adds a lot of day-to-day overhead.