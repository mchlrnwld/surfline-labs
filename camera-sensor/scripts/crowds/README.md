# Crowd Counting

`count_crowds.py` allows us to: 
1. Find and extract the first panning frames from a stream.
2. Stitch those frames together to create a panoramic image.
3. Run inference on 720p crops with our detection model and localization to get counts of each class.
4. Save each of those crops to the same directory as the stream, along with raw and annotated panoramic frames with counts.

To run the script on the command line, provide the path to a stream that contains a single pan you would like to extract using the `-s` flag:
```
python count_crowds.py -s /path/to/stream/<alias>.stream.<timestamp>.mp4
```

Other flags allow you to specify (example below):
1. `-d`: the detection threshold.
2. `-r`: fps to search for and extract pannning frames.
3. `-f`: optical flow threshold to determine if two successive frames are panning.

```
python count_crowds.py -s /path/to/stream/<alias>.stream.<timestamp>.mp4 -d 0.25 -r 2 -f 3
```
