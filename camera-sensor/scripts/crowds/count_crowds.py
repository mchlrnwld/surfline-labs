import cv2
from PIL import Image
import os
from pathlib import Path
from typing import Tuple, Optional, Union, List
import imutils
import numpy as np
import argparse
import math
import time

from sl_camera_sensor.nets.detection.detection_model import DetectionModel
from sl_camera_sensor.nets.segmentation.segmentation_model import SegmentationModel
from sl_camera_sensor.lib.localization.localize_dets import LocalizeDets
from sl_camera_sensor.lib.crowds.clip_above_sigma import clip_above_sigma
from predictors.camera_sensor.segmentation.train import myutils
from sl_object_detection.annotator import boxes


def resize(
    frame: np.ndarray,
    size: Tuple[int, int],
    method: Optional[str] = "nearest"
) -> np.ndarray:
    """
    Resize frame to size.
    Args:
        frame (np.ndarray): array representing the frame.
        size (tuple): tuple of the shape to resize to.
        method (str): resize method.

    Returns:
        np.ndarray: Resized frame
    """
    return np.array(
        Image.fromarray(frame).resize(size, getattr(Image, method.upper()))
    )


def analyze_flow(
    img: np.ndarray,
    flow: np.ndarray,
    step: Optional[int] = 8
) -> Tuple[np.ndarray, np.ndarray, list]:
    """
    Function to calculate optical flow trajectories. The code also allows to specify a
    step value. The greater the value, the more sparse the calculation and visualization
    Args:
        img (np.ndarray): Image.
        flow (np.ndarray): Optical flow.
        step (int): Measurement of sparsity.

    Returns:
        angles (np.ndarray): Array of angles of optical flow lines to the x-axis.
        translation (np.ndarray): Array of length values for optical flow lines.
        lines (np.ndarray): List of actual optical flow lines (where each line
        represents a trajectory of a particular point in the image)
    """
    h, w = img.shape[:2]
    y, x = np.mgrid[step / 2:h:step, step / 2:w:step].reshape(2, -1).astype(int)
    fx, fy = flow[y, x].T
    lines = np.vstack([x, y, x + fx, y + fy]).T.reshape(-1, 2, 2)
    lines = np.int32(lines + 0.5)

    starts = lines[:, 0]
    ends = lines[:, 1]
    x1s = starts[:, 0].astype(int)
    y1s = starts[:, 1].astype(int)
    x2s = ends[:, 0].astype(int)
    y2s = ends[:, 1].astype(int)
    angles = np.arctan2(y1s - y2s, x2s - x1s) * 180 / np.pi
    translation = np.hypot(x2s - x1s, y1s - y2s)

    return np.array(angles), np.array(translation), lines


def draw_flow(
    img: np.ndarray,
    flow: np.ndarray,
    step: Optional[int] = 16
) -> np.ndarray:
    """
    Draw optical flow vectors on an image.
    Args:
        img (np.ndarray): Image to be drawn on.
        flow: Output of cv2 optical flow function.
        step (int): Step size.

    Returns:
        np.ndarray: Image overlaid with optical flow vectors.
    """
    arrows = []
    h, w = img.shape[:2]
    y, x = np.mgrid[step/2:h:step, step/2:w:step].reshape(2, -1).astype(int)
    fx, fy = flow[y, x].T
    lines = np.vstack([x, y, x+fx, y+fy]).T.reshape(-1, 2, 2)
    lines = np.int32(lines + 0.5)
    vis = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    cv2.polylines(vis, lines, 0, (0, 255, 0))
    for (x1, y1), (x2, y2) in lines:
        arrows.append([x1, y1, math.sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1))])
        cv2.circle(vis, (x1, y1), 1, (0, 255, 0), -1)
    return vis


def is_panning(
    prev: np.ndarray,
    cur: np.ndarray,
    flow_thresh: Optional[int] = 3
) -> bool:
    """
    Uses optical flow to determine if there is panning occurring between two frames.
    Args:
        prev (np.ndarray): First frame read using cv2.
        cur (np.ndarray): Second frame read using cv2.
        flow_thresh (float): Threshold of mean optical flow vector length used to
        determine whether the camera is steady or not.

    Returns:
        bool: True if panning, false if stationary.
    """
    prev_gray = cv2.cvtColor(prev.copy(), cv2.COLOR_BGR2GRAY)
    cur_gray = cv2.cvtColor(cur.copy(), cv2.COLOR_BGR2GRAY)

    # Calculate optical flow
    flow = cv2.calcOpticalFlowFarneback(
        prev_gray, cur_gray, None, 0.5, 3, 15, 3, 5, 1.2, 0
    )

    # Calculate trajectories and analyse them
    angles, translation, lines = analyze_flow(prev_gray, flow)
    mean_vector_length = np.mean(translation)
    panning = mean_vector_length > flow_thresh

    return panning


def crop_pano(stitched: np.ndarray) -> np.ndarray:
    """
    When cv2 stitches frames together, it creates a panorama with black regions
    around it. This function takes that stitched frame and extracts the maximum inner
    rectangle of the panorama. Adapted from:
    https://github.com/insamniac/image-stitching-opencv24-python/blob/master/stitch.py
    Args:
        stitched (np.ndarray): Stitched panorama.

    Returns:
        stitched (np.ndarray): Maximum inner rectangle of the original panorama.
    """
    stitched = cv2.copyMakeBorder(
        stitched, 10, 10, 10, 10,
        cv2.BORDER_CONSTANT, (0, 0, 0)
    )

    # Convert the stitched image to grayscale and threshold it
    gray = cv2.cvtColor(stitched, cv2.COLOR_BGR2GRAY)
    thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY)[1]

    # Find the largest contour which will be the contour/outline of the stitched image
    counts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    counts = imutils.grab_contours(counts)
    max_counts = max(counts, key=cv2.contourArea)

    # Create mask with rectangular bounding box of the stitched image region
    mask = np.zeros(thresh.shape, dtype="uint8")
    (x, y, w, h) = cv2.boundingRect(max_counts)
    cv2.rectangle(mask, (x, y), (x + w, y + h), 255, -1)

    # Create mask to serve as minimum rectangular region
    min_rect = mask.copy()
    # Create mask to serve as a counter for pixel removal
    sub = mask.copy()

    # Keep looping until there are no non-zero pixels left in the subtracted image
    while cv2.countNonZero(sub) > 0:
        # Erode the minimum rectangular mask
        min_rect = cv2.erode(min_rect, None)
        # Subtract the thresholded image from the minimum rectangular mask
        sub = cv2.subtract(min_rect, thresh)

    # Find contours in the minimum rectangular mask
    counts = cv2.findContours(min_rect.copy(), cv2.RETR_EXTERNAL,
                              cv2.CHAIN_APPROX_SIMPLE)
    counts = imutils.grab_contours(counts)
    max_counts = max(counts, key=cv2.contourArea)
    (x, y, w, h) = cv2.boundingRect(max_counts)

    # Use the bounding box coordinates to extract the our final stitched image
    stitched = stitched[y:y + h, x:x + w]

    return stitched


def get_pano(
    stream_path: Path,
    fps: Optional[int] = 1,
    flow_thresh: Optional[float] = 3
) -> np.ndarray:
    """
    Find panning frames from stream and stitch together to make panoramic image. This
    method also accounts for false positives and negatives returned from
    is_panning() while searching through the video.

    Args:
        stream_path (Path): Path to stream.
        fps (int): FPS to extract frames to search for pans.
        flow_thresh (float): Threshold to determine panning with optical flow vectors.

    Returns:
        stitched (np.ndarray): Stitched panoramic image.
    """
    start_search = time.time()
    cap = cv2.VideoCapture(str(stream_path))
    frame_rate = int(cap.get(cv2.CAP_PROP_FPS))
    extract_rate = int(frame_rate // fps)
    ret, prev = cap.read()
    frame_num = 0
    panning_images = []
    panning_candidates = []
    end_pan_candidates = []
    found_pan = False
    print('Searching for panning frames...')
    while True:
        ret, cur = cap.read()
        if not ret:
            break
        elif frame_num != extract_rate:
            frame_num += 1
            continue

        panning = is_panning(prev, cur, flow_thresh=flow_thresh)
        if found_pan and not panning:
            if len(end_pan_candidates) == 3:
                break
            else:
                end_pan_candidates.append(prev.copy())
        elif found_pan and panning:
            if end_pan_candidates:
                panning_images.extend(end_pan_candidates)
                panning_images.append(prev.copy())
                end_pan_candidates = []
            else:
                panning_images.append(prev.copy())
        elif panning and len(panning_candidates) > 3:
            panning_candidates.append(prev.copy())
            panning_images.extend(panning_candidates)
            found_pan = True
        elif panning:
            panning_candidates.append(prev.copy())

        prev = cur.copy()
        frame_num = 0
    print(f'Found panning frames in {time.time() - start_search} seconds.')

    # Stitch the images into one long panorama
    stitcher = cv2.Stitcher_create()
    print('Stitching...')
    (status, stitched) = stitcher.stitch(panning_images)
    print('Stitching successful.') if not status else print('Stitching failed.')
    stitched = crop_pano(stitched)
    return stitched


def run_segmentation(
    frames: List[np.ndarray],
    seg_bot: SegmentationModel
) -> Tuple[List[np.ndarray], List[np.ndarray]]:
    """
    Runs the SegmentationModel on a list frames.
    Args:
        frames (list): Frames to be segmented.
        seg_bot (SegmentationModel): Segmentation model to be used.

    Returns:
        (list, list): Tuple of annotated frames and corresponding masks
    """
    annotated = []
    masks = []
    for frame in frames:
        mask = seg_bot.predict(frame, output_size=frame.shape[:-1])
        mask = np.squeeze(mask)
        mask = resize(mask, (1280, 720))
        frame = resize(frame, (1280, 720))

        # Overlay the segmentation prediction on the frame, save the overlay/mask
        if frame.shape[:2] != mask.shape[:2]:
            raise ValueError('Shape mismatch')
        overlaid = myutils.overlay(frame, mask, return_overlay=True)
        annotated.append(overlaid)
        masks.append(mask)
    return annotated, masks


def main(
    stream_path: Path,
    detection_thresh: Optional[float] = 0.2,
    fps: Optional[int] = 1,
    flow_thresh: Optional[float] = 3
) -> None:
    """
    Given a stream from a panning camera, this will extract the first panorama from
    the stream and run detection on 720p crops. Each of those crops will be
    saved to the same directory as the stream, along with raw and annotated panoramic
    frames.
    Args:
        stream_path (Path): Path object representing the path to the panning stream
        we want to crowd count.
        detection_thresh (float): Detection threshold.
        fps (int): Rate to look for and extract panning frames.
        flow_thresh (float): Threshold for optical that determines if a cam is panning.

    Returns:
        None
    """
    alias = stream_path.name.split('.')[0]
    timestamp = stream_path.name.split('.')[1]

    # Get stitched panorama
    stitched = get_pano(stream_path, fps, flow_thresh)

    # Create 720p crops from the panorama
    pano_width = np.array(stitched).shape[1]
    det_bot = DetectionModel()
    seg_bot = SegmentationModel()
    crops = []
    for i in range(math.ceil(pano_width / 1280)):
        x, y, w, h = (1280 * i), 0, 1280, 720
        cropped = stitched[y:y+h, x:x+w]
        padded = np.zeros((720, 1280, 3), dtype=np.uint8)
        padded[:cropped.shape[0], :cropped.shape[1]] = cropped
        crops.append(padded)

    # Run detection on each crop
    detections = det_bot.predict(crops)

    # Run segmentation on each crop
    seg_annotated, masks = run_segmentation(crops, seg_bot)

    # Localize detections to fix misclassified beach people
    localizer = LocalizeDets(
        dets_label_map=det_bot.label_map,
        mask_label_map=seg_bot.label_map,
    )
    detections = localizer(detections, np.asarray(masks))

    # Annotate the crops
    annotated = boxes.label_frames_from_dict(
        crops, detections, detection_thresh=detection_thresh, return_pil=True,
        skip_scores=True
    )

    # Get counts for each class
    class_map = det_bot.label_map
    counts = dict.fromkeys(class_map, 0)
    for det in detections:
        for cls in class_map:
            counts[cls] += np.sum(
                (det['detection_scores'] >= detection_thresh) &
                (det['detection_classes'] == class_map[cls])
            )

    # Save annotated crops and convert to PIL
    annotated_pil = []
    for i in range(len(annotated)):
        cv2.imwrite(
            str(stream_path / f'{alias}.{timestamp}.crop{i}.bbox.jpg'),
            np.array(annotated[i])
        )
        pil = Image.fromarray(cv2.cvtColor(np.array(annotated[i]), cv2.COLOR_BGR2RGB))
        annotated_pil.append(pil)

    # Stitch annotated frames back together
    widths, heights = zip(*(i.size for i in annotated_pil))
    total_width = sum(widths)
    max_height = max(heights)
    combined = Image.new('RGB', (total_width, max_height))
    x_offset = 0
    for im in annotated_pil:
        combined.paste(im, (x_offset, 0))
        x_offset += im.size[0]
    combined = cv2.cvtColor(np.array(combined), cv2.COLOR_RGB2BGR)
    annotated_pano = combined[0:stitched.shape[0], 0:stitched.shape[1]]

    # Overlay counts on the annotated pano
    text = list(counts.keys())
    for i in range(len(text)):
        text[i] = f'{text[i]}: {counts[text[i]]}'
    offset = 30
    x, y = 25, 50
    for idx, lbl in enumerate(text):
        cv2.putText(
            annotated_pano, str(lbl), (x, y + offset * idx),
            cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 0), 2
        )

    # Save annotated pano and raw extracted pano
    cv2.imwrite(
        str(stream_path / f'{alias}.{timestamp}.detection.stitched.jpg'),
        annotated_pano
    )
    cv2.imwrite(str(stream_path / f'{alias}.{timestamp}.stitched.jpg'), stitched)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    # TODO(zane): Add method to find desired panning stream to crowd count. Add
    #  additional parameters.
    parser.add_argument(
        '-s', dest='stream_path', default=None, required=True,
        type=str, help='Specify path to panning stream for crowd counting.'
    )
    parser.add_argument(
        '-d', dest='detection_thresh', default=0.2,
        type=float, help='Specify detection confidence threshold.'
    )
    parser.add_argument(
        '-r', dest='fps', default=1,
        type=float, help='Specify rate to look for panning frames.'
    )
    parser.add_argument(
        '-f', dest='flow_thresh', default=2,
        type=float, help='Specify optical flow panning threshold.'
    )
    args = parser.parse_args()
    main(
        Path(args.stream_path), args.detection_thresh,
        args.fps, args.flow_thresh
    )
