# Spectra from Partitions

This project creates a 2 dimensional spectra from the WW3 partition data. The main objective of the process is to inject the partition data directly or reading it from a grid file, create a JONSWAP spectrum for each sigH/Tp combination and then apply a directional discretization based on the peak direction and directional spread of each partition. It can also produce a txt file with the WW3 format, ready to be used as boundary conditions on a WW3 or SWAN run.

# Requirements

The necessary requirements are defined in the file `requirements.txt`

# Testing

The project includes some very basic unit testing. To run:

```bash
pip install --editable .
pytest
```
