"""
Converts a CSV of partition data into synthetic spectra format and saves

Expects a CSV of LOTUS ML training data with the following columns:

SpotID
UnixTime
LotusSigH
--- FOR ALL FOUR PARTS part1 .. part4 ----
LotusSigH_part1
LotusTp_part1
LotusPdir_part1
LotusSpred_part1
LotusBreaking_part1
LotusPdir_norm_part1
------------------------------------------
GFS_wind_speed
GFS_wind_dir
GFS_wind_dir_norm
optimal_direction
human_report_surf_min
human_report_surf_max
human_report_surf_occasional
human_report_surf_condition
human_reporter
"""
import pandas as pd
from argparse import ArgumentParser
from spectra.spectra_from_partitions import create_2d_spectra


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument("input_filename")
    parser.add_argument("-o", "--output", dest="output_filename")
    args = parser.parse_args()

    dat = pd.read_csv(args.input_filename, nrows=1000)

    # Map columns into array rows
    sigh = dat[dat.filter(like='LotusSigH_').columns].fillna(0).values
    tp = dat[dat.filter(like='LotusTp_').columns].fillna(0).values
    pdir = dat[dat.filter(like='LotusPdir_').columns].fillna(0).values
    spread = dat[dat.filter(like='LotusSpred_').columns].fillna(0).values
    total_sigh = dat.LotusSigH.fillna(0).values

    for i in range(0, len(sigh)):
        print(total_sigh[i])
        [spec_data, synthetic_sigh] = create_2d_spectra(sigh[i], tp[i],
                                                        pdir[i], spread[i],
                                                        total_sigh[i])
        print(spec_data.shape)
        # todo - output the spectra to a file

