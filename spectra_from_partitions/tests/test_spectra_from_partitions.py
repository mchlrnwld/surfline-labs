import pytest
import numpy as np
from spectra.spectra_from_partitions import create_2d_spectra


def test_sigh_calc():

    hs_part = np.asarray([1.77, 0.76, 0.27])
    tp_part = np.asarray([10.95, 8.32, 10.62])
    pdir_part = np.asarray([182, 106, 166])
    dspread_part = np.asarray([14, 12, 6])
    overall_sigh = 1.95

    [spec_data, synthetic_sigh] = create_2d_spectra(hs_part, tp_part, pdir_part,
                                                    dspread_part, overall_sigh)

    assert spec_data.shape == (29, 36)
    assert round(synthetic_sigh, 2) == overall_sigh
