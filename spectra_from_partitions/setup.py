import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="surfline-spectra",
    version="0.0.1",
    author="Francisco Silva",
    author_email="francisco@surfline.com",
    description="Utilities to convert spectral wave formats",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/Surfline/surfline-labs",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)