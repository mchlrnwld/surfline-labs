# Labs Utils

A collection of general helper utilities to access MSW & SL APIs for things we want semi-regularly.
Trying to put these in one place so I don't have to keep searching for snippets in Slack.

This is essentially a consolidation of a couple of the services from [wavetrack-services](https://github.com/Surfline/wavetrak-services) - [cameras-service](https://github.com/Surfline/wavetrak-services/tree/master/services/cameras-service), [spots-api](https://github.com/Surfline/wavetrak-services/tree/master/services/spots-api), and a parser for retrieving historical spot reports, which do contain a lot of valuable data in one spot.

Things I often encounter that I can never remember:

* Camera ID / alias / spot ID mappings. These are often changing so local databse won't help.

* Metadata for spots - names, locations, timezones

* Historical tides - script I have now is only good for HB and cannot be easily generalized.

* Report text - still can't find these but the solution is partly here.

* Forecast partitions. There are a few APIs for these.

* Historical surf heights and ratings - currently in `surfline-science/science/data-lake` but
I think the `spotReportsViews.json` nicely supplants this.

## Reports
Historical data from spots looks like all the non computer vision data we could ever want,
all in one place (apart from live wind), including text reports, reported wave heights,
historical partitions, tides, weather, air and water temps, & spot details. 

These files are named `spotReportViews.json`, located in the s3 bucket `wt-data-lake-prod`,
prefixed like ` /2019/02/02/01/` (YYYY/MM/DD/HH). In every hour directory is a json file
called `spotReportViews.json`, a snapshot of the “spot report view” which should have all
the attributes here: http://services.surfline.com/kbyg/spots/reports?spotId=5842041f4e65fad6a770882a

## Historical Tides
These are contained in the `spotReportViews.json` data, which you can access directly by calling
```python
reports.get_report('5a9d8a84b0f634001ada2d1e', datetime.datetime(2021,...))
# or
tide = reports.get_tide('5a9d8a84b0f634001ada2d1e', datetime.datetime(2021,...))
```

## Spots
Typically I want the associated camera IDs, aliases, lat-lons and names of a spot.

### Spot Info
To get spot info from a spotID:
``` python
from labs_utils import spots
camID, name, location = spots.spots.get_spot_info('5a9d8a84b0f634001ada2d1e', 'cams', 'name', 'location')
```
which returns `[['5a99977bf1898800126798f2'], 'Lake Worth Pier Southside', {'type': 'Point', 'coordinates': [-80.03577704430852, 26.612151266770404]}]`

### Camera IDs -> spot IDs
```python
spots.get_spotid_from_camid(camid)
```

### Camera ID -> alias
```python
spots.get_alias(camid)
```
### Alias -> cameraID
```python
spots.get_camid(alias)
```
### All the camera IDs that belong to a spot, from either a spot ID or cam ID
```python
spots.get_camids(spotid)
# or
spotid = spots.get_spotid(camid)
spots.get_camids(spotid)
```
