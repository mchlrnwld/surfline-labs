# This queries S3 and retrieves historical data from spotReportViews.json files.
# At the moment I'm only using this for historical tide levels, but we can
# potentially use this for a lot more. See all the fields returned when you
# call reports.get_report(spotid, date)
#
import os
import numpy as np
import pandas as pd
import boto3
import json
import datetime
import logging
import pytz

from typing import Union
from scipy.interpolate import interp1d
from labs_utils import spots

log = logging.getLogger()


def get_wave_hts(spotid: str, date: datetime.datetime) -> dict:
    """
    Get the historical wave height of a spot at some timestamp. If timestamp is
    between 5am and 11am local time, a morning report will be queried at 9am. If
    timestamp is between 11am and 9pm local time, an afternoon report will be queried at
    3pm.
    :param spotid: 24 char string spotID
    :param date: datetime timestamp
    :returns: wave heights dict
    """
    local_tz = pytz.timezone(spots.get_spot_info(spotid, 'timezone')['timezone'])
    local_requested_dt = date.replace(tzinfo=pytz.utc).astimezone(local_tz)
    year = local_requested_dt.year
    month = local_requested_dt.month
    day = local_requested_dt.day
    hour = local_requested_dt.hour
    if 5 <= hour <= 11:
        morning_dt = local_tz.localize(
            datetime.datetime(year=year, month=month, day=day, hour=9)
        )
        rpt = get_report(spotid, morning_dt.astimezone(pytz.utc))
    elif 11 < hour <= 21:
        afternoon_dt = local_tz.localize(
            datetime.datetime(year=year, month=month, day=day, hour=15)
        )
        rpt = get_report(spotid, afternoon_dt.astimezone(pytz.utc))
    else:
        raise ValueError('Input datetime must be between 5am – 9pm local time.')
    hts = rpt['waveHeight']
    hts['datetime'] = datetime.datetime.strptime(
        rpt['updatedAt']['$date'].split('.')[0], '%Y-%m-%dT%H:%M:%S'
    )
    hts['timestamp'] = hts['datetime'].timestamp()
    hts['tide'] = get_tide(spotid, date)
    return hts


def get_tide(spotid: str, date: datetime.datetime) -> list:
    """ 
    Get the historical tide of a spot at some timestamp
    :param spotid: 24 char string spotID
    :param date: datatime timestamp
    :returns: tide level in feet. If the nearest returned tide timestamp 
    is larger than one hour away from the requested timestamp,
    this tries to interpolate given the dict_keys(['previous', 'current', 'next'])
    in report['tide'] block.
    """
    report = get_report(spotid, date)
    try:
        assert abs(report['tide']['current']['timestamp'] - date.timestamp()) < 3600
        curr_tide = report['tide']['current']['height']
    except AssertionError:
        # In this case, try to interpolate. We're given previous, current
        # and next keys in the tide attribute.
        tides = report['tide']
        heights, times = zip(
            *[(tides[k]['height'], tides[k]['timestamp']) for k in tides]
        )
        curr_tide = float(
            interp1d(times, heights, fill_value='extrapolate')(date.timestamp())
        )
    return curr_tide


def get_report(spotid: str, date: Union[datetime.datetime, str]) -> list:
    """ 
    Get a single spot report for some date, looking in the wt-data-lake-prod
    bucket.
    :param spotid: 24 length spotid string
    :param date: YYYY/MM/DD/HH string of report date you want
    :returns: full text report for single spot & day, as a list since
    there may be more than one report per day.
    """
    if not isinstance(date, datetime.datetime):
        try:
            date = datetime.datetime.strptime(date, '%Y/%m/%d/%H')
        except ValueError:
            raise ValueError('Input date as %Y/%m/%d/%H, ex 2021/01/31/08')
    profile_name = 'surfline-prod'
    bucket_name = 'wt-data-lake-prod'
    # From here it goes YYYY/MM/DD/HH/spotReportViews.json
    key_name = 'product/surfline/spotreportviews/'
    boto3.setup_default_session(profile_name=profile_name)
    s3 = boto3.client('s3')
    bucket = boto3.session.Session(profile_name=profile_name).resource('s3')
    pfx = key_name + date.strftime('%Y/%m/%d/%H') + '/'
    obj_key = pfx + 'spotReportViews.json'
    dat = s3.select_object_content(
        Bucket=bucket_name,
        Key=obj_key,
        ExpressionType='SQL',
        Expression="select * from s3object s where s._id.$oid = '" + spotid+ "' limit 1",
        InputSerialization={'JSON': {"Type": "Lines"}},
        OutputSerialization={'JSON': {}}
    )
    all_dat = []
    for event in dat['Payload']:
        if 'Records' in event:
            records = event['Records']['Payload'].decode('utf-8')
            data = json.loads(records)
            all_dat.append(data)
    return all_dat[0] if len(all_dat) == 1 else all_dat
