#
# This is a script to query the sessions data using pymongo
# For examples on how to query and filter, see:
# https://docs.mongodb.com/manual/reference/sql-comparison/
# https://pymongo.readthedocs.io/en/stable/tutorial.html
# Requirements:
# prod mongo dB access (username + password)
# Python 3.6+

# Example: Get all session IDs and waveCounts for sessions with more than 200 wavecounts:
# query = {'waveCount': {"$gt": 200}} # queries all sessions with waveCount>200
# projection = {}
# projection["_id"] = 1.0 # returns _id
# projection["waveCount"] = 1.0 # returns waveCount
# client, collection = initialize_collection(username, pwd)
# doc = query_collection(collection, query, projection, iterative=True)
# savejson(doc, 'test.json')
# client.close()

import json
from typing import Optional
from bson.json_util import dumps
from pymongo import MongoClient


def initialize_collection(username: str, pwd: str):
    """
    Get sessions collection object with user credentials
    :param username: prod mongodb username
    :param pwd: prod mongodb password
    :returns: client and collection object
    """
    # !!! using the 'srv' uri also requires dnspython to be installed
    uri = "mongodb+srv://" + username + ":" + pwd + "@prod-common-mongodb-atl.aj1dk.mongodb.net/"
    client = MongoClient(uri)
    database = client["Sessions"]
    collection = database["sessions"]
    print('Number of entries in collection: ', collection.count_documents({}))
    return client, collection


def query_collection(collection, query: dict, projection: Optional[dict] = None, iterative: Optional[bool] = True) -> list:
    """
    Get filtered iterable cursor object
    :param collection: initialized collection
    :param query: dictionary with all queries/filters
    :param projection: dictionary with all projections/selected fields
    :param iterative: if false, query all sessions at once, which could lead to high disk I/O
    :returns: list of all sessions as dicts
    """
    cursor = collection.find(query, projection=projection)
    if iterative:
        docs = []
        for c in cursor:
            docs.append(c)
    else:
        docs = list(cursor)
    return docs


def query_one(collection, query: dict, projection: Optional[dict] = None) -> dict:
    """
    Get a single (first) document matching a query
    :param collection: initialized collection
    :param query: dictionary with all queries/filters
    :param projection: dictionary with all projections/selected fields
    :returns: one session dictionary
    """
    docs = collection.find_one(query, projection=projection)
    return docs


def savejson(docs: list, outfile: str) -> None:
    """
    Save list of session dictionaries as json file
    :param docs: list of sessions dictionaries
    :param outfile: full file path (including .json) where to save the file
    :returns:
    """
    with open(outfile, 'w') as file:
        file.write(dumps(docs))


def loadjson(file: str) -> list:
    """
    Load data from json
    :param file: full file path (including .json) of downloaded json file
    :returns: list with each entry being a dictionary
    """
    with open(file) as f:
        data = json.load(f)
    return data
