# Info from spot ID/camera ID
#
import json
import urllib
from typing import Optional
from urllib.error import HTTPError


def query_url(url: str) -> dict:
    try:
        with urllib.request.urlopen(url) as response:
            data = json.loads(response.read())
        return data
    except HTTPError as e:
        print(f'Bad request for url: {url}')
        print(e)
    return []

def query_spots_api_with_camid(camid: str) -> dict:
    url = (
        "http://spots-api.prod.surfline.com/admin/spots/"
        f"?select=timezone,name,cams&cams={camid}"
    )
    return query_url(url)
    
def query_spots_api_with_spotid(spotid: str) -> dict:
    """
    Query the spots API to get info on a spot from a spot ID. For
    example you can get the list of cam IDs at that spot with 
    the 'cams' key. I don't know who made this API but it's magical.
    """
    url = f"http://spots-api.surfline.com/admin/spots/{spotid}"
    return query_url(url)

def get_spot_info(spotid: str, *args) -> dict:
    """ 
    Given a spot ID, get back some info on that spot. For example if you call
    as cams = get_spot_info('5a9d8a84b0f634001ada2d1e', 'cams') you'll get 
    back a the list of cam IDs at Lake Worth Pier Southside.
    
    Set either a camid or spotid when you call.

    args options:
    politicalLocation, lolaSurfBiasCoefficients, best, humanReport, 
    travelDetails, pointOfInterestId, tideStation, cams, hasThumbnail, 
    popular, noraBiasCorrection, spotType, abilityLevels, boardTypes, 
    relivableRating, sds, _id, forecastLocation, status, timezone, name, 
    legacyId, location, subregion, geoname, offshoreDirection, swellWindow, 
    __v, rank, legacyRegionId, thumbnail, id, subregionId, subregionForecastStatus
    """
    spot_data = query_spots_api_with_spotid(spotid)
    if len(spot_data) > 0:
        if len(args) > 0:
            return {
                arg:spot_data[arg]
                for arg in args if arg in spot_data
            }
        else:
            # Just return everything if no args were passed
            return spot_data

def get_cam_info(camid: str, *args) -> dict:
    spotid = get_spotid(camid)
    return get_spot_info(spotid, *args)
    
def get_spotid(camid: str) -> str:
    """ Input a cam ID and get back the spot ID for that cam. """
    if '-' in camid:
        if camid.startswith('mx-puerto'):
            # TODO why doesn't this work with mx-puertocloseup????
            return '5842041f4e65fad6a7708b43'
        camid = get_camid_from_alias(camid)
    spot_data = query_spots_api_with_camid(camid)
    return spot_data[0]['_id'] if len(spot_data) > 0 else None

def get_camids(spotid: str) -> list:
    """ get a list of camIDs for some spotID """
    spot_data = query_spots_api_with_spotid(spotid)
    return spot_data[0]['cams'] if len(spot_data) > 0 else None

def get_nearby_cams(camid: str) -> list:
    spot_dat = query_spots_api_with_camid(camid)
    if len(spot_dat) > 0:
        nearby_camids = spot_dat[0]['cams']
        return [x for x in nearby_camids if x != camid]

def get_camid_from_alias(alias: str) -> str:
    """ wc-hbpierns -> "5842041f4e65fad6a7708827' """
    url = f'http://cameras-service.prod.surfline.com/cameras/ValidateAlias?alias={alias}'
    dat = query_url(url)
    if len(dat) > 0:
        return dat['_id']
    else:
        print(f'Not a valid alias: {alias}')

def get_camid(alias: str) -> str:
    """ 
    note get_camids() requres a spotid input as one spot can have 
    multiple cams.
    """
    return get_camid_from_alias(alias)
        
def get_alias(camid:str) -> str:
    """ '5842041f4e65fad6a7708827' -> wc-hbpierns """
    url = f'http://cameras-service.prod.surfline.com/cameras/{camid}'
    cam_dat = query_url(url)
    if len(cam_dat) > 0:
        return cam_dat['alias']
