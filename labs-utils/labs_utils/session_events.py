#
# This queries S3 and retrieves all events for a specific session ID
# Requirements:
# surfline prod VPN
# surfline prod AWS account
# saved AWS credentials on machine where you're running the code, with named profile
# Python 3.6+

import boto3
import botocore


def pull_data(bucket, sessionid: str, outfile: str) -> None:
    """
    Try downloading events, raise error if file doesn't exist
    :param bucket: s3 bucket object
    :param sessionid: 24 char string sessionID
    :param outfile: full file path (including .json) where to download the file
    :returns: None
    """
    try:
        bucket.download_file(sessionid + '.json', outfile)
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == "404":
            print('The object does not exist')
        else:
            print('Something else has gone wrong')
            raise


def download_events(sessionid: str, botoprofile: str, outfile: str) -> None:
    """
    Get all events for a session
    :param sessionid: 24 char string sessionID
    :param botoprofile: profile string in your aws config
    :param outfile: full file path (including .json) where to download the file
    :returns: None
    """
    session = boto3.Session(profile_name=botoprofile)
    s3 = session.resource('s3')
    bucket = s3.Bucket('sl-sessions-api-sessions-events-prod')
    pull_data(bucket, sessionid, outfile)
