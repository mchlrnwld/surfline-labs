import pandas as pd
import pickle
import json
import numpy as np
import urllib.request
from collections import defaultdict as dd
"""
A script that aggregates the best conditions for a given list of spots from the
Spots API and organizes into a pandas dataframe for reference.

"""


def get_relative_angle(angle, shore_dir):
    """
    Calculates the relative angle of the shore direction.
    """
    a = 360 - shore_dir
    b = (angle + a) % 360
    return b


# Provide a list of spots to gather info about.
lotus_spots = pd.read_csv('lotus_spots.csv')
lotus_spots = list(lotus_spots['SpotID'])
lotus_spots.remove('594214c030e9860013bc472d')
print(f'Num spots in dset: {len(lotus_spots)}')
# dead spot : 594214c030e9860013bc472d


# Want to look up a specific spot?
# lotus_spots = ['5842041f4e65fad6a7708e1d']
opt_data = []
for spot in lotus_spots:
    url = f'http://spots-api.prod.surfline.com/admin/spots/{spot}'
    # print(f'Request url: {url}')
    # try:
    with urllib.request.urlopen(url) as response:
        data = json.loads(response.read())
        opt = dd()

        opt['spot_id'] = data['_id']
        opt['poi_id'] = data['pointOfInterestId']
        opt['tide_station'] = data['tideStation']
        opt['subregion_id'] = data['subregionId']
        opt['offshore_dir'] = data['offshoreDirection']

        # # Optimal Conditions breakdown
        best = data['best']

        # Wind directions
        wind_dir = best['windDirection']
        opt['wind_dir_1_min'] = get_relative_angle(wind_dir[0]['min'], opt['offshore_dir'])
        opt['wind_dir_1_max'] = get_relative_angle(wind_dir[0]['max'], opt['offshore_dir'])
        opt['wind_dir_2_min'] = get_relative_angle(wind_dir[1]['min'], opt['offshore_dir']) if len(wind_dir) > 1 else np.nan
        opt['wind_dir_2_max'] = get_relative_angle(wind_dir[1]['max'], opt['offshore_dir']) if len(wind_dir) > 1 else np.nan

        # Swell period
        swell_per = best['swellPeriod'][0]
        opt['swell_per_min'] = swell_per['min']
        opt['swell_per_max'] = swell_per['max']

        # Swell direction
        swell_dir = best['swellWindow']
        opt['swell_dir_1_min'] = get_relative_angle(swell_dir[0]['min'], opt['offshore_dir'])
        opt['swell_dir_1_max'] = get_relative_angle(swell_dir[0]['max'], opt['offshore_dir'])
        opt['swell_dir_2_min'] = get_relative_angle(swell_dir[1]['min'], opt['offshore_dir']) if len(swell_dir) > 1 else np.nan
        opt['swell_dir_2_max'] = get_relative_angle(swell_dir[1]['max'], opt['offshore_dir']) if len(swell_dir) > 1 else np.nan
        opt['swell_dir_3_min'] = get_relative_angle(swell_dir[2]['min'], opt['offshore_dir']) if len(swell_dir) > 2 else np.nan
        opt['swell_dir_3_max'] = get_relative_angle(swell_dir[2]['max'], opt['offshore_dir']) if len(swell_dir) > 2 else np.nan

        # Wave height
        size = best['sizeRange'][0]
        opt['size_min'] = size['min']
        opt['size_max'] = size['max']

        # Tide
        tide = data['travelDetails']['best']['tide']['value']
        if tide == []:
            tides = ['low', 'med_low', 'med', 'med_high', 'high']
            for t in tides:
                opt[f'tide_{t}'] = np.nan
        else:
            opt['tide_low'] = True if 'Low' in tide else False
            opt['tide_med_low'] = True if 'Medium_Low' in tide else False
            opt['tide_med'] = True if 'Medium' in tide else False
            opt['tide_med_high'] = True if 'Medium_High' in tide else False
            opt['tide_high'] = True if 'High' in tide else False

        # Category of Break
        break_type = data['travelDetails']['breakType']
        types = ['Beach_Break', 'Pier', 'Jetty', 'Reef', 'Point', 'Offshore', 'Slab', 'Canyon']
        labels = ['beach', 'pier', 'jetty', 'reef', 'point', 'offshore', 'slab', 'canyon']
        for i in range(len(types)):
            if break_type == [] or break_type is None:
                opt[f'break_{labels[i]}'] = np.nan
            else:
                opt[f'break_{labels[i]}'] = True if types[i] in break_type else False

        opt_data.append(opt)

optimal_data = pd.DataFrame(opt_data, columns=opt_data[0].keys())
optimal_data.to_csv('highlighted_cond.csv', index=False)



    # except:
    #     print(f'Request for {spot} failed.')
