# Data prep for testing the ratings model on wind station data,
# and for some wind ratings validation.

import logging
import numpy as np
import os
import pandas as pd
import pickle
import ipdb
import datetime as dt

from science.data_lake.data_lake_sql import query_dl_sl_weather_stations as qws

logging.basicConfig(format='%(asctime)s - %(levelname)s : %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S')
log = logging.getLogger()
log.setLevel(logging.INFO)


def get_spot_time_range(dat, set):
    """
    Return DataFrame of earliest and latest timestamps corresponding to spot
    IDs.
    :param dat: DataFrame with spot IDs and Unix timestamps, at minimum.
    :param set: string indicating 'test' or 'train'
    """
    if os.path.exists(f'query_time_ranges_{set}.pkl'):
        log.info('Got time ranges from extant pickle.')
        return pickle.load(open(f'query_time_ranges_{set}.pkl', "rb"))

    log.info('Finding earliest and latest data point for each spot....')
    spottime = dat[['SpotID', 'UnixTime']]
    spotranges = []
    spots = dat.SpotID.unique()
    for s in spots:
        start = int(spottime.loc[(spottime['SpotID'] == s)]['UnixTime'].min())
        stop = int(spottime.loc[(spottime['SpotID'] == s)]['UnixTime'].max())
        spotranges.append([s, start, stop])
    spotranges = pd.DataFrame(spotranges, columns=['SpotID', 'Start', 'End'])
    log.info('Finished finding time ranges!')
    # pickle.dump(spotranges, open(f'query_time_ranges_{set}.pkl', "wb"))
    return spotranges


def query_wind_data(dat, set):
    """
    Query wind station data from data lake against existing DataFrame.
    :param dat: DataFrame to query against
    :param set: string indicating 'test' or 'train'
    """
    nspots = len(dat.SpotID.unique())
    log.info('Querying for each spot time range....')
    df = get_spot_time_range(dat, set)

    # Time to query based on those ranges we found...
    if not os.path.exists('wind_data'):
        os.makedirs('wind_data')

    n = 0
    for index, row in df.iterrows():
        n = n + 1
        spotid, start, end = row['SpotID'], row['Start'], row['End']
        if not os.path.exists(f'./wind_data/{spotid}_{set}.pkl'):
            log.info(f'Querying {spotid}, spot {n} of {nspots}')
            res = qws(spotid, start, end)
            pickle.dump(res, open(f'./wind_data/{spotid}_{set}.pkl', "wb"))


def get_relative_angle(angle, shore_dir):
    """
    Return original angle adjusted relative to angle of shore for some spot. 
    :param angle: cardinal angle to be adjusted
    :param shore_dir: offshore angle of spot to make angle relative to
    """
    a = angle - shore_dir
    if a < 0:
        b = a + 360
        return b
    else:
        return a


def parse_wind_data(dat, set, gust):
    """
    Parse data from wind station datalake query into our dataset's format.
    :param dat: DataFrame of dataset queried against
    :param set: string indicating 'test' or 'train'
    :param gust: toggle whether to use gust or wind value as speed
    """
    lis = os.listdir('./wind_data/')
    res = pd.DataFrame()
    for l in lis:
        pth = os.path.join('wind_data/', l)
        df = pickle.load(open(pth, 'rb'))
        df['spotid'] = l.strip(f'_{set}.pkl')
        res = res.append(df)

    # Columns returned from query:
    # ['valid_time', 'wind_speed_ms', 'wind_gust_ms',
    #  'wind_direction', 'spotid']
    beach_angles = pd.read_csv('beach_angles.csv')
    # Columns are : 'SpotId', 'OptimalWindDirection'
    log.info('Merging query results with beach angle info.')
    df = pd.merge(res, beach_angles, left_on='spotid', right_on='SpotId')
    # GFS columns:
    # This is where Ben did it:
    # https://github.com/Surfline/surfline-labs/blob/3f859229c03328937f3e4090eada54ba5ccfe5a3/lotus_ml_api/src/ingest.py#L69
    # https://github.com/Surfline/surfline-labs/blob/d22c219e9c8fadfa988d5044b0c22400e54751ef/ml_ratings/src/clean_data.ipynb
    # GFS_wind_speed : DWISOTT, evidently is in KNOTS
    log.info('Getting GFS data equivalents.')
    MS_TO_KNOTS = 1.94384
    if gust:
        df['wind_speed'] = df['wind_gust_ms'] * MS_TO_KNOTS
    else:
        df['wind_speed'] = df['wind_speed_ms'] * MS_TO_KNOTS

    # GFS_wind_dir_norm : relative angle of wind to beach
    # This is kind of slow, but it works :(
    df['wind_dir_norm'] = [get_relative_angle(row['wind_direction'],
                                              row['OptimalWindDirection'])
                           for ind, row in df.iterrows()]
    # GFS_onshore_comp : cos(wind_dir_norm) * wind_speed
    df['onshore_comp'] = np.cos(np.radians(df['wind_dir_norm'])) * df['wind_speed']
    # GFS_crossshore_comp: I'm assuming perpendicular to onshore?
    df['crossshore_comp'] = np.sin(np.radians(df['wind_dir_norm'])) * df['wind_speed']

    fname = f'ws_query_data_normalized_{set}.csv'
    log.info(f'Writing parsed data to {fname}')
    df.to_csv(fname, index=False)


def merge_wind_data(dat, set):
    """
    Incorporate wind station data from queries into dataset.
    :param dat: DataFrame of dataset to be merged into
    :param set: string indicating 'test' or 'train'
    """
    wind_data = pd.read_csv(f'ws_query_data_normalized_{set}.csv')
    wind_data.drop(columns='spotid', inplace=True)
    try:
        dat.drop(columns=['Unnamed: 0', 'Unnamed: 0.1', 'index'], inplace=True)
    except:
        pass
    windspots = wind_data.SpotId.unique()
    merged = pd.DataFrame()
    for spot in windspots:
        spot_dat = dat.loc[dat.SpotID == spot].sort_values(by='UnixTime')
        spot_wind = wind_data.loc[wind_data.SpotId == spot].sort_values(by='valid_time')
        mer = pd.merge_asof(spot_dat, spot_wind,
                            left_on='UnixTime', right_on='valid_time',
                            tolerance=dt.timedelta(minutes=30).seconds)
        merged = merged.append(mer)
    merged.dropna(subset=['OptimalWindDirection'], inplace=True)
    dataset = merged.fillna(0)

    # Return un-trimmed dataset
    return dataset


def get_set(set, gust=False):
    """
    Return requested dataset as DataFrame.
    :param set: string naming desired set, 'test' or 'train'
    :param gust: toggle whether to use gust or wind value as speed
    """
    log.info('Reading in LotusDataset')
    dat = pd.read_csv(f'../data/{set}.csv')

    query_wind_data(dat, set)
    parse_wind_data(dat, set, True)
    dataset = merge_wind_data(dat, set)
    log.info(f'Done getting {set}')
    return dataset


def main():
    log.info('Creating full dataset-- combined test/train.')
    # Set gust to true to use gust instead of regular wind speed from wind station.
    gust = False
    test = get_set('test', gust)
    train = get_set('train', gust)

    if gust:
        test.to_csv('ws_gust_test.csv')
    else:
        test.to_csv('ws_speed_test.csv')

    combo = pd.concat([test, train])
    if gust:
        out_name = 'ws_gust_full.csv'
    else:
        out_name = 'ws_speed_full.csv'  # legacy name, tbh
    combo.to_csv(out_name)
    log.info(f'Done, wrote output to `{out_name}`')


if __name__ == "__main__":
    main()
