# Surf Condition Models

Models designed to predict surf conditions on SL rating scale from FLAT to
EPIC.

## XGBoost and Keras Classifiers
Validation for these models can be found in `poor_to_fair_classifiers.ipynb`.
These initial models were trained using the lotus hindcast ML dataset.

Data prep and training for these models can be found in `train_xgbc.ipynb`
and `train_kclf.ipynb` respectively.

### Testing Compatibility with Wind Station Data
In `ptfc_with_windstation.ipynb`, predictions and validation for the favored
xgb classifier are run on the test set using wind station data substituted for
the GFS model data from the original test/train set.

## Analytical Models
A 'purely analytical' form of classifying conditions, there is essentially a
replication of MSW's wind classification system (RED, AMBER, GREEN) in
`analytical_classifier.ipynb`.
