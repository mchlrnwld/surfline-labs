import setuptools

with open("/opt/app/src/tools/README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="mlsurfzone", # Replace with your own username
    version="0.0.1",
    author="Ben Freeston",
    author_email="ben@surfline.com",
    description="A small example package",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/pypa/sampleproject",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
