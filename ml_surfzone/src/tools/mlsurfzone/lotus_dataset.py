# PyTorch custom data loader. Generates batches from csv on disk
#
import os
import re
import yaml
import torch
import datetime
import numpy as np
import pandas as pd
import logging
from collections import defaultdict

from mlsurfzone import transforms as T
from torch.utils.data import Dataset
from subprocess import Popen, PIPE
from typing import Optional, Union, Tuple, Dict

logging.basicConfig()
logging.getLogger().setLevel(logging.INFO)
log = logging.getLogger(__name__)


class LotusDataset(Dataset):

    def __init__(self, train: Optional[bool] = True,
                 config_name: Optional[str] = 'config.yaml',
                 force_df: Optional[pd.DataFrame] = None) -> None:
        """
        Training data generator. It takes forever to load the entire
        training csv in one go.

        Args:
          train: flag if we're training or testing.
                 These come from different files, which are set in
                 the config.yaml
        """
        self.train = train
        self.config_name = config_name
        self.force_df = force_df
        self._orig_df = None
        self._spotids_hist = defaultdict(int)
        self._full_feature_cols = None
        self._full_label_cols = None

        try:
            self._basedir = os.environ['MLSURFZONE_DATA_DIR']
        except KeyError:
            raise ValueError('Set MLSURFZONE_DATA_DIR path in .env')

        # Load the feature & label columns names so we know what to drop
        self.__load_config()
        self.__load_csv()
        self.__prep_sequences()

        if len(self.transforms) > 0:
            log.info(f'Doing {len(self.transforms)} transform(s)')
        self.meta = {}  # Updated at every getitem call

    @classmethod
    def factory(cls, train: Optional[bool] = True,
                config_name: Optional[str] = 'config.yaml') -> None:
        return cls(train, config_name)

    @property
    def spotids(self):
        return self._spotids

    @spotids.setter
    def spotids(self, ids):
        self._spotids = ids if ids is not None else ['all']

        if not isinstance(self._spotids, (list, tuple, np.ndarray)):
            self._spotids = [self._spotids]

        if self._spotids[0].lower() != 'all':
            for spotid in self._spotids:
                if spotid not in self.available_spotids:
                    raise ValueError(f'Spot {spotid} not '
                                     'found in this dataset')

            # Filter out all other spotids
            if self._orig_df is None:
                self._orig_df = self._df
            self._df = pd.concat([self._orig_df[self._orig_df['SpotID'] == s]
                                  for s in self._spotids])
            self.__prep_sequences()

        else:
            if self._orig_df is not None and self._df != self._orig_df:
                self._df = self._orig_df
                self.__prep_sequences()

    def __len__(self):
        return len(self._df) if self.seq['length'] == 1 else len(
            self._seq_idxs)

    def get_spot_distribution(self,
                              spotid:
                              Optional[str] = 'all') -> Union[Dict, int]:
        """
        Just get a histogram of counts for the number of samples
        for each spot.
        """
        if len(self._spotids_hist) == 0:
            self._get_spot_distribution()

        if spotid == 'all':
            return {k: v
                    for k, v in sorted(
                            self._spotids_hist.items(),
                            key=lambda x: x[1])[::-1]}
        else:
            try:
                return self._spotids_hist[spotid]
            except KeyError:
                if spotid not in self.available_spotids:
                    raise ValueError(f'Spotid {spotid} not found'
                                     ' in this dataset')
                else:
                    log.critical('need to rebuild spotids_hist')

    def _get_spot_distribution(self):
        """ Make the histogram """
        for spotid, grp in self._df.groupby('SpotID'):
            self._spotids_hist[spotid] = len(grp)

    @property
    def full_feature_cols(self):
        """
        A list of the full feature columns AFTER all transforms have been
        applied cumulatively. This will match the actual output of a request.

        :return: list of feature column names matching returned numpy
                 data array
        """

        # The column names are only exposed when we run the transforms so
        # if that's not been done we need to do it for a single row here
        if self._full_feature_cols is None:
            _ = self.__getitem__(0)
        return self._full_feature_cols

    @property
    def full_label_cols(self):
        """
        A list of the full label columns AFTER all transforms have been
        applied cumulatively. This will match the actual output of a request.

        :return: list of label column names matching returned numpy data array
        """

        # The column names are only exposed when we run the transforms so
        # if that's not been done we need to do it for a single row here
        if self._full_label_cols is None:
            _ = self.__getitem__(0)
        return self._full_label_cols

    @full_feature_cols.setter
    def full_feature_cols(self, cols):
        self._full_feature_cols = cols

    @full_label_cols.setter
    def full_label_cols(self, cols):
        self._full_label_cols = cols

    def __getitem__(self, idx: int) -> Tuple[np.ndarray, np.ndarray]:
        """
        Load a single row from the csv and maybe do some transforms.
        This feeds back a numpy array because that's what a model will want,
        and they're stackable for varying batch sizes.

        Args:
          index or slice, between 0 and len(self).
        Returns:
          features, labels tuple of numpy arrays, with extra dimension. E.g.
          Shaped as (1, 55), (1, 2) for 2 ground truths and 55 features
        """
        if torch.is_tensor(idx):
            idx = idx.tolist()

        if self.seq['length'] == 1:
            rows = self._df.iloc[idx]
            if not isinstance(rows, pd.DataFrame):
                # happens if stop & step are none in idx slice
                rows = rows.to_frame().T
        else:
            rows = pd.concat([self._df.loc[seq_idx].reset_index(drop=True)
                              for seq_idx in self._seq_idxs[idx]])

        if ['UnixTime', 'SpotID'] in self.feature_cols:
            self.meta['timestamps'] = rows['UnixTime']
            self.meta['spotid'] = rows['SpotID'].values
            self.timestamps = rows['UnixTime']

        # Add lola data for validations.
<<<<<<< HEAD
        # self.meta['lola_max'] = rows[
        #     'lola_report_no_assim_surf_maximum_m'].values.astype(np.float32) * 3.28
=======
        if ['lola_report_no_assim_surf_maximum_m'] in self.feature_cols:
            self.meta['lola_max'] = rows[
                'lola_report_no_assim_surf_maximum_m']\
                    .values.astype(np.float32) * 3.28
>>>>>>> origin/master

        # Add lotus max height?
        self.meta['lotus_max'] = rows.filter(
            regex='^LotusBreaking_part', axis=1).astype(float).max(
                axis=1, skipna=True).values * 3.28

        # Need to drop everything except the labels & features cols
        features = rows.drop(self.non_feature_cols, axis=1)
        if 'SpotID' not in self.feature_cols:
            features = features.astype(np.float32)
        labels = rows[self.label_cols]

        for transform_func in self.transforms.values():
            features = transform_func(features)
        self.full_feature_cols = features.columns.values

        for transform_func in self.label_transforms.values():
            labels = transform_func(labels)
        self.full_label_cols = labels.columns.values

        # Convert to np arrays and set nans to zeros
        features = features.fillna(0).values
        labels = labels.values.astype(np.float32)
        if np.any(np.isnan(labels)):
            raise ValueError('NaNs in the labels!')

        return features, labels

    def __load_config(self) -> None:
        self._cwd = os.path.dirname(os.path.realpath(__file__))
        cfg_file = os.path.join(self._cwd, self.config_name)
        if not os.path.isfile(cfg_file):
            raise ValueError('Cannot find config file: '
                             f'{cfg_file}')
        with open(cfg_file, 'r') as f:
            self.cfg = yaml.load(f, Loader=yaml.FullLoader)
        self.feature_cols = self.cfg['Data']['feature_columns']
        self.label_cols = self.cfg['Data']['ground_truth_columns']

        self.spotids = self.cfg['Data']['spotids']  # property

        # Are we doing sequential models or just single sample models?
        self.seq = self.cfg['Data']['sequences']
        self.seq['length'] = max(1, self.seq['length'])
        self.seq['shift'] = max(1, self.seq['shift'])

        # Are there data transformations set in config?
        self.transforms = {}
        if self.cfg['Data']['transforms'] is not None:
            for transform in self.cfg['Data']['transforms']:
                if transform in dir(T):
                    self.transforms[transform] = getattr(T, transform)
                elif transform in globals():
                    self.transforms[transform] = globals()[transform]
                else:
                    raise ValueError('Requested feature transform not '
                                     f'understood: {transform}. '
                                     'Was it imported?')
        # Is SpotID being tokenized if used?
        if 'SpotID' in self.feature_cols:
            if ('tokenize_spotids_ohe' not in self.transforms) & (
                    'tokenize_spotids_int' not in self.transforms):
                raise ValueError('When using SpotID as a feature, you '
                                 'must include `tokenize_spotids` as a'
                                 ' transform')

        # Are there any label transformations set in config?
        self.label_transforms = {}
        if self.cfg['Data']['label_transforms'] is not None:
            for transform in self.cfg['Data']['label_transforms']:
                if transform in dir(T):
                    self.label_transforms[transform] = getattr(T, transform)
                elif transform in globals():
                    self.label_transforms[transform] = globals()[transform]
                else:
                    raise ValueError('Requested label transform not '
                                     f'understood: {transform}. '
                                     'Was it imported?')

    def __prep_sequences(self) -> None:
        """
        Build a lookup table to extract sequential sequences.
        In this case it's slower to construct this object since we need
        to get SpotIDs and timstamps in full, right now
        """
        # Build the sequence lookup table only if sequence lengths > 1
        if self.seq['length'] > 1:
            log.info('Loading data for sequential '
                     f'sequences of {self.seq["length"]}')
        else:
            return

        self._seq_idxs = []
        for spotid, grp in self._df.groupby('SpotID'):
            if len(grp) < self.seq['length']:
                log.debug('No sequences of length {self.seq["length"]} '
                          f'for {spotid}')

            # get timedeltas for this group and start with 0.
            dt = np.diff(grp['UnixTime']) / np.timedelta64(1, 'D')
            dt = np.hstack(([0], dt))

            iseq_cnt = 0  # count how many sequences we get for this group
            for idx in range(
                    0, len(grp) - self.seq['length'], self.seq['shift']):

                # Check all the timedeltas in this sequence are less
                # than max_dela. If not just omit it completely.
                dt_slice = dt[idx: idx + self.seq['length']]
                if np.all(
                        dt_slice < self.seq['max_delta'] + self.seq['epsilon']):
                    iseq_indices = grp.index[idx: idx + self.seq['length']]
                    # TODO better data structure for this?
                    self._seq_idxs.append(iseq_indices)
                    iseq_cnt += 1

            tot_possible_seq = (
                len(grp) - self.seq['length']) // self.seq['shift']
            log.debug(f'Found {iseq_cnt} sequences for {spotid}, out of'
                      f'{len(grp) // self.seq["shift"]} total possible sequences')
            self._spotids_hist[spotid] = len(grp)

        log.info(f'Found {len(self._seq_idxs)} sequences')

        if len(self._seq_idxs) == 0:
            log.critical('Probably your sequence length is '
                         f'too long ({self.seq["length"]})')

        self._seq_idxs = np.asarray(self._seq_idxs)

    def __load_csv(self) -> None:
        if self.force_df is not None:
            self._df = self.force_df
        else:
            # look for csv file name
            csv_name = (self.cfg['Data']['train_csv_name']
                        if self.train else self.cfg['Data']['test_csv_name'])
            self.csv_name = os.path.join(self._basedir, csv_name)
            if not os.path.isfile(self.csv_name):
                raise ValueError(f'Unable to find {self.csv_name}')

            # Load it and drop unnecessary columns
            self._df = pd.read_csv(
                self.csv_name, dtype=str).sort_values('UnixTime')

            self._df = self._df.reset_index(drop=True)

            self._df['UnixTime'] = [datetime.datetime.utcfromtimestamp(int(t))
                                    for t in self._df['UnixTime']]

        drop_cols = ['level_0', 'Unnamed: 0', 'Unnamed: 0.1', 'index']
        for drop_col in drop_cols:
            if drop_col in self._df.columns:
                self._df.drop(drop_col, axis=1, inplace=True)

        # Figure out which columns we regularly don't want.
        self.non_feature_cols = [c.strip() for c in self._df.columns
                                 if c not in self.feature_cols]

        # Figure out which spots we have (if there are spot ids in the data)
        if 'SpotID' in self._df.columns:
            self.available_spotids = self._df['SpotID'].unique().tolist()
        else:
            self.available_spotids = []
