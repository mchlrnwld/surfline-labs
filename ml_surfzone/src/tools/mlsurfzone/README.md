# ML Surfzone Data Prep

## PyTorch Dataset Generators
I suggest we use a custom torch [Dataset](https://pytorch.org/tutorials/beginner/data_loading_tutorial.html), where we slice single elements or sequences from the dataframe at a single index,
and return that single sample or sequence. This can be extended to batch loading by wrapping a `Dataset`
instance with a [dataloader](https://pytorch.org/docs/1.1.0/_modules/torch/utils/data/dataloader.html), or
just iterating over a class instance yourself.

### config.yaml
Everything is defined in the config file except whether or not you're training (in which case the only difference is the class loads a different csv).

* The `train_csv_name` and `test_csv_name` fields in the config file are expected to live
in the directory set in your `.env` (currently set as `MLSURFZONE_DATA_DIR`). If you set `Train=False` when you construct a LotusDataset object it will load `test_csv_name`.

* **`sequences`**

  Sequences are defined by length and relative offsets.
  
  * `length`: length of each sequence, in units of samples. This needs to be standardized
  for recurrent models, so using units of time makes this more complicated. Set this to 1 for all
  non-recurrent models, and all subsequent keywords will be ignored.

  * `shift`: Index offset of the first element in S_i+1 from the first element in S_i. Always >= 1.

  * `max_delta`: Threshold in days from S_i,j to S_i,j+1. So the timedelta between each sample
  in every sequence will never be more than this (plus some error). Sequences where this is violated
  are discarded completely.

  * `epsilon`: Temporal error in days that we think is appropriate.

  Each spot will have (N_samples - `length`) // `shift` samples, minus number of `max_delta + epsilon` violations. 

* **`transforms`**

  All the fields below `transforms` are expected to be methods in the `globals()` space of `LotusDataset`, or live in `transforms.py`. Each of these transforms will be applied to the feature vectors.

* **`feature/ground_truth_columns`**

  Add and remove ground truth columns and feature columns by commenting out/removing in the config file. For training,
  the `ground_truth_columns` will be human reports. For validating against lola, set these to be `lola_report_no_assim_surf_minimum` etc, where you can compare model outputs with the lola 'ground truths'. See config_lola_val.yaml

* **`spotids`**

  Add one or more SpotIDs that you want to focus on. Leave blank to use the entire ~250 spots. At the moment this can only be set one time.


### Iterating Over the Train or Test Set
The LotusDataset class currently loads `./config.yaml` by default. Also takes it as a keyword.
```python
train_dat = LotusDataset()
# or
train_dat = LotusDataset.factory()

# or testing with custom config:
train_dat = LotusDataset(config='some_config.yaml', train=False)

# Accepts slicing as `[start:stop:step]`. To get the full dataset:
features, labels = train_dat[:]

# Or the first 3 elements:
features, labels = train_dat[:3]

# Get sample at index 10 in the original dataframe:
features_i, label_i = train_dat[10]

# Get the metadata that corresponds to the last slice that was called:
meta = train_dat.meta
# which has keys:
dict_keys(['timestamps', 'spotid', 'lola_max', 'lotus_max'])  # `max` in feet!

# Get a batch of random samples (e.g. for TensorFlow model training):
batch_size = 5
labels, features = [], []
for _ in range(batch_size):
    feature, label = train_dat[np.random.randint(low=0, high=len(train_dat))]
    features.append(feature)
    labels.append(label)
labels = np.vstack(labels)
features = np.vstack(features)
```

If you're building a torch model, batch loading can be abstracted away with `DataLoaders`:
```python
from torch.utils.data import DataLoader
from torch.utils.data import RandomSampler

train_dat = LotusDataset()
sampler = RandomSampler(train_dat)
data_loader = DataLoader(dataset, batch_size=batch_size,
	                 num_workers=2, sampler=sampler)
# then just loop over and train:
for feature_batch, label_batch in data_loader:
    predictions = model(feature_batch)
    ...
    optimizer.step()
```