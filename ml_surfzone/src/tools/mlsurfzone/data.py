# Basic data loading from csv -> dataframe. Optionally do some feature
# transformations.
#
import os
import pandas as pd
import numpy as np
from typing import Optional, Union


def load_data(filename: str, nrows: int, array: bool) -> pd.DataFrame:
    """ 
    Load some csv into a dataframe and optionally do some
    feature transformations.
    
    Args:
      filename: "train.csv", "test.csv", etc
      nrows: not sure
      array: do feature transforms via binning
    
    Returns:
      dataframe, with or without those feature transforms
    """
    
    if 'MLSURFZONE_DATA_DIR' not in os.environ.keys():  
        raise ValueError('Please set MLSURFZONE_DATA_DIR in .env')
    dat = pd.read_csv(
        os.path.join(os.environ['MLSURFZONE_DATA_DIR'], filename),
        nrows=nrows)
    return create_array(dat) if array else dat


def get_train_data(nrows: Optional[int] = None,
                   array: Optional[bool] = True) -> pd.DataFrame:
    return load_data('train.csv', nrows, array)

def get_test_data(nrows: Optional[int] = None,
                  array: Optional[bool] = True) -> pd.DataFrame:
    return load_data('test.csv', nrows, array)

def get_test_data_full(nrows: Optional[int] = None,
                       array: Optional[bool] = True) -> pd.DataFrame:
    return load_data('test_full.csv', nrows, array)


# Standard feature pre-processing (for now)
# Create directionally binned data from set
# TODO name this something more descriptive and remove hard-coded column names!
def create_array(
        dat: Union[pd.DataFrame, pd.Series]) -> Union[pd.DataFrame, pd.Series]:

    if isinstance(dat, pd.Series):
        dat = dat.to_frame().transpose()
    
    period_by_dir = np.zeros([len(dat), 37])
    sigh_by_dir = np.zeros([len(dat), 37])

    for i in range(0, len(dat)):
        # Loop for each partition
        for j in range(1,5):
            # Process only partitions with data
            if not np.isnan(dat['LotusTp_part' + str(j)][i]):
                # Round directions and fix the offset
                # (change -180 -> +180 to 0 -> 360)
                dir = int(dat['LotusPdir_norm_part' + str(j)][i] / 10) + 18
                per = int(dat['LotusTp_part'+ str(j)][i])
                sig = dat['LotusSigH_part' + str(j)][i]
                period_by_dir[i, dir] = per
                sigh_by_dir[i, dir] = sig

    spec = np.concatenate([period_by_dir, sigh_by_dir], axis=1)

    per_idx = ["per_" + str(i) for i in np.array(range(0, 37))]
    sig_idx = ["sig_" + str(i) for i in np.array(range(0, 37))]
    feature_columns = np.concatenate([per_idx, sig_idx])

    columns = ['GFS_wind_dir_norm', 'GFS_wind_speed', 'GFS_onshore_comp',
               'human_report_surf_max', 'human_report_surf_condition_int']

    features = dat[columns].values
    features = np.concatenate([spec, features], axis=1)
    columns = np.concatenate([feature_columns, columns])
    print(columns)
    out = pd.DataFrame(features, columns=columns)

    return out
