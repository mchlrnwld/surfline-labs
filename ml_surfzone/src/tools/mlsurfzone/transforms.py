# Feature transforms and augmentations. Not all live here,
# eg 2D spectra from partitions.
#
import os

import pandas as pd
import numpy as np
import pickle
import json
import math
from ipdb import set_trace
from typing import Optional, Union
from spectra.spectra_from_partitions import create_2d_spectra
from sklearn.preprocessing import OneHotEncoder
from sklearn.feature_extraction.text import CountVectorizer


def directional_binning(
        dat: Union[
            pd.DataFrame,
            pd.Series]) -> Union[pd.DataFrame, pd.Series]:
    """
    Directonal binning of partition data: period -> by direction and
    sigH -> by direction.

    Args:
      dat: dataframe or series of partition features. If the columns
      required by this binning aren't present this raises an error.

    Returns:
      Dataframe or Series with LotusTp_part*, LotusSigH_part* and
      LotusPdir_norm_par* columns removed, and 2x binned features
      added in 10 degree intervals, one for sigH and one for period.
    """
    if isinstance(dat, pd.Series):
        dat = dat.to_frame().transpose()

    period_by_dir = np.zeros([len(dat), 37])
    sigh_by_dir = np.zeros([len(dat), 37])

    # After we bin sigH and period, we should drop the original columns
    # and insert the binned columns.
    cols_to_drop = set()

    for i in range(0, len(dat)):

        # Loop for each partition
        for j in range(1, 5):
            # Process only partitions with data
            col_nameTp = 'LotusTp_part' + str(j)
            col_nameSigH = 'LotusSigH_part' + str(j)
            col_namePdir = 'LotusPdir_norm_part' + str(j)

            if (col_nameTp not in dat.columns
                    or col_nameSigH not in dat.columns
                    or col_namePdir not in dat.columns):
                raise ValueError('Requested directional binning'
                                 ' but features are missing: '
                                 f'{col_nameTp}, {col_nameSigH}'
                                 f'{col_namePdir}')

            # Drop them even if they're nan (below)
            cols_to_drop.add(col_nameTp)
            cols_to_drop.add(col_nameSigH)

            if not np.isnan(dat[col_nameTp].iloc[i]):
                # Round directions and fix the offset
                # (change -180 -> +180 to 0 -> 360)
                dir = int(dat[col_namePdir].iloc[i] / 10) + 18
                per = int(dat[col_nameTp].iloc[i])
                sig = dat[col_nameSigH].iloc[i]
                period_by_dir[i, dir] = per
                sigh_by_dir[i, dir] = sig

    spec = np.concatenate([period_by_dir, sigh_by_dir], axis=1)

    per_idx = ["per_" + str(i) for i in np.array(range(0, 37))]
    sig_idx = ["sig_" + str(i) for i in np.array(range(0, 37))]
    feature_columns = np.concatenate([per_idx, sig_idx])

    for drop_col in cols_to_drop:
        dat = dat.drop(drop_col, axis=1)

    features = dat.values
    features = np.concatenate([spec, features], axis=1)
    columns = np.concatenate([feature_columns, dat.columns])

    out = pd.DataFrame(features, columns=columns)
    return out


def convert_2d_spectra(dat: Union[pd.DataFrame, pd.Series]) \
        -> Union[pd.DataFrame, pd.Series]:
    """
    Convert the partitions into 2D spectra

    :param dat: dataframe or series of feature data
    :return: dataframe of feature data with 2D spectra
    """
    if isinstance(dat, pd.Series):
        print(type(dat))
        dat = dat.to_frame().transpose()

    sigh = dat[dat.filter(like='LotusSigH_').columns].fillna(0).values
    tp = dat[dat.filter(like='LotusTp_').columns].fillna(0).values
    pdir = dat[dat.filter(like='LotusPdir_').columns].fillna(0).values
    spread = dat[dat.filter(like='LotusSpred_').columns].fillna(0).values
    total_sigh = dat.LotusSigH.fillna(0).values

    cols = None
    all_spec_data = []
    for i in range(0, len(sigh)):
        [spec_data, synthetic_sigh] = create_2d_spectra(sigh[i], tp[i],
                                                        pdir[i], spread[i],
                                                        total_sigh[i])

        all_spec_data.append(spec_data.ravel())

    # Create column labels - we only want to do this once per sequence
    if not cols:
        xx, yy = np.meshgrid(np.arange(spec_data.shape[1]),
                             np.arange(spec_data.shape[0]))
        xx = xx.ravel().astype(str)
        yy = yy.ravel().astype(str)
        xx = np.core.defchararray.add(
            np.core.defchararray.add("spec2d_", xx), "_")
        cols = np.core.defchararray.add(xx, yy)

    dat[cols] = pd.DataFrame(np.array(all_spec_data), columns=cols)

    dat = dat[dat.columns.drop(list(dat.filter(regex='LotusSigH_')))]
    dat = dat[dat.columns.drop(list(dat.filter(regex='LotusTp_')))]
    dat = dat[dat.columns.drop(list(dat.filter(regex='LotusPdir_')))]
    dat = dat[dat.columns.drop(list(dat.filter(regex='LotusSpred_')))]

    return dat


def height_classification_ohe(dat: Union[pd.DataFrame, pd.Series],
                              bins=[0, 2, 3, 4, 5, 6, 7, 8, 10, 12,
                                    15, 20, 25, 30, 35, 40, 50],
                              meters=True) \
                                 -> Union[pd.DataFrame, pd.Series]:
    """
    Transforms labels to one-hot encoded categorical indexes to treat surf
    height as a classification problem.

    Surfline's actual current binning based on model output:
            bins=[0.0, 1.0, 2.0, 3.0, 3.5, 4.1, 4.3, 4.8, 5.3, 6.0, 7.0,
                  8.0, 10.0, 12.0, 16.5, 20.0, 22.5, 27.5, 32.5, 36, 39.5]
    See this page on Confluence:
    https://wavetrak.atlassian.net/wiki/spaces/CW/pages/585564265/Forecaster+Report+Forecast+Updates+-+2019

    :param dat: dataframe or series of label data, i.e. max/min height
    :param bins: specify boundaries for classifications to be made on, the
                 default follows the current surfline max height system
    :param meters: specify unit of input as metric or imperial

    :return: dataframe of one-hot encoded, categorically indexed labels
             based on the bins provided
    """
    labels = dat.columns
    bin_indexes = range(0, (len(bins)-2))
    # This is roughly equivalent to OneHotEncoder
    ohe = pd.get_dummies(height_classification(dat), columns=labels)
    # Assure there are columns for all possible classification bins
    # (even those not present in data)
    for col in labels:
        for i in bin_indexes:
            nom = f'{col}_{i}'
            if nom not in ohe.columns:
                ohe[nom] = 0
    ohe.sort_index(axis=1, inplace=True)  # want equality to work

    return ohe


def height_classification(dat: Union[pd.DataFrame, pd.Series],
                          bins=[0, 2, 3, 4, 5, 6, 7, 8, 10, 12,
                                15, 20, 25, 30, 35, 40, 50],
                          meters=True) \
                                 -> Union[pd.DataFrame, pd.Series]:
    """
    Transforms labels to categorical indexes to treat surf height as a
    classification problem.

    :param dat: dataframe or series of label data, i.e. max/min height
    :param bins: specify boundaries for classifications to be made on, the
                 default follows the current surfline max height system
    :param meters: specify unit of input as metric or imperial

    :return: dataframe of categorically indexed labels based on the bins
    """
    if isinstance(dat, pd.Series):
        dat = dat.to_frame().transpose()
    labels = dat.columns
    # np.digitize indexes starting from 1, but I'm going to force it to 0
    scal = 3.28
    # unit conversion unless otherwise specified
    if meters:
        dat = dat.astype(float) * scal
    # fit max heights into classification bins (0-indexed)
    binned = pd.DataFrame((np.digitize(dat, bins, right=False)-1),
                          columns=labels)
    return binned


def random_gauss_augmentations():
    pass


def ratings_ohe(dat: Union[pd.DataFrame, pd.Series], show_bug=False) \
                      -> Union[pd.DataFrame, pd.Series]:
    input_data = dat.copy()
    enc = OneHotEncoder(sparse=False)
    if show_bug:
        dat = enc.fit_transform(dat)
    else:
        dat = enc.fit_transform(dat.astype(float).astype(int))
    dat = pd.DataFrame(dat)
    # Check the encoding is doing what you think it's doing - count class
    # instances before and after.
    if len(input_data.columns) != 1:
        print('one-hot encoding check might be wrong with multi-column input')
    input_data = input_data[
        input_data.columns[0]].values.astype(float).astype(int)
    if input_data.min() != 0:
        input_data -= input_data.min()  # OHE will do this.
    before = {cls: np.count_nonzero(input_data==cls)
              for cls in np.unique(input_data)}
    output_data = np.argmax(dat.values, axis=1)  # class integers after OHE
    after = {cls: np.count_nonzero(output_data==cls)
             for cls in np.unique(output_data)}
    if before != after:
        print('Before OHE: ', before)
        print('After OHE: ', after)
        print('** OHE is not maintaining class distribution. **')
    return dat


# TODO: ratings unhot currently scrambles labels
def ratings_unhot(dat: Union[pd.DataFrame, pd.Series],
                  col='human_report_surf_condition_int') \
                      -> Union[pd.DataFrame, pd.Series]:
    enc = OneHotEncoder(sparse=False)
    ser = np.arange(1, 11).transpose().reshape(-1, 1)
    enc.fit(ser)
    dat = enc.inverse_transform(dat)
    dat = pd.DataFrame(dat, columns=[col])
    return dat


def create_vocabulary():
    """
    Helper function for tokenizing spot IDs that creates a vocabulary
    for encoding them based on complete list of SL spot IDs.

    Outputs vocabulary to json.

    :return: CountVectorizer fitted to all Surfline spot IDs
    """
    cwd = os.path.dirname(os.path.realpath(__file__))
    csv_path = os.path.join(cwd, 'all_spot_ids.csv')
    if not os.path.exists(csv_path):
        raise FileNotFoundError("Please acquire the csv containing all "
                                "Surfline spot IDs. Ask Ketron or Gracie. \n"
                                "Should be named `all_spot_ids.csv` and left "
                                "in the `mlsurfzone` directory.")
    else:
        SpotIDs = pd.read_csv(csv_path)['SpotID'].values
    count_vect = CountVectorizer()
    count_vect.fit(SpotIDs)
    vec_dict = count_vect.vocabulary_
    # Output vocabulary to JSON, this is needed for inference later.
    json_path = os.path.join(cwd, 'spot_vocabulary_dict.json')
    if not os.path.exists(json_path):
        with open(json_path, 'w') as outfile:
            json.dump(vec_dict, outfile)

    return count_vect


def tokenize_spotids_int(dat: Union[pd.DataFrame, pd.Series]) \
                        -> Union[pd.DataFrame, pd.Series]:
    """
    Integer encodes spot IDs to allow them to be used as a feature in training.

    To have model be able to predict across all SL spots, all spotids must be
    encoded at training-time.

    :param dat: data with SpotID as feature

    :return: dat but with spot IDs integer encoded
    """
    if isinstance(dat, pd.Series):
        dat = dat.to_frame().transpose()

    try:
        spotids_raw = dat['SpotID']
    except:
        raise IndexError("You should only be using this transform "
                         "if you're using SpotID as a feature.")

    count_vect = create_vocabulary()
    vec_dict = count_vect.vocabulary_

    spotids_vec = [vec_dict[i] for i in spotids_raw]
    dat['SpotID'] = spotids_vec

    return dat


def tokenize_spotids_ohe(dat: Union[pd.DataFrame, pd.Series]) \
                            -> Union[pd.DataFrame, pd.Series]:
    """
    One-hot encodes spot IDs to allow them to be used as a feature in training.
    Based on an updated version of this:
    https://stackoverflow.com/questions/43577590/adding-sparse-matrix-from-countvectorizer-into-dataframe-with-complimentary-info

    :param dat: data with SpotID as feature

    :return: dat but with spot IDs one-hot encoded
    """
    if isinstance(dat, pd.Series):
        dat = dat.to_frame().transpose()

    try:
        spotids_raw = dat['SpotID']
    except:
        raise IndexError("You should only be using this transform "
                         "if you're using SpotID as a feature.")

    count_vect = create_vocabulary()
    X = count_vect.transform(spotids_raw.values)

    for i, col in enumerate(count_vect.get_feature_names()):
        dat[col] = pd.Series(pd.arrays.SparseArray(X[:, i].toarray().ravel(),
                             fill_value=0))

    dat.drop(['SpotID'], axis=1, inplace=True)

    return dat


def trig_encode_angles(dat: Union[pd.DataFrame, pd.Series], tup_ok=False) \
                            -> Union[pd.DataFrame, pd.Series]:
    """
    Trig encoding all angle columns in dataset.

    :param dat: data with angle features to be encoded

    :return: data with all directional features encoded
    """
    # Find which columns need encoding.
    cols = list(dat.columns)
    angle_columns = [a for a in cols if 'dir' in a]

    for col in angle_columns:
        dat[f'{col}_sin'] = dat[col].astype(float).apply(math.sin)
        dat[f'{col}_cos'] = dat[col].astype(float).apply(math.cos)
    dat.drop(angle_columns, axis=1, inplace=True)

    return dat
