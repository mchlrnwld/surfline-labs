import logging
import math
import stat
import json
import urllib
from urllib.error import HTTPError, URLError

import numpy as np
import pandas as pd
from sklearn.metrics import mean_squared_error, mean_absolute_error
from sklearn.metrics import confusion_matrix
import matplotlib
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import seaborn as sn

from mlsurfzone.lotus_dataset import LotusDataset
from mlsurfzone.data import get_test_data, get_test_data_full
from mlsurfzone.transforms import height_classification


logging.basicConfig(format='%(asctime)s - %(levelname)s : %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S')
log = logging.getLogger()
log.setLevel(logging.CRITICAL)

scal = 3.28  # meters to feet conversion

bin_labels = ['FLAT', 'VERY POOR', 'POOR',
              'POOR TO FAIR', 'FAIR', 'FAIR TO GOOD+']
# This is here to provide average data for polar_5class_plot
df = pd.read_csv('../data/new_train.csv')

"""
Validate model and visualize performance by several metrics.

Need to rewrite this to play more nicely with Dataset class.
"""


def get_alias(spotid):
    """
    Queries the Spots API for a given spot id and returns the associated
    alias as a string.

    Args:
        spotid: spotid as string

    Returns:
        Associated alias or 'alias unknown'
    """
    url = f'http://services.surfline.com/kbyg/spots/reports?spotId={spotid}'
    try:
        resp = urllib.request.urlopen(url)
        spot_data = json.loads(resp.read())['spot']
    except (URLError, HTTPError):
        return None
    try:
        name = spot_data['name']
        return name
    except KeyError:
        return None


def smoothness(dat):
    """
    Print stats regarding 'smoothness' of data.

    Intended originally to work on surf height data.

    Args:
        dat: array of sequential data whose smoothness is to be evaluated.

    Returns:
        Printout of ave. change between points, standard deviation.
    """
    diff = np.ediff1d(dat).tolist()
    ave = stat.mean(diff)
    dev = stat.pstdev(diff, ave)
    print("Ave change b/w points: ", ave, "\t Standard dev: ", dev)


def rmse(x, y):
    # Cute wrapper for Root Mean Squared Error
    return np.round(math.sqrt(mean_squared_error(x, y)), 2)


def mae(x, y):
    # Cute wrapper for mean absolute error, rounded to 2 decimals
    return np.round(mean_absolute_error(x, y), 2)


def lola_stats():
    """
    Prints stats on LOLA data in test set.

    Return:
        Mean absolute error of LOLA max height vs. human reports
    """
    test = get_test_data(array=False)
    lola_test = test.dropna(subset=['lola_report_no_assim_surf_maximum_m'])
    ypred_lola = lola_test['lola_report_no_assim_surf_maximum_m'].values
    ytrue_human_lola = lola_test['human_report_surf_max'].values
    mae_lola_vs_human = mae(ypred_lola, ytrue_human_lola)
    print(mae_lola_vs_human)
    return mae_lola_vs_human


def plot_cm(pred, true, name='model',
            bin_labels=None, normalize_over='all',
            log_density=False, type='clf'):
    """
    Plots confusion matrix and log density map of pred and true.

    Args:
        pred: Array of predicted classification indexes.
        true: Array of ground truth classification indexes.
        name: Specify name of model, will be used to label axes of plots.
        bin_labels: Specify 'names' corresponding to classification indexes
                        to be used as tick labels

    Returns:
        Returns nothing, plots confusion matrix and log density map for inputs
    """
    pred = pred.astype(int)
    true = true.astype(int)

    # Common input problem is 2D array
    if true.ndim > 1:
        true = true.flatten()

    if bin_labels is None:
        bin_labels = ['Flat', '0-2ft', '2-3ft', '3-4ft', '4-5ft', '5-6ft',
                      '6-7ft', '7-8ft', '8-10ft', '10-12ft', '12-15ft',
                      '15-20ft', '20-25ft', '30-35ft', '35-40ft',
                      '40-50ft', '50ft+']

    cm = confusion_matrix(true, pred, normalize=normalize_over,
<<<<<<< HEAD
                          labels=list(range(1, len(bin_labels)+1)))
=======
                          labels=list(range(min(pred), max(pred)+1)))
>>>>>>> origin/master
    # use slice based on shape so it doesn't break when missing larger classes
    bin_labels = bin_labels[:(cm.shape[0])]

    cm_rounded = cm.round(decimals=2)  # rounded for readable confusion matrix
    cm = pd.DataFrame(cm,
                      index=[i for i in bin_labels],
                      columns=[i for i in bin_labels])
    cm_rounded = pd.DataFrame(cm_rounded,
                              index=[i for i in bin_labels],
                              columns=[i for i in bin_labels])

    # Plot confusion matrix normalized over truth, rounded to 2 decimals
    plt.figure(figsize=(10, 7))
    sn.set(font_scale=1.4)
    sn.heatmap(cm_rounded, annot=True, annot_kws={"size": 10})
    plt.xlabel(name)
    plt.ylabel('Human')
    plt.show()

    if log_density:
        # Plot log density map to see anything strange lost by rounding
        plt.figure(figsize=(10, 7))
        plt.imshow(np.log(cm+1 + 0.00001))
        plt.xlabel(name)
        plt.ylabel('Human')
        plt.show()


def validate(test_heights, name='model', visualize=True,
             classifier=False, units_feet=False, spot_validate=True):
    """
    Method to take a pandas series test height output data and provide
    validation stats

    Args:
        test_heights: pandas series of height data with timestamp index,
                data units are meters unless `units_feet` is true
        name: name of model being validated
        visualize: provide inline plots and printed validation data
                (good for notebooks)
        classifier: when False, test_heights need to be transformed to
                validate model as a classifier
        units_feet: True if the test_heights are feet, will convert and
        display all metrics in feet
        spot_validate: Run spot validations and display.

    Returns:
        Prints stats and graphs on model performance, returns tuple
        of mean absolute error for model, LOLA
    """
    test = get_test_data(array=False)

    # Check that validation index matches the test data index
    # Since there is no index on the input (it's taken to just be an array?),
    # I'm taking this as being just "make sure they're the same length".

    test_indexes = len(test.index.to_list())
    input_indexes = len(test_heights)
    # Raise error if the two do not match.
    if not test_indexes == input_indexes:
        raise ValueError('Heights provided do not match test set.')

    # Proper subset of prediction data based on what you're looking at.
    test['test_heights'] = test_heights
    test = test.dropna(subset=['lola_report_no_assim_surf_maximum_m'])

    # Calculate validations
    human = test['human_report_surf_max']
    # lotus = test[['LotusBreaking_part1', 'LotusBreaking_part2',
    #              'LotusBreaking_part3', 'LotusBreaking_part4']].max(axis=1)
    lola = test['lola_report_no_assim_surf_maximum_m']
    # For user inputted surf height predictions
    ypred = test['test_heights']

    meters_to_feet = 3.28
    if units_feet:
        if visualize:
            print("Units in Feet")
        lola = lola * meters_to_feet
        human = test['human_report_surf_max_ft']

    # TODO: different stats for classifier=True

    rmse_vs_lola = rmse(ypred.values, lola.values)
    rmse_vs_human = rmse(human.values, ypred.values)
    rmse_lola_vs_human = rmse(human.values, lola.values)

    mae_vs_human = mae(human.values, ypred.values)
    mae_vs_lola = mae(ypred.values, lola.values)
    mae_lola_vs_human = mae(lola.values, human.values)

    # cor_vs_human = np.corrcoef(human.values, ypred.values)
    # cor_vs_lola = np.corrcoef(lola.values, ypred.values)
    # cor_lola_vs_human = np.corrcoef(human.values, lola.values)

    # Spot by spot validations
    if spot_validate:
        spot_error = pd.DataFrame(columns=['SpotID', 'Alias',
                                           'mae_model', 'mae_lola',
                                           'rmse_mod', 'rmse_lola'])
        for spotid, group in test.groupby('SpotID'):

            hum = group['human_report_surf_max']
            mod = group['test_heights']
            lol = group['lola_report_no_assim_surf_maximum_m']

            if units_feet:
                lol = lol * meters_to_feet
                hum = group['human_report_surf_max_ft']

            mae_mod = mae(mod, hum)
            mae_lola = mae(lol, hum)

            rmse_mod = rmse(mod, hum)
            rmse_lola = mae(lol, hum)

            alias = get_alias(spotid)
            if alias is not None:
                row = pd.Series([spotid, alias, mae_mod, mae_lola, rmse_mod,
                                 rmse_lola], index=spot_error.columns)
                spot_error = spot_error.append(row, ignore_index=True)
        spot_error.sort_values(by=['mae_model'], ascending=False,
                               inplace=True, ignore_index=True)

    # Inline visualizations
    # This breaks nice, tidy encapsulation so we'll make it switchable,
    # if we're using this from a notebook we can just allow the default
    # behaviour.
    if visualize:
        # print stats
        print(f'MAE \nmodel vs. human: {mae_vs_human}')
        print(f'model vs. lola: {mae_vs_lola}')
        print(f'lola vs. human: {mae_lola_vs_human}')

        print(f'RMSE \nmodel vs. human: {rmse_vs_human}')
        print(f'model vs. lola: {rmse_vs_lola}')
        print(f'lola vs. human: {rmse_lola_vs_human}\n')

        print('MAE per spot:')
        pd.set_option('display.max_rows', 300)
        if spot_validate:
            print(spot_error.describe())
            display(spot_error)

        # Basic histogram and scatterplot comparisons to LOLA and human reports
        bins = 50
        r = [-6, 6]
        ax = plt.figure(figsize=(16, 8))
        plt.hist([(human.values - ypred.values),
                 (human.values - lola.values)],
                 bins=bins, range=r, alpha=0.5,
                 label=['model vs. human', 'lola vs. human'])
        ax.legend(bbox_to_anchor=(0.8, 0.8), shadow=True,
                  prop={'size': 12})
        plt.xlabel('error size (ft)', fontsize=20)
        plt.ylabel('# of occurences', fontsize=20)
        plt.show()

        # TODO: fix scatter to add density - otherwise unclear with binned data
        # rx = plt.figure(figsize=(16, 8))
        # plt.scatter(human.values, lola.values, label='human vs. lola')
        # plt.scatter(human.values, ypred.values,
        #            label='human vs. model')
        # rx.legend(bbox_to_anchor=(0.8, 0.8),
        #          shadow=True, prop={'size': 12})
        # plt.xlabel('height (ft)', fontsize=20)
        # plt.ylabel('height (ft)', fontsize=20)
        # plt.show()

        if classifier:
            plot_cm(ypred.to_numpy().transpose(),
                    human.to_numpy().transpose(), name)
        else:
            true = height_classification(human,
                                         meters=not units_feet).\
                to_numpy().transpose()
            pred = height_classification(ypred,
                                         meters=not units_feet).\
                to_numpy().transpose()
            plot_cm(pred, true, name)

    return mae_vs_human, mae_vs_lola, mae_lola_vs_human


def wind_val_plot(model, config, fname=None, specify_surf_height=None,
                  wind_min=0, wind_max=50, speed_incr=2,
                  degree_incr=1, bin_labels=bin_labels,
                  title='Class Changes Across Wind '
                        'Direction & Speed (kts)'):
    """
    Creates a polar plot that shows class prediction transitions across changes
    in wind speed and direction.

    Args:
        model: A trained tensorflow model.
        config: LotusDataset config file name corresponding to that
            trained model.
        specify_surf_height: (int) A parameter to override average swell and
                        surf height data in the synthetic dataset with a data
                        sample that has a specific surf height.
        wind_min: (int) Minimum wind speed for test range in knots.
        wind_max: (int) Maximum wind speed for test range in knots.
        speed_incr: (int) Sampling rate for speed, i.e. speed_incr of 2
                tests samples at 0, 2, 4 knots, etc.
        degree_incr: (int) Sampling rate for direction in degrees.
        bin_labels: List of strings to label the predicted classes.

    Returns:
        The synthesized data, prints out a plot.
    """
    synth = make_synth_data_wind(model, config,
                                 r_min=wind_min, r_max=wind_max,
                                 r_incr=speed_incr,
                                 th_incr=degree_incr,
                                 specify_surf_height=specify_surf_height)
    if synth is None:
        return

    z = get_z(synth,
              r_col='GFS_wind_speed',
              r_min=wind_min, r_max=wind_max, r_incr=2,
              th_col='GFS_wind_dir_norm',
              th_min=0, th_max=359, th_incr=degree_incr)

    polar_5class_plot(z, title, fname,
                      r_min=wind_min, r_max=wind_max,
                      r_incr=speed_incr,
                      th_incr=degree_incr,
                      bin_labels=bin_labels)

    return synth


def get_theta_by_r(r_col='GFS_wind_speed', r_min=0, r_max=20, r_incr=1,
                   th_col='GFS_wind_dir_norm',
                   th_min=0, th_max=360, th_incr=1):
    """
    Create a theta by r array of synthetic values across our sample space
    and return a DataFrame to be incorporated into features for model
    predictions.

    Args:
        r_col: Name of the column label for radius. (i.e. speed)
        r_min: Minimum radius value. (i.e. min speed / 0 kts)
        r_max: Maximum radius value. (i.e. max speed)
        r_incr: Increment at which to sample r value.
        th_col: Name of the column label for theta. (i.e. direction)
        th_min: Minimum theta value. (i.e. 0 degrees)
        th_max: Maximum theta value. (i.e. 359 degrees)
        th_incr: Increment at which to sample theta value.

    Returns:
        A dataframe of all r and theta value combos.
    """
    theta = np.array((range(0, th_max-(th_incr-1), th_incr)))
    theta_bins = len(theta)
    theta = np.repeat(
        [theta], int(
            np.ceil(
                (r_max + r_incr + 1) / r_incr)), axis=0).flatten()
    r = np.array((range(r_min, r_max+r_incr+1, r_incr)))
    r = np.repeat(r, theta_bins, axis=0)
    synth = pd.DataFrame(np.array([theta, r]).transpose(),
                         columns=[th_col, r_col])
    return synth


def make_synth_data_wind(model, config, r_min, r_max, r_incr=1,
                         th_incr=1, specify_surf_height=None):
    """
    Create a synthetic dataset to run through model and produce validation
    plot.

    Args:
        config: Name of config file used by model.
        r_min: (int) Minimum of radius, aka min wind speed. (kts)
        r_max: (int) Maximum of radius, aka max wind speed. (kts)
        r_incr: (int) Increment for wind speed samples.
        th_incr: (int) Increment for degree samples.
        specify_surf_height: (int) Specify a surf height for the swell
                                and surf height samples to override the ave
                                value used in the plot by default.

    Returns:
        DataFrame of synthetic wind data to satisfy plot axes.
    """
    # Get theta by r grid for wind speed, direction
    synth = get_theta_by_r(r_min=r_min, r_max=r_max, r_incr=r_incr,
                           th_incr=th_incr)

    # Adjust synthetic values to be -180, 180
    synth['GFS_wind_dir_norm'] -= 180
    # Get on- and offshore components of wind
    synth['GFS_onshore_comp'] = np.cos(np.radians(
        synth['GFS_wind_dir_norm'])) * synth['GFS_wind_speed']
    synth['GFS_crossshore_comp'] = np.sin(np.radians(
        synth['GFS_wind_dir_norm'])) * synth['GFS_wind_speed']

    # Filling in the rest of the features with 'average' data
    if specify_surf_height is None:
        # Take average across whole training set
        sample = df
    else:
        # Take average of specific surf height subset
        spec = df.loc[df['human_report_surf_max_ft'] == specify_surf_height]
        if spec.shape[0] == 0:
            print(f'No sample for surf height of {specify_surf_height}')
            return None
        sample = spec

    synth['LotusSigH'] = sample.LotusSigH.mean()
    synth['human_report_surf_max_ft'] = int(
        sample.human_report_surf_max_ft.mean())
    for i in range(1, 5):
        for n in ['SigH', 'Tp', 'Spred', 'Pdir_norm']:
            synth[f'Lotus{n}_part{i}'] = sample.fillna(
                0)[f'Lotus{n}_part{i}'].mean()

    # Get feature info for this model from config by loading test set.
    ldsynth = LotusDataset(train=False, config_name=config)
    # Make sure synthetic features are in correct order
    model_feed = synth[list(ldsynth.feature_cols)]
    # Make sure we have a null value for the label col to support LotusDataset
    model_feed[ldsynth.label_cols[0]] = 0
    # Now force our synthetic dataset through any transforms.
    ldsynth = LotusDataset(config_name=config, force_df=model_feed)
    x, y = ldsynth[:]  # Correct feature order in class

    pred = np.round(model.predict(x)).flatten()

    synth['human_report_surf_condition_5pt'] = pred

    with pd.option_context('display.max_rows', 5,
                           'display.max_columns', None,
                           'display.width', 1000,
                           'display.precision', 3,
                           'display.colheader_justify', 'left'):
        display(synth.head(1))

    # Set wind dir back to display range, 0-360, 0 is offshore angle
    synth['GFS_wind_dir_norm'] = [t if t >= 0 else t +
                                  360 for t in synth['GFS_wind_dir_norm'].values]

    return synth


def get_z(synth, r_col, r_min, r_max, r_incr,
          th_col, th_min, th_max, th_incr):
    """
    Function to format model prediction data for plotting as
    colormesh.

    Args:
        synth: Dataframe of features/labels for every theta x r
                    to be plotted.
        r_col: Name of the column label for radius. (i.e. speed)
        r_min: Minimum radius value. (i.e. min speed / 0 kts)
        r_max: Maximum radius value. (i.e. max speed)
        r_incr: Increment at which to sample r value.
        th_col: Name of the column label for theta. (i.e. direction)
        th_min: Minimum theta value. (i.e. 0 degrees)
        th_max: Maximum theta value. (i.e. 359 degrees)
        th_incr: Increment at which to sample theta value.

    Returns: 
        Array `z` of model predictions to be used in colormesh.
    """
    y = []
    for d in range(th_min, th_max-(th_incr-1), th_incr):
        x = []
        rel = synth.loc[synth[th_col] == d]
        # Useful printout to precede possible exception....
        if rel.shape[0] == 0:
            print(f'd: {d}, shape: {rel.shape}')

        for s in range(r_min, r_max+1, r_incr):
            # TODO perhaps change hardcoded string to 'class col'
            cl = rel.loc[synth[r_col] == s][
                    'human_report_surf_condition_5pt'].values[0].astype('int')
            x.append(cl)
        y.append(x)
    z = np.array(y)
    # Set FLAT to zero for cmap purposes
    z -= 1

    return z


def polar_5class_plot(z, title, fname=None, r_min=0, r_max=30, r_incr=1,
                      th_incr=1, bin_labels=bin_labels):
    """
    Plot given z as colormesh.

    Args:
        z: 2D array of labels.
        title: Title for plot.
        fname: If specified, filename to save plot png to.
        r_min: Minimum radius value. (i.e. min speed / 0 kts)
        r_max: Maximum radius value. (i.e. max speed)
        r_incr: Increment at which to sample r value.
        th_incr: Increment at which to sample theta value.

    Returns:
        Nothing, plots polar plot of class predictions across changes
        in theta and r. Saves plot to `fname` if specified.
    """
    # Create custom colormap
    #         flat    vp         poor       ptf        fair       ftg
    colors = ['grey', '#043464', '#47a7f9', '#32d5c5', '#32d543', '#f78e25']
    if z.min() == z.max():
        colors = colors[z.min():z.max()+1]
    else:
        colors = colors[z.min():z.max()]
    # if (z.min() == 0) and (z.max() == 0):
    #     raise Exception('Model output all zeroes.')
    cmap = matplotlib.colors.ListedColormap(colors)

    # Setup figure
    fig = plt.figure(figsize=(30, 30))
    plt.rcParams.update({'font.size': 22})
    plt.tight_layout()
    ax = Axes3D(fig)
    az = plt.subplot(projection='polar')
    az.set_title(title)
    az.set_theta_zero_location('N')
    az.set_theta_direction(-1)
    az.set_rlim(r_min, r_max)

    # Radius, azimuth, theta for mesh
    rad = np.linspace(0, r_max + r_incr + 1,
                      int((r_max + r_incr + 2) / r_incr))  # wind speed
    azm = np.linspace(0, 2*np.pi, int(360/th_incr))  # wind dir
    r, th = np.meshgrid(rad, azm)

    # Plot data
    plt.pcolormesh(th, r, z, cmap=cmap)
    plt.plot(azm, r, color='k', ls='none')

    # Gridlines
    plt.grid(b=True, color='black', linestyle='-', which='major', alpha=0.7)
    # theta lines every 10deg
    lines, labels = plt.thetagrids([theta * 10 for theta in range(360//10)])
    # r lines at increment of samples
    plt.rgrids([_ for _ in range(r_min, r_max+r_incr+1, r_incr)])
    # Faux gridlines test
    # for rad in range(r_min, r_max + r_incr + 1, r_incr):
    #     plt.plot(np.linspace(0, 2*np.pi, 100), np.ones(100)*rad,
    #                          color='black', linestyle='-')

    # Colorbar
    cbar = plt.colorbar()
    # print(f'zmin: {z.min()}, zmax: {z.max()}')
    loc = np.array(
        [0.0]) if z.min() == z.max() else np.arange(
        0.5, z.max() + 0.5, 1)
    cbar.set_ticks(loc)
    cbar.set_ticklabels(bin_labels)

    plt.show()

    if fname is not None:
        fig.savefig(fname=fname, dpi=200, bbox_inches='tight')

    # (synth.head())
