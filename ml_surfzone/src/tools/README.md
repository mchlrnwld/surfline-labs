# ML Surfzone Tools

Tooling to retrieve the training and test data, run feature pre-processing 
and validate results against existing benchmarks.

## `data.py`
Rudimentary functions to access datasets as pandas DataFrames, can also 
apply transformations. 

## `validate.py`
Tools for validating and visualizing new models against LOLA and LOTUS data.

`validate` takes model-predicted max surf heights against the test set as 
input and prints a variety of error metrics, a list of the top errors by
spot, a histogram of error size and scatter plot of distribution, and a 
confusion matrix and log density map for max height as a classification 
problem.

Right now, it does not print stats any differently when a classifier model
is being fed in, and that needs updating. 

Example usage: 
```python
ypred = clf.predict(features)
validate(ypred, name='smooth_brain', classifier=False)
```

## `lotus_dataset.py`
See README.md in `mlsurfzone` directory for details.

## `transforms.py`
Transformations formatted to be used by `lotus_dataset.py`. 

They accept a pandas DataFrame or Series as input and output a 
df or Series with the transform applied. Can be imported for use 
on outside DataFrames.
