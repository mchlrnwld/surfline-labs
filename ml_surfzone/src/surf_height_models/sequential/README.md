# Validations of both Sequential and Non-Sequential Samples

## Inputs
I've decoupled this from the models for the time being, but we can merge them later if we decide that's better.
`validate_seq.ipynb` wants to load a pickle file that is a nested dictionary structured as:
```python
{'5d703b2ae4a3a8000133196a': {['predictions':array, 'labels':array, 'timestamps':array, 'lola':array]}, ...}
```
where each spotid key has a dictionary with model predictions, timestamps, and lola & lotus values.

## Plots
This notebook basically makes just two sets of plots, one that plots the raw data in time chunks (one plot for
each temporal chunk of sequential samples (<1 day offsets between s_i and s_i+1), for each spot.

The other set of plots generated are just 2D density plots of `model` vs `label`, with some
basic (non-temporal) statistics overlaid:

![alt text](hex.png)

where `out` shows the percent of 2 sigma outliers and `sig` shows the effective spread about the line `y=x`.
`sig` does differ from MAE as you can have similar MAE values but varying degrees of spread in the `y=x`
basis. For perfect models, all three of the numbers in the plots would go to zero. One caveat here is
I'm assuming these distributions can be approximated by a bivariate gaussian, which with a larger sample
set I think would be clearer.