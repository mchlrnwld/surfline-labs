# Training script for an all spot model with single-spot evaluation,
# using only the first two lotus partitions as features with no additional transforms
# (set in mlsurfzone/config_mlp.py). this can be generalized for different
# models by changing the get_model def.
#
# But two callable methods here: train() and evaluate()
#
import os
import logging
import numpy as np
import torch
from collections import defaultdict

from torch.utils.data import DataLoader
from mlsurfzone.lotus_dataset import LotusDataset

from ipdb import set_trace

logging.basicConfig()
logging.getLogger().setLevel(logging.INFO)
log = logging.getLogger(__name__)


def get_model(input_shape, output_shape, n_layer=3, n_neuron=10):
    """
    Build a small fully connected net with relu activations,
    where n_layer excludes input and output layers.
    """
    layers = []
    for i in range(n_layer + 1):
        n = input_shape if i == 0 else n_neuron
        layers.append(torch.nn.Linear(n, n_neuron))
        layers.append(torch.nn.ReLU())

    # Define output layer. We want linear activation output for regression,
    # so just leave off the last ReLU
    layers.append(torch.nn.Linear(n_neuron, output_shape))
    return torch.nn.Sequential(*layers)


def train(epochs=1, batch_size=30, cuda=True, checkpoint_name='./mlp.pth'):

    # Build dataset generator specific to the data requirements of this model
    # (specifically sequence_length = 1)
    dataset = LotusDataset(config_name='config_mlp.yaml')

    # Show me some stuff, like which features I'm using.
    # Shut this up by changing logging.INFO at the top.
    log.info(f'Using {len(dataset.feature_cols)} features:')
    log.info(f' {dataset.feature_cols}')
    log.info(f'Dataset has {len(dataset)} samples for '
              f'len(dataset.available_spotids)')
    log.info(f'Ground truth labels: {dataset.label_cols}')

    # Figure out what the shapes of the data are
    feature, label = dataset[0]

    # Outputs have an extra dimension, so remove it to get sizes
    input_shape, output_shape = feature.shape[-1], label.shape[-1]

    # Get a model instance. Maybe define arch somewhere else?
    model = get_model(input_shape, output_shape, checkpoint=checkpoint_name)
    if cuda: model = model.to('cuda')
    
    # Test the model shapes are sane -- can we do a forward pass
    # without shape errors?
    tensor = torch.from_numpy(feature)
    if cuda: tensor = tensor.to('cuda')
    random_pred = model(tensor)

    # Forward pass should give us two random numbers, since the net weights
    # are randomly initialized. Two because we have two labels defined
    # in the config_mlp.yaml file - surf_height_min and surf_height_max
    log.info(f'Random prediction: {random_pred}')

    # Construct DataLoader from our custom LotusDataset
    dataloader = DataLoader(dataset, batch_size=batch_size, shuffle=True, num_workers=3)
    
    # Now we can set up a loss funciton and optimizer
    loss_fn = torch.nn.MSELoss(reduction='sum')
    optimizer = torch.optim.Adam(model.parameters(), lr=1e-2)
    # Add a learning rate scheduler here

    n_batch = len(dataset) // batch_size
    for epoch in range(epochs):
        for ibatch, (features, labels) in enumerate(dataloader):
            if cuda:
                features = features.to('cuda')
                labels = labels.to('cuda')
                
            # Step forward through network
            predictions = model(features)

            # Compute loss for this step
            loss = loss_fn(predictions, labels)
                
            # Compute gradient wrt weights in newtork & update
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            
            if not ibatch % 200:
                pct_done = (ibatch + 1.) / n_batch * 100.
                log.info(f'loss: {loss:.4f} ({pct_done:.1f}%)')

    # Save the model to disk.
    torch.save(model.state_dict(), checkpoint_name)
    log.info(f'Saved trained model to {checkpoint_name}')


def evaluate(checkpoint_name='./mlp.pth', cuda=True):

    # Eval dataset
    dataset = LotusDataset(config_name='config_mlp.yaml', train=False)
    feature, label = dataset[0]
    input_shape, output_shape = feature.shape[-1], label.shape[-1]
    
    # Load traind model weights into a model instance:
    if not os.path.isfile(checkpoint_name):
        raise ValueError(f'Checkpoint not found: {checkpoint_name}')

    model = get_model(input_shape, output_shape)  # model backbone
    checkpoint = torch.load(checkpoint_name, map_location='cpu')  # actual weights
    model.load_state_dict(checkpoint)
    log.info(f'Loaded weights from {checkpoint_name}')
    if cuda: model.to('cuda')
        
    # Do inference per spot
    predictions = defaultdict(list)
    ground_truths = defaultdict(list)
    timestamps = defaultdict(list)
    
    # get list of all spotids in the dataset
    available_spotids = dataset.available_spotids
    # or just look at 5 most populated spots:
    available_spotids = list(dataset.get_spot_distribution().keys())[:5]
    for spotid in available_spotids:
        dataset.spotids = spotid
        log.info(f'{len(dataset)} samples for spot {spotid}')
        for i in range(len(dataset)):  # len will be updated for only this spot
            dataset.spotids = spotid
            feature, label = dataset[i]
            feature = torch.from_numpy(feature)
            if cuda: feature = feature.to('cuda')

            # Forward pass
            prediction = model(feature)
            
            # Detach from gpu and store
            predictions[spotid].append(prediction.cpu().detach().numpy())
            ground_truths[spotid].append(label)

            # Store timestamp for this sample, which is in dataset.meta
            timestamps[spotid].append(dataset.meta['timestamps'])

            # sanity check we're on the right spot in the dataset
            assert np.all(dataset.meta['spotid'].values == spotid)

        print(f'Finished {spotid}')

    # do some stats with preds & ground truths. flatten it out to get
    # general stats instead of per spot
    # maybe just pickle those predictions to do stuff with them later.
        
if __name__ == '__main__':
    evaluate()
