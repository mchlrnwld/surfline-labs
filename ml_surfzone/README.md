# ML Surfzone

All models designed to take LOTUS model data and create surf height or condition
forecasts. This project includes the data access and feature processing and 
a validation toolkit for analysing results.

# Getting Started

### Setup environment

Copy `.env.sample` to `.env` and update remote paths to the correct test / train
data.

Create an account with Artifactory and follow the instructions here to create 
your API key:

https://wavetrak.atlassian.net/wiki/spaces/TECH/pages/763002972/Artifactory+Development+Workflow

(Head to Artifactory, click your email top right and hit 'generate key')

Copy `pip.conf.sample` to `pip.conf` adding your username and this key.

### Retrieve the data

The project data is maintained and stored in S3 as static CSV and pkl files,
these can be easily downloaded:

```
make data
```

*N.B. You'll need AWS credentials for `surfline-dev` in a profile of that name*

### Run the notebook server

```
make notebook
```
