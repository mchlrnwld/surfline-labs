import numpy as np
import pandas as pd


def merge_reports_into_forecasts(reportsdf: pd.DataFrame, hindcastdf: pd.DataFrame) -> pd.DataFrame:
    """
    Merges a reports dataframe with a forecast dataframe by finding the report closest to each forecast
    :param reportsdf: pandas dataframe containing reports
    :param hindcastdf: pandas dataframe containing the forecast values
    :returns: new dataframe with all forecast and corresponding reports data
    """
    hindcast_w_reports = hindcastdf.copy()

    matching_report_index = []
    for hindcasttime in hindcast_w_reports["Datetime"]:
        matching_report_index.append(((reportsdf["valid_time"].sub(hindcasttime)).abs()).idxmin())

    surf_minimum_ft = [reportsdf.iloc[x]["surf_minimum_ft"] for x in matching_report_index]
    surf_maximum_ft = [reportsdf.iloc[x]["surf_maximum_ft"] for x in matching_report_index]
    surf_occasional_ft = [reportsdf.iloc[x]["surf_occasional_ft"] for x in matching_report_index]
    surf_condition = [reportsdf.iloc[x]["surf_condition"] for x in matching_report_index]
    surf_reporter = [reportsdf.iloc[x]["surf_reporter"] for x in matching_report_index]

    reporttimes = [reportsdf.iloc[x]["valid_time"] for x in matching_report_index]
    timediffs = reporttimes - hindcast_w_reports["Datetime"].values

    hindcast_w_reports["surf_minimum_ft"] = surf_minimum_ft
    hindcast_w_reports["surf_maximum_ft"] = surf_maximum_ft
    hindcast_w_reports["surf_occasional_ft"] = surf_occasional_ft
    hindcast_w_reports["surf_condition"] = surf_condition
    hindcast_w_reports["surf_reporter"] = surf_reporter
    hindcast_w_reports["report_time"] = reporttimes
    hindcast_w_reports["report_time_diff"] = timediffs

    hindcast_w_reports.loc[
        hindcast_w_reports["report_time_diff"].abs() > (0.51 * 3600),
        [
            "surf_minimum_ft",
            "surf_maximum_ft",
            "surf_occasional_ft",
            "surf_condition",
            "surf_reporter",
        ],
    ] = None

    return hindcast_w_reports


def get_rel_angles(angles: list) -> list:
    """
    Shifts angles [0,360] to be between -180 and 180
    """
    angles = angles % 360
    angles = np.where(angles > 180, angles - 360, angles)
    return angles


def generate_data(
    report: str = "/opt/app/src/report.csv",
    lotus: str = "/opt/app/src/hindcast.csv",
    outfile: str = "/opt/app/src/merged_data.csv",
):
    """
    Generates a merged csv, using reports and hindcasts
    :param report: location of csv with reports
    :param lotus: location of csv with hindcasts
    :returns: location of new merged file
    """
    merged = pd.read_csv(lotus)
    reportsdf = pd.read_csv(report)

    merged = merge_reports_into_forecasts(reportsdf, merged)
    # merged = merged.loc[merged["report_time_diff"].abs() < (0.51 * 3600)]
    merged.dropna(subset=["surf_maximum_ft"], inplace=True)

    merged.to_csv(outfile, index=False)
