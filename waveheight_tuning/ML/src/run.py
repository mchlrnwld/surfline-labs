import pandas as pd
from BWH import SpectralRefraction as SR
import torch
import pickle
import multiprocessing
from functools import partial
import argparse
import os
from typing import Iterable

multiprocessing.freeze_support()


def run_training(spot: str):
    """
    Runs training for a spot
    """
    # create results folder
    os.makedirs("/opt/app/src/results/", exist_ok=True)
    # initialize class instance and download data
    tuner = SR(spotid=spot)
    # run training if data is complete
    if tuner.data_complete:
        print("Running for spot {}".format(spot))
        # some hard coded ML parameters
        epochs = 30
        optimizer = torch.optim.RMSprop([tuner.def_ref_matrix_tuned], lr=1e-3)
        scheduler = torch.optim.lr_scheduler.CosineAnnealingWarmRestarts(optimizer, epochs)
        tuner.init_optimizer(optimizer, scheduler)
        # loop through epochs
        for epoch in range(epochs):
            print("epoch ", epoch + 1)
            # shuffle dataframe of input data
            tuner.df = tuner.df.sample(frac=1).reset_index(drop=True)
            _ = tuner.train_one_epoch()
        # validate model after training
        _, _, _, _, _ = tuner.validate()
        # save model after training
        with open("/opt/app/src/results/" + spot + ".pkl", "wb") as fp:
            pickle.dump(tuner.def_ref_matrix_tuned, fp)


def chunks(spotlist: list, n: int) -> Iterable:
    """
    takes a list and integer n as input and returns generator objects of n lengths from that list
    """
    for i in range(0, len(spotlist), n):
        yield spotlist[i : i + n]


def run(spots: list, processes: int = 1):
    """
    Runs training for all spots in list on a certain number of processors
    """
    # pool object creation
    pool = multiprocessing.Pool(processes=processes)  # spawn 4 processes
    calculate_partial = partial(
        run_training
    )  # partial function creation to allow input to be sent to the calculate function
    _ = pool.map(calculate_partial, list(spots))
    pool.close()


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Waveheight tuning")
    parser.add_argument("--spots", nargs="+", default="emptyspotlist", type=str)
    parser.add_argument("--csv", default="nocsv", type=str)
    parser.add_argument("--spotcolumn", default="SpotId", type=str)
    parser.add_argument("--processes", default=1, type=int)
    args = parser.parse_args()

    # check if either spot list or csv is specified
    assert not (("emptyspotlist" in args.spots) and (args.csv == "nocsv")), "Please provide a spot list or csv"
    if args.csv != "nocsv":
        assert os.path.isfile(args.csv), "Please provide a valid csv"
        df = pd.read_csv(args.csv)
        spots = df[args.spotcolumn].unique()
        spots = list(spots)
    else:
        spots = args.spots

    # print list of spots
    print("Spot list:", spots)
    # print number of processes
    print("Number of processes:", args.processes)

    # run training
    if len(spots) == 1:
        spot = spots[0]
        run_training(spot)
    else:
        run(spots, processes=args.processes)
