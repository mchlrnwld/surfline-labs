from math import isnan
from typing import Optional


class SwellPartition:
    def __init__(
        self,
        height: Optional[float],
        period: Optional[float],
        direction: Optional[float],
        spread: Optional[float] = None,
    ):
        """
        Construct an instance of SwellPartition.

        Args:
            height: Swell partition height, in meters.
            period: Swell partition period, in seconds.
            direction: Swell partition direction, in degrees. This is the
                direction the swell is travelling from. I.e. 270 degrees is a
                swell travelling, from the west, going east.
        """
        self.height = float(height) if height is not None and not isnan(height) else 0.0
        self.period = float(period) if period is not None and not isnan(period) else 0.0
        self.direction = (
            float(direction) if direction is not None and not isnan(direction) else 0.0
        )
        self.spread = float(spread) if spread is not None and not isnan(spread) else 0.0

    def __bool__(self):
        """
        Defines the truth value of a SwellPartition object.
        A SwellPartition object is considered True if all of
        its height and period attributes are greater than 0.
        Otherwise it is False.
        """
        return self.height > 0.0 and self.period > 0.0

    def __eq__(self, other):
        return (
            self.height == other.height
            and self.period == other.period
            and self.direction == other.direction
        )

    def angle_difference(self, angle: float) -> float:
        """
        Calculates the difference in angle from the swell's direction
        compared to the passed angle, in range (-180, 180].

        Args:
            angle: an angle in, in degrees

        Returns:
            Angle difference, in range (-180, 180].
        """
        difference = (angle - self.direction) % 360

        if difference > 180:
            return difference - 360

        return difference
