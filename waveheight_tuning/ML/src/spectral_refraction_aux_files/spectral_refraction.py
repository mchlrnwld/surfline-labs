import math
from typing import Any, List, Optional, Tuple

import numpy as np  # type: ignore
from scipy.interpolate import RectBivariateSpline  # type: ignore

from spectral_refraction_aux_files.frequency_calculations import (
    create_discrete_frequency_steps,
    create_frequency_distribution,
)
from spectral_refraction_aux_files.surf_height_input import SurfHeightInput
from spectral_refraction_aux_files.swell_partition import SwellPartition


class SpectralRefraction:
    """
    Initializes all values necessary to compute
    surf heights using the Spectral Refraction algorithm.
    """

    def __init__(self):
        self._direction_bin_size = 15
        minimum_frequency = 0.0418
        maximum_frequency = 0.411

        self.lola_directions = np.arange(
            self._direction_bin_size,
            360 + self._direction_bin_size,
            self._direction_bin_size,
        )

        self.lola_frequencies = create_frequency_distribution(
            minimum_frequency, maximum_frequency
        )

        self.discrete_frequency_steps = create_discrete_frequency_steps(
            self.lola_frequencies
        )

        self._frequencies = [
            0.0417999998,
            0.0458999984,
            0.0505000018,
            0.0555999987,
            0.0612000003,
            0.0672999993,
            0.074000001,
            0.0813999996,
            0.0895000026,
            0.0984999985,
            0.108000003,
            0.119000003,
            0.130999997,
            0.143999994,
            0.158999994,
            0.173999995,
            0.192000002,
            0.210999995,
            0.231999993,
            0.254999995,
            0.280999988,
            0.308999985,
            0.340000004,
            0.374000013,
            0.411000013,
        ]

    def _spectra_to_swell_partitions(
        self, spectra: List[List[float]]
    ) -> Tuple[float, List[SwellPartition]]:
        """
        Generates the top 3 swell partitions
        from spectra.

        Args:
            spectra: Spectra to generate top 3 swell partitions for.

        Returns:
            Top 3 swell partitions.

        """
        swell_partitions = [SwellPartition(0.0, 0.0, None) for i in range(3)]

        overall_swell_height = (
            3.28 * 4 * math.sqrt(sum(data for row in spectra for data in row))
        )

        # Now find direction peaks.
        sorted_spectra = self._sort_spectra(spectra)
        # Then find the top 3 swell partitions.
        for swell_index, row_index_and_data in enumerate(
            sorted(
                [
                    (row_index, data)
                    for row_index, row in enumerate(sorted_spectra)
                    for data in row
                    if data > 0.0
                ],
                reverse=True,
                key=lambda row_index_and_data: row_index_and_data[1],
            )
        ):
            row_index, data = (row_index_and_data[0], row_index_and_data[1])
            height = 3.28 * 4 * math.sqrt(data)
            if height < 0.5 or swell_index > 2:
                break

            period = 1.0 / self._frequencies[row_index]

            swell_partitions[swell_index].height = height if height > 0.0 else 0.0
            swell_partitions[swell_index].period = period if 1.0 / period else 0.0

        return (
            overall_swell_height if overall_swell_height > 0.0 else 0.0,
            swell_partitions,
        )

    def _sweep_through_spectra_(
        self, spectra: List[List[float]], sorted_spectra: List[List[float]]
    ) -> Tuple[List[List[float]], List[List[float]]]:
        """
        Sweeps through the spectra.

        Args:
            spectra: The original spectra unmodified.
            sorted_spectra: The sorted spectra to sweep and sort through.

        Returns:
            The previous sorted spectra and current sorted spectra.

        """
        spectra_row_length = len(spectra)
        spectra_column_length = len(spectra[0])
        previous_sorted_spectra = [[data for data in row] for row in sorted_spectra]
        current_sorted_spectra = [[data for data in row] for row in sorted_spectra]

        # First directional pass
        # First compare end directions
        for j in range(spectra_column_length):
            if spectra[1][j] >= spectra[spectra_row_length - 1][j]:
                current_sorted_spectra[1][j] = (
                    current_sorted_spectra[1][j]
                    + current_sorted_spectra[spectra_row_length - 1][j]
                )
                current_sorted_spectra[spectra_row_length - 1][j] = 0

        for i in range(1, spectra_row_length):
            for j in range(spectra_column_length):
                if spectra[i][j] >= spectra[i - 1][j]:
                    current_sorted_spectra[i][j] = (
                        current_sorted_spectra[i][j] + current_sorted_spectra[i - 1][j]
                    )
                    current_sorted_spectra[i - 1][j] = 0

        # Second second directional pass
        # First compare end directions
        for j in range(spectra_column_length):
            if spectra[spectra_row_length - 1][j] >= spectra[1][j]:
                current_sorted_spectra[spectra_row_length - 1][j] = (
                    current_sorted_spectra[spectra_row_length - 1][j]
                    + current_sorted_spectra[1][j]
                )
                current_sorted_spectra[0][j] = 0

        for i in range(spectra_row_length - 2, 0, -1):
            for j in range(spectra_column_length):
                if spectra[i][j] >= spectra[i + 1][j]:
                    current_sorted_spectra[i][j] = (
                        current_sorted_spectra[i][j] + current_sorted_spectra[i + 1][j]
                    )
                    current_sorted_spectra[i + 1][j] = 0

        # First frequency pass
        for j in range(1, spectra_column_length):
            for i in range(spectra_row_length):
                if spectra[i][j] >= spectra[i][j - 1]:
                    current_sorted_spectra[i][j] = (
                        current_sorted_spectra[i][j] + current_sorted_spectra[i][j - 1]
                    )
                    current_sorted_spectra[i][j - 1] = 0

        # Second frequency pass
        for j in range(spectra_column_length - 2, 0, -1):
            for i in range(spectra_row_length):
                if spectra[i][j] >= spectra[i][j + 1]:
                    current_sorted_spectra[i][j] = (
                        current_sorted_spectra[i][j] + current_sorted_spectra[i][j + 1]
                    )
                    current_sorted_spectra[i][j + 1] = 0

        # First diagonal pass
        for i in range(1, spectra_row_length):
            for j in range(1, spectra_column_length):
                if spectra[i][j] >= spectra[i - 1][j - 1]:
                    current_sorted_spectra[i][j] = (
                        current_sorted_spectra[i][j]
                        + current_sorted_spectra[i - 1][j - 1]
                    )
                    current_sorted_spectra[i - 1][j - 1] = 0

        # Second diagonal pass
        for i in range(spectra_row_length - 2, 0, -1):
            for j in range(spectra_column_length - 1):
                if spectra[i][j] >= spectra[i + 1][j + 1]:
                    current_sorted_spectra[i][j] = (
                        current_sorted_spectra[i][j]
                        + current_sorted_spectra[i + 1][j + 1]
                    )
                    current_sorted_spectra[i + 1][j + 1] = 0

        # Third diagonal pass
        for i in range(1, spectra_row_length):
            for j in range(spectra_column_length - 1):
                if spectra[i][j] >= spectra[i - 1][j + 1]:
                    current_sorted_spectra[i][j] = (
                        current_sorted_spectra[i][j]
                        + current_sorted_spectra[i - 1][j + 1]
                    )
                    current_sorted_spectra[i - 1][j + 1] = 0

        # Fourth diagonal pass
        for i in range(spectra_row_length - 2, 0, -1):
            for j in range(1, spectra_column_length):
                if spectra[i][j] >= spectra[i + 1][j - 1]:
                    current_sorted_spectra[i][j] = (
                        current_sorted_spectra[i][j]
                        + current_sorted_spectra[i + 1][j - 1]
                    )
                    current_sorted_spectra[i + 1][j - 1] = 0

        return previous_sorted_spectra, current_sorted_spectra

    def _sort_spectra(self, spectra: List[List[float]]) -> List[List[float]]:
        """
        Sort and sweep through spectra until the spectra no longer changes.
        Or until 100 passes have been made.

        Args:
            spectra: The spectra to sort and sweep through.

        Returns:
            Sorted spectra.
        """
        current_sorted_spectra = [[data for data in row] for row in spectra]

        for k in range(1, 100):
            (
                previous_sorted_spectra,
                current_sorted_spectra,
            ) = self._sweep_through_spectra_(spectra, current_sorted_spectra)
            if all(
                math.isclose(num_1, num_2, abs_tol=1e-4)
                for row_1, row_2 in zip(current_sorted_spectra, previous_sorted_spectra)
                for num_1, num_2 in zip(row_1, row_2)
            ):
                break

        return current_sorted_spectra

    def _calculate_surf_height(
        self, factor: float, swell_partitions: List[SwellPartition]
    ) -> float:
        """
        Calculate surf heights given swell partitions
        and a multiplication factor.

        Args:
            factor: Multiplication factor to calculate surf height.
            sorted_spectra: The sorted spectra to sweep and sort through.

        Returns:
            Surf height.
        """
        if len(swell_partitions) < 3:
            raise ValueError("3 Swell Partitions must be given")

        return math.sqrt(
            (
                swell_partitions[0].height
                * math.sqrt(swell_partitions[0].period * factor)
            )
            ** 2
            + (
                swell_partitions[1].height
                * math.sqrt(swell_partitions[1].period * factor)
            )
            ** 2
            + (
                swell_partitions[2].height
                * math.sqrt(swell_partitions[2].period * factor)
            )
            ** 2
        )

    def _surf_heights_from_swell_partitions(
        self,
        overall_swell_height: float,
        swell_partitions: List[SwellPartition],
    ) -> Tuple[float, float]:
        """
        Calculate surf height min and max.

        Args:
            overall_swell_height: Swell height used to calculate surf height.
            swell_partitions: Swell partitions used to calculate surf heights.

        Returns:
            Surf height min and max.
        """
        if len(swell_partitions) < 3:
            raise ValueError("3 Swell Partitions must be given")

        swell_partition_1, swell_partition_2, swell_partition_3 = (
            swell_partitions[0],
            swell_partitions[1],
            swell_partitions[2],
        )

        surf_min = self._calculate_surf_height(0.07, swell_partitions)
        surf_max = self._calculate_surf_height(0.10, swell_partitions)

        if surf_min < swell_partition_1.height:
            surf_min = swell_partition_1.height

        if surf_min < swell_partition_2.height:
            surf_min = swell_partition_2.height

        if surf_min < swell_partition_3.height:
            surf_min = swell_partition_3.height

        if surf_max < swell_partition_1.height:
            surf_max = swell_partition_1.height

        if surf_max < swell_partition_2.height:
            surf_max = swell_partition_2.height

        if surf_max < swell_partition_3.height:
            surf_max = swell_partition_3.height

        if surf_max <= 3:
            surf_max = overall_swell_height
            surf_min = overall_swell_height - 1

        if round(surf_min) == round(surf_max):
            surf_min = surf_max - 1

        if surf_min <= 0:
            surf_min = 0

        return surf_min / 3.28, surf_max / 3.28

    def _generate_lola_spectra(
        self, frequencies: Any, directions: Any, spectra_wave_energy: Any
    ) -> Any:
        """
        Generate LOLA spectra based on the frequencies and directions.

        Args:
            frequencies: Numpy array of 29 frequencies
                         for Lotus wave spectra.
            directions:  Numpy array of 36 directions
                         for Lotus wave spectra.
            spectra_wave_energy: Numpy array of Lotus 2D (29 x 36)
                                 spectra wave energy values.

        Returns:
            Lola spectra as numpy array.
        """
        adjusted_directions = np.concatenate([directions[1:], np.array([360])])
        adjusted_spectra_wave_energy = np.concatenate(
            (spectra_wave_energy[:, 1:], spectra_wave_energy[:, 0][:, None]),
            axis=1,
        )

        spline_interpolation = RectBivariateSpline(
            frequencies, adjusted_directions, adjusted_spectra_wave_energy
        )

        lola_spectra = spline_interpolation(self.lola_frequencies, self.lola_directions)

        # Remove any negative values artifacts
        # created by the interpolation
        lola_spectra[lola_spectra < 0] = 0
        return lola_spectra

    def _transform_spectra(
        self,
        frequencies: Any,
        directions: Any,
        spectra_wave_energy: Any,
        spectral_refraction_matrix: Any,
    ) -> List[List[float]]:
        """
        Transform spectra using spectral refraction matrix,
        frequency and directions.

        Args:
            frequencies: Numpy array of 29 frequencies
                         for Lotus wave spectra.
            directions:  Numpy array of 36 directions
                         for Lotus wave spectra.
            spectra_wave_energy: Numpy array of Lotus 2D (29 x 36)
                                 spectra wave energy values.
            spectral_refraction_matrix: Numpy array of 2D (24 x 18)
                                        spectral refraction matrix.

        Returns:
            Transformed spectra as list.
        """

        lola_spectra = self._generate_lola_spectra(
            frequencies, directions, spectra_wave_energy
        )

        return [
            [
                (
                    self.discrete_frequency_steps[i]
                    * (self._direction_bin_size * math.pi / 180)
                    * (
                        lola_spectra[i][j]
                        * (
                            spectral_refraction_matrix[j][17]
                            # Use the last column of the matrix
                            # for the remaining spectra.
                            if i >= 18
                            else spectral_refraction_matrix[j][i]
                        )
                    )
                )
                for j in range(24)
            ]
            for i in range(25)
        ]

    def _calculate_helper(
        self,
        frequencies: Any,
        directions: Any,
        spectra_wave_energy: Any,
        spectral_refraction_matrix: Any,
    ) -> Tuple[Optional[Tuple[float, float]], Optional[ValueError]]:
        """
        Helper function to calculate surf heights.

        Args:
            frequencies: Numpy array of 29 frequencies
                         for Lotus wave spectra.
            directions:  Numpy array of 36 directions
                         for Lotus wave spectra.
            spectra_wave_energy: Numpy array of Lotus 2D (29 x 36)
                                 spectra wave energy values.
            spectral_refraction_matrix: Numpy array of 2D (24 x 18)
                                        spectral refraction matrix.

        Returns:
            Surf height for given input.
        """

        if spectra_wave_energy is None or spectra_wave_energy.shape != (
            29,
            36,
        ):
            return (
                None,
                ValueError("spectra_wave_energy must have dimensions 29x36."),
            )

        if spectral_refraction_matrix is None or spectral_refraction_matrix.shape != (
            24,
            18,
        ):
            return (
                None,
                ValueError("spectral_refraction_matrix must have " "dimensions 24x18."),
            )

        transformed_spectra = self._transform_spectra(
            frequencies,
            directions,
            spectra_wave_energy,
            spectral_refraction_matrix,
        )

        (
            overall_swell_height,
            swell_partitions,
        ) = self._spectra_to_swell_partitions(transformed_spectra)

        surf_min, surf_max = self._surf_heights_from_swell_partitions(
            overall_swell_height, swell_partitions
        )
        return (round(surf_min, 2), round(surf_max, 2)), None

    def calculate(
        self, surf_height_inputs: List[SurfHeightInput]
    ) -> List[Tuple[Optional[Tuple[float, float]], Optional[ValueError]]]:
        """
        Calculate surf heights using the Spectral Refraction method.

        Args:
            surf_height_inputs: The inputs to calculate surf heights for.

        Returns:
            Surf height for each input.
        """
        return [
            self._calculate_helper(
                surf_height_input.frequencies,
                surf_height_input.directions,
                surf_height_input.spectra_wave_energy,
                surf_height_input.spectral_refraction_matrix,
            )
            for surf_height_input in surf_height_inputs
        ]
