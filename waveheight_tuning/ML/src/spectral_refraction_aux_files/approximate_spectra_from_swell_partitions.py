from typing import List

import numpy as np  # type: ignore
from scipy.special import gamma  # type: ignore

from spectral_refraction_aux_files.frequency_calculations import (
    create_frequency_distribution,
)
from spectral_refraction_aux_files.swell_partition import SwellPartition


def _create_jonswap_spectrum(
    frequencies: np.array,
    swell: SwellPartition,
    peak_enhancement_factor: float = 3.3,
    peak_width_low: float = 0.07,
    peak_width_high: float = 0.09,
    gravitational_constant: float = 9.81,
    normalize: bool = True,
) -> np.array:
    """
    Create JONSWAP spectrum for the given swell partition.
    The JONSWAP spectrum calculation is based on research
    done by the Joint North Sea Wave Project.

    Args:
        frequencies : Numpy array of frequencies.
        swell: Swell partition to generate JONSWAP spectrum for.
        peak_enhancement_factor : JONSWAP peak-enhancement factor.
        peak_width_low : Sigma value for frequencies <= 1/swell.period.
        peak_width_high : Sigma value for frequencies > 1/swell/period.
        gravitational_constant : Gravitational constant.
        normalize : Normalize resulting spectrum to match swell.height.
    """

    # Pierson-Moskowitz
    alpha = 1.0 / (0.06533 * peak_enhancement_factor ** 0.8015 + 0.13467) / 16.0
    spectrum = (
        alpha
        * swell.height ** 2
        * swell.period ** -4
        * frequencies ** -5
        * np.exp(-1.25 * (swell.period * frequencies) ** -4)
    )

    # JONSWAP
    peak_width = np.ones(frequencies.shape) * peak_width_low
    peak_width[frequencies > 1.0 / swell.period] = peak_width_high

    jonswap_spectrum = spectrum * peak_enhancement_factor ** np.exp(
        -0.5 * (swell.period * frequencies - 1) ** 2.0 / peak_width ** 2.0
    )

    if normalize:
        return jonswap_spectrum * (
            swell.height ** 2.0
            / (16.0 * np.trapz(jonswap_spectrum, frequencies, axis=0))
        )

    return jonswap_spectrum


def approximate_spectra_from_swell_partitions(
    swell_partitions: List[SwellPartition],
    minimum_frequency: float = 0.035,
    maximum_frequency: float = 0.5,
    direction_bin_size: int = 10,
) -> np.array:
    """
    Given a list of swell partitions, create a 2D spectra.
    The 2D spectra is created by generating JONSWAP spectrum
    for each swell partition. Then a directional discretization
    is applied to the JONSWAP spectrum based on swell direction and spread.

    Args:
        swell_partitions: List of SwellPartitions to create 2D spectra for.
        minimum_frequency: Minimum frequency  (default: 0.035)
        maximum_frequency: Maximum frequency  (default: 0.5)
        direction_bin_size: Directional bin size in degrees (default: 10)
    """

    number_of_directions = 360 / direction_bin_size

    frequencies = create_frequency_distribution(minimum_frequency, maximum_frequency)

    directions = np.arange(0, 360, direction_bin_size)

    valid_swell_partitions = [
        swell
        for swell in swell_partitions
        if swell.height >= 0.05 and swell.period >= 0.5
    ]

    spectra = np.zeros((int(len(frequencies)), int(number_of_directions)))

    for swell in valid_swell_partitions:
        jonswap_spectrum = _create_jonswap_spectrum(frequencies, swell)

        # Limit the minimum spread value to 8º to maintain stability.
        average_spread = 2 / max(8 * np.pi / 180, swell.spread * np.pi / 180) ** 2

        if average_spread > 3 or swell.spread < 10:
            minimum_spread = int(np.ceil(average_spread - 3))
            ratios = np.array(
                [
                    (average_spread - num) / (average_spread - num - 0.5)
                    for num in range(minimum_spread)
                ]
            )
            R = (
                np.prod(ratios)
                * gamma(average_spread - minimum_spread + 1)
                / gamma(average_spread - minimum_spread + 0.5)
            )
        else:
            R = gamma(average_spread + 1) / gamma(average_spread + 0.5)

        gamma_ratio = R / (2 * np.sqrt(np.pi))

        alpha_direction = 180 - abs(180 - abs(directions - swell.direction))
        ee = gamma_ratio * np.cos(np.radians(alpha_direction) / 2) ** (
            2 * average_spread
        )
        # Apply directional discretization to the JONSWAP spectrum
        # and add to spectra.
        spectra = np.array(
            [spectra[i] + jonswap_spectrum[i] * ee for i in range(len(frequencies))]
        )

    return spectra
