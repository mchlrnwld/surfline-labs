import numpy as np
import os
import netCDF4
import math
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap
from matplotlib.colors import BoundaryNorm
import matplotlib.dates as mdates
from matplotlib.dates import DateFormatter
import datetime
import glob
from pathlib import Path
from pytz import timezone
import pandas as pd


from approximate_spectra_from_swell_partitions import (
    _create_jonswap_spectrum,
    approximate_spectra_from_swell_partitions,
)


from lotus_directions import directions
from lotus_frequencies import frequencies
from spectral_refraction import (
    SpectralRefraction,
)
from surf_height_input import SurfHeightInput
from swell_partition import SwellPartition
from spectra_from_partitions import (
    get_partitions_from_grid,
)


main_dir = os.getcwd()


# Chart Colormap definition
blevels = [-1000, -750, -500, -250, -200, -100, -75, -50, -25, -10, 0]
cmap = LinearSegmentedColormap.from_list(name="cmap", colors=["Blue", "CornflowerBlue", "w"], N=len(blevels) - 1)
bnorm = BoundaryNorm(blevels, ncolors=len(blevels) - 1, clip=False)
cmap.set_over("0.7")  # set positive values (land) as light gray

colors_text = [
    [0.0039, 0.2588, 0.9961],
    [0.0039, 0.7922, 0.9961],
    [0.0039, 0.9686, 0.2980],
    [0.9922, 0.9725, 0.0000],
    [1.0000, 0.6667, 0.0000],
    [0.9961, 0.3804, 0.0000],
    [0.9961, 0.0000, 0.0000],
    [0.9961, 0.1608, 0.5765],
    [0.5490, 0.5490, 0.5490],
    [0.0863, 0.0863, 0.0863],
]


def quantize(signal, partitions, codebook):
    index = 0
    while index < len(partitions) and signal > partitions[index]:
        index += 1
    quanta = codebook[index]
    return index, quanta


def create_grid_position_chart(
    spot_xx,
    spot_yy,
    spot_lotus_lon,
    spot_lotus_lat,
    lotus_grid,
    spotPOI,
    spotName,
    bathymetric_file,
):

    lotus_source = main_dir + "/model_config_files"

    minimum_depth = 0

    if lotus_grid[0:4] == "GLOB":
        delta_xy_chart = 1
        buffer_multiplier_nearest_grid_point = 4
        delta_xy_text = 3 / 60
    else:
        delta_xy_chart = 0.45
        buffer_multiplier_nearest_grid_point = 8
        delta_xy_text = 1 / 60

    if spot_xx > 180:
        spot_xx = spot_xx - 360

    # Get grid caracteristics

    with open(lotus_source + "/ww3_grid.inp." + lotus_grid) as f:
        data_line = f.readlines()[20]
        data_line = data_line.split()
        nx = int(data_line[0])
        ny = int(data_line[1])
    with open(lotus_source + "/ww3_grid.inp." + lotus_grid) as f:
        data_line = f.readlines()[21]
        data_line = data_line.split()
        delta_xy_grid = float(data_line[0]) / float(data_line[2])
    with open(lotus_source + "/ww3_grid.inp." + lotus_grid) as f:
        data_line = f.readlines()[22]
        data_line = data_line.split()
        xi = float(data_line[0])
        yi = float(data_line[1])

    lon_grid = np.linspace(xi, xi + nx * delta_xy_grid, nx, endpoint=False)
    lon_grid = [round(float(x * 10000)) / 10000 for x in lon_grid]
    lon_grid = np.asarray(lon_grid)
    lat_grid = np.linspace(yi, yi + ny * delta_xy_grid, ny, endpoint=False)
    lat_grid = [round(float(x * 10000)) / 10000 for x in lat_grid]
    lat_grid = np.asarray(lat_grid)

    depth_grid = np.loadtxt(lotus_source + "/" + lotus_grid + ".depth", dtype="i", delimiter="  ") * 0.001
    mask_raw = np.loadtxt(lotus_source + "/" + lotus_grid + ".mask", dtype="i")

    ocean_mask = np.ma.masked_equal(mask_raw, 1)
    ocean_mask = np.ma.getmask(ocean_mask)

    mask_grid = mask_raw * 0
    mask_grid[ocean_mask] = 1

    land_mask = np.ma.masked_equal(mask_grid, 0)
    land_mask = np.ma.getmask(land_mask)

    depth_grid[land_mask] = np.NaN

    min_lon_gebco = round(spot_xx) - 5
    max_lon_gebco = round(spot_xx) + 5
    min_lat_gebco = round(spot_yy) - 5
    max_lat_gebco = round(spot_yy) + 5

    if min_lon_gebco > 180:
        min_lon_gebco = min_lon_gebco - 360
    if max_lon_gebco > 180:
        max_lon_gebco = max_lon_gebco - 360

    # Get grid data
    netcdf_gebco = netCDF4.Dataset(bathymetric_file)

    # Get all lat/lon, but just a pointer to the elevation
    lat_gebco = netcdf_gebco.variables["lat"][:]
    lon_gebco = netcdf_gebco.variables["lon"][:]

    # nonzero returns a tuple of idx per dimension
    # we're unpacking the tuple here so we can lookup max and min
    (latidx,) = np.logical_and(lat_gebco >= min_lat_gebco, lat_gebco <= max_lat_gebco).nonzero()
    (lonidx,) = np.logical_and(lon_gebco >= min_lon_gebco, lon_gebco <= max_lon_gebco).nonzero()

    assert lat_gebco.shape[0] == netcdf_gebco.variables["elevation"].shape[0], "We assume first dim is lat here"
    assert lon_gebco.shape[0] == netcdf_gebco.variables["elevation"].shape[1], "We assume 2nd dim is lon here"

    # get rid of the non used lat/lon now
    lat_gebco = lat_gebco[latidx]
    lon_gebco = lon_gebco[lonidx]

    # Get the topography data with the new indices
    depth_gebco = netcdf_gebco.variables["elevation"][latidx, lonidx]
    lon_gebco_mesh, lat_gebco_mesh = np.meshgrid(lon_gebco, lat_gebco)
    netcdf_gebco.close()

    if spot_xx >= min_lon_gebco and spot_xx <= max_lon_gebco and spot_yy >= min_lat_gebco and spot_yy <= max_lat_gebco:

        if spot_xx < 0:
            spot_yy_grid = spot_yy
            spot_xx_grid = 360 + spot_xx
        else:
            spot_yy_grid = spot_yy
            spot_xx_grid = spot_xx

        # Get nearest valid grid point

        x_min = spot_xx_grid - delta_xy_grid * buffer_multiplier_nearest_grid_point
        x_max = spot_xx_grid + delta_xy_grid * buffer_multiplier_nearest_grid_point
        y_min = spot_yy_grid - delta_xy_grid * buffer_multiplier_nearest_grid_point
        y_max = spot_yy_grid + delta_xy_grid * buffer_multiplier_nearest_grid_point

        idx_lon = (lon_grid >= x_min) & (lon_grid <= x_max)
        idx_lon = np.where(idx_lon)
        lon_subregion = lon_grid[idx_lon]

        idx_lat = (lat_grid >= y_min) & (lat_grid <= y_max)
        idx_lat = np.where(idx_lat)
        lat_subregion = lat_grid[idx_lat]

        lon_mesh_subregion, lat_mesh_subregion = np.meshgrid(lon_subregion, lat_subregion, indexing="xy")
        positions_tuple_subregion = np.rec.fromarrays([lon_mesh_subregion, lat_mesh_subregion], names="x,y")

        depth_subregion = depth_grid[np.ix_(idx_lat[0], idx_lon[0])]

        depth_subregion_vec = depth_subregion.flatten()

        xx_vec = []
        yy_vec = []
        xx_grid_final = []
        yy_grid_final = []
        depth_vec_final = []

        for x, y in np.ravel(positions_tuple_subregion):
            xx_vec.append(x)
            yy_vec.append(y)

        for m in range(len(xx_vec)):
            if not math.isnan(depth_subregion_vec[m]):
                pt_1 = np.array((spot_xx_grid, spot_yy_grid))
                pt_2 = np.array((xx_vec[m], yy_vec[m]))
                dist_spot = np.linalg.norm(pt_1 - pt_2)

                if dist_spot < delta_xy_grid * 5 and depth_subregion_vec[m] < minimum_depth:
                    xx_grid_final.append(xx_vec[m])
                    yy_grid_final.append(yy_vec[m])
                    depth_vec_final.append(depth_subregion_vec[m] * -1)

        # Convert to -180/180 orientation
        if spot_xx > 180:
            spot_xx = spot_xx - 360
        if spot_lotus_lon > 180:
            spot_lotus_lon = spot_lotus_lon - 360

        counter = 0
        for xx in xx_grid_final:
            if xx > 180:
                xx_grid_final[counter] = xx - 360
            counter += 1
        # Start Figure for position
        fig, ax = plt.subplots(1, 1, figsize=(20, 25))
        # set the aspect ratio for a local cartesian grid
        ax.set_aspect(1 / np.cos(spot_yy * np.pi / 180))

        mesh = plt.contourf(
            lon_gebco_mesh,
            lat_gebco_mesh,
            depth_gebco,
            cmap=cmap,
            vmin=-1000,
            vmax=0,
            levels=blevels,
            norm=bnorm,
            extend="both",
        )
        cb = plt.colorbar(mesh, ticks=blevels, spacing="uniform")
        cb.set_label(label="Depth [mt]", size="large", weight="bold")
        cb.ax.tick_params(labelsize="large")
        plt.contour(
            lon_gebco_mesh,
            lat_gebco_mesh,
            depth_gebco,
            levels=[minimum_depth],
            colors="k",
        )

        ax.tick_params(axis="both", which="major", labelsize=15)
        ax.tick_params(axis="both", which="minor", labelsize=15)
        # plot nearby grid points
        plt.plot(xx_grid_final, yy_grid_final, "k.", markersize=15)
        # plot spot position
        plt.plot(spot_xx, spot_yy, ".", markersize=25, color="lime")
        # plot Lotus valid grid point
        plt.plot(spot_lotus_lon, spot_lotus_lat, ".", markersize=25, color="red")

        # show depth of each point

        for m in range(len(xx_grid_final)):
            depth_text = "%.0f mt" % depth_vec_final[m]
            plt.text(
                xx_grid_final[m] - delta_xy_text,
                yy_grid_final[m] - delta_xy_text,
                depth_text,
            )

        plt.xlim(spot_xx - delta_xy_chart, spot_xx + delta_xy_chart)
        plt.ylim(spot_yy - delta_xy_chart, spot_yy + delta_xy_chart)

        plt.title(spotName + " \n Grid: " + lotus_grid, fontsize=20, weight="bold")

        plt.savefig(main_dir + "/grid_pos_" + str(spotPOI) + "_" + lotus_grid + ".png")

        plt.close()


def create_polar_chart_matrix(
    spot_xx,
    spot_yy,
    spot_lotus_lon,
    spot_lotus_lat,
    lotus_grid,
    spotPOI,
    spotName,
    bathymetric_file,
    spot_breakingWaveHeightIntercept,
    spot_breakingWaveHeightCoefficient,
    matrix,
    name_label,
):

    minimum_depth = 0

    tp_list = range(6, 25, 1)
    pdir_list = range(0, 360, 5)
    hs_input = 4
    hs_test = hs_input * 0.3048  # in ft
    # delta_xy_text = 2.5/60

    if lotus_grid[0:4] == "GLOB":
        delta_xy_chart = 1
        delta_xy_text = 3 / 60
    else:
        delta_xy_chart = 0.25
        delta_xy_text = 0.75 / 60

    if spot_xx > 180:
        spot_xx = spot_xx - 360

    min_lon_gebco = spot_xx - 1
    max_lon_gebco = spot_xx + 1
    min_lat_gebco = spot_yy - 1
    max_lat_gebco = spot_yy + 1

    if min_lon_gebco > 180:
        min_lon_gebco = min_lon_gebco - 360
    if max_lon_gebco > 180:
        max_lon_gebco = max_lon_gebco - 360

    # Get grid data
    netcdf_gebco = netCDF4.Dataset(bathymetric_file)

    # Get all lat/lon, but just a pointer to the elevation
    lat_gebco = netcdf_gebco.variables["lat"][:]
    lon_gebco = netcdf_gebco.variables["lon"][:]

    # nonzero returns a tuple of idx per dimension
    # we're unpacking the tuple here so we can lookup max and min
    (latidx,) = np.logical_and(lat_gebco >= min_lat_gebco, lat_gebco <= max_lat_gebco).nonzero()
    (lonidx,) = np.logical_and(lon_gebco >= min_lon_gebco, lon_gebco <= max_lon_gebco).nonzero()

    assert lat_gebco.shape[0] == netcdf_gebco.variables["elevation"].shape[0], "We assume first dim is lat here"
    assert lon_gebco.shape[0] == netcdf_gebco.variables["elevation"].shape[1], "We assume 2nd dim is lon here"

    # get rid of the non used lat/lon now
    lat_gebco = lat_gebco[latidx]
    lon_gebco = lon_gebco[lonidx]

    # Get the topography data with the new indices
    depth_gebco = netcdf_gebco.variables["elevation"][latidx, lonidx]
    lon_gebco_mesh, lat_gebco_mesh = np.meshgrid(lon_gebco, lat_gebco)
    netcdf_gebco.close()

    # Convert to -180/180 orientation
    if spot_xx > 180:
        spot_xx = spot_xx - 360
    if spot_lotus_lon > 180:
        spot_lotus_lon = spot_lotus_lon - 360

    # Start Figure for position
    fig, ax = plt.subplots(1, 1, figsize=(20, 25))
    # set the aspect ratio for a local cartesian grid
    ax.set_aspect(1 / np.cos(spot_yy * np.pi / 180))

    mesh = plt.contourf(
        lon_gebco_mesh,
        lat_gebco_mesh,
        depth_gebco,
        cmap=cmap,
        vmin=-1000,
        vmax=0,
        levels=blevels,
        norm=bnorm,
        extend="both",
    )
    cb = plt.colorbar(mesh, ticks=blevels, spacing="uniform")
    cb.set_label(label="Depth [mt]", size="large", weight="bold")
    cb.ax.tick_params(labelsize="large")
    plt.contour(lon_gebco_mesh, lat_gebco_mesh, depth_gebco, levels=[minimum_depth], colors="k")

    # plot spot position
    plt.plot(spot_xx, spot_yy, ".", markersize=25, color="lime")
    # plot Lotus valid grid point
    plt.plot(spot_lotus_lon, spot_lotus_lat, ".", markersize=25, color="red")

    count = 1
    for i in tp_list:
        for j in pdir_list:

            swell_partitions = [
                SwellPartition(height, period, direction, spread)
                for height, period, direction, spread in zip([hs_test, 0], [i, 0], [j, 0], [25, 0])
            ]

            minimum_frequency = 0.035
            maximum_frequency = 0.5
            direction_bin_size = 10
            valid_spectra_wave_energy = approximate_spectra_from_swell_partitions(
                swell_partitions=swell_partitions,
                minimum_frequency=minimum_frequency,
                maximum_frequency=maximum_frequency,
                direction_bin_size=direction_bin_size,
            )

            surf_height_inputs = [
                SurfHeightInput(
                    "SPECTRAL_REFRACTION",
                    frequencies=frequencies,
                    directions=directions,
                    spectra_wave_energy=valid_spectra_wave_energy,
                    spectral_refraction_matrix=matrix,
                ),
            ]

            surf_heights = SpectralRefraction().calculate(surf_height_inputs)

            hs_breaking = (
                surf_heights[0][0][1] * 3.2808399 * spot_breakingWaveHeightCoefficient
                + spot_breakingWaveHeightIntercept * 3.2808399
            )  # converted to ft

            if hs_breaking >= 1:
                vec_azimuth_xx = spot_xx + delta_xy_text * count * math.sin(math.radians(j))
                vec_azimuth_yy = spot_yy + delta_xy_text * count * math.cos(math.radians(j))

                index, quants = quantize(hs_breaking, range(1, 10, 1), range(1, 11, 1))

                break_str = "%.0f" % hs_breaking
                plt.text(
                    vec_azimuth_xx,
                    vec_azimuth_yy,
                    break_str,
                    size=12,
                    color=colors_text[index],
                    weight="bold",
                )

        # Draw 10 and 20 sec circles
        if i == 10 or i == 20:
            circle10 = plt.Circle(
                (spot_xx, spot_yy),
                delta_xy_text * count,
                color="k",
                fill=False,
                linestyle="--",
            )
            ax.add_patch(circle10)
        count += 1

    plt.xlim(spot_xx - delta_xy_chart, spot_xx + delta_xy_chart)
    plt.ylim(spot_yy - delta_xy_chart, spot_yy + delta_xy_chart)

    plt.title(
        "Refraction Matrix:"
        + spotName
        + " \n Grid: "
        + lotus_grid
        + "   |  Input conditions: "
        + str(hs_input)
        + " ft",
        fontsize=20,
        weight="bold",
    )

    plt.savefig(main_dir + "/" + name_label + "_ref_matrix_" + str(spotPOI) + "_" + lotus_grid + ".png")

    plt.close()


def create_forecast_comparasion(
    main_dir,
    current_run_date,
    delta_length_forecast,
    spot_timezone,
    spot_lotus_lon,
    spot_lotus_lat,
    lotus_grid,
    spotPOI,
    spotName,
    spot_breakingWaveHeightIntercept,
    spot_breakingWaveHeightCoefficient,
    spot_breakingWaveHeightIntercept_adjusted,
    spot_breakingWaveHeightCoefficient_adjusted,
    matrix_default,
    matrix_adjusted,
):

    date_pd = pd.to_datetime("2000-01-01")
    tz1 = timezone("UTC")
    tz2 = timezone(spot_timezone)

    nb_hours_timezone = (tz1.localize(date_pd) - tz2.localize(date_pd).astimezone(tz1)).seconds / 3600

    if not os.path.exists(main_dir + "/archive_lotus/" + lotus_grid):
        os.makedirs(main_dir + "/archive_lotus/" + lotus_grid)

    start_date = current_run_date - datetime.timedelta(days=delta_length_forecast)
    end_date = current_run_date + datetime.timedelta(days=delta_length_forecast)

    filelist = glob.glob(main_dir + "/archive_lotus/" + lotus_grid + "/ww3_lotus_" + lotus_grid + ".*.nc")
    for f in filelist:
        file_date = datetime.datetime.strptime(f[-13:-3], "%Y%m%d%H")
        if file_date < start_date:
            print("deleting " + f)
            os.remove(f)

    step_date = start_date
    while step_date <= end_date:

        check4file = Path(
            main_dir
            + "/archive_lotus/"
            + lotus_grid
            + "/ww3_lotus_"
            + lotus_grid
            + "."
            + step_date.strftime("%Y%m%d%H")
            + ".nc"
        )
        if check4file.is_file():
            pass
        else:
            print("downloading file ww3_lotus_" + lotus_grid + "." + step_date.strftime("%Y%m%d%H") + ".nc")
            os.system(
                "aws s3 cp s3://msw-lotus/archive/grids_analysis/"
                + lotus_grid
                + "/"
                + step_date.strftime("%Y%m")
                + "/ww3_lotus_"
                + lotus_grid
                + "."
                + step_date.strftime("%Y%m%d%H")
                + ".nc "
                + main_dir
                + "/archive_lotus/"
                + lotus_grid
                + "/."
            )

        step_date = step_date + datetime.timedelta(hours=3)

    min_BWH_def = []
    max_BWH_def = []
    delta_BWH_def = []
    min_BWH_adj = []
    max_BWH_adj = []
    delta_BWH_adj = []
    BWH_datelist = []

    step_date = start_date
    while step_date <= end_date:

        inputfile = Path(
            main_dir
            + "/archive_lotus/"
            + lotus_grid
            + "/ww3_lotus_"
            + lotus_grid
            + "."
            + step_date.strftime("%Y%m%d%H")
            + ".nc"
        )
        [
            overall_sigH,
            wind_speed,
            wind_dir,
            hs_part,
            tp_part,
            pdir_part,
            dspread_part,
            step_time,
        ] = get_partitions_from_grid(inputfile, spot_lotus_lon, spot_lotus_lat, npar=6)

        swell_partitions = [
            SwellPartition(height, period, direction, spread)
            for height, period, direction, spread in zip(hs_part, tp_part, pdir_part, dspread_part)
        ]

        minimum_frequency = 0.035
        maximum_frequency = 0.5
        direction_bin_size = 10
        valid_spectra_wave_energy = approximate_spectra_from_swell_partitions(
            swell_partitions=swell_partitions,
            minimum_frequency=minimum_frequency,
            maximum_frequency=maximum_frequency,
            direction_bin_size=direction_bin_size,
        )

        # Default Matrix
        surf_height_inputs = [
            SurfHeightInput(
                "SPECTRAL_REFRACTION",
                frequencies=frequencies,
                directions=directions,
                spectra_wave_energy=valid_spectra_wave_energy,
                spectral_refraction_matrix=matrix_default,
            ),
        ]

        surf_heights = SpectralRefraction().calculate(surf_height_inputs)

        min_hs_breaking = (
            surf_heights[0][0][0] * 3.2808399 * spot_breakingWaveHeightCoefficient
            + spot_breakingWaveHeightIntercept * 3.2808399
        )
        max_hs_breaking = (
            surf_heights[0][0][1] * 3.2808399 * spot_breakingWaveHeightCoefficient
            + spot_breakingWaveHeightIntercept * 3.2808399
        )

        if max_hs_breaking <= 0.5 and max_hs_breaking > 0:
            max_hs_breaking = 0.5
        else:
            max_hs_breaking = round(max_hs_breaking)

        if min_hs_breaking <= 0.5 and min_hs_breaking > 0 and max_hs_breaking > 0.5 or max_hs_breaking == 1:
            min_hs_breaking = 0.5
        else:
            min_hs_breaking = round(min_hs_breaking)
        min_BWH_def.append(min_hs_breaking)
        max_BWH_def.append(max_hs_breaking)
        delta_BWH_def.append(max_hs_breaking - min_hs_breaking)

        # Adjusted Matrix
        surf_height_inputs = [
            SurfHeightInput(
                "SPECTRAL_REFRACTION",
                frequencies=frequencies,
                directions=directions,
                spectra_wave_energy=valid_spectra_wave_energy,
                spectral_refraction_matrix=matrix_adjusted,
            ),
        ]

        surf_heights = SpectralRefraction().calculate(surf_height_inputs)

        min_hs_breaking = (
            surf_heights[0][0][0] * 3.2808399 * spot_breakingWaveHeightCoefficient_adjusted
            + spot_breakingWaveHeightIntercept_adjusted * 3.2808399
        )
        max_hs_breaking = (
            surf_heights[0][0][1] * 3.2808399 * spot_breakingWaveHeightCoefficient_adjusted
            + spot_breakingWaveHeightIntercept_adjusted * 3.2808399
        )

        if max_hs_breaking <= 0.5 and max_hs_breaking > 0:
            max_hs_breaking = 0.5
        else:
            max_hs_breaking = round(max_hs_breaking)

        if min_hs_breaking <= 0.5 and min_hs_breaking > 0 and max_hs_breaking > 0.5 or max_hs_breaking == 1:
            min_hs_breaking = 0.5
        else:
            min_hs_breaking = round(min_hs_breaking)

        min_BWH_adj.append(min_hs_breaking)
        max_BWH_adj.append(max_hs_breaking)
        delta_BWH_adj.append(max_hs_breaking - min_hs_breaking)

        timezone_adjusted_date = step_date + datetime.timedelta(hours=nb_hours_timezone)

        BWH_datelist.append(timezone_adjusted_date)

        step_date = step_date + datetime.timedelta(hours=3)

    max_ylim = max(max(max_BWH_def), max(max_BWH_adj))
    fig, (ax1, ax2) = plt.subplots(2, figsize=(30, 15))
    fig.autofmt_xdate()
    # Add x-axis and y-axis
    ax1.bar(BWH_datelist, min_BWH_def, color="#0058b0", width=0.05)
    ax1.bar(BWH_datelist, delta_BWH_def, color="#66b5fa", bottom=min_BWH_def, width=0.05)
    # Set title and labels for axes
    ax1.set(ylabel="Breaking Wave Height [ft]", title="Default Matrix")
    # Define the date format
    # date_form = DateFormatter("%m-%d")
    # ax1.xaxis.set_major_formatter(date_form)
    # Ensure a major tick for each week using (interval=1)
    # ax1.xaxis.set_major_locator(mdates.DayLocator(interval=1))
    ax1.set_xlim([BWH_datelist[0], BWH_datelist[-1]])
    ax1.set_ylim([0, max_ylim])

    ax2.bar(BWH_datelist, min_BWH_adj, color="#0058b0", width=0.05)
    ax2.bar(BWH_datelist, delta_BWH_adj, color="#66b5fa", bottom=min_BWH_adj, width=0.05)
    ax2.set(xlabel="Date", ylabel="Breaking Wave Height [ft]", title="Adjusted Matrix")
    # Define the date format
    date_form = DateFormatter("%m-%d")
    ax2.xaxis.set_major_formatter(date_form)
    # Ensure a major tick for each week using (interval=1)
    ax2.xaxis.set_major_locator(mdates.DayLocator(interval=1))
    ax2.set_xlim([BWH_datelist[0], BWH_datelist[-1]])
    ax2.set_ylim([0, max_ylim])

    fig.suptitle(
        "Breaking Wave Height:" + spotName + " \n Grid: " + lotus_grid,
        fontsize=16,
        weight="bold",
    )

    plt.savefig(main_dir + "/BWH_" + str(spotPOI) + "_" + lotus_grid + ".png")


def create_forecast_BWH_def_matrix(
    main_dir,
    current_run_date,
    delta_length_forecast,
    spot_timezone,
    spot_lotus_lon,
    spot_lotus_lat,
    lotus_grid,
    spotPOI,
    spotName,
    spot_breakingWaveHeightIntercept,
    spot_breakingWaveHeightCoefficient,
    matrix_default,
):

    date_pd = pd.to_datetime("2000-01-01")
    tz1 = timezone("UTC")
    tz2 = timezone(spot_timezone)

    nb_hours_timezone = (tz1.localize(date_pd) - tz2.localize(date_pd).astimezone(tz1)).seconds / 3600

    if not os.path.exists(main_dir + "/archive_lotus/" + lotus_grid):
        os.makedirs(main_dir + "/archive_lotus/" + lotus_grid)

    start_date = current_run_date
    end_date = current_run_date + datetime.timedelta(days=delta_length_forecast)

    filelist = glob.glob(main_dir + "/archive_lotus/" + lotus_grid + "/ww3_lotus_" + lotus_grid + ".*.nc")
    for f in filelist:
        file_date = datetime.datetime.strptime(f[-13:-3], "%Y%m%d%H")
        if file_date < start_date:
            print("deleting " + f)
            os.remove(f)

    step_date = start_date
    while step_date <= end_date:

        check4file = Path(
            main_dir
            + "/archive_lotus/"
            + lotus_grid
            + "/ww3_lotus_"
            + lotus_grid
            + "."
            + step_date.strftime("%Y%m%d%H")
            + ".nc"
        )
        if check4file.is_file():
            pass
        else:
            print("downloading file ww3_lotus_" + lotus_grid + "." + step_date.strftime("%Y%m%d%H") + ".nc")
            os.system(
                "aws s3 cp s3://msw-lotus/archive/grids_analysis/"
                + lotus_grid
                + "/"
                + step_date.strftime("%Y%m")
                + "/ww3_lotus_"
                + lotus_grid
                + "."
                + step_date.strftime("%Y%m%d%H")
                + ".nc "
                + main_dir
                + "/archive_lotus/"
                + lotus_grid
                + "/."
            )

        step_date = step_date + datetime.timedelta(hours=3)

    min_BWH_def = []
    max_BWH_def = []
    delta_BWH_def = []
    BWH_datelist = []

    step_date = start_date
    while step_date <= end_date:

        inputfile = Path(
            main_dir
            + "/archive_lotus/"
            + lotus_grid
            + "/ww3_lotus_"
            + lotus_grid
            + "."
            + step_date.strftime("%Y%m%d%H")
            + ".nc"
        )
        [
            overall_sigH,
            wind_speed,
            wind_dir,
            hs_part,
            tp_part,
            pdir_part,
            dspread_part,
            step_time,
        ] = get_partitions_from_grid(inputfile, spot_lotus_lon, spot_lotus_lat, npar=6)

        swell_partitions = [
            SwellPartition(height, period, direction, spread)
            for height, period, direction, spread in zip(hs_part, tp_part, pdir_part, dspread_part)
        ]

        minimum_frequency = 0.035
        maximum_frequency = 0.5
        direction_bin_size = 10
        valid_spectra_wave_energy = approximate_spectra_from_swell_partitions(
            swell_partitions=swell_partitions,
            minimum_frequency=minimum_frequency,
            maximum_frequency=maximum_frequency,
            direction_bin_size=direction_bin_size,
        )

        # Default Matrix
        surf_height_inputs = [
            SurfHeightInput(
                "SPECTRAL_REFRACTION",
                frequencies=frequencies,
                directions=directions,
                spectra_wave_energy=valid_spectra_wave_energy,
                spectral_refraction_matrix=matrix_default,
            ),
        ]

        surf_heights = SpectralRefraction().calculate(surf_height_inputs)

        min_hs_breaking = (
            surf_heights[0][0][0] * 3.2808399 * spot_breakingWaveHeightCoefficient
            + spot_breakingWaveHeightIntercept * 3.2808399
        )
        max_hs_breaking = (
            surf_heights[0][0][1] * 3.2808399 * spot_breakingWaveHeightCoefficient
            + spot_breakingWaveHeightIntercept * 3.2808399
        )

        if max_hs_breaking <= 0.5 and max_hs_breaking > 0:
            max_hs_breaking = 0.5
        else:
            max_hs_breaking = round(max_hs_breaking)

        if min_hs_breaking <= 0.5 and min_hs_breaking > 0 and max_hs_breaking > 0.5 or max_hs_breaking == 1:
            min_hs_breaking = 0.5
        else:
            min_hs_breaking = round(min_hs_breaking)
        min_BWH_def.append(min_hs_breaking)
        max_BWH_def.append(max_hs_breaking)
        delta_BWH_def.append(max_hs_breaking - min_hs_breaking)

        timezone_adjusted_date = step_date + datetime.timedelta(hours=nb_hours_timezone)

        BWH_datelist.append(timezone_adjusted_date)

        step_date = step_date + datetime.timedelta(hours=3)

    max_ylim = max(max_BWH_def)
    fig, ax = plt.subplots(1, 1, figsize=(20, 10))
    fig.autofmt_xdate()
    # Add x-axis and y-axis
    ax.bar(BWH_datelist, min_BWH_def, color="#0058b0", width=0.05)
    ax.bar(BWH_datelist, delta_BWH_def, color="#66b5fa", bottom=min_BWH_def, width=0.05)

    ax.set(xlabel="Date", ylabel="Breaking Wave Height [ft]", title="Default Matrix")
    # Define the date format
    date_form = DateFormatter("%m-%d")
    ax.xaxis.set_major_formatter(date_form)
    # Ensure a major tick for each week using (interval=1)
    ax.xaxis.set_major_locator(mdates.DayLocator(interval=1))
    ax.set_xlim([BWH_datelist[0], BWH_datelist[-1]])
    ax.set_ylim([0, max_ylim])

    fig.suptitle(
        "Breaking Wave Height:" + spotName + " \n Grid: " + lotus_grid,
        fontsize=16,
        weight="bold",
    )

    plt.savefig(main_dir + "/BWH_" + str(spotPOI) + "_" + lotus_grid + ".png")
