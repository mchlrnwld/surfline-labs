#
# This is a script to query all spot data using pymongo
# For examples on how to query and filter, see:
# https://docs.mongodb.com/manual/reference/sql-comparison/
# https://pymongo.readthedocs.io/en/stable/tutorial.html
# Requirements:
# prod mongo dB access (username + password)
# Python 3.6+
# whitelisted ip

import sys
import json
import os
from typing import Optional
from bson.json_util import dumps
from pymongo import MongoClient, errors
import pandas as pd
from datetime import datetime
import argparse
from pathlib import Path
import six
from typing import TypeVar, Tuple, List, Dict, Union


def _construct_key(previous_key: str, separator: str, new_key: str, replace_separators: str = None) -> str:
    """
    Returns the new_key if no previous key exists, otherwise concatenates
    previous key, separator, and new_key
    :param previous_key:
    :param separator:
    :param new_key:
    :param replace_separators: Replace separators within keys
    :return: a string if previous_key exists and simply passes through the
    new_key otherwise
    """
    if replace_separators is not None:
        new_key = str(new_key).replace(separator, replace_separators)
    if previous_key:
        return u"{}{}{}".format(previous_key, separator, new_key)
    else:
        return new_key


def flatten(
    nested_dict: dict, separator: str = "_", root_keys_to_ignore: list = None, replace_separators: bool = None
) -> dict:
    """
    Flattens a dictionary with nested structure to a dictionary with no
    hierarchy
    Consider ignoring keys that you are not interested in to prevent
    unnecessary processing
    This is specially true for very deep objects

    :param nested_dict: dictionary we want to flatten
    :param separator: string to separate dictionary keys by
    :param root_keys_to_ignore: set of root keys to ignore from flattening
    :param str replace_separators: Replace separators within keys
    :return: flattened dictionary
    """
    assert isinstance(nested_dict, dict), "flatten requires a dictionary input"
    assert isinstance(separator, six.string_types), "separator must be string"

    if root_keys_to_ignore is None:
        root_keys_to_ignore = set()

    if len(nested_dict) == 0:
        return {}

    # This global dictionary stores the flattened keys and values and is
    # ultimately returned
    flattened_dict = dict()

    def _flatten(object_, key):
        """
        For dict, list and set objects_ calls itself on the elements and for
        other types assigns the object_ to
        the corresponding key in the global flattened_dict
        :param object_: object to flatten
        :param key: carries the concatenated key for the object_
        :return: None
        """
        # Empty object can't be iterated, take as is
        if not object_:
            flattened_dict[key] = object_
        # These object types support iteration
        elif isinstance(object_, dict):
            for object_key in object_:
                if not (not key and object_key in root_keys_to_ignore):
                    _flatten(
                        object_[object_key],
                        _construct_key(key, separator, object_key, replace_separators=replace_separators),
                    )
        elif isinstance(object_, (list, set, tuple)):
            for index, item in enumerate(object_):
                _flatten(item, _construct_key(key, separator, index, replace_separators=replace_separators))
        # Anything left take as is
        else:
            flattened_dict[key] = object_

    _flatten(nested_dict, None)
    return flattened_dict


def initialize_collection(username: str, pwd: str, collection: str):
    """
    Get collection object with user credentials
    :param username: prod mongodb username
    :param pwd: prod mongodb password
    :returns: client and collection object
    """
    # !!! using the 'srv' uri also requires dnspython to be installed
    uri = "mongodb+srv://" + username + ":" + pwd + "@prod-kbyg-mongodb-atlas.aj1dk.mongodb.net/?authSource=admin"
    client = MongoClient(uri, connect=False)
    database = client["KBYG"]
    collection = database[collection]
    return client, collection


def query_collection(
    collection,
    query: dict,
    projection: Optional[dict] = None,
    iterative: Optional[bool] = True,
) -> list:
    """
    Get filtered iterable cursor object
    :param collection: initialized collection
    :param query: dictionary with all queries/filters
    :param projection: dictionary with all projections/selected fields
    :param iterative: if false, query all sessions at once, which could lead to high disk I/O
    :returns: list of all sessions as dicts
    """
    cursor = collection.find(query, projection=projection)
    if iterative:
        docs = []
        for c in cursor:
            docs.append(c)
    else:
        docs = list(cursor)
    return docs


def savejson(docs: list, outfile: str) -> None:
    """
    Save list of spot dictionaries as json file
    :param docs: list of spot dictionaries
    :param outfile: full file path (including .json) where to save the file
    :returns:
    """
    with open(outfile, "w") as file:
        file.write(dumps(docs))


def run_allspots(path: Path) -> None:
    """
    Downloads ALL spot data from mongo DB
    :param path: local output folder
    """

    # create output folder
    os.makedirs(path, exist_ok=True)

    # use your mongodb credentials to connect to the KBYG database
    client, collection = initialize_collection(
        os.environ["MONGO_USER"], os.environ["MONGO_PWD_PROD"], collection="Spots"
    )

    # get a list of ALL spots
    try:
        spotlist = query_collection(collection, query={}, iterative=False)
        print(len(spotlist))
    except errors.ServerSelectionTimeoutError:
        print(datetime.now(), "FAIL")
        client.close()
        sys.exit(1)

    # save the entire list in one json file
    savejson(spotlist, path + "/spots.json")

    # create separate json files for each spot
    os.makedirs(path + "/individual_spots/", exist_ok=True)
    for spot in spotlist:
        spotid = str(spot["_id"])
        savejson(
            spot,
            path + "/individual_spots/" + spotid + ".json",
        )

    # flatten each spot dictionary and save everything as one csv file
    spotdf = pd.DataFrame([flatten(spot, ".") for spot in spotlist])
    spotdf.to_csv(path + "/spots.csv", index=False)

    client.close()


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Download ALL spot info from mongodb")
    # path is requried
    parser.add_argument("path", type=str)
    args = parser.parse_args()

    # download everything
    run_allspots(args.path)
