import pandas as pd
import boto3
import botocore
import requests
import numpy as np
from typing import TypeVar


def download_specific_surfreport(spotid: str, botoprofile: str, outfile: str) -> None:
    """
    Get all reports for a spot
    :param sessionid: 24 char string spotID
    :param botoprofile: profile string in your aws config
    :param outfile: full file path (including .parquet) where to download the file
    :returns: None
    """
    session = boto3.Session(profile_name=botoprofile)
    s3 = session.resource("s3")
    bucket = s3.Bucket("wt-data-lake-prod")
    pull_specific_surfreport(bucket, spotid, outfile)


def pull_specific_surfreport(bucket, spotid: str, outfile: str) -> None:
    """
    Try downloading events, raise error if file doesn't exist
    :param bucket: s3 bucket object
    :param spotid: 24 char string spotID
    :param outfile: full file path (including .parquet) where to download the file
    :returns: None
    """
    try:
        bucket.download_file("science/historical_human_surf_reports/" + spotid + "/data.parquet", outfile)
    except botocore.exceptions.ClientError as e:
        if e.response["Error"]["Code"] == "404":
            # print('The object does not exist')
            pass
        else:
            print("Something else has gone wrong")
            raise


def download_specific_hindcast(spotid: str, botoprofile: str, outfile: str) -> None:
    """
    Get all hindcasts for a spot
    :param sessionid: 24 char string spotID
    :param botoprofile: profile string in your aws config
    :param outfile: full file path where to download the file
    :returns: None
    """
    session = boto3.Session(profile_name=botoprofile)
    s3 = session.resource("s3")
    bucket = s3.Bucket("wt-lotus-hindcast-dev")
    pull_specific_hindcast(bucket, spotid, outfile)


def pull_specific_hindcast(bucket, spotid: str, outfile: str) -> None:
    """
    Try downloading events, raise error if file doesn't exist
    :param bucket: s3 bucket object
    :param spotid: 24 char string spotID
    :param outfile: full file path where to download the file
    :returns: None
    """
    try:
        bucket.download_file("hindcast/lotus_bwh_" + spotid + ".csv", outfile)
    except botocore.exceptions.ClientError as e:
        if e.response["Error"]["Code"] == "404":
            print("404 - Does the hindcast exist on S3 for spot {}?".format(spotid))
            pass
        else:
            print("Something else has gone wrong")
            raise


def convert_to_csv(infile: str, outfile: str) -> None:
    """
    Loads parquet and saves csv
    :param infile: parquet file to load
    :param outfile: csv file to save
    :returns: None
    """
    assert infile.endswith(".parquet"), "Expecting parquet file as input"
    assert outfile.endswith(".csv"), "Expecting csv file as output"

    df = pd.read_parquet(infile)
    df.to_csv(outfile, index=False)


def pull_sds_data(poi_id: str, graphurl: str) -> dict:
    """
    Used to pull forecast data from the science data service using the graphql
    query.
    Args:
        poi_id: point of interest id for the spot to pull forecast data for
    Returns:
        the pulled sds data in dict form
    """
    query = f"""
    query {{
        pointOfInterest(pointOfInterestId: "{poi_id}") {{
            pointOfInterestId
            name
            lat
            lon
            surfSpotConfiguration {{
                offshoreDirection
                optimalSwellDirection
                spectralRefractionMatrix
            }}
            spots {{
                __typename
                ...on SLSpot {{
                    _id
                    name
                    timezone
                }}
            }}
        }}
    }}
    """
    resp = requests.post(graphurl, json={"query": query})

    if resp.status_code != 200:
        raise Exception(f"Error pulling SDS data ({resp.status_code})")

    if resp:
        return resp.json()

    else:
        return None


def extract_refraction_matrix(sds_data: dict) -> np.array:
    """
    Get refraction matrix from SDS data dictionary
    Args:
        sds_data: dictionary containing all SDS data from one spot
    Returns:
        refraction matrix of the spot
    """
    def_ref_matrix = sds_data["data"]["pointOfInterest"]["surfSpotConfiguration"]["spectralRefractionMatrix"]
    assert def_ref_matrix != "null", "refraction matrix is null"
    def_ref_matrix = np.array(def_ref_matrix)
    assert np.shape(def_ref_matrix) == (24, 18), "refraction matrix is not the correct shape"
    return def_ref_matrix
