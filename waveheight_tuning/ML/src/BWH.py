import numpy as np
import pandas as pd
import os
from spectral_refraction_aux_files.approximate_spectra_from_swell_partitions import (
    approximate_spectra_from_swell_partitions,
)
from spectral_refraction_aux_files.lotus_directions import directions
from spectral_refraction_aux_files.lotus_frequencies import frequencies
from spectral_refraction_aux_files.swell_partition import SwellPartition
from spectral_refraction_aux_files.frequency_calculations import (
    create_discrete_frequency_steps,
    create_frequency_distribution,
)

import torch
from tqdm import tqdm
from scipy.interpolate import RectBivariateSpline
from pull_data import (
    pull_sds_data,
    extract_refraction_matrix,
    download_specific_hindcast,
    download_specific_surfreport,
    convert_to_csv,
)

from getspotinfo import Spot
from process_data import generate_data

from torch import Tensor
from typing import TypeVar, Tuple, List, Dict, Union


def loss(y_predicted: Tensor, y_target: Tensor) -> Tensor:
    """
    Computes mean squared error between predicted and target values
    """
    return ((y_predicted - y_target) ** 2).sum()


def maeloss(y_predicted: Tensor, y_target: Tensor) -> Tensor:
    """
    Computes mean average error between predicted and target values
    """
    return (y_predicted - y_target).abs().mean()


def unravel_index(index: Union[List[int], int], shape: Tuple) -> Tuple:
    """
    Convert a flattened index into a tuple of indices, representing a position in a multi-dimensional array.
    """
    out = []
    for dim in reversed(shape):
        out.append(index % dim)
        index = index // dim
    return tuple(reversed(out))


def _sort_spectra(spectra: Tensor, current_sorted_spectra: Tensor) -> Tensor:
    """
    Sort spectra by grouping adjacent bins together, see https://en.wikipedia.org/wiki/Cumulative_sum
    """
    spectra_row_length = len(spectra)
    spectra_column_length = len(spectra[0])

    # First directional pass
    # First compare end directions
    for j in range(spectra_column_length):
        if spectra[1][j] >= spectra[spectra_row_length - 1][j]:
            current_sorted_spectra[1][j] = (
                current_sorted_spectra[1][j] + current_sorted_spectra[spectra_row_length - 1][j]
            )
            current_sorted_spectra[spectra_row_length - 1][j] = 0

    for i in range(1, spectra_row_length):
        for j in range(spectra_column_length):
            if spectra[i][j] >= spectra[i - 1][j]:
                current_sorted_spectra[i][j] = current_sorted_spectra[i][j] + current_sorted_spectra[i - 1][j]
                current_sorted_spectra[i - 1][j] = 0

    # Second second directional pass
    # First compare end directions
    for j in range(spectra_column_length):
        if spectra[spectra_row_length - 1][j] >= spectra[1][j]:
            current_sorted_spectra[spectra_row_length - 1][j] = (
                current_sorted_spectra[spectra_row_length - 1][j] + current_sorted_spectra[1][j]
            )
            current_sorted_spectra[0][j] = 0

    for i in range(spectra_row_length - 2, 0, -1):
        for j in range(spectra_column_length):
            if spectra[i][j] >= spectra[i + 1][j]:
                current_sorted_spectra[i][j] = current_sorted_spectra[i][j] + current_sorted_spectra[i + 1][j]
                current_sorted_spectra[i + 1][j] = 0

    # First frequency pass
    for j in range(1, spectra_column_length):
        for i in range(spectra_row_length):
            if spectra[i][j] >= spectra[i][j - 1]:
                current_sorted_spectra[i][j] = current_sorted_spectra[i][j] + current_sorted_spectra[i][j - 1]
                current_sorted_spectra[i][j - 1] = 0

    # Second frequency pass
    for j in range(spectra_column_length - 2, 0, -1):
        for i in range(spectra_row_length):
            if spectra[i][j] >= spectra[i][j + 1]:
                current_sorted_spectra[i][j] = current_sorted_spectra[i][j] + current_sorted_spectra[i][j + 1]
                current_sorted_spectra[i][j + 1] = 0

    # First diagonal pass
    for i in range(1, spectra_row_length):
        for j in range(1, spectra_column_length):
            if spectra[i][j] >= spectra[i - 1][j - 1]:
                current_sorted_spectra[i][j] = current_sorted_spectra[i][j] + current_sorted_spectra[i - 1][j - 1]
                current_sorted_spectra[i - 1][j - 1] = 0

    # Second diagonal pass
    for i in range(spectra_row_length - 2, 0, -1):
        for j in range(spectra_column_length - 1):
            if spectra[i][j] >= spectra[i + 1][j + 1]:
                current_sorted_spectra[i][j] = current_sorted_spectra[i][j] + current_sorted_spectra[i + 1][j + 1]
                current_sorted_spectra[i + 1][j + 1] = 0

    # Third diagonal pass
    for i in range(1, spectra_row_length):
        for j in range(spectra_column_length - 1):
            if spectra[i][j] >= spectra[i - 1][j + 1]:
                current_sorted_spectra[i][j] = current_sorted_spectra[i][j] + current_sorted_spectra[i - 1][j + 1]
                current_sorted_spectra[i - 1][j + 1] = 0

    # Fourth diagonal pass
    for i in range(spectra_row_length - 2, 0, -1):
        for j in range(1, spectra_column_length):
            if spectra[i][j] >= spectra[i + 1][j - 1]:
                current_sorted_spectra[i][j] = current_sorted_spectra[i][j] + current_sorted_spectra[i + 1][j - 1]
                current_sorted_spectra[i + 1][j - 1] = 0

    return spectra, current_sorted_spectra


class SpectralRefraction:
    def __init__(self, spotid: str = None):
        """
        Main class for spectral refraction tuning
        """
        # initialize spot id
        self.id = spotid
        # get POI - otherwise we can't get the refraction matrix
        spotinfo = Spot(self.id)
        spotinfo.initialize_database(os.environ["MONGO_USER"], os.environ["MONGO_PWD_PROD"])
        spotinfo.get_spot_info()
        self.spotname = spotinfo.name
        self.spotpoi = spotinfo.poi
        spotinfo.client.close()

        # download reports
        download_specific_surfreport(
            self.id, botoprofile="surfline-prod", outfile="/opt/app/src/{}_report.parquet".format(self.id)
        )
        found_report = os.path.isfile("/opt/app/src/{}_report.parquet".format(self.id))
        if found_report:
            convert_to_csv(
                "/opt/app/src/{}_report.parquet".format(self.id), "/opt/app/src/{}_report.csv".format(self.id)
            )
        else:
            print("No surf report found for {}".format(self.id))

        # download hindcasts
        download_specific_hindcast(
            self.id, botoprofile="surfline-dev", outfile="/opt/app/src/{}_hindcast.csv".format(self.id)
        )
        found_hindcast = os.path.isfile("/opt/app/src/{}_hindcast.csv".format(self.id))
        if not found_hindcast:
            print("No hindcast found for {}".format(self.id))

        # merge data
        self.data_complete = False
        if found_report and found_hindcast:
            generate_data(
                report="/opt/app/src/{}_report.csv".format(self.id),
                lotus="/opt/app/src/{}_hindcast.csv".format(self.id),
                outfile="/opt/app/src/{}_merged_data.csv".format(self.id),
            )
            self.df = pd.read_csv("/opt/app/src/{}_merged_data.csv".format(self.id))

            # get the refraction matrix and initialize it as a tunable tensor
            if len(self.df) > 0:

                spotdata_sds = pull_sds_data(
                    self.spotpoi,
                    graphurl="https://services.surfline.com/graphql?api_key=temporary_and_insecure_wavetrak_api_key",
                )
                self.def_ref_matrix = extract_refraction_matrix(spotdata_sds)
                if self.def_ref_matrix is not None:
                    self.data_complete = True

                    def_ref_matrix_expanded = np.ones((24, 25))
                    def_ref_matrix_expanded[:24, :18] = self.def_ref_matrix
                    for i in np.arange(18, 25):
                        def_ref_matrix_expanded[:, i] = self.def_ref_matrix[:, 17]
                    self.def_ref_matrix_expanded = def_ref_matrix_expanded.T
                    self.def_ref_matrix_tuned = torch.tensor(
                        np.array(self.def_ref_matrix_expanded),
                        requires_grad=True,
                        dtype=torch.float,
                    )
                    self.def_ref_matrix_orig = torch.tensor(np.array(self.def_ref_matrix_expanded), dtype=torch.float)

        # initialize some helper tensors
        self.factors = torch.ones((25, 24), requires_grad=True, dtype=torch.float)
        self.shifts = torch.zeros((25, 24), requires_grad=True, dtype=torch.float)
        self.scale_min = torch.tensor(0.07, requires_grad=True, dtype=torch.float)
        self.scale_max = torch.tensor(0.10, requires_grad=True, dtype=torch.float)
        self.zero = torch.tensor(0, dtype=torch.float)
        self._frequencies = [
            0.0417999998,
            0.0458999984,
            0.0505000018,
            0.0555999987,
            0.0612000003,
            0.0672999993,
            0.074000001,
            0.0813999996,
            0.0895000026,
            0.0984999985,
            0.108000003,
            0.119000003,
            0.130999997,
            0.143999994,
            0.158999994,
            0.173999995,
            0.192000002,
            0.210999995,
            0.231999993,
            0.254999995,
            0.280999988,
            0.308999985,
            0.340000004,
            0.374000013,
            0.411000013,
        ]

        self.minimum_frequency = 0.035
        self.maximum_frequency = 0.5
        self.direction_bin_size = 10

        self.direction_bin_size2 = 15
        self.lola_directions = np.arange(
            self.direction_bin_size2,
            360 + self.direction_bin_size2,
            self.direction_bin_size2,
        )
        minimum_frequency2 = 0.0418
        maximum_frequency2 = 0.411
        self.lola_frequencies = create_frequency_distribution(minimum_frequency2, maximum_frequency2)

        discrete_frequency_steps = create_discrete_frequency_steps(self.lola_frequencies)
        self.discrete_frequency_steps = np.tile(discrete_frequency_steps, (24, 1)).T

        self.adjusted_directions = np.concatenate([directions[1:], np.array([360])])
        self.periods = torch.tensor(1.0 / np.array(self._frequencies))
        self.minval = torch.tensor(0.0).float()
        self.m = torch.nn.ReLU()

    def init_optimizer(self, optimizer, scheduler=None):
        """Initialize the optimizer and scheduler and load them as class instance variables."""
        self.optimizer = optimizer
        self.scheduler = scheduler

    def train_one_epoch(
        self,
        n_sort: int = 100,
        losstype: str = "mae",
        grad_clip: float = None,
        simplified: bool = False,
        onlymax: bool = False,
    ) -> float:
        """
        Train one epoch of the model.
        """

        # check if losstype is implemented
        assert losstype in ["mae", "rmse"], "loss type needs to be either mae or rmse"

        # loop through every row in the dataframe
        epoch_loss = []
        for i in tqdm(range(len(self.df))):
            df_sample = self.df.iloc[i]

            # calculate swell partitions
            hs_part = [
                df_sample["LotusSigHPart0_mt"],
                df_sample["LotusSigHPart1_mt"],
                df_sample["LotusSigHPart2_mt"],
                df_sample["LotusSigHPart3_mt"],
                df_sample["LotusSigHPart4_mt"],
                df_sample["LotusSigHPart5_mt"],
            ]
            tp_part = [
                df_sample["LotusTPPart0_sec"],
                df_sample["LotusTPPart1_sec"],
                df_sample["LotusTPPart2_sec"],
                df_sample["LotusTPPart3_sec"],
                df_sample["LotusTPPart4_sec"],
                df_sample["LotusTPPart5_sec"],
            ]
            pdir_part = [
                df_sample["LotusPDirPart0_deg"],
                df_sample["LotusPDirPart1_deg"],
                df_sample["LotusPDirPart2_deg"],
                df_sample["LotusPDirPart3_deg"],
                df_sample["LotusPDirPart4_deg"],
                df_sample["LotusPDirPart5_deg"],
            ]
            dspread_part = [
                df_sample["LotusSpreadPart0_deg"],
                df_sample["LotusSpreadPart1_deg"],
                df_sample["LotusSpreadPart2_deg"],
                df_sample["LotusSpreadPart3_deg"],
                df_sample["LotusSpreadPart4_deg"],
                df_sample["LotusSpreadPart5_deg"],
            ]
            swell_partitions = [
                SwellPartition(height, period, direction, spread)
                for height, period, direction, spread in zip(hs_part, tp_part, pdir_part, dspread_part)
            ]
            # ground truth wave heights
            min_true = df_sample["surf_minimum_ft"]
            max_true = df_sample["surf_maximum_ft"]
            # calculate wave heights from current refraction matrix
            surf_min, surf_max, _ = self.calculate_minmax_from_swell(
                swell_partitions, n_sort=n_sort, simplified=simplified
            )
            # calculate loss between ground truth and calculated wave heights
            min_lossmae = maeloss(surf_min, min_true)
            max_lossmae = maeloss(surf_max, max_true)
            min_loss = loss(surf_min, min_true)
            max_loss = loss(surf_max, max_true)
            if onlymax == False:
                if losstype == "mae":
                    current_loss = (min_lossmae + max_lossmae) / 2
                elif losstype == "rmse":
                    current_loss = (min_loss + max_loss) / 2
            else:
                if losstype == "mae":
                    current_loss = max_lossmae
                elif losstype == "rmse":
                    current_loss = max_loss
            # clear gradients for next train
            self.optimizer.zero_grad()
            # Compute the gradient of the loss with respect to A and b.
            current_loss.backward()
            # clip gradients
            if grad_clip:
                torch.nn.utils.clip_grad_norm_([self.def_ref_matrix_tuned], max_norm=grad_clip)
            # gradient step
            self.optimizer.step()
            # scheduler step
            if self.scheduler:
                self.scheduler.step()

            epoch_loss.append(current_loss.detach().numpy().item())

        return np.mean(epoch_loss)

    def validate(self, n_sort: int = 100, simplified: bool = False) -> Tuple:
        """Validate model"""
        surf_mins = []
        surf_maxs = []

        surf_mins_true = []
        surf_maxs_true = []

        sorted_spectras = []

        with torch.no_grad():

            for i in tqdm(range(len(self.df))):
                df_sample = self.df.iloc[i]

                hs_part = [
                    df_sample["LotusSigHPart0_mt"],
                    df_sample["LotusSigHPart1_mt"],
                    df_sample["LotusSigHPart2_mt"],
                    df_sample["LotusSigHPart3_mt"],
                    df_sample["LotusSigHPart4_mt"],
                    df_sample["LotusSigHPart5_mt"],
                ]
                tp_part = [
                    df_sample["LotusTPPart0_sec"],
                    df_sample["LotusTPPart1_sec"],
                    df_sample["LotusTPPart2_sec"],
                    df_sample["LotusTPPart3_sec"],
                    df_sample["LotusTPPart4_sec"],
                    df_sample["LotusTPPart5_sec"],
                ]
                pdir_part = [
                    df_sample["LotusPDirPart0_deg"],
                    df_sample["LotusPDirPart1_deg"],
                    df_sample["LotusPDirPart2_deg"],
                    df_sample["LotusPDirPart3_deg"],
                    df_sample["LotusPDirPart4_deg"],
                    df_sample["LotusPDirPart5_deg"],
                ]
                dspread_part = [
                    df_sample["LotusSpreadPart0_deg"],
                    df_sample["LotusSpreadPart1_deg"],
                    df_sample["LotusSpreadPart2_deg"],
                    df_sample["LotusSpreadPart3_deg"],
                    df_sample["LotusSpreadPart4_deg"],
                    df_sample["LotusSpreadPart5_deg"],
                ]
                swell_partitions = [
                    SwellPartition(height, period, direction, spread)
                    for height, period, direction, spread in zip(hs_part, tp_part, pdir_part, dspread_part)
                ]

                min_true = df_sample["surf_minimum_ft"]
                max_true = df_sample["surf_maximum_ft"]

                surf_min, surf_max, s = self.calculate_minmax_from_swell(
                    swell_partitions, n_sort=n_sort, simplified=simplified
                )

                surf_mins.append(surf_min.detach().numpy())
                surf_maxs.append(surf_max.detach().numpy())

                sorted_spectras.append(s.detach().numpy())

                surf_mins_true.append(min_true)
                surf_maxs_true.append(max_true)

            return (
                np.array(surf_mins),
                np.array(surf_maxs),
                np.array(surf_mins_true),
                np.array(surf_maxs_true),
                np.array(sorted_spectras),
            )

    def calculate_threeswells_from_swell(self, swell_partitions: list, n_sort: int = 100):
        "calculate three biggest swells from a list of swell partitions"
        valid_spectra_wave_energy = self._calculate_valid_spectra_wave_energy(swell_partitions)
        lola_spectra = self._calculate_lola_spectra(valid_spectra_wave_energy)
        threeswells = self._calculate_threebiggestswells(lola_spectra, n_sort=n_sort)

        return threeswells

    def calculate_lolaspectra_from_swell(self, swell_partitions: list):
        "calculate lola spectra from a list of swell partitions"
        valid_spectra_wave_energy = self._calculate_valid_spectra_wave_energy(swell_partitions)
        lola_spectra = self._calculate_lola_spectra(valid_spectra_wave_energy)

        return lola_spectra

    def calculate_minmax_from_swell(self, swell_partitions: list, n_sort: int = 100, simplified: bool = False):
        "calculate min and max surf height from a list of swell partitions"
        valid_spectra_wave_energy = self._calculate_valid_spectra_wave_energy(swell_partitions)
        lola_spectra = self._calculate_lola_spectra(valid_spectra_wave_energy)
        surf_min, surf_max, s = self._transform_spectra(lola_spectra, n_sort=n_sort, simplified=simplified)

        return surf_min, surf_max, s

    def _calculate_valid_spectra_wave_energy(self, swell_partitions: list) -> np.array:
        "calculate spectra from a list of swell partitions"
        valid_spectra_wave_energy = approximate_spectra_from_swell_partitions(
            swell_partitions=swell_partitions,
            minimum_frequency=self.minimum_frequency,
            maximum_frequency=self.maximum_frequency,
            direction_bin_size=self.direction_bin_size,
        )

        return valid_spectra_wave_energy

    def _calculate_lola_spectra(self, valid_spectra_wave_energy: np.array) -> np.array:
        "calculate lola spectra from lotus spectra"
        adjusted_spectra_wave_energy = np.concatenate(
            (
                valid_spectra_wave_energy[:, 1:],
                valid_spectra_wave_energy[:, 0][:, None],
            ),
            axis=1,
        )
        spline_interpolation = RectBivariateSpline(frequencies, self.adjusted_directions, adjusted_spectra_wave_energy)
        lola_spectra = spline_interpolation(self.lola_frequencies, self.lola_directions)
        lola_spectra[lola_spectra < 0] = 0

        return lola_spectra

    def _transform_spectra(self, lola_spectra: np.array, n_sort: int = 100, simplified: bool = False):
        """
        This is the actual BWH algorithm: Takes lola spectra and returns the min and max waveheight
        """
        # initialize spectra
        discrete_frequency_steps = torch.tensor(np.array(self.discrete_frequency_steps), dtype=torch.float)
        direction_bin_size2 = torch.tensor(np.array(self.direction_bin_size2), dtype=torch.float)
        lola_spectra = torch.tensor(np.array(lola_spectra), dtype=torch.float)

        # make sure ref matrix stays 0 where it was 0
        self.def_ref_matrix_tuned_corrected = torch.where(
            self.def_ref_matrix_orig == 0, self.zero, self.def_ref_matrix_tuned
        )

        # THIS IS WHERE THE MATRIX COMES IN
        transformed_spectra = (
            discrete_frequency_steps
            * (direction_bin_size2 * np.pi / 180)
            * (lola_spectra * self.m(self.def_ref_matrix_tuned_corrected))
        )

        # sort spectra
        previous_spectra, current_sorted_spectra = _sort_spectra(transformed_spectra.clone(), transformed_spectra)
        for iloop in range(n_sort):
            previous_spectra, current_sorted_spectra = _sort_spectra(
                current_sorted_spectra.clone(), current_sorted_spectra
            )
            if torch.allclose(previous_spectra, current_sorted_spectra, atol=1e-4, equal_nan=True):
                finished_sorted_spectra = self.m(current_sorted_spectra.float())
                break
            if iloop == n_sort - 1:
                finished_sorted_spectra = self.m(current_sorted_spectra.float())
                break

        # calculate min and max
        s = 3.28 * 4 * torch.sqrt(finished_sorted_spectra)
        overall_swell_height = 3.28 * 4 * torch.sqrt(torch.sum(finished_sorted_spectra))

        if torch.isnan(overall_swell_height):
            print(torch.sum(finished_sorted_spectra))
            raise ValueError("Output is nan")

        ind1d = torch.argsort(s.ravel())

        ind = unravel_index(ind1d, s.shape)

        swell1_height = s[ind[0][-1], ind[1][-1]]
        swell2_height = s[ind[0][-2], ind[1][-2]]
        swell3_height = s[ind[0][-3], ind[1][-3]]

        swell1_period = self.periods[ind[0][-1]]
        swell2_period = self.periods[ind[0][-2]]
        swell3_period = self.periods[ind[0][-3]]

        surf_min = torch.sqrt(
            (swell1_height * torch.sqrt(swell1_period * self.m(self.scale_min))) ** 2
            + (swell2_height * torch.sqrt(swell2_period * self.m(self.scale_min))) ** 2
            + (swell3_height * torch.sqrt(swell3_period * self.m(self.scale_min))) ** 2
        )

        surf_max = torch.sqrt(
            (swell1_height * torch.sqrt(swell1_period * self.m(self.scale_max))) ** 2
            + (swell2_height * torch.sqrt(swell2_period * self.m(self.scale_max))) ** 2
            + (swell3_height * torch.sqrt(swell3_period * self.m(self.scale_max))) ** 2
        )

        if torch.isnan(surf_min):
            raise ValueError("Output is nan 2")

        if simplified == False:

            surf_min = torch.max(
                torch.stack([self.minval, surf_min, swell1_height, swell2_height, swell3_height]).float()
            )
            surf_max = torch.max(
                torch.stack([self.minval, surf_max, swell1_height, swell2_height, swell3_height]).float()
            )

            surf_max = torch.where(surf_max <= 3, overall_swell_height, surf_max).float()
            surf_min = torch.where(surf_max <= 3, overall_swell_height - 1, surf_min).float()
            # if surf_max <= 3:
            #     surf_max = overall_swell_height
            #     surf_min = overall_swell_height - 1

            surf_min = torch.where(torch.round(surf_min) == torch.round(surf_max), surf_max - 1, surf_min)
            # if round(surf_min) == round(surf_max):
            #     surf_min = surf_max - 1

            surf_min = torch.where(surf_min <= 0.0, self.minval, surf_min)

        return surf_min, surf_max, s

    def _calculate_threebiggestswells(self, lola_spectra: np.array, n_sort: int = 100):
        """Calculates three biggest swells from lola spectra"""
        discrete_frequency_steps = torch.tensor(np.array(self.discrete_frequency_steps), dtype=torch.float)
        direction_bin_size2 = torch.tensor(np.array(self.direction_bin_size2), dtype=torch.float)
        lola_spectra = torch.tensor(np.array(lola_spectra), dtype=torch.float)

        transformed_spectra = (
            discrete_frequency_steps * (direction_bin_size2 * np.pi / 180) * (lola_spectra * self.def_ref_matrix_orig)
        )

        previous_spectra, current_sorted_spectra = _sort_spectra(transformed_spectra.clone(), transformed_spectra)
        for _ in range(n_sort - 1):
            previous_spectra, current_sorted_spectra = _sort_spectra(
                current_sorted_spectra.clone(), current_sorted_spectra
            )

            if torch.allclose(previous_spectra, current_sorted_spectra, atol=1e-4, equal_nan=True):
                finished_sorted_spectra = self.m(current_sorted_spectra.float())
                break

        s = 3.28 * 4 * torch.sqrt(finished_sorted_spectra)

        ind1d = torch.argsort(s.ravel())
        ind = unravel_index(ind1d, s.shape)

        swell1_height = s[ind[0][-1], ind[1][-1]]
        swell2_height = s[ind[0][-2], ind[1][-2]]
        swell3_height = s[ind[0][-3], ind[1][-3]]

        swell1_period = self.periods[ind[0][-1]]
        swell2_period = self.periods[ind[0][-2]]
        swell3_period = self.periods[ind[0][-3]]

        return torch.stack(
            [
                swell1_height,
                swell2_height,
                swell3_height,
                swell1_period,
                swell2_period,
                swell3_period,
            ]
        ).float()
