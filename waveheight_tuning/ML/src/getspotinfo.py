from typing import Optional
from pymongo import MongoClient
from bson import ObjectId
import os
import pandas as pd
import argparse
from pull_data import download_specific_hindcast, download_specific_surfreport, convert_to_csv
from process_data import generate_data


class Spot:
    """
    Spot class for getting spot info from mongodb - initialized with spot id
    """

    def __init__(self, spotid: str):
        self.spotid = spotid
        self.subregion = None
        self.database = None

    def initialize_database(self, username: str, pwd: str):
        """
        Initialize mongo db connection with username and password and set database as class variable
        :param username: prod mongodb username
        :param pwd: prod mongodb password
        """
        # !!! using the 'srv' uri also requires dnspython to be installed
        uri = "mongodb+srv://" + username + ":" + pwd + "@prod-kbyg-mongodb-atlas.aj1dk.mongodb.net/"
        self.client = MongoClient(uri)
        self.database = self.client["KBYG"]

    def _query_database(self, collectionname: str, query: dict, projection: Optional[dict] = None) -> list:
        """
        Get a single (i.e. first) document matching a query
        :param collectionname: name of collection within the database
        :param query: dictionary with all queries/filters
        :param projection: dictionary with all projections/selected fields
        :returns: one session dictionary
        """
        assert self.database != None, "database not initialized"
        collection = self.database[collectionname]
        docs = collection.find_one(query, projection=projection)
        return docs

    def get_spot_info(self):
        """
        queries spot using spotid and writes the following spot info into class variables:
        - POI
        - subregion
        - spot name
        """
        # query spot
        mongoquery = {"_id": ObjectId(self.spotid)}
        projection = {"_id": 1, "pointOfInterestId": 1, "subregion": 1, "name": 1}
        data = self._query_database("Spots", mongoquery, projection)
        # write variables
        self.poi = data["pointOfInterestId"]
        self.subregion = data["subregion"]
        self.name = data["name"]

    def get_region_info(self):
        """
        queries spot using spotid and writes the following spot info into class variables:
        - subregion name
        - region
        - region name
        - spot name
        """
        # if we didnt get spot info first, we need to run it to get subregion id
        if not self.subregion:
            # query spot just to get subregion id
            mongoquery = {"_id": ObjectId(self.spotid)}
            projection = {"subregion": 1}
            spotdata = self._query_database("Spots", mongoquery, projection)
            # write variables
            self.subregion = spotdata["subregion"]

        # query subregion
        mongoquery_subregion = {"_id": ObjectId(self.subregion)}
        projection_subregion = {"region": 1, "name": 1}
        subregiondata = self._query_database("Subregions", mongoquery_subregion, projection_subregion)
        # write variables
        self.region = subregiondata["region"]
        self.subregion_name = subregiondata["name"]

        # query region
        mongoquery_region = {"_id": ObjectId(self.region)}
        projection_region = {"name": 1}
        regiondata = self._query_database("Regions", mongoquery_region, projection_region)
        # write variables
        self.region_name = regiondata["name"]

    def check_reports(self):
        """
        checks if hindcasts and reports exist on S3
        """
        # download hindcast if it exists
        download_specific_hindcast(self.spotid, botoprofile="surfline-dev", outfile=str(self.spotid) + "_hindcast.csv")
        # check if it actually downloaded anything
        self.hindcastfound = True
        if not os.path.isfile(str(self.spotid) + "_hindcast.csv"):
            print("No hindcast found for {}".format(self.spotid))
            self.hindcastfound = False
        # download reports if they exist
        download_specific_surfreport(
            self.spotid, botoprofile="surfline-prod", outfile=str(self.spotid) + "_report.parquet"
        )
        # check if it actually downloaded anything
        self.numberofreports = 0
        if os.path.isfile(str(self.spotid) + "_report.parquet"):
            # convert parquet file to csv
            convert_to_csv(str(self.spotid) + "_report.parquet", str(self.spotid) + "_report.csv")
            print("Number of reports: {}".format(len(pd.read_csv(str(self.spotid) + "_report.csv"))))
            self.numberofreports = len(pd.read_csv(str(self.spotid) + "_report.csv"))
            self.numberofreportswhindcast = 0
            if self.hindcastfound:
                # merge reports and hindcasts
                generate_data(
                    report=str(self.spotid) + "_report.csv",
                    lotus=str(self.spotid) + "_hindcast.csv",
                    outfile=str(self.spotid) + "_merged_data.csv",
                )
                print(
                    "Number of reports within hindcasts time: {}".format(
                        len(pd.read_csv(str(self.spotid) + "_merged_data.csv"))
                    )
                )
                self.numberofreportswhindcast = len(pd.read_csv(str(self.spotid) + "_merged_data.csv"))
        else:
            print("No surf report found for {}".format(self.spotid))


def run_spotinfo(spotid: str, checkreports: bool = True):
    """
    Creates a Spot object, queries mongoDB for the spot info, writes them into class variables and optionally checks if we have hindcast and/or reports stored on S3 for this spot ID
    :param spotid: Spot ID
    :param checkreports: if True, checks if we have reports on  S3 and have many of them match the hindcast data
    """

    # initialize class with spot ID
    spot = Spot(spotid)
    # initialize mongo user
    spot.initialize_database(os.environ["MONGO_USER"], os.environ["MONGO_PWD_PROD"])
    # query spot info
    spot.get_spot_info()
    spot.get_region_info()
    # close the mongo DB client
    spot.client.close()

    # print out spot info
    print("{}, {}, {}".format(spotid, spot.poi, spot.name))
    # print("Spot Name: {}".format(spot.name))
    # print("Subregion ID: {}".format(spot.subregion))
    # print("Subregion Name: {}".format(spot.subregion_name))
    # print("Region ID: {}".format(spot.region))
    # print("Region Name: {}".format(spot.region_name))

    if checkreports:
        spot.check_reports()


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Get spot info from mongo and S3")
    # spotid is required
    parser.add_argument("spotid", type=str)
    # checkreports is optional
    parser.add_argument("--checkreports", action="store_true")
    args = parser.parse_args()

    # run everything for the spot ID
    run_spotinfo(args.spotid, args.checkreports)
