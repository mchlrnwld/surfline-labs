# ML Tuning of refraction matrices

Training for machine-learning based optimization of refraction matrices to match breaking wave heights with human reported heights.

Some scripts to get spot info from mongo DB.

# Requirements

- AWS CLI (https://docs.aws.amazon.com/cli/v1/userguide/cli-chap-install.html) installed
- Docker (https://www.docker.com/products/docker-desktop) installed
- Surfline _DEV_ and _PROD_ AWS profiles in your $HOME/.aws/ directory
- MongoDB _PROD_ credentials

# Building the docker image

1. `copy .env.sample .env` - Copies sample env file to _.env_,

2. Populate `.env` with your mongoDB credentials

3. `make build` - Creates docker container for all scripts

# Using the image to get spot info

There are several scripts to get spot info from mongoDB:

- `make download` - Downloads ALL info for ALL spots from mongoDB, saves into json files, flattens all json files and saves them as one single csv.

- `make spotpoi spotid=X` - Downloads name and POI for spot with spot id `X` from mongo DB.

- `make checkreports spotid=X` - Does the same, but additionally check if we have reports and LOTUS hindcasts for this spot on S3.

# Using the image for ML tuning of refraction matrices

- `make run spots="A B C ..." processes=X` - Runs training on list of spotid's (`A, B, C`) and specified number of processes (`X`).

  - Downloads spot POI via mongoDB, and latest refraction matrix from SDS for each spot. This refraction matrix will be the starting point of training.
  - Each spot runs on max 1 process so no need to specify more processes than spots.
  - Alternatively, you can specify a csv file instead of a list of spots and a column with spot ids to train on.
  - Examples:
    - `make run spots=5842041f4e65fad6a7708827`
    - `make run spots="5977abb3b38c2300127471ec 5842041f4e65fad6a7708827" processes=2`
    - `make run csv="spots.csv" spotcolumn=SpotID processes=4`
