import datetime
import time
import os
import glob
import numpy as np
import netCDF4
import ftplib
from pathlib import Path


def get_sat_files(current_run_date, working_dir):
    """
    Main routine to control the satellite input data download process and creating the input files.
    Data is downloaded from Copernicus ftp.

    Information about the data at:

    http://marine.copernicus.eu/new-satellite-based-wave-product-released/

    Mechanism:
        - Evaluate what files are within the limits we are looking for
        - Download individual netcdf files
        - Export data from netcdf files
        - Remove data that is not within the limits or is flagged as wrong
        - Write output files

    The sat files have the following structure:

    Timestep in Epoch, lon, lat, sigH [m]


    :parameters: current_run_date, working_dir
    :return:
    :output: sat_data.dat

    Francisco
    Aus2019
    """

    os.chdir(working_dir)

    flag_new_files = 0

    username_ftp = 'fsilva'
    password_ftp = 'Oceano$1'

    low_limite_date = current_run_date - datetime.timedelta(hours=2.5)
    upper_limite_date = current_run_date + datetime.timedelta(hours=2.5)

    low_limite_date_download = current_run_date - datetime.timedelta(hours=3)
    upper_limite_date_download = current_run_date + datetime.timedelta(hours=3)

    datasets_list = ['al', 'c2', 'cfo', 'h2b', 'j3', 's3a', 's3b']

    for dataset in datasets_list:

        dir_lower_limit = 'Core/WAVE_GLO_WAV_L3_SWH_NRT_OBSERVATIONS_014_001/dataset-wav-alti-l3-swh-rt-global-' + dataset + '/' + \
            low_limite_date_download.strftime(
                '%Y') + '/' + low_limite_date_download.strftime('%m')
        dir_upper_limit = 'Core/WAVE_GLO_WAV_L3_SWH_NRT_OBSERVATIONS_014_001/dataset-wav-alti-l3-swh-rt-global-' + dataset + '/' + \
            upper_limite_date_download.strftime('%Y') + '/' + \
            upper_limite_date_download.strftime('%m')

        ftp_error = []
        if dir_lower_limit == dir_upper_limit:
            try:
                ftpcon = ftplib.FTP()
                ftpcon.connect('nrt.cmems-du.eu')
                ftpcon.login(user=username_ftp, passwd=password_ftp)
                ftpcon.cwd(dir_lower_limit)
                files = ftpcon.nlst()
                for sat_file in files:
                    alpha = sat_file.find('_' + dataset + '_')
                    file_dates_string = sat_file[alpha +
                                                 len(dataset)+2:alpha+len(dataset)+33]
                    start_file_date = datetime.datetime.strptime(
                        file_dates_string[0:14], '%Y%m%dT%H%M%S')
                    end_file_date = datetime.datetime.strptime(
                        file_dates_string[16:], '%Y%m%dT%H%M%S')
                    if start_file_date >= low_limite_date_download and end_file_date <= upper_limite_date_download:
                        # check if exists at the archive and skip the donwload in that case
                        check4file = Path(working_dir + '/' + sat_file)
                        if check4file.is_file():
                            pass
                        else:
                            # check if older file exists, remove it before downloading the new one
                            file_list2delete = glob.glob(
                                working_dir + '/' + sat_file[0:53] + '*')
                            for file2delete in file_list2delete:
                                os.remove(file2delete)
                            print('downloading file ' + sat_file)
                            ftpcon.retrbinary(
                                "RETR " + sat_file, open(sat_file, 'wb').write)
                            flag_new_files = 1
                ftpcon.quit()
            except ftplib.all_errors as e:
                ftp_error = e
        else:
            try:
                ftpcon = ftplib.FTP()
                ftpcon.connect('nrt.cmems-du.eu')
                ftpcon.login(user=username_ftp, passwd=password_ftp)
                ftpcon.cwd(dir_lower_limit)
                files = ftpcon.nlst()
                for sat_file in files:
                    alpha = sat_file.find('_' + dataset + '_')
                    file_dates_string = sat_file[alpha +
                                                 len(dataset)+2:alpha+len(dataset)+33]
                    start_file_date = datetime.datetime.strptime(
                        file_dates_string[0:14], '%Y%m%dT%H%M%S')
                    end_file_date = datetime.datetime.strptime(
                        file_dates_string[16:], '%Y%m%dT%H%M%S')
                    if start_file_date >= low_limite_date_download and end_file_date <= upper_limite_date_download:
                        check4file = Path(working_dir + '/' + sat_file)
                        if check4file.is_file():
                            pass
                        else:
                            print('downloading file ' + sat_file)
                            ftpcon.retrbinary(
                                "RETR " + sat_file, open(sat_file, 'wb').write)
                            flag_new_files = 1
                ftpcon.quit()
            except ftplib.all_errors as e:
                ftp_error = e
            try:
                ftpcon = ftplib.FTP()
                ftpcon.connect('nrt.cmems-du.eu')
                ftpcon.login(user=username_ftp, passwd=password_ftp)
                ftpcon.cwd(dir_upper_limit)
                files = ftpcon.nlst()
                for sat_file in files:
                    alpha = sat_file.find('_' + dataset + '_')
                    file_dates_string = sat_file[alpha +
                                                 len(dataset)+2:alpha+len(dataset)+33]
                    start_file_date = datetime.datetime.strptime(
                        file_dates_string[0:14], '%Y%m%dT%H%M%S')
                    end_file_date = datetime.datetime.strptime(
                        file_dates_string[16:], '%Y%m%dT%H%M%S')
                    if start_file_date >= low_limite_date_download and end_file_date <= upper_limite_date_download:
                        check4file = Path(working_dir + '/' + sat_file)
                        if check4file.is_file():
                            pass
                        else:
                            print('downloading file ' + sat_file)
                            ftpcon.retrbinary(
                                "RETR " + sat_file, open(sat_file, 'wb').write)
                            flag_new_files = 1
                ftpcon.quit()
            except ftplib.all_errors as e:
                ftp_error = e
        if ftp_error:
            print(' ')
            print('##################################################################')
            print('##################################################################')
            print(' ')
            print(ftp_error)
            print(' ')
            print('##################################################################')
            print('##################################################################')
            print(' Running The download process again')
            print(' ')

            ftp_error = []
            if dir_lower_limit == dir_upper_limit:
                try:
                    ftpcon = ftplib.FTP()
                    ftpcon.connect('nrt.cmems-du.eu')
                    ftpcon.login(user=username_ftp, passwd=password_ftp)
                    ftpcon.cwd(dir_lower_limit)
                    files = ftpcon.nlst()
                    for sat_file in files:
                        alpha = sat_file.find('_' + dataset + '_')
                        file_dates_string = sat_file[alpha +
                                                     len(dataset)+2:alpha+len(dataset)+33]
                        start_file_date = datetime.datetime.strptime(
                            file_dates_string[0:14], '%Y%m%dT%H%M%S')
                        end_file_date = datetime.datetime.strptime(
                            file_dates_string[16:], '%Y%m%dT%H%M%S')
                        if start_file_date >= low_limite_date_download and end_file_date <= upper_limite_date_download:
                            # check if exists at the archive and skip the donwload in that case
                            check4file = Path(working_dir + '/' + sat_file)
                            if check4file.is_file():
                                pass
                            else:
                                # check if older file exists, remove it before downloading the new one
                                file_list2delete = glob.glob(
                                    working_dir + '/' + sat_file[0:53] + '*')
                                for file2delete in file_list2delete:
                                    os.remove(file2delete)
                                print('downloading file ' + sat_file)
                                ftpcon.retrbinary(
                                    "RETR " + sat_file, open(sat_file, 'wb').write)
                                flag_new_files = 1
                    ftpcon.quit()
                except ftplib.all_errors as e:
                    ftp_error = e
            else:
                try:
                    ftpcon = ftplib.FTP()
                    ftpcon.connect('nrt.cmems-du.eu')
                    ftpcon.login(user=username_ftp, passwd=password_ftp)
                    ftpcon.cwd(dir_lower_limit)
                    files = ftpcon.nlst()
                    for sat_file in files:
                        alpha = sat_file.find('_' + dataset + '_')
                        file_dates_string = sat_file[alpha +
                                                     len(dataset)+2:alpha+len(dataset)+33]
                        start_file_date = datetime.datetime.strptime(
                            file_dates_string[0:14], '%Y%m%dT%H%M%S')
                        end_file_date = datetime.datetime.strptime(
                            file_dates_string[16:], '%Y%m%dT%H%M%S')
                        if start_file_date >= low_limite_date_download and end_file_date <= upper_limite_date_download:
                            check4file = Path(working_dir + '/' + sat_file)
                            if check4file.is_file():
                                pass
                            else:
                                print('downloading file ' + sat_file)
                                ftpcon.retrbinary(
                                    "RETR " + sat_file, open(sat_file, 'wb').write)
                                flag_new_files = 1
                    ftpcon.quit()
                except ftplib.all_errors as e:
                    ftp_error = e
                try:
                    ftpcon = ftplib.FTP()
                    ftpcon.connect('nrt.cmems-du.eu')
                    ftpcon.login(user=username_ftp, passwd=password_ftp)
                    ftpcon.cwd(dir_upper_limit)
                    files = ftpcon.nlst()
                    for sat_file in files:
                        alpha = sat_file.find('_' + dataset + '_')
                        file_dates_string = sat_file[alpha +
                                                     len(dataset)+2:alpha+len(dataset)+33]
                        start_file_date = datetime.datetime.strptime(
                            file_dates_string[0:14], '%Y%m%dT%H%M%S')
                        end_file_date = datetime.datetime.strptime(
                            file_dates_string[16:], '%Y%m%dT%H%M%S')
                        if start_file_date >= low_limite_date_download and end_file_date <= upper_limite_date_download:
                            check4file = Path(working_dir + '/' + sat_file)
                            if check4file.is_file():
                                pass
                            else:
                                print('downloading file ' + sat_file)
                                ftpcon.retrbinary(
                                    "RETR " + sat_file, open(sat_file, 'wb').write)
                                flag_new_files = 1
                    ftpcon.quit()
                except ftplib.all_errors as e:
                    ftp_error = e
            if ftp_error:
                print(' ')
                print('##########################################################')
                print('##########################################################')
                print(' ')
                print(ftp_error)
                print(' ')
                print('##########################################################')
                print('##########################################################')
                print(' Failled for the second time - FILE NOT DOWNLOADED')

    file_list = []
    filelist = glob.glob('global_vavh_*.nc')
    for f in filelist:
        start_file_date = datetime.datetime.strptime(
            f[-50:-35], '%Y%m%dT%H%M%S')
        end_file_date = datetime.datetime.strptime(f[-34:-19], '%Y%m%dT%H%M%S')
        if start_file_date >= low_limite_date_download and end_file_date <= upper_limite_date_download:
            file_list.append(f)

    lon = []
    lat = []
    hs = []
    hs_raw = []
    timesteps = []
    platform = []

    for file_sat in file_list:

        check4file = Path(working_dir + '/' + file_sat)

        if check4file.is_file():
            if os.stat(str(check4file)).st_size == 0:
                print(file_sat + ' is empty')
            else:
                netcdf_file_id = netCDF4.Dataset(str(check4file))
                platform_string = netcdf_file_id.getncattr('platform')
                print(file_sat)

                time_var = netcdf_file_id.variables['time'][:]
                t_unit = netcdf_file_id.variables['time'].units
                t_cal = netcdf_file_id.variables['time'].calendar
                lon_temp = []
                lat_temp = []
                hs_temp = []
                hs_raw_temp = []
                timesteps_temp = []
                platform_temp = []

                timesteps_temp[:] = netCDF4.num2date(
                    time_var, units=t_unit, calendar=t_cal, 
                    only_use_cftime_datetimes=False,
                    only_use_python_datetimes=True)
                # /1000000
                lon_temp = netcdf_file_id.variables['longitude'][:]
                # /1000000
                lat_temp = netcdf_file_id.variables['latitude'][:]
                hs_temp = netcdf_file_id.variables['VAVH'][:]  # /1000
                # /1000
                hs_raw_temp = netcdf_file_id.variables['VAVH_UNFILTERED'][:]
                if len(hs_temp) > 0:
                    pos_remove = np.nonzero(hs_temp > 100)
                    hs_temp = np.delete(hs_temp, pos_remove)
                    hs_raw_temp = np.delete(hs_raw_temp, pos_remove)
                    timesteps_temp = np.delete(timesteps_temp, pos_remove)
                    lon_temp = np.delete(lon_temp, pos_remove)
                    lat_temp = np.delete(lat_temp, pos_remove)
                if len(hs_temp) > 0:
                    pos_remove = np.nonzero(hs_temp <= 0)
                    hs_temp = np.delete(hs_temp, pos_remove)
                    hs_raw_temp = np.delete(hs_raw_temp, pos_remove)
                    timesteps_temp = np.delete(timesteps_temp, pos_remove)
                    lon_temp = np.delete(lon_temp, pos_remove)
                    lat_temp = np.delete(lat_temp, pos_remove)
                netcdf_file_id.close()

                platform_temp = np.full(len(hs_temp), 0)

                if platform_string == 'Cryosat-2':
                    platform_temp = np.full(len(hs_temp), 1)
                if platform_string == 'CFOSAT':
                    platform_temp = np.full(len(hs_temp), 2)
                if platform_string == 'HaiYang-2B':
                    platform_temp = np.full(len(hs_temp), 3)
                if platform_string == 'Jason-3':
                    platform_temp = np.full(len(hs_temp), 4)
                if platform_string == 'SARAL-Altika':
                    platform_temp = np.full(len(hs_temp), 5)
                if platform_string == 'Sentinel-3A':
                    platform_temp = np.full(len(hs_temp), 6)
                if platform_string == 'Sentinel-3B':
                    platform_temp = np.full(len(hs_temp), 7)

                lon.extend(lon_temp)
                lat.extend(lat_temp)
                hs.extend(hs_temp)
                hs_raw.extend(hs_raw_temp)
                timesteps.extend(timesteps_temp)
                platform.extend(platform_temp)

    pos_remove = []
    counter = 0
    for timestep in timesteps:
        if timestep < low_limite_date or timestep > upper_limite_date:
            pos_remove.append(counter)
        counter += 1

    timesteps = np.delete(timesteps, pos_remove)
    lon = np.delete(lon, pos_remove)
    lat = np.delete(lat, pos_remove)
    hs = np.delete(hs, pos_remove)
    platform = np.delete(platform, pos_remove)

    if len(hs) > 0:
        output_file = open(working_dir + '/sat_data_' +
                           current_run_date.strftime('%Y%m%d%H') + '.dat', 'w')
        output_file.write(
            'timestamp,longitude,latitude,sigH,sigH_raw,platform\n')
        for k in range(0, len(timesteps)):
            output_file.write("%f,%.5f,%.5f,%.1f,%.1f,%.0f\n" %(timesteps[k].timestamp(), lon[k], lat[k], hs[k], hs_raw[k], platform[k]))

        output_file.close()

    return flag_new_files


def get_lotus_data(current_run_date, working_dir):

    flag_new_files = 0

    check4file = Path(working_dir + '/ww3_lotus_GLOB_30m.' +
                      datetime.datetime.strftime(current_run_date, '%Y%m%d%H') + '_op.nc')
    if check4file.is_file():
        pass
    else:
        try:
            os.system('aws --profile msw s3 cp s3://msw-lotus/archive/grids_analysis/GLOB_30m/' + datetime.datetime.strftime(
                current_run_date, '%Y%m') + '/ww3_lotus_GLOB_30m.' + datetime.datetime.strftime(current_run_date, '%Y%m%d%H') + '.nc ' + working_dir + '/ww3_lotus_GLOB_30m.' + datetime.datetime.strftime(current_run_date, '%Y%m%d%H') + '_op.nc')
            if check4file.is_file():
                flag_new_files = 1
        except:
            print('No valid run available')

    # check if the timestamp is a exact run time or not and adjust based on that
    current_run_date_hour = int(
        datetime.datetime.strftime(current_run_date, '%H'))
    if current_run_date_hour % 6 == 0:
        assimilation_date = current_run_date + datetime.timedelta(hours=12)
    else:
        assimilation_date = current_run_date + datetime.timedelta(hours=9)

    check4file = Path(working_dir + '/ww3_lotus_GLOB_30m.' +
                      datetime.datetime.strftime(current_run_date, '%Y%m%d%H') + '_assim.nc')
    if check4file.is_file():
        pass
    else:
        try:
            os.system('aws --profile msw s3 cp s3://msw-lotus/archive/grids/GLOB_30m/' + datetime.datetime.strftime(
                assimilation_date, '%Y%m%d%H') + '/hindcast/ww3_lotus_GLOB_30m.' + datetime.datetime.strftime(current_run_date, '%Y%m%d%H') + '.nc ' + working_dir + '/ww3_lotus_GLOB_30m.' + datetime.datetime.strftime(current_run_date, '%Y%m%d%H') + '_assim.nc')
            if check4file.is_file():
                flag_new_files = 1
        except:
            print('No valid run available')
    return flag_new_files
