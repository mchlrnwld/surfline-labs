import os
import datetime
import pandas as pd
import numpy as np
import time
from pathlib import Path
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
from mpl_toolkits.basemap import shiftgrid
from matplotlib.colors import LinearSegmentedColormap
from random import randrange
from mpl_toolkits.axes_grid1 import make_axes_locatable
from scipy.interpolate import RectBivariateSpline
import pytz

msw_colors = [(0, 0, 0.8), (0, 0, 0.8), (0, 0.2, 1), (0, 0.8, 1), (0, 1, 1), (0.4, 1, 0.8), (0, 1, 0.2), (0, 0.8, 0.2), (0.6, 1, 0.4), (1, 1, 0.4), (1, 1, 0), (1, 0.6, 0), (1, 0.4, 0), (1, 0.2, 0),
              (1, 0, 0), (0.8, 0, 0), (0.6, 0, 0), (0.6, 0, 0.4), (0.6, 0, 0.4), (0.8, 0, 1), (1, 0, 1), (0.886275, 0.372549, 0.807843), (0.929412, 0.611765, 0.882353)]
cm = LinearSegmentedColormap.from_list(
    'msw_colormap', msw_colors, N=23)

assim_colors = [(0, 0, 0.2941175), (0, 0, 0.588235), (0, 0, 0.7941175), (0, 0, 1), (0, 0.147059, 1), (0, 0.294118, 1), (0, 0.4411765, 1), (0, 0.588235, 1), (0.5, 0.7941175, 1), (1, 1, 1), (1, 1, 1), (1, 1, 0.852941), (1, 1, 0.705882), (1, 0.872549, 0.352941), (1, 0.745098, 0), (1, 0.5686275, 0), (1, 0.392157, 0), (1, 0.1960785, 0), (1, 0, 0), (0.5, 0, 0.5)]
assim_cm = LinearSegmentedColormap.from_list('msw_assim_colormap', assim_colors, N=20)


colors_list = ['yellow', 'fuchsia', 'lime',
               'blue', 'red', 'darkorange', 'black']
satellite_list = ['Cryosat-2', 'CFOSAT', 'HaiYang-2B',
                  'Jason-3', 'SARAL-Altika', 'Sentinel-3A', 'Sentinel-3B']

max_lat = 70
min_lat = -70

time_buffer = 1.5

longitude_buffer = 60

sat_spatial_jump = 10  # to reduce the spot density on the chart


def create_main_index(start_website_date, end_website_date, archive_outputs_dir):

    OutFile = open(archive_outputs_dir + '/index.html', 'w')

    OutFile.write('%s\n' % '<!DOCTYPE html>')
    OutFile.write('%s\n' % '<html>')
    OutFile.write('%s\n' % '<head>')
    OutFile.write('%s\n' % '<meta charset="UTF-8" />')
    OutFile.write('%s\n' %
                  '<title>Lotus Satellite Assimilation System</title>')
    OutFile.write('%s\n' % '<link rel="stylesheet" href="css/reset.css" />')
    OutFile.write('%s\n' % '<link rel="stylesheet" href="css/style.css" />')
    OutFile.write('%s\n' % '</head>')
    OutFile.write('%s\n' % '<body>')
    OutFile.write('%s\n' % '<div class="container">')
    OutFile.write('%s\n' % '<header>')
    OutFile.write('%s\n' %
                  '<h1 align="center">Lotus Satellite Assimilation System</h1>')
    OutFile.write('%s\n' % '</header>')

    delta = end_website_date - start_website_date
    for i in range(delta.days + 1):

        step_date = end_website_date - datetime.timedelta(days=i)
        for step in range(21, -3, -3):

            daily_step_date = step_date + datetime.timedelta(hours=step)
            step_string = datetime.datetime.strftime(daily_step_date, '%Y%m%d%H')
            check4file = Path(archive_outputs_dir + '/' +
                              step_string + '/glob_grd_trk_' + step_string + '.png')

            if check4file.is_file():
                OutFile.write('<a href="' + step_string + '/glob_grd_trk_' + step_string + '.html" style="color:blue; text-decoration: underline;">' + step_string + '</a>\n<br>\n')

    OutFile.write('%s\n' % '<p>&nbsp;</p>')
    OutFile.write('%s\n' % '<p>&nbsp;</p>')
    OutFile.write('%s\n' % '<p>&nbsp;</p>')
    OutFile.write('%s\n' % '<p>&nbsp;</p>')

    # Last update strings

    # get the standard UTC time
    UTC = pytz.utc

    # get time for specific locations
    LAtime = pytz.timezone('America/Los_Angeles')
    NYtime = pytz.timezone('America/New_York')
    HWtime = pytz.timezone('US/Hawaii')
    LDtime = pytz.timezone('Europe/London')
    SItime = pytz.timezone('Australia/Sydney')

    # print the date and time in
    # specified format
    datetime_utc = datetime.datetime.now(UTC)
    OutFile.write(
        '%s\n' % '<h3 align="left">Last Update [UTC]: ' + datetime_utc.strftime('%Y %B %d   %H:%M') + '</h3>')
    datetime_hw = datetime.datetime.now(HWtime)
    OutFile.write(
        '%s\n' % '<h3 align="left">Last Update [HW]: ' + datetime_hw.strftime('%Y %B %d   %H:%M') + '</h3>')
    datetime_la = datetime.datetime.now(LAtime)
    OutFile.write(
        '%s\n' % '<h4 align="left">Last Update [LA]: ' + datetime_la.strftime('%Y %B %d   %H:%M') + '</h4>')
    datetime_ny = datetime.datetime.now(NYtime)
    OutFile.write(
        '%s\n' % '<h5 align="left">Last Update [NY]: ' + datetime_ny.strftime('%Y %B %d   %H:%M') + '</h5>')

    datetime_uk = datetime.datetime.now(LDtime)
    OutFile.write(
        '%s\n' % '<h3 align="left">Last Update [UK]: ' + datetime_uk.strftime('%Y %B %d   %H:%M') + '</h1>')
    datetime_aus = datetime.datetime.now(SItime)
    OutFile.write(
        '%s\n' % '<h3 align="left">Last Update [AUS]: ' + datetime_aus.strftime('%Y %B %d   %H:%M') + '</h1>')

    OutFile.write('%s\n' % '</section>')
    OutFile.write('%s\n' % '</div>')
    OutFile.write('%s\n' % '</body>')
    OutFile.write('%s\n' % '</html>')
    OutFile.close()


def create_glob_grd_trk_html(current_run_date, archive_outputs_dir, nb_sat_passes):

    date_string = datetime.datetime.strftime(current_run_date, '%Y%m%d%H')

    OutFile = open(archive_outputs_dir + '/glob_grd_trk_' +
                   date_string + '.html', 'w')

    OutFile.write('%s\n' % '<!DOCTYPE html>')
    OutFile.write('%s\n' % '<html>')
    OutFile.write('%s\n' % '<head>')
    OutFile.write('%s\n' % '<meta charset="UTF-8" />')
    OutFile.write('%s\n' %
                  '<title>Lotus Satellite Assimilation System</title>')
    OutFile.write('%s\n' % '<style>')
    OutFile.write('%s\n' %
                  'table {border-collapse: separate;border-spacing: 0 10px;margin-left: auto;margin-right: auto;}')
    OutFile.write(
        '%s\n' % 'td {width: 40px;text-align: center;border: 0px solid black;padding: 5px;font-size: 1.5em;white-space: nowrap;}')
    OutFile.write('%s\n' % 'img {max-width: 100%;}')
    OutFile.write('%s\n' % '</style>')
    OutFile.write('%s\n' % '</head>')
    OutFile.write('%s\n' % '<body>')
    OutFile.write('%s\n' % '<div class="container">')

    OutFile.write('%s\n' %
                  '<h1 align="center">Global Ground Tracks</h1>')
    OutFile.write('%s\n' %
                  '<h2 align="center"><a href="http://api-charts.s3.amazonaws.com/validation/lotus/index.html">Home</a></h2>')

    OutFile.write('%s\n' % '<table>')
    OutFile.write('%s\n' % '<td class="td">')
    OutFile.write('%s\n' % 'Time Steps')
    OutFile.write('%s\n' % '</td>')
    for step in range(3, -48, -3):
            prev_run_date = current_run_date + datetime.timedelta(hours=step)
            prev_date_string = datetime.datetime.strftime(prev_run_date, '%Y%m%d%H')
            display_date = datetime.datetime.strftime(prev_run_date, '%m/%d %H')
            OutFile.write('%s\n' % '<td class="td">')
            OutFile.write('%s\n' % '<a href="../' + prev_date_string +
                          '/glob_grd_trk_' + prev_date_string + '.html">' + display_date +
                          '</a>')
            OutFile.write('%s\n' % '</td>')
    OutFile.write('%s\n' % '</table>')
    OutFile.write('%s\n' % '<table>')
    OutFile.write('%s\n' % '<td class="td">')
    OutFile.write('%s\n' % 'Individual Tracks')
    OutFile.write('%s\n' % '</td>')
    for nb_sat in range(1, nb_sat_passes+1):
        OutFile.write('%s\n' % '<td class="td">')
        OutFile.write('%s\n' % '<a href="individual_pass_' + date_string +
                      '_' + str(nb_sat) + '.html">' + str(nb_sat) + '</a>')
        OutFile.write('%s\n' % '</td>')
    OutFile.write('%s\n' % '</table>')

    OutFile.write('%s\n' % '<img src="glob_grd_trk_' +
                  date_string + '.png" class="center">')

    OutFile.write('%s\n' % '<img src="assimilation_overview_' +
                  date_string + '.png" class="center">')

    OutFile.write('%s\n' %
                  '<h1 align="center">Global Statistics</h1>')
    OutFile.write('%s\n' % '<img src="global_scatter_' +
                  date_string + '.png" class="center">')

    OutFile.write('%s\n' % '</div>')
    OutFile.write('%s\n' % '</body>')
    OutFile.write('%s\n' % '</html>')
    OutFile.close()

    for nb_sat in range(1, nb_sat_passes+1):

        OutFile = open(archive_outputs_dir + '/individual_pass_' +
                       date_string + '_' + str(nb_sat) + '.html', 'w')

        OutFile.write('%s\n' % '<!DOCTYPE html>')
        OutFile.write('%s\n' % '<html>')
        OutFile.write('%s\n' % '<head>')
        OutFile.write('%s\n' % '<meta charset="UTF-8" />')
        OutFile.write('%s\n' %
                      '<title>Lotus Satellite Assimilation System</title>')
        OutFile.write('%s\n' % '<style>')
        OutFile.write('%s\n' %
                      'table {border-collapse: separate;border-spacing: 0 10px;margin-left: auto;margin-right: auto;}')
        OutFile.write(
            '%s\n' % 'td {width: 40px;text-align: center;border: 0px solid black;padding: 5px;font-size: 1.5em;white-space: nowrap;}')
        OutFile.write('%s\n' % 'img {max-width: 100%;}')
        OutFile.write('%s\n' % '</style>')
        OutFile.write('%s\n' % '</head>')
        OutFile.write('%s\n' % '<body>')
        OutFile.write('%s\n' % '<div class="container">')
        OutFile.write('%s\n' % '<table>')
        OutFile.write('%s\n' % '<td class="td">')
        OutFile.write('%s\n' % 'Time Steps')
        OutFile.write('%s\n' % '</td>')
        for step in range(-3, -48, -3):
                prev_run_date = current_run_date + datetime.timedelta(hours=step)
                prev_date_string = datetime.datetime.strftime(prev_run_date, '%Y%m%d%H')
                display_date = datetime.datetime.strftime(prev_run_date, '%m/%d %H')
                OutFile.write('%s\n' % '<td class="td">')
                OutFile.write('%s\n' % '<a href="../' + prev_date_string +
                              '/glob_grd_trk_' + prev_date_string + '.html">' + display_date +
                              '</a>')
                OutFile.write('%s\n' % '</td>')
        OutFile.write('%s\n' % '</table>')
        OutFile.write('%s\n' % '<img src="indiv_pass_' +
                      date_string + '_' + str(nb_sat) + '.png" class="center">')

        OutFile.write('%s\n' % '<img src="individual_pass_scatter_' +
                      date_string + '_' + str(nb_sat) + '.png" class="center">')

        OutFile.write('%s\n' % '</div>')
        OutFile.write('%s\n' % '</body>')
        OutFile.write('%s\n' % '</html>')
        OutFile.close()


def show_global_ground_tracks(grid_sigH_operational, grid_lon, grid_lat, sat_data_file, current_run_date, center_basin, archive_dir):

    start_date_string = current_run_date - \
        datetime.timedelta(hours=time_buffer)
    end_date_string = current_run_date + datetime.timedelta(hours=time_buffer)

    start_date_unix = time.mktime(start_date_string.utctimetuple())
    end_date_unix = time.mktime(end_date_string.utctimetuple())

    sat_full_data = pd.read_csv(sat_data_file)

    cond_time_1 = sat_full_data['timestamp'] >= start_date_unix
    cond_time_2 = sat_full_data['timestamp'] <= end_date_unix
    time_subset = sat_full_data[cond_time_1 & cond_time_2]

    if center_basin == 'Pacific':
        grid_lat_temp = grid_lat
        grid_lon_temp = grid_lon
        grid_sigH_temp = grid_sigH_operational
    else:
        grid_lat_temp = grid_lat
        grid_sigH_temp, grid_lon_temp = shiftgrid(
            180., grid_sigH_operational, grid_lon, start=False)  # shiftgrid

    fig = plt.figure(figsize=(26, 13), facecolor='white')
    ax = fig.add_axes([0.1, 0.1, 0.8, 0.8])

    # setup mercator map projection.
    if center_basin == 'Pacific':
        m = Basemap(llcrnrlon=0, llcrnrlat=min_lat, urcrnrlon=360, urcrnrlat=max_lat,
                    resolution='l', projection='merc')
    else:
        m = Basemap(llcrnrlon=-180, llcrnrlat=min_lat, urcrnrlon=180, urcrnrlat=max_lat,
                    resolution='l', projection='merc')

    x, y = m(*np.meshgrid(grid_lon_temp, grid_lat_temp))
    try:
        max_hs = np.nanmax(grid_sigH_temp)
    except IndexError:
        max_hs = np.nan
        pass
    max_hs_idx = np.where(grid_sigH_temp==max_hs)
    max_hs_lat_idx = max_hs_idx[0][0]
    max_hs_lon_idx = max_hs_idx[1][0]
    max_hs_x = x[max_hs_lat_idx, max_hs_lon_idx]
    max_hs_y = y[max_hs_lat_idx, max_hs_lon_idx]
    max_hs_lat = grid_lat_temp[max_hs_lat_idx]
    max_hs_lon = grid_lon_temp[max_hs_lon_idx]
    if max_hs_lon > 180.0:
        max_hs_lon = max_hs_lon - 360.0

    m.contourf(x, y, grid_sigH_temp, cmap=cm, vmin=0,
               vmax=50, levels=range(0, 52, 2), extend='max')

    m.drawcoastlines()
    m.fillcontinents()
    m.drawmapboundary()
    m.drawparallels(np.arange(-90., 90., 10.),
                    labels=[1, 0, 0, 0], fontsize=14)
    m.drawmeridians(np.arange(-180., 180., 20.),
                    labels=[0, 0, 0, 1], fontsize=14)
    plt.annotate('X', xy=(max_hs_x, max_hs_y), fontsize=14, weight='bold', ha='center', va='center')
    plt.annotate('Max Hs = ' + str(round(max_hs, 2)) + 'ft', xy=(0, 1.03),
                 xycoords='axes fraction', fontsize=12, weight='bold')
    plt.annotate('Lat, Lon: ' + str(max_hs_lat) + ', ' + str(max_hs_lon), xy=(0, 1.01),
                 xycoords='axes fraction', fontsize=12, weight='bold')

    sat_pass = 1

    for platform_nb in range(1, len(satellite_list)+1):

        platform_subset = time_subset[time_subset['platform'] == platform_nb]

        cond_limits_1 = platform_subset['latitude'] >= min_lat
        cond_limits_2 = platform_subset['latitude'] <= max_lat
        platform_subset = platform_subset[cond_limits_1 & cond_limits_2]

        lon_temp = platform_subset['longitude'].values
        lat_temp = platform_subset['latitude'].values
        sigH_temp = platform_subset['sigH'].values * 3.2808  # convert mt to ft
        platform_temp = platform_subset['platform'].values
        timestep_temp = platform_subset['timestamp'].values

        if len(lon_temp) > 10:

            if center_basin != 'Pacific':
                # readjust to atlantic centered
                idx = list(np.where(lon_temp > 180)[0])
                lon_temp[idx] = lon_temp[idx]-360
            grad_lat = np.gradient(lat_temp, timestep_temp)
            test_signal = np.sign(grad_lat)

            signal_change = ((np.roll(test_signal, 1) - test_signal) != 0).astype(int)

            inflex_points_temp = [i for i, e in enumerate(signal_change) if e != 0]

            # Make sure the first point and last point are included
            inflex_points = []
            inflex_points.append(0)
            inflex_points.append(len(lon_temp))
            [inflex_points.append(x) for x in inflex_points_temp if x not in inflex_points]
            inflex_points = np.sort(inflex_points)

            nb_cycles = len(inflex_points)-1  # to discount the last point

            # create legend labels
            lon_pass = lon_temp[inflex_points[0]:inflex_points[0]+1]
            lat_pass = lat_temp[inflex_points[0]:inflex_points[0]+1]
            x, y = m(lon_pass, lat_pass)
            m.plot(x, y, '-', color=colors_list[platform_nb-1],
                   markerfacecolor=colors_list[platform_nb-1], markersize=2, label=satellite_list[platform_nb-1])

            for k in range(0, nb_cycles):
                lon_pass = lon_temp[inflex_points[k]:inflex_points[k+1]-1]
                lat_pass = lat_temp[inflex_points[k]:inflex_points[k+1]-1]

                if len(lon_pass) > 10:
                    x, y = m(lon_pass, lat_pass)
                    m.plot(x, y, '.', color=colors_list[platform_nb-1],
                           markerfacecolor=colors_list[platform_nb-1], markersize=2)
                    irand = randrange(2, 5)
                    text_spot_lon = lon_pass[round(len(lon_pass)/irand)]
                    text_spot_lat = lat_pass[round(len(lat_pass)/irand)]
                    ax.annotate(str(sat_pass), m(text_spot_lon, text_spot_lat), color='black', ha='center', 
                                fontsize=15, bbox=dict(facecolor='white', edgecolor='black', boxstyle='round,pad=0.25'))
                    sat_pass += 1

    ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.1),
              ncol=7, fancybox=True, shadow=True, fontsize=15)

    outname = archive_dir + '/glob_grd_trk_' + \
        datetime.datetime.strftime(current_run_date, '%Y%m%d%H') + '.png'
    plt.savefig(outname, facecolor=fig.get_facecolor())

    plt.close()

    create_glob_grd_trk_html(
        current_run_date, archive_dir, sat_pass)


def create_assimilation_impact(grid_sigH_operational, grid_sigH_assim, grid_lon, grid_lat, current_run_date, center_basin, archive_dir):

    if center_basin != 'Pacific':
        grid_lon_temp = grid_lon
        grid_sigH_operational, grid_lon = shiftgrid(
            180., grid_sigH_operational, grid_lon, start=False)  # shiftgrid
        grid_sigH_assim, grid_lon = shiftgrid(
            180., grid_sigH_assim, grid_lon_temp, start=False)  # shiftgrid

    fig = plt.figure(figsize=(26, 13), facecolor='white')
    ax = fig.add_axes([0.1, 0.1, 0.8, 0.8])
    if center_basin == 'Pacific':
        m = Basemap(llcrnrlon=0, llcrnrlat=min_lat, urcrnrlon=360, urcrnrlat=max_lat,
                    resolution='l', projection='merc')
    else:
        m = Basemap(llcrnrlon=-180, llcrnrlat=min_lat, urcrnrlon=180, urcrnrlat=max_lat,
                    resolution='l', projection='merc')

    x, y = m(*np.meshgrid(grid_lon, grid_lat))

    assimilation_grid = grid_sigH_assim-grid_sigH_operational
    try:
        max_hs_diff = np.nanmax(assimilation_grid)
        max_hs_diff_idx = np.where(assimilation_grid==max_hs_diff)
        max_hs_diff_lat_idx = max_hs_diff_idx[0][0]
        max_hs_diff_lon_idx = max_hs_diff_idx[1][0]
        max_hs_diff_x = x[max_hs_diff_lat_idx, max_hs_diff_lon_idx]
        max_hs_diff_y = y[max_hs_diff_lat_idx, max_hs_diff_lon_idx]
        max_hs_diff_lat = grid_lat[max_hs_diff_lat_idx]
        max_hs_diff_lon = grid_lon[max_hs_diff_lon_idx]
        if max_hs_diff_lon > 180.0:
            max_hs_diff_lon = max_hs_diff_lon - 360.0
        min_hs_diff = np.nanmin(assimilation_grid)
        min_hs_diff_idx = np.where(assimilation_grid==min_hs_diff)
        min_hs_diff_lat_idx = min_hs_diff_idx[0][0]
        min_hs_diff_lon_idx = min_hs_diff_idx[1][0]
        min_hs_diff_x = x[min_hs_diff_lat_idx, min_hs_diff_lon_idx]
        min_hs_diff_y = y[min_hs_diff_lat_idx, min_hs_diff_lon_idx]
        min_hs_diff_lat = grid_lat[min_hs_diff_lat_idx]
        min_hs_diff_lon = grid_lon[min_hs_diff_lon_idx]
        if min_hs_diff_lon > 180.0:
            min_hs_diff_lon = min_hs_diff_lon - 360.0
    except IndexError:
        max_hs_diff = float(0)
        max_hs_diff_x = float(0)
        max_hs_diff_y = float(0)
        max_hs_diff_lat = float(0)
        max_hs_diff_lon = float(0)
        min_hs_diff = float(0)
        min_hs_diff_x = float(0)
        min_hs_diff_y = float(0)
        min_hs_diff_lat = float(0)
        min_hs_diff_lon = float(0)
        pass

    contour_handle = m.contourf(x, y, assimilation_grid,
                                levels=np.arange(-10, 10.25, 1.0), cmap=assim_cm, extend="both")

    plt.title('Assimilation Impact [ft]: Ob - Model', fontsize='20')

    m.drawcoastlines()
    m.fillcontinents()
    m.drawmapboundary()
    m.drawparallels(np.arange(-90., 90., 10.),
                    labels=[1, 0, 0, 0], fontsize=14)
    m.drawmeridians(np.arange(-180., 180., 20.),
                    labels=[0, 0, 0, 1], fontsize=14)

    plt.annotate('X', xy=(max_hs_diff_x, max_hs_diff_y), fontsize=14, weight='bold', ha='center', va='center')
    plt.annotate('X', xy=(min_hs_diff_x, min_hs_diff_y), fontsize=14, weight='bold', ha='center', va='center')
    plt.annotate('Max Negative Hs Correction = ' + str(round(min_hs_diff, 2)) + 'ft', xy=(0, 1.07),
                 xycoords='axes fraction', fontsize=12, weight='bold')
    plt.annotate('Lat, Lon: ' + str(min_hs_diff_lat) + ', ' + str(min_hs_diff_lon), xy=(0, 1.05),
                 xycoords='axes fraction', fontsize=12, weight='bold')
    plt.annotate('Max Positive Hs Correction = ' + str(round(max_hs_diff, 2)) + 'ft', xy=(0, 1.03),
                 xycoords='axes fraction', fontsize=12, weight='bold')
    plt.annotate('Lat, Lon: ' + str(max_hs_diff_lat) + ', ' + str(max_hs_diff_lon), xy=(0, 1.01),
                 xycoords='axes fraction', fontsize=12, weight='bold')

    divider = make_axes_locatable(ax)
    cax = divider.append_axes('right', size=0.2, pad=0.5)
    cbar = fig.colorbar(contour_handle, cax=cax, ticks=np.arange(-10, 10.5, 1.0))
    cbar.ax.tick_params(labelsize=14)

    outname = archive_dir + '/assimilation_overview_' + \
        datetime.datetime.strftime(current_run_date, '%Y%m%d%H') + '.png'
    plt.savefig(outname)
    plt.close()


def create_global_scatter(grid_sigH_operational, grid_sigH_assim, grid_lon, grid_lat, sat_data_file, current_run_date, archive_dir):

    start_date_string = current_run_date - \
        datetime.timedelta(hours=time_buffer)
    end_date_string = current_run_date + datetime.timedelta(hours=time_buffer)

    start_date_unix = time.mktime(start_date_string.utctimetuple())
    end_date_unix = time.mktime(end_date_string.utctimetuple())

    sat_full_data = pd.read_csv(sat_data_file)

    cond_time_1 = sat_full_data['timestamp'] >= start_date_unix
    cond_time_2 = sat_full_data['timestamp'] <= end_date_unix
    time_subset = sat_full_data[cond_time_1 & cond_time_2]

    cond_limits_1 = time_subset['latitude'] >= min_lat
    cond_limits_2 = time_subset['latitude'] <= max_lat
    platform_subset = time_subset[cond_limits_1 & cond_limits_2]

    lon_pass = platform_subset['longitude'].values
    lat_pass = platform_subset['latitude'].values
    # convert mt to ft
    overall_sat_sigH = platform_subset['sigH'].values * 3.2808

    if len(lon_pass) > 10:

        # Replace NaN with zero for the interpolation
        grid_sigH_operational_interp = grid_sigH_operational
        nan_index = np.isnan(grid_sigH_operational_interp)
        grid_sigH_operational_interp[nan_index] = 0

        operational_interp_spline = RectBivariateSpline(
            grid_lat, grid_lon, grid_sigH_operational_interp)

        sigH_op_alongtrack = operational_interp_spline.ev(
            lat_pass, lon_pass)

        # Replace NaN with zero for the interpolation
        grid_sigH_assim_interp = grid_sigH_assim
        nan_index = np.isnan(grid_sigH_assim_interp)
        grid_sigH_assim_interp[nan_index] = 0

        assim_interp_spline = RectBivariateSpline(
            grid_lat, grid_lon, grid_sigH_assim_interp)

        sigH_assim_alongtrack = assim_interp_spline.ev(lat_pass, lon_pass)

        sigH_op_alongtrack[sigH_op_alongtrack < 0] = 0
        sigH_assim_alongtrack[sigH_assim_alongtrack < 0] = 0

        max_chart_data = max([max(overall_sat_sigH), max(
            sigH_op_alongtrack), max(sigH_assim_alongtrack)])

        max_max_chart_datadata_lim = 5 * round(max_chart_data/5) + 5

        is_assim_valid = np.where(sigH_assim_alongtrack > 0)[0]

        rmse_op = np.sqrt(
            np.mean(np.square(overall_sat_sigH - sigH_op_alongtrack)))
        nrmse_op = rmse_op/(max(overall_sat_sigH) - min(overall_sat_sigH))*100
        bias_op = sum(overall_sat_sigH - sigH_op_alongtrack) / \
            len(overall_sat_sigH)
        corrcoef_op = np.corrcoef(overall_sat_sigH, sigH_op_alongtrack)[1, 0]
        scattter_index_op = rmse_op/np.mean(overall_sat_sigH)
        max_sat = max(overall_sat_sigH)
        nb_obs = len(overall_sat_sigH)

        rmse_assim = np.sqrt(
            np.mean(np.square(overall_sat_sigH - sigH_assim_alongtrack)))
        nrmse_assim = rmse_assim / \
            (max(overall_sat_sigH) - min(overall_sat_sigH))*100
        bias_assim = sum(overall_sat_sigH - sigH_assim_alongtrack) / \
            len(overall_sat_sigH)
        corrcoef_assim = np.corrcoef(
            overall_sat_sigH, sigH_assim_alongtrack)[1, 0]
        scattter_index_assim = rmse_assim/np.mean(overall_sat_sigH)

        fig, ax = plt.subplots(figsize=(20, 10), facecolor='white')
        ax1 = plt.subplot(1, 2,  1)
        ax.set_xlim((0, max_max_chart_datadata_lim))
        ax.set_ylim((0, max_max_chart_datadata_lim))
        x0, x1 = ax.get_xlim()
        y0, y1 = ax.get_ylim()
        ax.set_aspect(abs(x1 - x0) / abs(y1 - y0))
        x = overall_sat_sigH
        y = sigH_op_alongtrack
        plt.scatter(x, y, marker=".", alpha=0.5, label='operational')
        a = np.vstack([x, np.ones(len(x))]).T
        m, c = np.linalg.lstsq(a, y, rcond=None)[0]
        plt.plot(np.arange(0, max_max_chart_datadata_lim-1, 1),
                 m * np.arange(0, max_max_chart_datadata_lim-1, 1) + c,
                 "--",
                 color="lime", label='Linear Fit')

        plt.xlabel("Satellite")
        plt.ylabel("Model")
        plt.plot(np.arange(0, max_max_chart_datadata_lim-1, 1),
                 "--", color="red", label="Identity")

        plt.title('Operational')

        textstr_box = '\n'.join((r'RMSE = %.2f ft' % rmse_op,
                                 r'NRMSE = %.1f %% ' % nrmse_op,
                                 r'Bias = %.2f ' % bias_op,
                                 r'R = %.2f ' % corrcoef_op,
                                 r'Sct Idx = %.2f ' % scattter_index_op,
                                 r'Max Obs = %.1f ft ' % max_sat,
                                 r'Nb Obs = %.0f ' % nb_obs))

        props = dict(boxstyle='round', facecolor='white', alpha=0.5)

        plt.text(0.03, 0.97, textstr_box, transform=ax1.transAxes, fontsize=10,
                 verticalalignment='top', bbox=props)

        ax2 = plt.subplot(1, 2,  2)

        if len(is_assim_valid) < 20:
            ax.set_xlim((0, max_max_chart_datadata_lim))
            ax.set_ylim((0, max_max_chart_datadata_lim))
            x0, x1 = ax.get_xlim()
            y0, y1 = ax.get_ylim()
            ax.set_aspect(abs(x1 - x0) / abs(y1 - y0))
            plt.xlabel("Satellite")
            plt.ylabel("Model")
            plt.title('Assimilated')
            plt.text(0.5, 0.5, 'No Available Data',
                     fontsize=30, horizontalalignment='center')

        else:
            ax.set_xlim((0, max_max_chart_datadata_lim))
            ax.set_ylim((0, max_max_chart_datadata_lim))
            x0, x1 = ax.get_xlim()
            y0, y1 = ax.get_ylim()
            ax.set_aspect(abs(x1 - x0) / abs(y1 - y0))
            x = overall_sat_sigH
            y = sigH_assim_alongtrack
            plt.scatter(x, y, marker=".", alpha=0.5, label='assimilated')
            a = np.vstack([x, np.ones(len(x))]).T
            m, c = np.linalg.lstsq(a, y, rcond=None)[0]
            plt.plot(np.arange(0, max_max_chart_datadata_lim-1, 1),
                     m * np.arange(0, max_max_chart_datadata_lim-1, 1) + c,
                     "--",
                     color="lime", label='Linear Fit')
            plt.xlabel("Satellite")
            plt.ylabel("Model")
            plt.plot(np.arange(0, max_max_chart_datadata_lim-1, 1),
                     "--", color="red", label="Identity")

            plt.title('Assimilated')

            textstr_box = '\n'.join((r'RMSE = %.2f ft' % rmse_assim,
                                     r'NRMSE = %.1f %% ' % nrmse_assim,
                                     r'Bias = %.2f ' % bias_assim,
                                     r'R = %.2f ' % corrcoef_assim,
                                     r'Sct Idx = %.2f ' % scattter_index_assim,
                                     r'Max Obs = %.1f ft ' % max_sat,
                                     r'Nb Obs = %.0f ' % nb_obs))

            props = dict(boxstyle='round', facecolor='white', alpha=0.5)

            plt.text(0.03, 0.97, textstr_box, transform=ax2.transAxes, fontsize=10,
                     verticalalignment='top', bbox=props)

        outname = archive_dir + '/global_scatter_' + \
            datetime.datetime.strftime(current_run_date, '%Y%m%d%H') + '.png'
        plt.savefig(outname)
        plt.close()


def run_individual_pass(grid_sigH_operational, grid_sigH_assim, grid_lon, grid_lat, sat_data_file, current_run_date, archive_dir):

    start_date_string = current_run_date - \
        datetime.timedelta(hours=time_buffer)
    end_date_string = current_run_date + datetime.timedelta(hours=time_buffer)

    start_date_unix = time.mktime(start_date_string.utctimetuple())
    end_date_unix = time.mktime(end_date_string.utctimetuple())

    sat_full_data = pd.read_csv(sat_data_file)

    cond_time_1 = sat_full_data['timestamp'] >= start_date_unix
    cond_time_2 = sat_full_data['timestamp'] <= end_date_unix
    time_subset = sat_full_data[cond_time_1 & cond_time_2]

    sat_pass = 1

    for platform_nb in range(1, 8):

        platform_subset = time_subset[time_subset['platform'] == platform_nb]

        cond_limits_1 = platform_subset['latitude'] >= min_lat
        cond_limits_2 = platform_subset['latitude'] <= max_lat
        platform_subset = platform_subset[cond_limits_1 & cond_limits_2]

        lon_temp = platform_subset['longitude'].values
        lat_temp = platform_subset['latitude'].values
        sigH_temp = platform_subset['sigH'].values * 3.2808  # convert mt to ft
        platform_temp = platform_subset['platform'].values
        timestep_temp = platform_subset['timestamp'].values

        if len(lon_temp) > 10:

            grad_lat = np.gradient(lat_temp, timestep_temp)
            test_signal = np.sign(grad_lat)

            signal_change = ((np.roll(test_signal, 1) -
                              test_signal) != 0).astype(int)

            inflex_points_temp = [
                i for i, e in enumerate(signal_change) if e != 0]

            # Make sure the first point and last point are included
            inflex_points = []
            inflex_points.append(0)
            inflex_points.append(len(lon_temp))
            [inflex_points.append(x)
                for x in inflex_points_temp if x not in inflex_points]
            inflex_points = np.sort(inflex_points)

            nb_cycles = len(inflex_points)-1  # to discount the last point
            for nb in range(0, nb_cycles):

                lon_pass = lon_temp[inflex_points[nb]:inflex_points[nb+1]-1]
                lat_pass = lat_temp[inflex_points[nb]:inflex_points[nb+1]-1]
                sigH_pass = sigH_temp[inflex_points[nb]:inflex_points[nb+1]-1]
                timestep_pass = timestep_temp[inflex_points[nb]
                    :inflex_points[nb+1]-1]

                if len(lon_pass) > 10:
                    lon_pass_chart = lon_pass[0:-1:sat_spatial_jump]
                    lat_pass_chart = lat_pass[0:-1:sat_spatial_jump]
                    sigH_pass_chart = sigH_pass[0:-1:sat_spatial_jump]

                    min_lon = lon_pass[round(len(lon_pass)/2)] - longitude_buffer
                    max_lon = lon_pass[round(len(lon_pass)/2)] + longitude_buffer

                    if max_lon > 360:
                        # Readjust to -180 | 180 format
                        min_lon = min_lon - 360
                        max_lon = max_lon - 360

                    if min_lon < 0 or max_lon > 360:
                        # Readjust to -180 | 180 format
                        grid_sigH_temp_operational, grid_lon_temp_operational = shiftgrid(
                            180., grid_sigH_operational, grid_lon, start=False)  # shiftgrid
                        grid_lat_temp_operational = grid_lat
                        grid_sigH_temp_assim, grid_lon_temp_assim = shiftgrid(
                            180., grid_sigH_assim, grid_lon, start=False)  # shiftgrid
                        grid_lat_temp_assim = grid_lat

                        idx = (lon_pass > 180)
                        lon_pass[idx] = lon_pass[idx]-360
                        idx = (lon_pass_chart > 180)
                        lon_pass_chart[idx] = lon_pass_chart[idx]-360

                    else:
                        grid_lat_temp_operational = grid_lat
                        grid_lon_temp_operational = grid_lon
                        grid_sigH_temp_operational = grid_sigH_operational
                        grid_lat_temp_assim = grid_lat
                        grid_lon_temp_assim = grid_lon
                        grid_sigH_temp_assim = grid_sigH_assim

                    # Replace NaN with zero for the interpolation
                    grid_sigH_temp_operational_interp = grid_sigH_temp_operational
                    nan_index = np.isnan(grid_sigH_temp_operational_interp)
                    grid_sigH_temp_operational_interp[nan_index] = 0

                    operational_interp_spline = RectBivariateSpline(
                        grid_lat_temp_operational, grid_lon_temp_operational, grid_sigH_temp_operational_interp)

                    sigH_op_alongtrack = operational_interp_spline.ev(
                        lat_pass, lon_pass)

                    # Replace NaN with zero for the interpolation
                    grid_sigH_temp_assim_interp = grid_sigH_temp_assim
                    nan_index = np.isnan(grid_sigH_temp_assim_interp)
                    grid_sigH_temp_assim_interp[nan_index] = 0

                    assim_interp_spline = RectBivariateSpline(
                        grid_lat_temp_assim, grid_lon_temp_assim, grid_sigH_temp_assim_interp)

                    sigH_assim_alongtrack = assim_interp_spline.ev(
                        lat_pass, lon_pass)

                    is_assim_valid = np.where(sigH_assim_alongtrack > 0)[0]

                    fig, main_ax = plt.subplots(figsize=(25, 18))

                    suptitle_string = 'Satellite comparasion for ' + \
                        satellite_list[platform_nb-1] + ' data'
                    title_string = 'Lotus model data for : ' + datetime.datetime.strftime(current_run_date, '%Y%m%d %H:00') + '\n Satellite data between ' + datetime.datetime.utcfromtimestamp(
                        timestep_pass[0]).strftime('%Y%m%d %H:%M') + ' and ' + datetime.datetime.utcfromtimestamp(timestep_pass[-1]).strftime('%Y%m%d %H:%M')

                    plt.suptitle(suptitle_string, fontsize=30)
                    plt.title(title_string, fontsize=20)
                    # setup mercator map projection.
                    m = Basemap(llcrnrlon=min_lon, llcrnrlat=min_lat, urcrnrlon=max_lon, urcrnrlat=max_lat,
                                resolution='l', projection='merc')
                    x, y = m(*np.meshgrid(grid_lon_temp_operational,
                                          grid_lat_temp_operational))
                    m.contourf(x, y, grid_sigH_temp_operational, cmap=cm, vmin=0,
                               vmax=50, levels=range(0, 52, 2), extend='max')

                    x, y = m(lon_pass_chart, lat_pass_chart)
                    cls = m.scatter(x, y, marker='o', s=50, c=sigH_pass_chart,
                                    cmap=cm, edgecolor='black', vmin=0, vmax=50)

                    irand = randrange(2, 5)
                    text_spot_lon = lon_pass_chart[round(len(lon_pass_chart)/irand)]
                    text_spot_lat = lat_pass_chart[round(len(lat_pass_chart)/irand)]
                    main_ax.annotate(str(sat_pass), m(text_spot_lon, text_spot_lat), color='black', ha='center',
                                     fontsize=15, bbox=dict(facecolor='white', edgecolor='black', boxstyle='round,pad=0.25'))

                    m.drawcoastlines()
                    m.fillcontinents()
                    m.drawmapboundary()
                    lats = np.arange(-90, 90, 10)
                    lons = np.arange(-180, 180, 20)
                    m.drawparallels(lats, labels=[0, 1, 0, 0])
                    m.drawmeridians(lons, labels=[0, 0, 0, 1])

                    divider = make_axes_locatable(main_ax)
                    # Add colorbar
                    cax = divider.append_axes('left', size=0.2, pad=0.5)
                    cbar = fig.colorbar(
                        cls, cax=cax, ticks=np.arange(0, 52, 2))
                    cbar.ax.tick_params(labelsize=14)

                    # Add Left Sat comparasion chart
                    y_plot = divider.append_axes(
                        'right', size='50%', pad='5%', sharey=main_ax)

                    x_sat, y_sat = m(sigH_pass, lat_pass)
                    y_plot.plot(x_sat, y_sat, '.', color='black',
                                markersize=4, label='Satellite')

                    x_op, y_op = m(sigH_op_alongtrack, lat_pass)
                    y_plot.plot(x_op, y_op, '.', color='red',
                                markersize=2, label='Operational')

                    if len(is_assim_valid) > 20:
                        x_assim, y_assim = m(sigH_assim_alongtrack, lat_pass)
                        y_plot.plot(x_assim, y_assim, '.', color='lime',
                                    markersize=2, label='Assimilated')
                    else:
                        y_plot.plot(np.nan, np.nan, '.',
                                    color='lime', markersize=4)

                    max_sat_data = max([max(sigH_pass), max(
                        sigH_op_alongtrack), max(sigH_assim_alongtrack)])
                    if max_sat_data > 50:
                        sigh_steps_label = np.arange(0, 125, 25)
                    if max_sat_data <= 50:
                        sigh_steps_label = np.arange(0, 60, 10)
                    if max_sat_data <= 40:
                        sigh_steps_label = np.arange(0, 50, 10)
                    if max_sat_data <= 30:
                        sigh_steps_label = np.arange(0, 35, 5)
                    if max_sat_data <= 25:
                        sigh_steps_label = np.arange(0, 30, 5)
                    if max_sat_data <= 20:
                        sigh_steps_label = np.arange(0, 25, 5)
                    if max_sat_data <= 15:
                        sigh_steps_label = np.arange(0, 20, 2)

                    y_plot.grid(which='major', axis='both', linestyle='--')
                    xticks_data, _ = m(sigh_steps_label, 0*sigh_steps_label)
                    y_plot.set_xticks(xticks_data)
                    y_plot.set_xticklabels(["%d" % int(float(y))
                                            for y in sigh_steps_label])
                    y_plot.set_xlabel('Sig Wave Height [ft]', fontsize=14)
                    lgnd = y_plot.legend(loc='best', ncol=1, fancybox=True,
                                         shadow=True, fontsize=12)

                    # change the marker size manually for both lines
                    lgnd.legendHandles[0]._legmarker.set_markersize(15)
                    lgnd.legendHandles[1]._legmarker.set_markersize(15)
                    if len(is_assim_valid) > 20:
                        lgnd.legendHandles[2]._legmarker.set_markersize(15)

                    # Add Second Sat comparasion chart
                    y_plot_2 = divider.append_axes(
                        'right', size='50%', pad='8%', sharey=main_ax)

                    if len(is_assim_valid) > 20:
                        op_delta = sigH_pass-sigH_assim_alongtrack
                        y_plot_2.set_xlabel(
                            'Satellite - Assimilated [ft]', fontsize=14)
                    else:
                        op_delta = sigH_pass-sigH_op_alongtrack
                        y_plot_2.set_xlabel(
                            'Satellite - Operational [ft]', fontsize=14)

                    colors_scater = []
                    for cc in op_delta:
                        if cc <= 0:
                            colors_scater.append('blue')
                        else:
                            colors_scater.append('red')

                    x_op, y_op = m(op_delta, lat_pass)

                    y_plot_2.scatter(
                        x_op, y_op, c=colors_scater, marker=".")
                    max_sat_data = max(abs(op_delta))
                    if max_sat_data > 50:
                        sigh_steps_label = np.arange(-100, 125, 25)
                    if max_sat_data <= 50:
                        sigh_steps_label = np.arange(-50, 60, 10)
                    if max_sat_data <= 40:
                        sigh_steps_label = np.arange(-40, 50, 10)
                    if max_sat_data <= 30:
                        sigh_steps_label = np.arange(-30, 35, 5)
                    if max_sat_data <= 25:
                        sigh_steps_label = np.arange(-25, 30, 5)
                    if max_sat_data <= 20:
                        sigh_steps_label = np.arange(-20, 25, 5)
                    if max_sat_data <= 15:
                        sigh_steps_label = np.arange(-15, 20, 2)
                    if max_sat_data <= 10:
                        sigh_steps_label = np.arange(-10, 12, 2)
                    if max_sat_data <= 5:
                        sigh_steps_label = np.arange(-5, 6, 1)

                    y_plot_2.grid(which='major', axis='both',
                                  linestyle='--')
                    xticks_data, _ = m(
                        sigh_steps_label, 0*sigh_steps_label)
                    y_plot_2.set_xticks(xticks_data)
                    y_plot_2.set_xticklabels(["%d" % int(float(y))
                                              for y in sigh_steps_label])

                    outname = archive_dir + '/indiv_pass_' + datetime.datetime.strftime(
                        current_run_date, '%Y%m%d%H') + '_' + str(sat_pass) + '.png'
                    plt.savefig(outname)
                    plt.close()

                    # Create Scatter for individual pass

                    max_chart_data = max([max(sigH_pass), max(
                        sigH_op_alongtrack), max(sigH_assim_alongtrack)])

                    max_max_chart_datadata_lim = 5 * \
                        round(max_chart_data/5) + 5

                    rmse_op = np.sqrt(
                        np.mean(np.square(sigH_pass - sigH_op_alongtrack)))
                    nrmse_op = rmse_op/(max(sigH_pass) - min(sigH_pass))*100
                    bias_op = sum(sigH_pass - sigH_op_alongtrack) / \
                        len(sigH_pass)
                    corrcoef_op = np.corrcoef(
                        sigH_pass, sigH_op_alongtrack)[1, 0]
                    scattter_index_op = rmse_op/np.mean(sigH_pass)
                    max_sat = max(sigH_pass)
                    nb_obs = len(sigH_pass)

                    rmse_assim = np.sqrt(
                        np.mean(np.square(sigH_pass - sigH_assim_alongtrack)))
                    nrmse_assim = rmse_assim / \
                        (max(sigH_pass) - min(sigH_pass))*100
                    bias_assim = sum(sigH_pass - sigH_assim_alongtrack) / \
                        len(sigH_pass)
                    corrcoef_assim = np.corrcoef(
                        sigH_pass, sigH_assim_alongtrack)[1, 0]
                    scattter_index_assim = rmse_assim/np.mean(sigH_pass)

                    is_assim_valid = np.where(sigH_assim_alongtrack > 0)[0]

                    fig, ax = plt.subplots(figsize=(20, 10), facecolor='white')
                    ax1 = plt.subplot(1, 2,  1)
                    ax.set_xlim((0, max_max_chart_datadata_lim))
                    ax.set_ylim((0, max_max_chart_datadata_lim))
                    x0, x1 = ax.get_xlim()
                    y0, y1 = ax.get_ylim()
                    ax.set_aspect(abs(x1 - x0) / abs(y1 - y0))
                    x = sigH_pass
                    y = sigH_op_alongtrack
                    plt.scatter(x, y, marker=".", alpha=0.5,
                                label='operational')
                    a = np.vstack([x, np.ones(len(x))]).T
                    m, c = np.linalg.lstsq(a, y, rcond=None)[0]
                    plt.plot(np.arange(0, max_max_chart_datadata_lim-1, 1),
                             m *
                             np.arange(0, max_max_chart_datadata_lim-1, 1) + c,
                             "--",
                             color="lime", label='Linear Fit')

                    plt.xlabel("Satellite")
                    plt.ylabel("Model")
                    plt.plot(np.arange(0, max_max_chart_datadata_lim-1, 1),
                             "--", color="red", label="Identity")

                    plt.title('Operational')

                    textstr_box = '\n'.join((r'RMSE = %.2f ft' % rmse_op,
                                             r'NRMSE = %.1f %% ' % nrmse_op,
                                             r'Bias = %.2f ' % bias_op,
                                             r'R = %.2f ' % corrcoef_op,
                                             r'Sct Idx = %.2f ' % scattter_index_op,
                                             r'Max Obs = %.1f ft ' % max_sat,
                                             r'Nb Obs = %.0f ' % nb_obs))

                    props = dict(boxstyle='round',
                                 facecolor='white', alpha=0.5)

                    plt.text(0.03, 0.97, textstr_box, transform=ax1.transAxes, fontsize=10,
                             verticalalignment='top', bbox=props)

                    ax2 = plt.subplot(1, 2,  2)

                    if len(is_assim_valid) < 20:
                        ax.set_xlim((0, max_max_chart_datadata_lim))
                        ax.set_ylim((0, max_max_chart_datadata_lim))
                        x0, x1 = ax.get_xlim()
                        y0, y1 = ax.get_ylim()
                        ax.set_aspect(abs(x1 - x0) / abs(y1 - y0))
                        plt.xlabel("Satellite")
                        plt.ylabel("Model")
                        plt.title('Assimilated')
                        plt.text(0.5, 0.5, 'No Available Data',
                                 fontsize=30, horizontalalignment='center')

                    else:
                        ax.set_xlim((0, max_max_chart_datadata_lim))
                        ax.set_ylim((0, max_max_chart_datadata_lim))
                        x0, x1 = ax.get_xlim()
                        y0, y1 = ax.get_ylim()
                        ax.set_aspect(abs(x1 - x0) / abs(y1 - y0))
                        x = sigH_pass
                        y = sigH_assim_alongtrack
                        plt.scatter(x, y, marker=".", alpha=0.5,
                                    label='assimilated')
                        a = np.vstack([x, np.ones(len(x))]).T
                        m, c = np.linalg.lstsq(a, y, rcond=None)[0]
                        plt.plot(np.arange(0, max_max_chart_datadata_lim-1, 1),
                                 m *
                                 np.arange(
                                     0, max_max_chart_datadata_lim-1, 1) + c,
                                 "--",
                                 color="lime", label='Linear Fit')

                        plt.xlabel("Satellite")
                        plt.ylabel("Model")
                        plt.plot(np.arange(0, max_max_chart_datadata_lim-1, 1),
                                 "--", color="red", label="Identity")

                        plt.title('Assimilated')

                        textstr_box = '\n'.join((r'RMSE = %.2f ft' % rmse_assim,
                                                 r'NRMSE = %.1f %% ' % nrmse_assim,
                                                 r'Bias = %.2f ' % bias_assim,
                                                 r'R = %.2f ' % corrcoef_assim,
                                                 r'Sct Idx = %.2f ' % scattter_index_assim,
                                                 r'Max Obs = %.1f ft ' % max_sat,
                                                 r'Nb Obs = %.0f ' % nb_obs))

                        props = dict(boxstyle='round',
                                     facecolor='white', alpha=0.5)

                        plt.text(0.03, 0.97, textstr_box, transform=ax2.transAxes, fontsize=10,
                                 verticalalignment='top', bbox=props)

                    outname = archive_dir + '/individual_pass_scatter_' + \
                        datetime.datetime.strftime(
                            current_run_date, '%Y%m%d%H') + '_' + str(sat_pass) + '.png'
                    plt.savefig(outname)
                    plt.close()

                    sat_pass += 1
