#!/bin/sh

source /home/sam.wilson/.bashrc
source ${AWS_SCIENCE_BASE}/AWS_PATH_CONFIG

cd /home/sam.wilson/surfline-labs/Satellite_Validation
/usr/bin/python3 /home/sam.wilson/surfline-labs/Satellite_Validation/run_sat_validation.py
