import os
import sys
import datetime
import shutil
import netCDF4
import numpy as np
import time
from pathlib import Path
import shutil
import glob

sys.path.insert(0, './python_aux_scripts')
import process_data
import download_data

main_dir = os.getcwd()
data_dir = '/ocean/transient/lotus/Satellite_Validation'

# Pacific or Atlantic
center_basin = 'Pacific'

# in days
run_period = 2
website_period = 6 

archive_data_dir = data_dir + '/archive_data'
archive_outputs_dir = data_dir + '/archive_outputs'

if os.path.exists(archive_outputs_dir + '/css'):
    pass
else:
    os.makedirs(archive_outputs_dir + '/css')

if os.path.exists(archive_data_dir):
    pass
else:
    os.makedirs(archive_data_dir)

shutil.copy2(main_dir + '/python_aux_scripts/reset.css',
             archive_outputs_dir + '/css/reset.css')
shutil.copy2(main_dir + '/python_aux_scripts/style.css',
             archive_outputs_dir + '/css/style.css')

end_website_date_string = time.strftime('%Y%m%d')
end_website_date = datetime.datetime.strptime(end_website_date_string, '%Y%m%d')
start_website_date = (datetime.datetime.strptime(end_website_date_string, '%Y%m%d') -
                      datetime.timedelta(days=website_period))
delete_old_data_limit = (datetime.datetime.strptime(end_website_date_string, '%Y%m%d') -
                         datetime.timedelta(days=website_period+1))

last_run_date_string = time.strftime('%Y%m%d')
last_run_date = datetime.datetime.strptime(last_run_date_string, '%Y%m%d')
date_list = [last_run_date + datetime.timedelta(hours=3*x) for x in range(0, 9)]
alpha = min(date_list, key=lambda x: abs(x - datetime.datetime.now()))
end_run_date = alpha - datetime.timedelta(hours=3)
start_run_date = end_run_date - datetime.timedelta(days=run_period)

step_date = start_run_date
while step_date <= end_run_date:
    print(step_date)
    archive_dir = datetime.datetime.strftime(step_date, '%Y%m%d%H')
    archive_output_dir_step = os.path.join(archive_outputs_dir, archive_dir)
    if os.path.exists(archive_output_dir_step):
        pass
    else:
        os.makedirs(archive_output_dir_step)

    new_sat_files = download_data.get_sat_files(step_date, archive_data_dir)
    new_lotus_files = download_data.get_lotus_data(step_date, archive_data_dir)

    if new_sat_files != 0 or new_lotus_files != 0:
        lotus_file_time = datetime.datetime.strftime(step_date, '%Y%m%d%H')
        lotus_file_name = os.path.join(archive_data_dir, 'ww3_lotus_GLOB_30m.' + lotus_file_time)
        operational_lotus_file = Path(lotus_file_name + '_op.nc')
        assimilated_lotus_file = Path(lotus_file_name + '_assim.nc')
        nc_file = netCDF4.Dataset(str(operational_lotus_file), 'r', mmap=False)
        grid_sigH_operational = nc_file.variables['hs'][0][:][:] * 3.2808
        grid_lon = nc_file.variables['longitude'][:]
        grid_lat = nc_file.variables['latitude'][:]
        nc_file.close()
        grid_sigH_operational = np.array(grid_sigH_operational)
        grid_sigH_operational[grid_sigH_operational > 1000] = np.nan

        if assimilated_lotus_file.is_file():
            nc_file = netCDF4.Dataset(str(assimilated_lotus_file), 'r', mmap=False)
            grid_sigH_assim = nc_file.variables['hs'][0][:][:] * 3.2808
            nc_file.close()
            grid_sigH_assim = np.array(grid_sigH_assim)
            grid_sigH_assim[grid_sigH_assim > 1000] = np.nan
        else:
            grid_sigH_assim = grid_sigH_operational * np.nan

        sat_data_file = os.path.join(archive_data_dir, 'sat_data_' + lotus_file_time + '.dat')
        check4file = Path(sat_data_file)

        if check4file.is_file():
            process_data.show_global_ground_tracks(grid_sigH_operational, grid_lon, grid_lat,
                                                   sat_data_file, step_date, center_basin,
                                                   archive_output_dir_step)
            process_data.create_assimilation_impact(grid_sigH_operational, grid_sigH_assim, grid_lon,
                                                    grid_lat, step_date, center_basin,
                                                    archive_output_dir_step)
            process_data.create_global_scatter(grid_sigH_operational, grid_sigH_assim, grid_lon,
                                               grid_lat, sat_data_file, step_date,
                                               archive_output_dir_step)
            process_data.run_individual_pass(grid_sigH_operational, grid_sigH_assim, grid_lon,
                                             grid_lat, sat_data_file, step_date,
                                             archive_output_dir_step)

    step_date = step_date + datetime.timedelta(hours=3)

process_data.create_main_index(start_website_date, end_website_date, archive_outputs_dir)

# Remove old files
for f in glob.glob(archive_data_dir + '/global_vavh_l3_rt_*.nc'):
    file_date = datetime.datetime.strptime(f[-50:-35], '%Y%m%dT%H%M%S')
    if file_date < delete_old_data_limit:
        os.remove(f)

for f in glob.glob(archive_data_dir + '/sat_data*.dat'):
    file_date = datetime.datetime.strptime(f[-14:-4], '%Y%m%d%H')
    if file_date < delete_old_data_limit:
        os.remove(f)

for f in glob.glob(archive_data_dir + '/ww3_lotus_GLOB_30m.*assim.nc'):
    file_date = datetime.datetime.strptime(f[-19:-9], '%Y%m%d%H')
    if file_date < delete_old_data_limit:
        os.remove(f)

for f in glob.glob(archive_data_dir + '/ww3_lotus_GLOB_30m.*op.nc'):
    file_date = datetime.datetime.strptime(f[-16:-6], '%Y%m%d%H')
    if file_date < delete_old_data_limit:
        os.remove(f)

folder_list = os.walk(archive_outputs_dir)
for x in folder_list:
    dir_file = x[0]
    if dir_file[-10:-8] == '20':
        folder_date = datetime.datetime.strptime(dir_file[-10:], '%Y%m%d%H')
        if folder_date < delete_old_data_limit:
            shutil.rmtree(dir_file)

os.system('aws --profile surfline-legacy s3 cp --recursive ' + archive_outputs_dir +
          '/ s3://api-charts/validation/lotus/ --acl public-read')
