import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import cv2

import tensorflow as tf

class Model(object):
    def __init__(self):
        # Initialize a 3x3 homography with a slightly perturbed identity
        self.hom = tf.Variable(tf.random.normal([3, 3], dtype="float64") / 9.,
                               dtype="float64")
        # self.hom = tf.Variable(np.array([[1,0,0], [0,1,0], [0,0,1.02]]), dtype="float64")
        print(self.hom)

    def __call__(self, val):
        lat = tf.Variable(val[0], dtype="float64")
        lon = tf.Variable(val[1], dtype="float64")

        # This simple test demonstrates that the training data we have will converge.
        # There's no fundemental problem with out loss function or process.
        # return tf.convert_to_tensor([self.hom[0][0] * lat + self.hom[1][0],
        # self.hom[0][1] * lon + self.hom[1][1]], dtype="float64")

        # Expand out the matrix calculation so we can replace terms easily later
        return tf.convert_to_tensor(
            [((self.hom[0][0] * lat) + (self.hom[0][1] * lon) + self.hom[0][2])
             / ((self.hom[2][0] * lat) + (self.hom[2][1] * lon) + self.hom[2][
                2]),
             ((self.hom[1][0] * lat) + (self.hom[1][1] * lon) + self.hom[1][2])
             / ((self.hom[2][0] * lat) + (self.hom[2][1] * lon) + self.hom[2][
                 2])])


# model = Model()
# print(model([100., 200.]))

def loss(predicted_y, target_y):
    return tf.reduce_mean(
        tf.sqrt(
            tf.square(predicted_y[0] - target_y[0])
            + tf.square(predicted_y[1] - target_y[1])
        )
    )


model = Model()

print(loss(model([100., 200.]), [100., 200.]))

optimizer = tf.optimizers.SGD(learning_rate=1e-2)
loss_history = []

epochs = range(1000000)
for epoch in epochs:

    inputs = tf.convert_to_tensor(np.random.uniform(0, 1, [2, 20]),
                                  dtype="float64")
    outputs = inputs

    with tf.GradientTape() as t:
        current_loss = loss(model(inputs), outputs)

    loss_history.append(current_loss.numpy())
    dHom = t.gradient(current_loss, [model.hom])

    optimizer.apply_gradients(zip(dHom, [model.hom]))

    if epoch % 300 == 0:
        print((epoch, current_loss))
        print("GRADIENT", tf.dtypes.cast(dHom, tf.float16))
        print("HOMOGRAPHY", tf.dtypes.cast(model.hom, tf.float16))
        print('CURRENT LOSS', current_loss)

    # if current_loss < .0001:
    #    break

print("COMPLETE")

print(model.hom.numpy())