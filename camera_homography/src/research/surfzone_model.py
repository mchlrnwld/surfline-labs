"""
Camera detections are single frame, stateless observations with some amount of
error. We're using these to infer a stateful understanding of the surfzone and
this class represents that.

We'll use detections to build this model and understand it's change through
time, the final output will look very similar to the input detections.
"""
import numpy as np
import pandas as pd
import copy
import logging


class SurfzoneModel(object):

    def __init__(self, detection):
        """
        Constructor

        :param detection: Detection object
        """
        self.detection = detection
        self.times = self.detection.get_times()

        self.model = detection.get_frame_detections(self.times[0],
                                                    [3, 4, 5])

        self.model['tracking_index'] = self.model.index.values + 1

        # We have two internal representations, one is representative of a
        # moment in time (the model) the other is representative of the
        # model through time
        self.timemodel = self.model

        self.model['num_detections'] = 3
        self.model['last_update'] = 0
        self.model['first_added'] = self.times[0]
        self.model['index_added'] = self.model.index.values
        self.model['index_updated'] = self.model.index.values

        self.logger = logging.getLogger('surfzone_model')
        fh = logging.FileHandler("./output/tracking.log", mode='w')
        fh.setFormatter(logging.Formatter('%(message)s'))
        self.logger.addHandler(fh)
        #logging.basicConfig(level=logging.DEBUG)
        self.log = self.logger.info
        pd.set_option('display.max_rows', 500)
        pd.set_option('display.max_columns', 500)
        pd.set_option('display.width', 1000)
        pd.options.display.float_format = '{:.2f}'.format

    def process(self):
        """
        Process through the detections and build the model state
        """

        detections = []
        for i in range(0, len(self.times)):
            self.add_detection(self.times[i])
            detections.append(copy.copy(self.model[self.model['num_detections'] > 10]))
        self.timemodel = pd.concat(detections)

    def add_detection(self, timestep):
        """
        Take detections for a single timestep, process their impact on the model

        :param timestep: detection timestep
        """

        # CONFIGURATION
        NEW_DETECTION_DISTANCE_THRESHOLD = 40
        DISTANCE_THRESHOLD = 6 * self.detection.frame_step
        TIME_TO_DIE = 1.2 * self.detection.frame_step
        UPDATE_FIELDS = ['timestamp',
                         'center_x',
                         'center_y',
                         'px1',
                         'px2',
                         'py1',
                         'py2']

        new_det = self.detection.get_frame_detections(timestep, [3, 4, 5])
        idx, dist = self.detection.calc_hungarian_distances(
            self.model['center_x'].values,
            self.model['center_y'].values,
            new_det['center_x'].values,
            new_det['center_y'].values)

        # UPDATE MATCHING DETECTIONS
        # For objects in the model for which there appears to be a new matching
        # detection we can update the model accordingly.

        # Get a list of IDs for
        matched_new_det_index = new_det.index[idx][dist < DISTANCE_THRESHOLD]
        matched_model_index = self.model.index.values[dist < DISTANCE_THRESHOLD]

        for field in UPDATE_FIELDS:
            self.model[field].loc[matched_model_index] = \
                new_det[field].loc[matched_new_det_index].values

        self.model['num_detections'].loc[matched_model_index] = \
            self.model['num_detections'].loc[matched_model_index].values + 1

        self.model['index_updated'].loc[matched_model_index] =\
            matched_new_det_index

        # Every detection in the model is still valid, unless pruned below
        # so we should update it's timestamp
        self.model['timestamp'] = timestep

        # However we should only record a last_update date for tnose with
        # positional matches
        self.model['last_update'].loc[matched_model_index] = timestep

        # ADD NEW DETECTIONS
        # New detections that appear to not match existing detections will be
        # added to the model, although they won't be considered to be objects
        # until they've exceeded a threshold of matching detections.
        unique_new_det_index = \
            np.setdiff1d(new_det.index, matched_new_det_index)

        new_model = new_det.loc[unique_new_det_index]

        if len(new_model) > 0:
            new_model['num_detections'] = 1
            new_model['last_update'] = timestep
            new_model['first_added'] = timestep
            new_model['index_added'] = unique_new_det_index
            new_model['index_updated'] = unique_new_det_index
            tracking_index = self.model['tracking_index'].values.max()
            new_model['tracking_index'] = range(int(tracking_index + 1),
                                                int(tracking_index + len(new_model) + 1))

            idx, dist = self.detection.calc_hungarian_distances(
                self.model['center_x'].values,
                self.model['center_y'].values,
                new_model['center_x'].values,
                new_model['center_y'].values)

            new_model = new_model.drop(
                new_model.index[idx[dist < NEW_DETECTION_DISTANCE_THRESHOLD]])

            self.model = self.model.append(new_model)

        self.log("\n\n"
                 + str(timestep)
                 + " -----------------------------------------\n")
        model_log_fields = ['tracking_index', 'timestamp',
                            'px1', 'py1', 'px2', 'py2',
                            'class',
                            'num_detections',
                            'last_update',
                            'index_updated',
                            'first_added',
                            'index_added']

        new_log_fields = ['timestamp',
                          'px1', 'py1', 'px2', 'py2',
                          'class', 'score']

        self.log("MODEL STATE " + str(len(self.model)))
        self.log(self.model[model_log_fields].astype({'px1': 'int32',
                                                      'px2': 'int32',
                                                      'py1': 'int32',
                                                      'py2': 'int32'}))
        self.log("NEW DETECTIONS " + str(len(new_det)))
        self.log(new_det[new_log_fields].astype({'px1': 'int32',
                                                 'px2': 'int32',
                                                 'py1': 'int32',
                                                 'py2': 'int32'}))

        # PRUNE DEAD OBJECTS
        # After a certain amount of time since a last matching detection we
        # need to prune our model of dead objects.
        self.model = self.model[
            timestep - self.model['last_update'] < TIME_TO_DIE]

        return

    def make_diagnostic_video(self, input_filename, output_filename):
        """
        Create a simple diagnostic video to visualise the frame detections

        :param input_filename: input filename
        :param output_filename:
        :return:
        """

        det = copy.copy(self.detection)
        det.data = self.timemodel

        det.make_diagnostic_video(input_filename, output_filename)
