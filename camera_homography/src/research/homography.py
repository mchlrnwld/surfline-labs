"""
Explicit homography solvers and utilities
"""
import numpy as np
import cv2
import random
import pyproj
import math


def get_homography_svd(p1, p2):
    """
    Solve homography using SVD

    :param p1: source points array
    :param p2: destination points array
    :return: 3x3 homography
    """

    a = []

    for i in range(0, len(p1)):
        x, y = p1[i][0], p1[i][1]
        u, v = p2[i][0], p2[i][1]
        a.append([x, y, 1, 0, 0, 0, -u * x, -u * y, -u])
        a.append([0, 0, 0, x, y, 1, -v * x, -v * y, -v])
        #a.append([x, y, 1, -u * x * 200, 0, 0, -u * x, -u * y, -u])
        #a.append([0, 0, 0, -v * x * 200, y, 1, -v * x, -v * y, -v])

    a = np.asarray(a)
    u, s, vh = np.linalg.svd(a)
    l = vh[-1, :] / vh[-1, -1]
    h = l.reshape(3, 3)

    return h


def get_x2y2(x1, y1, hom):
    """
    Given a pair of source points and a homography calculate the destination
    points

    :param x1: Source X point
    :param y1: Source Y point
    :param hom: 3x3 homography
    :return: x2, y2
    """
    inp = [x1, y1, 1]
    res = np.matmul(hom, inp)
    x2 = res[0] / res[2]
    y2 = res[1] / res[2]
    return x2, y2


def get_RANSAC_homography(p1, p2, method=get_homography_svd, iter=1000):
    """
    RANSAC is random sample consensus, normally a random sample of points are
    taken and a model built from these, the count of points that conform to
    this model to some extent are then the score for that random sample,
    more points are randomly sampled to try to maximise this score.

    The algorithm is typically used where there is some set of the data that
    represents true correpondences and some that doesn't. Because our
    GPS / image matching technique is unlikely to lead to true outliers, rather
    have a somewhat consistent amount of noise with each sample we're scoring
    based on distance as a loss function. There have to be better ways to
    minimise this loss than this random sample approach.

    :param p1: source points
    :param p2: destination points
    :param method: homography function
    :param iter: number of random sample iterations
    :return: the best homography calculated over the random sample
    """

    best_hom = None
    best_loss = 999999

    random.seed(10)

    for i in range(0, iter):
        idx = random.sample(range(0, len(p1)), 4)
        hom = method(p1[idx], p2[idx])
        loss = get_loss(p1, p2, hom)
        if loss < best_loss:
            best_hom = hom
            best_loss = loss

    return best_hom, idx


def get_loss(source_points, dest_points, hom):
    """
    Calculate the sum of squares of differences between calcualted and
    actual locations

    :param source_points: source points
    :param dest_points:  truth destination points
    :param hom: homography
    :return:
    """

    calc_points = []
    for i in range(0, source_points.shape[0]):
        calc_points.append(get_x2y2(source_points[i][0],
                                    source_points[i][1], hom))

    calc_points = np.array(calc_points)
    loss = np.sqrt(np.mean(np.sum(
        np.power(dest_points - calc_points, 2), axis=1)))
    return loss


def get_identity_test_points(num_points, noise_std):
    """
    Return a set of points matching except for the addition of noise. This
    can be used to test how a given calculation

    :param num_points: how many points in the test set
    :param noise_std: standard deviation of noise
    :return: test set array
    """

    np.random.seed(10)

    x1 = np.random.uniform(0, 1920, size=[num_points])
    y1 = np.random.uniform(0, 1080, size=[num_points])

    x2 = x1 + np.random.normal(0, noise_std, size=[num_points])
    y2 = y1 + np.random.normal(0, noise_std, size=[num_points])

    return np.array([np.stack([x1, y1], axis=1),
                     np.stack([x2, y2], axis=1)])


def get_distance(p1, p2):
    """
    Basic geo functions on two points

    :param p1: [lat, lon]
    :param p2: [lat, lon]
    :return: fwd_azimuth, back_azimuth, distance
    """
    R = 6372800  # Earth radius in meters
    lat1, lon1 = p1
    lat2, lon2 = p2

    phi1, phi2 = math.radians(lat1), math.radians(lat2)
    dphi = math.radians(lat2 - lat1)
    dlambda = math.radians(lon2 - lon1)

    a = math.sin(dphi / 2) ** 2 + \
        math.cos(phi1) * math.cos(phi2) * math.sin(dlambda / 2) ** 2

    return 2 * R * math.atan2(math.sqrt(a), math.sqrt(1 - a))


def get_initial_compass_bearing(p1, p2):
    """
    Public domain from:

    https://gist.github.com/jeromer/2005586

    Calculates the bearing between two points.
    The formulae used is the following:
        θ = atan2(sin(Δlong).cos(lat2),
                  cos(lat1).sin(lat2) − sin(lat1).cos(lat2).cos(Δlong))
    :Parameters:
      - `pointA: The tuple representing the latitude/longitude for the
        first point. Latitude and longitude must be in decimal degrees
      - `pointB: The tuple representing the latitude/longitude for the
        second point. Latitude and longitude must be in decimal degrees
    :Returns:
      The bearing in degrees
    :Returns Type:
      float
    """

    lat1 = math.radians(p1[0])
    lat2 = math.radians(p2[0])

    diffLong = math.radians(p2[1] - p1[1])

    x = math.sin(diffLong) * math.cos(lat2)
    y = math.cos(lat1) * math.sin(lat2) \
        - (math.sin(lat1) * math.cos(lat2) * math.cos(diffLong))

    initial_bearing = math.atan2(x, y)

    # Now we have the initial bearing but math.atan2 return values
    # from -180° to + 180° which is not what we want for a compass bearing
    # The solution is to normalize the initial bearing as shown below
    initial_bearing = math.degrees(initial_bearing)
    compass_bearing = (initial_bearing + 360) % 360

    return compass_bearing


def get_distance_from_shore(point, shorepoint, beachangle):
    """
    Calculate the distance of a point from the beach in meters

    :param point: offshore point [lat, lon]
    :param shorepoint: shore point [lat, lon]
    :param beachangle: angle in degrees of a perpendicular to the shore in the
    offshore direction (eg. 270 is a beach facing due west)
    :return: distance in meters from the beach
    """

    distance = get_distance(shorepoint, point)
    bearing = get_initial_compass_bearing(shorepoint, point)
    angle_diff = beachangle - bearing
    angle_diff = 90 - abs((angle_diff + 180) % 360 - 180)

    distance_shore = math.sin(math.radians(angle_diff)) * distance

    return distance_shore


def main():
    """
    Run a basic test of the RANSAC homography method on fake data
    """
    test = get_identity_test_points(100, 50)
    print(np.sqrt(np.mean(np.sum(
            np.power(test[0] - test[1], 2), axis=1))))
    hom_svd = get_homography_svd(test[0], test[1])
    hom_ransac = cv2.findHomography(test[0], test[1], method=cv2.RANSAC)[0]

    print(get_loss(test[0], test[1], hom_svd))
    print(get_loss(test[0], test[1], hom_ransac))
    print(get_loss(test[0], test[1],
                   get_RANSAC_homography(test[0], test[1], iter=10000)[0]))
