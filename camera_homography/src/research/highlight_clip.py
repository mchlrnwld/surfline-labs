"""
Given a video clip and a detection file cut a highlights clip of the most
surfy moments
"""
import numpy as np
from moviepy.editor import VideoFileClip, concatenate_videoclips
from detection import Detection
from surfzone_model import SurfzoneModel


X_SIZE = 1920
Y_SIZE = 1080

# Homography estimated by Ryan Paddle data (see notebook)
HOM = [[-5.97251010e+01,  6.13060731e+01,  9.24442835e+03],
       [-2.08624544e+00,  2.48309236e+00,  3.63199217e+02],
       [1.10514264e-03,  8.78972002e-03,  1.00000000e+00]]

HOM_INV = np.linalg.inv(HOM)

SHORE_POINT = [33.655844, -118.003742]
SHORE_DIR = 220

CLIP_ROOT = "./data/wc-hbpierns.stream.20190902T170123863"

detection = Detection(SHORE_POINT, SHORE_DIR, HOM_INV, (X_SIZE, Y_SIZE))
detection.read_pkl(CLIP_ROOT + ".detections.pkl", 4)
detection.process()
szm = SurfzoneModel(detection)
szm.process()
szm.make_diagnostic_video('./data/wc-hbpierns.stream.20190902T170123863.mp4',
                          './output/model.mp4')

exit()


times = np.unique(detection.data['timestamp'].values)
detection.add_tracking()
detection.make_diagnostic_video('./data/wc-hbpierns.stream.20190902T170123863.mp4',
                                './output/diag.mp4')

peaks = detection.get_surfed_waveiness_peak_time(5)

print(peaks)

clips = []
for peak in peaks:
    clips.append(VideoFileClip(CLIP_ROOT + ".mp4").subclip(peak-5, peak+5))

highlight_clip = concatenate_videoclips(clips)
highlight_clip.write_videofile("./data/highlight.mp4")
