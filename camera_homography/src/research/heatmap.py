"""
Create CSV data for import into kepler.gl to create heatmaps of surfers sitting
and tracks of surfed waves
"""
import numpy as np
import pandas as pd
import pickle
import cv2
import matplotlib.pyplot as plt
import homography

from detection import Detection

X_SIZE = 1920
Y_SIZE = 1080

# Homography estimated by Ryan Paddle data (see notebook)
HOM = [[-5.97251010e+01,  6.13060731e+01,  9.24442835e+03],
       [-2.08624544e+00,  2.48309236e+00,  3.63199217e+02],
       [1.10514264e-03,  8.78972002e-03,  1.00000000e+00]]

inp = [[1, 2, 1], [1, 2, 1]]
res = np.matmul(inp, np.transpose(HOM))
print(res)

HOM_INV = np.linalg.inv(HOM)

SHORE_POINT = [33.655844, -118.003742]
SHORE_DIR = 220

detection = Detection(SHORE_POINT, SHORE_DIR, HOM_INV, (X_SIZE, Y_SIZE))
detection.read_pkl("./data/wc-hbpierns.stream.20190902T170123863.detections.pkl")
detection.process(diagnostic=True)
detection.get_surfed_waveiness()
exit()
detection.get_heatmap(4, filename="./output/heatmap.csv")
detection.save_csv(4, ["center_lat", "center_lon", "shore_distance"],
                   "./output/sitting.csv")
