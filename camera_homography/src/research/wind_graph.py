"""
Graph daily wind speeds as vector onshore / offshore
"""
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib

DATE = "2019-07-13"
UTC_OFFSET = -7

wind = pd.read_csv("./data/trestles_wind_speed.csv",
                   parse_dates=['date'])
wind['date'] = wind['date'] + pd.to_timedelta(UTC_OFFSET, unit='h')
wind = wind.set_index('date')

wind['wind_speed'] = wind['wind_speed_ms'] * 2.237
wind = wind.resample("5T").pad()
wind = wind[DATE]

wind['wind_direction_shore'] = wind['wind_direction'] - 200
wind['wind_direction_shore'] = (wind['wind_direction_shore'] + 180) % 360 - 180

wind['wind_speed_avg'] = wind['wind_speed'].rolling(12).mean()
wind['wind_direction_shore_avg'] = wind['wind_direction_shore'].rolling(6).mean()

print(wind.head)


wind['u'] = wind['wind_speed_avg']  \
            * np.cos(np.radians(wind['wind_direction_shore_avg'])) * -1

wind['v'] = wind['wind_speed_avg']  \
            * np.sin(np.radians(wind['wind_direction_shore_avg'])) * -1

wind['bar_green'] = wind['u'][wind['u'] >= -2]
wind['bar_amber'] = wind['u'][(wind['u'] < -2) & (wind['u'] > -10)]
wind['bar_red'] = wind['u'][wind['u'] < -10]

fig = plt.subplots(figsize=(18, 6))
plt.style.use('fivethirtyeight')
plt.yticks([-3, 5], ['onshore', 'offshore'], rotation='vertical')
plt.tick_params(axis="y", length=0)
plt.bar(x=wind.index, height=wind['bar_green'],
        color='green', width=0.002, label="Offshore")
plt.bar(x=wind.index, height=wind['bar_amber'],
        color='orange', width=0.002, label="Light Onshore")
plt.bar(x=wind.index, height=wind['bar_red'],
        color='#AA0000', width=0.002, label="Strong Onshore")
plt.plot(wind['wind_speed_avg'], label="Wind Speed", linewidth=1, linestyle="--")
plt.legend()
plt.axhline(0, linestyle='--', linewidth=1, color='grey')
#plt.plot(wind['v'])
plt.savefig("test.png")

