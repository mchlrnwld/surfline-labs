import unittest
import numpy as np
from detection import Detection
from surfzone_model import SurfzoneModel


class TestTracking(unittest.TestCase):

    X_SIZE = 1920
    Y_SIZE = 1080

    # Homography estimated by Ryan Paddle data (see notebook)
    HOM = [[-5.97251010e+01, 6.13060731e+01, 9.24442835e+03],
           [-2.08624544e+00, 2.48309236e+00, 3.63199217e+02],
           [1.10514264e-03, 8.78972002e-03, 1.00000000e+00]]

    HOM_INV = np.linalg.inv(HOM)

    SHORE_POINT = [33.655844, -118.003742]
    SHORE_DIR = 220

    CLIP_ROOT = "./data/wc-hbpierns.stream.20190902T170123863"

    def setup_method(self, method):

        self.detection = Detection(self.SHORE_POINT,
                                   self.SHORE_DIR,
                                   self.HOM_INV,
                                   (self.X_SIZE, self.Y_SIZE))

        self.detection.read_pkl(self.CLIP_ROOT + ".detections.pkl")
        self.detection.process()

    def test_create_instance_spt(self):
        """
        Test import  of channel coast 1D spectra files
        """
        times = np.unique(self.detection.data['timestamp'].values)

        idx, dist = self.detection.get_detection_distances(
            times[0], times[10])

        print(idx)

        idx, dist = self.detection.get_detection_distances(
            times[0], times[10], use_hungarian=True)

        print(idx)

    def test_full_tracking(self):

        szm = SurfzoneModel(self.detection)
        szm.process()
        szm.make_diagnostic_video(
            './data/wc-hbpierns.stream.20190902T170123863.mp4',
            './output/model.mp4')
