"""
File takes the path of a local rewind and process with tracking, producing
logs and a diagnostic video
"""

import pickle
from tracking_model import TrackingModel
import cv2
import numpy as np

# We should be passing this via the command line or .env but we'll just set
# it here while we test it
CLIP_ROOT = "./research/data/hbpiernscam.20180409T131100"


def main(filename):

    file = open(CLIP_ROOT + filename, "rb")
    data = pickle.load(file)
    file.close()

    FRAME_SKIP = 2

    cap = cv2.VideoCapture(CLIP_ROOT + ".mp4")

    i = 0
    num_objects = []
    num_dets = []

    frame_stack = []

    while cap.isOpened():

        ret, frame = cap.read()

        if ret and i < 1000:
            if i % FRAME_SKIP == 0:
                frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                frame_stack.append(frame[200:500, 500:600])
            i = i + 1
        else:
            break

    frame_stack = np.array(frame_stack)
    frame_stack = np.mean(frame_stack, axis=1)
    print(frame_stack.shape)
    cv2.imwrite("wave_stack.png", frame_stack)

if __name__ == '__main__':
    main(".detections.pkl")
