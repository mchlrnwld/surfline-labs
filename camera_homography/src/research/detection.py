"""
Wrap the reading of a detection file in an object representation
"""
import pickle
import pandas as pd
import numpy as np
from scipy.signal import find_peaks_cwt
import matplotlib.pyplot as plt
import cv2
from scipy.optimize import linear_sum_assignment


class Detection(object):

    classes = {1: "BeachPerson",
               2: "SUP",
               3: "SurferPaddling",
               4: "SurferSitting",
               5: "SurferSurfing",
               6: "WhiteWater"}

    detection_threshold = 0.26

    def __init__(self, shore_point, shore_dir, homography, img_size):
        """
        Constructor

        :param shore_point: [lat, lon] of a point on the shoreline
        :param shore_dir: direction in degrees of a perpendicular offshore
        direction from the shore_point. Eg. 270 is a beach facing west
        :param homography: A 3x3 numpy array matrix homography converting
        pixel space into lat / lon
        :param img_size: [x, y] size of image in pixels
        """

        self.shore_point = shore_point
        self.shore_dir = shore_dir
        self.homography = homography
        self.x_size = img_size[0]
        self.y_size = img_size[1]
        self.data = None

        # Start time of video in unixtime
        self.start_time = None
        # End time of video in unixtime
        self.end_time = None
        # Duration of video in seconds
        self.duration = None
        # Number of frames per second in the video
        self.frame_rate = None
        # Number of frames per second in the detection set
        self.detection_frame_rate = None
        # Step between frames, eg. int(frame_rate / detection_frame_rate)
        self.frame_step = None

    def get_times(self):
        """
        Returns a list of unique timestamps for this detection data set
        :return: List of unique unix timestamps
        """

        return np.unique(self.data['timestamp'].values)


    def process(self, diagnostic=False):
        """
        Convert the basic detection data into a more complex set including
        the homographic transformations and other meta data

        :param diagnostic: Print a description of the resulting data
        """
        boxes = self.data[["y1", "x1", "y2", "x2"]].values

        # Convert the y1, x1, y2, x2 relative positions to pixel space
        boxes = np.multiply(boxes, [self.y_size,
                                    self.x_size,
                                    self.y_size,
                                    self.x_size])

        # Calculate the centers of the boxes
        box_center_x = boxes[:, 1] + ((boxes[:, 3] - boxes[:, 1]) / 2)
        box_center_y = boxes[:, 0] + ((boxes[:, 2] - boxes[:, 0]) / 2)

        # combine the centers
        boxes = np.insert(boxes, 4, box_center_x, axis=1)
        boxes = np.insert(boxes, 5, box_center_y, axis=1)

        # Calculate homography transformed center points
        centers = self.__apply_homography(boxes[:, 4:6])
        distances = self.__get_distance_from_shore(centers)

        lat1, lon1 = self.__apply_homography(boxes[:, [1, 0]])[0]
        lat2, lon2 = self.__apply_homography(boxes[:, [3, 2]])[0]

        centers = np.split(centers, 2, axis=1)

        self.data['center_x'] = box_center_x
        self.data['center_y'] = box_center_y
        self.data['center_lat'] = centers[0]
        self.data['center_lon'] = centers[1]
        self.data['lat1'] = lat1
        self.data['lon1'] = lon1
        self.data['lat2'] = lat2
        self.data['lon2'] = lon2
        self.data['shore_distance'] = distances[0]
        self.data['cross_shore_distance'] = distances[1]

        # Replace relative locations with pixel locations
        self.data['y1'] = boxes[:, 0]
        self.data['x1'] = boxes[:, 1]
        self.data['y2'] = boxes[:, 2]
        self.data['x2'] = boxes[:, 3]

        if diagnostic:
            with pd.option_context('display.max_rows',
                                   None,
                                   'display.max_columns',
                                   None):
                print(self.data.describe())

    def __apply_homography(self, points):
        """
        Vectorized applications of the homography to an array of points.

        :param points: [num, 2] array of num points
        :return: same shaped array of lat / lon coordinates
        """

        # Calculate homography transformed center points
        points = np.insert(points,
                           2,
                           np.zeros(points.shape[0]) + 1,
                           axis=1)
        points = np.matmul(points, np.transpose(self.homography))
        points[:, 0] = points[:, 0] / points[:, 2]
        points[:, 1] = points[:, 1] / points[:, 2]

        return points[:, 0:2]

    def __get_distance_bearing(self, points):
        """
        Calculate distance and bearing between the shore_point and the
        array of points passed

        :param points: array of [lat, lon]
        :return: array of distances, array of initial bearings
        """

        R = 6372800  # Earth radius in meters
        lat1, lon1 = points[:, 0], points[:, 1]
        lat2, lon2 = self.shore_point

        phi1, phi2 = np.radians(lat1), np.radians(lat2)
        dphi = np.radians(lat2 - lat1)
        dlambda = np.radians(lon2 - lon1)

        a = np.power(np.sin(dphi / 2), 2) + \
            np.cos(phi1) * np.cos(phi2) * np.power(np.sin(dlambda / 2), 2)

        distance = 2 * R * np.arctan2(np.sqrt(a), np.sqrt(1 - a))

        x = np.sin(dlambda) * np.cos(phi2)
        y = np.cos(phi1) * np.sin(phi2) \
            - (np.sin(phi1) * np.cos(phi2) * np.cos(dlambda))

        initial_bearing = np.arctan2(x, y)

        initial_bearing = np.degrees(initial_bearing)
        compass_bearing = (initial_bearing + 360) % 360

        return distance, compass_bearing

    def __get_distance_from_shore(self, points):
        """
        Calculate distance of passed points to the shore_point set assuming
        the beach is perpendicular to shore_dir

        :param points: array of [lat, lon]
        :return: array of distances from shore
        """

        distance, compass_bearing = self.__get_distance_bearing(points)
        angle_diff = self.shore_dir - compass_bearing
        angle_diff = 90 - abs((angle_diff + 180) % 360 - 180)

        distance_shore = np.sin(np.radians(angle_diff)) * distance * -1
        distance_cross_shore = np.cos(np.radians(angle_diff)) * distance
        distance_cross_shore[compass_bearing > self.shore_dir] = \
            distance_cross_shore[compass_bearing > self.shore_dir] * -1

        return distance_shore, distance_cross_shore

    def get_heatmap(self, detection_class, filename=None):
        """
        Create an array of unique center point positions rounded to the nearest
        pixel then converted into lat / lon space

        :param filename: optional filename to save heatmap as csv
        :param detection_class: class integer to filter detections
        :return: [lat, lon, count] heatmap
        """
        centers = self.data[["center_x",
                             "center_y"]][self.data['class']
                                          == detection_class].values
        centers = np.round(centers)
        indexes, counts = np.unique(centers, return_counts=True, axis=0)
        latlon = self.__apply_homography(indexes)
        counts = counts.reshape([counts.shape[0], 1])
        heatmap = np.hstack((latlon, counts))

        if filename:
            np.savetxt(filename, heatmap, delimiter=",")

        return heatmap

    def get_surfed_waveiness(self, window_size=3):
        """
        Return a moving sum timeseries of surfer surfing detections including a
        surfiness index weighted by the detection score and the distance
        from shore in meters.

        N.B. This method biases in favour of multiple surfers surfing
        simultaneously. This might, in some circumstances, not be a good proxy
        for the quality of surfed waves.

        :param window_size: window length in seconds
        :return: pandas.DataFrame of
        """
        waves = self.data[self.data['class'] == 5]
        waves = waves.groupby(["timestamp"]).sum()
        moving_sum = waves.rolling(round(window_size * self.frame_rate),
                                   min_periods=1).sum()
        moving_sum['surfiness'] = \
            moving_sum['shore_distance'] * moving_sum['score']
        return moving_sum

    def get_surfed_waveiness_peak_time(self, count):
        """
        Return the number of seconds since the start of these detections
         of `count` peaks of surfed_waveiness in self.data.

        This can be used to extract `count` peak moments of surfiness in the
        parsed detections.

        :param count: Number of peaks to return
        :return: Array of peak indexes for self.data.
        """
        moving_sum = self.get_surfed_waveiness()
        peaks = find_peaks_cwt(moving_sum['surfiness'], np.arange(1, 300))
        peak_dat = np.column_stack((peaks,
                                    moving_sum['surfiness'].values[peaks]))
        peak_dat = peak_dat[peak_dat[:, 1].argsort()]
        if count > len(peak_dat):
            count = len(peak_dat)
        return np.flip(moving_sum.index[peak_dat[-count:,
                                        0].astype('int')] - self.start_time)

    def __get_time_offset(self, timestamp):
        """
        Return the number of seconds between the start of this detection set
        and the passed timestamp

        :param timestamp: timestamp
        :return: number of seconds since start of video
        """
        return timestamp - self.start_time

    def save_csv(self, detection_class, columns, filename):
        """
        Save detection data as a csv

        :param detection_class: Detection class to filter on
        :param columns: List of columns to export
        :param filename: Filename
        """

        single_class = self.data[self.data['class'] == detection_class]
        single_class.to_csv(filename, columns=columns)

    def read_pkl(self, filename, frame_rate=None):
        """
        Read a detections pickle file

        :param filename: Filename
        :param frame_rate: Desired frame rate or None for video default. Actual
        frame rate will be nearest integer frame rate possible by dividing
        actual video rate by desired rate.
        """
        all_timestamps = []
        all_boxes = []
        all_classes = []
        all_scores = []

        file = open(filename, "rb")
        self.data = pickle.load(file)
        file.close()

        self.start_time = self.data[0]['timestamp']
        self.end_time = self.data[-1]['timestamp']
        self.duration = self.end_time - self.start_time
        self.frame_rate = len(self.data) / self.duration

        if frame_rate:
            self.detection_frame_rate = frame_rate
        else:
            self.detection_frame_rate = self.frame_rate

        for i in range(0, len(self.data)):
            classes = np.array(self.data[i]['detection_classes'])
            boxes = np.array(self.data[i]['detection_boxes'])
            scores = np.array(self.data[i]['detection_scores'])

            boxes = boxes[scores > self.detection_threshold]
            classes = classes[scores > self.detection_threshold]
            scores = scores[scores> self.detection_threshold]
            timestamps = np.zeros(len(classes), dtype="float64") \
                         + self.data[i]['timestamp']
            all_timestamps.append(timestamps)
            all_boxes.append(boxes)
            all_classes.append(classes)
            all_scores.append(scores)
        all_timestamps = np.concatenate(all_timestamps)
        all_boxes = np.concatenate(all_boxes)
        all_classes = np.concatenate(all_classes)
        all_scores = np.concatenate(all_scores)

        data = np.split(all_boxes, 4, axis=1)

        self.data = pd.DataFrame({"timestamp": all_timestamps,
                                  "class": all_classes,
                                  "score": all_scores,
                                  "y1": data[0].reshape(data[0].shape[0]),
                                  "x1": data[1].reshape(data[0].shape[0]),
                                  "y2": data[2].reshape(data[0].shape[0]),
                                  "x2": data[3].reshape(data[0].shape[0]),
                                  "py1": data[0].reshape(data[0].shape[0]) * self.y_size,
                                  "px1": data[1].reshape(data[0].shape[0]) * self.x_size,
                                  "py2": data[2].reshape(data[0].shape[0]) * self.y_size,
                                  "px2": data[3].reshape(data[0].shape[0]) * self.x_size
                                  })

        self.all_timestamps = all_timestamps

        unique_timestamps = np.unique(all_timestamps)
        step = int(self.frame_rate / self.detection_frame_rate)
        self.frame_step = step
        step_timestamps = unique_timestamps[::step]

        self.data = self.data[self.data.timestamp.isin(step_timestamps)]



    def get_frame_detections(self, timestamp, classes):
        """
        Return the detections for a single frame timestamp for the passed list
        of classes

        :param timestamp: frame timestamp
        :param classes: list of classes to match
        :return: dataframe in the format of self.data filtered for the timestamp
        and classes
        """
        data = self.data[self.data['timestamp'] == timestamp]
        data = data[data['class'].isin(classes)]
        return data

    def calc_detection_distances(self, x1, y1, x2, y2):
        """
        Given lists of x,y points for two sets of detections calculate the
        minimum distances between corresponding points and return as a list
        indexed the same as the first set of points.

        :param x1: x positions of detection 1 as a list
        :param y1: y positions of detection 1 as a list
        :param x2: x positions of detection 2
        :param y2: y positions of detection 2
        :return: indexes of nearest detection 2 point to each detection 1 point,
        distance of nearest detection 2 point from each detection 1 point
        point
        """
        x_diff = self.__get_list_difference_matrix(x1, x2)
        y_diff = self.__get_list_difference_matrix(y1, y2)
        distance = np.sqrt(np.power(x_diff, 2) + np.power(y_diff, 2))
        return np.argmin(distance, axis=0), np.min(distance, axis=0)

    def calc_hungarian_distances(self, x1, y1, x2, y2):

        x_diff = self.__get_list_difference_matrix(x1, x2)
        y_diff = self.__get_list_difference_matrix(y1, y2)
        distance = np.sqrt(np.power(x_diff, 2) + np.power(y_diff, 2))

        dim = np.array(distance.shape).max()
        original_dim = len(x1)
        distance_pad = (np.zeros([dim, dim]) * 9999)
        distance_pad[:distance.shape[0], :distance.shape[1]] = distance

        rowi, coli = linear_sum_assignment(distance_pad)

        rowi = rowi[:original_dim]
        coli = coli[:original_dim]
        coli[coli >= len(x2)] = 0

        return coli, distance[rowi, coli]


    @staticmethod
    def __get_list_difference_matrix(list_a, list_b):
        """
        calculate the absolute difference of every element in list_a and list_b
        and return the results as a 2d array of differences with list_a on axis
        0 and list_b on axis 1

        :param list_a: first list of floats to difference
        :param list_b: second list of floats to difference
        :return: 2d array of differences (a x b)
        """
        arr_b = np.array(list_b)
        res = np.vstack([abs(arr_b - i) for i in list_a])
        return res

    def get_detection_distances(self, time_a, time_b, use_hungarian=False):
        frame_a = self.get_frame_detections(time_a, [3, 4, 5])
        frame_b = self.get_frame_detections(time_b, [3, 4, 5])

        if use_hungarian:
            return self.calc_hungarian_distances(frame_a['center_x'].values,
                                                 frame_a['center_y'].values,
                                                 frame_b['center_x'].values,
                                                 frame_b['center_y'].values)
        else:
            return self.calc_detection_distances(frame_b['center_x'].values,
                                                 frame_b['center_y'].values,
                                                 frame_a['center_x'].values,
                                                 frame_a['center_y'].values)

    def get_detection_distance_histogram(self, starttime, endtime, step):
        """

        :param starttime:
        :param endtime:
        :param step:
        :return:
        """

        times = np.unique(self.data['timestamp'].values)
        times = times[times >= starttime][times <= endtime]
        distances = []
        for i in np.arange(0, (len(times) / step) - step, step).astype('int'):
            idx, dist = self.get_detection_distances(times[i], times[i + step])
            distances.append(dist)
        distances = np.concatenate(distances)
        plt.figure(figsize=(12, 8))
        plt.hist(distances, bins=np.arange(0, 200, 2))
        plt.savefig("./output/detection_dist_hist.png")

    def make_diagnostic_frame(self, timestamp, frame, box_size=20):
        det = self.get_frame_detections(timestamp, [3, 4, 5])

        cv2.putText(frame, str(timestamp),
                    (10, 50),
                    cv2.FONT_HERSHEY_SIMPLEX,
                    1,
                    (255, 0, 0),
                    1)

        for index, row in det.iterrows():

            # We can't write the full ID to the image so we'll just write the
            # first two digits
            id = str(row['tracking_index'])[:2]

            # Create a random color for this tracking index
            np.random.seed(int(row['tracking_index']))
            color = np.random.rand(3) * 255
            color = color.astype(int).tolist()

            if row['tracking_index'] > 0:
                #cv2.putText(frame, id,
                #            (int(row['px1']), int(row['py1']) - 4),
                #            cv2.FONT_HERSHEY_SIMPLEX,
                #            0.5,
                #            color,
                #            1)

                cv2.rectangle(frame,
                              (int(row['px1']),
                               int(row['py1'])),
                              (int(row['px2']),
                               int(row['py2'])),
                              color,
                              2)
        return frame

    def make_diagnostic_video(self, input_filename, output_filename):
        times = np.repeat(np.unique(self.data['timestamp'].values),
                          self.frame_step).tolist()

        times = np.unique(self.all_timestamps).tolist()

        cap = cv2.VideoCapture(input_filename)
        out = cv2.VideoWriter(output_filename,
                              cv2.VideoWriter_fourcc(*'MP4V'),
                              20.0,
                              (1920, 1080))
        i = 0
        last_timestamp = None
        while cap.isOpened():
            i = i + 1
            ret, frame = cap.read()

            if ret and len(times) > 0:
                timestamp = times.pop(0)
                if timestamp in self.data['timestamp'].values:
                    frame = self.make_diagnostic_frame(timestamp, frame)
                    out.write(frame)
            else:
                break

        cap.release()
        out.release()
