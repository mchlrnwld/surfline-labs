"""
Read in JSON data and format into pandas fixing as we go
"""

import json
import pandas as pd

detect_files = [{"filename": "track.jloqwl9benuasiiq9zz3ddtn.json",
                 "starttime": 1560817383000},
                {"filename": "track.w9bd14um9d8qbwgbd7v6cve4.json",
                 "starttime": 1560816473999}
               ]

gps_files = [{"filename": "left_pos.json"},
             {"filename": "right_pos.json"}]

gps = []
detect = []


def read_detect(filename, starttime):
    """
    Read a detection track file and create a pandas dataframe

    :param filename: filename
    :param starttime: Video start time in unixtime seconds
    :return: dataframe
    """

    with open('./data/' + filename, 'r') as f:
        detect = json.load(f)

    detect = pd.DataFrame.from_dict(detect['segments'])
    detect['timestamp'] = (detect['timestamp'] * 1000) + int(starttime)

    return detect


def read_gps(filename):
    """
    Read a GPS / Sessions data file and return as a pandas dataframe

    :param filename: name of file
    :return: trackpoints in a dataframe
    """

    with open('./data/' + filename, 'r') as f:
        gps = json.load(f)

    gps = pd.DataFrame.from_dict(gps['positions'])

    return gps


for item in detect_files:
    detect.append(read_detect(item['filename'], item['starttime']))

for item in gps_files:
    gps.append(read_gps(item['filename']))

