# Camera Homography

Investigate methods for constructing a 3x3 homography matrix to convert pixel
space into ocean space.

In conclusion we can create useable homographies from data collected by
paddling in the camera view, annotating the paddler position manually and 
munging with GPS data. Then running the homography generation method in 
`homography.py`

## Building this project.

To build the project:
 
 ```
 cp .env.sample .env
 make build
 ``` 

## Running this project

To run the project:

```
make shell
source activate camera-homography
python homography.py
```
