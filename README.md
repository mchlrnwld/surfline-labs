# Surfline Labs

A single repository for all science / labs code from early stage 
proof of concept / R&D to working prototypes.

## Creating a new project

See `/example_project/README.md` for instructions on cloning a 
simple python boilerplate