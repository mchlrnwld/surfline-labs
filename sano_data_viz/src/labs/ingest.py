import datetime
import time
import calendar
import pandas as pd
import numpy as np
import math


class Forecast(object):

    def __init__(self, msw_spot_id, lotus_path):
        self.msw_spot_id = msw_spot_id
        pass

    def get_spot(self):



def run(file_name, msw_spot_id):
    """
    Read a standard wavewatch partitions file
    """
    with open(file_name) as f:
        lines = f.readlines()

    header_line = lines.pop(0)
    print(header_line)

    j = 0
    surface = []
    idx = []

    while True:
        line = lines[j].strip()
        split = line.split()

        # UTC timestamp as "YYYY-MM-DD HH:MM:SS"
        timestamp = datetime.datetime.strptime(line[0:13],
                                               '%Y%m%d %H:%M')

        timestamp = calendar.timegm(timestamp.utctimetuple())

        part_count = int(split[8])

        # Process partitions
        parts = []
        for k in range(1, part_count + 1):

            data = lines[j + k].split()

            sig_height = float(data[1])
            peak_period = float(data[2])
            mean_direction = float(data[4])
            directional_spread = float(data[5])

        #os = OceanSurface(timestamp,
        #                  combined=combined,
        #                  partitions=parts)

        #surface.append(os)
        idx.append(pd.to_datetime(timestamp, unit='s'))

        j = j + part_count + 1

        if j >= len(lines):
            break

        print(idx)


read_ww_part("../data/0148_CAL_3m.txt")
