import os
import optparse

import numpy as np
import pandas as pd

from datetime import datetime

import wmp_functions
import pytz


if __name__ == "__main__":
    """Generate wind model correction coefficients, based on
    various input parameters below, for a single wind station.

    Required Arguments:
    station_id (string) - the id associated with a wind station.

    start_time (int) - the unix time stamp indicating the start of the
    coefficient generation period.

    end_time (int) - the unix time stamp indicating the end of the
    coefficient generation period.

    start_fhr (int) - the starting forecast hour of the model forecast
    hour range to account for in the coefficient generation period.

    end_fhr (int) - the ending forecast hour of the model forecast
    hour range to account for in the coefficient generation period.

    statistical_correction (int) - the version of statistical correction
    you'd like to apply.

    qc (string) - whether you'd like to apply qc to the station data
    prior to generating correction coefficients.

    Examples to execute for Surfline or MADIS station, respectively:
    python generate_linear_correction_coefficients_single_station.py --station_id="5842041f4e65fad6a77088e8"
    --start_time=1586152800 --end_time=1586322000 --start_fhr=0 --end_fhr=5 --statistical_correction=3 --qc="no"

    Returns:
    An output csv file containing correction coefficients, based on
    various input parameters, for a single wind station.
    """

    parser = optparse.OptionParser()

    # Arguments
    parser.add_option('--station_id',
                      dest='station_id',
                      help='Station ID',
                      type='string')

    parser.add_option('--start_time',
                      dest='start_time',
                      help='Starting unix timestamp',
                      type='int')

    parser.add_option('--end_time',
                      dest='end_time',
                      help='Ending unix timestamp',
                      type='int')

    parser.add_option('--start_fhr',
                      dest='start_fhr',
                      help='Starting model forecast hour',
                      type='int')

    parser.add_option('--end_fhr',
                      dest='end_fhr',
                      help='Ending model forecast hour',
                      type='int')

    parser.add_option('--statistical_correction',
                      dest='statistical_correction',
                      help='The statistical correction version',
                      type='int')

    parser.add_option('--qc',
                      dest='qc',
                      help='QC',
                      type='string')

    (opts, args) = parser.parse_args()

    station_id = opts.station_id
    start_time = opts.start_time
    end_time = opts.end_time
    start_fhr = opts.start_fhr
    end_fhr = opts.end_fhr
    statistical_correction = opts.statistical_correction
    qc = opts.qc

    # Formatted validation time range for print statement.
    start_date = datetime.utcfromtimestamp(start_time).strftime('%Y-%m-%d %H:%M:%S')
    end_date = datetime.utcfromtimestamp(end_time).strftime('%Y-%m-%d %H:%M:%S')

    # Extract useful information about the nearest surf spot
    # associated with the wind station.
    nearest_spot_specs = wmp_functions.get_nearest_surf_spot_specs(station_id)

    # Relevant specifics about the nearest spot tied to the station.
    spot_id = nearest_spot_specs[0]
    spot_distance_km = round(nearest_spot_specs[1], 2)
    prime_wind_direction = nearest_spot_specs[2]
    spot_name = nearest_spot_specs[3]
    spot_timezone = nearest_spot_specs[4]

    # Stored coefficient and bad data (post-qc) file paths and names.
    # The files are simply named based on the spot id. The rows within
    # the coefficient files denote what the passed dataframe is grouped
    # by prior to generating the coefficients.
    coefficient_path = ('/ocean/static/validation/wind_model_performance/coefficients_v' +
                        str(statistical_correction))
    coefficient_csv = station_id + '.csv'
    bad_data_csv = station_id + '_bad_data.csv'
    coefficient_file = os.path.join(coefficient_path, coefficient_csv)
    bad_data_file = os.path.join(coefficient_path, bad_data_csv)

    # The station-model dataframe grouping options to generate the
    # correction coefficients based on.

    # V1 statistical correction grouping variables.
    if statistical_correction == 1:

        # Apply linear correction by model across all wind speeds and directions.
        coefficient_group = ['model_id']

    # V2 statistical correction grouping variables.
    elif statistical_correction == 2:

        # Apply linear correction by model based on wind speed and direction.
        coefficient_group = ['model_id', 'model_wind_condition']

    # V3 statistical correction grouping variables.
    elif statistical_correction == 3:

        # Apply linear correction by model based on wind speed, direction,
        # and hour of day.
        coefficient_group = ['model_id', 'model_wind_condition', 'hour']

    # V4 statistical correction grouping variables.
    elif statistical_correction == 4:

        # Apply linear correction by model based on wind condition
        # and local time of day.
        coefficient_group = ['model_id', 'model_wind_condition', 'time_of_day']

    # Raise error if statistical correction is not one of the above.
    else:

        raise AttributeError('Invalid entry for statistical_correction variable.\n')

    # Log to screen.
    print 'Generating linear correction coefficients for:'
    print 'Station id: ' + station_id
    print 'Nearest Spot: ' + spot_name
    print 'Time Zone: ' + spot_timezone
    print 'Validation period: ' + str(start_date) + ' to ' + str(end_date)
    print 'Model forecast hour range: ' + str(start_fhr) + '-' + str(end_fhr)
    print 'Statistical correction version: ' + str(statistical_correction)

    # Extract station observations for provided station id and time range.
    station_data_df = wmp_functions.get_station_data(station_id, start_time, end_time)

    # Check to ensure station data exists before proceeding.
    if station_data_df.empty:

        print 'No station data found for:'
        print 'Station id: ' + station_id
        print 'Nearest Spot: ' + spot_name
        print 'Validation period: ' + str(start_date) + ' to ' + str(end_date)
        print 'Model forecast hour range: ' + str(start_fhr) + '-' + str(end_fhr)

    else:

        # Check if the station is a Surfline station.
        if len(station_id) > 20:

            # Apply 180 degree wind direction rotation to the observed wind
            # direction for southern hemisphere Surfline stations.
            station_data_df = wmp_functions.rotate_sl_station_wind_direction(station_id, station_data_df)

        # Adjust station wind speeds to a height of 10 meters.
        station_data_df = wmp_functions.apply_wind_height_correction(station_id, station_data_df)

        # Apply qc to station data prior to generating coefficients.
        if qc == 'yes':

            # QC station data and remove bad data.
            qcd_station_data_df = wmp_functions.station_data_qc(station_data_df)

            # Log bad data removed by qc for analysis/plotting.
            qcd_data_removed = station_data_df.index.difference(qcd_station_data_df.index)
            bad_station_data_df = station_data_df[station_data_df.index.isin(qcd_data_removed)]
            bad_station_data_df.to_csv(bad_data_file, index=False)

            # Overwrite original station dataframe with qc'd.
            station_data_df = qcd_station_data_df

        # Check to ensure station data exists before proceeding.
        if station_data_df.empty:

            print 'All station data did not pass QC for:'
            print 'Station id: ' + station_id
            print 'Nearest Spot: ' + spot_name
            print 'Validation period: ' + str(start_date) + ' to ' + str(end_date)
            print 'Model forecast hour range: ' + str(start_fhr) + '-' + str(end_fhr)

        else:

            # Extract observed wind conditions based on our derivation.
            ob_wind_condition = wmp_functions.get_wind_condition(station_data_df['ob_wind_direction'],
                                                                 prime_wind_direction,
                                                                 station_data_df['ob_wind_speed_ms'])

            # Create ob_wind_condition column in station dataframe.
            station_data_df['ob_wind_condition'] = ob_wind_condition.values

            # Extract all model predictions for provided station id, model id,
            # forecast hour, and time range.
            all_model_data_df = wmp_functions.get_model_data(station_id, start_time, end_time,
                                                             start_fhr, end_fhr)

            # Check to ensure model data exists before proceeding.
            if all_model_data_df.empty:

                print 'No model data found for:'
                print 'Station id: ' + station_id
                print 'Nearest Spot: ' + spot_name
                print 'Validation period: ' + str(start_date) + ' to ' + str(end_date)
                print 'Model forecast hour range: ' + str(start_fhr) + '-' + str(end_fhr)

            else:

                # Drop wrf models used for sensitivity testing since they are
                # only available for a small sample period.
                select_model_data_df = (all_model_data_df[~all_model_data_df['model_id']
                                                          .str.contains('socal', na=False)])
                # select_model_data_df = all_model_data_df

                # Ensure an equal sample size between all models considered
                # for raw (uncorrected) model comparisons.
                num_models = select_model_data_df['model_id'].nunique()
                model_data_df_raw = (select_model_data_df.groupby(['model_valid_time', 'forecast_hour'],
                                                                  as_index=False)
                                     .filter(lambda x: x.shape[0] == num_models))

                # Extract modeled wind conditions based on our derivation
                # for raw (uncorrected) model comparisons.
                model_wind_condition_raw = wmp_functions.get_wind_condition(model_data_df_raw['model_wind_direction'],
                                                                            prime_wind_direction,
                                                                            model_data_df_raw['model_wind_speed_ms'])

                # For statistical correction, all models are treated independently
                # i.e. we want to use all model-ob comparisons available to generate
                # the per-model statistical correction coefficients and not limit
                # the number of comparisons by an equal sample size requirement between
                # models.
                model_wind_condition_correction = wmp_functions.get_wind_condition(select_model_data_df['model_wind_direction'],
                                                                                   prime_wind_direction,
                                                                                   select_model_data_df['model_wind_speed_ms'])

                # Create model_wind_condition column in model dataframe used
                # for raw (uncorrected) model comparisons.
                model_data_df_raw['model_wind_condition'] = model_wind_condition_raw.values

                # Create model_wind_condition column in model dataframe used
                # for raw vs. corrected model comparisons.
                select_model_data_df['model_wind_condition'] = model_wind_condition_correction.values

                # Align observed with predicted wind data in time in model dataframe
                # used for raw (uncorrected) model comparisons.
                station_model_df_raw = wmp_functions.align_observed_v_predicted_wind_data(station_data_df,
                                                                                          model_data_df_raw)

                # Check to ensure model-ob matches are found before proceeding.
                if station_model_df_raw.empty:

                    print 'No model-ob matches found for:'
                    print 'Station id: ' + station_id
                    print 'Nearest Spot: ' + spot_name
                    print 'Validation period: ' + str(start_date) + ' to ' + str(end_date)
                    print 'Model forecast hour range: ' + str(start_fhr) + '-' + str(end_fhr)

                else:

                    # Align observed with predicted wind data in time in model dataframe
                    # used for raw vs. corrected model comparisons.
                    station_model_df_correction = wmp_functions.align_observed_v_predicted_wind_data(station_data_df,
                                                                                                     select_model_data_df)

                    # Reset the index of the station-model dataframe used for raw vs.
                    # corrected model comparisons to avoid reindexing on duplicate axis.
                    station_model_df_correction.reset_index(inplace=True)

                    # Define hour column for V3 statistical correction.
                    if statistical_correction == 3:

                        station_model_df_correction['hour'] = station_model_df_correction['datetime'].dt.hour

                    # Generate time of day classification for V4 statistical correction.
                    if statistical_correction == 4:

                        # Convert naive to aware datetime.
                        station_model_df_correction['datetime'] = (station_model_df_correction['datetime']
                                                                   .dt.tz_localize(pytz.utc))

                        # Apply timezone offset to datetime.
                        station_model_df_correction['datetime'] = (station_model_df_correction['datetime']
                                                                   .dt.tz_convert(pytz.timezone(spot_timezone)))

                        # Create local hour column for time of day classification.
                        station_model_df_correction['hour'] = station_model_df_correction['datetime'].dt.hour

                        # Create time of day column.
                        station_model_df_correction['time_of_day'] = wmp_functions.get_time_of_day(station_model_df_correction['hour'])

                    # Apply linear correction by the grouping denoted in the coefficient_group variable.
                    station_model_df_correction = station_model_df_correction.groupby(coefficient_group,
                                                                                      as_index=False)

                    # Create coefficient directory if it doesn't already exist.
                    if not os.path.exists(coefficient_path):
                        os.makedirs(coefficient_path)

                    # Remove stored coefficient file if it already exists for recreation
                    # below.
                    if os.path.exists(coefficient_file):
                        os.remove(coefficient_file)

                    # Generate linear model wind correction coefficients based on the
                    # station-model dataframe grouping and append them to the csv
                    # created above.
                    station_model_df_correction = station_model_df_correction.apply(wmp_functions.generate_linear_correction_coefficients,
                                                                                    station_id, statistical_correction, coefficient_file)
