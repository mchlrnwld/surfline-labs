#!/bin/sh

source /home/sam.wilson/.bashrc
source ${AWS_SCIENCE_BASE}/AWS_PATH_CONFIG

export PATH="/home/sam.wilson/anaconda2/bin:$PATH"
source activate py27_data

### SCRIPT ARGUMENGTS: ###
### VALIDATION ALIAS - AN ALIAS DEFINING THE VALIDATION AND THE DIRECTORY FOR PERFORMANCE RESULTS. ###
### STATION_ID_FILE - A FILE CONTAINING THE STATION ID'S TO VALIDATE THE MODELS AGAINST. ###
### START_TIME - A UNIX TIMESTAMP INDICATING THE START TIME OF THE VALIDATION PERIOD. ###
### END_TIME - A UNIX TIMESTAMP INDICATING THE END TIME OF THE VALIDATION PERIOD. ###
### START_FHR - THE STARTING FORECAST HOUR OF THE FORECAST HOUR RANGE TO VALIDATE. ###
### END_FHR - THE ENDING FORECAST HOUR OF THE FORECAST HOUR RANGE TO VALIDATE. ###

VALIDATION_ALIAS="wind_station_validity"
STATION_ID_FILE="/ocean/static/sl_weather_stations/wx_station_specs.csv"
START_TIME=`date +%s --date "1 month ago"`
END_TIME=`date +%s`
START_FHR="0"
END_FHR="6"

### DIRECTORY FOR OUTPUT ###
OUTPUT_DIR="/ocean/static/validation/wind_model_performance/${VALIDATION_ALIAS}"

### CODE DIRECTORY FOR PROCESSING VIA CRON ###
CODE_DIR="/home/sam.wilson/surfline-labs/wind_model_performance"

### MAKE OUTPUT DIRECTORY IF IT DOESN'T EXIST OR IF IT DOES ###
### START WITH A CLEAN DIRECTORY ###
if [ ! -e "${OUTPUT_DIR}" ]; then
  mkdir ${OUTPUT_DIR}
fi

### LIST OF VALIDATION STATION ID'S ###
STATION_IDS=`cut -d',' -f1 ${STATION_ID_FILE} | tail -n +2`

### LOOP THROUGH ALL STATION IDS FOR VALIDATION ###
for STATION_ID in $STATION_IDS
do

  ### CORE PROCESS TO PRODUCE WIND MODEL PERFORMANCE STATISTICS ###
  python ${CODE_DIR}/wind_station_validity.py \
  --station_id="${STATION_ID}" --start_time=${START_TIME} \
  --end_time=${END_TIME} --start_fhr=${START_FHR} \
  --end_fhr=${END_FHR} --out_dir=${OUTPUT_DIR} 

done

### GENERATE README ###
cat > ${OUTPUT_DIR}/README << EOF
Performance results produced using the following parameters:
Start time: ${START_TIME}
End time: ${END_TIME}
Start fhr: ${START_FHR}
End fhr: ${END_FHR}
EOF

### CREATE FINAL WIND MODEL PERFORMANCE CSV FOR STATIONS THAT PASS ###
### THE VALIDITY CHECK. ###
cat ${OUTPUT_DIR}/*wind_model_performance_summary.csv ${OUTPUT_DIR}/wind_station_validity_override.csv > ${OUTPUT_DIR}/valid_wind_stations_summary.csv
