import os
import optparse

import numpy as np
import pandas as pd

from datetime import datetime

import wmp_functions
import pytz


if __name__ == "__main__":
    """Validate all wind models available against wind station
    observations at a single wind station. Both raw and raw v
    corrected model performance results are produced based on
    various input parameters below.

    Required Arguments:
    station_id (string) - the id associated with a wind station.

    start_time (int) - the unix time stamp indicating the start of the
    validation period.

    end_time (int) - the unix time stamp indicating the end of the
    validation period.

    start_fhr (int) - the starting forecast hour of the model forecast
    hour range to validate.

    end_fhr (int) - the ending forecast hour of the model forecast
    hour range to validate.

    out_dir (string) - the output directory for performance data.

    statistical_correction (int) - the version of statistical correction
    you'd like to apply.

    qc (string) - whether you'd like to apply qc to the station data
    prior to analyzing model performance.

    new_model (string) - if validating against a new model (i.e. in-house wrf),
    not included in the model_list variable below, this is the model identifier
    in the data lake for that model. If not provided, all models included in
    the model_list variable below are validated.

    Examples to execute for Surfline or MADIS station, respectively:
    python compare_model_performance_single_station.py --station_id="5842041f4e65fad6a77088e8"
    --start_time=1586152800 --end_time=1586322000 --start_fhr=0 --end_fhr=5
    --out_dir=/ocean/static/validation/wind_model_performance/tmp
    --statistical_correction=3 --qc="no"

    python compare_model_performance_single_station.py --station_id="E3141"
    --start_time=1586152800 --end_time=1586322000 --start_fhr=0 --end_fhr=5
    --out_dir=/ocean/static/validation/wind_model_performance/tmp
    --statistical_correction=3 --qc="yes"

    python compare_model_performance_single_station.py --station_id="5842041f4e65fad6a77088e8"
    --start_time=1586152800 --end_time=1586322000 --start_fhr=0 --end_fhr=5
    --out_dir=/ocean/static/validation/wind_model_performance/tmp
    --statistical_correction=3 --qc="no" --new_model="socal_1month_v12"

    Returns:
    Multiple csv files containing raw and raw v corrected model results
    at a single wind station.
    """

    parser = optparse.OptionParser()

    # Arguments
    parser.add_option('--station_id',
                      dest='station_id',
                      help='Station ID',
                      type='string')

    parser.add_option('--start_time',
                      dest='start_time',
                      help='Starting unix timestamp',
                      type='int')

    parser.add_option('--end_time',
                      dest='end_time',
                      help='Ending unix timestamp',
                      type='int')

    parser.add_option('--start_fhr',
                      dest='start_fhr',
                      help='Starting model forecast hour',
                      type='int')

    parser.add_option('--end_fhr',
                      dest='end_fhr',
                      help='Ending model forecast hour',
                      type='int')

    parser.add_option('--out_dir',
                      dest='out_dir',
                      help='Output directory',
                      type='string',
                      default='/ocean/static/validation/wind_model_performance/tmp')

    parser.add_option('--statistical_correction',
                      dest='statistical_correction',
                      help='Statistical correction version',
                      type='int')

    parser.add_option('--qc',
                      dest='qc',
                      help='QC',
                      type='string')

    parser.add_option('--new_model',
                      dest='new_model',
                      help='New model identifier',
                      type='string',
                      default='None')

    (opts, args) = parser.parse_args()

    station_id = opts.station_id
    start_time = opts.start_time
    end_time = opts.end_time
    start_fhr = opts.start_fhr
    end_fhr = opts.end_fhr
    out_dir = opts.out_dir
    statistical_correction = opts.statistical_correction
    qc = opts.qc
    new_model = opts.new_model

    # List of all publicly available and old in-house
    # WRF models with data in the data lake to validate
    # unless a valid "new_model" argument is provided.
    model_list = ['gfs0p25', 'hrrrconus', 'namconus',
                  'namhi', 'nampr', 'wrf_aust_nsw_3km',
                  'wrf_aust_qland_3km', 'wrf_aust_south_3km',
                  'wrf_aust_vic_tas_3km', 'wrf_aust_west_3km',
                  'wrf_baja_3km', 'wrf_baja_9km', 'wrf_bali_3km',
                  'wrf_brazil_rio_3km', 'wrf_fiji_3km',
                  'wrf_hawaii_1km', 'wrf_hawaii_3km',
                  'wrf_iberia_6km', 'wrf_med_east_3km',
                  'wrf_med_east_9km', 'wrf_mexico_central_3km_1',
                  'wrf_mexico_central_3km_2', 'wrf_new_zealand_3km',
                  'wrf_new_zealand_9km', 'wrf_puerto_rico_3km',
                  'wrf_sumatra_new_3km', 'wrf_tahiti_1km',
                  'wrf_tahiti_3km', 'wrf_uk_3km', 'wrf_uk_9km']

    # List of only publicly available wind models.
#    model_list = ['gfs0p25', 'hrrrconus', 'namconus', 'namhi', 'nampr']

    # Formatted validation time range for print statement.
    start_date = datetime.utcfromtimestamp(start_time).strftime('%Y-%m-%d %H:%M:%S')
    end_date = datetime.utcfromtimestamp(end_time).strftime('%Y-%m-%d %H:%M:%S')

    # Output csv's containing model performance results and
    # data removed by qc (if the qc parameter was set to 'yes')
    raw_model_performance_csv = os.path.join(out_dir, station_id + '_raw_model_performance.csv')
    corrected_model_performance_csv = os.path.join(out_dir, station_id + '_raw_v_corrected_model_performance.csv')
    station_model_raw_csv = os.path.join(out_dir, station_id + '_raw_model_conditions.csv')
    station_model_corrected_csv = os.path.join(out_dir, station_id + '_raw_v_corrected_model_conditions.csv')
    bad_data_csv = os.path.join(out_dir, station_id + '_bad_data.csv')

    # Extract useful information about the nearest surf spot
    # associated with the wind station.
    nearest_spot_specs = wmp_functions.get_nearest_surf_spot_specs(station_id)

    # Relevant specifics about the nearest spot tied to the station.
    spot_id = nearest_spot_specs[0]
    spot_distance_km = round(nearest_spot_specs[1], 2)
    prime_wind_direction = nearest_spot_specs[2]
    spot_name = nearest_spot_specs[3]
    spot_timezone = nearest_spot_specs[4]

    # Log to screen.
    print 'Analyzing model performance for:'
    print 'Station id: ' + station_id
    print 'Spot name: ' + spot_name
    print 'Spot timezone: ' + spot_timezone
    print 'Validation period: ' + str(start_time) + ' to ' + str(end_time)
    print 'Model forecast hour range: ' + str(start_fhr) + '-' + str(end_fhr)

    # Extract station observations for provided station id and time range.
    station_data_df = wmp_functions.get_station_data(station_id, start_time, end_time)

    # Check to ensure station data exists before proceeding.
    if station_data_df.empty:

        print 'No station data found for:'
        print 'Station id: ' + station_id
        print 'Nearest Spot: ' + spot_name
        print 'Validation period: ' + str(start_date) + ' to ' + str(end_date)
        print 'Model forecast hour range: ' + str(start_fhr) + '-' + str(end_fhr)

    else:

        # Check if the station is a Surfline station.
        #if len(station_id) > 20:

            # Apply 180 degree wind direction rotation to the observed wind
            # direction for southern hemisphere Surfline stations.
            #station_data_df = wmp_functions.rotate_sl_station_wind_direction(station_id, station_data_df)

        # Adjust station wind speeds to a height of 10 meters.
        station_data_df = wmp_functions.apply_wind_height_correction(station_id, station_data_df)

        # Apply qc to station data prior to generating model performance.
        if qc == 'yes':

            # QC station data and remove bad data.
            qcd_station_data_df = wmp_functions.station_data_qc(station_data_df)

            # Log bad data removed by qc for analysis/plotting.
            qcd_data_removed = station_data_df.index.difference(qcd_station_data_df.index)
            bad_station_data_df = station_data_df[station_data_df.index.isin(qcd_data_removed)]
            bad_station_data_df.to_csv(bad_data_csv, index=False)

            # Overwrite original station dataframe with qc'd.
            station_data_df = qcd_station_data_df

        # Check to ensure station data exists before proceeding.
        if station_data_df.empty:

            print 'All station data did not pass QC for:'
            print 'Station id: ' + station_id
            print 'Nearest Spot: ' + spot_name
            print 'Validation period: ' + str(start_date) + ' to ' + str(end_date)
            print 'Model forecast hour range: ' + str(start_fhr) + '-' + str(end_fhr)

        else:

            # Extract observed wind conditions based on our derivation.
            ob_wind_condition = wmp_functions.get_wind_condition(station_data_df['ob_wind_direction'],
                                                                 prime_wind_direction,
                                                                 station_data_df['ob_wind_speed_ms'])

            # Create ob_wind_condition column in station dataframe.
            station_data_df['ob_wind_condition'] = ob_wind_condition.values

            # Extract all model predictions for provided station id, model id,
            # forecast hour, and time range.
            all_model_data_df = wmp_functions.get_model_data(station_id, start_time, end_time,
                                                             start_fhr, end_fhr)

            # Check to ensure model data exists before proceeding.
            if all_model_data_df.empty:

                print 'No model data found for:'
                print 'Station id: ' + station_id
                print 'Nearest Spot: ' + spot_name
                print 'Validation period: ' + str(start_date) + ' to ' + str(end_date)
                print 'Model forecast hour range: ' + str(start_fhr) + '-' + str(end_fhr)

            else:

                # Check to see if the provided new_model argument is a valid
                # model id containing data in the data lake. If so, add it to the
                # model_list variable for inclusion in the validation.
                new_model_check = all_model_data_df[all_model_data_df['model_id'] == new_model]
                if not new_model_check.empty:
                    model_list = model_list + [new_model]
                    model_list = list(dict.fromkeys(model_list))

                # Drop models not included in the model_list at the top. Note this also accounts
                # for the case where a valid new_model model id is passed.
                select_model_data_df = all_model_data_df[all_model_data_df.model_id.isin(model_list)]

                # Ensure an equal sample size between all models considered
                # for raw (uncorrected) model comparisons.
                num_models = select_model_data_df['model_id'].nunique()
                model_data_df_raw = (select_model_data_df.groupby(['model_valid_time', 'forecast_hour'],
                                                                  as_index=False)
                                     .filter(lambda x: x.shape[0] == num_models))

                # Extract modeled wind conditions based on our derivation
                # for raw (uncorrected) model comparisons.
                model_wind_condition_raw = wmp_functions.get_wind_condition(model_data_df_raw['model_wind_direction'],
                                                                            prime_wind_direction,
                                                                            model_data_df_raw['model_wind_speed_ms'])

                # For statistical correction, all models are treated independently
                # i.e. we want to use all model-ob comparisons available to generate
                # the per-model statistical correction coefficients and not limit
                # the number of comparisons by an equal sample size requirement between
                # models.
                model_wind_condition_correction = wmp_functions.get_wind_condition(select_model_data_df['model_wind_direction'],
                                                                                   prime_wind_direction,
                                                                                   select_model_data_df['model_wind_speed_ms'])

                # Create model_wind_condition column in model dataframe used
                # for raw (uncorrected) model comparisons.
                model_data_df_raw['model_wind_condition'] = model_wind_condition_raw.values

                # Create model_wind_condition column in model dataframe used
                # for raw vs. corrected model comparisons.
                select_model_data_df['model_wind_condition'] = model_wind_condition_correction.values

                # Align observed with predicted wind data in time in model dataframe
                # used for raw (uncorrected) model comparisons.
                station_model_df_raw = wmp_functions.align_observed_v_predicted_wind_data(station_data_df,
                                                                                          model_data_df_raw)

                # Check to ensure model-ob matches are found before proceeding.
                if station_model_df_raw.empty:

                    print 'No model-ob matches found for:'
                    print 'Station id: ' + station_id
                    print 'Nearest Spot: ' + spot_name
                    print 'Validation period: ' + str(start_date) + ' to ' + str(end_date)
                    print 'Model forecast hour range: ' + str(start_fhr) + '-' + str(end_fhr)

                else:

                    # Align observed with predicted wind data in time in model dataframe
                    # used for raw vs. corrected model comparisons.
                    station_model_df_correction = wmp_functions.align_observed_v_predicted_wind_data(station_data_df,
                                                                                                     select_model_data_df)

                    # Reset the index of the station-model dataframe used for raw vs.
                    # corrected model comparisons to avoid reindexing on duplicate axis.
                    station_model_df_correction.reset_index(inplace=True)

                    # V1 statistical correction grouping variables.
                    if statistical_correction == 1:

                        # Apply linear correction by model across all wind conditions.
                        coefficient_group = ['model_id']

                    # V2 statistical correction grouping variables.
                    elif statistical_correction == 2:

                        # Apply linear correction by model based on wind condition.
                        coefficient_group = ['model_id', 'model_wind_condition']

                    # V3 statistical correction grouping variables.
                    elif statistical_correction == 3:

                        # Define hour column for V3 statistical correction.
                        station_model_df_correction['hour'] = station_model_df_correction['datetime'].dt.hour

                        # Apply linear correction by model based on wind condition
                        # and hour of day.
                        coefficient_group = ['model_id', 'model_wind_condition', 'hour']

                    # V4 statistical correction grouping variables.
                    elif statistical_correction == 4:

                        # Convert naive to aware datetime.
                        station_model_df_correction['datetime'] = (station_model_df_correction['datetime']
                                                                   .dt.tz_localize(pytz.utc))

                        # Apply timezone offset to datetime.
                        station_model_df_correction['datetime'] = (station_model_df_correction['datetime']
                                                                   .dt.tz_convert(pytz.timezone(spot_timezone)))

                        # Create local hour column for time of day classification.
                        station_model_df_correction['hour'] = station_model_df_correction['datetime'].dt.hour

                        # Create time of day column.
                        station_model_df_correction['time_of_day'] = wmp_functions.get_time_of_day(station_model_df_correction['hour'])

                        # Apply linear correction by model based on wind speed, direction,
                        # and local time of day.
                        coefficient_group = ['model_id', 'model_wind_condition', 'time_of_day']

                    # Raise error if statistical correction is not one of the above.
                    else:

                        raise AttributeError('Invalid entry for statistical_correction variable.\n')

                    # Group the station-model dataframe by the desired parameters for
                    # applying the correction coefficients, which are based on the statistical
                    # correction version, to the results.
                    station_model_df_correction = station_model_df_correction.groupby(coefficient_group,
                                                                                      as_index=False)

                    # Apply linear correction specified via the statistical correction
                    # version to the model data.
                    station_model_df_correction = station_model_df_correction.apply(wmp_functions.linear_model_wind_correction,
                                                                                    station_id, statistical_correction)

                    # Drop the multi-index in the dataframe used for raw vs. corrected
                    # model comparisons.
                    station_model_df_correction.reset_index(level=0, drop=True, inplace=True)

                    # Extract modeled wind conditions using our derivation but on the
                    # corrected model wind data.
                    model_wind_condition_correction = wmp_functions.get_wind_condition(station_model_df_correction['model_wind_direction_corrected'],
                                                                                       prime_wind_direction,
                                                                                       station_model_df_correction['model_wind_speed_ms_corrected'])

                    # Create model_wind_condition_corrected column in the station-model dataframe
                    # used for raw vs. corrected model comparisons.
                    station_model_df_correction['model_wind_condition_corrected'] = model_wind_condition_correction.values

                    # Group the station-model dataframe used for raw (uncorrected) model comparisons
                    # by the model id to generate the raw v raw model performance dataframe based on that.
                    model_performance_df_raw = station_model_df_raw.groupby('model_id', as_index=False)

                    # Group the station-model dataframe used for raw (uncorrected) vs. corrected
                    # model comparisons by the model id to generate the raw v corrected model
                    # performance dataframe based on that.
                    model_performance_df_correction = station_model_df_correction.groupby('model_id', as_index=False)

                    # Generate the model performance dataframe for raw (uncorrected) model comparisons
                    # using the aligned station-model dataframe grouped by the model id.
                    model_performance_df_raw = model_performance_df_raw.apply(wmp_functions.generate_model_performance_df_raw)

                    # Generate the model performance dataframe for raw (uncorrected) vs corrected
                    # model comparisons using the aligned station-model dataframe grouped by the model id.
                    model_performance_df_correction = model_performance_df_correction.apply(wmp_functions.generate_model_performance_df_correction)

                    # Drop the multi-index created in the model performance dataframe for
                    # raw (uncorrected) model comparisons.
                    model_performance_df_raw.reset_index(level=0, drop=True, inplace=True)

                    # Drop the multi-index created in the model performance dataframe for
                    # raw (uncorrected) vs corrected model comparisons.
                    model_performance_df_correction.reset_index(level=0, drop=True, inplace=True)

                    # Add useful spot specifics to model performance dataframe.
                    model_performance_df_raw['spot_name'] = spot_name
                    model_performance_df_raw['spot_distance_km'] = spot_distance_km
                    model_performance_df_raw['station_id'] = station_id
                    model_performance_df_raw['model_id'] = model_performance_df_raw.index
                    model_performance_df_correction['spot_name'] = spot_name
                    model_performance_df_correction['spot_distance_km'] = spot_distance_km
                    model_performance_df_correction['station_id'] = station_id
                    model_performance_df_correction['model_id'] = model_performance_df_correction.index

                    # Order model performance dataframe columns.
                    model_performance_df_raw = model_performance_df_raw[['spot_name',
                                                                         'spot_distance_km',
                                                                         'station_id',
                                                                         'model_id',
                                                                         'f1_score',
                                                                         'num_obs']]
                    model_performance_df_correction = model_performance_df_correction[['spot_name',
                                                                                       'spot_distance_km',
                                                                                       'station_id',
                                                                                       'model_id',
                                                                                       'f1_score',
                                                                                       'f1_score_corrected',
                                                                                       'num_obs']]

                    # Output model performance results to csv for analysis.
                    model_performance_df_raw.to_csv(raw_model_performance_csv, index=False)
                    model_performance_df_correction.to_csv(corrected_model_performance_csv, index=False)
                    station_model_df_raw.to_csv(station_model_raw_csv, index=False)
                    station_model_df_correction.to_csv(station_model_corrected_csv, index=False)
