import os
import csv

import numpy as np
import pandas as pd

import athena_access
from datetime import datetime
from sklearn.metrics import f1_score


def df_valid_time_2_datetime_index(df):
    """Update and output the passed dataframe with a
    datetime object, representing the valid time of the
    data, as the index.

    Required Arguments:
    df (dataframe) - dataframe containing a "valid_time" column
    in unix format.

    Example to execute:
    wmp_functions.df_valid_time_2_datetime_index(df)

    Returns:
    Passed pandas dataframe with datetime as index.
    """

    # Find column with "valid_time" in the name.
    [valid_time_col] = [col for col in df.columns if 'valid_time' in col]

    # Set dataframe index to datetime object based on data valid time.
    df.set_index(pd.to_datetime(df[valid_time_col], unit='s'),
                 inplace=True, drop=False)

    # Set dataframe index name to datetime.
    df.index.names = ['datetime']

    # Return passed dataframe with index set to datetime
    # object based on data valid time.
    return df


def get_circ_mean(directions):
    """Calculate the mean of directions passed in degrees.

    Required Arguments:
    directions (list) - a list of directions in degrees i.e. [320, 10].

    Example to execute:
    wmp_functions.get_circ_mean([320, 10])

    Returns:
    The circular mean of the directions passed in degrees i.e.
    345.0 for the above example.
    """

    # Convert directions passed in degrees to radians.
    rads = np.deg2rad(directions)

    # Calculate average sin and cosine of directions.
    av_sin = np.mean(np.sin(rads))
    av_cos = np.mean(np.cos(rads))

    # Take the arctangent of the average sin and cosine.
    ang_rad = np.arctan2(av_sin, av_cos)

    # Convert back to degrees and take the modulus.
    ang_deg = np.rad2deg(ang_rad)
    ang_deg = np.mod(ang_deg, 360.)

    # Return average of the provided directions in degrees.
    return ang_deg


def get_circ_variance(directions_df):
    """Calculate the variance of directions passed in degrees.

    Required Arguments:
    directions (dataframe) - a dataframe of directions in degrees.

    Example to execute:
    wmp_functions.get_circ_variance(directions_df)

    Returns:
    The circular variance of the directions passed in degrees.
    """

    # Convert directions passed in degrees to radians and remove nan.
    rads = np.deg2rad(directions_df)
    rads = rads[~np.isnan(rads)]

    # Array length
    length = np.array(rads).size

    # Calculate circular variance using wind components.
    S = np.sum(np.sin(np.array(rads)))
    C = np.sum(np.cos(np.array(rads)))
    R = np.sqrt(S**2 + C**2)
    R_avg = R/length
    V = 1 - R_avg

    # Return circular variance.
    return V


def haversine(lon1, lat1, lon2, lat2):
    """Calculate the great circle distance between two points
    on the earth (specified in decimal degrees).

    Required Arguments:
    lon1, lat1, lon2, lat2 (floats) - the longitude and latitude of
    two points on the earth.

    Example to execute:
    wmp_functions.haversine(-130.2, 20.0, -120.3, 17.0)

    Returns:
    The distance in kilometers between the two points i.e.
    20.0N, -130.2W and 17.0N, -120.3W.
    """

    # Convert decimal degrees to Radians:
    lon1 = np.radians(lon1.values)
    lat1 = np.radians(lat1.values)
    lon2 = np.radians(lon2.values)
    lat2 = np.radians(lat2.values)

    # Implementing Haversine Formula:
    dlon = np.subtract(lon2, lon1)
    dlat = np.subtract(lat2, lat1)

    a = np.add(np.power(np.sin(np.divide(dlat, 2)), 2),
               np.multiply(np.cos(lat1),
                           np.multiply(np.cos(lat2),
                                       np.power(np.sin(np.divide(dlon, 2)), 2))))

    c = np.multiply(2, np.arcsin(np.sqrt(a)))
    r = 6371

    # Return distance in kilometers between the two points.
    return c*r


def get_wind_height_correction(ref_height, roughness_length=0.143):
    """Calculate a 10 meter wind speed correction coefficient
    based on the height of the known wind speed and a constant
    roughness length.

    Required Arguments:
    ref_height (float) - the height (in meters) of the known
    wind speed.

    roughness_length (float) - a constant representing the terrain
    of the surface where the measurements are taken.

    Example to execute:
    wmp_functions.get_wind_height_correction(3.5)

    Returns:
    A single value (float) representing the coefficient to multiply
    the original wind speed by to extrapolate it to the standard
    10 meter height.
    """

    correction_coeff = (np.log(10 / roughness_length) /
                        np.log(ref_height / roughness_length))

    return correction_coeff


def apply_wind_height_correction(station_id, station_data_df):
    """Apply a 10 meter wind speed correction coefficient
    to the wind speeds provided in the wind station dataframe.

    Required Arguments:
    station_id (string) - the id of a wind station.

    station_data_df (dataframe) - a dataframe containing wind data
    associated with the provided station id.

    Example to execute:
    wmp_functions.apply_wind_height_correction("E3141", station_data_df)

    Returns:
    The original station wind dataframe but with speeds corrected
    using a correction coefficient to adjust the height to 10m.
    """

    # Extract station anemometer height - only available for Surfline
    # stations, otherwise assumed to be 10 meters.
    try:
        station_height = get_sl_anemometer_height(station_id)
    except IndexError:
        station_height = 10.0
        pass

    # Apply wind speed correction to a height of 10 meters.
    if station_height != 10.0:
        # Extract wind speed correction coefficient based on anemometer height.
        wind_correction_coeff = get_wind_height_correction(station_height)

        # Apply wind speed correction coefficient to station data.
        station_data_df['ob_wind_speed_ms'] = wind_correction_coeff*station_data_df['ob_wind_speed_ms']
        station_data_df['ob_wind_gust_ms'] = wind_correction_coeff*station_data_df['ob_wind_gust_ms']

    # Return the original station wind dataframe but with speeds
    # corrected to 10m height.
    return station_data_df


def get_wind_condition(wind_direction, prime_wind_direction,
                       wind_speed_ms):
    """Calculate the wind condition, based on a derivation
    found to best fit wind conditions with human surf reports,
    based on the provided wind direction, wind speed, and prime
    wind direction of a spot.

    Required Arguments:
    wind_direction (dataframe) - a dataframe of wind directions
    provided in degrees.

    prime_wind_direction (float) - a single value representing
    the prime wind direction of a surf spot.

    wind_speed_ms (dataframe) - a dataframe of wind speeds
    provided in meters per second.

    Example to execute:
    wmp_functions.get_wind_condition(df['wind_direction'], 42, df['wind_speed_ms'])

    Returns:
    A dataframe containing the derived wind condition associated
    with the provided dataframes wind directions, wind speeds,
    and prime wind direction.
    """

    # The angle difference between the wind directions provided
    # in the dataframe and the prime wind direction of the surf spot.
    direction_diff = 180 - abs(abs(wind_direction -
                                   prime_wind_direction) - 180)

    # Wind speed (meters per second) and directional (degrees)
    # thresholds. Wind speeds defined using the Beaufort scale
    # for reference.
    calm, light, moderate = 2.06, 3.60, 8.23
    offshore, sideshore, onshore = 45, 135, 225

    # Construct dataframe of prime wind direction differences
    # alongside wind speeds to perform wind condition classification on.
    df = pd.DataFrame({'direction_diff': list(direction_diff),
                       'wind_speed_ms': list(wind_speed_ms)})

    # Derived wind conditions based on the speed and direction of the wind
    # with respect to the prime wind direction of a surf spot.
    conditions = [
        (df['direction_diff'] < offshore) & (df['wind_speed_ms'] >= calm) &
        (df['wind_speed_ms'] <= light),
        (df['direction_diff'] < offshore) & (df['wind_speed_ms'] > light) &
        (df['wind_speed_ms'] <= moderate),
        (df['direction_diff'] < offshore) & (df['wind_speed_ms'] > moderate),
        (df['direction_diff'] >= offshore) & (df['direction_diff'] < sideshore) &
        (df['wind_speed_ms'] >= calm) & (df['wind_speed_ms'] <= light),
        (df['direction_diff'] >= offshore) & (df['direction_diff'] < sideshore) &
        (df['wind_speed_ms'] > light) & (df['wind_speed_ms'] <= moderate),
        (df['direction_diff'] >= offshore) & (df['direction_diff'] < sideshore) &
        (df['wind_speed_ms'] > moderate),
        (df['direction_diff'] >= sideshore) & (df['direction_diff'] < onshore) &
        (df['wind_speed_ms'] >= calm) & (df['wind_speed_ms'] <= light),
        (df['direction_diff'] >= sideshore) & (df['direction_diff'] < onshore) &
        (df['wind_speed_ms'] > light) & (df['wind_speed_ms'] <= moderate),
        (df['direction_diff'] >= sideshore) & (df['direction_diff'] < onshore) &
        (df['wind_speed_ms'] > moderate),
        (df['wind_speed_ms'] < calm)
    ]

    # Wind conditions associated with each derivation above.
    choices = ['light offshore', 'moderate offshore',
               'strong offshore', 'light sideshore',
               'moderate sideshore', 'strong sideshore',
               'light onshore', 'moderate onshore',
               'strong onshore', 'calm']

    # Apply wind condition logic to each row of the dataframe
    # and return a dataframe of the resulting wind conditions.
    df['wind_condition'] = np.select(conditions, choices)
    df = df['wind_condition']

    # Return dataframe of derived wind conditions based on the passed
    # wind speed, wind direction, and prime wind direction.
    return df


def get_time_of_day(local_hour_df):
    """Classify time_of_day using the hour
    column in the passed dataframe.

    Required Arguments:
    local_hour_df (dataframe) - a dataframe containing a local
    hour column.

    Example to execute:
    wmp_functions.get_time_of_day(df['hour'])

    Returns:
    A dataframe containing the derived time of day.
    """

    # Derived time of day based on the local hour.
    conditions = [
        (local_hour_df >= 5) & (local_hour_df <= 8),
        (local_hour_df >= 9) & (local_hour_df <= 12),
        (local_hour_df >= 13) & (local_hour_df <= 16),
        (local_hour_df >= 17) & (local_hour_df <= 20),
        (local_hour_df >= 21) & (local_hour_df <= 23) | (local_hour_df == 0),
        (local_hour_df >= 1) & (local_hour_df <= 4)
    ]

    # Time of day associated with each derivation above.
    choices = ['early morning', 'late morning',
               'early afternoon', 'late afternoon',
               'dusk', 'pre dawn']

    # Apply time of day logic to each row of the dataframe
    # and return a list of the resulting time of day.
    local_hour_df = pd.DataFrame(np.select(conditions, choices),
                                 columns=['time_of_day'])

    # Return dataframe of derived time of day based on the passed
    # local hour.
    return local_hour_df


def get_station_data(station_id, start_time, end_time):
    """Extract wind station data from the data lake for the
    provided station id and time range.

    Required Arguments:
    station_id (string) - a wind station id.

    start_time (integer) - a unix time stamp representing
    the start date of the data to be extracted.

    end_time (integer) - a unix time stamp representing
    the end date of the data to be extracted.

    Examples to execute:
    wmp_functions.get_station_data("5842041f4e65fad6a77088e8", 1577883600, 1582659781)
    wmp_functions.get_station_data("E3141", 1577883600, 1582659781)

    Returns:
    A dataframe containing wind observations for the provided
    station and time range.
    """

    # Connect to athena in the surfline-prod s3 account.
    connection = athena_access.connect_to_athena('surfline-prod')

    # Generate daterange list containing all years and months between
    # the start and end times provided - for faster/cheaper data
    # extraction using the partitions.
    start_date = datetime.fromtimestamp(start_time).strftime("%Y%m")
    end_date = datetime.fromtimestamp(end_time).strftime("%Y%m")
    start_date = datetime.strptime(start_date, "%Y%m").date()
    end_date = datetime.strptime(end_date, "%Y%m").date()

    if start_date == end_date:
        daterange = [start_date]
    else:
        daterange = pd.date_range(start_date, end_date, freq='1M')
        daterange = daterange.union([daterange[-1] + 1])

    # Year and month list passed to SQL query to get data from
    # only those partitions.
    daterange = [d.strftime('%Y%m') for d in daterange]

    # SQL query to extract valid time and wind station information
    # measured by the surfline-owned wind stations from the
    # sl_weather_stations data lake at the spot id and time range
    # provided.
    df = pd.read_sql("""SELECT valid_time AS ob_valid_time,
                     spot_id AS ob_station_id,
                     wind_speed_ms AS ob_wind_speed_ms,
                     wind_gust_ms AS ob_wind_gust_ms,
                     wind_direction AS ob_wind_direction
                     FROM
                     wt_data_lake_prod.wt_dl_sl_weather_stations
                     WHERE
                     partition_0 = %(station_id)s
                     AND
                     CONCAT(partition_1, partition_2) IN %(daterange)s
                     AND
                     valid_time BETWEEN %(start_time)s AND %(end_time)s
                     UNION ALL
                     SELECT
                     valid_time AS ob_valid_time,
                     station_id AS ob_station_id,
                     wind_speed_ms AS ob_wind_speed_ms,
                     wind_gust_ms AS ob_wind_gust_ms,
                     wind_direction AS ob_wind_direction
                     FROM
                     wt_data_lake_prod.wt_dl_madis_reliable_coastal
                     WHERE
                     partition_0 = %(station_id)s
                     AND
                     valid_time BETWEEN %(start_time)s AND %(end_time)s
                     ORDER BY ob_valid_time
                     """, connection, params={"station_id": station_id,
                                              "daterange": daterange,
                                              "start_time": start_time,
                                              "end_time": end_time})

    # Drop rows with null wind speed.
    df = df[df['ob_wind_speed_ms'].notna()]

    # When the station wind speed is zero, the wind direction is null.
    # Fill null directions with zero where wind data is valid.
    df['ob_wind_direction'].fillna(0.0, inplace=True)

    # Set valid_time field as dataframe datetime index.
    df = df_valid_time_2_datetime_index(df)

    # Return a dataframe of the wind station data based on the passed
    # station id and time range.
    return df


def rotate_sl_station_wind_direction(station_id, station_data_df):
    """Rotate the wind station wind direction 180 degrees
    in the passed dataframe.

    Required Arguments:
    station_id (string) - the station id.

    station_data_df (dataframe) - a dataframe of wind observations
    from a single Surfline wind station.

    Example to execute:
    wmp_functions.rotate_sl_station_wind_direction('5842041f4e65fad6a7708c2e', station_data_df)

    Returns:
    The passed dataframe but with the wind direction
    rotated 180 degrees.
    """

    # Backend file containing Surfline wind station specifics.
    sl_station_specs_file = '/ocean/static/sl_weather_stations/wx_station_specs.csv'

    # Read the specs file and extract only specifics about the station.
    station_specs_df = pd.read_csv(sl_station_specs_file)
    station_specs = station_specs_df[station_specs_df['spotid'] == station_id]

    # Extract station specifics needed to determine if a rotation is required.
    # Since the southern hemi stations are the only ones where a rotation is
    # needed, we simply need the latitude of the station.
    station_lat = station_specs['latitude'].values[0]
    station_lon = station_specs['longitude'].values[0]

    # If a southern hemisphere station, rotate the wind.
    if station_lat < 0.0:

        # Rotate ob_wind_direction by 180 degrees.
        station_data_df['ob_wind_direction'] = [wd-180 if wd >= 180
                                                else
                                                wd-180+360
                                                for wd in
                                                station_data_df['ob_wind_direction']]

    # Return passed dataframe but with the observed wind direction
    # rotated 180 degrees if a southern hemisphere station.
    return station_data_df


def get_sl_anemometer_height(station_id):
    """Extract the anemometer height of the provided
    station id.

    Required Arguments:
    station_id (string) - the id associated with a wind station.

    Example to execute:
    wmp_functions.get_anemometer_height("5842041f4e65fad6a77088e8")

    Returns:
    The height of the wind stations anemometer.
    """

    # Routinely updated backend file containing information
    # about surfline wind stations.
    sl_weather_stations_file = '/ocean/static/sl_weather_stations/wx_station_specs.csv'

    # Read Surfline station specs into dataframe.
    sl_weather_stations_df = pd.read_csv(sl_weather_stations_file)

    # Extract anemometer height associated with the station_id.
    station_height = (sl_weather_stations_df[sl_weather_stations_df['spotid'] == station_id]
                      ['anemometer_height'].values[0])

    return station_height


def get_model_data(station_id, start_time, end_time,
                   start_forecast_hour, end_forecast_hour):
    """Extract predicted wind data for a wind station from the data lake.

    Required Arguments:
    station_id (string) - the id associated with a wind station.

    start_time (integer) - a unix time stamp representing the start date of
    the data to be extracted.

    end_time (integer) - a unix time stamp representing the end date of
    the data to be extracted.

    start_forecast_hour (integer) - the starting model forecast hour.

    end_forecast_hour (integer) - the ending model forecast hour.

    Example to execute:
    wmp_functions.get_model_data("5842041f4e65fad6a77088e8", 1577883600, 1582659781, 0, 12)

    Returns:
    A dataframe containing the wind predictions from all available weather models
    at the provided station, time and forecast hour range.
    """

    # Connect to athena in the surfline-prod s3 account.
    connection = athena_access.connect_to_athena('surfline-prod')

    # SQL query to extract model data for the provided station id,
    # model id, and forecast hour.
    df = pd.read_sql("""SELECT valid_time AS model_valid_time,
                     model_id,
                     station_id AS model_station_id,
                     forecast_hour,
                     sqrt(pow(wind_u_component_ms, 2) + pow(wind_v_component_ms, 2)) AS model_wind_speed_ms,
                     180 + atan2(wind_u_component_ms, wind_v_component_ms) * (180/3.14) AS model_wind_direction
                     FROM
                     wt_data_lake_prod.wt_dl_wind_validation
                     WHERE
                     partition_1 = %(station_id)s
                     AND
                     forecast_hour BETWEEN %(start_forecast_hour)s AND %(end_forecast_hour)s
                     AND
                     valid_time BETWEEN %(start_time)s AND %(end_time)s
                     """, connection, params={"station_id": station_id,
                                              "start_forecast_hour": start_forecast_hour,
                                              "end_forecast_hour": end_forecast_hour,
                                              "start_time": start_time,
                                              "end_time": end_time})

    # Set valid_time field as dataframe datetime index.
    df = df_valid_time_2_datetime_index(df)

    # Return a dataframe of the wind model data based on the passed
    # station id, model id, forecast hour range, and time range.
    return df


def align_observed_v_predicted_wind_data(observed_wind_df, predicted_wind_df):
    """Align the observed and predicted wind data in time for comparison.

    Required Arguments:
    observed_wind_df (dataframe) - a dataframe containing wind observations.

    predicted_wind_df (dataframe) - a dataframe containing model wind predictions.

    Example to execute:
    wmp_functions.align_observed_v_predicted_wind_data(observed_wind_df, predicted_wind_df)

    Returns:
    A dataframe containing the wind predictions from the provided
    weather model and forecast hour alongside the wind observations
    at the provided station and time range.
    """

    # Set station data index to observation valid time rounded to
    # the nearest 10 minutes for matching with the model data.
    observed_wind_df.index = observed_wind_df.index.round('10min')

    # Join station and model data on matching datetime index.
    station_model_df = pd.merge(observed_wind_df, predicted_wind_df,
                                left_index=True, right_index=True,
                                how='right')

    # Find columns with "valid_time" in the name and drop if null.
    valid_time_cols = [col for col in station_model_df.columns if 'valid_time' in col]
    station_model_df.dropna(axis=0, subset=valid_time_cols,
                            inplace=True)

    # Return a dataframe of the observed v predicted wind data
    # aligned in time.
    return station_model_df


def get_f1_score(observed_df, predicted_df):
    """Calculate the weighted f1 score using the provided observations
    alongside the predictions.

    Required Arguments:
    observed_df (dataframe) - a dataframe containing observations
    broken down into classes.

    predicted_df (dataframe) - a dataframe containing predictions
    broken down into the same classes as the observations.

    Example to execute:
    wmp_functions.get_f1_score(df['ob_wind_condition'], df['model_wind_condition'])

    Returns:
    A single floating point value representing the f1 score
    calculated by comparing the observed with the predicted classes.
    """

    # Calculate weighted f1 score.
    # Warning means:
    # some labels in y_true don't appear in y_pred.
    # Way to fix if necessary:
    # https://stackoverflow.com/questions/43162506/undefinedmetricwarning-f-score-is-ill-defined-and-being-set-to-0-0-in-labels-wi
    weighted_f1_score = f1_score(observed_df,
                                 predicted_df,
                                 average='weighted')

    # Return the weighted f1 score of the observed v predicted data.
    return weighted_f1_score


def get_models_available(station_id, forecast_hour, start_time, end_time):
    """Get the list of wind models available for the provided wind station.

    Required Arguments:
    station_id (string) - the id associated with a wind station.

    forecast_hour (string) - the model forecast hour.

    start_time (integer) - a unix time stamp representing the
    start date of the data to be extracted.

    end_time (integer) - a unix time stamp representing the
    end date of the data to be extracted.

    Example to execute:
    wmp_functions.get_models_available("5842041f4e65fad6a77088e8", "24", 1577883600, 1582659781)

    Returns:
    A dataframe containing the list of available wind models for the
    provided station id, forecast hour, and time range.
    """

    # Connect to athena in the surfline-prod s3 account.
    connection = athena_access.connect_to_athena('surfline-prod')

    # SQL query to extract available wind models for the provided station id,
    # forecast hour, and time range.
    df = pd.read_sql("""SELECT DISTINCT partition_0 AS model_id
                     FROM
                     wt_data_lake_prod.wt_dl_wind_validation
                     WHERE
                     partition_1 = %(station_id)s
                     AND
                     partition_2 = %(forecast_hour)s
                     AND
                     valid_time BETWEEN %(start_time)s AND %(end_time)s
                     """, connection, params={"station_id": station_id,
                                              "forecast_hour": forecast_hour,
                                              "start_time": start_time,
                                              "end_time": end_time})

    # Return a dataframe of the model id's available for the provided station,
    # model forecast hour, and time range.
    return df['model_id']


def get_madis_station_lat_lon(station_id):
    """Get the latitude and longitude of the provided MADIS station id.

    Required Arguments:
    station_id (string) - the id associated with a MADIS wind station.

    Example to execute:
    wmp_functions.get_madis_station_lat_lon("E3141")

    Returns:
    A dataframe containing the latitude and longitude of the provided
    MADIS station id.
    """

    # Query the routinely updated madis locations file for the provided station
    # id and extract the lat/lon - much faster than extracting from the data lake.
    madis_locations_file = '/ocean/transient/madis/madis_validation_station_metadata.csv'
    df = pd.read_csv(madis_locations_file)
    df = df[df['station_id'] == station_id]

    # Return a dataframe containing the latitude and longitude
    # of the MADIS station.
    return df[['latitude', 'longitude']]


def get_nearest_surf_spot_specs(station_id):
    """Get specifics about the nearest surf spot associated with the
    provided station id.

    Required Arguments:
    station_id (string) - the id associated with a wind station.

    Example to execute:
    wmp_functions.get_nearest_surf_spot_specs("E3141")

    Returns:
    The surf spot id (string), distance in km, spot name, prime
    wind direction, and timezone of the nearest surf spot to the
    station provided.
    """

    # Routinely updated file containing information about all surf spots
    # for use in finding the nearest spot to a MADIS station and in
    # constructing useful output.
    spot_cms_file = '/ocean/transient/data_lake/spot_specs/surfline_spot_latlon_info.csv'

    # Read information about all surf spots into dataframe.
    all_spot_cms_specs = pd.read_csv(spot_cms_file)

    # Extract the surf spot id associated with the wind station.
    # If a surfline-owned station, the spot id is the station id.
    if len(station_id) > 20:

        spot_id = station_id
        min_distance = 0.0

    # If a MADIS station, the nearest spot id must be extracted,
    # which is found using the lat/lon of the station.
    else:

        # Extract latitude and longitude of the MADIS station.
        madis_location = get_madis_station_lat_lon(station_id)
        madis_latitude = madis_location['latitude']
        madis_longitude = madis_location['longitude']

        # Find distance from the MADIS station to all surf spots.
        distance = haversine(madis_longitude,
                             madis_latitude,
                             all_spot_cms_specs['longitude_map'],
                             all_spot_cms_specs['latitude_map'])

        # Find the record with the minimum distance.
        min_distance, idx = min((min_distance, idx) for
                                (idx, min_distance) in
                                enumerate(distance))

        # The spot id of the nearest spot to the MADIS station.
        spot_id = all_spot_cms_specs.iloc[idx]['spot_id']

    # Extract additional specifics associated with the surf spot id.
    spot_cms_specs = all_spot_cms_specs[all_spot_cms_specs['spot_id'] == spot_id]

    # The name of the nearest surf spot to the station.
    spot_name = spot_cms_specs['spot_name'].values[0]

    # The surf spots prime wind minimum and maximum directions.
    # These must be set in the CMS in order to perform validation.
    prime_wind_minimum = float(spot_cms_specs['prime_wind_minimum_1'].tolist()[0])
    prime_wind_maximum = float(spot_cms_specs['prime_wind_maximum_1'].tolist()[0])
    if np.isnan(prime_wind_minimum) or np.isnan(prime_wind_maximum):
        raise ValueError('Spot Name: ' + spot_name + ' has '
                         'no prime wind directions set in the CMS. '
                         'Set the prime wind directions and rerun '
                         'for station id: ' + station_id)
    else:
        prime_wind_minimum = int(prime_wind_minimum)
        prime_wind_maximum = int(prime_wind_maximum)

    # Average the prime wind minimum and maximum to get the offshore direction.
    prime_wind_direction = get_circ_mean([prime_wind_minimum,
                                          prime_wind_maximum])

    # The timezone of the nearest surf spot to the station.
    station_tz = spot_cms_specs['timezone'].values[0]

    # Return the spot id, distance, spot name, and prime wind direction
    # of the nearest spot to the station.
    return spot_id, min_distance, prime_wind_direction, spot_name, station_tz


def list_all_station_ids():
    """Generate a list of all station ids with data
    for wind validation.

    Example to execute:
    wmp_functions.list_all_station_ids()

    Returns:
    A list of all station ids with data for wind validation.
    """

    # Routinely updated backend files containing information
    # about surfline and madis reliable/coastal wind stations,
    # respectively.
    sl_weather_stations_file = '/ocean/static/sl_weather_stations/wx_station_specs.csv'
    madis_weather_stations_file = '/ocean/transient/madis/madis_reliable_coastal_station_ids.csv'

    # Read Surfline and MADIS station specs into dataframes
    sl_weather_stations_df = pd.read_csv(sl_weather_stations_file)
    madis_weather_stations_df = pd.read_csv(madis_weather_stations_file)

    # Concatenate Surfline and MADIS station specs into single dataframe
    # and output to list for reference.
    all_station_ids = pd.concat([madis_weather_stations_df['station_id'],
                                sl_weather_stations_df['spotid'].rename({'spotid': 'station_id'})],
                                ignore_index=True).tolist()

    # Return a list of all station ids with data for wind validation.
    return all_station_ids


def generate_linear_correction_coefficients(station_model_df, station_id,
                                            statistical_correction, coefficient_file):
    """Use linear regression to generate model wind
    correction coefficients based on the passed dataframes
    grouping parameters.

    Required Arguments:
    station_model_df (dataframe) - a dataframe containing observations
    broken down into classes alongside model data aligned in time.

    station_id (string) - the station id.

    statistical_correction (int) - the statistical correction version.

    coefficient_file (string) - the output file to store the coefficients.

    Example to execute:
    wmp_functions.generate_linear_correction_coefficients(station_model_df, 'E3141', 3, tmp.csv)

    Returns:
    A csv file per group containing linear wind correction coefficients.
    """

    # The model used to generate correction coefficients for.
    model_id = station_model_df['model_id'].unique()[0]

    # Define the passed dataframe grouping variables
    # based on the statistical correction version.
    if statistical_correction == 1:

        # Apply linear correction by model across all wind speeds and directions.
        coefficient_group = ['model_id']
        grouping_dict = {'model_id': model_id}

    elif statistical_correction == 2:

        # Extract model wind condition
        model_wind_condition = station_model_df['model_wind_condition'].unique()[0]

        # Apply linear correction by model based on wind condition.
        coefficient_group = ['model_id', 'model_wind_condition']
        grouping_dict = {'model_id': model_id,
                         'model_wind_condition': model_wind_condition}

    elif statistical_correction == 3:

        # Extract model wind condition and hour of day.
        model_wind_condition = station_model_df['model_wind_condition'].unique()[0]
        hour_of_day = station_model_df['hour'].unique()[0]

        # Apply linear correction by model based on wind condition
        # and hour of day.
        coefficient_group = ['model_id', 'model_wind_condition', 'hour']
        grouping_dict = {'model_id': model_id,
                         'model_wind_condition': model_wind_condition,
                         'hour': hour_of_day}

    elif statistical_correction == 4:

        # Extract model wind condition and hour of day.
        model_wind_condition = station_model_df['model_wind_condition'].unique()[0]
        time_of_day = station_model_df['time_of_day'].unique()[0]

        # Apply linear correction by model based on wind condition
        # and hour of day.
        coefficient_group = ['model_id', 'model_wind_condition', 'time_of_day']
        grouping_dict = {'model_id': model_id,
                         'model_wind_condition': model_wind_condition,
                         'time_of_day': time_of_day}

    # Raise error if statistical correction is not one of the above.
    else:
        raise AttributeError('Invalid entry for statistical_correction variable.\n')

    # Constants used to convert degrees to radians
    # and radians to degrees.
    deg_2_rad = float(0.01745)
    rad_2_deg = float(57.2957)

    # Convert observed and modeled wind directions to radians.
    ob_wind_dir_rad = station_model_df['ob_wind_direction'] * deg_2_rad
    model_wind_dir_rad = station_model_df['model_wind_direction'] * deg_2_rad

    # Calculate observed and modeled u and v wind components.
    ob_wind_u_comp = (-1 * (station_model_df['ob_wind_speed_ms']) * np.sin(ob_wind_dir_rad))
    ob_wind_v_comp = (-1 * (station_model_df['ob_wind_speed_ms']) * np.cos(ob_wind_dir_rad))
    model_wind_u_comp = (-1 * (station_model_df['model_wind_speed_ms']) * np.sin(model_wind_dir_rad))
    model_wind_v_comp = (-1 * (station_model_df['model_wind_speed_ms']) * np.cos(model_wind_dir_rad))

    # Create model specific wind component dataframe.
    wind_component_df_dict = {'ob_wind_u_comp': ob_wind_u_comp,
                              'ob_wind_v_comp': ob_wind_v_comp,
                              'model_wind_u_comp': model_wind_u_comp,
                              'model_wind_v_comp': model_wind_v_comp}

    model_wind_component_df = pd.DataFrame(wind_component_df_dict)

    # Calculate linear correction coefficients.
    u_coefficients = np.polyfit(model_wind_component_df['model_wind_u_comp'],
                                model_wind_component_df['ob_wind_u_comp'], 1)

    u_coefficient_a = "%.4f" % (u_coefficients[0])
    u_coefficient_b = "%.4f" % (u_coefficients[1])

    v_coefficients = np.polyfit(model_wind_component_df['model_wind_v_comp'],
                                model_wind_component_df['ob_wind_v_comp'], 1)

    v_coefficient_a = "%.4f" % (v_coefficients[0])
    v_coefficient_b = "%.4f" % (v_coefficients[1])

    # Used to combine with the variable grouping dictionary,
    # which is based on the statistical version, for the
    # output coefficient csv.
    coefficient_dict = {'u_coefficient_a': u_coefficient_a,
                        'u_coefficient_b': u_coefficient_b,
                        'v_coefficient_a': v_coefficient_a,
                        'v_coefficient_b': v_coefficient_b}

    # Combine the grouping dictionary and the coefficient
    # dictionary for the output csv.
    csv_dict = dict(grouping_dict.items() + coefficient_dict.items())

    # List of wind component variable names to combine with the
    # coefficient group for use in creating the coefficient csv file.
    wind_component_list = ['u_coefficient_a', 'u_coefficient_b',
                           'v_coefficient_a', 'v_coefficient_b']

    # Field names to output to the coefficient csv file. Include
    # variables which the station-model dataframe is grouped by
    # in the following list.
    coefficient_file_vars = coefficient_group + wind_component_list

    # Output the coefficients to a csv file.
    with open(coefficient_file, mode='a') as csv_file:
        writer = csv.DictWriter(csv_file, fieldnames=coefficient_file_vars)
        if csv_file.tell() == 0:
            writer.writeheader()
        writer.writerow(csv_dict)

    return 'Coefficients written to ' + str(coefficient_file)


def linear_model_wind_correction(station_model_df, station_id, statistical_correction):
    """Extract and apply linear model wind correction coefficients
    from previously generated csv files based on the passed dataframes
    grouping parameters.

    Required Arguments:
    station_model_df (dataframe) - a dataframe containing observations
    broken down into classes alongside model data aligned in time.

    station_id (string) - the station id.

    statistical_correction (int) - the statistical correction version.

    Example to execute:
    wmp_functions.linear_model_wind_correction(station_model_df, 'E3141', 3)

    Returns:
    A dataframe containing corrected model wind data using
    linear regression.
    """

    # The model used to generate correction coefficients for.
    model_id = station_model_df['model_id'].unique()[0]

    # Stored coefficient file paths and names. The files are simply named
    # based on the spot id with the rows within denoting what the passed
    # dataframe is grouped by prior to generating the coefficients.
    coefficient_path = ('/ocean/static/validation/wind_model_performance/coefficients_v' +
                        str(statistical_correction))
    coefficient_csv = station_id + '.csv'
    coefficient_file = os.path.join(coefficient_path, coefficient_csv)

    # Initialize u and v wind component coefficients in case they
    # don't exist for the passed dataframe grouping being analyzed.
    u_coefficient_a = 1.0
    u_coefficient_b = 0.0
    v_coefficient_a = 1.0
    v_coefficient_b = 0.0

    # Check if the coefficient file exists. If it does, extract the u and v
    # wind component coefficients. Otherwise, set them to 1 and 0, respectively.
    if os.path.exists(coefficient_file):

        # Read the stations coefficient file.
        coefficients = pd.read_csv(coefficient_file)

        # Define the passed dataframe grouping variables
        # based on the statistical correction version.
        if statistical_correction == 1:
            coefficients = coefficients[(coefficients['model_id'] == model_id)]

        elif statistical_correction == 2:
            model_wind_condition = station_model_df['model_wind_condition'].unique()[0]
            coefficients = coefficients[(coefficients['model_id'] == model_id) &
                                        (coefficients['model_wind_condition'] == model_wind_condition)]

        elif statistical_correction == 3:
            model_wind_condition = station_model_df['model_wind_condition'].unique()[0]
            hour_of_day = station_model_df['hour'].unique()[0]
            coefficients = coefficients[(coefficients['model_id'] == model_id) &
                                        (coefficients['model_wind_condition'] == model_wind_condition) &
                                        (coefficients['hour'] == hour_of_day)]

        elif statistical_correction == 4:
            model_wind_condition = station_model_df['model_wind_condition'].unique()[0]
            time_of_day = station_model_df['time_of_day'].unique()[0]
            coefficients = coefficients[(coefficients['model_id'] == model_id) &
                                        (coefficients['model_wind_condition'] == model_wind_condition) &
                                        (coefficients['time_of_day'] == time_of_day)]

        # Ensure coefficients exist, otherwise they're set to 1 and 0, respectively.
        if not coefficients.empty:
            coefficients = coefficients.drop_duplicates()
            u_coefficient_a = coefficients['u_coefficient_a'].values[0]
            u_coefficient_b = coefficients['u_coefficient_b'].values[0]
            v_coefficient_a = coefficients['v_coefficient_a'].values[0]
            v_coefficient_b = coefficients['v_coefficient_b'].values[0]

    # Constants used to convert degrees to radians
    # and radians to degrees.
    deg_2_rad = float(0.01745)
    rad_2_deg = float(57.2957)

    # Convert observed and modeled wind directions to radians.
    ob_wind_dir_rad = station_model_df['ob_wind_direction'] * deg_2_rad
    model_wind_dir_rad = station_model_df['model_wind_direction'] * deg_2_rad

    # Calculate observed and modeled u and v wind components.
    ob_wind_u_comp = (-1 * (station_model_df['ob_wind_speed_ms']) * np.sin(ob_wind_dir_rad))
    ob_wind_v_comp = (-1 * (station_model_df['ob_wind_speed_ms']) * np.cos(ob_wind_dir_rad))
    model_wind_u_comp = (-1 * (station_model_df['model_wind_speed_ms']) * np.sin(model_wind_dir_rad))
    model_wind_v_comp = (-1 * (station_model_df['model_wind_speed_ms']) * np.cos(model_wind_dir_rad))

    # Create model specific wind component dataframe.
    wind_component_df_dict = {'ob_wind_u_comp': ob_wind_u_comp,
                              'ob_wind_v_comp': ob_wind_v_comp,
                              'model_wind_u_comp': model_wind_u_comp,
                              'model_wind_v_comp': model_wind_v_comp}

    model_wind_component_df = pd.DataFrame(wind_component_df_dict)

    # Linearly corrected u and v wind components.
    model_wind_u_comp_corrected = ((float(u_coefficient_a) *
                                    model_wind_component_df['model_wind_u_comp']) + float(u_coefficient_b))

    model_wind_v_comp_corrected = ((float(v_coefficient_a) *
                                    model_wind_component_df['model_wind_v_comp']) + float(v_coefficient_b))

    # Compute corrected model wind speed and direction based on corrected u and v wind components.
    station_model_df['model_wind_speed_ms_corrected'] = (np.sqrt(np.square(model_wind_u_comp_corrected) +
                                                                 np.square(model_wind_v_comp_corrected)))

    station_model_df['model_wind_direction_corrected'] = (180 + (rad_2_deg *
                                                                 np.arctan2(model_wind_u_comp_corrected,
                                                                            model_wind_v_comp_corrected)))

    # Set output dataframe index to datetime.
    station_model_df.set_index(pd.to_datetime(station_model_df['ob_valid_time'],
                                              unit='s'), inplace=True, drop=False)
    station_model_df.index.names = ['datetime']

    # Return the original dataframe with corrected model data included.
    return station_model_df


def generate_model_performance_df_raw(station_model_df):
    """Generate a dataframe containing model performance
    by model in the form of a weighted f1 score.

    Required Arguments:
    station_model_df (dataframe) - a dataframe containing observations
    broken down into classes alongside model data aligned in time.

    Example to execute:
    wmp_functions.generate_model_performance_df_raw(station_model_df)

    Returns:
    A dataframe containing the weighted f1 score by model.
    """

    # The model id.
    model_id = station_model_df['model_id'].iloc[0]

    # Number of observations by model - only forecast hour 0 taken into account.
    num_obs_by_model = len(station_model_df)

    # Model specific observed wind conditions
    ob_wind_conditions = station_model_df['ob_wind_condition']

    # Model specific predicted wind conditions
    model_wind_conditions = station_model_df['model_wind_condition']

    # Calculate the model f1 score.
    f1_score = get_f1_score(ob_wind_conditions, model_wind_conditions)

    # Append to the final model performance dataframe.
    model_performance_df = pd.DataFrame({'model_id': [model_id],
                                         'f1_score': [f1_score],
                                         'num_obs': [num_obs_by_model]})

    # Set model performance dataframe index to the model id.
    model_performance_df.set_index('model_id', inplace=True, drop=True)

    # Return the model performance dataframe containing the
    # weighted f1 score and the number of observations used.
    return model_performance_df


def generate_model_performance_df_correction(station_model_df):
    """Generate a dataframe containing model performance
    by model in the form of a weighted f1 score and a weighted
    f1 score post model correction.

    Required Arguments:
    station_model_df (dataframe) - a dataframe containing observations
    broken down into classes alongside model data aligned in time.

    Example to execute:
    wmp_functions.generate_model_performance_df_correction(station_model_df)

    Returns:
    A dataframe containing the weighted f1 scores (raw and corrected)
    by model.
    """

    # The model id.
    model_id = station_model_df['model_id'].iloc[0]

    # Number of observations by model - only forecast hour 0 taken into account.
    num_obs_by_model = len(station_model_df)

    # Model specific observed wind conditions
    ob_wind_conditions = station_model_df['ob_wind_condition']

    # Model specific predicted wind conditions
    model_wind_conditions = station_model_df['model_wind_condition']
    model_wind_conditions_corrected = station_model_df['model_wind_condition_corrected']

    # Calculate the model f1 score.
    f1_score = get_f1_score(ob_wind_conditions, model_wind_conditions)
    f1_score_corrected = get_f1_score(ob_wind_conditions, model_wind_conditions_corrected)

    # Append to the final model performance dataframe.
    model_performance_df = pd.DataFrame({'model_id': [model_id],
                                         'f1_score': [f1_score],
                                         'f1_score_corrected': [f1_score_corrected],
                                         'num_obs': [num_obs_by_model]})

    # Set model performance dataframe index to the model id.
    model_performance_df.set_index('model_id', inplace=True, drop=True)

    # Return the model performance dataframe containing the
    # weighted f1 score and the number of observations used.
    return model_performance_df


def station_data_qc(station_data_df):
    """Quality control wind station data and remove bad data.

    Required Arguments:
    station_data_df (dataframe) - a dataframe containing observations
    observations from a single station.

    Example to execute:
    wmp_functions.station_data_qc(station_data_df)

    Returns:
    The passed dataframe with data not passing qc removed.
    """

    # Plausible value check and removal.

    # Min and max wind speed in meters per second
    wind_speed_min = 0.0
    wind_speed_max = 61.0

    # Min and max wind direction in degrees
    wind_dir_min = 0.0
    wind_dir_max = 360.0

    # Drop rows where wind speed or direction are
    # not within plausible values specified above.
    station_data_df = (station_data_df[(station_data_df['ob_wind_speed_ms'] >= wind_speed_min) &
                                       (station_data_df['ob_wind_speed_ms'] <= wind_speed_max) &
                                       (station_data_df['ob_wind_direction'] >= wind_dir_min) &
                                       (station_data_df['ob_wind_direction'] <= wind_dir_max)])

    # Drop rows where wind speed and direction are both 0.0.
    station_data_df = (station_data_df[(station_data_df['ob_wind_speed_ms'] != 0.0) &
                                       (station_data_df['ob_wind_direction'] != 0.0)])

    # Add datetime index as column and create columns for grouping by time.
    station_data_df['datetime_fmt'] = station_data_df.index
    station_data_df['ymdh'] = station_data_df['datetime_fmt'].dt.strftime('%Y%m%d%H')

    # Group the station dataframe by time for variability checks.
    hourly_station_data_df_grouped = station_data_df.groupby('ymdh')

    # Check wind speed variability by time.
    hourly_wind_speed_stats = hourly_station_data_df_grouped[['ob_wind_speed_ms']].describe()

    # Hourly wind speed qc for removing hours with standard deviation < 0.1.
    bad_speed_hours = (hourly_wind_speed_stats.loc[((hourly_wind_speed_stats
                                                     ['ob_wind_speed_ms']['count'] > 2) &
                                                    (hourly_wind_speed_stats
                                                     ['ob_wind_speed_ms']['std'] < 0.1))].index)

    # Hourly wind direction variance.
    hourly_wind_dir_variance = (hourly_station_data_df_grouped.apply
                                (lambda x: pd.Series({'variance': get_circ_variance(x['ob_wind_direction']),
                                                      'count': x['ob_wind_direction'].count()})))

    # Hourly wind direction qc for removing hours with 0 standard deviation.
    bad_dir_hours = (hourly_wind_dir_variance.loc[((hourly_wind_dir_variance['count'] > 2) &
                                                   (hourly_wind_dir_variance['variance'] < 0.000001))].index)

    # Remove hours from passed dataframe that didn't pass qc.
    station_data_df = station_data_df[(~station_data_df.ymdh.isin(bad_speed_hours)) &
                                      (~station_data_df.ymdh.isin(bad_dir_hours))]

    # One final pass to remove data where wind direction is
    # constant throughout the remaining time series.
    wind_dir_variance = get_circ_variance(station_data_df['ob_wind_direction'])

    # If wind direction is constant throughout the remaining
    # time series, remove all data and return empty df.
    if wind_dir_variance < 0.000001:
        station_data_df = pd.DataFrame()

    # Return passed dataframe but with bad data removed.
    return station_data_df
