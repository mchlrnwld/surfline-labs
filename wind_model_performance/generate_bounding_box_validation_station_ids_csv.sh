#!/bin/sh

### PROCESS TO GENERATE A CSV FILE IN THE LOCAL DIRECTORY ###
### CONTAINING ALL RELIABLE/COASTAL MADIS WIND STATIONS IN ADDITION ###
### TO SURFLINE PROPRIETARY STATIONS WITHIN A PROVIDED GEOGRAPHICAL ###
### BOUNDING BOX ###

source ${AWS_SCIENCE_BASE}/AWS_PATH_CONFIG

### PASS THE LATITUDE/LONGITUDE VALUES DEFINING THE BOUNDING BOX TO ###
### TO EXTRACT STATION ID'S WITHIN ###
SLAT=$1
NLAT=$2
WLON=$3
ELON=$4

### ENSURE APPROPRIATE NUMBER OF ARGUMENTS ARE PASSED TO THE SCRIPT ###
if [ $# -ne 4 ];

  echo "Need to pass 4 parameters defining a latitude/longitude bounding box i.e.:"
  echo "./generate_bounding_box_validation_station_ids_csv.sh 30 40 -120 -115"
  echo "Exiting"
  exit

fi

### ROUTINELY UPDATED CSV FILE CONTAINING INFORMATION ON ALL ###
### PROPRIETARY SURFLINE WIND STATIONS ###
SL_STATION_FILE="${AWS_STATIC}/sl_weather_stations/wx_station_specs.csv"

### ROUTINELY UPDATED CSV FILE CONTAINING INFORMATION ON ALL ###
### RELIABLE/COASTAL MADIS WIND STATIONS ###
MADIS_STATION_FILE="${AWS_TRANSIENT}/madis/madis_validation_station_metadata.csv"

### A PREVIOUSLY GENERATED CSV FILE CONTAINING A LIST OF ALL SURFLINE-PROPRIETARY ###
### AND MADIS RELIABLE/COASTAL STATION ID'S ###
ALL_STATION_IDS=`cat all_station_ids.csv`

### OUTPUT CSV FILE TO CONTAIN ALL WIND STATION ID'S TO VALIDATE AGAINST ###
BOUNDING_BOX_STATION_FILE="bounding_box_station_ids.csv"

### REMOVE OUTPUT CSV FILE IF IT EXISTS ###
if [ -e "${BOUNDING_BOX_STATION_FILE}" ]; then

  rm ${BOUNDING_BOX_STATION_FILE}

fi

### LOOP THROUGH ALL SURFLINE-PROPRIETARY AND MADIS RELIABLE/COASTAL STATION ID'S ###
for STATION_ID in $ALL_STATION_IDS
do

  ### CHECK IF THE STATION IS A SURFLINE OR A MADIS STATION ###
  ### AND EXTRACT THE LATITUDE/LONGITUDE OF THE STATION ###
  SL_STATION_CHECK=`grep "${STATION_ID}," ${SL_STATION_FILE}`
  if [ ! -z "${SL_STATION_CHECK}" ]; then

    LAT=`echo "${SL_STATION_CHECK}" | cut -d',' -f3`
    LON=`echo "${SL_STATION_CHECK}" | cut -d',' -f4`

  else

    MADIS_STATION_CHECK=`awk -v sid=${STATION_ID} -F',' '{ if($1 == sid) print $0 }' ${MADIS_STATION_FILE}`
    LAT=`echo "${MADIS_STATION_CHECK}" | cut -d',' -f2`
    LON=`echo "${MADIS_STATION_CHECK}" | cut -d',' -f3`

  fi

  ### CHECK IF THE STATION IS WITHIN THE PASSED LATITUDE/LONGITUDE BOUNDING BOX ###
  ### AND IF SO ADD IT TO THE OUTPUT CSV FILE ###
  DOMAIN_CHECK=$(awk -v lat=${LAT} -v lon=${LON} -v slat=${SLAT} -v nlat=${NLAT} \
                 -v wlon=${WLON} -v elon=${ELON} \
                 'BEGIN { if(lat > slat && lat < nlat && lon > wlon && lon < elon) print "true" }')

  if [ "${DOMAIN_CHECK}" == "true" ]; then

    echo "${STATION_ID}" >> ${BOUNDING_BOX_STATION_FILE}

  fi

done
