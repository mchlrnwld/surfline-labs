import os
import optparse
import csv

import numpy as np
import pandas as pd

from datetime import datetime

import wmp_functions
import pytz

import warnings
warnings.filterwarnings("ignore")


if __name__ == "__main__":
    """Validate all wind models available against wind station
    observations at a single wind station to determine wind station
    data validity. 

    Required Arguments:
    station_id (string) - the id associated with a wind station.

    start_time (int) - the unix time stamp indicating the start of the
    validation period.

    end_time (int) - the unix time stamp indicating the end of the
    validation period.

    start_fhr (int) - the starting forecast hour of the model forecast
    hour range to validate.

    end_fhr (int) - the ending forecast hour of the model forecast
    hour range to validate.

    out_dir (string) - the output directory for performance data.

    Example to execute for a wind station:
    python wind_station_validity.py --station_id="5842041f4e65fad6a77088e8"
    --start_time=1586152800 --end_time=1586322000 --start_fhr=0 --end_fhr=5
    --out_dir=/ocean/static/validation/wind_model_performance/tmp

    python compare_model_performance_single_station.py --station_id="E3141"
    --start_time=1586152800 --end_time=1586322000 --start_fhr=0 --end_fhr=5
    --out_dir=/ocean/static/validation/wind_model_performance/tmp
    --statistical_correction=3 --qc="yes"

    python compare_model_performance_single_station.py --station_id="5842041f4e65fad6a77088e8"
    --start_time=1586152800 --end_time=1586322000 --start_fhr=0 --end_fhr=5
    --out_dir=/ocean/static/validation/wind_model_performance/tmp
    --statistical_correction=3 --qc="no" --new_model="socal_1month_v12"

    Returns:
    A csv file containing raw model results at a single wind station.
    """

    parser = optparse.OptionParser()

    # Arguments
    parser.add_option('--station_id',
                      dest='station_id',
                      help='Station ID',
                      type='string')

    parser.add_option('--start_time',
                      dest='start_time',
                      help='Starting unix timestamp',
                      type='int')

    parser.add_option('--end_time',
                      dest='end_time',
                      help='Ending unix timestamp',
                      type='int')

    parser.add_option('--start_fhr',
                      dest='start_fhr',
                      help='Starting model forecast hour',
                      type='int')

    parser.add_option('--end_fhr',
                      dest='end_fhr',
                      help='Ending model forecast hour',
                      type='int')

    parser.add_option('--out_dir',
                      dest='out_dir',
                      help='Output directory',
                      type='string',
                      default='/ocean/static/validation/wind_model_performance/tmp')

    (opts, args) = parser.parse_args()

    station_id = opts.station_id
    start_time = opts.start_time
    end_time = opts.end_time
    start_fhr = opts.start_fhr
    end_fhr = opts.end_fhr
    out_dir = opts.out_dir

    # List of all publicly available and old in-house
    # WRF models with data in the data lake to validate
    # unless a valid "new_model" argument is provided.
#    model_list = ['gfs0p25', 'hrrrconus', 'namconus',
#                  'namhi', 'nampr', 'wrf_aust_nsw_3km',
#                  'wrf_aust_qland_3km', 'wrf_aust_south_3km',
#                  'wrf_aust_vic_tas_3km', 'wrf_aust_west_3km',
#                  'wrf_baja_3km', 'wrf_baja_9km', 'wrf_bali_3km',
#                  'wrf_brazil_rio_3km', 'wrf_fiji_3km',
#                  'wrf_hawaii_1km', 'wrf_hawaii_3km',
#                  'wrf_iberia_6km', 'wrf_med_east_3km',
#                  'wrf_med_east_9km', 'wrf_mexico_central_3km_1',
#                  'wrf_mexico_central_3km_2', 'wrf_new_zealand_3km',
#                  'wrf_new_zealand_9km', 'wrf_puerto_rico_3km',
#                  'wrf_sumatra_new_3km', 'wrf_tahiti_1km',
#                  'wrf_tahiti_3km', 'wrf_uk_3km', 'wrf_uk_9km']

    # List of only publicly available wind models.
    model_list = ['gfs0p25', 'hrrrconus', 'namconus', 'namhi', 'nampr']

    # Formatted validation time range for print statement.
    start_date = datetime.utcfromtimestamp(start_time).strftime('%Y-%m-%d %H:%M:%S')
    end_date = datetime.utcfromtimestamp(end_time).strftime('%Y-%m-%d %H:%M:%S')

    # Output csv's containing model performance results and
    # data removed by qc (if the qc parameter was set to 'yes')
    raw_model_performance_csv = os.path.join(out_dir, station_id + '_raw_model_performance.csv')
    model_performance_summary_csv = os.path.join(out_dir, station_id + '_wind_model_performance_summary.csv')

    # Extract useful information about the nearest surf spot
    # associated with the wind station.
    nearest_spot_specs = wmp_functions.get_nearest_surf_spot_specs(station_id)

    # Relevant specifics about the nearest spot tied to the station.
    spot_id = nearest_spot_specs[0]
    spot_distance_km = round(nearest_spot_specs[1], 2)
    prime_wind_direction = nearest_spot_specs[2]
    spot_name = nearest_spot_specs[3]
    spot_timezone = nearest_spot_specs[4]

    # Log to screen.
    print 'Analyzing model performance for:'
    print 'Station id: ' + station_id
    print 'Spot name: ' + spot_name
    print 'Spot timezone: ' + spot_timezone
    print 'Validation period: ' + str(start_time) + ' to ' + str(end_time)
    print 'Model forecast hour range: ' + str(start_fhr) + '-' + str(end_fhr)

    # Extract station observations for provided station id and time range.
    station_data_df = wmp_functions.get_station_data(station_id, start_time, end_time)

    # Check to ensure station data exists before proceeding.
    if station_data_df.empty:

        print 'No station data found for:'
        print 'Station id: ' + station_id
        print 'Nearest Spot: ' + spot_name
        print 'Validation period: ' + str(start_date) + ' to ' + str(end_date)
        print 'Model forecast hour range: ' + str(start_fhr) + '-' + str(end_fhr)

    else:

        # Adjust station wind speeds to a height of 10 meters.
        station_data_df = wmp_functions.apply_wind_height_correction(station_id, station_data_df)

        # Extract observed wind conditions based on our derivation.
        ob_wind_condition = wmp_functions.get_wind_condition(station_data_df['ob_wind_direction'],
                                                             prime_wind_direction,
                                                             station_data_df['ob_wind_speed_ms'])

        # Create ob_wind_condition column in station dataframe.
        station_data_df['ob_wind_condition'] = ob_wind_condition.tolist()

        # Extract all model predictions for provided station id, model id,
        # forecast hour, and time range.
        all_model_data_df = wmp_functions.get_model_data(station_id, start_time, end_time,
                                                         start_fhr, end_fhr)

        # Check to ensure model data exists before proceeding.
        if all_model_data_df.empty:

            print 'No model data found for:'
            print 'Station id: ' + station_id
            print 'Nearest Spot: ' + spot_name
            print 'Validation period: ' + str(start_date) + ' to ' + str(end_date)
            print 'Model forecast hour range: ' + str(start_fhr) + '-' + str(end_fhr)

        else:

            # Drop models not included in the model_list at the top. Note this also accounts
            # for the case where a valid new_model model id is passed.
            select_model_data_df = all_model_data_df[all_model_data_df.model_id.isin(model_list)]

            # Ensure an equal sample size between all models considered
            # for raw (uncorrected) model comparisons.
            num_models = select_model_data_df['model_id'].nunique()
            model_data_df_raw = (select_model_data_df.groupby(['model_valid_time', 'forecast_hour'],
                                                               as_index=False)
                                 .filter(lambda x: x.shape[0] == num_models))

            # Extract modeled wind conditions based on our derivation
            # for raw (uncorrected) model comparisons.
            model_wind_condition_raw = wmp_functions.get_wind_condition(model_data_df_raw['model_wind_direction'],
                                                                        prime_wind_direction,
                                                                        model_data_df_raw['model_wind_speed_ms'])

            # Create model_wind_condition column in model dataframe used
            # for raw (uncorrected) model comparisons.
            model_data_df_raw['model_wind_condition'] = model_wind_condition_raw.tolist()

            # Align observed with predicted wind data in time in model dataframe
            # used for raw (uncorrected) model comparisons.
            station_model_df_raw = wmp_functions.align_observed_v_predicted_wind_data(station_data_df,
                                                                                      model_data_df_raw)

            # Check to ensure model-ob matches are found before proceeding.
            if station_model_df_raw.empty:

                print 'No model-ob matches found for:'
                print 'Station id: ' + station_id
                print 'Nearest Spot: ' + spot_name
                print 'Validation period: ' + str(start_date) + ' to ' + str(end_date)
                print 'Model forecast hour range: ' + str(start_fhr) + '-' + str(end_fhr)

            else:

                # Group the station-model dataframe used for raw (uncorrected) model comparisons
                # by the model id to generate the raw v raw model performance dataframe based on that.
                model_performance_df_raw = station_model_df_raw.groupby('model_id', as_index=False)

                # Generate the model performance dataframe for raw (uncorrected) model comparisons
                # using the aligned station-model dataframe grouped by the model id.
                model_performance_df_raw = model_performance_df_raw.apply(wmp_functions.generate_model_performance_df_raw)

                # Drop the multi-index created in the model performance dataframe for
                # raw (uncorrected) model comparisons.
                model_performance_df_raw.reset_index(level=0, drop=True, inplace=True)

                # Add useful spot specifics to model performance dataframe.
                model_performance_df_raw['spot_name'] = spot_name
                model_performance_df_raw['spot_distance_km'] = spot_distance_km
                model_performance_df_raw['station_id'] = station_id
                model_performance_df_raw['model_id'] = model_performance_df_raw.index

                # Order model performance dataframe columns.
                model_performance_df_raw = model_performance_df_raw[['spot_name',
                                                                     'spot_distance_km',
                                                                     'station_id',
                                                                     'model_id',
                                                                     'f1_score',
                                                                     'num_obs']]

                # Output model performance results to csv for analysis.
                model_performance_df_raw.to_csv(raw_model_performance_csv, index=False)

                # Model performance summary metrics.
                model_ob_matches = model_performance_df_raw[model_performance_df_raw['model_id'] == 'gfs0p25']['num_obs'].iloc[0]
                avg_f1_score = model_performance_df_raw['f1_score'].mean()
                best_f1_score = model_performance_df_raw['f1_score'].max()
                best_model = model_performance_df_raw[model_performance_df_raw['f1_score'] == best_f1_score]['model_id'].iloc[0]

                # Output file based on whether the station is determined to be valid or not.
                if (model_ob_matches > 500) and (avg_f1_score >= 0.3):

                    print 'Station PASSED validity test: ' + spot_name
                    print 'Average model F1 score: ' + str(avg_f1_score)
                    print 'Model-Ob matches: ' + str(model_ob_matches)
                    print 'Best model: ' + best_model

                    with open(model_performance_summary_csv, mode='w') as performance_csv:
                        performance_writer = csv.writer(performance_csv, delimiter=',')
                        performance_writer.writerow([station_id, best_model])

                else:

                    print 'Station FAILED validity test: ' + spot_name
                    print 'Average model F1 score: ' + str(avg_f1_score)
                    print 'Model-Ob matches: ' + str(model_ob_matches)

                    if os.path.isfile(model_performance_summary_csv):
                        os.remove(model_performance_summary_csv)
