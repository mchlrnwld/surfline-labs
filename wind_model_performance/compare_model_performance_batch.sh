#!/bin/sh

source /home/sam.wilson/.bashrc
source ${AWS_SCIENCE_BASE}/AWS_PATH_CONFIG

export PATH="/home/sam.wilson/anaconda2/bin:$PATH"
source activate py27_data

### SCRIPT ARGUMENGTS: ###
### VALIDATION ALIAS - AN ALIAS DEFINING THE VALIDATION AND THE DIRECTORY FOR PERFORMANCE RESULTS. ###
### STATION_ID_FILE - A FILE CONTAINING THE STATION ID'S TO VALIDATE THE MODELS AGAINST. ###
### START_TIME - A UNIX TIMESTAMP INDICATING THE START TIME OF THE VALIDATION PERIOD. ###
### END_TIME - A UNIX TIMESTAMP INDICATING THE END TIME OF THE VALIDATION PERIOD. ###
### START_FHR - THE STARTING FORECAST HOUR OF THE FORECAST HOUR RANGE TO VALIDATE. ###
### END_FHR - THE ENDING FORECAST HOUR OF THE FORECAST HOUR RANGE TO VALIDATE. ###
### STATISTICAL_CORRECTION - THE STATISTICAL CORRECTION VERSION TO APPLY (1-4) ###
### QC - INDICATE WHETHER OR NOT TO QC THE STATION DATA (YES OR NO) PRIOR TO GENERATING PERFORMANCE RESULTS. ###
### NEW_MODEL (OPTIONAL) - THE ID OF THE NEW MODEL YOU'D LIKE INCLUDED IN THE VALIDATION. ###

VALIDATION_ALIAS=$1
STATION_ID_FILE=$2
START_TIME=$3
END_TIME=$4
START_FHR=$5
END_FHR=$6
STATISTICAL_CORRECTION=$7
QC=$8
NEW_MODEL=$9

### ENSURE APPROPRIATE NUMBER OF ARGUMENTS PASSED ###
if [[ $# -lt 8 ]]; then

  echo "Bad number of arguments passed."
  echo "Provide validation_alias, station_id_file, start_time, end_time, start_fhr, and end_fhr, statistical correction version, qc id (yes or no), and (optional) new model id."
  echo "e.g. ./compare_model_performance_batch.sh socal_v2 station_ids.csv 1586152800 1586322000 0 5 3 no"
  echo "e.g. ./compare_model_performance_batch.sh socal_1month_v12 station_ids.csv 1586152800 1586322000 0 5 3 no socal_1month_v12"
  echo "Exiting."
  exit

fi

### DIRECTORY FOR OUTPUT ###
OUTPUT_DIR="/ocean/static/validation/wind_model_performance/${VALIDATION_ALIAS}"

### CODE DIRECTORY FOR PROCESSING VIA CRON ###
CODE_DIR="/home/sam.wilson/surfline-labs/wind_model_performance"

### MAKE OUTPUT DIRECTORY IF IT DOESN'T EXIST OR IF IT DOES ###
### START WITH A CLEAN DIRECTORY ###
if [ ! -e "${OUTPUT_DIR}" ]; then
  mkdir ${OUTPUT_DIR}
else
  rm -f ${OUTPUT_DIR}/*csv
fi

### LIST OF VALIDATION STATION ID'S ###
STATION_IDS=`cat ${STATION_ID_FILE}`

### LOOP THROUGH ALL STATION IDS FOR VALIDATION ###
for STATION_ID in $STATION_IDS
do

  ### CORE PROCESS TO PRODUCE WIND MODEL PERFORMANCE STATISTICS ###
  python ${CODE_DIR}/compare_model_performance_single_station.py \
  --station_id="${STATION_ID}" --start_time=${START_TIME} \
  --end_time=${END_TIME} --start_fhr=${START_FHR} \
  --end_fhr=${END_FHR} --out_dir=${OUTPUT_DIR} \
  --statistical_correction=${STATISTICAL_CORRECTION} \
  --qc=${QC} --new_model=${NEW_MODEL}

done

### GENERATE README ###
cat > ${OUTPUT_DIR}/README << EOF
Performance results produced using the following parameters:
Start time: ${START_TIME}
End time: ${END_TIME}
Start fhr: ${START_FHR}
End fhr: ${END_FHR}
Statistical correction version: ${STATISTICAL_CORRECTION}
Station data qc'd: ${QC}
EOF
