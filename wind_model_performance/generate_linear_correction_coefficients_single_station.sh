#!/bin/sh

source /home/sam.wilson/.bashrc
source ${AWS_SCIENCE_BASE}/AWS_PATH_CONFIG

export PATH="/home/sam.wilson/anaconda2/bin:$PATH"
source activate py27_data

### SCRIPT ARGUMENGTS: ###
### STATION_ID - A STATION ID TO GENERATE CORRECTION COEFFICIENTS FOR. ###
### START_TIME - A UNIX TIMESTAMP INDICATING THE START TIME OF THE COEFFICIENT GENERATION PERIOD. ###
### END_TIME - A UNIX TIMESTAMP INDICATING THE END TIME OF THE COEFFICIENT GENERATION PERIOD. ###
### START_FHR - THE STARTING FORECAST HOUR IN THE FORECAST HOUR RANGE TO ACCOUNT FOR IN THE COEFFICIENT GENERATION PERIOD. ###
### END_FHR - THE ENDING FORECAST HOUR IN THE FORECAST HOUR RANGE TO ACCOUNT FOR IN THE COEFFICIENT GENERATION PERIOD. ###
### STATISTICAL_CORRECTION - THE STATISTICAL CORRECTION VERSION TO APPLY (1-4) ###
### QC - INDICATE WHETHER OR NOT TO QC THE STATION DATA (YES OR NO) PRIOR TO GENERATING PERFORMANCE RESULTS. ###

STATION_ID=$1
START_TIME=$2
END_TIME=$3
START_FHR=$4
END_FHR=$5
STATISTICAL_CORRECTION=$6
QC=$7

### ENSURE APPROPRIATE NUMBER OF ARGUMENTS PASSED ###
if [[ $# -ne 7 ]]; then

  echo "Bad number of arguments passed."
  echo "Provide validation_alias, station_id_file, start_time, end_time, start_fhr, end_fhr, statistical correction version, and qc id (yes or no)."
  echo "e.g. ./generate_linear_correction_coefficients_single_station.sh 5842041f4e65fad6a77088ae 1557072220 1588612220 0 12 4 no" 
  echo "Exiting."
  exit

fi

### DIRECTORY FOR OUTPUT ###
OUTPUT_DIR="/ocean/static/validation/wind_model_performance/coefficients_v${STATISTICAL_CORRECTION}"

### MAKE OUTPUT DIRECTORY IF IT DOESN'T EXIST. NOTE THAT IF A STATIONS
### COEFFICIENT FILE IS PRE-EXISTING, IT WILL BE OVERWRITTEN IN THIS PROCESS.
if [ ! -e "${OUTPUT_DIR}" ]; then
  mkdir ${OUTPUT_DIR}
fi

### CORE PROCESS TO PRODUCE WIND MODEL CORRECTION COEFFICIENTS ###
python generate_linear_correction_coefficients_single_station.py \
 --station_id="${STATION_ID}" --start_time=${START_TIME} \
 --end_time=${END_TIME} --start_fhr=${START_FHR} \
 --end_fhr=${END_FHR} --statistical_correction=${STATISTICAL_CORRECTION} \
 --qc=${QC}
