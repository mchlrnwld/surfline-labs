import os

import configparser


def get_keys(aws_profile_name):
    """Get AWS s3 keys associated with provided profile name.

    Required Arguments:
    aws_profile_name - string

    Example to execute:
    s3_credentials.get_keys('surfline-prod')

    Returns:
    A list

    Details:
    This function returns the aws_access_key_id
    and the aws_secret_access_key needed for accessing
    the s3 account for the profile name provided.
    The access key and secret key are returned in
    a python list.
    """

    aws_config_file = '~/.aws/credentials'
    Config = configparser.ConfigParser()
    Config.read(os.path.expanduser(aws_config_file))
    access_key_id = Config[aws_profile_name]['aws_access_key_id']
    secret_key_id = Config[aws_profile_name]['aws_secret_access_key']

    return [access_key_id, secret_key_id]
