from pyathena import connect

import s3_credentials


def connect_to_athena(aws_profile_name):
    """Establish connection with AWS Athena database.

    Required Arguments:
    aws_profile_name - string

    Example to execute:
    athena_access.connect_to_athena('surfline-prod')

    Details:
    This function establishes a connection to our
    production athena database in the us-west-2 region.
    It requires s3 credentials, which are gathered below
    using the s3_credentials module and providing the
    aws profile name you have set in your .aws/credentials
    file to the function.
    """

    connection = connect(aws_access_key_id=s3_credentials.get_keys(aws_profile_name)[0],
                         aws_secret_access_key=s3_credentials.get_keys(aws_profile_name)[1],
                         s3_staging_dir='s3://aws-athena-query-results-833713747344-us-west-2/',
                         region_name='us-west-2')

    return connection
