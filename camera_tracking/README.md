# Camera Tracking
A project to create a simple tracking process that runs at 3.75hz and can
track surfers in the water as well as surfers surfing.

`tracking_model.py` implements `TrackingModel` a stateless tracker that
maintains track IDs between detections. It also implements `BatchTrackingModel`
which is stateful and writes SurferSurfing tracks to a stack that can be
pulled and written.

`process_rewind.py` provides a simple implementation of the `BatchTrackingModel`
that takes a video and detections pickle from `/data` and runs tracking with
a basic re-rendering of the video into `/output` with annotated objects and 
tracking Ids. 

## Running this project

To run the project:

```
# Build container.
make build

# You'll need a rewind file and associated detections pickle file so get those.

# Enter the container
make shell

# Active python env
source activate camera-tracking

# --rewind absolute path to the source rewind file
# --detections absolute path to the detections pickle file for this rewind
# --output absolute path to where you want the validation mp4 file to go.
python process_rewind.py --rewind=/opt/app/src/583499cb3421b20545c4b53f-20200117T150112823.mp4 --detections=/opt/app/src/583499cb3421b20545c4b53f-20200117T150112823.detections.pkl --fps=4 --output=/opt/app/src/validate.mp4
```
