# Ground Truth Tracks

We've asked taskeater to build a ground truth tracks set. The easiest way for them to do this
is to use their existing bounding box annotation software to label SurferSurfing, except
the class label is unique for each surfed wave. So if there are 10 surfed waves in one
rewind, there will be 10 unique classes in the labels.

## Data Collection & Tracks Parsing
These scripts want a `TRACKING_VALIDATION_DIR` environment variable set, which
defines where the raw data will be downloaded to and read from. To download all the
labels and frames, run the download script:

```bash
./download_raw.sh
```
which will populate that directory with `.xml` files and their associated raw frames.
Everything is partitioned by `rewind_id`, so after you download the latest labeled
data, you can get the associated tracks for any rewind as

```python
tracks = utils.get_tracks('hi-pipeline.stream.20190314T190830229')
```

which will return a list of dictionaries, where each dict will have `startTimestamp`, `endTimestamp`,
`trackID`, `x`, `y` and `timestamps` keys.



