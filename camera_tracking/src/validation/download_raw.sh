#!/bin/bash
#
# Download latest labels and frames from S3 to $TRACKING_VALIDATION_DIR/labeled
# with a frames/ subdir for each set of labels. We can add rewinds download
# if we want to process rewinds as well, tbd.
#
if [[ -z "${TRACKING_VALIDATION_DIR}" ]]; then
    echo "Please set TRACKING_VALIDATION_DIR env, which points to where "
    echo "the raw data will be downloaded to."
    exit 1;
fi

# Easiest way to process this is to put the original frames together
# with the labels, matched on rewind IDs. First download separately:
# Labels
aws s3 cp s3://wt-science-data-labeling-dev/labeled/Tracks/ \
    $TRACKING_VALIDATION_DIR/labeled --recursive --profile surfline-dev

# Raw frames - only download those that have labels for now.
labels_cnt=0
for rwnd_dir in $(ls "$TRACKING_VALIDATION_DIR"/labeled/); do
    vid_pfx=$(basename "$rwnd_dir")
    target=$TRACKING_VALIDATION_DIR"labeled/"$vid_pfx"/frames"
    # Don't re-download existing frames
    if [ ! -d "$target" ]; then
	echo "Downloading ${vid_pfx} frames...";
	aws s3 cp s3://wt-science-data-labeling-dev/unlabeled/tracks/tracks_EXPORT_v0/$vid_pfx \
	    $target --recursive --profile surfline-dev;
    else
	echo "Frames for "$vid_pfx "already exists, skipping..."
    fi
    labels_cnt+=1
done;

echo "Downloaded track labels for" $labels_cnt "rewinds to" $TRACKING_VALIDATION_DIR"/labeled/"
