# Utilities for track labels. Convert raw bounding box .xml labels to
# our normal track format.
#
import os
import datetime
import xml.etree.ElementTree as ET

import numpy as np
import boxes as _boxes

from itertools import chain, groupby
from collections import defaultdict
from PIL import Image, ImageColor
from ipdb import set_trace

def _parse_xml(xml_name):
    """
    Parse Taskeater xml file -> list of bounding box dicts 
    
    :param xml_name: name of xml file to parse
    :returns: list of dicts, where each dict will have the 
    normal bbox keys to build a TensorFlow records file - "bboxes",
    "classes_text", "filename" etc.
    """
    if not os.path.isfile(xml_name):
        raise ValueError(f'File does not exist: {xml_name}')
    tree = ET.parse(xml_name)
    root = tree.getroot()
    ddl = []
    dd = defaultdict(list)
    im_found, cnt = False, 0
    for elem in root.iter():
        if elem.tag == 'image':
            if len(dd) > 0:
                ddl.append(dict(dd))
                dd = defaultdict(list)
            frame_items = dict(elem.items())
            for key in ['width', 'height']:
                dd[key] = int(frame_items[key])
            imname = frame_items['name']
            frame_dir = os.path.dirname(xml_name)
            dd['filename'] = os.path.join(*[frame_dir, 'frames', imname])
            t0, fnum = imname.split('.')[2:4]
            try:
                t0 = datetime.datetime.strptime(t0, '%Y%m%dT%H%M%S%f')
                # All these extractions are done at 3fps!!
                dd['timestamp'] = (
                    t0 + datetime.timedelta(seconds=float(fnum)/3.)).timestamp()
            except:
                raise ValueError('Unable to parse timestamp from file name. '
                                 'Is it of the normal convention? '
                                 f'{imname}')
            im_found = True
            cnt += 1
        # Accumlate all the bbox positions. There may be more than one surfer
        # surfing at a time.
        if elem.tag != 'image':
            if im_found:
                bbox_items = dict(elem.items())
                dd['classes_text'].append(bbox_items['label'])
                #dd['classes'].append(label_map[bbox_items['label']])
                dd['boxes'].append(
                    [float(bbox_items['ytl']),
                     float(bbox_items['xtl']),
                     float(bbox_items['ybr']),
                     float(bbox_items['xbr'])
                    ]
                )
            else:
                # Assumes box tags occur immediately after image tag
                im_found = False
    return ddl

def get_boxes(rewind_id):
    """ 
    Given a rewind ID that corresponds to a single
    rewind that has labels, parse all the xml files in that
    directory and return bounding box dicts. This follows
    the labeling methodology of Taskeater - they split each
    rewind into batches, so this loads and concatenates all
    those batches for this single rewind_id.
    
    :param rewind_id: unique identifier that corresponds to some
    directory of labeled data, eg hi-pipeline.stream.20190314T190830229
    :returns: list of bbox dicts
    """
    try:
        base_dir = os.environ['TRACKING_VALIDATION_DIR']
    except KeyError:
        raise ValueError('Please set TRACKING_VALIDATION_DIR env.')
    xml_dir = os.path.join(*[base_dir, 'labeled', rewind_id])
    if not os.path.isdir(xml_dir):
        raise ValueError(f'No labels exist for rewind_id {rewind_id}.'
                         'Did you run the download_raw script?')
    xml_files = sorted([os.path.join(xml_dir, f) for f in os.listdir(xml_dir)
                        if f.endswith('.xml')])
    return list(chain.from_iterable([_parse_xml(x) for x in xml_files]))
    
def get_tracks(rewind_id):
    """
    Get ground truth tracks for some rewind ID (see above).
    You must've already downloaded the labels to $TRACKING_VALIDATION_DIR
    via the download script.

    :param rewind_id: rewind identifier: hi-pipeline.stream.20190314T190830229
    :returns: list of track dicts
    """
    bbox_dicts = get_boxes(rewind_id)
    # Unpack bboxes to have only one bbox per element.
    new_bbox_dicts = []
    for sample in bbox_dicts:
        n_ele = len(sample['boxes'])
        for i in range(n_ele):
            new_bbox_dicts.append({'timestamp': sample['timestamp'],
                                   'boxes':sample['boxes'][i],
                                   'classes_text': sample['classes_text'][i]})
    new_bbox_dicts = sorted(new_bbox_dicts, key=lambda x: x['classes_text'])
    # Now inject bbox -> tracks into this tracks dict
    tracks = []
    for cnt, (cls, grp) in enumerate(
            groupby(new_bbox_dicts, key=lambda x: x['classes_text'])):
        track = defaultdict(list)
        for track_cnt, g in enumerate(grp):
            if track_cnt == 0:
                track['startTimestamp'] = g['timestamp']
                track['trackID'] = cnt + 1
            track['timestamps'].append(g['timestamp'])
            track['x'].append(int(np.mean([g['boxes'][1], g['boxes'][3]])))
            track['y'].append(int(np.mean([g['boxes'][0], g['boxes'][2]])))
        track['endTimestamp'] = g['timestamp']
        tracks.append(dict(track))
    # Sort these based on start times and correct trackIDs.
    tracks = sorted(tracks, key=lambda x: x['startTimestamp'])
    for i, track in enumerate(tracks):
        track['trackID'] = i + 1
    return tracks

def draw_boxes(rewind_id):
    boxes = get_boxes(rewind_id)
    color = ImageColor.getrgb('Red')
    for dd in boxes:
        pil_im = Image.open(dd['filename'])
        for idx in range(len(dd['classes_text'])):
            _boxes.draw_bounding_box_on_image(
                pil_im,
                *list(map(int, dd['boxes'][idx])), 
                color=color,
                use_normalized_coordinates=False,
                thickness=1,
                display_str=dd['classes_text'][idx])
        save_dir = os.path.dirname(
            dd['filename'].replace('frames','annotated_frames'))
        if not os.path.isdir(save_dir):
            os.mkdir(save_dir)
        pil_im.save(
            dd['filename'].replace(
                '.jpg','.bbox.jpg').replace(
                    'frames', 'annotated_frames'))
        print(f'Annotated {len(dd)} frames to {save_dir}')

def get_frames(rewind_id):
    pass

def save_tracks(tracks_or_rewind_id):
    pass
