"""
Model represents all detected objects in a scene at a moment in time
"""
import numpy as np
from scipy.optimize import linear_sum_assignment
from sort import KalmanBoxTracker
from itertools import compress
import copy
from bbox.utils import nms
from bbox.bbox2d_list import BBox2DList
from bbox.box_modes import XYXY, XYWH


class TrackingModel(object):
    """
    Represent the instantaneous state of all objects and tracking Ids
    """
    FIELDS = ['x1',
              'x2',
              'y1',
              'y2',
              'center_x',
              'center_y',
              'last_class',
              'first_added',
              'last_updated',
              'num_detections',
              'object_id',
              'nearest_neighbor_dist']

    def __init__(self, class_filter, threshold,
                 x_size, y_size, max_age, frame_rate,
                 distance_threshold,
                 new_distance_threshold,
                 use_kalman=True):
        """
        Constructor sets up tracking but doesn't initalise state.

        :param class_filter: list of class IDs to process, all others are
        excluded.
        :param threshold: detection threshold below which detections are ignored
        :param x_size: Pixel x dimension of movie
        :param y_size: Pixel y dimension of movie
        :param max_age: Maximum age since last update before objects are
        pruned from the list
        :param frame_rate: Frame rate in Hz
        :param distance_threshold: Maximum pixel distance for a detection to be
        considered valid
        :param new_distance_threshold: Minimum distance from an existing model
        item a detection needs to be to be added a as new object. This must be
        greater than distance_threshold
        :param use_kalman: Run a kalman filter on the tracking
        """

        self.next_id = 0
        self.timestamp = None

        self.threshold = threshold
        self.class_filter = class_filter
        self.x_size = x_size
        self.y_size = y_size

        self.max_age = max_age
        self.frame_rate = frame_rate

        self.distance_threshold = distance_threshold * (30 / self.frame_rate)
        self.new_detection_distance = new_distance_threshold
        self.kalman = []

        # Initialise arrays to contain the tracked objects
        self.data = None

        # A counter of unique detections over time.
        self.objects_tracked = 0
        self.num_detections = 0

        self.use_kalman = use_kalman

    def update(self, det, nonmaxsup=False):
        """
        Take a detections dictionary in the format delivered by
        convert_detections_data and updates this model

        :param det: Dictionary of detections
        :param nonmaxsup: Run non-max suppression of high IOU boxes
        """

        # --- DELETED OLD ---------------------------------------------
        if self.data is not None:
            old_filter = self.timestamp - self.data[8, :] < self.max_age

            self.data = self.data[:, old_filter]

            self.kalman = list(compress(self.kalman, old_filter))

            if self.data.shape[1] < 1:
                self.data = None

        # --- UPDATE KALMAN TRACKERS BEFORE WE MATCH DETECTIONS -------
            if self.use_kalman:
                for i, val in enumerate(self.kalman):
                    tx1, ty1, tx2, ty2 = self.kalman[i].predict()[0]
                    self.data[0, i] = tx1
                    self.data[1, i] = tx2
                    self.data[2, i] = ty1
                    self.data[3, i] = ty2
                    self.data[4, i] = tx1 + ((tx2 - tx1) / 2)
                    self.data[5, i] = ty1 + ((ty2 - ty1) / 2)

        self.timestamp = det['timestamp']

        # We're going to apply thresholding here even if we use NMS below
        # because NMS is slow so it's faster to pre-filter
        scores = np.array(det['detection_scores'])
        classes = np.array(det['detection_classes'])[scores > self.threshold]
        boxes = np.array(det['detection_boxes'])[scores > self.threshold]
        scores = scores[scores > self.threshold]

        class_filter = np.isin(classes, self.class_filter)
        classes = classes[class_filter]
        boxes = boxes[class_filter]
        scores = scores[class_filter]

        # Run non-max suppression if wanted
        if nonmaxsup:
            bb = BBox2DList(boxes, mode=XYXY)
            bb_idx = nms(bb, scores, self.threshold)

            boxes = boxes[bb_idx]
            classes = classes[bb_idx]

        length = len(classes)

        self.num_detections = len(boxes)

        # If we don't have any detections we don't need to do anything else now
        if self.num_detections == 0:
            return

        y1 = boxes[:, 0] * self.y_size
        x1 = boxes[:, 1] * self.x_size
        y2 = boxes[:, 2] * self.y_size
        x2 = boxes[:, 3] * self.x_size

        center_x = x1 + ((x2 - x1) / 2)
        center_y = y1 + ((y2 - y1) / 2)

        if self.data is not None:
            _, new_dist = self.calc_distances(self.get_data_field('center_x'),
                                              self.get_data_field('center_y'),
                                              center_x,
                                              center_y)

        new_data = np.array([x1,
                             x2,
                             y1,
                             y2,
                             center_x,
                             center_y,
                             classes,
                             np.repeat(self.timestamp, length),
                             np.repeat(self.timestamp, length),
                             np.repeat(1, length),
                             np.arange(self.next_id,
                                       self.next_id + length, 1),
                             np.empty([len(x1)])])  # Make space for Kalman

        # --- CLEAN UP AGED OBJECTS -----------------------------
        # We have a fixed memory allocation for numpy for optimal
        # performance but we'll eventually fill it, particularly in
        # a streaming paradigm so we need some pruning and clean up
        # however this is potentially costly in terms of processor
        # so we'll only trigger it when we get close to completely
        # allocating all available memory.

        # --- MATCH NEW DETECTIONS WITH EXISTING OBJECTS AND UPDATE ---
        if self.data is not None:
            idx, dist = self.calc_hungarian_distances(
                self.get_data_field("center_x"),
                self.get_data_field("center_y"),
                center_x,
                center_y)

            # Unmatched elements return an index of zero so exclude them and
            # anything over the distance threshold
            idx_filter = dist < self.get_data_field("nearest_neighbor_dist")

            # Get the indexes of all existing elements of the match for update
            model_index = np.arange(0, self.data.shape[1], 1)
            model_index_update = model_index[idx_filter]

            # Get the index off all matching new detections
            new_index = idx[idx_filter]

            # Get the indexes of all existing elements that WONT be updated
            new_index_unmod = np.arange(0, len(x1), 1)
            new_index_unmod = np.logical_not(np.isin(new_index_unmod,
                                                     new_index,))

            self.data[0][model_index_update] = x1[new_index]
            self.data[1][model_index_update] = x2[new_index]
            self.data[2][model_index_update] = y1[new_index]
            self.data[3][model_index_update] = y2[new_index]
            self.data[4][model_index_update] = center_x[new_index]
            self.data[5][model_index_update] = center_y[new_index]
            self.data[6][model_index_update] = classes[new_index]
            self.data[8][model_index_update] = self.timestamp
            self.data[9][model_index_update] = \
                self.data[9][model_index_update] + 1

            # Update Kalman trackers
            for i, j in enumerate(model_index_update):
                self.kalman[j].update([new_data[self.FIELDS.index('x1'),
                                                new_index[i]],
                                       new_data[self.FIELDS.index('y1'),
                                                new_index[i]],
                                       new_data[self.FIELDS.index('x2'),
                                                new_index[i]],
                                       new_data[self.FIELDS.index('y2'),
                                                new_index[i]]])

        # --- ADD NEW -------------------------------------------------
        # Any detections that weren't updated can be considered new and
        # need to be added

        if self.data is None:
            self.data = new_data
        else:
            # todo: This is potentially slow :(
            distant_new_idx = \
                np.logical_and(new_index_unmod,
                               new_dist > self.new_detection_distance)

            new_data = new_data[:, distant_new_idx]

            self.data = np.concatenate((self.data,
                                        new_data), axis=1)

        new_kalman = []
        for i in range(0, new_data.shape[1]):
            new_kalman.append(
                KalmanBoxTracker(
                    [new_data[self.FIELDS.index('x1'), i],
                     new_data[self.FIELDS.index('y1'), i],
                     new_data[self.FIELDS.index('x2'), i],
                     new_data[self.FIELDS.index('y2'), i]]))

        self.kalman = self.kalman + new_kalman

        self.next_id = self.next_id + length
        self.objects_tracked = self.objects_tracked + new_data.shape[1]

        neighbors = self.calc_detection_distances(
            self.get_data_field('center_x'),
            self.get_data_field('center_y'),
            self.get_data_field('center_x'),
            self.get_data_field('center_y'),
            ignore_self=True)[1]

        neighbors[neighbors > self.distance_threshold] = self.distance_threshold

        self.data[self.FIELDS.index("nearest_neighbor_dist"), :] = neighbors

    def get_data_field(self, field_name):
        """
        Returns the data field with a given field name, from the FIELDS list
        :param field_name: String name of field
        :return: Numpy array
        """

        return self.data[self.FIELDS.index(field_name)]

    def calc_distances(self, x1, y1, x2, y2):
        """
        Given lists of x,y points for two sets of detections calculate the
        minimum distances between corresponding points and return as a list
        indexed the same as the first set of points.

        :param x1: x positions of detection 1 as a list
        :param y1: y positions of detection 1 as a list
        :param x2: x positions of detection 2
        :param y2: y positions of detection 2
        :return: indexes of nearest detection 2 point to each detection 1 point,
        distance of nearest detection 2 point from each detection 1 point
        point
        """
        x_diff = self.__get_list_difference_matrix(x1, x2)
        y_diff = self.__get_list_difference_matrix(y1, y2)
        distance = np.sqrt(np.power(x_diff, 2) + np.power(y_diff, 2))
        return np.argmin(distance, axis=0), np.min(distance, axis=0)

    def calc_hungarian_distances(self, x1, y1, x2, y2):

        x_diff = self.__get_list_difference_matrix(x1, x2)
        y_diff = self.__get_list_difference_matrix(y1, y2)
        distance = np.sqrt(np.power(x_diff, 2) + np.power(y_diff, 2))

        dim = np.array(distance.shape).max()
        original_dim = len(x1)
        distance_pad = (np.zeros([dim, dim]) * 9999)
        distance_pad[:distance.shape[0], :distance.shape[1]] = distance

        rowi, coli = linear_sum_assignment(distance_pad)

        rowi = rowi[:original_dim]
        coli = coli[:original_dim]
        coli[coli >= len(x2)] = 0

        return coli, distance[rowi, coli]

    @staticmethod
    def __get_list_difference_matrix(list_a, list_b):
        """
        calculate the absolute difference of every element in list_a and list_b
        and return the results as a 2d array of differences with list_a on axis
        0 and list_b on axis 1

        :param list_a: first list of floats to difference
        :param list_b: second list of floats to difference
        :return: 2d array of differences (a x b)
        """
        arr_b = np.array(list_b)
        res = np.vstack([abs(arr_b - i) for i in list_a])
        return res

    def calc_detection_distances(self, x1, y1, x2, y2, ignore_self=False):
        """
        Given lists of x,y points for two sets of detections calculate the
        minimum distances between corresponding points and return as a list
        indexed the same as the first set of points.

        :param x1: x positions of detection 1 as a list
        :param y1: y positions of detection 1 as a list
        :param x2: x positions of detection 2
        :param y2: y positions of detection 2
        :return: indexes of nearest detection 2 point to each detection 1 point,
        distance of nearest detection 2 point from each detection 1 point
        point
        :param ignore_self: If list one and list two are the same the default
        behaviour will be to return each object as it's own nearest neighbour.
        Setting this to true will set all identity distances to np.inf which
        will solve this problem.
        """
        x_diff = self.__get_list_difference_matrix(x1, x2)
        y_diff = self.__get_list_difference_matrix(y1, y2)
        distance = np.sqrt(np.power(x_diff, 2) + np.power(y_diff, 2))
        if ignore_self and len(x1) == len(x2):
            distance[np.identity(len(x1)).astype(bool)] = np.inf
        return np.argmin(distance, axis=0), np.min(distance, axis=0)

    def get_as_detections(self):
        pass

    def get_centroid(self, class_filter):

        if self.data is not None:
            filter = np.isin(self.get_data_field("last_class"), class_filter)
            x = self.get_data_field("center_x")[filter]
            y = self.get_data_field("center_y")[filter]
            return np.mean(x), np.mean(y)
        else:
            return None


class BatchTrackingModel(object):
    """
    The Tracking model is stateless, it assigns an ID to items but
    to turn this into tracks we need a stateful implementation. This
    model takes batches of frames and detections, maintains an internal list
    of tracks and terminates and QAs tracked into a list of completed tracks.

    Completed tracks can be popped from the stack as required.
    """

    classes = {1: "BeachPerson",
               2: "SUP",
               3: "SurferPaddling",
               4: "SurferSitting",
               5: "SurferSurfing",
               6: "WhiteWater"}

    def __init__(self,
                 class_list,
                 threshold,
                 x_size, y_size,
                 max_age,
                 frame_rate,
                 distance_threshold,
                 new_distance_threshold,
                 nonmaxsup=False):

        self.nonmaxsup = nonmaxsup

        # Maintain a single Tracking model for surfer surfing state
        self.surfing_model = TrackingModel(class_list,
                                           threshold,
                                           x_size, y_size,
                                           max_age,
                                           frame_rate,
                                           distance_threshold,
                                           new_distance_threshold,
                                           use_kalman=True)

        # Stateful representation of the tracks will be maintained by object_id
        # in this dictionary
        self.tracks = {}
        # Completed tracks that pass QA will be written here and can be
        # be periodically popped and removed
        self.complete_tracks = []

    def update(self, detections):
        """
        Update the internal model states from a batch of detections

        :param detections: list of detection dictionaries
        """

        for i in range(0, len(detections)):

            self.surfing_model.update(detections[i], nonmaxsup=self.nonmaxsup)
            if self.surfing_model.data is not None:

                # For convenience and clarity retrieve required track params
                # into their own variables
                tracked_id = \
                    list(self.surfing_model.get_data_field("object_id"))
                x = self.surfing_model.get_data_field("center_x")
                y = self.surfing_model.get_data_field("center_y")
                last_updated = \
                    self.surfing_model.get_data_field("last_updated")

                existing_id = self.tracks.keys()

                # New Ids only exist in the tracked Ids, not the existing Ids
                new_id = list(set(tracked_id) - set(existing_id))
                # Update Ids exist in the tracked Ids and existing Ids
                update_id = list(set(tracked_id) & set(existing_id))
                # Ids are dead when they are no longer being tracked
                dead_id = list(set(existing_id) - set(tracked_id))

                # Add new tracks
                for j in new_id:
                    self.tracks[j] = \
                        {"timestamp": [self.surfing_model.timestamp],
                         "x": [int(x[tracked_id.index(j)])],
                         "y": [int(y[tracked_id.index(j)])],
                         "last_updated": last_updated[tracked_id.index(j)]}

                # Update tracks
                for j in update_id:
                    self.tracks[j]['timestamp'].append(
                        self.surfing_model.timestamp)
                    self.tracks[j]['x'].append(int(x[tracked_id.index(j)]))
                    self.tracks[j]['y'].append(int(y[tracked_id.index(j)]))
                    self.tracks[j]['last_updated'] = \
                        last_updated[tracked_id.index(j)]

                # Write complete tracks
                for j in dead_id:
                    # Trim the track back to the last detection (or we'll see
                    # the Kalman filter continue tracks forward until max_age
                    # seconds after the last detection
                    last_valid_id = self.tracks[j]['timestamp'].index(
                        self.tracks[j]['last_updated'])

                    trk_dict = {"start": self.tracks[j]['timestamp'][0],
                                "end":
                                    self.tracks[j]['timestamp'][last_valid_id],
                                "timestamp":
                                    self.tracks[j]['timestamp'][
                                    0:last_valid_id],
                                "x": self.tracks[j]['x'][0:last_valid_id],
                                "y": self.tracks[j]['y'][0:last_valid_id]}

                    # If we pass QA (track is longer than 1 second) append to
                    # the completed tracks list
                    if trk_dict['end'] - trk_dict['start'] > 1:
                        print("adding track")
                        self.complete_tracks.append(trk_dict)
                        print(len(self.complete_tracks))

                    self.tracks.pop(j, None)

    def pop_tracks(self):
        """
        Return the list of complete tracks and reset the list. We need this in
        a real-time batch processing situation. We don't know that tracks that
        are going to end will all end in the given batch (in fact we know
        they wont) and we don't want the list to be appended to indefinitely
        without being written somewhere persistent. So we want to be able to
        pull all tracks from the stack.
        :return: list of track dictionaries
        """
        temp_tracks = self.complete_tracks
        self.complete_tracks = []
        return temp_tracks
