"""
File takes the path of a local rewind and process with tracking, producing
logs and a diagnostic video
"""

import argparse
import pickle
import sys
from tracking_model import TrackingModel, BatchTrackingModel
import cv2
import numpy as np


def make_diagnostic_frame(track, frame):

    cv2.putText(frame, str(track.timestamp),
                (10, 50),
                cv2.FONT_HERSHEY_SIMPLEX,
                1,
                (255, 0, 0),
                1)

    cv2.putText(frame, "Count: " + str(track.data.shape[1]),
                (10, 90),
                cv2.FONT_HERSHEY_SIMPLEX,
                1,
                (255, 0, 0),
                1)

    for i in range(0, track.data.shape[1]):

        if track.get_data_field("num_detections")[i] > 0:
            # We can't write the full ID to the image so we'll just write the
            # first two digits
            id = str(int(track.get_data_field("object_id")[i]))

            # Create a random color for this tracking index
            np.random.seed(int(track.get_data_field("object_id")[i]))
            color = np.random.rand(3) * 255
            color = color.astype(int).tolist()

            cv2.putText(frame, id,
                       (int(track.get_data_field("x1")[i]),
                        int(track.get_data_field("y1")[i]) - 4),
                        cv2.FONT_HERSHEY_SIMPLEX,
                        0.5,
                        color,
                        1)

            if track.get_data_field("last_updated")[i] == track.timestamp:
                cv2.rectangle(frame,
                              (int(track.get_data_field("x1")[i]),
                               int(track.get_data_field("y1")[i])),
                              (int(track.get_data_field("x2")[i]),
                               int(track.get_data_field("y2")[i])),
                              color,
                              2)
    return frame


def main():
    parser = argparse.ArgumentParser(
      description='Test tracking algorithm with rewind and detections file.')

    parser.add_argument('--rewind', '-v', default=None, type=str,
      help='Full path to the .mp4 rewind you want to process.')

    parser.add_argument('--detections', '-d', default=None, type=str,
      help='Full path to a detections pickle file for that rewind.')

    parser.add_argument('--output', '-o', default=None, type=str,
      help='Full path to output video file this code will create to validate the tracking algorithm')

    parser.add_argument('--fs', '-f', default=8, type=int,
      help='How many frames to skip, relative to the FPS detections were run at.')

    parser = parser.parse_args()

    file = open(parser.detections, "rb")
    data = pickle.load(file)
    file.close()

    FRAME_SKIP = parser.fs

    batchtrack = BatchTrackingModel([5],
                                    threshold=0.26,
                                    x_size=1280,
                                    y_size=720,
                                    max_age=2,
                                    frame_rate=30 / FRAME_SKIP,
                                    distance_threshold=10,
                                    new_distance_threshold=30,
                                    nonmaxsup=True)

    cap = cv2.VideoCapture(parser.rewind)
    out = cv2.VideoWriter(parser.output,
                          cv2.VideoWriter_fourcc(*'mp4v'),
                          20.0,
                          (1280, 720))

    i = 0
    num_objects = []
    num_dets = []

    while cap.isOpened():

        ret, frame = cap.read()

        if ret and i < len(data):
            if i % FRAME_SKIP == 0:
                batchtrack.update([data[int(i / FRAME_SKIP)]])
                if batchtrack.surfing_model.data is not None:
                    frame = make_diagnostic_frame(batchtrack.surfing_model, frame)
                    out.write(frame)
                    num_objects.append(batchtrack.surfing_model.data.shape[1])
                    num_dets.append(batchtrack.surfing_model.num_detections)
            i = i + 1
        else:
            break

    print(f"{i} frames processed - {batchtrack.surfing_model.objects_tracked} objects identified")
    print(f"Track Count: {sum(num_objects) / len(num_objects)} Det Count: {sum(num_dets) / len(num_dets)}")
    print(f"Max Track Count: {max(num_objects)} Max Det Count {max(num_dets)}")
    tracks = batchtrack.pop_tracks()
    print(f"Number of tracks {len(tracks)}")

    x_disp_rate = []
    for track in tracks:
        x_disp = track['x'][-1] - track['x'][0]
        y_disp = track['y'][-1] - track['y'][0]
        t_disp = track['timestamp'][-1] - track['timestamp'][0]
        x_disp_rate.append(x_disp / t_disp)
    x_disp_rate = np.array(x_disp_rate)

    x_disp_rate = x_disp_rate[
        abs(x_disp_rate - np.mean(x_disp_rate)) < np.std(x_disp_rate)]

    print(np.mean(x_disp_rate))

if __name__ == '__main__':
    main()
